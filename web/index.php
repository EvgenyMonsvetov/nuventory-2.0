<?php

$yiiDebug = (isset($_ENV['YII_DEBUG']) || isset($_SERVER['YII_DEBUG'])) ? getenv('YII_DEBUG') : true;
$yiiEnv   = (isset($_ENV['YII_ENV'])   || isset($_SERVER['YII_ENV']))   ? getenv('YII_ENV')   : 'dev';

defined('YII_DEBUG') or define('YII_DEBUG', (boolean)$yiiDebug);
defined('YII_ENV')   or define('YII_ENV',   $yiiEnv);

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();

