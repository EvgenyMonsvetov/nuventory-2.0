<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\MasterCategory as MasterCategoryModel;

class MasterCategory extends Component
{
    public function generateXls($params) {
        $filePrefix = 'MasterCategories_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new MasterCategoryModel();
	    $query = $model->getSearchQuery($params);

	    $xlsService = new XLSService();
	    return $xlsService->generateXls($params, $query, $filename);
    }
}