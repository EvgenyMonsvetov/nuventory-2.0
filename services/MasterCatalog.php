<?php

namespace app\services;

use app\models\Company;
use app\models\File;
use app\models\Location;
use app\models\Upload;
use app\services\Upload as UploadService;
use Yii;
use yii\base\Component;
use app\models\MasterCatalog as MasterCatalogModel;
use app\models\MasterCatalog as MasterDataModel;

class MasterCatalog extends Component
{
    public function createMasterCatalog($data)
    {
        $errors = [];
        $masterDataWithImage = null;

        $data = Yii::$app->request->post();

        if (isset($data['MD0_ID']) && (int)$data['MD0_ID'] > 0) {
	        $masterCatalogModel = MasterCatalogModel::findOne((int)$data['MD0_ID']);
	        $masterCatalogModel->MD0_ID = (int)$data['MD0_ID'];
            $data['MD0_CREATED_ON'] = $masterCatalogModel->MD0_CREATED_ON;
            $data['MD0_MODIFIED_ON'] = $masterCatalogModel->MD0_MODIFIED_ON;

            if ( ! empty($data['FI1_ID'])) {

                $user = Yii::$app->user->getIdentity();
                $company = Company::findOne(['CO1_ID' => $user->CO1_ID]);
                $fromLocation = Location::findOne(['LO1_ID' => $user->LO1_ID]);

                $masterDataWithImageModels = \app\models\MasterData::find()
                    ->select([
                        'md1_master_data.MD1_ID',
                        'MD1_PART_NUMBER',
                        'MD1_DESC1',
                        'FI1_FILE_PATH'
                    ])
                    ->leftJoin("md2_location as md2_to", "md2_to.MD1_ID=md1_master_data.MD1_ID AND md2_to.MD2_DELETE_FLAG=0")
                    ->leftJoin("fi1_file as fi1", "fi1.FI1_ID=md1_master_data.FI1_ID AND fi1.FI1_DELETE_FLAG=0")
                    ->where([
                        'md1_master_data.MD0_ID' => (int)$data['MD0_ID'],
                        'md1_master_data.CO1_ID' => $company->CO1_ID,
                        'md2_to.LO1_ID'          => $fromLocation->LO1_ID,
                    ])
                    ->andWhere(['not', ['fi1.FI1_ID' => null]])
                    ->all();

                if ( ! empty($masterDataWithImageModels)) {
                    /** @var \app\models\MasterData $item */
                    foreach ($masterDataWithImageModels as $model) {
                        $item = $model->toArray();
                        if ( ! empty($item['FI1_FILE_PATH'])) {
                            $masterDataWithImage[] = [
                                'MD1_ID'          => $item['MD1_ID'],
                                'MD1_PART_NUMBER' => $item['MD1_PART_NUMBER'],
                                'MD1_DESC1'       => $item['MD1_DESC1'],
                                'FI1_FILE_PATH'   => $item['FI1_FILE_PATH'],
                            ];
                        }
                    }
                }
            }

        } else {
            unset($data['MD0_ID']);
	        $masterCatalogModel = new MasterCatalogModel();
        }

	    $FI1_ID = $masterCatalogModel->FI1_ID;

        if ( !($masterCatalogModel->load($data, '') && $masterCatalogModel->save( true )) ){
            $errors = $masterCatalogModel->getErrorSummary(true);
        } else {
	        // save/delete image
	        if ($FI1_ID && $FI1_ID > 0 && ($FI1_ID != $masterCatalogModel->FI1_ID)) {
		        $file = File::findOne(['FI1_ID' => $FI1_ID]);
		        if ($file) {
			        $file->delete();
		        }
	        }
        }

        return [
            'errors'              => $errors,
            'masterdatawithimage' => $masterDataWithImage
        ];
    }

    public function deleteMasterCatalog($id)
    {
        $model = MasterCatalogModel::find()->where(["MD0_ID" => $id])->one();

        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    public function updateParts($records)
    {
        $errors = [];

        foreach($records as $data)
        {
            if (isset($data['MASTER_DATA']) && $data['MASTER_DATA'] == 1) {
                $model = MasterCatalogModel::findOne((int)$data['MD1_ID']);

                if (!$model) {
                    $model = new MasterCatalogModel();
                    $model->MD0_ID = $data['MD1_ID'];
                    if (isset($data['FI0_ID']) && $data['FI0_ID'] && $data['FI0_ID'] != '')
                       $model->FI1_ID = (int)$data['FI0_ID'];
                } else {
                    if (isset($data['FI1_ID']) && $data['FI1_ID'] && $data['FI1_ID'] != '')
                       $model->FI1_ID = (int)$data['FI1_ID'];
                }

                if (isset($data['MD0_DESC1']) && $data['MD0_DESC1'] && $data['MD0_DESC1'] != '')
                    $model->MD0_DESC1 = $data['MD0_DESC1'];
                if (isset($data['MD0_BODY']) && $data['MD0_BODY'] && $data['MD0_BODY'] != '')
                    $model->MD0_BODY = (int)$data['MD0_BODY'];
                if (isset($data['MD0_DETAIL']) && $data['MD0_DETAIL'] && $data['MD0_DETAIL'] != '')
                    $model->MD0_DETAIL = (int)$data['MD0_DETAIL'];
                if (isset($data['MD0_PAINT']) && $data['MD0_PAINT'] && $data['MD0_PAINT'] != '')
                   $model->MD0_PAINT = (int)$data['MD0_PAINT'];
                if (isset($data['MD0_UNIT_PRICE']) && $data['MD0_UNIT_PRICE'] && $data['MD0_UNIT_PRICE'] != '')
                   $model->MD0_UNIT_PRICE = $data['MD0_UNIT_PRICE'];
                if (isset($data['UM1_DEFAULT_PRICING']) && $data['UM1_DEFAULT_PRICING'] && $data['UM1_DEFAULT_PRICING'] != '')
                   $model->UM1_DEFAULT_PRICING = $data['UM1_DEFAULT_PRICING'];
                if (isset($data['MD0_MARKUP']) && $data['MD0_MARKUP'] && $data['MD0_MARKUP'] != '')
                   $model->MD0_MARKUP = $data['MD0_MARKUP'];
                if (isset($data['MD0_PARENT_ID']) && $data['MD0_PARENT_ID'] && $data['MD0_PARENT_ID'] != '')
                   $model->MD0_PARENT_ID = $data['MD0_PARENT_ID'];

                if ( !$model->save(false) ){
                    $errors = $model->getErrorSummary(true);
                }
            } else {
                $model = MasterDataModel::findOne((int)$data['MD1_ID']);

                if (!$model) {
                    $model = new MasterDataModel();
                    $model->MD1_ID = $data['MD1_ID'];

                    if (isset($data['MD0_DESC1']) && $data['MD0_DESC1'] && $data['MD0_DESC1'] != '')
                        $model->MD1_DESC1 = $data['MD0_DESC1'];
                    if (isset($data['MD0_BODY']) && $data['MD0_BODY'] && $data['MD0_BODY'] != '')
                        $model->MD1_BODY = (int)$data['MD0_BODY'];
                    if (isset($data['MD0_DETAIL']) && $data['MD0_DETAIL'] && $data['MD0_DETAIL'] != '')
                        $model->MD1_DETAIL = (int)$data['MD0_DETAIL'];
                    if (isset($data['MD0_PAINT']) && $data['MD0_PAINT'] && $data['MD0_PAINT'] != '')
                       $model->MD1_PAINT = (int)$data['MD0_PAINT'];
                    if (isset($data['MD0_UNIT_PRICE']) && $data['MD0_UNIT_PRICE'] && $data['MD0_UNIT_PRICE'] != '')
                       $model->MD1_UNIT_PRICE = $data['MD0_UNIT_PRICE'];
                    if (isset($data['UM1_PRICING_ID']) && $data['UM1_PRICING_ID'] && $data['UM1_PRICING_ID'] != '')
                       $model->UM1_DEFAULT_PRICING = $data['UM1_PRICING_ID'];
                    if (isset($data['MD0_MARKUP']) && $data['MD0_MARKUP'] && $data['MD0_MARKUP'] != '')
                       $model->MD1_MARKUP = $data['MD0_MARKUP'];
                    if (isset($data['FI0_ID']) && $data['FI0_ID'] && $data['FI0_ID'] != '')
                       $model->FI1_ID = (int)$data['FI0_ID'];
                } else {
                    if (isset($data['MD0_DESC1']) && $data['MD0_DESC1'] && $data['MD0_DESC1'] != '')
                        $model->MD1_DESC1 = $data['MD0_DESC1'];
                    if (isset($data['MD0_BODY']) && $data['MD0_BODY'] && $data['MD0_BODY'] != '')
                        $model->MD1_BODY = (int)$data['MD0_BODY'];
                    if (isset($data['MD0_DETAIL']) && $data['MD0_DETAIL'] && $data['MD0_DETAIL'] != '')
                        $model->MD1_DETAIL = (int)$data['MD0_DETAIL'];
                    if (isset($data['MD0_PAINT']) && $data['MD0_PAINT'] && $data['MD0_PAINT'] != '')
                       $model->MD1_PAINT = (int)$data['MD0_PAINT'];
                    if (isset($data['MD0_UNIT_PRICE']) && $data['MD0_UNIT_PRICE'] && $data['MD0_UNIT_PRICE'] != '')
                       $model->MD1_UNIT_PRICE = $data['MD0_UNIT_PRICE'];
                    if (isset($data['UM1_PRICING_ID']) && $data['UM1_PRICING_ID'] && $data['UM1_PRICING_ID'] != '')
                       $model->UM1_DEFAULT_PRICING = $data['UM1_PRICING_ID'];
                    if (isset($data['MD0_MARKUP']) && $data['MD0_MARKUP'] && $data['MD0_MARKUP'] != '')
                       $model->MD1_MARKUP = $data['MD0_MARKUP'];
                    if (isset($data['FI1_ID']) && $data['FI1_ID'] && $data['FI1_ID'] != '')
                       $model->FI1_ID = (int)$data['FI1_ID'];
                }

                if ( !$model->save(false) ){
                    $errors = $model->getErrorSummary(true);
                }

            }
        }

        return $errors;
    }

	public function generateXls($params) {
		$filePrefix = 'VendorCatalogs_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new MasterCatalogModel();
		$query = $model->getQuery($params)->limit(10000);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}

    public function setDefaultImages(int $MD0_ID, array $MD1_IDS = []): array
    {
        $errors = [];

        $masterCatalogModel = MasterCatalogModel::findOne($MD0_ID);

        $user = Yii::$app->user->getIdentity();
        $company = Company::findOne(['CO1_ID' => $user->CO1_ID]);
        $fromLocation = Location::findOne(['LO1_ID' => $user->LO1_ID]);

        $masterDataWithImageModels = \app\models\MasterData::find()
            ->leftJoin("md2_location as md2_to", "md2_to.MD1_ID=md1_master_data.MD1_ID AND md2_to.MD2_DELETE_FLAG=0")
            ->leftJoin("fi1_file as fi1", "fi1.FI1_ID=md1_master_data.FI1_ID AND fi1.FI1_DELETE_FLAG=0")
            ->where([
                'md1_master_data.MD0_ID' => $MD0_ID,
                'md1_master_data.CO1_ID' => $company->CO1_ID,
                'md2_to.LO1_ID'          => $fromLocation->LO1_ID,
            ])
            ->andWhere(['not', ['fi1.FI1_ID' => null]])
            ->all();

        $upload = new UploadService();
        $file = File::findOne($masterCatalogModel->FI1_ID);
        $path = Yii::$app->params['partsImagesPath'] . DIRECTORY_SEPARATOR . $file->FI1_FILE_PATH;

        $transaction = Yii::$app->db->beginTransaction();

        foreach ($masterDataWithImageModels as $masterDataWithImageModel) {

            if (empty($masterDataWithImageModel->FI1_ID) || in_array($masterDataWithImageModel->MD1_ID, $MD1_IDS)) {

                $model = new Upload();
                $result = $model->saveImage($path);

                if ($result['success']) {

                    $file = new File();
                    $file->FI1_FILE_NAME = $result['name'];
                    $file->FI1_FILE_PATH = $result['path'];
                    $file->FI1_FILE_SIZE = $result['size'];
                    $file->FI1_FILE_TYPE = $result['type'] == 'image/png' || $result['type'] == 'image/jpeg' || $result['type'] == 'image/gif' ? 1 : 2;

                    if ( ! $file->save()) {
                        $errors[] = $file->getErrors();
                    }
                } else {
                    $errors = $result['errors'];
                }

                if (empty($errors)) {
                    $masterDataWithImageModel->FI1_ID = $file->FI1_ID;
                    if ($masterDataWithImageModel->save() === false)
                        $errors = $masterDataWithImageModel->getErrors();
                }

                if ( ! empty($errors)) {
                    $transaction->rollBack();
                    break;
                }
            }
        }

        if (empty($errors)) {
            $transaction->commit();
        }

        return [
            'success' => empty($errors),
            'errors'  => $errors
        ];
    }
}
