<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\Number as NumberModel;
use app\models\Vendor;
use app\models\PurchaseOrder;
use yii\data\ActiveDataProvider;

class Number extends Component
{
    /**
     * @return NumberModel
     */
    public function getOrderNumber()
    {
        $number = NumberModel::find()->one();
        if (!$number){
            $number = new NumberModel();
            $number->OR1_ID = 0;
            $number->OR1_NUMBER = 0;
        }

        $vendors = Vendor::find()->alias('VD1')
            ->select(['total' => 'count(VD1.VD1_ID)'])
            ->where(['<>', 'VD1.VD1_DELETE_FLAG', 1])
            ->asArray()
            ->one();

        $number->OR1_ID += $vendors['total'];
        $number->OR1_NUMBER += $vendors['total'];
        $number->NU1_LAST_SESSION = Yii::$app->session->getId();
        $number->save( false );

        return $number;
    }

    public function getPurchaseOrderNumber()
    {
        $number = NumberModel::find()->one();
        if (!$number){
            $number = new NumberModel();
            $number->PO1_ID = 1;
            $number->PO1_NUMBER = 1;
        }

        $model = PurchaseOrder::find()->alias('PO1')
            ->select([
            'GREATEST(if(MAX(PO1.PO1_ID) IS NULL, 1, MAX(PO1.PO1_ID)+1), NU1.PO1_ID+1) AS PO1_NUMBER',
            'GREATEST(if(MAX(PO1.PO1_ID) IS NULL, 1, MAX(PO1.PO1_ID)+1), NU1.PO1_ID+1) AS PO1_ID'])
            ->leftJoin('nu1_number as NU1' , '1 = 1')
            ->groupBy(['PO1.PO1_ID', 'NU1.PO1_ID'])
            ->asArray()
            ->one();

        if($model){
            $number->PO1_ID     = $model['PO1_ID'];
            $number->PO1_NUMBER = $model['PO1_NUMBER'];
        }

        $number->NU1_LAST_SESSION = Yii::$app->session->getId();
        $number->save( false );

        return $number;
    }
}