<?php

namespace app\services;

use yii\base\Component;
use app\models\Inventory as InventoryModel;
use yii\data\ArrayDataProvider;
use yii2tech\spreadsheet\Spreadsheet;
use app\models\Company as CompanyModel;
use app\models\Location as ShopModel;

class Inventory extends Component
{
	public function getManualInventoryEdits( $params )
	{
		$masterData = new \app\models\Inventory();
		$models = $masterData->getManualInventoryEdits($params);
		$result = array();

		if (isset($models) && $models != false) {
			$tempSeries = array();

			$drilldownData = $masterData->getManualInventoryEdits($params, true);

			foreach ($models as $model) {
				$array = [];
				$array['name'] = $model['LO1_NAME'];
				$array['y'] = floatval($model['manual_edits']);
				$array['cost'] = floatval($model['cost']);
				$array['drilldown'] = $model['LO1_SHORT_CODE'];
				$tempSeries[] = $array;

				$drilldown = [];
				$drilldown['name'] = $model['LO1_NAME'];
				$drilldown['id'] = $model['LO1_SHORT_CODE'];

				foreach ($drilldownData as $drill) {
					if ($drill['LO1_ID'] == $model['LO1_ID'] && $drill['manual_edits'] > 0) {
						$tempDrillDownAdd = [];
						$tempDrillDownAdd['name'] = $drill['US1_NAME'];
						$tempDrillDownAdd['y'] = floatval($drill['manual_edits']);
						$tempDrillDownAdd['cost'] = floatval($drill['cost']);
						$drilldown['data'][] = $tempDrillDownAdd;
					}
				}

				$result['drilldown']['series'][] = $drilldown;
			}

			$temp = [];
			$temp['name'] = 'Manual Edits Data';
			$temp['data'] = array_values($tempSeries);
			$temp['drilldown'] = isset($result['drilldown']['series']) ? array_values($result['drilldown']['series']) : array_values(array());

			$result = $temp;
		}

		return $result;
	}

	public function getManualInventoryEditsExport( $params )
	{
		$masterData = new \app\models\Inventory();
		$models = $masterData->getManualInventoryEdits($params);

		if (isset($models) && $models != false) {
			$tempSeries = array();

			$drilldownData = $masterData->getManualInventoryEdits($params, true);

			foreach ($models as &$model) {
				foreach ($drilldownData as $drill) {
					if ($drill['LO1_ID'] == $model['LO1_ID'] && $drill['manual_edits'] > 0) {
						$model['data'][] = $drill;
					}
				}
			}
		}

		return $models;
	}

	public function generateXls($params) {
		switch ($params['subtype']) {
			case 'manualInventoryEdits':
				return $this->getManualInventoryEditsXls($params);
				break;
			default:
				return $this->getInventoryXls($params);
		}
	}

	private function getInventoryXls($params) {
		$filePrefix = 'InventoryActivities_';
		$model = new InventoryModel();

		$models = $model->getActivityLogsForXLS($params);
		$query = '';

		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename, $models);
	}

	private function getManualInventoryEditsXls($params) {
		$filePrefix = 'ManualInventoryEditsXls_';

		$data = $this->getManualInventoryEditsExport($params);

		$currDate = Date('Ymd_His');
		$levelName = $params['level'] == 1 ? 'CompanyLevel_' : 'ShopLevel_';
		$filename = $filePrefix . $levelName . $currDate . '.xls' ;

		return $this->generateManualInventoryEditsXls($params, $filename, $data);
	}

	private function generateManualInventoryEditsXls($params, $filename, $items = []) {
		$level = $params['level'];
		$spreadsheet = new Spreadsheet([
			'dataProvider' => new ArrayDataProvider([
				'allModels' => []
			]),
			'writerType' => 'Xls'
		]);

		$startDate = Date('m/Y', strtotime($params['startDate']));
		$endDate = Date('m/Y', strtotime($params['endDate']));

		$companies = CompanyModel::find()
			->select(['CO1_NAME'])
			->where(['IN', 'CO1_ID', explode(',', $params['CO1_IDs'])])
			->orderBy(['CO1_NAME' => SORT_ASC])->all();

		if ($level == 1) {
			$companyNames  = implode(', ', array_column($companies, 'CO1_NAME'));
		} else {
			$shop = ShopModel::find()->alias('LO1')
				->select(['Lo1.LO1_NAME', 'CO1.CO1_NAME'])
				->leftJoin("co1_company as CO1" , "LO1.CO1_ID = CO1.CO1_ID")
				->where(['LO1.LO1_SHORT_CODE' => $params['shopLevelId']])->one();
			$companyNames  = $shop['CO1_NAME'];
		}

		$dateRowText = 'Date: ' . $startDate . ' - ' . $endDate;
		$companyRowText = 'Company Name: ' . $companyNames;

		$spreadsheet->getDocument()->setActiveSheetIndex(0);
		$objWorksheet = $spreadsheet->getDocument()->getActiveSheet();
		$objWorksheet->getColumnDimension('A')->setWidth(40);
		$objWorksheet->getColumnDimension('B')->setWidth(20);
		$objWorksheet->getColumnDimension('C')->setWidth(12);

		$currentRowIndex = 1;

		$objWorksheet->setCellValueByColumnAndRow(1, $currentRowIndex, $dateRowText);
		$objWorksheet->getStyleByColumnAndRow(1, $currentRowIndex)->getFont()->getColor()->setRGB('000000');
		$objWorksheet->getStyleByColumnAndRow(1, $currentRowIndex)->getFont()->setBold(true);
		$currentRowIndex ++;

		$objWorksheet->setCellValueByColumnAndRow(1, $currentRowIndex, $companyRowText);
		$objWorksheet->getStyleByColumnAndRow(1, $currentRowIndex)->getFont()->getColor()->setRGB('000000');
		$objWorksheet->getStyleByColumnAndRow(1, $currentRowIndex)->getFont()->setBold(true);
		$currentRowIndex ++;

		if ($level == 1) {
			$currentRowIndex += 1;
			$objWorksheet->setCellValueByColumnAndRow(1, $currentRowIndex, 'Shop Name');
		} else {
			$shopRowText = 'Shop Name: ' . $shop['LO1_NAME'];

			$objWorksheet->setCellValueByColumnAndRow(1, $currentRowIndex, $shopRowText);
			$objWorksheet->getStyleByColumnAndRow(1, $currentRowIndex)->getFont()->getColor()->setRGB('000000');
			$objWorksheet->getStyleByColumnAndRow(1, $currentRowIndex)->getFont()->setBold(true);
			$currentRowIndex += 2;
			$objWorksheet->setCellValueByColumnAndRow(1, $currentRowIndex, 'User Name');
		}
		$objWorksheet->setCellValueByColumnAndRow(2, $currentRowIndex, 'Number of manual Edits');
		$objWorksheet->setCellValueByColumnAndRow(3, $currentRowIndex, 'Cost $');
		$currentRowIndex ++;

		$shopColumns = ['LO1_NAME', 'manual_edits', 'cost'];
		$userColumns = ['US1_NAME', 'manual_edits', 'cost'];

		if ($level == 1) {
			foreach ($items as $shop) {
				$columnIndex = 1;
				foreach ($shopColumns as $column) {
					$objWorksheet->setCellValueByColumnAndRow($columnIndex, $currentRowIndex, $shop[$column]);
					$columnIndex ++;
				}
				$currentRowIndex ++;
				foreach ($shop['data'] as $user) {
					$columnIndex = 1;
					foreach ($userColumns as $column) {
						$objWorksheet->setCellValueByColumnAndRow($columnIndex, $currentRowIndex, $user[$column]);
						$objWorksheet->getStyleByColumnAndRow($columnIndex, $currentRowIndex)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
						$columnIndex ++;
					}
					$currentRowIndex ++;
				}
			}
		} else {
			foreach ($items as $shop) {
				if ($shop['LO1_SHORT_CODE'] == $params['shopLevelId']) {
					foreach ($shop['data'] as $user) {
						$columnIndex = 1;
						foreach ($userColumns as $column) {
							$objWorksheet->setCellValueByColumnAndRow($columnIndex, $currentRowIndex, $user[$column]);
							$columnIndex ++;
						}
						$currentRowIndex ++;
					}
					break;
				}
			}

		}

		return $spreadsheet->send($filename);
	}
}