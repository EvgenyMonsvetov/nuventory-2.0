<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 09/07/2018
 * Time: 10:13
 */

namespace app\services;

use yii\base\Component;
use app\models\Customer as CustomerModel;

class Customer extends Component
{
    public function generateShortCode()
    {
        $customerModel = new CustomerModel();
        $customers = $customerModel->getSearchQuery()->all();

        $CU1_SHORT_CODE = 100;
        for( $i=0; $i<count($customers); $i++){
            if ($i<0){
                $i=0;
            }
            $customer = $customers[$i];
            if ($customer->CU1_SHORT_CODE == $CU1_SHORT_CODE) {
                $CU1_SHORT_CODE ++;
                $i --;
            }
        }

        return $CU1_SHORT_CODE;
    }

	public function generateXls($params) {
		$filePrefix = 'Customers_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new CustomerModel();
		$query = $model->getSearchQuery($params);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}
}