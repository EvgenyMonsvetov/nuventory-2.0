<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 09/07/2018
 * Time: 10:13
 */

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\Vendor as VendorModel;

class Vendor extends Component
{
    public function createVendor($data)
    {
        $errors = [];

        $data = Yii::$app->request->post();

        if (isset($data['VD1_ID']) && (int)$data['VD1_ID'] > 0) {
            $vendorModel = VendorModel::findOne((int)$data['VD1_ID']);
            $vendorModel->VD1_ID = (int)$data['VD1_ID'];
            $data['VD1_CREATED_ON'] = $vendorModel->VD1_CREATED_ON;
            $data['VD1_MODIFIED_ON'] = $vendorModel->VD1_MODIFIED_ON;
        } else {
            unset($data['VD1_ID']);
            $vendorModel = new VendorModel();
        }

        if ( !($vendorModel->load($data, '') && $vendorModel->save( true )) ){
            $errors = $vendorModel->getErrorSummary(true);
        }

        return $errors;
    }

    public function deleteVendor($id)
    {
        $vendorModel = new VendorModel();
        $model = VendorModel::find()->where(["VD1_ID" => $id])->one();

        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

	public function generateXls($params) {
		$filePrefix = 'Vendors_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new VendorModel();
		$query = $model->getQuery($params);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}
}