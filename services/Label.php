<?php
namespace app\services;

use yii\base\Component;
use Yii;
use app\models\Label as LabelModel;
use yii\data\ActiveDataProvider;

class Label extends Component
{
    public function getAllVocabularies($params)
    {
    	$model = new LabelModel();
        $query = $model->getQuery($params);
        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize'  => $params['perPage'],
                    'page'      => $params['page']
                ]
            ]);

        return $dataProvider;
    }

    public function getVocabulary( $params )
    {
    	$keys = $params['keys'];
	    $model = new LabelModel();
	    $query = $model->getQuery($params);
        $query->andFilterWhere(['in', 'la2_label.LA2_KEY', explode(',', $keys)]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'  => $params['perPage'],
                'page'      => $params['page']
            ]
        ]);

        return $dataProvider;
    }

    public function createLabel($data)
    {
        $errors = [];

        $labelModel = new LabelModel();
        $model = LabelModel::find()->alias("la2_label")
	            ->select(["*"])
                ->where(["LA1_ID" => (int)$data['LA1_ID']])
                ->andWhere(['LA2_KEY' => $data['LA2_KEY']])
                ->one();

        if (isset($model) && isset($model['LA2_ID']) && $model['LA2_ID'] > 0) {
            $model->load([
	            'LA2_KEY'  => $data['LA2_KEY'],
                'LA1_ID'   => (int)$data['LA1_ID'],
	            'LA2_TEXT' => $data['LA2_TEXT']
             ], '');
        } else {
            $model = new LabelModel();
            $model->load([
                'LA2_KEY'           => $data['LA2_KEY'],
                'LA1_ID'            => (int)$data['LA1_ID'],
                'LA2_TEXT'          => $data['LA2_TEXT'],
                'LA2_DESCRIPTION'   => '',
                'LA2_DELETE_FLAG'   => 0,
                'LA2_CREATED_BY'    => Yii::$app->user->identity->US1_ID,
                'LA2_MODIFIED_BY'   => Yii::$app->user->identity->US1_ID
            ], '');
        }

        $model->save();
        if ($model->hasErrors()) {
            $errors = $importHistoryModel->getErrors();
        }

        return $errors;
    }

	public function generateXls($params) {
		$filePrefix = 'Translations_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls';

		$dataProvider = $this->getAllVocabularies($params);
		$dataProvider->query->asArray();
		$keys = $dataProvider->getModels();
		$columns = explode(',', $params['columns']);

		$vocabulary = [];
        for ($i = 0; $i < sizeof($keys); $i++) {
			$item = ['LA2_KEY' => $keys[$i]['LA2_KEY']];
            $langs = explode(';', $keys[$i]['List']);
			for ($j = 0; $j < sizeof($langs); $j++) {
				for ($columnIndex = 0; $columnIndex < sizeof($columns); $columnIndex++) {
					$record =  explode('::', $langs[$j]);
					if ($columns[$columnIndex] == $record[0]) {
						$item[$record[0]] = $record[1] ? urldecode($record[1]) : '';
						break;
					}
				}
			}
	        array_push($vocabulary, $item);
        }

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, '', $filename, $vocabulary);
	}

}