<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 13/07/2018
 * Time: 13:53
 */

namespace app\services;

use app\models\vTransaction;
use yii\base\Component;
use app\models\Transaction as TransactionModel;
use app\models\ImportHistory as ImportHistoryModel;
use yii\db\Exception;
use yii\helpers\VarDumper;
use Yii;

class Transaction extends Component
{
    const ERR_ED1_TYPE = 1;

    public function getErrorDescription( $errType = null )
    {
        $errors = [
            self::ERR_ED1_TYPE => 'ED1_TYPE id empty'
        ];
        return ($errType && !empty($errors[$errType])) ? $errors[$errType] : null;
    }
    /**
     * @param $companyName
     * @return \app\models\Company
     */
    public function import( $data )
    {
        $startImport = microtime(true);

        $companyService = new \app\services\Company();
        $company = $companyService->registerCompany( $data['CompanyName'] );

        $transactionsAdded  = 0;
        $transactionsErrors = 0;
        if(!$company->hasErrors()){

            $importHistoryModel = new ImportHistoryModel();
            $importHistoryModel->load([
                'CO1_ID'         => $company->CO1_ID,
                'IH1_TYPE'       => ImportHistoryModel::TYPE_TRANSACTIONS,
                'IH1_CREATED_ON' => date('Y-m-d H:i:s'),
                'IH1_FILEPATH'   => '',
                'IH1_REMOTE_IP'  => \Yii::$app->getRequest()->getUserIP(),
                'IH1_ADDED'      => 0,
                'IH1_UPDATED'    => 0,
                'IH1_ALL'        => count($data['Transactions']),
                'IH1_ERRORS'     => 0
            ], '');

            $importHistoryService = new ImportHistory();
            $importHistoryModel->IH1_FILEPATH = $importHistoryService->saveFile( $data, $importHistoryModel );

            $importHistoryModel->save();

            if( !$importHistoryModel->hasErrors() ){
                $result = $this->_importRealData(
                    $data['Transactions'], $company->CO1_ID, $importHistoryModel->IH1_ID
                );

                $transactionsAdded  = $result['transactionsAdded'];
                $transactionsErrors = $result['transactionsErrors'];

                $importHistoryModel->IH1_ADDED = $transactionsAdded;
                $importHistoryModel->IH1_ERRORS = $transactionsErrors;
                $importHistoryModel->save();

                $importHistoryService = new ImportHistory();
                $importHistoryService->saveFile( $data, $importHistoryModel );


                $errors = $result['errors'];
            }else{
                $errors = $importHistoryModel->getErrors();
            }
        }else{
            $errors = $company->getErrors();
        }

        $importTime = microtime(true) - $startImport;
        return [
            'success'            => !$errors,
            'errors'             => $errors,
            'importTime'         => sprintf( "%.2f s", $importTime),
            'transactionsAdded'  => $transactionsAdded,
            'transactionsErrors' => $transactionsErrors
        ];
    }

    private function _prepareTransactions( &$transactions, $CO1_ID, $IH1_ID, $testMode=false)
    {
        $date      = date('Y-m-d');
        $timeStamp = date('Y-m-d h:i:s');
        $uploadTimestamp = round(microtime(true) * 1000);
        $values = [];

        foreach( $transactions as &$transaction ){
            $item = [];

            $item['DATE']             = $date;
            $item['TIMESTAMP']        = $timeStamp;

            $item['ED1_ID']           = $testMode ? rand(1000000, 1100000) : (int)$transaction['ED1_ID'];
            $item['ED1_TYPE']         = $transaction['ED1_TYPE'];

            $errorCode = 0;
            if(!$transaction['ED1_TYPE']){
                $errorCode = self::ERR_ED1_TYPE;
            }

            $item['ED1_DOCUMENT_NO']  = $transaction['ED1_DOCUMENT_NO'];
            $item['ED1_FILENAME']     = $transaction['ED1_FILENAME'];
            $item['ED1_STATUS']       = (int)$transaction['ED1_STATUS'];
            $item['CU1_ID']           = (int)$transaction['CU1_ID'];
            $item['VD1_ID']           = (int)$transaction['VD1_ID'];

            $modifiedOn = new \DateTime($transaction['ED1_MODIFIED_ON']);
            $item['ED1_MODIFIED_ON']  = $modifiedOn->format('Y-m-d H:i:s');
            $item['ED1_MODIFIED_BY']  = (int)$transaction['ED1_MODIFIED_BY'];

            $createdOn = new \DateTime($transaction['ED1_CREATED_ON']);
            $item['ED1_CREATED_ON']   = $createdOn->format('Y-m-d H:i:s');
            $item['ED1_CREATED_BY']   = (int)$transaction['ED1_CREATED_BY'];

            $item['ED1_IN_OUT']       = (int)$transaction['ED1_IN_OUT'];
            $item['ED1_RESEND']       = (int)$transaction['ED1_RESEND'];
            $item['ED1_ACKNOWLEDGED'] = (int)$transaction['ED1_ACKNOWLEDGED'];
            $item['ED1_TEST_MODE']    = (int)$transaction['ED1_TEST_MODE'];
            $item['ED1_CONFIRMED']    = (int)$transaction['ED1_CONFIRMED'];

            $ed1ConfirmationDate = '0000-00-00 00:00:00';
            if(    $transaction['ED1_CONFIRMATION_DATE']
                && $transaction['ED1_CONFIRMATION_DATE'] != $ed1ConfirmationDate ){

                $confirmationDate = new \DateTime($transaction['ED1_CONFIRMATION_DATE']);
                $ed1ConfirmationDate = $confirmationDate->format('Y-m-d H:i:s');
            }

            $item['ED1_CONFIRMATION_DATE'] = $ed1ConfirmationDate;
            $item['ED1_SHIPPED'] = (int)$transaction['ED1_SHIPPED'];

            $ed1ShipmentDate = '0000-00-00 00:00:00';
            if(    $transaction['ED1_SHIPMENT_DATE']
                && $transaction['ED1_SHIPMENT_DATE'] != $ed1ShipmentDate ){
                $shipmentDate = new \DateTime($transaction['ED1_SHIPMENT_DATE']);
                $ed1ShipmentDate = $shipmentDate->format('Y-m-d H:i:s');
            }

            $item['ED1_SHIPMENT_DATE']    = $ed1ShipmentDate;

            if( !array_key_exists('ED1_SHOW_DEFAULT', $transaction)){
                $item['ED1_SHOW_DEFAULT'] = TransactionModel::SHOW_DEFAULT;
            }else{
                $item['ED1_SHOW_DEFAULT'] = $transaction['ED1_SHOW_DEFAULT'];
            }

            if( !array_key_exists('ED1_DESCRIPTION', $transaction)){
                $item['ED1_DESCRIPTION'] = '';
            }else{
                $item['ED1_DESCRIPTION'] = $transaction['ED1_DESCRIPTION'];
            }

            $item['ED1_UPLOAD_TIMESTAMP'] = $uploadTimestamp;
            $item['CO1_ID']               = $CO1_ID;
            $item['IH1_ID']               = $IH1_ID;

            $transaction['ED1_ERR_CODE']  = $errorCode;
            $transaction['ED1_ERR_DESC']  = $this->getErrorDescription($errorCode);

            if(!$errorCode){
                $values[] = $item;
            }
        }

        return $values;
    }

    private function _batchInsert( $values )
    {
        $error = null;
        $sqlCommand = Yii::$app->clickhouse->createCommand();
        try{
            $sqlCommand->batchInsert(
                TransactionModel::tableName(), array_keys($values[0]), $values
            )->execute();
        }catch(Exception $e){
            $error = $e->getMessage() . '; sql = ' . $sqlCommand->getRawSql();
        }

        return $error;
    }

    private function _importTestData( $transactions, $CO1_ID, $IH1_ID )
    {
        $errors = [];
        $transactionsAdded = 0;

        for($j=0; $j<100; $j++){

            $values = [];
            for($i = 0; $i < 10; $i++){
                $values = array_merge( $values, $this->_prepareTransactions($transactions, $CO1_ID, $IH1_ID,true));
            }

            if(!$errors){
                $error = $this->_batchInsert($values);
                if($error){
                    $errors[] = $error;
                }else{
                    $transactionsAdded += count($values);
                }
            }
        }

        return [
            'transactionsAdded' => $transactionsAdded,
            'errors'            => $errors
        ];
    }

    private function _importRealData( &$transactions, $CO1_ID, $IH1_ID )
    {
        $errors = [];
        $transactionsAdded  = 0;
        $transactionsErrors = 0;

        $values = $this->_prepareTransactions($transactions, $CO1_ID, $IH1_ID);

        if($values){
            $error = $this->_batchInsert($values);
            if($error){
                $errors[] = $error;
            }
        }

        foreach( $transactions as $value ){
            if($value['ED1_ERR_CODE']){
                $transactionsErrors ++;
                $errors[] = [
                    'ED1_ID'       => $value['ED1_ID'],
                    'ED1_ERR_CODE' => $value['ED1_ERR_CODE'],
                    'ED1_ERR_DESC' => $this->getErrorDescription( $value['ED1_ERR_CODE'] )
                ];
            }else{
                $transactionsAdded ++;
            }
        }

        return [
            'transactionsAdded'  => $transactionsAdded,
            'transactionsErrors' => $transactionsErrors,
            'errors'             => $errors
        ];
    }

    public function getSerie( $params )
    {
        $transaction = new vTransaction();
        $transaction->setScenario(vTransaction::SCENARIO_REPORT);

        $aggrigatedBy = !empty($params['aggrigatedBy']) ? $params['aggrigatedBy'] : 0;
        $groupBy      = !empty($params['groupBy'])      ? $params['groupBy']      : 0;

        $items = $transaction->getDrilldown( $params, $aggrigatedBy, $groupBy);

        $series = [];
        if(empty($groupBy)){
            $serie = [
                'name' => 'Totals',
                'data' => []
            ];
            foreach( $items as $item ){
                $timestamp = (new \DateTime($item['time']))->getTimestamp() * 1000;
                $serie['data'][] = [ $timestamp, intval($item['total']) ];
            }
            $series[] = $serie;
        }else{
            foreach( $items as $item ){

                if(!isset($series[$item[$groupBy]])){
                    $series[$item[$groupBy]] = [
                        $groupBy => $item[$groupBy],
                        'data'   => []
                    ];
                }

                if($aggrigatedBy){
                    $timestamp = (new \DateTime($item['time']))->getTimestamp() * 1000;
                    $data = [ $timestamp, intval($item['total']) ];
                }else{
                    $data = [ intval($item['total']) ];
                }

                $series[$item[$groupBy]]['data'][] = $data;
            }
        }

        return array_values($series);
    }

    public function getDetailsReport( $params )
    {
        $transaction = new vTransaction();
        $transaction->setScenario(vTransaction::SCENARIO_REPORT);

        $groupBy = '';
        if(!empty($params['CO1_ID'])){
            $groupBy = 'CO1_ID';
        }elseif(!empty($params['ED1_TYPE'])){
            $groupBy = 'ED1_TYPE';
        }

        $aggrigatedBy = !empty($params['aggrigatedBy']) ? $params['aggrigatedBy'] : 1;
        $items = $transaction->getDrilldown( $params, $aggrigatedBy, $groupBy);

        $series = [];
        if(empty($groupBy)){
            $serie = [
                'name' => 'Totals',
                'data' => []
            ];
            foreach( $items as $item ){
                $timestamp = (new \DateTime($item['time']))->getTimestamp() * 1000;
                $serie['data'][] = [ $timestamp, intval($item['total']) ];
            }
            $series[] = $serie;
        }else{
            foreach( $items as $item ){
                $timestamp = (new \DateTime($item['time']))->getTimestamp() * 1000;

                if(!isset($series[$item[$groupBy]])){
                    $series[$item[$groupBy]] = [
                        $groupBy => $item[$groupBy],
                        'data'   => []
                    ];
                }
                $series[$item[$groupBy]]['data'][] = [ $timestamp, intval($item['total']) ];
            }
        }

        return array_values($series);
    }

    public function getInOutBoundReport( $params )
    {
        $transaction = new vTransaction();
        $transaction->setScenario(vTransaction::SCENARIO_REPORT);

        $items = $transaction->getInOutBoundReport( $params );

        $totals = [];
        foreach( $items as $item ){
            if(! isset($totals[$item['ED1_IN_OUT']]) ){
                $totals[$item['ED1_IN_OUT']] = [
                    'name'      => $item['ED1_IN_OUT'] ? 'Outbound' : 'Inbound',
                    'totals'    => 0,
                    'drilldown' => []
                ];
            }

            $totals[$item['ED1_IN_OUT']]['totals'] += $item['total'];
            $totals[$item['ED1_IN_OUT']]['drilldown'][] = [
                'name'       => $item['ED1_TYPE'],
                'ED1_IN_OUT' => $item['ED1_IN_OUT'],
                'total'      => $item['total']
            ];
        }

        return $totals;
    }

    public function getTradingPartnersReport( $params )
    {
        $transaction = new vTransaction();
        $transaction->setScenario(vTransaction::SCENARIO_REPORT);

        $items = $transaction->getTradingPartnersReport( $params );

        $totals = [];
        foreach( $items as $item ){
            if(! isset($totals[$item['CO1_ID']]) ){
                $totals[$item['CO1_ID']] = [
                    'totals'    => 0,
                    'CO1_ID'    => $item['CO1_ID'],
                    'drilldown' => []
                ];
            }

            $totals[$item['CO1_ID']]['totals'] += $item['total'];
            $totals[$item['CO1_ID']]['drilldown'][] = [
                'VD1_ID' => $item['VD1_ID'],
                'CU1_ID' => $item['CU1_ID'],
                'total'  => $item['total']
            ];
        }

        return $totals;
    }

    public function resend( $items )
    {

        $data = [];
        foreach( $items as &$item ){
            $item['ED1_TYPE']   = str_replace("\0", '', $item['ED1_TYPE']);
            $item['ED1_STATUS'] = vTransaction::STATUS_RESENDING;

            if(!isset( $data[$item['CO1_ID']] )){
                $data[$item['CO1_ID']] = [];
            }
            $data[$item['CO1_ID']][] = $item;
        }

        $results = [];
        foreach( $data as $CO1_ID => $transactions ){
            $result = $this->_importRealData( $transactions, $CO1_ID );
            $results[] = array_merge($result, ['transactions' => $transactions]);
        }

        return $results;
    }

    public function getResending( $CO1_ID )
    {
        $transaction = new vTransaction();
        $items = $transaction->getResending( $CO1_ID );

        $maxUplTimeItems = $transaction->getResending( $CO1_ID, true );

        $results = [];
        foreach( $items as $item){
            $overlap = false;
            foreach( $maxUplTimeItems as $maxUplTimeItem){
                if(  $maxUplTimeItem['ED1_ID'] == $item['ED1_ID'] ){
                    if( $maxUplTimeItem['MAX_ED1_UPLOAD_TIMESTAMP'] > $item['ED1_UPLOAD_TIMESTAMP'] ){
                        $overlap = true;
                    }
                    break;
                }
            }

            if(!$overlap){
                $results[] = [
                    'ED1_ID' => $item['ED1_ID']
                ];
            }
        }

        return $results;
    }
}