<?php

namespace app\services;

use Yii;
use yii\base\Component;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii2tech\spreadsheet\Spreadsheet;

class XLSService extends Component
{
    public function generateXls($params, $query, $filename, $items = []) {
        $attributes = explode(',', $params['columns'] );

        $columns = [];
	    foreach ($attributes as $column) {
		    $columns[] = ['attribute'=> $column, 'headerOptions' => ['font' => ['bold'  => true]]];
	    }

	    if ($query && $query != '') {
	    	$dataProvider = new ActiveDataProvider([
			    'query' => $query
		    ]);
	    } else {
		    $dataProvider = new ArrayDataProvider([
			    'allModels' => $items
		    ]);
	    }

	    $spreadsheet = new Spreadsheet([
		    'dataProvider' => $dataProvider,
		    'columns' => $columns,
		    'writerType' => 'Xls'
	    ]);

	    return $spreadsheet->send($filename);
    }

	public function generateXlsWithLines($params, $headerName, $header, $headerColumns, $lines, $filename) {
		$spreadsheet = new Spreadsheet([
				'dataProvider' => new ArrayDataProvider([
					'allModels' => []
				]),
			    'writerType' => 'Xls'
			]);

		$spreadsheet->getDocument()->setActiveSheetIndex(0);
		$objWorksheet = $spreadsheet->getDocument()->getActiveSheet();

		$objWorksheet->setCellValueByColumnAndRow(1, 1, $headerName . ':');
		$objWorksheet->getStyleByColumnAndRow(1, 1)->getFont()->getColor()->setRGB('000000');
		$objWorksheet->getStyleByColumnAndRow(1, 1)->getFont()->setBold(true);

		$colIndex = 1;
		foreach ($headerColumns as $column) {
			$objWorksheet->setCellValueByColumnAndRow($colIndex, 2, $column['header']);
			$objWorksheet->getStyleByColumnAndRow($colIndex, 2)->getFont()->getColor()->setRGB('000000');
			$objWorksheet->getStyleByColumnAndRow($colIndex, 2)->getFont()->setBold(true);
			$objWorksheet->setCellValueByColumnAndRow($colIndex, 3, $header[$column['field']]);

			$colIndex ++;
		}

		$objWorksheet->setCellValueByColumnAndRow(1, 5, 'Lines:');
		$objWorksheet->getStyleByColumnAndRow(1, 5)->getFont()->getColor()->setRGB('000000');
		$objWorksheet->getStyleByColumnAndRow(1, 5)->getFont()->setBold(true);

		$colIndex = 1;
		foreach (json_decode($params['columnsLines'], true) as $columnLine) {
			$objWorksheet->setCellValueByColumnAndRow($colIndex, 6, $columnLine['header']);
			$objWorksheet->getStyleByColumnAndRow($colIndex, 6)->getFont()->getColor()->setRGB('000000');
			$objWorksheet->getStyleByColumnAndRow($colIndex, 6)->getFont()->setBold(true);

			$rowIndex = 7;
			foreach ($lines as $line) {
				$objWorksheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $line[$columnLine['field']]);
				$rowIndex ++;
			}

			$colIndex ++;
		}

		return $spreadsheet->send($filename);
	}

}