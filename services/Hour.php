<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\Hour as HourModel;
use yii\db\Expression;
use yii\web\ServerErrorHttpException;

class Hour extends Component
{
    public function createHours($params)
    {
        $errors = [];
	    $user = Yii::$app->user->getIdentity();

	    $transaction = Yii::$app->db->beginTransaction();
	    try {
		    foreach ($params as $data) {
			    if (isset($data['HO1_ID']) && (int)$data['HO1_ID'] > 0) {
				    $hourModel = HourModel::findOne((int)$data['HO1_ID']);
				    $hourModel->HO1_ID = (int)$data['HO1_ID'];
				    $hourModel->HO1_HOURS = (int)$data['HO1_HOURS'];
				    unset($data['HO1_CREATED_ON']);
				    unset($data['HO1_CREATED_BY']);
				    unset($data['HO1_DATE']);
			    } else {
				    unset($data['HO1_ID']);
				    $hourModel = new HourModel();
				    $data['HO1_DATE'] = date('Y-m-d H:i:s', (strtotime($data['HO1_DATE'])));
				    $data['HO1_CREATED_BY'] = $user->US1_ID;
				    $data['HO1_CREATED_ON'] = new Expression('UTC_TIMESTAMP()');
			    }

			    $data['HO1_MODIFIED_BY'] = $user->US1_ID;
			    $data['HO1_MODIFIED_ON'] = new Expression('UTC_TIMESTAMP()');

			    if ( !($hourModel->load($data, '') && $hourModel->save( true )) ){
				    $errors = array_merge($errors, $hourModel->getErrorSummary(true));
			    }
	        }
		    $transaction->commit();
	    } catch (Exception $e) {
		    $errors[] = $e->getMessage();
		    $transaction->rollBack();
	    }

	    return [
		    'data' => [
			    'success'=> !$errors,
			    'errors' => $errors
		    ]
	    ];
    }

    public function saveMultiple( $items )
    {
        $errors = [];
        foreach( $items as $item){
            $HO1_DATE = $item['HO1_DATE'];
            $hourModel =  \app\models\Hour::find()
                ->where(['TE1_ID'   => $item['TE1_ID']])
                ->andFilterWhere(['like', 'DATE_FORMAT(HO1_DATE, "%Y-%m")', $HO1_DATE])
                ->one();

            if(!$hourModel){
                $technician = \app\models\Technician::findOne(['TE1_ID' => $item['TE1_ID']]);
                $hourModel = new \app\models\Hour();
                $hourModel->CO1_ID   = $technician->CO1_ID;
                $hourModel->LO1_ID   = $technician->LO1_ID;
                $hourModel->TE1_ID   = $technician->TE1_ID;
                $hourModel->HO1_DATE = $HO1_DATE.'-01';
            }
            $hourModel->HO1_HOURS = $item['value'];
            if(!$hourModel->save(true)){
                $errors = $hourModel->getErrorSummary(true);
            }
        }

        return [
            'success' => !$errors,
            'errors'  => $errors
        ];
    }

    public function delete($id)
    {
        $model = HourModel::find()->where(["HO1_ID" => $id])->one();
        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }
    }
}
