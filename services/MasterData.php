<?php

namespace app\services;

use app\models\Conversion;
use app\models\File;
use app\models\MasterDataLocation;
use app\models\Location as ShopModel;
use Yii;
use yii\base\Component;
use yii\db\Exception;
use app\models\MasterData as MasterDataModel;
use app\models\MasterCatalog as MasterCatalogModel;
use yii\helpers\ArrayHelper;

class MasterData extends Component
{
    public function updateParts($records)
    {
        $errors = [];

        foreach($records as $data)
        {
            $model = MasterDataModel::findOne((int)$data['MD1_ID']);

            if (!$model) {
                $model = new MasterDataModel();
                $model->MD1_ID = $data['MD1_ID'];
            }

            if (isset($data['MD0_ID']) && $data['MD0_ID'])
                $model->MD0_ID = $data['MD0_ID'];

            if ( !$model->save(false) ){
                $errors = $model->getErrorSummary(true);
            }
        }

        return $errors;
    }

	public function save($data, $MD1_ID = null)
    {
        $errors = [];

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $masterDataModel = MasterDataModel::findOne(['MD1_ID' => $MD1_ID]);
            if (!$masterDataModel) {
                $masterDataModel = new MasterDataModel();
            }
            $masterDataModel->setScenario(MasterDataModel::SCENARIO_STANDARD_EDIT );
            $FI1_ID = $masterDataModel->FI1_ID;

            $data['MD1_CENTRAL_ORDER_FLAG'] = (int)$data['MD1_CENTRAL_ORDER_FLAG'] ? 1 : 0;
            if (isset($data['GL1_ID'])) {
                $data['GL1_ID'] = (int)$data['GL1_ID'];
            }

            $user = Yii::$app->user->getIdentity();
            if (!isset($data['CO1_ID']) || $data['CO1_ID'] == 0) {
	            $data['CO1_ID'] = $user->CO1_ID;
            }
            if (!isset($data['LO1_ID']) || $data['LO1_ID'] == 0) {
	            $data['LO1_ID'] = $user->LO1_ID;
            }

            $masterDataModel->load($data, '');

            $params = [];
	        $params['MD1_PART_NUMBER'] = $data['MD1_PART_NUMBER'];

	        if (isset($data['MD1_ID']) && $data['MD1_ID'] > 0)
	            $params['MD1_ID'] = $data['MD1_ID'];

            $duplicates = $masterDataModel->checkDuplicate($params);

            if (intval($duplicates['NUM']) > 0) {
	            throw new Exception('', [ 'MD1_PART_NUMBER' => ['Duplicate entry for part# ' . $params['MD1_PART_NUMBER']] ], MasterDataModel::$MODEL_ERROR);
            }

            if (!$masterDataModel->save(true)) {
                throw new Exception('', $masterDataModel->getErrors(), MasterDataModel::$MODEL_ERROR);
            } else {
                $dataLocationFilters = [
                    'MD1_ID' => $masterDataModel->MD1_ID,
                    'MD2_DELETE_FLAG' => 0
                ];
                if (!$user->isAtLeastCompanyManager() && $user->LO1_ID) {
                    $dataLocationFilters['LO1_ID'] = $user->LO1_ID;
                }

                // save/delete image
                if ($FI1_ID && $FI1_ID > 0 && $MD1_ID > 0) {
	                $file = File::findOne(['FI1_ID' => $FI1_ID]);
	                if ($file) {
	                	if ($FI1_ID != $masterDataModel->FI1_ID) {
			                $file->delete();
		                } else {
			                $file->MD1_ID = $masterDataModel->MD1_ID;
			                if (!$file->save(true)) {
				                throw new Exception('', $file->getErrors());
			                }
		                }
	                }
                } else if ($masterDataModel->FI1_ID && $masterDataModel->FI1_ID > 0) {
	                $file = File::findOne(['FI1_ID' => $masterDataModel->FI1_ID]);
	                if ($file) {
		                $file->MD1_ID = $masterDataModel->MD1_ID;
		                if (!$file->save(true)) {
			                throw new Exception('', $file->getErrors());
		                }
	                }
                }

	            $MD1_ID = $masterDataModel->MD1_ID;

                $masterDataLocations = MasterDataLocation::findAll($dataLocationFilters);
                foreach($data['locationData'] as $locationData) {

                    $dataLocation = null;
                    foreach( $masterDataLocations as $masterDataLocation ) {
                        if ($masterDataLocation->LO1_ID == $locationData['LO1_ID'] ) {
                            $dataLocation = $masterDataLocation;
                            break;
                        }
                    }

                    if (!$dataLocation) {
                        $dataLocation = new MasterDataLocation();
                        $dataLocation->CO1_ID = $user->CO1_ID;
                        $dataLocation->LO1_ID = $user->LO1_ID;
                        $dataLocation->MD1_ID = $masterDataModel->MD1_ID;
                    }

                    $locationData['MD2_ACTIVE']  = $locationData['MD2_ACTIVE'] ? '1' : '0';
                    $locationData['MD2_PARTIAL'] = $locationData['MD2_PARTIAL'] ? '1' : '0';
                    $locationData['MD1_ID']      = $masterDataModel->MD1_ID;

                    $MD2_NO_INVENTORY = 0;
                    /*
                    if($masterDataModel->CA1_ID){
                        $categoryModel = \app\models\Category::findOne(['CA1_ID' => $masterDataModel->CA1_ID]);
                        $MD2_NO_INVENTORY = $categoryModel->CA1_NO_INVENTORY;
                    }
                    */
                    if ($dataLocation->VD1_ID) {
                        $vendorModel = \app\models\Vendor::findOne(['VD1_ID' => $dataLocation->VD1_ID]);
                        if ($vendorModel) {
                            $MD2_NO_INVENTORY = $vendorModel->VD1_NO_INVENTORY;
                        }
                    }
                    $locationData['MD2_NO_INVENTORY'] = $MD2_NO_INVENTORY;
                    $dataLocation->load($locationData, '');

                    if (!$dataLocation->save(true)) {
                        throw new Exception('', $dataLocation->getErrors(), MasterDataModel::$MODEL_ERROR);
                    }
                }

                $conversionFilters = ['MD1_ID' => $masterDataModel->MD1_ID];
                $conversions = Conversion::findAll($conversionFilters);

                foreach($conversions as $conversion) {
                    $conversion->UM2_DELETE_FLAG = 1;
                    $conversion->save();
                }

                foreach($data['locationData'] as $locationData) {
                    if ($locationData['UM1_RECEIPT_ID'] && $locationData['UM1_PURCHASE_ID'] && $locationData['UM1_RECEIPT_ID'] != $locationData['UM1_PURCHASE_ID']) {
                        $conversion = Conversion::findOne([
                            'MD1_ID' => $masterDataModel->MD1_ID,
                            'UM1_FROM_ID' => $locationData['UM1_PURCHASE_ID'],
                            'UM1_TO_ID' => $locationData['UM1_RECEIPT_ID']
                        ]);
                        if (!$conversion) {
                            $conversion = new Conversion();
                            $conversion->CO1_ID = $user->CO1_ID;
                            $conversion->MD1_ID = $masterDataModel->MD1_ID;
                            $conversion->UM1_FROM_ID = $locationData['UM1_PURCHASE_ID'];
                            $conversion->UM1_TO_ID = $locationData['UM1_RECEIPT_ID'];
                        }
                        $conversion->UM2_FACTOR = $locationData['UM2_FACTOR'];
                        $conversion->UM2_DELETE_FLAG = 0;
                    } elseif ($data['UM1_PRICING_ID'] && $locationData['UM1_PURCHASE_ID'] && $data['UM1_PRICING_ID'] != $locationData['UM1_PURCHASE_ID']) {
                        $conversion = Conversion::findOne([
                            'MD1_ID' => $masterDataModel->MD1_ID,
                            'UM1_FROM_ID' => $data['UM1_PRICING_ID'],
                            'UM1_TO_ID' => $locationData['UM1_PURCHASE_ID']
                        ]);
                        if (!$conversion) {
                            $conversion = new Conversion();
                            $conversion->CO1_ID = $user->CO1_ID;
                            $conversion->MD1_ID = $masterDataModel->MD1_ID;
                            $conversion->UM1_FROM_ID = $data['UM1_PRICING_ID'];
                            $conversion->UM1_TO_ID = $locationData['UM1_PURCHASE_ID'];
                        }
                        $conversion->UM2_FACTOR = $locationData['UM2_PRICE_FACTOR'];
                        $conversion->UM2_DELETE_FLAG = 0;
                    } elseif ($data['UM1_PRICING_ID'] && $locationData['UM1_PURCHASE_ID'] && $locationData['UM1_RECEIPT_ID'] && $data['UM1_PRICING_ID'] != $locationData['UM1_RECEIPT_ID']) {
                        $conversion = Conversion::findOne([
                            'MD1_ID' => $masterDataModel->MD1_ID,
                            'UM1_FROM_ID' => $data['UM1_PRICING_ID'],
                            'UM1_TO_ID' => $locationData['UM1_RECEIPT_ID']
                        ]);
                        if (!$conversion) {
                            $conversion = new Conversion();
                            $conversion->CO1_ID = $user->CO1_ID;
                            $conversion->MD1_ID = $masterDataModel->MD1_ID;
                            $conversion->UM1_FROM_ID = $data['UM1_PRICING_ID'];
                            $conversion->UM1_TO_ID = $locationData['UM1_RECEIPT_ID'];
                        }
                        $conversion->UM2_FACTOR = ($locationData['UM2_FACTOR'] * $locationData['UM2_PRICE_FACTOR']);
                        $conversion->UM2_DELETE_FLAG = 0;
                    }
                    if (isset($conversion) && !$conversion->save(true)) {
                        throw new Exception('', $conversion->getErrors(), MasterDataModel::$MODEL_ERROR);
                    }
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            if ($e->getCode() == MasterDataModel::$MODEL_ERROR) {
                $errors = $e->errorInfo;
            } else {
            	$errors[] = $e->getMessage();
            }
            $transaction->rollBack();
        }

        return [
            'success' => !$errors,
            'errors'  => $errors,
            'MD1_ID'  => $MD1_ID,
	        'data' => !$errors ? $masterDataModel : null
        ];
    }

	public function generateXls($params) {
		$filePrefix = 'MasterData_';
		$model = new MasterDataModel();
		$models = [];

		switch ($params['subtype']) {
			case 'inventory-optimization':
				$filePrefix = 'InventoryOptimizationReport_';
				$query  = $model->getInventoryOptimizationQuery( $params );
		        $models = $query->asArray()->all();
				$query = '';
				break;
			case 'tech-usage-technician-details':
				$filePrefix = 'TechUsageTechnicianDetails-' . $params['TE1_NAME'] . '_';
				$models = $this->getTechUsageByMonths($params, true);
				$query = '';
				break;
			case 'voc_usage':
				$filePrefix = 'VOC_Usage_';
				$models = $this->getTechUsageByMonths($params, true);
				$query = '';
				break;
			default:
				$query = $model->getSearchQuery($params);
		}

		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename, $models);
	}

	public function getTechUsage( $params )
	{
		if (isset($params['MTD'])) {
			$now = new \DateTime('now');
			$params['endDate'] = date('Y-m-t', strtotime($now->format('Y-m-d')));
			$params['startDate'] =  date('Y-m-01', strtotime($now->format('Y-m-d')));
		}

		$masterData = new \app\models\MasterData();
		$models = $masterData->getTechUsage( $params );
		$TE1_IDs = ArrayHelper::getColumn($models, 'TE1_ID');

		$hour = new \app\models\Hour();
		$hourProvider = $hour->getAllHours(['TE1_IDs' => $TE1_IDs, 'startDate' => $params['startDate'], 'endDate' => $params['endDate']]);
		$hourProvider->query->asArray();
		$hourModels = $hourProvider->getModels();

		$shops = [];
		if (isset($params['LO1_IDs'])) {
			$shops = explode(',', $params['LO1_IDs']);
		} else if (isset($params['MTD'])) {
			$user = Yii::$app->user->getIdentity();
			$shops = ShopModel::findAll(['CO1_ID' => $user->CO1_ID, 'LO1_DELETE_FLAG' => 0]);
		}

		$result = array();
		if (!empty($models)) {

		    $items = [];
		    foreach( $models as $model ){
		        if(!isset($items[$model['TE1_ID']])){
		            $items[$model['TE1_ID']] = [
		                'TE1_ID' => $model['TE1_ID'],
                        'TE1_NAME' => $model['TE1_NAME'],
                        'LO1_ID' => $model['LO1_ID'],
                        'LO1_NAME' => $model['LO1_NAME'],
                        'TOTAL_QTY' => 0,
                        'TOTAL_PRICE' => 0,
                        'TOTAL_HOURS' => 0
                    ];
                }
		        $items[$model['TE1_ID']]['TOTAL_QTY']   += $model['TOTAL_QTY'];
		        $items[$model['TE1_ID']]['TOTAL_PRICE'] += $model['TOTAL_PRICE'];
            }

			foreach ($items as $data) {

				// Setup Group Series
                if(!isset($result['data'][$data['LO1_ID']])){
                    $result['data'][$data['LO1_ID']] = [
                        'name'      => $data['LO1_NAME'],
                        'drilldown' => $data['LO1_ID'],
                        'y'         => 0.0,
                        'perhour'   => 0.0
                    ];
                }

				foreach ($hourModels as $hourModel) {
					if (intval($hourModel['TE1_ID']) == intval($data['TE1_ID'])) {
						$data['TOTAL_HOURS'] += (double)($hourModel['HO1_HOURS']);
					}
				}

				$result['data'][$data['LO1_ID']]['y']       += (double)$data['TOTAL_PRICE'];
				$result['data'][$data['LO1_ID']]['perhour'] += (double)$data['TOTAL_HOURS'];

				if(count($shops) > 1) {
					if(!isset($result['drilldown']['data'][$data['LO1_ID']])) {
						$result['drilldown']['data'][$data['LO1_ID']] = [
						    'name' => $data['LO1_NAME'],
                            'id' => $data['LO1_ID'],
                            'data' => []
                        ];
					}
				}

				$duplicateTechicianNames = false;
				foreach( $models as $model1 ){
				    if ($model1['TE1_NAME'] == $data['TE1_NAME'] && $model1['TE1_ID'] != $data['TE1_ID']) {
						$duplicateTechicianNames = true;
						break;
					}
                }

				$data['name'] = $duplicateTechicianNames ? $data['TE1_NAME'] . '#' . $data['TE1_ID'] : $data['TE1_NAME'];
				$data['y'] = (double)$data['TOTAL_PRICE'];
				$data['perhour'] = (double)$data['TOTAL_HOURS'];
				$data['perhour'] =  $data['perhour'] > 0 ? ($data['y'] / $data['perhour']) : 0.0;

				unset($data['TECH_NAME']);
				unset($data['LO1_NAME']);
				unset($data['TOTAL_PRICE']);

				if(count($shops) > 1) {
					$result['drilldown']['data'][$data['LO1_ID']]['data'][] = $data;
				} else {
					$result['drilldown']['data'][] = $data;
				}
			}

			foreach( $result['data'] as $LO1_ID => &$item ){
			    $item['perhour'] = $item['perhour'] > 0 ? ($item['y'] / $item['perhour']) : 0.0;
            }

			if(count($shops) > 1) {
				$result['data'] = array_values($result['data']);
				$result['drilldown']['data'] = array_values($result['drilldown']['data']);
			} else {
				$result['data'] = isset($result['drilldown']['data']) ? array_values($result['drilldown']['data']) : array();
			}

			$result = array(
				'name' => 'Tech Usage Data',
				'drilldown' => $result['drilldown']['data'],
				'data' => $result['data']
			);
		}
		return $result;
	}

	public function getTechUsageByMonths( $params, $export = false )
	{
		if (isset($params['MTD'])) {
			$now = new \DateTime('now');
			$params['endDate'] = date('Y-m-t', strtotime($now->format('Y-m-d')));
			$params['startDate'] =  date('Y-m-01', strtotime($now->format('Y-m-d')));
		}

		$masterDataModel = new MasterDataModel();
		$models = $masterDataModel->getTechUsage( $params );

		$CA1_IDs = ArrayHelper::getColumn($models, 'CA1_ID');
		$categories = \app\models\Category::find()->where(['CA1_ID' => $CA1_IDs, 'CA1_DELETE_FLAG' => 0])->orderBy(['CA1_NAME' => SORT_ASC])->all();

		$hasNoCategoryModels = array_filter($models, function($value) {
			return !$value['CA1_ID'] || $value['CA1_ID'] == 0;
		});

		if (sizeof($hasNoCategoryModels) > 0) {
			$categories[] = [ 'CA1_ID' => 0, 'CA1_NAME' => 'No Category'];
		}

		$nodesArray = [];
		foreach($categories as $category) {
			$categoryData = [ 'CA1_ID' => $category['CA1_ID'], 'CA1_NAME' => $category['CA1_NAME'], 'TOTAL_PRICE' => 0,  'TOTAL_QTY' => 0, 'CREATED_ON' => ''];
			$categoryTreeItem = [ 'data' => $categoryData, 'expanded' => true, 'children' => []];

			$childrenModels = array_filter($models, function($value) use (&$category) {
				if ($category['CA1_ID'] == 0) {
					return !$value['CA1_ID'] || $value['CA1_ID'] == 0;
				}
				if ($value['CA1_ID'] == $category['CA1_ID']) {
					return true;
				} else {
					return false;
				}
			});
			ArrayHelper::multisort($childrenModels, ['OR_CREATED_ON'], [SORT_DESC]);
			$CREATED_ON_DATEs = ArrayHelper::map($models, 'CREATED_ON', 'CREATED_ON');
			$categoryChildrensNodesArray = [];
			$categoryChildrensArray = [];

			foreach($CREATED_ON_DATEs as $key => $CREATED_ON_DATE) {
				$dateModels = array_filter($childrenModels, function($value) use (&$CREATED_ON_DATE, &$category) {
					if ($value['CREATED_ON'] == $CREATED_ON_DATE && $value['CA1_ID'] == $category['CA1_ID']) {
						return true;
					} else {
						return false;
					}
				});
				ArrayHelper::multisort($dateModels, ['OR_CREATED_ON'], [SORT_DESC]);
				$dateData = [ 'CA1_ID' => $category['CA1_ID'], 'CA1_NAME' => $CREATED_ON_DATE, 'TOTAL_PRICE' => 0,  'TOTAL_QTY' => 0, 'CREATED_ON' => $CREATED_ON_DATE, 'OR_CREATED_ON' => '', 'CREATED_BY' => ''];
				$dateTreeItem = [ 'data' => $dateData, 'expanded' => false, 'children' => []];

				foreach($dateModels as &$dateModel) {
					if (floatval($dateModel['TOTAL_PRICE']) > 0) {
						$dateModel['CA1_NAME'] = '';
						$dateChildren = ['data' => $dateModel];
						$dateTreeItem['children'][] = $dateChildren;
						$dateData['TOTAL_PRICE'] += floatval($dateModel['TOTAL_PRICE']);
						$dateData['TOTAL_QTY'] += $dateModel['TOTAL_QTY'];
					}
				}

				$dateTreeItem['data'] = $dateData;
				if ($dateData['TOTAL_PRICE'] > 0) {
					if ($export) {
						$categoryChildrensArray[] = $dateData;
						$categoryChildrensArray = array_merge($categoryChildrensArray, $dateModels);
					}
					$categoryChildrensNodesArray[] = $dateTreeItem;
				}
			}

			foreach($categoryChildrensNodesArray as $categoryChildren) {
				$categoryTreeItem['children'][] = $categoryChildren;
				$categoryData['TOTAL_PRICE'] += $categoryChildren['data']['TOTAL_PRICE'];
				$categoryData['TOTAL_QTY'] += $categoryChildren['data']['TOTAL_QTY'];
			}


			if ($categoryData['TOTAL_PRICE'] > 0) {
				if ($export) {
					$nodesArray[] = $categoryData;
					$nodesArray = array_merge($nodesArray, $categoryChildrensArray);
				} else {
					$categoryTreeItem['data'] = $categoryData;
					$nodesArray[] = $categoryTreeItem;
				}
			}
		}

		return $nodesArray;
	}

	const PAGE_DPI = 96;
	const PAGE_WIDTH = 17;
	const PAGE_HEIGHT = 11;
	const LINE_HEIGHT = 21;
	const BARCODE_HEIGHT = 35;
	const PADDING_LEFT = 10;
	const PADDING_TOP = 10;
	const PADDING_BOTTOM = 20;
	const TEXT_PADDING_LEFT = 10;
	const TEXT_PADDING_TOP = 5;
	const IMAGE_PADDING_LEFT = 30;
	const IMAGE_WIDTH = 250;

	public function printRackHTML($params) {
		try {
			$masterData = new MasterDataModel();
			$items = $masterData->getRack( $params );
		} catch (\Exception $exception) {
			return [
				'error' => $exception
			];
		}

		$pageH = self::PAGE_HEIGHT * self::PAGE_DPI - self::PADDING_TOP - self::PADDING_BOTTOM;
		$pageW = self::PAGE_WIDTH * self::PAGE_DPI - self::PADDING_LEFT*2;
		$params['pageW'] = $pageW;
		$params['pageH'] = $pageH;
		$params['pageHeight'] = $pageH - self::LINE_HEIGHT * 4;
		$params['itemHeight'] = ($params['pageHeight']) / 4;
		$params['imageWidth'] = $pageW/6 - self::IMAGE_PADDING_LEFT*2;
		$params['imageHeight'] = $params['itemHeight'] - 5 * self::LINE_HEIGHT - self::BARCODE_HEIGHT;

		$selectedItems = [];

		$user = Yii::$app->user->getIdentity();
		$fromLocation = \app\models\Location::findOne(['LO1_ID' => $user->LO1_ID]);

		if ($fromLocation && isset($fromLocation['LO1_RACKS'])) {
			for ($i = 0; $i < intval($fromLocation['LO1_RACKS']) * 4 * 24; $i++) {
				$selectedItems[] = [];
			}
		}

		$basePath = isset(Yii::$app->params['partsImagesPath']) ? Yii::$app->params['partsImagesPath'] . DIRECTORY_SEPARATOR : '';
		$params['basePath'] = str_replace('web/', '', $basePath);

		foreach ($items as $record) {
			$idx = ((intval($record['MD2_RACK'])-1)*4*24) + ((intval($record['MD2_DRAWER'])-1)*24) + ((intval($record['MD2_BIN'])-1));
			if (isset($idx) && $idx >= 0) {
				$counter = 1;
				if (isset($selectedItems[$idx]) && $selectedItems[$idx] && isset($selectedItems[$idx]['counter']))
					$counter = $selectedItems[$idx]['counter'] + 1;
				$selectedItems[$idx] = [
					'MD1_ID' => $record['MD1_ID'],
					'MD2_ID' => $record['MD2_ID'],
					'VD1_ID' => $record['VD1_ID'],
					'CA1_ID' => $record['CA1_ID'],
					'VD1_NAME' => $record['VD1_NAME'],
					'CA1_NAME' => $record['CA1_NAME'],
					'MD1_PART_NUMBER' => $record['MD1_PART_NUMBER'],
					'MD1_IMAGE' => $record['MD1_IMAGE'],
					'FI1_FILE_PATH' => $record['FI1_FILE_PATH'],
					'MD1_VENDOR_PART_NUMBER' => $record['MD1_VENDOR_PART_NUMBER'],
					'MD1_UPC1' => $record['MD1_UPC1'],
					'MD1_DESC1' => $record['MD1_DESC1'],
					'UM1_NAME' => $record['UM1_NAME'],
					'MD2_MIN_QTY' => $record['MD2_MIN_QTY'],
					'MD2_RACK' => $record['MD2_RACK'],
					'MD2_DRAWER' => $record['MD2_DRAWER'],
					'MD2_BIN' => $record['MD2_BIN'],
					'counter' => $counter];
			}
		}

		$numPages = ceil(sizeof($selectedItems) / 24);

		$params['numPages'] = $numPages;
		$params['selectedItems'] = $selectedItems;

		$html = Yii::$app->getView()->render('/master-data/print-rack.php', ['params' => $params]);

		return [
			'html' => $html,
			'pagesCount' => $numPages,
			'pageHeight' => $pageH
		];
	}

	public function createMultiple($MD0_IDs) {
		$errors = [];

		$transaction = Yii::$app->db->beginTransaction();
		try {
			$masterCatalogModel = new MasterCatalogModel();
			$dataProvider = $masterCatalogModel->getMasterCatalog($MD0_IDs);
			$dataProvider->query->asArray();
			$masterCatalogModels = $dataProvider->getModels();

			if ($masterCatalogModels) {
				$masterDataParts = [];
				// fill masterData models from masterCatalog models
				foreach ($masterCatalogModels as $masterCatalogModel) {
					$masterDataModel = [];
					$masterDataModel['MD1_CENTRAL_ORDER_FLAG'] = 0;
					$masterDataModel['CA2_ID'] = 0;
					$masterDataModel['MD1_PART_NUMBER'] = $masterCatalogModel['MD0_PART_NUMBER'];
					$masterDataModel['MD1_VENDOR_PART_NUMBER'] = $masterCatalogModel['MD0_PART_NUMBER'];
					$masterDataModel['MD1_DESC1'] = $masterCatalogModel['MD0_DESC1'];
					$masterDataModel['MD1_UPC1'] = $masterCatalogModel['MD0_UPC1'];
					$masterDataModel['C00_ID'] = $masterCatalogModel['C00_ID'];
					$masterDataModel['CA0_NAME'] = $masterCatalogModel['CA0_NAME'];
					$masterDataModel['CA0_ID'] = $masterCatalogModel['CA0_ID'];
					$masterDataModel['CA0_NAME'] = $masterCatalogModel['CA0_NAME'];
					$masterDataModel['MD1_MARKUP'] = $masterCatalogModel['MD0_MARKUP'];
					$masterDataModel['MD1_UNIT_PRICE'] = $masterCatalogModel['MD0_UNIT_PRICE'];
					$masterDataModel['MD1_ORIGINAL_UNIT_PRICE'] = $masterCatalogModel['MD0_UNIT_PRICE'];
					$masterDataModel['MD1_SAFETY_URL'] = $masterCatalogModel['MD0_SAFETY_URL'];
					$masterDataModel['MD1_SELL_PRICE'] = $masterCatalogModel['MD0_SELL_PRICE'];
					$masterDataModel['UM1_PRICING_ID'] = $masterCatalogModel['UM1_PRICE_ID'];
					$masterDataModel['UM1_PRICE_SIZE'] = $masterCatalogModel['UM1_PRICE_SIZE'];
					$masterDataModel['MD1_VOC_FLAG'] = $masterCatalogModel['MD0_VOC_FLAG'];
					$masterDataModel['MD1_VOC_MFG'] = $masterCatalogModel['MD0_VOC_MFG'];
					$masterDataModel['MD1_VOC_CATA'] = $masterCatalogModel['MD0_VOC_CATA'];
					$masterDataModel['MD1_VOC_VALUE'] = $masterCatalogModel['MD0_VOC_VALUE'];
					$masterDataModel['MD1_VOC_UNIT'] = $masterCatalogModel['MD0_VOC_UNIT'];
					$masterDataModel['CA1_ID'] = $masterCatalogModel['CA1_ID'];
					$masterDataModel['CA1_NAME'] = $masterCatalogModel['CA1_NAME'];
					$masterDataModel['FI1_ID'] = $masterCatalogModel['FI1_ID'];
					$masterDataModel['UM1_RECEIPT_ID'] = $masterCatalogModel['UM1_DEFAULT_RECEIVE'];
					$masterDataModel['UM2_FACTOR'] = $masterCatalogModel['UM1_DEFAULT_RECEIVE_FACTOR'];
					$masterDataModel['UM1_PURCHASE_ID'] = $masterCatalogModel['UM1_DEFAULT_PURCHASE'];
					$masterDataModel['UM2_PRICE_FACTOR'] = $masterCatalogModel['UM1_DEFAULT_PURCHASE_FACTOR'];
					$masterDataModel['VD0_ID'] = $masterCatalogModel['VD0_ID'];
					$masterDataModel['MD0_ID'] = $masterCatalogModel['MD0_ID'];
					$masterDataParts[] = $masterDataModel;
				}

				//get empty masterData locations
				$masterData = MasterDataModel::findOne(['MD1_ID' => 0]);
				if (!$masterData) {
					$masterData = new MasterDataModel();
				}
				$location = new \app\models\Location();
				$user = Yii::$app->user->getIdentity();
				$masterDataLocations = $location->getMasterDataItems( $user->CO1_ID, $masterData );
				//set active current locations only
				foreach ($masterDataLocations as &$masterDataLocation) {
					if (intval($masterDataLocation['LO1_ID']) == $user->LO1_ID) {
						$masterDataLocation['MD2_ACTIVE'] = 1;
					} else {
						$masterDataLocation['MD2_ACTIVE'] = 0;
					}
		            $masterDataLocation['MD2_PARTIAL']        = intval($masterDataLocation['MD2_PARTIAL']);
		            $masterDataLocation['UM2_FACTOR']         = $masterDataLocation['UM2_FACTOR'] || 1;
		            $masterDataLocation['UM2_PRICE_FACTOR']   = $masterDataLocation['UM2_PRICE_FACTOR'] || 1;
                }

                foreach ($masterDataParts as &$masterDataPart) {
	                $locationMasterData = $masterDataLocations;
	                $filterParams = [];
	                $filterParams['MD0_PART_NUMBER'] = $masterDataPart['MD1_PART_NUMBER'];
	                $filterParams['VD0_ID'] = $masterDataPart['VD0_ID'];
	                $filterParams['MD0_ID'] = $masterDataPart['MD0_ID'];
	                //update locations from parts
	                foreach ($locationMasterData as &$locationData) {
		                $locationData['UM1_RECEIPT_ID']   = $masterDataPart['UM1_RECEIPT_ID'];
		                $locationData['UM2_FACTOR']       = $masterDataPart['UM2_FACTOR'];
		                $locationData['UM1_PURCHASE_ID']  = $masterDataPart['UM1_PURCHASE_ID'];
		                $locationData['UM2_PRICE_FACTOR'] = $masterDataPart['UM2_PRICE_FACTOR'];
	                }
	                $masterDataPart['locationData'] = $locationMasterData;
	                //check linked parts
	                $masterCatalogModel = new MasterCatalogModel();
	                $linkedParts = $masterCatalogModel->getLinkedParts($filterParams);
					$MD0_ID = 0;
					if (sizeof($linkedParts) == 0 || !$linkedParts) {
						$filterParams = [];
						$filterParams['MD0_PART_NUMBER'] = $masterDataPart['MD1_PART_NUMBER'];
						$filterParams['VD0_ID'] = $masterDataPart['VD0_ID'];
						$linkedParts = $masterCatalogModel->getLinkedParts($filterParams);
						if (sizeof($linkedParts) == 1) {
							$MD0_ID = $linkedParts[0]['MD0_ID'];
						} else if (sizeof($linkedParts) > 1) {
							return $errors[] = 'Part ' . $masterDataPart['MD1_PART_NUMBER'] . ' has multiple linked parts.';
						}
					} else if (sizeof($linkedParts) == 1)  {
						$MD0_ID = $linkedParts[0]['MD0_ID'];
					} else {
						$errors[] = 'Part ' . $masterDataPart['MD1_PART_NUMBER'] . ' has multiple linked parts.';
					}
					$masterDataPart['MD0_ID'] = $MD0_ID;

					//check duplicates
					$filterParams = [];
					$filterParams['MD1_PART_NUMBER'] = $masterDataPart['MD1_PART_NUMBER'];
	                $filterParams['VD0_ID'] = $masterDataPart['VD0_ID'];
	                $filterParams['MD0_ID'] = $masterDataPart['MD0_ID'];

					$masterDataModel = new MasterDataModel();
					$num = $masterDataModel->checkDuplicate( $filterParams );

					if ($num['NUM'] > 0) {
						$errors[] = ' The code "' . $masterDataPart['MD1_PART_NUMBER'] . '" exists already';
				    } else {
						$result = $this->save( $masterDataPart );
						if ($result['errors']) {
							foreach ($result['errors'] as $e) {
								$errors[] = $e;
							}
						}
					}
                }
			} else {
				$errors[] = 'Parts loading error occured';
			}

			if ($errors) {
				$transaction->rollBack();
			} else {
				$transaction->commit();
			}
		} catch (Exception $e) {
			if ($e->getCode() == MasterDataModel::$MODEL_ERROR) {
				$errors = $e->errorInfo;
			} else {
				$errors[] = $e->getMessage();
			}
			$transaction->rollBack();
		}

		return [
			'success' => !$errors,
			'errors'  => $errors
		];
	}

	public function getProjectedPOUsage( $params )
	{
		$sqlParams = $params;
		$sqlParams['PROJECTED_PO_USAGE'] = true;
		$now = new \DateTime('now');
		$sqlParams['startDate'] = $sqlParams['START_DATE'] =  date('Y-m-01', strtotime($now->format('Y-m-d')));
		$sqlParams['endDate'] = $sqlParams['END_DATE'] = date('Y-m-d', strtotime($now->format('Y-m-d')));

		$masterData = new \app\models\MasterData();
		$models = $masterData->getTechUsage( $sqlParams );

		$result = [];

		$currDay = date('d', strtotime($now->format('Y-m-d')));
		$prevTotal = 0;
		$xAver = 0;
		$sumX2 = 0;
		$yAver = 0;
		$sumY2 = 0;
		$xyAver = 0;

		for ($i = 1; $i <= $currDay; $i++) {
			$result[$i]['x'] = $i;
			$result[$i]['y'] = $prevTotal;

			foreach ($models as $k => $data) {
				if ($i == $data['DAY']) {
					$result[$i]['y'] += (double)$data['TOTAL_PRICE'];
				}
			}

			$prevTotal = $result[$i]['y'];

			$xAver += $i;
			$sumX2 += $i * $i;
			$yAver += $prevTotal;
			$sumY2 += $prevTotal * $prevTotal;
			$xyAver += $i * $prevTotal;
		}

		$xAver = $xAver / $currDay;
		$yAver = $yAver / $currDay;
		$xyAver = $xyAver / $currDay;

		$dX = $sumX2 / $currDay - $xAver * $xAver;
		$dY = $sumY2 / $currDay - $yAver * $yAver;
		$bX = sqrt($dX);
		$bY = sqrt($dY);

		if ($bX == 0 || $bY == 0) {
			$Rxy = 1;
		} else {
			$Rxy = ($xyAver - $xAver * $yAver) / ($bX * $bY);
		}
		
		$totalDays = date('t', strtotime($now->format('Y-m-d')));
		$yFirstDay = $Rxy * (1 - $xAver) * $bY / $bX + $yAver;
		$yLastDay = $Rxy * ($totalDays - $xAver) * $bY / $bX + $yAver;
		$correlationData = [[1, $yFirstDay] , [(int)$totalDays, $yLastDay]];

		$result = array_values($result);

		$budget = new \app\models\Budget();
		$dataProvider = $budget->search( $sqlParams );
		$dataProvider->query->asArray();
		$budgets = $dataProvider->getModels();
		$budget = 0;
		if ($budgets && sizeof($budgets) > 0) {
			$budget = floatval($budgets[0]['BU1_FINAL_RESULT']);
		}

		return $result = array(
			'data' => $result,
			'today' => $currDay,
			'totalDays' => $totalDays,
			'budget' => $budget,
			'projectedAmount' => $yLastDay,
			'correlationData' => $correlationData
		);
	}
}