<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\MeasurementUnit as MeasurementUnitModel;

class MeasurementUnit extends Component
{
    public function generateXls($params) {
        $filePrefix = 'MeasurementUnits_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new MeasurementUnitModel();
	    $query = $model->getSearchQuery($params);

	    $xlsService = new XLSService();
	    return $xlsService->generateXls($params, $query, $filename);
    }
}