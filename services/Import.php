<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 09/07/2018
 * Time: 10:13
 */

namespace app\services;

use app\models\MasterDataLocation;
use app\models\RawSalesDataStoreMonth;
use Yii;
use yii\base\Component;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class Import extends Component
{

    static $JOB_COST_SUMMARY_COLUMN_NAMES= array(
        'A'=>'Location',
        'B'=>'Sales Account Category',
        'C'=>'Sales Account Name',
        'D'=>'Sales $',
        'E'=>'Sales Hours',
        'F'=>'Sales Rate $',
        'G'=>'Actual Cost $',
        'H'=>'Actual Hours',
        'I'=>'Actual Rate $',
        'J'=>'Gross Profit $',
        'K'=>'Gross Profit %'
    );

    static $SALES_JOURNAL_REPORT_COLUMN_NAMES = array(
        'A'=>'Location',
        'B'=>'Posted Date',
        'C'=>'RO Number',
        'D'=>'Owner',
        'E'=>'Vehicle',
	    'F'=>'Estimator',
	    'G'=>'Insurance Company',
        'H'=>'Part $',
        'I'=>'Labor $',
        'J'=>'Material $',
        'K'=>'Other $',
        'L'=>'Adjustment $',
        'M'=>'Subtotal $',
        'N'=>'Tax $',
        'O'=>'Total Sales $'
    );

    static $SALES_JOURNAL_REPORT_COLUMN_NAMES_v2 = array(
        'A'=>'Location',
        'B'=>'Posted Date',
        'J'=>'Subtotal $',
        'K'=>'Total Sales $'
    );

    static $SALES_JOURNAL_REPORT_COLUMN_NAMES_v3 = array(
        'A'=>'Location',
        'B'=>'Posted Date',
        'C'=>'RO Number',
        'D'=>'Subtotal $',
        'E'=>'Total Sales $'
    );

	private $MD1_COLUMN_SET = [
	    ["column" => "CA1_NAME",                "excelColumn" => "Category",            "label" => ""],
		["column" => "GL1_NAME",                "excelColumn" => "GL Code",             "label" => ""],
		["column" => "MD1_CENTRAL_ORDER_FLAG",  "excelColumn" => "Central Order Only",  "label" => ""],
		["column" => "MD1_CREATED_ON",          "excelColumn" => "Created On",          "label" => ""],
		["column" => "MD1_CREATED_BY",          "excelColumn" => "Created By",          "label" => ""],
		["column" => "MD1_MODIFIED_ON",         "excelColumn" => "Modified On",         "label" => ""],
		["column" => "MD1_MODIFIED_BY",         "excelColumn" => "Modified By",         "label" => ""],
		["column" => "MD1_DESC1",               "excelColumn" => "Description",         "label" => ""],
		["column" => "MD1_PACK_SIZE",           "excelColumn" => "Pack Size",           "label" => ""],
		["column" => "MD1_MIN_QTY",             "excelColumn" => "Min Stock Qty",       "label" => ""],
		["column" => "MD1_ON_HAND_QTY",         "excelColumn" => "On Hand Qty",         "label" => ""],
		["column" => "MD1_AVAILABLE_QTY",       "excelColumn" => "Available Qty",       "label" => ""],
		["column" => "MD1_PART_NUMBER",         "excelColumn" => "Part #",              "label" => ""],
		["column" => "MD1_UNIT_PRICE",          "excelColumn" => "Unit Price",          "label" => ""],
		["column" => "MD1_MARKUP",              "excelColumn" => "Markup",              "label" => ""],
		["column" => "MD1_UPC1",                "excelColumn" => "UPC",                 "label" => ""],
		["column" => "MD1_VENDOR_PART_NUMBER",  "excelColumn" => "Vendor Part Number",  "label" => ""],
		["column" => "MD1_OEM_NUMBER",          "excelColumn" => "OEM Number",          "label" => ""],
		["column" => "VD1_NAME",                "excelColumn" => "Vendor",              "label" => ""],
		["column" => "UM1_PRICE_NAME",          "excelColumn" => "UM",                  "label" => ""]
	];

	public function importXls($file, $fileName, $params)
	{
		$startTime = microtime(true);
		$errors = [];
		$arr_file = explode('.', $fileName);
		$extension = end($arr_file);

		if('csv' == $extension) {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
		} else if('xls' == $extension) {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		} else if('xlsx' == $extension) {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		} else {
			$errors = ['Unsupported format'];
		}

		if (!$errors) {
			$spreadsheet = $reader->load($file->tempName);

			$sheetData = $spreadsheet->getActiveSheet()->toArray();
			$columns = $this->parseColumns($sheetData[0]);
			$records = $this->parseRows($sheetData, $columns);

			//Save items
			$transaction = Yii::$app->db->beginTransaction();
			try {
				$user = Yii::$app->user->getIdentity();

				$rowsCategory = [];
				$rowsCategoryNames = [];
				$rowsGLCode = [];
				$rowsGLCodeNames = [];
				$rowsVendor = [];
				$rowsVendorNames = [];
				$rowsMasterData = [];
				$rowsMasterDataLocations = [];
				$rowsInventory = [];

				// get all PartNumbers, GL_Codes, Vendors, Categories, Units for current import
				$partNumbers = ArrayHelper::getColumn($records, 'MD1_PART_NUMBER');
				$glcodeNames =  ArrayHelper::map($records, 'GL1_NAME', 'GL1_NAME');
				$vendorNames =  ArrayHelper::map($records, 'VD1_NAME', 'VD1_NAME');
				$categoryNames =  ArrayHelper::map($records, 'CA1_NAME', 'CA1_NAME');
				$unitNames =  ArrayHelper::map($records, 'UM1_PRICE_NAME', 'UM1_PRICE_NAME');

				$glCodeModels = \app\models\Glcode::find()->alias('GL1')->select(['GL1.GL1_ID', 'GL1.GL1_NAME'])
					->where(["IN", "GL1.GL1_NAME", $glcodeNames])
					->andWhere(["GL1.GL1_DELETE_FLAG" => 0])
					->andWhere(["GL1.CO1_ID" => $params['CO1_ID']])
					->all();

				$vendorModels = \app\models\Vendor::find()->alias('VD1')->select(['VD1.VD1_ID', 'VD1.VD1_NAME', 'VD1.VD1_NO_INVENTORY'])
					->where(["IN", "VD1.VD1_NAME", $vendorNames])
					->andWhere(["VD1.VD1_DELETE_FLAG" => 0])
					->andWhere(["VD1.CO1_ID" => $params['CO1_ID']])
					->all();

				$categoryModels = \app\models\Category::find()->alias('CA1')->select(['CA1.CA1_ID', 'CA1.CA1_NAME'])
					->where(["IN", "CA1.CA1_NAME", $categoryNames])
					->andWhere(["CA1.CA1_DELETE_FLAG" => 0])
					->andWhere(["CA1.CO1_ID" => $params['CO1_ID']])
					->all();

				$glCodes = ArrayHelper::map($glCodeModels, 'GL1_NAME', 'GL1_ID');
				$vendors = ArrayHelper::map($vendorModels, 'VD1_NAME', 'VD1_ID');
				$categories = ArrayHelper::map($categoryModels, 'CA1_NAME', 'CA1_ID');

				// Checking all GL_Codes, Vendors, Categories, if not exists create
				foreach($records as &$record) {
					$record['MD1_PART_NUMBER'] = strval($record['MD1_PART_NUMBER']);
					$record['GL1_NAME'] = strval($record['GL1_NAME']);

					if ($record['CA1_NAME'] !== NULL && $record['CA1_NAME'] !== "" && !isset($categories[$record['CA1_NAME']])
							&& !isset($rowsCategoryNames[$record['CA1_NAME']])) {

						$categoryModel = new \app\models\Category();
						$categoryModel['CA1_NAME'] = $record['CA1_NAME'];
						$categoryModel['CO1_ID'] = $params['CO1_ID'];
						$categoryModel['CA1_DESCRIPTION'] = $record['CA1_NAME'];
						$categoryModel['CA1_NO_INVENTORY'] = 0;
						$categoryModel['CA1_DELETE_FLAG'] = 0;
						$categoryModel['CA1_CREATED_BY'] = $user->US1_ID;
						$categoryModel['CA1_MODIFIED_BY'] = $user->US1_ID;
						$categoryModel['CA1_CREATED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');
						$categoryModel['CA1_MODIFIED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');

						$rowsCategory[] = $categoryModel->attributes;
						$rowsCategoryNames[$record['CA1_NAME']] = $record['CA1_NAME'];
					}

					if ($record['GL1_NAME'] !== NULL && $record['GL1_NAME'] !== "" && !isset($glCodes[$record['GL1_NAME']])
							&& !isset($rowsGLCodeNames[$record['GL1_NAME']])) {
						$glcodeModel = new \app\models\Glcode();
						$glcodeModel['GL1_NAME'] = $record['GL1_NAME'];
						$glcodeModel['CO1_ID'] = $params['CO1_ID'];
						$glcodeModel['GL1_DESCRIPTION'] = $record['GL1_NAME'];
						$glcodeModel['GL1_NO_STATEMENT'] = 0;
						$glcodeModel['GL1_DELETE_FLAG'] = 0;
						$glcodeModel['GL1_CREATED_BY'] = $user->US1_ID;
						$glcodeModel['GL1_MODIFIED_BY'] = $user->US1_ID;
						$glcodeModel['GL1_CREATED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');
						$glcodeModel['GL1_MODIFIED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');

						$rowsGLCode[] = $glcodeModel->attributes;
						$rowsGLCodeNames[$record['GL1_NAME']] = $record['GL1_NAME'];
					}

					if ($record['VD1_NAME'] && strlen($record['VD1_NAME']) > 0 && $record['VD1_NAME'] !== "" && !isset($vendors[$record['VD1_NAME']])
							&& !isset($rowsVendorNames[$record['VD1_NAME']])) {
						$vendorModel = new \app\models\Vendor();
						$vendorModel['VD1_SHORT_CODE'] = substr(strtoupper(str_replace(' ', '', $record['VD1_NAME'])), 0, 10);
						$vendorModel['VD1_CUSTOMER_NO'] = '';
						$vendorModel['VD1_NAME'] = $record['VD1_NAME'];
						$vendorModel['CO1_ID'] = (int)$params['CO1_ID'];
						$vendorModel['VD1_ADDRESS1'] = '';
						$vendorModel['VD1_ADDRESS2'] = '';
						$vendorModel['VD1_CITY'] = '';
						$vendorModel['VD1_STATE'] = '';
						$vendorModel['VD1_ZIP'] = '';
						$vendorModel['VD1_PHONE'] = '';
						$vendorModel['VD1_FAX'] = '';
						$vendorModel['VD1_EMAIL'] = '';
						$vendorModel['VD1_URL'] = '';
						$vendorModel['VD1_NO_INVENTORY'] = 0;
						$vendorModel['CY1_ID'] = 0;
						$vendorModel['VD1_DELETE_FLAG'] = 0;
						$vendorModel['VD1_CREATED_BY'] = $user->US1_ID;
						$vendorModel['VD1_MODIFIED_BY'] = $user->US1_ID;
						$vendorModel['VD1_CREATED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');
						$vendorModel['VD1_MODIFIED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');

						$rowsVendor[] = $vendorModel->attributes;
						$rowsVendorNames[$record['VD1_NAME']] = $record['VD1_NAME'];
					}
				}

				// insert new created GL_Codes, Vendors, Categories into DB
				$categoryModel = new \app\models\Category();
				Yii::$app->db->createCommand()->batchInsert(\app\models\Category::tableName(), $categoryModel->attributes(), $rowsCategory)->execute();

				$glCodeModel = new \app\models\Glcode();
				Yii::$app->db->createCommand()->batchInsert(\app\models\Glcode::tableName(), $glCodeModel->attributes(), $rowsGLCode)->execute();

				$vendorModel = new \app\models\Vendor();
				Yii::$app->db->createCommand()->batchInsert(\app\models\Vendor::tableName(), $vendorModel->attributes(), $rowsVendor)->execute();

				// get all models
				$glCodeModels = \app\models\Glcode::find()->alias('GL1')->select(['GL1.GL1_ID', 'GL1.GL1_NAME'])
					->where(["IN", "GL1.GL1_NAME", $glcodeNames])
					->andWhere(["GL1.GL1_DELETE_FLAG" => 0])
					->andWhere(["GL1.CO1_ID" => $params['CO1_ID']])
					->all();

				$vendorModels = \app\models\Vendor::find()->alias('VD1')->select(['VD1.VD1_ID', 'VD1.VD1_NAME', 'VD1.VD1_NO_INVENTORY'])
					->where(["IN", "VD1.VD1_NAME", $vendorNames])
					->andWhere(["VD1.VD1_DELETE_FLAG" => 0])
					->andWhere(["VD1.CO1_ID" => $params['CO1_ID']])
					->all();

				$categoryModels = \app\models\Category::find()->alias('CA1')->select(['CA1.CA1_ID', 'CA1.CA1_NAME'])
					->where(["IN", "CA1.CA1_NAME", $categoryNames])
					->andWhere(["CA1.CA1_DELETE_FLAG" => 0])
					->andWhere(["CA1.CO1_ID" => $params['CO1_ID']])
					->all();

				$unitModels = \app\models\MeasurementUnit::find()->alias('UM1')->select(['UM1.UM1_ID', 'UM1.UM1_NAME', 'UM1.UM1_SHORT_CODE'])
					->where(['OR', ["IN", "UM1.UM1_NAME", $unitNames], ["IN", "UM1.UM1_SHORT_CODE", $unitNames]])
					->andWhere(["UM1.UM1_DELETE_FLAG" => 0])
					->all();

				$masterDataModels = \app\models\MasterData::find()->alias('MD1')->select(['*'])
					->where(["MD1.CO1_ID" => $params['CO1_ID']])
					->andWhere(["MD1.MD1_DELETE_FLAG" => 0])
					->andWhere(["IN", "MD1.MD1_PART_NUMBER", $partNumbers])
					->all();

				$locations = \app\models\Location::find()->alias('LO1')->select(['LO1.LO1_ID'])
					->where(["IN", "LO1.LO1_ID", explode(',', $params['LO1_IDs'])])
					->andWhere(["LO1.LO1_DELETE_FLAG" => 0])
					->all();

				$glCodes = ArrayHelper::map($glCodeModels, 'GL1_NAME', 'GL1_ID');
				$vendors = ArrayHelper::map($vendorModels, 'VD1_NAME', 'VD1_ID');
				$categories = ArrayHelper::map($categoryModels, 'CA1_NAME', 'CA1_ID');
				$unitNames = ArrayHelper::map($unitModels, 'UM1_NAME', 'UM1_ID');
				$unitShortCodes = ArrayHelper::map($unitModels, 'UM1_SHORT_CODE', 'UM1_ID');
				$masterDatas = ArrayHelper::map($masterDataModels, 'MD1_PART_NUMBER', 'MD1_ID');

				foreach($masterDataModels as $masterDataModel) {
					$masterDatas[$masterDataModel['MD1_PART_NUMBER']] = $masterDataModel;
				}

				// Create or Update MasterDataModels
				foreach($records as &$record) {
					if ($record['GL1_NAME'] && isset($glCodes[$record['GL1_NAME']])) {
						$record['GL1_ID'] = $glCodes[$record['GL1_NAME']];
					} else {
						$record['GL1_ID'] = 0;
					}

					if ($record['VD1_NAME'] && isset($vendors[$record['VD1_NAME']])) {
						$record['VD1_ID'] = $vendors[$record['VD1_NAME']];
					} else {
						$record['VD1_ID'] = 0;
					}

					if ($record['CA1_NAME'] && isset($categories[$record['CA1_NAME']])) {
						$record['CA1_ID'] = $categories[$record['CA1_NAME']];
					} else {
						$record['CA1_ID'] = 0;
					}

					if (isset($unitNames[$record['UM1_PRICE_NAME']])) {
						$record['UM1_PRICING_ID'] = $unitNames[$record['UM1_PRICE_NAME']];
					} else if (isset($unitShortCodes[$record['UM1_PRICE_NAME']])) {
						$record['UM1_PRICING_ID'] = $unitShortCodes[$record['UM1_PRICE_NAME']];
					} else {
						$record['UM1_PRICING_ID'] = 0;
					}

					if (isset($masterDatas[$record['MD1_PART_NUMBER']]) && isset($masterDatas[$record['MD1_PART_NUMBER']]['MD1_ID']) && (int)$masterDatas[$record['MD1_PART_NUMBER']]['MD1_ID'] > 0 ) {
						$masterDataModel = $masterDatas[$record['MD1_PART_NUMBER']];
					} else {
						$masterDataModel = new \app\models\MasterData();
						$masterDataModel['CA2_ID'] = 0;
					}

					$masterDataModel['CO1_ID'] = $params['CO1_ID'];
					$masterDataModel['MD1_PART_NUMBER'] = $record['MD1_PART_NUMBER'];
					$masterDataModel['CA1_ID'] = isset($record['CA1_ID']) ? $record['CA1_ID'] : 0;
					$masterDataModel['GL1_ID'] = isset($record['GL1_ID']) ? $record['GL1_ID'] : 0;
					$masterDataModel['MD1_UPC1'] = $record['MD1_UPC1'];
					$masterDataModel['MD1_DESC1'] = $record['MD1_DESC1'];
					$masterDataModel['MD1_UNIT_PRICE'] = floatval($record['MD1_UNIT_PRICE']) > 0 ? $record['MD1_UNIT_PRICE'] : 0;
					$masterDataModel['MD1_MARKUP'] = $record['MD1_MARKUP'] > 100 ? $record['MD1_MARKUP'] / 100 : $record['MD1_MARKUP'];
					$masterDataModel['UM1_PRICING_ID'] = $record['UM1_PRICING_ID'];
					$masterDataModel['MD1_CENTRAL_ORDER_FLAG'] = $record['MD1_CENTRAL_ORDER_FLAG'];
					$masterDataModel['MD1_VENDOR_PART_NUMBER'] = strval($record['MD1_VENDOR_PART_NUMBER']);
					$masterDataModel['MD1_OEM_NUMBER'] = strval($record['MD1_OEM_NUMBER']);
					$masterDataModel['MD1_DELETE_FLAG'] = 0;
					$masterDataModel['MD1_CREATED_BY'] = $user->US1_ID;
					$masterDataModel['MD1_MODIFIED_BY'] = $user->US1_ID;
					$masterDataModel['MD1_CREATED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');
					$masterDataModel['MD1_MODIFIED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');

					$rowsMasterData[] = $masterDataModel->attributes;
				}

				//update records with new vendor, glcode, category, unit IDs

				$masterDataModel = new \app\models\MasterData();
				$db = Yii::$app->db;

				$rows = array_chunk($rowsMasterData, 1000);
				foreach ($rows as $row) {
					$sql = $db->queryBuilder->batchInsert(\app\models\MasterData::tableName(), $masterDataModel->attributes(), $row);
					$sql .= " ON DUPLICATE KEY UPDATE ";
					$sql .= "`CO1_ID` = VALUES(`CO1_ID`), ";
					$sql .= "`MD1_PART_NUMBER` = VALUES(`MD1_PART_NUMBER`), ";
					$sql .= "`CA1_ID` = VALUES(`CA1_ID`), ";
					$sql .= "`GL1_ID` = VALUES(`GL1_ID`), ";
					$sql .= "`MD1_UPC1` = VALUES(`MD1_UPC1`), ";
					$sql .= "`MD1_UNIT_PRICE` = VALUES(`MD1_UNIT_PRICE`), ";
					$sql .= "`MD1_MARKUP` = VALUES(`MD1_MARKUP`), ";
					$sql .= "`UM1_PRICING_ID` = VALUES(`UM1_PRICING_ID`), ";
					$sql .= "`MD1_CENTRAL_ORDER_FLAG` = VALUES(`MD1_CENTRAL_ORDER_FLAG`), ";
					$sql .= "`MD1_VENDOR_PART_NUMBER` = VALUES(`MD1_VENDOR_PART_NUMBER`), ";
					$sql .= "`MD1_OEM_NUMBER` = VALUES(`MD1_OEM_NUMBER`), ";
					$sql .= "`MD1_MODIFIED_ON` = VALUES(`MD1_MODIFIED_ON`), ";
					$sql .= "`MD1_MODIFIED_BY` = VALUES(`MD1_MODIFIED_BY`) ";
					$db->createCommand($sql)->execute();
				}

				// Get updated/created MasterDataModels
				$masterDataModels = \app\models\MasterData::find()->alias('MD1')->select(['MD1.MD1_ID', 'MD1.MD1_PART_NUMBER'])
					->where(["MD1.CO1_ID" => $params['CO1_ID']])
					->andWhere(["MD1.MD1_DELETE_FLAG" => 0])
					->andWhere(["IN", "MD1.MD1_PART_NUMBER", $partNumbers])
					->all();

				$MD1_IDs =  ArrayHelper::getColumn($masterDataModels, 'MD1_ID');
				$masterDatas = ArrayHelper::map($masterDataModels, 'MD1_PART_NUMBER', 'MD1_ID');

				foreach($masterDataModels as $masterDataModel) {
					$masterDatas[$masterDataModel['MD1_PART_NUMBER']] = $masterDataModel;
				}

				// Get exist MasterDataLocation models
				$masterDataLocationModels = \app\models\MasterDataLocation::find()->alias('MD2')->select(['*'])
					->where(["MD2.CO1_ID" => $params['CO1_ID']])
					->andWhere(["IN", "MD2.LO1_ID", explode(',', $params['LO1_IDs'])])
					->andWhere(["IN", "MD2.MD1_ID", $MD1_IDs])
					->all();

				$masterDataLocations = [];
				foreach ($masterDataLocationModels as $masterDataLocationModel) {
					$masterDataLocations[$masterDataLocationModel['LO1_ID'].'_'.$masterDataLocationModel['MD1_ID']] = $masterDataLocationModel;
				}

				$vendorsNoInventory = ArrayHelper::map($vendorModels, 'VD1_ID', 'VD1_NO_INVENTORY');

				// Create or Update MasterDataLocations
				foreach($records as &$record) {
					if (isset($masterDatas[$record['MD1_PART_NUMBER']])) {
						foreach($locations as $location) {
							if ( isset($masterDataLocations[$location['LO1_ID'].'_'.$masterDatas[$record['MD1_PART_NUMBER']]['MD1_ID']]) ) {
								$masterDataLocation = $masterDataLocations[$location['LO1_ID'].'_'.$masterDatas[$record['MD1_PART_NUMBER']]['MD1_ID']];
							} else {
								$masterDataLocation = new MasterDataLocation();
								$masterDataLocation['CO1_ID'] = $params['CO1_ID'];
								$masterDataLocation['MD1_ID'] = $masterDatas[$record['MD1_PART_NUMBER']]['MD1_ID'];
								$masterDataLocation['LO1_ID'] = $location['LO1_ID'];
								$masterDataLocation['MD2_ACTIVE'] = 1;
								$masterDataLocation['MD2_ON_HAND_QTY'] = 0;
								$masterDataLocation['MD2_AVAILABLE_QTY'] = 0;
								$masterDataLocation['MD2_MAX_QTY'] = 0;
								$masterDataLocation['MD2_REORDER_QTY'] = 0;
								$masterDataLocation['UM1_RECEIPT_ID'] = $record['UM1_PRICING_ID'];
								$masterDataLocation['UM1_PURCHASE_ID'] = $record['UM1_PRICING_ID'];
								$masterDataLocation['VD1_ID'] = isset($record['VD1_ID']) ? $record['VD1_ID'] : 0;
								$masterDataLocation['MD2_NO_INVENTORY'] = isset($vendorsNoInventory[$record['VD1_ID']]) && (int)$vendorsNoInventory[$record['VD1_ID']] == 1 ? 1 : 0;
								$masterDataLocation['MD2_DELETE_FLAG'] = 0;
								$masterDataLocation['MD2_CREATED_BY'] = $user->US1_ID;
								$masterDataLocation['MD2_MODIFIED_BY'] = $user->US1_ID;
								$masterDataLocation['MD2_CREATED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');
								$masterDataLocation['MD2_MODIFIED_ON'] = new \yii\db\Expression('UTC_TIMESTAMP()');
							}

							$masterDataLocation['MD2_MIN_QTY'] = $record['MD1_MIN_QTY'];

							$rowsMasterDataLocations[] = $masterDataLocation->attributes;
						}
					}
				}

				$masterDataLocationModel = new MasterDataLocation();
				$db = Yii::$app->db;
				$rows = array_chunk($rowsMasterDataLocations, 1000);
				foreach ($rows as $row) {
					$sql = $db->queryBuilder->batchInsert(MasterDataLocation::tableName(), $masterDataLocationModel->attributes(), $row);
					$sql .= " ON DUPLICATE KEY UPDATE ";
					$sql .= "`MD1_ID` = VALUES(`MD1_ID`), ";
					$sql .= "`MD2_MIN_QTY` = VALUES(`MD2_MIN_QTY`), ";
					$sql .= "`MD2_MODIFIED_ON` = VALUES(`MD2_MODIFIED_ON`), ";
					$sql .= "`MD2_MODIFIED_BY` = VALUES(`MD2_MODIFIED_BY`) ";
					$db->createCommand($sql)->execute();
				}

				// Get exist MasterDataLocation models
				$masterDataLocationModels = \app\models\MasterDataLocation::find()->alias('MD2')->select(['*'])
					->where(["MD2.CO1_ID" => $params['CO1_ID']])
					->andWhere(["IN", "MD2.LO1_ID", explode(',', $params['LO1_IDs'])])
					->andWhere(["IN", "MD2.MD1_ID", $MD1_IDs])
					->all();

				$masterDataLocations = [];
				foreach ($masterDataLocationModels as $masterDataLocationModel) {
					$masterDataLocations[$masterDataLocationModel['LO1_ID'].'_'.$masterDataLocationModel['MD1_ID']] = $masterDataLocationModel['MD2_ID'];
				}

				//create inventories
				foreach($masterDataModels as $masterDataModel) {
					foreach($locations as $location) {
						$inventory = new \app\models\Inventory();
						$inventory['CO1_ID'] = $params['CO1_ID'];
						$inventory['LO1_ID'] = $location['LO1_ID'];
						$inventory['TE1_ID'] = 0;
						$inventory['MD1_ID'] = $masterDataModel['MD1_ID'];
						$inventory['MD2_ID'] = isset($masterDataLocations[$location['LO1_ID'].'_'.$masterDataModel['MD1_ID']]) ? $masterDataLocations[$location['LO1_ID'].'_'.$masterDataModel['MD1_ID']] : 0;
						$inventory['JT2_ID'] = 0;
						$inventory['OR2_ID'] = 0;
						$inventory['PO2_ID'] = 0;
						$inventory['MD1_PART_NUMBER'] = $masterDataModel['MD1_PART_NUMBER'] ;
						$inventory['MD1_DESC1'] = $masterDataModel['MD1_DESC1'];
						$inventory['LG1_DESC'] = 'Part imported from excel';
						$inventory['LG1_UNIT_COST'] = $masterDataModel['MD1_UNIT_PRICE'];
						$inventory['LG1_EXTENDED_COST'] = 0;
						$inventory['LG1_OLD_QTY'] = 0;
						$inventory['LG1_NEW_QTY'] = 0;
						$inventory['LG1_ADJ_QTY'] = 0;
						$inventory['LG1_CREATED_BY'] = $user->US1_ID;
						$inventory['LG1_DELETE_FLAG'] = 0;

						$rowsInventory[] = $inventory->attributes;
					}
				}

				$inventoryModel = new \app\models\Inventory();
				Yii::$app->db->createCommand()->batchInsert(\app\models\Inventory::tableName(), $inventoryModel->attributes(), $rowsInventory)->execute();

				$transaction->commit();
			} catch (\Exception $e) {
				$errors[] = $e->getMessage();
				$transaction->rollBack();
			} catch (\Throwable $e) {
				$errors[] = $e->getMessage();
				$transaction->rollBack();
			}
		}

		return [
			'data' => [
				'imageUrl' => '',
				'success' => !$errors,
				'errors' => $errors,
				'transactionTime' => microtime(true) - $startTime
			]
		];
	}

	private function parseColumns($headers) {
		$cols = [];

		foreach ($headers as $header) {
			$found = false;
			foreach ($this->MD1_COLUMN_SET as $col) {
				if($header == $col['excelColumn']) {
					$cols[] = $col['column'];
					$found = true;
				}
			}
			if (!$found) {
				$cols[] = $header;
			}
		}

		return $cols;
	}

	private function parseRows($rows, $cols) {
		$items = [];
		unset($rows[0]);
		foreach ($rows as $row) {
			$item = [];
			for($x = 0; $x < sizeof($cols) ; $x++) {
				$item[$cols[$x]] = $row[$x];
			}
			$items[] = $item;
		}

		return $items;
	}

	public function importReportData(UploadedFile $file, $fileName, $params, $type)
    {
        set_time_limit(3600);

		$errors = [];
		$arr_file = explode('.', $fileName);
		$extension = end($arr_file);

		if('csv' == $extension) {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
		} else if('xls' == $extension) {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		} else if('xlsx' == $extension) {
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		} else {
			$errors = ['Unsupported file format'];
		}

		if (!$errors) {

			$spreadsheet = $reader->load($file->tempName);
			$importDataArray = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

			$byLocation = strrpos($importDataArray[2]['A'], 'by Location') > 0;
			$locationsLine = preg_split('/:/', $importDataArray[4]['A'], -1);
			$locationName = trim($locationsLine[1]);
			$isOneLocation = strtolower($locationName) !== 'all';
			$companyLocationNames = $this->parseJobSummarySalesJournalCompanyLocation($locationName, ' - ', false);

			if ($isOneLocation && ($companyLocationNames['companyName'] == '' || $companyLocationNames['locationName'] == '')) {
				$companyLocationNames['locationName'] = $locationName;
				$locationModel = \app\models\Location::find()
					->andWhere(['or', ['LO1_NAME' => $locationName],
							['LO1_CCCONE_NAME' => $locationName]]
					)->one();

				if ($locationModel) {
					$companyModel = \app\models\Company::findOne([
						'CO1_ID' => $locationModel['CO1_ID']
					]);

					if ($companyModel) {
						$companyLocationNames['companyName'] = $companyModel['CO1_NAME'];
					} else {
						$errors = ['Company not exist for current Location'];
					}
				} else {
					$errors = ['Wrong Location Name'];
				}
			}

			$reportDate = false;
			if (!$errors) {
				$skipLocation = $isOneLocation && $locationModel;
				$reportDate = $this->parseJobSummaryOrSalesJournalMonthYear($importDataArray);
				$headersResult = $this->createAndShiftExcelHeaders($importDataArray, $type, $skipLocation);
				$errors = $headersResult['errors'];
				if (!$errors) {
					$keys = $headersResult['result']['newKeys'];
					$oldKeysPositions = $headersResult['result']['oldKeysPositions'];
				}
			}


			if (!$errors) {
				$relatedData = array();
				foreach ($importDataArray as $row) {
					$values = array();
					foreach($row as $rowValue){
						if(!empty($rowValue)) {
							$values[] = rtrim($rowValue);
						}
					}
					$values = array_filter($row, 'strlen');

					$iter = 0;
					$keysCount = count($keys);
					foreach($values as $key => $value) {
						$iter++;
						if( $iter > $keysCount ) {
							unset($values[$key]);
						}
					}
					if(count($values) == count($keys)){
						$temp = array_combine($keys, $values);
						array_push($relatedData, $temp);
					}
				}

				// Only one excel spreadsheet will be parsed at a time, so we can start the transaction here
				$transaction = Yii::$app->db->beginTransaction();
				try {
					if($reportDate == false) {
						$results['result'] = false;
						$results['errors'] = ['No "Date Range:" was given for the Job Cost Summary'];
					} elseif( $type == \app\models\Import::JOB_COST_SUMMARY ) {
						$results = $this->processJobCostSummaryData($relatedData, $keys, $oldKeysPositions, $reportDate, $isOneLocation, $isOneLocation ? $companyLocationNames : Null, $skipLocation, $byLocation);
					} elseif( $type == \app\models\Import::SALES_JOURNAL_REPORT ) {
						$results = $this->processSalesJournalReportData($relatedData, $keys, $oldKeysPositions, $reportDate, $isOneLocation, $isOneLocation ? $companyLocationNames : Null, $skipLocation, $byLocation);
					}

					// Check for the result and either commit or rollback transaction
					if (!$results['errors']) {

						$timestamp = date("Y_m_d_H_i_s", time());
						$hiddenNameDate = $timestamp . '_' . $file->name;

						if( Yii::$app->params['importFolderUseAbsolutePath']  ){
							$importFolderPath = Yii::$app->params['importFolderPath'];
						}else{
							$importFolderPath = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['importFolderPath'];
						}

						$importFolderPath .= DIRECTORY_SEPARATOR . 'jobCostSummary';
						if (!file_exists($importFolderPath)){
							mkdir($importFolderPath, 0755, true);
						}
						$file->saveAs($importFolderPath . DIRECTORY_SEPARATOR . $hiddenNameDate);

						foreach( $results['results'] as $CO1_ID => $locations){
						    foreach( $locations as $LO1_ID => $result ){
						        $model = new \app\models\Import();
                                $model->im1_file_name = $file->name;
                                $model->im1_file_path = 'imports/jobCostSummary/' . $hiddenNameDate;
                                $model->im1_new_records     = 0; // not usable
                                $model->im1_updated_records = 0; // not usable
                                $model->im1_processed_records = $result['processed'];
                                $model->im1_total_records   = $result['total'];
                                $model->im1_ignored_records = $result['ignored'];
                                $model->im1_co1_id = $CO1_ID;
                                $model->im1_lo1_id = $LO1_ID;
                                $model->im1_file_name = $fileName;
                                $model->im1_file_path = $importFolderPath . DIRECTORY_SEPARATOR . $hiddenNameDate;
                                $model->im1_type = $type;
                                $model->delete_flag = 0;

                                if(!$model->save()){
                                    throw new Exception( join(",", $model->getErrorSummary(true)));
                                }
                            }
                        }

						$transaction->commit();
					} else {
					    $errors = $results['errors'];
						$transaction->rollback();
					}
				} catch (Exception $e) {
					$errors[] = $e->getMessage();
					$transaction->rollback();
				}
			}
		}

		return [
			'success'  => !$errors,
			'errors'   => $errors
		];
    }

    protected static function excelHeaderArrayCompare($keys, $columnNames, $skipLocation) {
        if(!is_array($keys) || count($keys) <= 0) {
            return false;
        }

	    $alphabet = range('A', 'Z');
        if ($skipLocation) {
	        array_shift($alphabet);
        }
		$errors = [];
		$success = false;
		$newKeys = array();
	    $oldKeysPositions = array();

	    $columnIndex = 0;
	    foreach ($columnNames as $index => $column) {
		    $columnFound = false;
		    for ($keyIndex = 0; $keyIndex < count($keys); $keyIndex++) {
			    $key = trim(strtolower($keys[$keyIndex]));
			    $col = trim(strtolower($column));
			    if ($key === $col) {
				    $newKeys[$alphabet[$columnIndex]] = $column;
				    $oldKeysPositions[$alphabet[$keyIndex]] = $alphabet[$columnIndex];
				    $columnFound = true;
				    $success = true;
				    break;
			    }
		    }
		    if ($columnFound == false) {
			    $errors[] = 'Column name "' . $column . '" not found';
		    }
		    $columnIndex++;
	    }

	    return [
		    'success'  => $success,
		    'errors'   => $errors,
		    'newKeys' => $newKeys,
		    'oldKeysPositions' => $oldKeysPositions
	    ];
    }

    /**
     * @author Bradley Cutshall <bradley.cutshall@comparatio.com>
     * @date 7th March 2017
     *
     * Parse the keys from the excel spreadsheet reference with the given type, otherwise defaults to the first line.
     *
     * @param $importDataReference Pointer to an Excel spreadsheet as an array
     * @param $type Integer Excel spreadsheet type
     * @return array Array of strings
     */
    protected function createAndShiftExcelHeaders(&$importDataReference, $type = -1, $skipLocation = false)
    {
        $errors = [];
	    $compareKeys = [];
	    $compareResult = [];

        switch ($type) {
            case \app\models\Import::JOB_COST_SUMMARY :
	            $compareKeys = self::$JOB_COST_SUMMARY_COLUMN_NAMES;
                break;
            case \app\models\Import::SALES_JOURNAL_REPORT :
                $compareKeys = self::$SALES_JOURNAL_REPORT_COLUMN_NAMES;
                if ($skipLocation) {
	                array_shift($compareKeys);
                }
                break;
            default:
            	break;
        }

	    $importDataRefCount = count($importDataReference);
	    for ($i = 0; $i < $importDataRefCount; $i++) {
		    $keys = array_values(array_shift($importDataReference));

		    $compareResult = self::excelHeaderArrayCompare($keys, $compareKeys, $skipLocation);
		    if ($compareResult['success']) {
			    $errors = $compareResult['errors'];
			    break;
		    }
	    }
	    return [
		    'errors'  => $errors,
		    'result' => $compareResult
        ];
    }


    /**
     * @author Bradley Cutshall <bradley.cutshall@comparatio.com>
     * @date 7 March 2017
     *
     * Find the first date within the ['A'] column of the Excel spreadsheet.
     *
     * @param &$importDataReference Array reference to the Excel data array
     * @return bool|string The date, otherwise false if not found
     */
    protected function parseJobSummaryOrSalesJournalMonthYear(&$importDataReference) {
        $date = '';
        $found = false;
        foreach($importDataReference as $i => $row) {
            // Find and parse the "Date"
            $pos = strpos($row['A'], 'Date');
            if($pos !== false) {

                // Convert string to an array
                foreach(str_split($row['A']) as $j => $char) {
                    // Add ascii characters between the '/' and '9'
                    $decChar = ord($char);
                    if( $decChar >= 47 && $decChar <= 57 ) {

                        // Attempt to turn the string into a date
                        $date .= $char;
                        $valid = preg_match("/(\d{1,2})\/(\d{1,2})\/(\d{4})$/", $date);

                        $dateExploded = $valid ? explode('/',$date) : 0;
                        if( $valid
                            && isset($dateExploded[0], $dateExploded[1], $dateExploded[2])
                            && checkdate(intval($dateExploded[0]), intval($dateExploded[1]), intval($dateExploded[2]))) {

                            $found = true;
                            // And exit once the date has been found
                            break;
                        }
                    }
                }
                // Exit loop when the 'Date Range:' has been found
                break;
            }
        }
        return ($found ? $date : $found);
    }

    /**
     * @author Bradley Cutshall <bradley.cutshall@comparatio.com>
     * @date 8 March 2017
     *
     * Splits the company name and location field into two strings
     *
     * @param $string String to split
     * @param $splitter String what to split on
     * @return array['companyName'] string of Company array['locationName'] string of location
     */
    protected function parseJobSummarySalesJournalCompanyLocation($string, $splitter, $byLocation) {
        $coLo = array();
        $formattedString = html_entity_decode($string, ENT_QUOTES | ENT_HTML5); // replace special chars
	    if ($byLocation) {
		    $coLo['companyName'] = '';
		    $coLo['locationName'] = $formattedString;
	    } else {
		    $hyphenIndex = strpos($formattedString, $splitter); // Break up the Location string
		    $coLo['companyName'] = $hyphenIndex ? substr($formattedString, 0, $hyphenIndex) : ''; // Remove apostrophe from company name
		    $coLo['locationName'] = $hyphenIndex ? substr($formattedString, $hyphenIndex + 3) : $formattedString; // Grab location name after the hyphen and space
	    }
        return $coLo;
    }

    /**
     * @author Bradley Cutshall <bradley.cutshall@comparatio.com>
     * @date 8 March 2017
     *
     * Searches the database for both the CimCompany and CimLocation based of the names and optional permutations given.
     *
     * @param $coName String company name to search for the CimCompany model with
     * @param $loName String location name to search for the CimLocation model with
     * @param $companyPermutations=array() Array of string permutations of the CimCompany name to search for the CimCompany model
     * @param $locationPermutations=array() Array of string permutations of the CimLocation name to search for the CimLocation model
     * @return array|bool array['cimCompany']=CimCompany Model and array['cimLocation']=CimLocation Model, or else false if the company and location could not be found.
     */
    protected function getCompanyAndLocationModels($coName, $loName, $companyPermutations = array(), $locationPermutations = array()) {

        // Generate company permutations
        $companyPermutations[] = $coName;
        $companyPermutations[] = str_replace('\'', '', $coName);

        $locationPermutations[] = $loName;
        $locationPermutations[] = str_replace(array('N.','S.','E.','W.'), array('North ','South ','East ','West '), $loName);

        $companyModel  = Null;
        $locationModel = Null;

        foreach($companyPermutations as $coPermu) {
            $companyModel = \app\models\Company::findOne([
                'CO1_NAME' => $coPermu
            ]);

            if(isset($companyModel)) {
                // Found company, now look for location
                foreach($locationPermutations as $loPermu) {

                    $locationModel = \app\models\Location::find()
                        ->andWhere(['CO1_ID'=>$companyModel->CO1_ID])
                        ->andWhere(['or', ['LO1_NAME' => $loPermu],
                                          ['LO1_CCCONE_NAME' => $loPermu]]
                        )->one();

                    if(isset($locationModel)) {
                        // Location was found with company
                        break;
                    }
                }
                break;
            }
        }

	    if (!isset($companyModel)) {
		    foreach($locationPermutations as $loPermu) {

			    $locationModel = \app\models\Location::find()
				    ->andWhere(['or', ['LO1_NAME' => $loPermu],
						    ['LO1_CCCONE_NAME' => $loPermu]]
				    )->one();

			    if (isset($locationModel)) {
				    $companyModel = \app\models\Company::findOne([
					    'CO1_ID' => $locationModel['CO1_ID']
				    ]);
				    break;
			    }
		    }
	    }

        $retVal = false; // Default return
        if(isset($companyModel, $locationModel)) {
            $retVal = array('cimCompany'=>$companyModel, 'cimLocation'=>$locationModel);
        }
        return $retVal;
    }

    /**
     * @author Bradley Cutshall <bradley.cutshall@comparatio.com>
     * @date 8 March 2017
     *
     * Processes the Job Cost excel data for the month and either creates a new record or updates an existing one.

     * @param $parsedFileArray Array the Excel spreadsheet
     * @param $reportDate String which contains a timestamp able to be created by a new DateTime('timestamp')
     * @return Array with ['result']=bool, ['new-records']=int, ['updated-records']=int, ['total-processed']=int;
     */
    protected function processJobCostSummaryData(&$parsedFileArray, $keys, $oldKeysPositions, $reportDate, $isOneLocation, $companyLocationNames, $skipLocation, $byLocation)
    {
        $errors = [];
        $results = [];
        if (isset($parsedFileArray, $reportDate)) {

            $companyData = [];

            foreach ($parsedFileArray as $i => $row) {
                if($errors){
                    break;
                }

                if (!$skipLocation) {
	                $companyLocationNames = $this->parseJobSummarySalesJournalCompanyLocation($row[$keys[$oldKeysPositions['A']]], ' - ', $byLocation);
                }
	            $companyLocationModels = $this->getCompanyAndLocationModels($companyLocationNames['companyName'], $companyLocationNames['locationName']);

	            $cimCompany = null;
	            $cimLocation = null;
	            if($companyLocationModels){
	                // Company and location were found! Look for an existing rawSalesDataStoreMonth or make a new one
                    $cimCompany  = $companyLocationModels['cimCompany'];
                    $cimLocation = $companyLocationModels['cimLocation'];
                }

	            if(!$cimCompany && !$cimLocation){
                    $errors[] = "Line Number #" . $i . ' does not have correct Company/Shop name';
                }else{
	                if(!isset($results[$cimCompany->CO1_ID][$cimLocation->LO1_ID])){
	                    $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID] = [
	                        'total' => 0,
                            'processed' => 0,
                            'ignored' => 0
                        ];
                    }

	                $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID]['total'] ++;

		            $zeroRow =  $row[$keys[$oldKeysPositions['D']]] == 0.0 &&
					            $row[$keys[$oldKeysPositions['E']]] == 0.0 &&
					            $row[$keys[$oldKeysPositions['F']]] == 0.0 &&
					            $row[$keys[$oldKeysPositions['G']]] == 0.0 &&
					            $row[$keys[$oldKeysPositions['H']]] == 0.0 &&
					            $row[$keys[$oldKeysPositions['I']]] == 0.0 &&
					            $row[$keys[$oldKeysPositions['J']]] == 0.0 &&
					            $row[$keys[$oldKeysPositions['K']]] == 0.0;

                    if ($zeroRow){
                         $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID]['ignored'] ++;
                    }else{

                        if(!isset($companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID])){
                            // Generate timestamp
                            $date = new \DateTime($reportDate);
                            $timestamp = $date->format('Y-m-d H:i:s');
                            $month = $date->format('F');
                            $year = $date->format('Y');

                            // Search for existing raw sales data store month
                            $rawSalesDataStoreMonth = RawSalesDataStoreMonth::findOne([
                                'r03_co1_name' => $cimCompany->CO1_NAME,
                                'r03_shop_name' => $cimLocation->LO1_NAME,
                                'r03_month' => $month,
                                'r03_year' => $year,
                            ]);

                            // Determine if we need to create a new record or update existing
                            if (!isset($rawSalesDataStoreMonth)) {
                                // Record doest not exist, add one
                                $rawSalesDataStoreMonth = new RawSalesDataStoreMonth();
                                // Set the required fields
                                $rawSalesDataStoreMonth->r03_type = 1;
                                $rawSalesDataStoreMonth->created_by = Yii::$app->user->getId();
                                $rawSalesDataStoreMonth->modified_by = Yii::$app->user->getId();
                                $rawSalesDataStoreMonth->r03_co1_id = $cimCompany->CO1_ID;
                                $rawSalesDataStoreMonth->r03_co1_name = $cimCompany->CO1_NAME;
                                $rawSalesDataStoreMonth->r03_shop_name = $cimLocation->LO1_NAME;
                                $rawSalesDataStoreMonth->r03_shop_code = $cimLocation->LO1_SHORT_CODE;
                                $rawSalesDataStoreMonth->r03_date = $timestamp;
                                $rawSalesDataStoreMonth->r03_month = $month;
                                $rawSalesDataStoreMonth->r03_year = $year;
                            }

                            // ENSURE THAT YOU SET THE VALUES YOU WILL BE UPDATING TO DEFAULT AT THIS POINT!
                            $rawSalesDataStoreMonth->r03_gross_amount = 0; //
                            $rawSalesDataStoreMonth->r03_body_labor_amount = 0; //
                            $rawSalesDataStoreMonth->r03_refinish_amount = 0; //
                            $rawSalesDataStoreMonth->r03_paint_amount = 0; //
                            $rawSalesDataStoreMonth->r03_paint_materials_amount = 0; //
                            $rawSalesDataStoreMonth->r03_body_materials_amount = 0; //
                            $rawSalesDataStoreMonth->r03_paint_hours = 0; //
                            $rawSalesDataStoreMonth->r03_c_paint_and_refinish_labor_total = 0; //
                            $rawSalesDataStoreMonth->r03_c_paint_and_refinish_labor_hours = 0; //
                            $rawSalesDataStoreMonth->r03_work_days = 24;
                            $rawSalesDataStoreMonth->r03_weeks = 4.2857;
                            $rawSalesDataStoreMonth->r03_manual_import = RawSalesDataStoreMonth::MANUAL_IMPORT;

                            $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID] = $rawSalesDataStoreMonth;
                            unset($rawSalesDataStoreMonth);
                        }

                        if($companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]){

                            $catName = $row[$keys[$oldKeysPositions['B']]];
                            $subCatName = $row[$keys[$oldKeysPositions['C']]];

                            //SALES - r03_gross_amount
                            if(intval($row[$keys[$oldKeysPositions['D']]]) > 0) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_gross_amount += $row[$keys[$oldKeysPositions['D']]];
                            }

                            // BODY LABOR - r03_body_labor_amount
                            if(($catName == 'Labor' && ($subCatName == 'Body' || $subCatName == 'Clean Up Labor' || $subCatName == 'Detail' || $subCatName == 'Glass' || $subCatName == 'Glass Charges' || $subCatName == 'In House Labor' || $subCatName == 'Plastic' || $subCatName == 'Aluminum Labor' || $subCatName == 'Body Plastic Repair Labor' || $subCatName == 'Classic Car Labor'))) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_body_labor_amount += $row[$keys[$oldKeysPositions['D']]];
                            }

                            //PAINT LABOR - r03_paint_amount
                            if(($catName == 'Labor' && ($subCatName == 'Paint'))) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_paint_amount += $row[$keys[$oldKeysPositions['D']]];
                            }

                            // PAINT AND REFINISH LABOR - r03_c_paint_and_refinish_labor_total
                            if(($catName == 'Labor' && ($subCatName == 'Refinish / Paint' || $subCatName == 'Paint' || $subCatName == 'Refinish' || $subCatName == 'Color Sand & Buff' || $subCatName == 'Classic Car Paint'))) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_c_paint_and_refinish_labor_total += $row[$keys[$oldKeysPositions['D']]];
                            }

                            //REFINISH LABOR - r03_refinish_amount
                            if(($catName == 'Labor' && ($subCatName == 'Refinish'))) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_refinish_amount += $row[$keys[$oldKeysPositions['D']]];
                            }

                            // PAINT SOLD - r03_paint_materials_amount
                            if(($catName == 'Miscellaneous' && ($subCatName == 'Paint Materials' || $subCatName == 'Classic Car Paint Materials'))
                                || ($catName == 'Parts' && ($subCatName == 'Add Paint Materials' || $subCatName == 'Paint' || $subCatName == 'Misc Paint Supplies' || $subCatName == 'Paint Supplies Non-Tax'))) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_paint_materials_amount += $row[$keys[$oldKeysPositions['D']]];
                            }

                            // BODY MATERIALS - r03_body_materials_amount
                            if(($catName == 'Parts' && ($subCatName == 'Body Material' || $subCatName == 'Miscellaneous'))
                                || ($catName == 'Miscellaneous' && ($subCatName == 'Shop Materials'))) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_body_materials_amount += $row[$keys[$oldKeysPositions['D']]];
                            }

                            // PAINT LABOR HOURS - r03_paint_hours
                            if(($catName == 'Labor' && ($subCatName == 'Paint'))) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_paint_hours += $row[$keys[$oldKeysPositions['E']]];
                            }

                            // PAINT AND REFINISH HOURS - r03_c_paint_and_refinish_labor_hours
                            if(($catName == 'Labor' && ($subCatName == 'Paint' || $subCatName == 'Refinish' || $subCatName == 'Refinish / Paint' || $subCatName == 'Classic Car Paint' || $subCatName == 'Color Sand & Buff'))) {
                                $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_c_paint_and_refinish_labor_hours += $row[$keys[$oldKeysPositions['E']]];
                            }

                            $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID]['processed'] ++;
                        }else{
                            $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID]['ignored'] ++;
                        }
                    }
                }
	            unset($parsedFileArray[$i]);
            }
            // Save
            if(!$errors){
                foreach($companyData as $CO1_ID => $locations ) {
                    foreach($locations as $LO1_ID => $rawSalesDataStoreMonth ) {
                        if(!$rawSalesDataStoreMonth->save(false)) {
                            $errors[] = 'Could Not Save Job Cost Data For Company-Location: ' . $rawSalesDataStoreMonth->r03_co1_name . '-' . $rawSalesDataStoreMonth->r03_shop_name;
                        }
                    }
                }
            }
        } else {
            Yii::getLogger()->log('Import Job Cost Summary Data - Parsed File Not Set! ');
        }

        return [
            'errors' => $errors,
            'results' => $results
        ];
    }

    /**
     * @author Bradley Cutshall <bradley.cutshall@comparatio.com>
     * @date 9 March 2017
     *
     * Processes the Raw Sales Data excel data for the month and either creates a new record or updates an existing one.
     *
     * @param $parsedFileArray Array the Excel spreadsheet
     * @param $reportDate String with a valid timestamp
     * @return Array ['result'] boolean, ['new-records'] int, ['updated-records'] int, ['total-processed'] int
     */
    protected function processSalesJournalReportData(&$parsedFileArray, $keys, $oldKeysPositions, $reportDate, $isOneLocation, $companyLocationNames, $skipLocation, $byLocation)
    {
        $errors = [];
        $results = [];
        if (isset($parsedFileArray, $reportDate)) {

            $companyData = [];
            $sum = 0;

            foreach ($parsedFileArray as $i => $row) {
                if($errors){
                    break;
                }

                if (!$skipLocation) {
	                $companyLocationNames = $this->parseJobSummarySalesJournalCompanyLocation($row[$keys[$oldKeysPositions['A']]], ' - ', $byLocation);
                }
	            $companyLocationModels = $this->getCompanyAndLocationModels($companyLocationNames['companyName'], $companyLocationNames['locationName']);

	            $cimCompany = null;
	            $cimLocation = null;
	            if($companyLocationModels){
	                // Company and location were found! Look for an existing rawSalesDataStoreMonth or make a new one
                    $cimCompany  = $companyLocationModels['cimCompany'];
                    $cimLocation = $companyLocationModels['cimLocation'];
                }

	            if(!$cimCompany && !$cimLocation){
                    $errors[] = "Line Number #" . $i . ' does not have correct Company/Shop name';
                }else{
	                if(!isset($results[$cimCompany->CO1_ID][$cimLocation->LO1_ID])){
                        $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID] = [
                            'total' => 0,
                            'processed' => 0,
                            'ignored' => 0
                        ];
                    }

	                $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID]['total'] ++;

	                $zeroRow =  $row[$keys[$oldKeysPositions['H']]] == 0.0 &&
			                    $row[$keys[$oldKeysPositions['I']]] == 0.0 &&
			                    $row[$keys[$oldKeysPositions['J']]] == 0.0 &&
			                    $row[$keys[$oldKeysPositions['K']]] == 0.0 &&
			                    $row[$keys[$oldKeysPositions['L']]] == 0.0 &&
			                    $row[$keys[$oldKeysPositions['M']]] == 0.0 &&
			                    $row[$keys[$oldKeysPositions['N']]] == 0.0 &&
			                    $row[$keys[$oldKeysPositions['O']]] == 0.0;

                    if ($zeroRow){
                         $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID]['ignored'] ++;
                    }else{
                        if(!isset($companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID])){
                            // Generate timestamp
                            $date = new \DateTime($reportDate);
                            $timestamp = $date->format('Y-m-d H:i:s');
                            $month = $date->format('F');
                            $year = $date->format('Y');

                            // Search for existing raw sales data store month
                            $rawSalesDataStoreMonth = RawSalesDataStoreMonth::findOne([
                                'r03_co1_name' => $cimCompany->CO1_NAME,
                                'r03_shop_name' => $cimLocation->LO1_NAME,
                                'r03_month' => $month,
                                'r03_year' => $year,
                            ]);

                            // Determine if we need to create a new record or update existing
                            if(!isset($rawSalesDataStoreMonth)){
                                // Record doest not exist, add one
                                $rawSalesDataStoreMonth = new RawSalesDataStoreMonth();
                                // Set the required fields
                                $rawSalesDataStoreMonth->r03_type = 1;
                                $rawSalesDataStoreMonth->created_by = Yii::$app->user->id;
                                $rawSalesDataStoreMonth->modified_by = Yii::$app->user->id;
                                $rawSalesDataStoreMonth->r03_co1_id = $cimCompany->CO1_ID;
                                $rawSalesDataStoreMonth->r03_co1_name = $cimCompany->CO1_NAME;
                                $rawSalesDataStoreMonth->r03_shop_name = $cimLocation->LO1_NAME;
                                $rawSalesDataStoreMonth->r03_shop_code = $cimLocation->LO1_SHORT_CODE;
                                $rawSalesDataStoreMonth->r03_date = $timestamp;
                                $rawSalesDataStoreMonth->r03_month = $month;
                                $rawSalesDataStoreMonth->r03_year = $year;
                            }

                            // Record now exists


                            // ENSURE THAT YOU SET THE VALUES YOU WILL BE UPDATING TO DEFAULT AT THIS POINT!
                            $rawSalesDataStoreMonth->r03_ro_count = 0;
                            $rawSalesDataStoreMonth->r03_gross_amount = 0;
                            $rawSalesDataStoreMonth->r03_work_days = 24;
                            $rawSalesDataStoreMonth->r03_weeks = 4.2857;
                            $rawSalesDataStoreMonth->r03_manual_import = RawSalesDataStoreMonth::MANUAL_IMPORT;

                            $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID] = $rawSalesDataStoreMonth;
                            unset($rawSalesDataStoreMonth);
                        }

                        if($companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]){
                            // ENSURE THAT YOU HAVE SET THE VALUES YOU WILL BE UPDATING TO DEFAULT BEFORE THIS POINT!

                            // Increment total ro counts
                            $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_ro_count++;

                            // Sum the total job cost
	                        $rowValue = $row[$keys[$oldKeysPositions['J']]];
                            $sum += $rowValue;
                            $companyData[$cimCompany->CO1_ID][$cimLocation->LO1_ID]->r03_gross_amount += $rowValue;
                            $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID]['processed'] ++;
                        }else{
                            $results[$cimCompany->CO1_ID][$cimLocation->LO1_ID]['ignored'] ++;
                        }
                    }

                }
                unset($parsedFileArray[$i]);
            }
            // Save
            // Save
            if(!$errors){
                foreach($companyData as $CO1_ID => $locations ) {
                    foreach($locations as $LO1_ID => $rawSalesDataStoreMonth ) {
                        if(!$rawSalesDataStoreMonth->save(false)) {
                            $errors[] =  'Could Not Save Sales Cost Data For Company-Location: ' . $rawSalesDataStoreMonth->r03_co1_name . '-' . $rawSalesDataStoreMonth->r03_shop_name;
                        }
                    }
                }
            }
        }

        return [
            'errors' => $errors,
            'results' => $results
        ];
    }
}