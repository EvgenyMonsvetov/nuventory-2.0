<?php

namespace app\services;

use app\models\Prices;
use yii\base\Component;

class Price extends Component
{
	public function generateXls($params) {
		$filePrefix = 'PriceChangeLog_';
		$model = new Prices();
		$models = $model->search($params);
		$query = '';

		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename, $models);
	}

}