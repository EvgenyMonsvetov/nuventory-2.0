<?php

namespace app\services;

use app\models\Inventory;
use app\models\MasterDataLocation;
use app\models\OrderLine;
use Yii;
use yii\base\Component;
use app\models\OrderHeader as OrderHeaderModel;
use app\models\OrderLine as OrderLineModel;
use yii\db\Exception;
use yii\db\Expression;

class OrderHeader extends Component
{
    public function create( $order )
    {
        $errors = [];

        $isTechScan = $order['LO1_FROM_ID'] == $order['LO1_TO_ID'] ? 1 : 0;

        if(!isset($order['parts'])){
            $errors[] = 'No order parts!';
        }else{
            foreach( $order['parts'] as $part){
                $AVAILABLE_QTY = $isTechScan ? $part['MD2_AVAILABLE_QTY'] : $part['MD2_AVAILABLE_QTY_FROM'];

                $NO_INVENTORY  = $isTechScan ? $part['NO_INVENTORY']: $part['NO_INVENTORY_FROM'];

                if ( $part['MD1_ORDER_QTY'] > $AVAILABLE_QTY && $NO_INVENTORY == 0) {
                    $errors[] = $part['MD1_PART_NUMBER'] . ' - Ordering: ' . $part['MD1_ORDER_QTY'] . ', Available: ' . $AVAILABLE_QTY;
                }
                if ($part['MD1_ORDER_QTY'] <= 0) {
                    $errors[] = $part['MD1_PART_NUMBER'] . ': ' . $part['MD1_DESC1'];
                }
            }
        }

        $OR1_ID = null;
        if(!$errors){

            $transaction = Yii::$app->db->beginTransaction();
            try {

                $orderHeaderModel = new OrderHeaderModel();

                $orderTotal = 0;
                foreach( $order['parts'] as $part){
                    $unitPrice = $part["MD1_UNIT_PRICE"];
                    $orderQty  = $part["MD1_ORDER_QTY"];
                    $orderTotal += $unitPrice * $orderQty;
                }

                $user = Yii::$app->user->getIdentity();
                $orderHeaderModel->load([
                    'VD1_ID'              => $order['VD1_ID'],
                    'TE1_ID'              => $isTechScan ? $order['TE1_ID']:"",
                    'CO1_ID'              => $user->CO1_ID,
                    'OR1_ORDERED_BY'      => $user->US1_ID,
                    'LO1_FROM_ID'         => $order['LO1_FROM_ID'],
                    'LO1_TO_ID'           => $order['LO1_TO_ID'],
                    'OR1_NUMBER'          => $order['OR1_NUMBER'],
                    'OR1_TYPE'            => $isTechScan ? OrderHeaderModel::TYPE_TECH_SCAN :
                                                           OrderHeaderModel::TYPE_STORE_ORDER,
                    'OR1_STATUS'          => $isTechScan ? OrderHeaderModel::ORDER_STATUS_RECEIVED :
                                                           OrderHeaderModel::ORDER_STATUS_OPEN,
                    'OR1_TOTAL_VALUE'     => $orderTotal,
                    'OR1_SHIPPING_METHOD' => '',
                    'OR1_COMMENT'         => $order['OR1_COMMENT']
                ], '');

                if(!$orderHeaderModel->save(true)){
                    throw new Exception($orderHeaderModel->getErrorSummary(true));
                }else{
                    $OR1_ID = $orderHeaderModel->OR1_ID;
                    foreach( $order['parts'] as $index=>$part){

                        $masterData = \app\models\MasterData::findOne(['MD1_ID' => $part["MD1_ID"], 'MD1_DELETE_FLAG' => 0]);
                        $index ++;
                        $orderLine = new \app\models\OrderLine();
                        $data = [
                            "OR1_ID" => $orderHeaderModel->OR1_ID,
                            "TE1_ID" => $isTechScan ? $order['TE1_ID']:"",
                            "OR2_LINE"   => $index,
                            "OR2_TYPE"   => $orderHeaderModel->OR1_TYPE,
                            "OR2_STATUS" => $orderHeaderModel->OR1_STATUS,
                            "OR2_ORDER_QTY" =>$part["MD1_ORDER_QTY"],
                            "MD1_ID"  => $part["MD1_ID"],
                            "CO1_ID"  => $user->CO1_ID,
                            "CA1_ID"  => $masterData->CA1_ID,
                            "GL1_ID"  => $masterData->GL1_ID,
                            "LO1_FROM_ID" => $orderHeaderModel->LO1_FROM_ID,
                            "LO1_TO_ID"   => $orderHeaderModel->LO1_TO_ID,
                            "FI1_ID" => $part["FI1_ID"],
                            "UM1_ID" => 0,
                            "MD1_PART_NUMBER" => $part["MD1_PART_NUMBER"],
                            "MD1_UPC1" => $part["MD1_UPC1"],
                            "MD1_UPC2" => '', //$part["MD1_UPC2"],
                            "MD1_UPC3" => '', //$part["MD1_UPC3"],
                            "MD1_DESC1" => $part["MD1_DESC1"],
                            "MD1_DESC2" => '', //$part["MD1_DESC2"],
                            "MD1_UM" => '', //$part["MD1_UM"],
                            "OR2_TOTAL_SELL" => $part["MD1_UNIT_PRICE"] * $part["MD1_ORDER_QTY"],
                            "OR2_TOTAL_VALUE" =>$part["MD1_UNIT_COST"] * $part["MD1_ORDER_QTY"],
                            "MD1_UNIT_PRICE" => $part["MD1_UNIT_PRICE"],
                            "MD1_ON_HAND_QTY" => $part["MD2_ON_HAND_QTY"],
                            "MD1_AVAILABLE_QTY" => $part["MD2_AVAILABLE_QTY"]
                        ];
                        $orderLine->load($data, '');

                        if(!$orderLine->save(true)){
                            throw new Exception($orderLine->getErrorSummary(true));
                        }
                    }

                    $inventory = new Inventory();
                    $inventory->addTechScanFromOrder( $orderHeaderModel->OR1_ID );

                    foreach( $order['parts'] as $index=>$part){

                        $mdLocationQuery = MasterDataLocation::find()
                                        ->andFilterWhere(['=', 'MD1_ID', $part['MD1_ID']])
                                        ->andFilterWhere(['=', 'LO1_ID', $orderHeaderModel->LO1_FROM_ID]);

                        if($isTechScan){
                            $mdLocationQuery->andFilterWhere(['=', 'coalesce(MD2_NO_INVENTORY,0)', 0]);
                        }

                        $mdLocation = $mdLocationQuery->one();

                        if($mdLocation){
                            $newQty       = $mdLocation->MD2_AVAILABLE_QTY - $part["MD1_ORDER_QTY"];
                            $mdLocation->MD2_AVAILABLE_QTY = $newQty < 0 ? 0 : $newQty;

                            if($isTechScan){
                                $newOnHandQty = $mdLocation->MD2_ON_HAND_QTY   - $part['MD1_ORDER_QTY'];
                                $mdLocation->MD2_ON_HAND_QTY = $newOnHandQty < 0 ? 0 : $newOnHandQty;
                            }

                            if(!$mdLocation->save( true )){
                                throw new Exception($mdLocation->getErrorSummary(true));
                            }
                        }
                    }
                }

                $transaction->commit();
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
                $transaction->rollBack();
            } catch (\Throwable $e) {
                $errors[] = $e->getMessage();
                $transaction->rollBack();
            }
        }

        return [
            'success' => !$errors,
            'errors'  => $errors,
            'OR1_ID'  => $OR1_ID
        ];
    }

    public function receive($OR1_ID, $orderLines)
    {
        $errors = [];
        $transaction = Yii::$app->db->beginTransaction();

        try{
            foreach( $orderLines as $line){
                if($line['QTY'] > 0){
                    $orderLine = OrderLineModel::findOne(['OR2_ID' => $line['OR2_ID'], 'OR2_DELETE_FLAG' => 0]);
                    $orderLine->updateStock($line['QTY']);

                    $masterDataLocation = new MasterDataLocation();
                    $masterDataLocation->updateOrderLineStock($orderLine, $line['QTY']);

                    $inventoryModel = new \app\models\Inventory();
                    $inventoryModel->updateOrderLineStock($orderLine, $line['QTY']);
                }
            }

            $orderLines = OrderLineModel::findAll(['OR1_ID' => $OR1_ID, 'OR2_DELETE_FLAG' => 0]);
            $received = 0;
            foreach($orderLines as $orderLine1 ){
                if($orderLine1->OR2_STATUS == \app\models\OrderHeader::ORDER_STATUS_RECEIVED ){
                    $received ++;
                }
            }
            if(count($orderLines) == $received){
                $orderHeader = \app\models\OrderHeader::findOne([
                    'OR1_ID' => $OR1_ID,
                    'OR1_DELETE_FLAG' => 0
                ]);
                $orderHeader->OR1_STATUS = \app\models\OrderHeader::ORDER_STATUS_RECEIVED;
                $orderHeader->save(true);
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }

        return [
            'success' => !$errors,
            'errors'  => $errors
        ];
    }

	public function pickTicket( $OR1_ID )
	{
		$errors = [];
		$transaction = Yii::$app->db->beginTransaction();
		try {
			$orderHeader = \app\models\OrderHeader::findOne([
				'OR1_ID' => $OR1_ID,
				'OR1_DELETE_FLAG' => 0
			]);

			$orderHeader->OR1_INV_STATUS = \app\models\OrderHeader::ORDER_STATUS_PICK_TICKET;
			if (!$orderHeader->save( true )) {
				$errors[] = $orderHeader->getErrorSummary(true);
			}

			$transaction->commit();
		} catch (\Exception $e) {
			$errors[] = $e->getMessage();
			$transaction->rollBack();
		}

		return [
			'success' => !$errors,
			'errors'  => $errors
		];
	}

    public function complete( $data )
    {
	    $OR1_ID = $data['OR1_ID'];
	    $orderLines = $data['orderLines'];

        $errors = [];
        $transaction = Yii::$app->db->beginTransaction();
        try {
        	$orderTotal = 0;
	        foreach( $orderLines as $line){
		        if($line['QTY'] > 0){
			        $orderLine = OrderLineModel::findOne(['OR2_ID' => $line['OR2_ID'], 'OR2_DELETE_FLAG' => 0]);
			        $orderLine->OR2_ORDER_QTY  = $line['OR2_ORDER_QTY'];
			        $orderLine->OR2_TOTAL_SELL = $line['OR2_EXTENDED_VALUE'];
			        $orderLine->MD1_UNIT_PRICE = $line['MD1_UNIT_PRICE'];
			        $orderLine->OR2_STATUS     = \app\models\OrderHeader::ORDER_STATUS_SENT;
			        $orderLine->save(true);

			        $orderTotal += $orderLine->OR2_TOTAL_SELL;
		        }
	        }

	        $orderHeader = \app\models\OrderHeader::findOne([
                'OR1_ID' => $OR1_ID,
                'OR1_DELETE_FLAG' => 0
            ]);

            $orderHeader->OR1_INV_STATUS = \app\models\OrderHeader::ORDER_STATUS_INVOICED;
            $orderHeader->OR1_STATUS = \app\models\OrderHeader::ORDER_STATUS_SENT;
            $orderHeader->OR1_COMPLETED_ON = new Expression('UTC_TIMESTAMP()');
            $orderHeader->OR1_TOTAL_VALUE = $orderTotal;
            if($orderHeader->save( true )){
                $masterDataLocation = new MasterDataLocation();
                $masterDataLocation->updateStockWhenOrderIsComplete($OR1_ID);

                $inventory = new Inventory();
                $inventory->updateStockWhenOrderIsComplete($OR1_ID);

	            $invoice = new \app\services\Invoice();
	            $result = $invoice->createFromStoreOrder( $OR1_ID );
	            if(!$result['success']){
		            throw new Exception(join("\n", $result['errors']));
	            }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }

        return [
            'success' => !$errors,
            'errors'  => $errors
        ];
    }

	public function generateXls($params, $type = OrderHeaderModel::TYPE_STORE_ORDER ) {

        $filePrefix = $type == OrderHeaderModel::TYPE_STORE_ORDER ? 'StoreOrders' : 'TechScans';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.xls' ;

		$model = new OrderHeaderModel();
		$query = $model->getQuery($params, $type);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}

	public function updateTotals($OR1_ID) {
		$orderLines = OrderLine::find()->where(["OR1_ID" => $OR1_ID, 'OR2_DELETE_FLAG' => 0])->all();
		$orderTotalValue = 0;
		foreach ($orderLines as $orderLine) {
			$orderTotalValue += $orderLine['OR2_TOTAL_SELL'];
		}
		$orderHeader = \app\models\OrderHeader::findOne([
			'OR1_ID' => $OR1_ID,
			'OR1_DELETE_FLAG' => 0
		]);

		$orderHeader->OR1_TOTAL_VALUE = $orderTotalValue;
		$errors = [];
		if (!$orderHeader->save( true )) {
			$errors[] = $orderHeader->getErrorSummary(true);
		}

		return [
			'success' => !$errors,
			'errors'  => $errors
		];
	}
}