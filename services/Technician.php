<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\Technician as TechnicianModel;
use app\models\User as UserModel;
use yii\db\Exception;

class Technician extends Component
{
    public function createTechnician($data)
    {
        $errors = [];

        $data = Yii::$app->request->post();

	    $transaction = Yii::$app->db->beginTransaction();
        try {
	        if (isset($data['TE1_ID']) && (int)$data['TE1_ID'] > 0) {
		        $technicianModel = TechnicianModel::findOne((int)$data['TE1_ID']);
		        $technicianModel->TE1_ID = (int)$data['TE1_ID'];
		        $data['TE1_CREATED_ON'] = $technicianModel->TE1_CREATED_ON;
		        $data['TE1_MODIFIED_ON'] = $technicianModel->TE1_MODIFIED_ON;
	        } else {
		        unset($data['TE1_ID']);
		        $technicianModel = new TechnicianModel();
	        }

	        if ( !($technicianModel->load($data, '') && $technicianModel->save( true )) ){
		        throw new Exception('', $technicianModel->getErrorSummary(true), TechnicianModel::$MODEL_ERROR);
	        } else {
		        if (isset($data['TE1_ID']) && (int)$data['TE1_ID'] > 0) {
			        $userModel = UserModel::findByTE1_ID($technicianModel->TE1_ID);
			        $userModel->setScenario(UserModel::SCENARIO_CHANGE_USER);
		        } else {
			        $userModel = new UserModel();
		        }

		        $data['user']['TE1_ID'] = $technicianModel->TE1_ID;
		        $data['user']['GR1_ID'] = \app\models\Group::GROUP_STANDARD_USER;
		        $data['user']['US1_TYPE'] = 0;

		        if ( !$userModel->load($data['user'], '') ) {
			        $errors = $userModel->getErrorSummary(true);
		        } else {
			        $userModel->setPassword($data['user']['US1_PASS']);
			        $userModel->generateAuthKey();

			        if(!$userModel->save( true )) {
				        throw new Exception('', $userModel->getErrorSummary(true), UserModel::$MODEL_ERROR);
			        }
		        }
	        }

	        $transaction->commit();
        } catch (\Exception $e) {
	        if ($e->getCode() == TechnicianModel::$MODEL_ERROR) {
		        $errors = $e->errorInfo;
	        } else {
		        $errors[] = $e->getMessage();
	        }
	        $transaction->rollBack();
        }

	    return [
		    'data' => [
			    'success'=> !$errors,
			    'TE1_ID' => !$errors ? $technicianModel->TE1_ID : null,
			    'errors' => $errors
		    ]
	    ];
    }

    public function deleteTechnician($id)
    {
        $technicianModel = new TechnicianModel();
        $model = TechnicianModel::find()->where(["TE1_ID" => $id])->one();

        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        } else {
            $userModel = UserModel::findOne(['TE1_ID' => $id]);
            $userModel->delete();
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

	public function generateXls($params) {
		$filePrefix = 'Technician_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new TechnicianModel();
		$query = $model->getQuery($params);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}
}