<?php

namespace app\services;

use PhpOffice\PhpSpreadsheet\Calculation\Exception;
use Yii;
use yii\base\Component;
use app\models\StatementLine as StatementLineModel;


class StatementLine extends Component
{
	public function deleteLinesForStatement($IS1_ID) {
		$statementLineModels = StatementLineModel::find()->where(["IS1_ID" => $IS1_ID])->all();
		$errors = [];

		foreach ($statementLineModels as $statementLineModel) {
			if ($statementLineModel->delete() === false) {
				$errors = array_merge($errors, $statementLineModel->getErrorSummary(true));
			}
		}
		
		return $errors;
	}
}