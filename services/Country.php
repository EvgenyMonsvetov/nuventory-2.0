<?php
namespace app\services;

use yii\base\Component;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\Country as CountryModel;
use app\models\User as UserModel;

class Country extends Component
{
    public function createCountry($data)
    {
        $errors = [];

        if (isset($data['CY1_ID']) && (int)$data['CY1_ID'] > 0) {
            $countryModel = CountryModel::findOne((int)$data['CY1_ID']);
            $countryModel->CY1_ID       = (int)$data['CY1_ID'];
        } else {
            $countryModel = new CountryModel();
        }

        $countryModel->CY1_NAME         = ucwords($data['CY1_NAME']);
        $countryModel->CY1_SHORT_CODE   = strtoupper($data['CY1_SHORT_CODE']);
        $countryModel->CY1_CREATED_BY   = Yii::$app->user->identity->US1_ID;
        $countryModel->CY1_MODIFIED_BY  = Yii::$app->user->identity->US1_ID;

        if( !$countryModel->save(true) ){
            $errors = $countryModel->getErrorSummary(true);
        }

        return $errors;
    }

    public function deleteCountry($id)
	{
	 $countryModel = new CountryModel();
	 $model = CountryModel::find()->where(["CY1_ID" => $id])->one();

	 if ($model->delete() === false) {
	     throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
	 }

	 Yii::$app->getResponse()->setStatusCode(204);
	}

	public function generateXls($params) {
		$filePrefix = 'Countries_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new CountryModel();
		$query = $model->getSearchQuery($params);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}
}