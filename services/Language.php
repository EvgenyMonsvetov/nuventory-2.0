<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 11/06/2018
 * Time: 15:42
 */

namespace app\services;

use yii\base\Component;
use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use app\models\Language as LanguageModel;

class Language extends Component
{
    public function createLanguage($data)
    {
        $errors = [];

        if (isset($data['LA1_ID']) && (int)$data['LA1_ID'] > 0) {
            $languageModel = LanguageModel::findOne((int)$data['LA1_ID']);
            $languageModel->LA1_ID = (int)$data['LA1_ID'];
        } else {
            $languageModel = new LanguageModel();
        }

        $languageModel->LA1_NAME         = ucwords($data['LA1_NAME']);
        $languageModel->LA1_SHORT_CODE   = strtoupper($data['LA1_SHORT_CODE']);
        $languageModel->LA1_CREATED_BY   = Yii::$app->user->identity->US1_ID;
        $languageModel->LA1_MODIFIED_BY  = Yii::$app->user->identity->US1_ID;
        $languageModel->LA1_DELETE_FLAG  = 0;

        if( !$languageModel->save( true ) ){
             $errors = $languageModel->getErrors();
        }

        return $errors;
    }

    public function deleteLanguage($id)
    {
         $languageModel = new LanguageModel();
         $model = LanguageModel::find()->where(["LA1_ID" => $id])->one();

         if ($model->delete() === false) {
             throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
         }

         Yii::$app->getResponse()->setStatusCode(204);
    }

	public function generateXls($params) {
		$filePrefix = 'Languages_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new LanguageModel();
		$query = $model->getQuery($params);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}
}