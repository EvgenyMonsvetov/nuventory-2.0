<?php

namespace app\services;

use app\models\Inventory;
use app\models\JobInvoiceLine;
use app\models\MasterDataLocation;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yii;
use yii\base\Component;
use app\models\JobTicket as JobTicketModel;
use app\services\JobLine as JobLineService;

class JobTicket extends Component
{
    public function create($order)
    {
        $errors = [];
        $JT1_ID = null;
        if(empty($order['CU1_ID']) || !$order['CU1_ID']){
            $errors[] = 'Customer is not selected!';
        }else{
            $transaction = Yii::$app->db->beginTransaction();
            try{

                $orderTotal = 0;
                foreach($order['parts'] as $part){
                    $unitPrice = $part["MD1_UNIT_PRICE"];
                    $orderQty = $part["MD1_ORDER_QTY"];
                    $orderTotal += $unitPrice * $orderQty;
                }

                $user = Yii::$app->user->getIdentity();

                $jobTicketModel = new \app\models\JobTicket();
                $jobTicketModel->load([
                    "CO1_ID" => $user->CO1_ID,
                    "LO1_ID" => $order['LO1_ID'],
                    "CU1_ID" => $order['CU1_ID'],
                    "VD1_ID" => $order['VD1_ID'],
                    "JT1_NUMBER" => $order['JT1_NUMBER'],
                    "JT1_STATUS" => \app\models\JobTicket::ORDER_STATUS_OPEN,
                    "JT1_TOTAL_VALUE" => $orderTotal,
                    "JT1_COMMENT" => $order['JT1_COMMENT']
                ], '');

                if (!$jobTicketModel->save(true)) {
                    $errors = $jobTicketModel->getErrorSummary(true);
                    throw new \Exception(join("\n", $errors));
                } else {
                    $JT1_ID = $jobTicketModel->JT1_ID;

	                $IJ1_NUMBER = \app\models\JobInvoice::find()->max('IJ1_NUMBER');
	                $jobInvoiceModel = new \app\models\JobInvoice();
	                $jobInvoiceModel->load([
	                	'JT1_ID' => $JT1_ID,
		                'CO1_ID' => $user->CO1_ID,
		                'US1_ID' => $user->US1_ID,
		                'LO1_ID' => $order['LO1_ID'],
		                'VD1_ID' => $order['VD1_ID'],
		                'IJ1_NUMBER' => strval(intval($IJ1_NUMBER) + 1 + intval($order['VD1_ID'])),
		                'IJ1_STATUS' => 0,
		                'IJ1_PRINT_FLAG' => 0,
		                'IJ1_TOTAL_VALUE' => $orderTotal,
		                'IJ1_COMMENT' => $order['JT1_COMMENT']
	                ], '');

	                if (!$jobInvoiceModel->save(true)) {
		                $errors = $jobInvoiceModel->getErrorSummary(true);
		                throw new \Exception($errors);
	                } else {
		                foreach($order['parts'] as $index => $part){
			                $index++;
			                $JT2_TOTAL_VALUE = $part["MD1_UNIT_PRICE"] * $part["MD1_ORDER_QTY"];
			                $jobLineModel = new \app\models\JobLine();
			                $jobLineModel->load([
				                "JT1_ID" => $JT1_ID,
				                "JT2_LINE" => $index,
				                "JT2_STATUS" => \app\models\JobTicket::ORDER_STATUS_OPEN,
				                "JT2_ORDER_QTY" => $part['MD1_ORDER_QTY'],
				                "MD1_ID" => $part['MD1_ID'],
				                "CO1_ID" => $user->CO1_ID,
				                "LO1_ID" => $order['LO1_ID'],
				                "VD1_ID" => $order['VD1_ID'],
				                "CA1_ID" => $part['CA1_ID'],
				                "GL1_ID" => $part['GL1_ID'],
				                "UM1_ID" => isset($part['UM1_ID']) ? $part['UM1_ID'] : 0,
				                "TE1_ID" => isset($part['TE1_ID']) ? $part['TE1_ID'] : null,
				                "JT2_PART_NUMBER" => $part['MD1_PART_NUMBER'],
				                "JT2_DESC1" => $part['MD1_DESC1'],
				                "JT2_DESC2" => $part['MD1_DESC2'] ? $part['MD1_DESC2'] : "",
				                "JT2_TOTAL_VALUE" => $JT2_TOTAL_VALUE,
				                "JT2_UNIT_PRICE" => $part["MD1_UNIT_PRICE"],
			                ], '');

			                if(!$jobLineModel->save(true)){
				                $errors = $jobLineModel->getErrorSummary(true);
				                throw new \Exception($errors);
			                } else {
				                $jobInvoiceLineModel = new JobInvoiceLine();
				                $jobInvoiceLineModel->load(["JT1_ID" => $JT1_ID,
					                'IJ1_ID' => $jobInvoiceModel->IJ1_ID,
					                'JT2_ID' => $jobLineModel->JT2_ID,
					                'IJ2_LINE' => $index,
					                'IJ2_STATUS' => 0,
					                'IJ2_INVOICE_QTY' => $part['MD1_ORDER_QTY'],
					                'MD1_ID' => $part['MD1_ID'],
					                'CA1_ID' => $part['CA1_ID'],
					                'GL1_ID' => $part['GL1_ID'],
					                'GL1_ID' => $part['GL1_ID'],
					                'CO1_ID' => $user->CO1_ID,
					                'IJ2_PART_NUMBER' => $part['MD1_PART_NUMBER'],
					                'IJ2_DESC1' => $part['MD1_DESC1'] ? $part['MD1_DESC1'] : '',
					                'IJ2_DESC2' => $part['MD1_DESC2'] ? $part['MD1_DESC2'] : '',
					                'UM1_ID' => isset($part['UM1_ID']) ? $part['UM1_ID'] : 0,
					                'IJ2_UNIT_PRICE' => $part['MD1_UNIT_PRICE'] / (1+0.01*(intval($part['MD1_MARKUP']) > 0 ? intval($part['MD1_MARKUP']) : 0)),
					                'IJ2_SELL_PRICE' => $part['MD1_UNIT_PRICE'],
					                'LO1_ID' => $user->LO1_ID,
									'IJ2_TOTAL_VALUE' => $JT2_TOTAL_VALUE / (1+0.01*(intval($part['MD1_MARKUP']) > 0 ? intval($part['MD1_MARKUP']) : 0)),
									'IJ2_TOTAL_SELL' => $JT2_TOTAL_VALUE
				                ], '');

				                if (!$jobInvoiceLineModel->save(true)) {
				                	return 1;
					                $errors = $jobInvoiceLineModel->getErrorSummary(true);
					                throw new \Exception($errors);
				                }
			                }

			                $inventory = new Inventory();
			                $inventory->addJobTicketFromOrder( $JT1_ID );

			                foreach( $order['parts'] as $index=>$part){

				                $mdLocationQuery = MasterDataLocation::find()->alias('m')
					                ->select(['m.*'])
					                ->leftJoin("md1_master_data  as m1", "m1.md1_id=m.md1_id")
					                ->leftJoin("ca1_category  as c", "c.ca1_id=m1.ca1_id")
					                ->andFilterWhere(['=', 'm.MD1_ID', $part['MD1_ID']])
					                ->andFilterWhere(['=', 'm.LO1_ID', $jobTicketModel->LO1_ID])
					                ->andFilterWhere(['=', 'coalesce(m.md2_no_inventory,0)', 0])
					                ->andFilterWhere(['=', 'c.ca1_no_inventory', 0]);
				                $mdLocation = $mdLocationQuery->one();

				                if($mdLocation){
					                $newQty       = $part["MD2_AVAILABLE_QTY"] - $part["MD1_ORDER_QTY"];
					                $newOnHandQty = $part['MD2_ON_HAND_QTY']   - $part['MD1_ORDER_QTY'];

					                $mdLocation->MD2_AVAILABLE_QTY = floor($newQty < 0 ? 0 : $newQty);
					                $mdLocation->MD2_ON_HAND_QTY   = floor($newOnHandQty < 0 ? 0 : $newOnHandQty);

					                if(!$mdLocation->save( true )){
						                throw new Exception($mdLocation->getErrorSummary());
					                }
				                }
			                }
		                }
	                }
                }

                $transaction->commit();
            }catch(\Exception $e){
                $errors[] = $e->getMessage();
                $transaction->rollBack();
            }catch(\Throwable $e){
                $errors[] = $e->getMessage();
                $transaction->rollBack();
            }
        }

        return [
            'success' => !$errors,
            'errors' => $errors,
            'PO1_ID' => $JT1_ID
        ];
    }

    public function generateXls($params) {
        $filePrefix = 'JobTickets_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new JobTicketModel();
	    $query = $model->getQuery($params);

	    $xlsService = new XLSService();

	    return $xlsService->generateXls($params, $query, $filename);
    }

	public function updateTotals($JT1_ID) {
		$jobLines = \app\models\JobLine::find()->where(["JT1_ID" => $JT1_ID, 'JT2_DELETE_FLAG' => 0])->all();
		$jobTicketTotals = 0;

		foreach ($jobLines as $jobLine) {
			$jobTicketTotals += $jobLine['JT2_TOTAL_VALUE'];
		}

		$jobTicket = \app\models\JobTicket::findOne([
			'JT1_ID' => $JT1_ID,
			'JT1_DELETE_FLAG' => 0
		]);

		$jobTicket->JT1_TOTAL_VALUE = $jobTicketTotals;
		$errors = [];
		if (!$jobTicket->save( true )) {
			$errors[] = $jobTicket->getErrorSummary(true);
		}

		return [
			'success' => !$errors,
			'errors'  => $errors
		];
	}
}