<?php

namespace app\services;

use app\models\File as FileModel;
use Yii;
use yii\base\Component;
use app\models\MasterVendor as MasterVendorModel;
use app\models\MasterData as MasterDataModel;
use app\components\ZipUpload as ZipUploadModel;
use yii\helpers\FileHelper;
use yii\db\Exception;


class MasterVendor extends Component
{
    public function createMasterVendor($data)
    {
        $errors = [];

        $data = Yii::$app->request->post();

        if (isset($data['VD0_ID']) && (int)$data['VD0_ID'] > 0) {
            $model = MasterVendorModel::findOne((int)$data['VD0_ID']);
            $model->VD0_ID = (int)$data['VD0_ID'];
            $data['VD0_CREATED_ON'] = $model->VD0_CREATED_ON;
            $data['VD0_MODIFIED_ON'] = $model->VD0_MODIFIED_ON;
        } else {
            unset($data['VD0_ID']);
            $model = new MasterVendorModel();
        }

        if ( !($model->load($data, '') && $model->save( true )) ){
            $errors = $model->getErrorSummary(true);
        }

        return $errors;
    }

    public function deleteMasterVendor($id)
    {
        $model = MasterVendorModel::find()->where(["VD0_ID" => $id])->one();

        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

	public function deleteMasterVendorParts( $params )
	{
		$masterData = new MasterDataModel();
		$masterCatalog = new \app\models\MasterCatalog();
		$errors = [];

		$transaction = Yii::$app->db->beginTransaction();
		try {
			$masterData->unlinkMasterVendorParts($params);
			$masterCatalog->deleteMasterVendorParts($params);
			$transaction->commit();
		} catch (Exception $e) {
			$errors[] = $e->getMessage();
			$transaction->rollBack();
		}

		return [
			'data' => [
				'success' => !$errors,
				'errors' => $errors
			]
		];
	}

	public function deleteAllImages( $params )
	{
		$masterData = new MasterDataModel();
		$errors = [];

		$transaction = Yii::$app->db->beginTransaction();
		try {
			$params['DELETE_IMAGES'] = true;
			$masterDatas = $masterData->getMasterVendorParts($params);
			foreach ($masterDatas as $masterData) {
				$fileModel = FileModel::findOne(['FI1_ID' => $masterData['FI1_ID']]);
				if ($fileModel) {
					$fileModel->delete();
				}
			}

			$transaction->commit();
		} catch (Exception $e) {
			$errors[] = $e->getMessage();
			$transaction->rollBack();
		}

		return [
			'data' => [
				'success' => !$errors,
				'errors' => $errors
			]
		];
	}

	public function uploadArchiveImages( $params, $zipFile )
	{
		$errors = [];

		$uploadModel = new ZipUploadModel();
		$uploadModel->fileKey = $zipFile;
		$uploadModel->uploadZipImages();

		$filePath = Yii::getAlias('@uploadImagesArchiveFolder');

		$zip = new \ZipArchive();
		$zipped = $zip->open($filePath . DIRECTORY_SEPARATOR . $zipFile->name);
		$zipFolderPath = Yii::getAlias('@uploadImagesArchiveFolder') . DIRECTORY_SEPARATOR . str_replace('.zip', '', $zipFile->name) . DIRECTORY_SEPARATOR;

		if ($zipped) {
			$extract = $zip->extractTo($zipFolderPath);

			//if unzipped unsuccesfully then show the error message
			if (!$extract) {
				$errors[] = "Not able to extract zip file " . $zipFile->name;
			}

			//close the zip
			$zip->close();
		} else {
			$errors[] = "Can not open zip file " . $zipFile->name;
		}

		// remove uploaded zip file
		FileHelper::unlink(Yii::getAlias('@uploadImagesArchiveFolder') . DIRECTORY_SEPARATOR . $zipFile->name );

		if (!$errors) {
			$imageFiles = scandir($zipFolderPath);
			$masterData = new MasterDataModel();
			$masterDatas = $masterData->getMasterVendorParts($params);
			$imagesFolderPath = Yii::getAlias('@partsImagesPath') . DIRECTORY_SEPARATOR . Yii::$app->params['imagesFolderName'];
			$folders = scandir($imagesFolderPath);

			$transaction = Yii::$app->db->beginTransaction();
			try {
				foreach ($masterDatas as $masterData) {
					$partNumber = $masterData['MD1_PART_NUMBER'];
					$searchingImageName = $partNumber . ".";

					foreach ($imageFiles as $imageFile) {
						if (strpos($imageFile, $searchingImageName) !== false && strpos($imageFile, $searchingImageName) == 0) {
							// check if image exist
							$foundPartImagePath = "";
							foreach ($folders as $folder) {
								if (strpos($folder, $searchingImageName) !== false && strpos($folder, $searchingImageName) == 0) {
									$foundPartImagePath = Yii::$app->params['imagesFolderName'] . DIRECTORY_SEPARATOR . $folder;
									break;
								}

								if (strpos($folder, ".") === false) {
									$filesDir = $imagesFolderPath . DIRECTORY_SEPARATOR . $folder;
									$files = scandir($filesDir);

									$found = false;
									foreach ($files as $file) {
										if (strpos($file, $searchingImageName) !== false && strpos($file, $searchingImageName) == 0) {
											$foundPartImagePath = Yii::$app->params['imagesFolderName'] . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $file;
											$found = true;
											break;
										}
									}
									if ($found) {
										break;
									}
								}
							}

							// if found image replace it
							if ($foundPartImagePath && strlen($foundPartImagePath) > 0) {
								$result = $uploadModel->copyImageToAppImageFolder($zipFolderPath.$imageFile, $imageFile, $foundPartImagePath);
							} else {
								$result = $uploadModel->copyImageToAppImageFolder($zipFolderPath.$imageFile, $imageFile);
							}

							if ($result && !$result['data']['errors'] ) {
								$fileModel = null;
								if ($foundPartImagePath) {
									$fileModel = FileModel::find()->where(["FI1_FILE_NAME" => $imageFile])->one();
								}

								if (!isset($fileModel) || !$fileModel) {
									$fileModel = new FileModel();
								}

								$fileModel->FI1_FILE_NAME = $imageFile;
								$fileModel->FI1_FILE_PATH = $result['data']['path'];
								$fileModel->FI1_FILE_SIZE = 0;
								$fileModel->FI1_FILE_TYPE = strpos(strtolower($imageFile), ".png") !== false || strpos(strtolower($imageFile), ".jpg") !== false || strpos(strtolower($imageFile), ".gif") !== false ? 1 : 2;

								if ( $fileModel->save() ) {
									$masterDataModel = MasterDataModel::findOne(['MD1_ID' => $masterData['MD1_ID']]);
									$masterDataModel->FI1_ID = $fileModel->FI1_ID;
									if (!$masterDataModel->save(true)) {
										throw new Exception('', $masterDataModel->getErrors(), MasterDataModel::$MODEL_ERROR);
									}
								} else {
									throw new Exception('', $fileModel->getErrors(), MasterDataModel::$MODEL_ERROR);
								}
							} else {
								throw new Exception('', $result['data']['errors'][0], MasterDataModel::$MODEL_ERROR);
							}

							//remove found image file from search
							$imageFiles = array_diff($imageFiles, [$imageFile]);
							break;
						}
					}
				}

				$transaction->commit();
			} catch (Exception $e) {
				if ($e->getCode() == MasterDataModel::$MODEL_ERROR) {
					$errors = $e->errorInfo;
				} else {
					$errors[] = $e->getMessage();
				}
				$transaction->rollBack();
			}
		}

		// remove extracted image files folder
		FileHelper::removeDirectory($filePath . DIRECTORY_SEPARATOR . str_replace('.zip', '', $zipFile->name) . DIRECTORY_SEPARATOR);

		return [
			'data' => [
				'success' => !$errors,
				'errors' => $errors
			]
		];
	}

	public function generateXls($params) {
		$filePrefix = 'MasterVendors_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new MasterVendorModel();
		$query = $model->getQuery($params);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}
}