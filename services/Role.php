<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 11/06/2018
 * Time: 15:42
 */

namespace app\services;

use yii\base\Component;
use Yii;
use yii\helpers\ArrayHelper;

class Role extends Component
{
    public function getRole( $roleId )
    {
        $allPermissions  = Yii::$app->getAuthManager()->getPermissions();
        $rolePermissions = array_keys(ArrayHelper::toArray(Yii::$app->getAuthManager()->getPermissionsByRole($roleId)));

        $permissions = [];
        foreach($allPermissions as $permission){
            $permissions[] = [
                'name'          => $permission->name,
                'description'   => $permission->description,
                'hasPermission' => in_array($permission->name, $rolePermissions) ? true : false
            ];
        }

        $role = ArrayHelper::toArray(Yii::$app->getAuthManager()->getRole($roleId));
        $role['permissions'] = $permissions;

        return $role;
    }
}