<?php

namespace app\services;

use app\models\InvoiceLine;
use app\models\StatementLine;
use app\pdfs\Pdf;
use PhpOffice\PhpSpreadsheet\Calculation\Exception;
use Yii;
use yii\base\Component;
use app\models\Statement as StatementModel;
use app\models\StatementLine as StatementLineModel;
use app\services\StatementLine as StatementLineService;


class Statement extends Component
{
	public function deleteMultiple($IS1_IDs) {
		$errors = [];

		$transaction = Yii::$app->db->beginTransaction();
		try {
			$statementLineService = new StatementLineService();
			foreach( $IS1_IDs as $IS1_ID ){
				$statementModel = StatementModel::findOne(['IS1_ID' => $IS1_ID]);
				if ($statementModel->delete() === false) {
					$errors = array_merge($errors, $statementModel->getErrorSummary(true));
				} else {
					$linesDeleteResult = $statementLineService->deleteLinesForStatement($IS1_ID);
					if ($linesDeleteResult) {
						$errors = array_merge($errors, $linesDeleteResult);
					}
				}
			}

			$transaction->commit();
		} catch (Exception $e) {
			$errors[] = $e->getMessage();
			$transaction->rollBack();
		}

		return [
			'data' => [
				'success' => !$errors,
				'errors' => $errors
			]
		];
	}
	
    public function generateXls($params) {
        $filePrefix = 'Statements_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new StatementModel();
        $query = $model->getQuery($params);

        $xlsService = new XLSService();

        return $xlsService->generateXls($params, $query, $filename);
    }

	public function generateStatements($params) {

	    $errors  = [];
		$IS1_IDs = [];

		$invoiceModel = new \app\models\Invoice();
		$invoices = $invoiceModel->getInvoicesWithoutStatements($params['from'], $params['to']);

		$statementModel = new StatementModel();
		$invoiceNumber = $statementModel->getInvoiceNumber();

		if ( $invoices ) {
			$headerPool = [];
			foreach($invoices as $invoice) {
				$contained = false;
				for ($i = 0; $i < sizeof($headerPool); $i++) {
					if ((int)$invoice["LO1_FROM_ID"] === (int)$headerPool[$i]["LO1_FROM_ID"] &&
                        (int)$invoice["LO1_TO_ID"]   === (int)$headerPool[$i]["LO1_TO_ID"]) {
						$headerPool[$i]["IS1_TOTAL_VALUE"] +=  floatval($invoice["IN1_TOTAL_VALUE"]);
						$headerPool[$i]['invoices'][] = $invoice;
						$contained = true;
						break;
					}
				}

				if (!$contained) {

					$newSummaryHeader = [];
					$newSummaryHeader["invoices"]        = [];
					$newSummaryHeader["invoices"][]      = $invoice;
					$newSummaryHeader["CO1_ID"]          = $invoice["CO1_ID"];
					$newSummaryHeader["LO1_FROM_ID"]     = $invoice["LO1_FROM_ID"];
					$newSummaryHeader["LO1_TO_ID"]       = $invoice["LO1_TO_ID"];
					$newSummaryHeader["IS1_TOTAL_VALUE"] = floatval($invoice["IN1_TOTAL_VALUE"]);
					$headerPool[] = $newSummaryHeader;
				}
			}

			$transaction = Yii::$app->db->beginTransaction();

            $dateString = date("m-Y", strtotime($params["to"]));
            foreach($headerPool as $summaryHeader){
                $statementModel = new StatementModel();

                $statementModel->load([
                    'CO1_ID' => (int)$summaryHeader["CO1_ID"],
                    'LO1_FROM_ID' => (int)$summaryHeader["LO1_FROM_ID"],
                    'LO1_TO_ID' => (int)$summaryHeader["LO1_TO_ID"],
                    'IS1_NUMBER' => $invoiceNumber,
                    'IS1_STATUS' => StatementModel::IS1_STATUS_OPEN,
                    'IS1_TOTAL_VALUE' => $summaryHeader["IS1_TOTAL_VALUE"],
                    'IS1_COMMENT' => $dateString,
                    'IS1_DELETE_FLAG' => 0
                ], '');

                if(!$statementModel->save(false)){
                    $errors = array_merge($errors, $statementModel->getErrors());
                }else{

                    $IS1_IDs[] = $statementModel->IS1_ID;

                    foreach($summaryHeader['invoices'] as $index => $invoiceModel){
                        $statementLineModel = new StatementLineModel();

                        $statementLineModel->load([
                            'IS1_ID' => $statementModel->IS1_ID,
                            'IN1_ID' => $invoiceModel->IN1_ID,
                            'IS2_LINE' => (++$index),
                            'IS2_STATUS' => StatementLine::IS2_STATUS_OPEN,
                            'IS2_INVOICED_AMOUNT' => floatval($invoiceModel->IN1_TOTAL_VALUE),
                            'CO1_ID' => $invoiceModel->CO1_ID,
                            'IS2_DELETE_FLAG' => '0'
                        ], '');

                        if(!$statementLineModel->save(true)){
                            $errors = array_merge($errors, $statementLineModel->getErrors());
                        }else{
                            $invoiceModel->IN1_STATUS = 1;
                            if(!$invoiceModel->save(true)){
                                $errors = array_merge($errors, $invoiceModel->getErrors());
                            }

                            // this update status needs to be rewrites through SQL query
                            $invoiceLineModel = new InvoiceLine();
                            $invoiceLines = $invoiceLineModel->getInvoiceLines(['IN1_ID' => $invoiceModel['IN1_ID']]);

                            foreach($invoiceLines as $invoiceLine){
                                $invoiceLine->IN2_STATUS = 1;
                                if(!$invoiceLine->save(true)){
                                    $errors = array_merge($errors, $invoiceLine->getErrors());
                                }
                            }
                        }
                    }
                    $invoiceNumber++;
                }
            }
            if($errors){
                $transaction->rollBack();
            }else{
                $transaction->commit();
            }
		} else {
		    $errors[] = 'There no invoices in selected date range';
		}

		return [
            'success' => !$errors,
            'errors'  => $errors,
            'IS1_IDs' => $IS1_IDs
        ];
	}

    public function send($IS1_IDs, $hours_offset = 0)
    {
        $locationsEmails = [];
        foreach ($IS1_IDs as $IS1_ID) {
            $statement = \app\models\Statement::findOne(['IS1_ID' => $IS1_ID]);
            $location = \app\models\Location::findOne(['LO1_ID' => $statement->LO1_TO_ID]);
            if (!isset($locationsEmails[$location->LO1_ID])) {
                $locationsEmails[$location->LO1_ID] = [
                    'location'   => $location,
                    'statements' => []
                ];
            }
            $locationsEmails[$location->LO1_ID]['statements'][] = $statement;
        }

        $missingEmails = [];
        $existsEmails = [];

        foreach ($locationsEmails as $LO1_ID => $data) {
            /**
             * var \app\models\Location
             */
            $location = $data['location'];
            if ($location->LO1_EMAILS) {
                $existsEmails[] = $LO1_ID;
                $pdfFileName = 'Statements for ' . $location->LO1_NAME;
                $mailer = Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['mailer']['sender'])
                    ->setTo(explode(',', $location->LO1_EMAILS ))
                    ->setSubject($pdfFileName)
                    ->setTextBody($pdfFileName . ' are attached.');

                $IS1_IDs = array_map( function(\app\models\Statement $statement){ return $statement->IS1_ID; }, $data['statements']);
                $pdfService = new Pdf();
                $pdf = $pdfService->printStatement(['IS1_IDs' => join(",", $IS1_IDs), 'perPage' => '50', 'page' => '0', 'fileName' => $pdfFileName . '.pdf', 'hours_offset' => $hours_offset]);
                $statementContent = $pdf->Output('', 'S');
                $mailer->attachContent($statementContent, [
                    'fileName' => $pdfFileName . '.pdf',
                    'contentType'=>'application/pdf'
                ]);
                $mailer->send();
            } else {
                $missingEmails[] = $LO1_ID;
            }
        }

        return ['missing'=>$missingEmails,'sent'=>$existsEmails];
    }
}