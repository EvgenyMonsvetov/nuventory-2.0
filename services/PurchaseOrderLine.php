<?php

namespace app\services;

use yii\base\Component;
use app\models\PurchaseOrderLine as PurchaseOrderLineModel;

class PurchaseOrderLine extends Component
{
    public function delete($id)
    {
        $errors = [];
        $purchaseOrderLineModel = PurchaseOrderLineModel::find()->where(["PO2_ID" => $id])->one();
	    $transaction = \Yii::$app->db->beginTransaction();

        try{
            $PO1_ID = $purchaseOrderLineModel->PO1_ID;

            $purchaseOrderLineModel->PO2_DELETE_FLAG = 1;
	        $purchaseOrderLineModel->save(false);

	        $purchaseOrderService = new PurchaseOrder();
	        $purchaseOrderService->recalculatePO($PO1_ID);

            $transaction->commit();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }

        return [
            'success' => !$errors,
            'errors'  => $errors
        ];
    }
}