<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 17/08/2018
 * Time: 17:10
 */

namespace app\services;

use yii\base\Component;
use yii\helpers\VarDumper;
use app\models\Customer as CustomerModel;
use app\models\Vendor as VendorModel;
use app\models\Company as CompanyModel;
use app\models\User as UserModel;

class User extends Component
{
    /**
     * @param $companyName
     * @return \app\models\Company
     */
    public function getAccessTree( $userId )
    {
        $errors = [];

        $companies = CompanyModel::find()->from('co1_company')->select(['CO1_NAME', 'CO1_ID'])
            ->orderBy(['CO1_NAME' => SORT_ASC])->asArray()->all();

        $customers = CustomerModel::find()->from('cu1_customer')->alias('cu')
            ->select(['cu.CU1_ID', 'cu.CU1_NAME', 'cu.CO1_ID'])
            ->orderBy(["cu.CU1_NAME" => SORT_ASC])
            ->asArray()->all();


        $vendors = VendorModel::find()->from('vd1_vendor')->alias('vd')
            ->select(['vd.VD1_ID', 'vd.VD1_NAME', 'vd.CO1_ID'])
            ->orderBy(["vd.VD1_NAME" => SORT_ASC])
            ->asArray()->all();

        $accessList = \app\models\AccessList::find()->where(['user_id' => $userId])->asArray()->all();

        foreach($companies as &$company){

            $company['access']    = AccessList::ACCESS_NO_ACCESS;

            $companyCustomers = [];
            foreach( $customers as &$customer ){
                if($customer['CO1_ID'] == $company['CO1_ID']){
                    $customer['hasAccess'] = false;
                    $companyCustomers[$customer['CU1_ID']] = $customer;
                }
            }

            $companyVendors = [];
            foreach( $vendors as &$vendor ){
                if($vendor['CO1_ID'] == $company['CO1_ID']){
                    $vendor['hasAccess'] = false;
                    $companyVendors[$vendor['VD1_ID']] = $vendor;
                }
            }

            foreach( $accessList as $accessItem ){
                if(   $company['CO1_ID'] == $accessItem['CO1_ID']
                   && $accessItem['AL1_OBJECT_TYPE'] != \app\models\AccessList::OBJ_TYPE_COMPANY){
                    if( $accessItem['AL1_OBJECT_TYPE'] == \app\models\AccessList::OBJ_TYPE_CUSTOMER ){
                        $companyCustomers[$accessItem['AL1_OBJECT_ID']]['hasAccess'] = true;
                    }else{
                        $companyVendors[$accessItem['AL1_OBJECT_ID']]['hasAccess'] = true;
                    }
                    $company['access'] = AccessList::ACCESS_RESTRICTED;
                }

                if(  $company['access'] != AccessList::ACCESS_RESTRICTED
                  && $company['CO1_ID'] == $accessItem['AL1_OBJECT_ID']
                  && $accessItem['AL1_OBJECT_TYPE'] == \app\models\AccessList::OBJ_TYPE_COMPANY ){

                    $company['access'] = AccessList::ACCESS_FULL;
                }
            }

            $company['customers'] = array_values($companyCustomers);
            $company['vendors']   = array_values($companyVendors);
        }

        return [
            'data'    => $companies,
            'success' => !$errors,
            'errors'  => $errors
        ];
    }

	public function generateXls($params) {
		$filePrefix = 'Users_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new UserModel();
		$query = $model->getSearchQuery($params);
		$query->initScope();

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}
}