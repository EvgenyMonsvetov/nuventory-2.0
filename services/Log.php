<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 17/07/2018
 * Time: 15:40
 */
namespace app\services;

use yii\base\Component;
use app\models\Log as LogModel;
use yii\db\Exception;
use yii\helpers\VarDumper;
use Yii;

class Log extends Component
{
    /**
     * @param $companyName
     * @return \app\models\Company
     */
    public function import( $data )
    {
        $startImport = microtime(true);

        $errors = [];

        $companyService = new \app\services\Company();
        $company = $companyService->registerCompany( $data['CompanyName'] );

        $logsAdded = 0;
        if(!$company->hasErrors()){
            //$result = $this->_importTestData( $data['Logs'], $company->CO1_ID );
            $result = $this->_importRealData( $data['Logs'], $company->CO1_ID );
            $logsAdded = $result['logsAdded'];
            $errors = $result['errors'];
        }

        $importTime = microtime(true) - $startImport;

        return [
            'success'    => !$errors,
            'errors'     => $errors,
            'importTime' => sprintf( "%.2f s", $importTime),
            'logsAdded'  => $logsAdded
        ];
    }

    private function _prepareLogs( $logs, $CO1_ID )
    {
        $date = date('Y-m-d');
        $uploadTimestamp = round(microtime(true) * 1000);;
        $values = [];

        foreach( $logs as $log ){
            $item = [];
            $item['DATE']   = $date;
            $item['ED1_ID'] = (int)$log['ED1_ID'];
            $item['LOG_ID'] = (int)$log['LOG_ID'];
            //$item['LOG_ID'] = rand(10000, 100000);
            $item['LOG_DESCRIPTION'] = $log['LOG_DESCRIPTION'];
            $item['LOG_FILENAME'] = $log['LOG_FILENAME'];
            $item['LOG_P21'] = (int)$log['LOG_P21'];
            $item['LOG_CHECKED'] = (int)$log['LOG_CHECKED'];
            $item['LOG_FILE_TYPE'] = $log['LOG_FILE_TYPE'];

            $modifiedOn = new \DateTime($log['LOG_UPDATED_ON']);
            $item['LOG_UPDATED_ON'] = $modifiedOn->format('Y-m-d H:i:s');
            $item['LOG_UPDATED_BY'] = (int)$log['LOG_UPDATED_BY'];

            $item['LOG_UPLOAD_TIMESTAMP'] = $uploadTimestamp;
            $item['CO1_ID']               = $CO1_ID;

            $values[] = $item;
        }

        return $values;
    }

    private function _batchInsert( $values )
    {
        $error = null;
        $sqlCommand = Yii::$app->clickhouse->createCommand();
        try{
            $sqlCommand->batchInsert(
                LogModel::tableName(), array_keys($values[0]), $values
            )->execute();
        }catch(Exception $e){
            $error = $e->getMessage() . '; sql = ' . $sqlCommand->getRawSql();
        }

        return $error;
    }

    private function _importTestData( $logs, $CO1_ID )
    {
        $errors = [];
        $logsAdded = 0;

        for($j=0; $j<100; $j++){
            $values = [];
            for($i = 0; $i < 100; $i++){
                $values = array_merge( $values, $this->_prepareLogs($logs, $CO1_ID));
            }

            if(!$errors){
                $error = $this->_batchInsert($values);
                if($error){
                    $errors[] = $error;
                }else{
                    $logsAdded += count($values);
                }
            }
        }

        return [
            'logsAdded' => $logsAdded,
            'errors'    => $errors
        ];
    }

    private function _importRealData( $logs, $CO1_ID )
    {
        $errors = [];
        $logsAdded = 0;

        $values = $this->_prepareLogs($logs, $CO1_ID);

        $error = $this->_batchInsert($values);
        if($error){
            $errors[] = $error;
        }else{
            $logsAdded += count($values);
        }

        return [
            'logsAdded' => $logsAdded,
            'errors'    => $errors
        ];
    }
}