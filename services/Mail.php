<?php

namespace app\services;

use Yii;
use yii\base\Component;

class Mail extends Component
{
	public function sendMail($params) {
		$toEmail = $params['EMAIL'];
		$subject = $params['SUBJECT'];
		$message = $params['MESSAGE'];

        return Yii::$app->mailer->compose()
            ->setFrom( Yii::$app->params['mailer']['sender'] )
            ->setTo($toEmail)
            ->setSubject($subject)
            ->setTextBody($message)
            ->send();
	}

}