<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\Category as CategoryModel;

class Category extends Component
{
    public function generateXls($params) {
        $filePrefix = 'Categories_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new CategoryModel();
	    $query = $model->getSearchQuery($params);

	    $xlsService = new XLSService();
	    return $xlsService->generateXls($params, $query, $filename);
    }
}