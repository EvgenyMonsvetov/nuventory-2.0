<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 09/07/2018
 * Time: 10:13
 */

namespace app\services;

use yii\base\Component;
use Yii;
use app\models\Company as CompanyModel;

class Company extends Component
{
    /**
     * @param $companyName
     * @return \app\models\Company
     */
    public function registerCompany( $companyName )
    {
        $companyName = trim($companyName);

        $company = \app\models\Company::find()->from('co1_company')
            ->where(['LOWER(CO1_NAME)' => strtolower($companyName)])->one();

        if(!$company){
            $company = new \app\models\Company();
            $company->CO1_NAME = $companyName;
            $company->CO1_SHOW_DEFAULT = 'X';
        }

        $company->CO1_LAST_SYNCH = date('Y-m-d h:i:s');
        $company->CO1_IP = Yii::$app->getRequest()->getUserIP();
        $company->save();

        return $company;
    }

    public function getAll( $incognitoMode = false )
    {
        $companies = CompanyModel::find()->select(['CO1_NAME', 'CO1_ID'])
            ->orderBy(['CO1_NAME' => SORT_ASC])->all();

        if($incognitoMode){
            foreach( $companies as $index => $company ){
                $company->CO1_NAME = 'Company'  . $company['CO1_ID'];
            }
        }

        return $companies;
    }

	public function generateXls($params) {
		$filePrefix = 'Companies_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new CompanyModel();
		$query = $model->getSearchQuery($params);

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, $query, $filename);
	}
}