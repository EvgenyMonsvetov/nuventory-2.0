<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\JobInvoice as JobInvoiceModel;

class JobInvoice extends Component
{
    public function deleteJobInvoice($id)
    {
        $model = JobInvoiceModel::find()->where(["IJ1_ID" => $id])->one();

        if ($model->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    public function generateXls($params) {
        $filePrefix = 'JobInvoices_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new JobInvoiceModel();
        $query = $model->getQuery($params);

        $xlsService = new XLSService();

        return $xlsService->generateXls($params, $query, $filename);
    }
}