<?php

namespace app\services;

use app\models\RawSalesDataStoreMonth as RawSalesDataStoreMonthModel;
use app\models\TargetEntry;
use app\models\Location as LocationModel;
use yii\base\Component;
use yii\data\ArrayDataProvider;

class RawSalesDataStoreMonth extends Component
{
	const FORMAT_NUMBER = 1;
	const FORMAT_PERCENTAGE = 2;
	const FORMAT_MONEY = 3;
	const FORMAT_TEXT = 4;

	public function getMonthlyChart($params, $attribute, $areaChart = false) {
		$startDate = !empty($params['startDate']) ? $params['startDate'] : '01/01/2015';
		$endDate   = !empty($params['endDate'])   ? $params['endDate']   : '12/31/2015';

		$locationModel = new LocationModel();
		$locationModels = $locationModel->getLocations($params);
		$locationCodes = [];
		foreach ($locationModels as $model) {
			array_push($locationCodes, $model['LO1_SHORT_CODE']);
		}

		$rawSalesDataStoreMonthModel = new RawSalesDataStoreMonthModel();
		$monthlyOverallSalesQuery = $rawSalesDataStoreMonthModel->getQuery([
		    'LO1_SHORT_CODEs' => implode(',', $locationCodes),
            'startDate' => $startDate,
            'endDate' => $endDate
        ]);

		$monthlyOverallSalesData = $monthlyOverallSalesQuery->all();
		if (!empty($monthlyOverallSalesData)) {
			$shopCodes = [];
			foreach ($monthlyOverallSalesData as $model) {
				$shopCodes[] = $model->r03_shop_code;
			}
			$shopCodes = array_values(array_unique($shopCodes));

			$categories = $this->getMonthsAndYearsFromDateRange($startDate, $endDate, "ASC");

			if ($areaChart) {
				if (count($categories) < 6) {//The chart does not display correctly unless there are 6 categories (months)
					$numOfMonths = ceil((6 - count($categories)) / 2);
					$startDate = date("Y-m-d", strtotime($startDate . ' -' . $numOfMonths . ' months'));

					$endDate = date('Y-m-d', strtotime("+" . $numOfMonths . " months", strtotime($endDate)));
					$categories = $this->getMonthsAndYearsFromDateRange($startDate, $endDate, "ASC");
				}
			}
			$series = [];

			foreach ($shopCodes as $shopCode) {
				$locationData = [];

				foreach ($categories as $category) {
					$match = null;

					foreach ($monthlyOverallSalesData as $model) {
						if ($shopCode == $model['r03_shop_code']) {
							if ($model['r03_short_month_and_year'] == $category) {
								$match = $model;
								break;
							}
						}
					}

					if (isset($match)) {
						if (!isset($locationData['name'])) {
							$locationData['name'] = $model['r03_shop_name'];
						}
						$locationData['data'][] = round($model->$attribute);
					} else {
						$locationData['data'][] = floatval(0);
					}
				}

				$series[] = $locationData;
			}

			$attributeType = self::getAttributeType($attribute);
			$targetModel = new TargetEntry();
			$shopTargets = $targetModel->getTargetEntries(implode(",", $locationCodes));
			$targetAttribute = $targetModel->getTargetAttribute($attribute);

			if (isset($shopTargets, $targetAttribute)) {
				if (isset($shopTargets->$targetAttribute)) {
					$target = $shopTargets->$targetAttribute;
				}
			}

			$result = [];
			$result['series'] = $series;
			$result['categories'] = $categories;
			$result['attributeType'] = isset ($attributeType['attributeType']) ? $attributeType['attributeType'] : '';
			$result['attributeTypeSymbol'] = isset ($attributeType['attributeTypeSymbol']) ? $attributeType['attributeTypeSymbol'] : '';
			$result['target'] = isset ($target) ? $target : '0';
			return $result;
		} else {
			return false;
		}
	}

	public function getPieChartDataInRange($params, $attribute)
	{
		if (empty($params['startDate']) || empty($params['endDate']))
			return false;
		$startDate = $params['startDate'];
		$endDate   = $params['endDate'];

		$locationModel = new LocationModel();
		$locationModels = $locationModel->getLocations($params);
		$locationCodes = [];
		foreach ($locationModels as $model) {
			array_push($locationCodes, $model['LO1_SHORT_CODE']);
		}

		$rawSalesDataStoreMonthModel = new RawSalesDataStoreMonthModel();
		$monthlyOverallSalesQuery = $rawSalesDataStoreMonthModel->getQuery([
			'LO1_SHORT_CODEs' => implode(',', $locationCodes),
			'startDate' => $startDate,
			'endDate' => $endDate
		]);

		$models = $monthlyOverallSalesQuery->all();

		$series = [];
		foreach ($locationCodes as $shop) {
			$tempResult = 0;
			$temp = [];
			foreach ($models as $model) {
				if ($shop == $model->r03_shop_code) {
					if (!isset($temp['name'])) {
						$temp["name"] = $model->r03_shop_name;
					}
					if (isset($model->$attribute)) {
						$tempResult += $model->$attribute;
					}
				}
			}

			if (isset($temp['name']) && $temp['name'] !== "") {
				$temp['y'] = round($tempResult);
				$series[] = $temp;
			}
		}

		return $series;
	}

	public function getAverageValueForCircleChart($params) {
		if (empty($params['startDate']) || empty($params['endDate']))
			return false;

		$locationModel = new LocationModel();
		$locationModels = $locationModel->getLocations($params);
		$locationCodes = [];
		foreach ($locationModels as $model) {
			array_push($locationCodes, $model['LO1_SHORT_CODE']);
		}

		$value = $this->getAverageValueInRange($locationCodes, $params['startDate'], $params['endDate'], $params['attribute_code']);
		$value = round($value);

		$result['series'] = [["y"=>$value], ["y"=>(100-$value)]];;
		$result['data'] = $this->getCirclePieData($params, $locationModels);

		return $result;
	}

	public function getCirclePieData($params, $locationModels) {
		$result = [];

		foreach($locationModels as $shop) {
			$value = $this->getAverageValueInRange([$shop['LO1_SHORT_CODE']], $params['startDate'], $params['endDate'], $params['attribute_code']);
			$tempArray = array();
			$tempArray['shopName'] = $shop['LO1_NAME'];
			$tempArray['value'] = round($value);
			$result[] = $tempArray;
		}

		return $result;
	}

	public function getAverageValueInRange($locationCodes, $startDate, $endDate, $attribute) {
		$rawSalesDataStoreMonthModel = new RawSalesDataStoreMonthModel();
		$monthlyOverallSalesQuery = $rawSalesDataStoreMonthModel->getQuery([
			'LO1_SHORT_CODEs' => implode(',', $locationCodes),
			'startDate' => $startDate,
			'endDate' => $endDate
		]);

		$models = $monthlyOverallSalesQuery->all();

		$result = 0;
		$count = 0;
		foreach ($models as $model) {
			if (isset($model->$attribute)) {
				$result += $model->$attribute;
				$count++;
			}
		}
		if ($count > 0) {
			$result = $result / $count;
		}
		return $result;
	}

	/**
	 * Returns the attribute type and corresponding symbol for a passed attribute
	 * @param string $attribute RawSalesDataStoreMonth Attribute to return type and symbol for
	 * @return array Containing the type and symbol for the passed attribute
	 */
	public static function getAttributeType($attribute)
	{
		if (
			$attribute == 'r03_gross_amount' ||
			$attribute == 'r03_body_labor_amount' ||
			$attribute == 'r03_refinish_amount' ||
			$attribute == 'r03_paint_amount' ||
			$attribute == 'r03_paint_materials_amount' ||
			$attribute == 'r03_body_materials_amount' ||
			$attribute == 'r03_hardener' ||
			$attribute == 'r03_paper' ||
			$attribute == 'r03_primer_and_sealer' ||
			$attribute == 'r03_fm_misc' ||
			$attribute == 'r03_tape' ||
			$attribute == 'r03_thinner' ||
			$attribute == 'r03_toner' ||
			$attribute == 'r03_fm_grand_total' ||
			$attribute == 'r03_ppg_products' ||
			$attribute == 'r03_misc' ||
			$attribute == 'r03_all_materials_total' ||
			$attribute == 'r03_solvent' ||
			$attribute == 'r03_rough_sand' ||
			$attribute == 'r03_fine_sand' ||
			$attribute == 'r03_equipment' ||
			$attribute == 'r03_mixing_supplies' ||
			$attribute == 'r03_filler' ||
			$attribute == 'r03_safety' ||
			$attribute == 'r03_detail' ||
			$attribute == 'r03_hardware' ||
			$attribute == 'r03_paint_and_materials_profit' ||
			$attribute == 'r03_parts_additional' ||
			$attribute == 'r03_materials_target' ||

			$attribute == 'r03_c_paint_materials_total' ||
			$attribute == 'r03_c_cost_per_paint_hour_total' ||
			$attribute == 'r03_c_cost_per_paint_hour_liquid' ||
			$attribute == 'r03_c_cost_per_paint_hour_allied' ||
			$attribute == 'r03_c_average_ro' ||
			$attribute == 'r03_c_paint_labor_per_ro' ||
			$attribute == 'r03_c_pm_cost_per_ro' ||
			$attribute == 'r03_c_clear_per_ro' ||
			$attribute == 'r03_c_toner_per_ro' ||
			$attribute == 'r03_c_allied_per_ro' ||
			$attribute == 'r03_c_paint_and_materials_sold_per_ro' ||
			$attribute == 'r03_c_misc_per_ro' ||
			$attribute == 'r03_c_materials_cost_vs_sales_p' ||
			$attribute == 'r03_c_paint_and_refinish_labor_total'
		) {
			$result['attributeType'] = 'Dollars';
			$result['attributeTypeSymbol'] = '$';
		} elseif (
			$attribute == 'r03_c_gp_percent' ||
			$attribute == 'r03_c_paint_sold_to_sales_p' ||
			$attribute == 'r03_c_paint_and_materials_sold_p' ||
			$attribute == 'r03_c_ppg_vs_all_grand_total_p' ||
			$attribute == 'r03_c_ppg_liq_vs_sales_p' ||
			$attribute == 'r03_c_materials_vs_paint_labor_p' ||
			$attribute == 'r03_c_ref_labor_vs_sales_p' ||
			$attribute == 'r03_c_paint_labor_vs_sales_p' ||
			$attribute == 'r03_c_ref_and_paint_labor_vs_sales_p' ||
			$attribute == 'r03_c_clear_vs_paint_labor_p' ||
			$attribute == 'r03_c_hdnr_vs_paint_labor_p' ||
			$attribute == 'r03_c_toner_vs_paint_labor_p' ||
			$attribute == 'r03_c_pmr_vs_paint_labor_p' ||
			$attribute == 'r03_c_tape_vs_paint_labor_p' ||
			$attribute == 'r03_c_thinner_vs_paint_labor_p' ||
			$attribute == 'r03_c_ppg_vs_paint_labor_p' ||
			$attribute == 'r03_c_ppg_liq_vs_paint_and_ref_labor_p' ||
			$attribute == 'r03_c_clear_per_ro_p' ||
			$attribute == 'r03_c_toner_per_ro_p' ||
			$attribute == 'r03_c_allied_per_ro_p' ||
			$attribute == 'r03_po_spent_vs_materials_target'
		) {
			$result['attributeType'] = 'Percent';
			$result['attributeTypeSymbol'] = '%';
		} else {
			$result['attributeType'] = 'Amount';
			$result['attributeTypeSymbol'] = '';
		}
		return $result;
	}

	private function getMonthsAndYearsFromDateRange($startDate, $endDate, $order)
	{
		$startDate = strtotime($startDate);
		$endDate = strtotime($endDate);

		$ASC_Month = $startDate;
		$DESC_Month = $endDate;
		$Y_Axis = Array();

		if ($order == 'DESC')//Big to small
		{
			while ($DESC_Month >= $startDate) {
				$Y_Axis[] = date('F-Y', $DESC_Month);
				$DESC_Month = strtotime(date('Y-m-d', $DESC_Month) . ' -1 month');
			}
			return $Y_Axis;
		} elseif ($order == 'ASC')//Small to big
		{
			while ($ASC_Month <= $endDate) {
				$Y_Axis[] = date('M y', $ASC_Month);
				$ASC_Month = strtotime(date('Y-m-d', $ASC_Month) . ' +1 month');
			}
			return $Y_Axis;
		}

	}

	public function getSummaryReportSheetData($params, $export = false) {
		$shops =  $params['LO1_IDs'];
		$monthOrRange = $params['timeSpan'];
		$aboveTargets = isset($params['aboveTargets']) ? $params['aboveTargets'] : false;
		$startDate = $params['startDate'];
		$endDate = $params['endDate'];

		$reportType = RawSalesDataStoreMonthModel::SHEET_REPORT_TYPE_COMBO;

		$locationModel = new LocationModel();
		$locationModels = $locationModel->getLocations($params);
		$locationCodes = [];
		foreach ($locationModels as $model) {
			array_push($locationCodes, $model['LO1_SHORT_CODE']);
		}

		$rawSalesDataStoreMonthModel = new RawSalesDataStoreMonthModel();
		//If we want to group by month, we must use FindAll instead of the generateReports method
		if ($monthOrRange == RawSalesDataStoreMonthModel::BY_RANGE) {
			$reportData = $rawSalesDataStoreMonthModel->generateReportsByRangeByShop(['LO1_SHORT_CODEs' => implode(",", $locationCodes), 'startDate' => $startDate, 'endDate' => $endDate]);
		} else {
			$reportData = $rawSalesDataStoreMonthModel->getAll(['LO1_SHORT_CODEs' => implode(",", $locationCodes), 'startDate' => $startDate, 'endDate' => $endDate]);
		}

		if (isset($reportData) && !empty($reportData)) {
			$tempResult = [];
			$numberOfShops = count($reportData);

			$reportCategories = $this->getCategoriesForSheetReport($reportType);

			//For the monthly reports, set it the headers to month, otherwise leave it to shop code
			if ($monthOrRange == RawSalesDataStoreMonthModel::BY_MONTH) {
				if ($reportType == count(explode(',', $shops)) > 1) {
					$columnName = 'shop_name_month_and_year';
				} else {
					$columnName = 'month_and_year';
				}
			} else {
				$columnName = 'r03_shop_code';
			}

			//Search the given shops for target entries in order until one is found
			if ($reportType == RawSalesDataStoreMonthModel::SHEET_REPORT_TYPE_COMBO) {
				$targetModel = new TargetEntry();
				$targets = $targetModel->getTargetEntries(implode(",", $locationCodes));
			}
			//Generate the data
			foreach ($reportCategories as $key => $value) {
				$category = [];
				$category['report_type'] = $monthOrRange;
				$category['attribute_code'] = $key;
				$category['attribute'] = $value['attribute'];
				if ($export) {
					$category['Attribute'] = $value['attribute'];
				}
				$category['finalColumn'] = $value['finalColumn'];
				$category['categoryType'] = $value['categoryType'];
				$category['reportStartDate'] = $startDate;
				$category['reportEndDate'] = $endDate;
				$category['displayRow'] = true;
				for ($i = 0; $i < $numberOfShops; $i++) {
					$category['shops'][$reportData[$i][$columnName]] = $reportData[$i]['r03_shop_name'];
					$val = isset($reportData[$i][$key]) || $reportData[$i][$key] ? $reportData[$i][$key] : 0;
					$formatedValue = self::formatSheetValue($category, $val);
					if ($export) {
						//for export we need field:value only
						$category[$reportData[$i]['r03_shop_name']] = $formatedValue;
						$category[$reportData[$i][$columnName]] = $formatedValue;
					} else {
						$category[$reportData[$i][$columnName]] = [
							'value' => $val,
							'formatedValue' => $formatedValue
						];
					}
				}

				//Target Reporting: Set the targets for that attribute in final column
				if ($value['finalColumn'] == RawSalesDataStoreMonthModel::TARGET) {
					if (isset($targets, $value['target_attribute']) && $value['target_attribute'] != '') {
						$targetAttribute = $value['target_attribute'];

						$category['target'] = $targets[$targetAttribute];

						//If the toggle for only displaying the rows that are above target is on, then display only those
						if ($aboveTargets == true) {
							$category['displayRow'] = false;

							foreach ($category['shops'] as $key1 => $value1) {
								$temp = array();
								$temp['target'] = $category['target'];
								$temp['attribute_code'] = $key;
								$displayRow = RawSalesDataStoreMonth::getDisplayRow($temp, null, $category[$key1]['value']);

								if ($displayRow) {
									$category['displayRow'] = true;
									break;
								}
							}
						}

					} else {
						$category['target'] = 'Not Available';
					}
				}

				$category['total/target'] = self::processFinalColumn($category, $reportType);
				if ($export) {
					$category['Total/Target'] = $category['total/target'];
				}

				if ($category['displayRow'] == true) {
					$tempResult[] = $category;
				}
			}

			//The first default column that lists the attribute
			$columns[] = array(
				'header' => 'Attribute',
				'field' => 'Attribute',
				'visible' => true,
				'export' => [
					'visible' => true,
					'checked' => true
				],
				'type' => 'raw'
			);

			//What column we are looking at
			if ($columnName == 'r03_shop_code') {
				$columnHeaderAttribute = 'r03_shop_name';
			} else {
				$columnHeaderAttribute = $columnName;
			}
			//Go through each shop and process the values
			foreach ($reportData as $shop) {
				$columns[] = array(
					'header' => $shop[$columnHeaderAttribute],
					'field' => $shop[$columnHeaderAttribute],
					'visible' => true,
					'export' => [
						'visible' => true,
						'checked' => true
					],
					'type' => 'raw',
					'value' => $shop[$columnName]
				);
			}

			//Add a Total/Target Column
			$columns[] = array(
				'header' => 'Total/Target',
				'field' => 'Total/Target',
				'visible' => true,
				'export' => [
					'visible' => true,
					'checked' => true
				]
			);


			//Put the data inside the provider
			$result['data'] = $export ? $tempResult : new ArrayDataProvider(
				[
					'allModels' => $tempResult,
					'sort' => [],
					'pagination' => [
						'pageSize' => 100
					]
				]
			);

			//Send Columns
			$result['columns'] = $columns;
			return $result;
		}

		return false;
	}

	/**
	 * @return The Categories for the ReportSheetData
	 */
	private function getCategoriesForSheetReport($reportType)
	{
		$array = array(
			'r03_gross_amount',
			'r03_part_expense',
			'r03_materials_target',
			'r03_body_labor_amount',
			'r03_body_materials_amount',
			'r03_c_paint_and_refinish_labor_total',
			'r03_c_paint_and_refinish_labor_hours',
//            'r03_paint_amount',
//            'r03_paint_hours',
			'r03_paint_materials_amount',
//            'r03_refinish_amount',
			'r03_ro_count',
		);
		foreach ($array as $key) {
			$result[$key]['categoryType'] = RawSalesDataStoreMonthModel::PRIMARY;
		}
		$array = array(
			'r03_c_paint_materials_total',
			'r03_clear',
			'r03_detail',
			'r03_equipment',
			'r03_filler',
			'r03_fine_sand',
			'r03_hardener',
			'r03_hardware',
			'r03_misc',
			'r03_mixing_supplies',
			'r03_paper',
			'r03_primer_and_sealer',
			'r03_rough_sand',
			'r03_safety',
			'r03_solvent',
			'r03_tape',
			'r03_thinner',
			'r03_toner',
//                'r03_fm_misc',
//                'r03_fm_grand_total',
//                'r03_ppg_products',

		);

		foreach ($array as $key) {
			$result[$key]['categoryType'] = RawSalesDataStoreMonthModel::SECONDARY;
		}

		foreach ($result as $key => $value) {
			$result[$key] = array('attribute' => (new RawSalesDataStoreMonthModel)->getAttributeLabel($key));
			$result[$key]['finalColumn'] = RawSalesDataStoreMonthModel::TOTAL;
			$result[$key]['categoryType'] = $value['categoryType'];
		}
		$array = array(
			'r03_c_gp_percent',
			'r03_c_paint_sold_to_sales_p',
			'r03_c_paint_and_materials_sold_p',
			'r03_c_paint_and_materials_sold_per_ro',
			'r03_c_cost_per_paint_hour_total',
			'r03_c_cost_per_paint_hour_liquid',
			'r03_c_cost_per_paint_hour_allied',
			'r03_c_ppg_vs_all_grand_total_p',
			'r03_c_ppg_liq_vs_sales_p',
			'r03_c_materials_vs_paint_labor_p',
			'r03_c_ref_labor_vs_sales_p',
			'r03_c_paint_labor_vs_sales_p',
			'r03_c_ref_and_paint_labor_vs_sales_p',
			'r03_c_clear_vs_paint_labor_p',
			'r03_c_hdnr_vs_paint_labor_p',
			'r03_c_toner_vs_paint_labor_p',
			'r03_c_pmr_vs_paint_labor_p',
			'r03_c_tape_vs_paint_labor_p',
			'r03_c_thinner_vs_paint_labor_p',
			'r03_c_ppg_vs_paint_labor_p',
			'r03_c_ppg_liq_vs_paint_and_ref_labor_p',
			'r03_c_average_ro',
			'r03_c_paint_labor_per_ro',
			'r03_c_pm_cost_per_ro',
			'r03_c_clear_per_ro',
			'r03_c_toner_per_ro',

			'r03_po_spent_vs_materials_target',
//                'r03_c_allied_per_ro',
			'r03_c_misc_per_ro',
//            'r03_c_clear_per_ro_p', //Requested to be removed 4/7 per Steve
//            'r03_c_toner_per_ro_p', //Requested to be removed 4/7 per Steve
//            'r03_c_allied_per_ro_p', //Requested to be removed 4/7 per Steve
			'r03_c_materials_cost_vs_sales_p',
		);
		foreach ($array as $key) {
			$result[$key] = array('attribute' => (new RawSalesDataStoreMonthModel)->getAttributeLabel($key));
			$result[$key]['finalColumn'] = RawSalesDataStoreMonthModel::TARGET;
			$result[$key]['target_attribute'] = $this->getTargetAttribute($key);
			$result[$key]['categoryType'] = RawSalesDataStoreMonthModel::CALCULATED;
		}

		return $result;
	}

	/**
	 * Returns the target attribute for a passed attribute, typically a calculated attribute
	 * @param string $attribute RawSalesDataStoreMonth attribute
	 * @return string The TargetEntry target, else false if not found
	 */
	private function getTargetAttribute($attribute)
	{
		$array = array(
			'r03_c_gp_percent' => 'ta1_gp_percent',
			'r03_c_paint_sold_to_sales_p' => 'ta1_paint_sold_percent_to_sales',
			'r03_c_paint_and_materials_sold_p' => 'ta1_paint_and_matl_sold_percent',
			'r03_c_cost_per_paint_hour_total' => 'ta1_cost_per_paint_hour_total',
			'r03_c_cost_per_paint_hour_liquid' => 'ta1_cost_per_paint_hour_liquid',
			'r03_c_cost_per_paint_hour_allied' => 'ta1_cost_per_paint_hour_allied',
			'r03_c_ppg_vs_all_grand_total_p' => 'ta1_ppg_vs_all_grand_total_p',
			'r03_c_ppg_liq_vs_sales_p' => 'ta1_ppg_liq_vs_sales_p',
			'r03_c_materials_vs_paint_labor_p' => 'ta1_material_vs_paint_labor',
			'r03_c_ref_labor_vs_sales_p' => 'ta1_ref_labor_vs_sales',
			'r03_c_paint_labor_vs_sales_p' => 'ta1_paint_labor_vs_sales',
			'r03_c_ref_and_paint_labor_vs_sales_p' => 'ta1_ref_and_paint_labor_vs_sales',
			'r03_c_clear_vs_paint_labor_p' => 'ta1_clear_vs_paint_labor_p',
			'r03_c_hdnr_vs_paint_labor_p' => 'ta1_hdnr_vs_paint_labor_p',
			'r03_c_toner_vs_paint_labor_p' => 'ta1_toner_vs_paint_labor_p',
			'r03_c_pmr_vs_paint_labor_p' => 'ta1_pmr_vs_paint_labor_p',
			'r03_c_tape_vs_paint_labor_p' => 'ta1_tape_vs_paint_labor_p',
			'r03_c_thinner_vs_paint_labor_p' => 'ta1_thinner_vs_paint_labor_p',
			'r03_c_ppg_vs_paint_labor_p' => 'ta1_ppg_vs_paint_labor_p',
			'r03_c_ppg_liq_vs_paint_and_ref_labor_p' => 'ta1_ppg_liq_vs_paint_and_ref_labor_p',
			'r03_c_average_ro' => 'ta1_average_ro',
			'r03_c_paint_labor_per_ro' => 'ta1_paint_labor_per_ro',
			'r03_c_pm_cost_per_ro' => 'ta1_pm_cost_per_ro',
			'r03_c_clear_per_ro' => 'ta1_clear_per_ro',
			'r03_c_toner_per_ro' => 'ta1_toner_per_ro',
			'r03_c_allied_per_ro' => 'ta1_allied_per_ro',
			'r03_c_paint_and_materials_sold_per_ro' => 'ta1_paint_and_matl_sold_per_ro',
			'r03_c_misc_per_ro' => 'ta1_misc_per_ro',
			'r03_c_clear_per_ro_p' => 'ta1_clear_per_ro_p',
			'r03_c_toner_per_ro_p' => 'ta1_toner_per_ro_p',
			'r03_c_allied_per_ro_p' => 'ta1_allied_per_ro_p',
			'r03_c_materials_cost_vs_sales_p' => 'ta1_material_cost_vs_sales',
			'r03_po_spent_vs_materials_target' => 'ta1_material_target_total',
			'r03_c_paint_and_refinish_labor_total' => 'ta1_c_paint_and_refinish_labor_total',//TODO: may not exist
		);
		foreach ($array as $key => $value) {
			if ($key == $attribute) {
				return $value;
			}
		}
		return false;
	}

	public static function getDisplayRow($data, $header, $value = null)
	{
		if ($value == null) {
			if (isset($header, $data[$header])) {
				$value = $data[$header];
			}
		}

		if (isset($value)) {
			if (isset($data['target']) && is_numeric($value) && $data['target'] != 0) {
				$calculatedPercentage = ($value / $data['target']) * 100;
				if ($calculatedPercentage < 100) {
					$displayRow = false;
				} else {
					$displayRow = true;
				}
			}

			//For certain attributes, higher is better, this condition will flip the displayRow for those
			if ($data['attribute_code'] == 'r03_c_gp_percent' ||
				$data['attribute_code'] == 'r03_c_average_ro' ||
				$data['attribute_code'] == 'r03_c_paint_and_materials_sold_per_ro' ||
				$data['attribute_code'] == 'r03_c_paint_sold_to_sales_p' ||
				$data['attribute_code'] == 'r03_c_paint_and_materials_sold_p' ||
				$data['attribute_code'] == 'r03_c_ref_labor_vs_sales_p' ||
				$data['attribute_code'] == 'r03_c_paint_labor_vs_sales_p' ||
				$data['attribute_code'] == 'r03_c_paint_labor_per_ro' ||
				$data['attribute_code'] == 'r03_c_ref_and_paint_labor_vs_sales_p') {

				if (isset($displayRow) && $displayRow == true) {
					$displayRow = false;
				} elseif (isset($displayRow) && $displayRow == false) {
					$displayRow = true;
				}
			}
		}
		return isset($displayRow) && $displayRow;
	}

	public static function formatSheetValue($attribute, $data)
	{
		$attribute = $attribute['attribute'];
		if (is_numeric($data)) {
			if ((stripos($attribute, 'hour') !== false || stripos($attribute, 'day') !== false || stripos($attribute, 'week') !== false || stripos($attribute, 'count') !== false) && stripos($attribute, 'Cost') !== 0) {
				return number_format($data);
			} elseif (stripos($attribute, '%') !== false) {
				return number_format($data, 1) . '%';
			} else {
				return RawSalesDataStoreMonth::formatMoney($data);
			}
		}
		return $data;
	}

	public static function formatMoney($value)
	{
		if ($value < 100) {
			return '$' . number_format($value, 2);
		} else {
			return '$' . number_format($value);
		}
	}

	public static function processFinalColumn($data, $reportType = null)
	{
		if ($data['finalColumn'] == RawSalesDataStoreMonthModel::TARGET) {
			return self::formatSheetValue($data, $data["target"]);
		} elseif ($data['finalColumn'] == RawSalesDataStoreMonthModel::TOTAL) {
			return self::formatSheetValue($data, self::getShopsTotalFromRow($data));
		}
	}

	public static function getShopsTotalFromRow($data)
	{
		$total = 0;
		foreach ($data['shops'] as $key => $value) {
			if(!isset($data[$key]['value']))
			{
				$total += (float)str_replace(',', '', str_replace('%', '', str_replace('$', '', $data[$key])));
			} else {
				$total += $data[$key]['value'];
			}
		}
		return $total;
	}

	public function generateXls($params, $subtype = 'dash') {
		switch ($subtype) {
			case 'summary':
				$data = $this->getSummaryReportSheetData($params, true);
				$filePrefix = 'SummaryReport';
				break;
			default:
				$data = $this->getSummaryReportSheetData($params, true);
				$filePrefix = 'DashReport';
				break;
		}

		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.xls' ;

		$xlsService = new XLSService();
		return $xlsService->generateXls($params, '', $filename, $data['data']);
	}

}