<?php

namespace app\services;

use yii\base\Component;
use app\models\InvoiceLine as InvoiceLineModel;


class InvoiceLine extends Component
{
	public function deleteLinesForInvoice($id)
    {
	    $invoiceLineModels = InvoiceLineModel::find()->where(["IN1_ID" => $id])->all();

        foreach ($invoiceLineModels as $invoiceLineModel) {
	        $invoiceLineModel->IN2_DELETE_FLAG = 1;
	        $invoiceLineModel->save(false);
	    }
    }
}