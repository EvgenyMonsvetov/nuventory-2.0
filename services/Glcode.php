<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\Glcode as GlcodeModel;

class Glcode extends Component
{
    public function generateXls($params) {
        $filePrefix = 'Categories_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new GlcodeModel();
	    $query = $model->getSearchQuery($params);

	    $xlsService = new XLSService();
	    return $xlsService->generateXls($params, $query, $filename);
    }
}