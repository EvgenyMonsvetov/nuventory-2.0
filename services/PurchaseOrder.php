<?php

namespace app\services;

use app\models\MasterDataLocation;
use app\pdfs\Pdf;
use Yii;
use yii\base\Component;
use app\models\PurchaseOrder as PurchaseOrderModel;
use app\services\PurchaseOrderLine as PurchaseOrderLineService;
use app\models\Group as GroupModel;
use app\models\Budget as BudgetModel;
use yii\db\Expression;
use app\models\PurchaseOrderLine as PurchaseOrderLineModel;

class PurchaseOrder extends Component
{

	public $purchaseOrderColumns = [
      [ 'field' => 'CREATED_ON',              'header' => 'Created  On' ],
      [ 'field' => 'ORDERED_BY',              'header' => 'Ordered  By' ],
      [ 'field' => 'PO1_NUMBER',              'header' => 'Po1  Number' ],
      [ 'field' => 'PO1_SHIPPING_METHOD',     'header' => 'Shipping  Method' ],
      [ 'field' => 'PO1_STATUS_TEXT',         'header' => 'PO Status' ],
	  [ 'field' => 'PO1_UPLOAD_STATUS_TEXT',  'header' => 'Upload Status' ],
      [ 'field' => 'PO1_TOTAL_VALUE',         'header' => 'Total Value' ],
      [ 'field' => 'LO1_NAME',                'header' => 'By Shop' ],
      [ 'field' => 'CO1_NAME',                'header' => 'From Company' ],
      [ 'field' => 'PO1_COMMENT',             'header' => 'Comment' ],
      [ 'field' => 'MODIFIED_ON',             'header' => 'Modified  On' ],
      [ 'field' => 'MODIFIED_BY',             'header' => 'Modified  By' ],
      [ 'field' => 'CREATED_BY',              'header' => 'Created  By' ]
    ];

    public function create($orders=[], $hours_offset = 0)
    {
        $errors = [];

        $PO1_ID = null;
        $transaction = Yii::$app->db->beginTransaction();

        try{
            $purchaseOrderModels = [];
            foreach( $orders as $order ){
                $orderTotal = 0;
                foreach($order['parts'] as $part){
                    $orderTotal += $part['MD1_EXTENDED_VALUE'];
                }
                $user = Yii::$app->user->getIdentity();

                $purchaseOrderModel = new PurchaseOrderModel();
                $purchaseOrderModel->load([
                    "PO1_ORDERED_BY" => $user->US1_ID,
                    'CO1_ID' => $user->CO1_ID,
                    'LO1_ID' => $order['LO1_ID'],
                    'VD1_ID' => $order['VD1_ID'],
                    "PO1_NUMBER" => $order['PO1_NUMBER'],
                    "PO1_SHIPPING_METHOD" => $order['PO1_SHIPPING_METHOD'],
                    "PO1_STATUS" => \app\models\OrderHeader::ORDER_STATUS_OPEN,
                    "PO1_TOTAL_VALUE" => $orderTotal,
                    "PO1_COMMENT" => $order['PO1_COMMENT']
                ], '');

                if(!$purchaseOrderModel->save(true)){
                    throw new \Exception(join("\n", $purchaseOrderModel->getErrorSummary(true)));
                }else{
                    $purchaseOrderModels[] = $purchaseOrderModel;
                    $PO1_ID = $purchaseOrderModel->PO1_ID;
                    foreach($order['parts'] as $index => $part){
                        $index++;
                        $purchaseOrderLine = new \app\models\PurchaseOrderLine();

                        $purchaseOrderLine->load([
                            "PO1_ID" => $PO1_ID,
                            "PO2_LINE" => $index,
                            "PO2_STATUS" => $part["MD1_ORDER_QTY"] > 0 ?
                                \app\models\OrderHeader::ORDER_STATUS_OPEN :
                                \app\models\OrderHeader::ORDER_STATUS_RECEIVED,
                            "PO2_ORDER_QTY" => $part["MD1_ORDER_QTY"],
                            "PO2_RECEIVED_QTY" => 0,
                            "MD1_ID" => $part["MD1_ID"],
                            "CO1_ID" => $user->CO1_ID,
                            "CA1_ID" => $part["CA1_ID"],
                            "GL1_ID" => $part["GL1_ID"],
                            "LO1_ID" => $purchaseOrderModel->LO1_ID,
                            "UM1_ID" => $part["UM1_PRICE_ID"] ? $part["UM1_PRICE_ID"] : 1,
                            "PO2_PART_NUMBER" => $part["MD1_PART_NUMBER"],
                            "PO2_DESC1" => $part["MD1_DESC1"] ? $part["MD1_DESC1"] : "",
                            "PO2_DESC2" => $part["MD1_DESC2"] ? $part["MD1_DESC2"] : "",
                            "PO2_UNIT_PRICE" => $part["MD1_ORIGINAL_UNIT_PRICE"],
                            "PO2_TOTAL_VALUE" => ceil($part["MD1_EXTENDED_VALUE"] * 100) / 100
                        ], '');

                        if(!$purchaseOrderLine->save(true)){
                            throw new \Exception(join(" ", $purchaseOrderLine->getErrorSummary(true)));
                        }
                    }
                }
            }

            if (!$errors) {
                foreach($purchaseOrderModels as $purchaseOrderModel1){
                    $this->notifyUsers($purchaseOrderModel1, $hours_offset);
                }
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }

        return [
            'success' => !$errors,
            'errors'  => $errors,
            'PO1_ID'  => $PO1_ID
        ];
    }

	private function notifyUsers($purchaseOrderModel, $hours_offset) {
		$CO1_ID = $purchaseOrderModel['CO1_ID'];
		$LO1_ID = $purchaseOrderModel['LO1_ID'];

		$shop = \app\models\Location::find()->alias('LO1')->select(['LO1.LO1_NAME'])
			->where(['=', 'LO1.LO1_ID', $purchaseOrderModel['LO1_ID']])
			->one();
		$vendor = \app\models\Vendor::find()->alias('VD1')->select(['VD1.VD1_NAME'])
			->where(['=', 'VD1.VD1_ID', $purchaseOrderModel['VD1_ID']])
			->one();

		$vendorName = isset($vendor['VD1_NAME']) ? $vendor['VD1_NAME'] : '';
		$shopName = isset($shop['LO1_NAME']) ? $shop['LO1_NAME'] : '';
		$params['PO1_IDs'] = $purchaseOrderModel['PO1_ID'];
		$params['hours_offset'] = $hours_offset;
		$pdfService = new Pdf();
		$pdf = $pdfService->printPurchaseOrders($params);
		$filePrefix = 'PurchaseOrder';
		$currDate = Date('Ymd_His');
		$fileName = $filePrefix . '_' . $currDate . '.pdf';

		Yii::$app->response->isSent = false;
		$pathToPdfFile = $pdf->Output($fileName, 'S');

		$VD1_ID_Exp = new Expression('0');

		$shopManagerAndStandardUserQuery = \app\models\User::find()->alias('US1')->select(['US1.*', 'US1.VD0_ID', 'VD1_ID' => $VD1_ID_Exp, 'GR1.GR1_ACCESS_LEVEL'])
			->leftJoin("gr1_group as GR1" , "GR1.GR1_ID = US1.GR1_ID AND GR1.GR1_DELETE_FLAG = 0")
			->where(['=', 'US1.CO1_ID', $CO1_ID])
			->andWhere(['=', 'US1.LO1_ID', $LO1_ID])
			->andWhere(['or', ['=', 'GR1.GR1_ACCESS_LEVEL', GroupModel::GROUP_STANDARD_USER],
			    ['=', 'GR1.GR1_ACCESS_LEVEL', GroupModel::GROUP_LOCATION_MANAGER]
		    ])
			->andWhere(['<>', 'US1.DELETED', 1])
			->andWhere(['is not', 'US1.US1_EMAIL', new \yii\db\Expression('null')])
			->andWhere(['!=', 'US1.US1_EMAIL', ''])
			->andWhere(['=', 'US1.US1_PO_NOTIFY', '1']);

		$companyAdminQuery = \app\models\User::find()->alias('US1')->select(['US1.*', 'US1.VD0_ID', 'VD1_ID' => $VD1_ID_Exp, 'GR1.GR1_ACCESS_LEVEL'])
			->leftJoin("gr1_group as GR1" , "GR1.GR1_ID = US1.GR1_ID AND GR1.GR1_DELETE_FLAG = 0")
			->where(['=', 'US1.CO1_ID', $CO1_ID])
			->andWhere(['=', 'GR1.GR1_ACCESS_LEVEL', GroupModel::GROUP_COMPANY_MANAGER])
			->andWhere(['<>', 'US1.DELETED', 1])
			->andWhere(['>', 'US1.VD0_ID', 0])
			->andWhere(['is not', 'US1.US1_EMAIL', new \yii\db\Expression('null')])
			->andWhere(['!=', 'US1.US1_EMAIL', ''])
			->andWhere(['=', 'US1.US1_PO_NOTIFY', '1']);

		$masterVendorQuery = \app\models\User::find()->alias('US1')->select(['US1.*', 'US1.VD0_ID', 'VD1.VD1_ID', 'GR1.GR1_ACCESS_LEVEL'])
			->leftJoin("gr1_group as GR1" , "GR1.GR1_ID = US1.GR1_ID AND GR1.GR1_DELETE_FLAG = 0")
			->leftJoin("vd0_vendor as VD0" , "VD0.VD0_ID = US1.VD0_ID AND VD0.VD0_DELETE_FLAG = 0")
			->leftJoin("vd1_vendor as VD1" , "VD1.VD0_ID = VD0.VD0_ID AND VD1.VD1_DELETE_FLAG = 0")
			->where(['=', 'US1.CO1_ID', $CO1_ID])
			->andWhere(['=', 'GR1.GR1_ACCESS_LEVEL', GroupModel::GROUP_MANUFACTURER_MANAGER])
			->andWhere(['<>', 'US1.DELETED', 1])
			->andWhere(['>', 'US1.VD0_ID', 0])
			->andWhere(['=', 'VD1.VD1_ID', $purchaseOrderModel['VD1_ID']])
			->andWhere(['>', 'VD1.VD1_ID', 0])
			->andWhere(['is not', 'US1.US1_EMAIL', new \yii\db\Expression('null')])
			->andWhere(['!=', 'US1.US1_EMAIL', ''])
			->andWhere(['=', 'US1.US1_PO_NOTIFY', '1']);

		$superAdminQuery = \app\models\User::find()->alias('US1')->select(['US1.*', 'US1.VD0_ID', 'VD1_ID' => $VD1_ID_Exp, 'GR1.GR1_ACCESS_LEVEL'])
			->leftJoin("gr1_group as GR1" , "GR1.GR1_ID = US1.GR1_ID AND GR1.GR1_DELETE_FLAG = 0")
			->where(['=', 'US1.CO1_ID', $CO1_ID])
			->andWhere(['=', 'GR1.GR1_ACCESS_LEVEL', GroupModel::GROUP_ADMIN])
			->andWhere(['<>', 'US1.DELETED', 1])
			->andWhere(['is not', 'US1.US1_EMAIL', new \yii\db\Expression('null')])
			->andWhere(['!=', 'US1.US1_EMAIL', ''])
			->andWhere(['=', 'US1.US1_PO_NOTIFY', '1']);

		$shopManagerAndStandardUserQuery->union($companyAdminQuery);
		$shopManagerAndStandardUserQuery->union($masterVendorQuery);
		$shopManagerAndStandardUserQuery->union($superAdminQuery);
		$users = $shopManagerAndStandardUserQuery->asArray()->all();

		foreach ($users as $user) {
			$this->sendMail($user['US1_EMAIL'], $vendorName, $shopName, $pathToPdfFile, $fileName);
		}
	}

	private function sendMail($to, $vendorName, $shopName, $pathToPdfFile, $fileName) {

        Yii::$app->mailer->compose()
            ->setFrom( Yii::$app->params['mailer']['sender'] )
            ->setTo($to)
            ->setSubject('New Purchase Order from Customer ' . $shopName)
            ->setTextBody("A new Purchase Order has been uploaded to ". $vendorName . ".\nPlease see attached.\n\nNuVentory LLC Administrator")
            ->attachContent($pathToPdfFile, ['fileName' => $fileName, 'contentType' => 'application/pdf'])
            ->send();
	}

    public function receive($PO1_ID, $purchaseOrderLines)
    {
        $errors = [];
        $transaction = Yii::$app->db->beginTransaction();

        $auth = Yii::$app->getAuthManager();
        $hasMasterDataEditAccess = $auth->checkAccess(Yii::$app->user->getId(), 'MASTER_DATA_EDIT');
        foreach( $purchaseOrderLines as $line){
            $poLine = \app\models\PurchaseOrderLine::findOne(['PO2_ID' => $line['PO2_ID'], 'PO2_DELETE_FLAG' => 0]);
            if($line['QTY'] > 0 && $poLine->PO2_STATUS != \app\models\OrderHeader::ORDER_STATUS_RECEIVED ){
                $poLine->PO2_RECEIVED_QTY += $line['QTY'];
                $poLine->PO2_STATUS = $poLine->PO2_RECEIVED_QTY > 0 ?
                                            \app\models\OrderHeader::ORDER_STATUS_RECEIVED :
                                            \app\models\OrderHeader::ORDER_STATUS_OPEN;


                if($poLine->PO2_UNIT_PRICE != $line['PO2_UNIT_PRICE'] && $hasMasterDataEditAccess ){
                    $poLine->PO2_UNIT_PRICE = $line['PO2_UNIT_PRICE'];
                    $poLine->PO2_TOTAL_VALUE = $line['PO2_TOTAL_VALUE'];

                    $masterData = \app\models\MasterData::findOne(['MD1_ID' => $line['MD1_ID'], 'MD1_DELETE_FLAG' => 0]);
                    $masterData->MD1_UNIT_PRICE = $poLine->PO2_UNIT_PRICE;
                    if(!$masterData->save(true)){
                        $errors += $masterData->getErrorSummary();
                    }
                }

                if(!$poLine->save(true)){
                    $errors += $poLine->getErrorSummary();
                }

                $masterDataLocation = new MasterDataLocation();
                $masterDataLocation->updatePurchaseOrderLineStock($poLine, $line['QTY']);

                $inventoryModel = new \app\models\Inventory();
                $inventoryModel->updatePurchaseOrderLineStock($poLine, $line['QTY']);

                if($errors){
                    break;
                }
            }
        }

        $orderLines = \app\models\PurchaseOrderLine::findAll(['PO1_ID' => $PO1_ID, 'PO2_DELETE_FLAG' => 0]);
        $received = 0;
        $orderTotal = 0;
        foreach($orderLines as $orderLine1 ){
            if($orderLine1->PO2_STATUS == \app\models\OrderHeader::ORDER_STATUS_RECEIVED ){
                $received ++;
            }
	        $orderTotal += $orderLine1->PO2_TOTAL_VALUE;
        }

        $orderHeader = \app\models\PurchaseOrder::findOne([
            'PO1_ID' => $PO1_ID,
            'PO1_DELETE_FLAG' => 0
        ]);

        $orderHeader->PO1_TOTAL_VALUE = $orderTotal;

        if (count($orderLines) == $received) {
            $orderHeader->PO1_STATUS = \app\models\OrderHeader::ORDER_STATUS_RECEIVED;
	    }

        if(! $orderHeader->save(true)){
            $errors = $orderHeader->getErrorSummary(true);
        };


        if(!$errors){
            $transaction->commit();
        }else{
            $transaction->rollBack();
        }

        return [
            'success' => !$errors,
            'errors'  => $errors
        ];
    }


    public function upload( $PO1_ID )
    {
        $errors = [];
        $transaction = Yii::$app->db->beginTransaction();
        try{
            $purchaseOrderHeader = \app\models\PurchaseOrder::findOne([
                'PO1_ID' => $PO1_ID,
                'PO1_DELETE_FLAG' => 0,
                'PO1_UPLOAD_STATUS' => 0
            ]);

            if($purchaseOrderHeader){
                $purchaseOrderHeader->PO1_UPLOAD_STATUS = 4;
                if(!$purchaseOrderHeader->save(true)){
                    throw new \Exception($purchaseOrderHeader->getErrorSummary(true));
                };
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }

        return [
            'success' => !$errors,
            'errors'  => $errors
        ];
    }

    public function delete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try{
            $purchaseOrderModel = PurchaseOrderModel::find()->where(["PO1_ID" => $id])->one();
            $purchaseOrderModel->PO1_DELETE_FLAG = 1;
            $purchaseOrderModel->save(false);

            $purchaseOrderLineModels = PurchaseOrderLineModel::find()->where(["PO1_ID" => $id])->all();
            foreach ($purchaseOrderLineModels as $purchaseOrderLineModel) {
                $purchaseOrderLineModel->PO2_DELETE_FLAG = 1;
                $purchaseOrderLineModel->save(false);
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        } catch (\Throwable $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }
    }

	public function generateXls($params) {
		$filePrefix = 'PurchaseOrders_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new PurchaseOrderModel();
		$query = $model->getQuery($params);

		$xlsService = new XLSService();

		return $xlsService->generateXls($params, $query, $filename);
	}

	public function generateXlsWithLines($params) {
		$filePrefix = 'PurchaseOrdersWithLines_';
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . $currDate . '.xls' ;

		$model = new PurchaseOrderModel();
		$dataProvider = $model->getPurchaseOrder($params['PO1_ID']);
		$dataProvider->query->asArray();
		$models = $dataProvider->getModels();

		$modelLine = new PurchaseOrderLineModel();
		$dataProviderLine = $modelLine->getAll($params);
		$lines = $dataProviderLine->getModels();

		$xlsService = new XLSService();

		return $xlsService->generateXlsWithLines($params, 'Purchase Order', $models[0], $this->purchaseOrderColumns, $lines, $filename);
	}

	public function getTotalOfPurchaseOrders($params) {
		$model = new PurchaseOrderModel();

		$dates = [];
		if (isset($params['START_DATE']) && isset($params['END_DATE'])) {
			$datetime1 = date_create($params['START_DATE']);
			$datetime2 = date_create($params['END_DATE']);

			$interval = date_diff($datetime1, $datetime2);
			$monthCount = $interval->m + $interval->y * 12;

			for ($i = $monthCount; $i>= 0; $i--) {
				$dates[] = date('M Y', strtotime((new \DateTime(date('Y-m-15', strtotime($params['END_DATE']))))->modify('-' . $i . ' months')->format('Y-m-d')));
			}
		} else {
			for ($i = 2; $i>= 0; $i--) {
				$dates[] = date('M Y', strtotime((new \DateTime(date('Y-m-15', strtotime((new \DateTime('now'))->format('Y-m-d')))))->modify('-' . $i . ' months')->format('Y-m-d')));
			}

			$params = [];
			$params['DASHBOARD_REPORT'] = true;
			$now = new \DateTime('now');
			$params['END_DATE'] = date('Y-m-t', strtotime($now->format('Y-m-d')));
			$params['START_DATE'] = date('Y-m-01', strtotime((new \DateTime(date('Y-m-15', strtotime((new \DateTime('now'))->format('Y-m-d')))))->modify('-2 months')->format('Y-m-d')));
		}

		$params['BY_VENDOR'] = true;
		$orderTotalsByVendor = $model->getPurchaseOrderTotals( $params );

		$result = [
		    'data' => [],
            'drilldown' => [
                'data' => []
            ]
        ];

		foreach ($orderTotalsByVendor as $i => $data) {
			if(!isset($result['data'][$data['MONTH']])) {
				$result['data'][$data['MONTH']] = [];
			}
			if(!isset($result['data'][$data['MONTH']]['name'])) {
				$result['data'][$data['MONTH']]['name'] = isset($params['MATERIAL_BUDGET_REPORT']) ? $data['MONTH'] : 'Total MTD of ' . $data['MONTH'];
			}
			if(!isset($result['data'][$data['MONTH']]['drilldown'])) {
				$result['data'][$data['MONTH']]['drilldown'] = $data['MONTH'];
			}
			if(!isset($result['data'][$data['MONTH']]['y'])) {
				$result['data'][$data['MONTH']]['y'] = 0.0;
			}
			if(!isset($result['data'][$data['MONTH']]['percentage'])) {
				$result['data'][$data['MONTH']]['percentage'] = 0.0;
			}
			if(!isset($result['data'][$data['MONTH']]['type'])) {
				$result['data'][$data['MONTH']]['type'] = 'column';
			}

			$result['data'][$data['MONTH']]['y'] += (double)$data['TOTAL_VALUE'];

			if (!isset($result['drilldown']['data'][$data['MONTH']])) {
				$result['drilldown']['data'][$data['MONTH']]['name'] = 'Totals per Vendor';
				$result['drilldown']['data'][$data['MONTH']]['colorByPoint'] = true;
				$result['drilldown']['data'][$data['MONTH']]['id'] = $data['MONTH'];
				$result['drilldown']['data'][$data['MONTH']]['type'] = 'column';
				$result['drilldown']['data'][$data['MONTH']]['data'] = array();
			}

			$data['name'] = $data['VD1_NAME'] . ' - ' . $data['MONTH'];
			$data['y'] = (double)$data['TOTAL_VALUE'];

			unset($data['CREATED_ON']);
			unset($data['TOTAL_VALUE']);

			$result['drilldown']['data'][$data['MONTH']]['data'][] = $data;
		}

		$budgetModel = new BudgetModel();
		$budgetDataProvider = $budgetModel->search( $params );
		$budgetDataProvider->query->asArray();
		$budgetModels = $budgetDataProvider->getModels();

		$budgetResults = [];
		foreach ($dates as $date) {
			$found = false;
			foreach ($budgetModels as $i => $data) {
				if ($data['MONTH'] == $date) {
					$budget['y'] = (double)$data['BU1_FINAL_RESULT'];
					if (isset($params['MATERIAL_BUDGET_REPORT'])) {
						$budget['name'] = $date;
					}
					$budgetResults[] = $budget;
					$found = true;
					break;
				}
			}
			if (!$found) {
				$budget['y'] = 0;
				if (isset($params['MATERIAL_BUDGET_REPORT'])) {
					$budget['name'] = $date;
				}
				$budgetResults[] = $budget;
			}
		}

		$newResult = [];
		foreach ($dates as $date) {
			$found = false;
			$months = array_keys($result['data']);
			foreach ($months as $month) {
				if ($month === $date) {
					foreach ($budgetModels as $budgetModel) {
						if ($budgetModel['MONTH'] == $date) {
							if ((double)$budgetModel['BU1_FINAL_RESULT'] > 0) {
								$result['data'][$date]['percentage'] = $result['data'][$date]['y'] * 100 / (double)$budgetModel['BU1_FINAL_RESULT'];
							}
							break;
						}
					}
					$found = true;
					$newResult[] = $result['data'][$date];
				}
			}

			if (!$found) {
				$result['data'][$date]['name'] = isset($params['MATERIAL_BUDGET_REPORT']) ? $date : 'Total MTD of ' . $date;
				$result['data'][$date]['drilldown'] = $date;
				$result['data'][$date]['y'] = 0;
				$result['data'][$date]['percentage'] = 0.0;
				$result['drilldown']['data'][$date]['name'] = 'Totals per Vendor';
				$result['drilldown']['data'][$date]['colorByPoint'] = true;
				$result['drilldown']['data'][$date]['id'] = $date;
				$result['drilldown']['data'][$date]['data'] = array();
				$newResult[] = $result['data'][$date];
			}
		}

		$result['data'] = array_values($newResult);
		$result['drilldown']['data'] = array_values($result['drilldown']['data']);

		return $result = array(
			'name' => 'Total of purchase orders',
			'drilldown' => $result['drilldown']['data'],
			'data' => $result['data'],
			'budgets' => $budgetResults
		);
	}

	public function recalculatePO($PO1_ID)
    {
        $errors = [];
        $orderHeader = \app\models\PurchaseOrder::findOne([
            'PO1_ID' => $PO1_ID,
            'PO1_DELETE_FLAG' => 0
        ]);

        $orderLines = \app\models\PurchaseOrderLine::findAll(['PO1_ID' => $PO1_ID, 'PO2_DELETE_FLAG' => 0]);
        $orderTotal = 0;
        $forceRecieved = true;
        foreach($orderLines as $orderLine1 ){
            $orderTotal += $orderLine1->PO2_TOTAL_VALUE;
            if ($orderLine1->PO2_STATUS == \app\models\PurchaseOrderLine::ORDER_LINE_STATUS_OPEN) {
                $forceRecieved = false;
            }
        }
        if($forceRecieved){
            $orderHeader->PO1_STATUS = \app\models\OrderHeader::ORDER_STATUS_RECEIVED;
        }
        $orderHeader->PO1_TOTAL_VALUE = $orderTotal;
        if(! $orderHeader->save(true)){
            $errors = $orderHeader->getErrorSummary(true);
        }

        return array(
            'success' => !$errors,
            'errors'  => $errors
        );
    }
}