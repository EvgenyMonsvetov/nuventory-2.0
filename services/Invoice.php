<?php

namespace app\services;

use app\models\InvoiceLine;
use Yii;
use yii\base\Component;
use app\models\Invoice as InvoiceModel;
use app\models\InvoiceLine as InvoiceLineModel;
use app\models\Inventory;
use app\models\MasterDataLocation;
use app\services\InvoiceLine as InvoiceLineService;
use yii\db\Exception;

class Invoice extends Component
{
	public function delete($id)
    {
        $invoiceModel = InvoiceModel::find()->where(["IN1_ID" => $id])->one();
	    $invoiceModel->IN1_DELETE_FLAG = 1;
	    $invoiceModel->save(false);

	    $invoiceLineService = new InvoiceLineService();
		$invoiceLineService->deleteLinesForInvoice($id);
    }

	public function create( $order, $IN1_NUMBER )
	{
		$errors = [];

		$IN1_ID = null;
		if (!$errors) {

			$transaction = Yii::$app->db->beginTransaction();
			try {

				$invoiceModel = new InvoiceModel();

				$orderTotal = 0;
				foreach( $order['parts'] as $part){
					$unitPrice = $part["MD1_UNIT_PRICE"];
					$orderQty  = $part["MD1_ORDER_QTY"];
					$orderTotal += -($unitPrice * $orderQty);
				}

				$user = Yii::$app->user->getIdentity();
				$invoiceModel->load([
					'CO1_ID'              => $user->CO1_ID,
					'LO1_FROM_ID'         => $user->LO1_ID,
					'LO1_TO_ID'           => $order['LO1_TO_ID'],
					'IN1_NUMBER'          => strval($IN1_NUMBER),
					'IN1_TOTAL_VALUE'     => $orderTotal,
					'IN1_COMMENT'         => $order['IN1_COMMENT'],
					'US1_ID'              => $user->US1_ID,
					'IN1_DELETE_FLAG'     => 0
				], '');

				if (!$invoiceModel->save(true)) {
					throw new Exception($invoiceModel->getErrorSummary(true));
				} else {
					$IN1_ID = $invoiceModel['IN1_ID'];
					foreach( $order['parts'] as $index=>$part){
						$index ++;
						$unitPrice = $part["MD1_UNIT_PRICE"];
						$orderQty  = $part["MD1_ORDER_QTY"];
						$orderLine = new \app\models\InvoiceLine();
						$orderLine->load([
							"IN1_ID" => $IN1_ID,
							"IN2_LINK"   => isset($part["IN2_LINK"]) ? $part["IN2_LINK"] : null,
							"OR2_ID"   => isset($part["OR2_ID"]) ? $part["OR2_ID"] : null,
							"IN2_LINE"   => $index,
							"IN2_STATUS" => 0,
							"IN2_INVOICE_QTY" =>$part["MD1_ORDER_QTY"],
							"MD1_ID"  => $part["MD1_ID"],
							"CO1_ID"  => $part["CO1_ID"],
							"CA1_ID"  => $part["CA1_ID"],
							"GL1_ID"  => $part["GL1_ID"],
							"LO1_ID" => $order['LO1_TO_ID'],
							"FI1_ID" => $part["FI1_ID"],
							"UM1_ID" => $part["UM1_PRICE_ID"],
							"IN2_PART_NUMBER" => $part["MD1_PART_NUMBER"],
							"IN2_DESC1" => $part["MD1_DESC1"],
							"IN2_DESC2" => isset($part["MD1_DESC2"]) ? $part["MD1_DESC2"] : '',
							"IN2_UNIT_PRICE" => $part["MD1_UNIT_PRICE"],
							"IN2_UM_SIZE" => 0,
							"IN2_TOTAL_VALUE" => -($unitPrice * $orderQty),
							"IN2_DELETE_FLAG" => 0
						], '');

						if(!$orderLine->save(true)){
							return $orderLine;
							throw new Exception($orderLine->getErrorSummary(true));
						}
					}

					foreach( $order['parts'] as $index=>$part){

						$mdLocationQuery = MasterDataLocation::find()
							->andFilterWhere(['=', 'MD1_ID', $part['MD1_ID']])
							->andFilterWhere(['=', 'LO1_ID', $invoiceModel->LO1_FROM_ID]);

						$mdLocation = $mdLocationQuery->one();

						if(!$mdLocation) {
							$mdLocation = new MasterDataLocation();
						}

						if($mdLocation->load([
							"MD2_ID" => $part["MD2_ID"],
							"MD2_ON_HAND_QTY"   => (int)$part["MD2_ON_HAND_QTY"] + (int)$part["MD1_ORDER_QTY"],
							"MD2_AVAILABLE_QTY" => (int)$part["MD2_AVAILABLE_QTY"]+ (int)$part["MD1_ORDER_QTY"]
						], '')) {
							if(!$mdLocation->save( true )){
								throw new Exception($mdLocation->getErrorSummary(true));
							}
						}

						$inventory = new Inventory();

						if($inventory->load([
							"CO1_ID"    => $part["CO1_ID"],
							"LO1_ID"   => $invoiceModel["LO1_FROM_ID"],
							"TE1_ID"   => '',
							"MD1_ID"   => $part["MD1_ID"],
							"MD2_ID"   => $part["MD2_ID"],
							"JT2_ID"   => '',
							"OR2_ID"   => '',
							"PO2_ID"   => '',
							"MD1_PART_NUMBER"  => $part["MD1_PART_NUMBER"],
							"MD1_DESC1"  => $part["MD1_DESC1"],
							"LG1_DESC" => 'Credit Memo Generated',
							"LG1_UNIT_COST" => 0,
							"LG1_EXTENDED_COST" => 0,
							"LG1_OLD_QTY" => $part["MD2_ON_HAND_QTY"],
							"LG1_NEW_QTY" => (int)$part["MD2_ON_HAND_QTY"] + (int)$part["MD1_ORDER_QTY"],
							"LG1_ADJ_QTY" => $part["MD1_ORDER_QTY"],
							"LG1_DELETE_FLAG" => 0
						], '')) {

							if(!$inventory->save( true )) {
								throw new Exception($inventory->getErrorSummary(true));
							}
						}
					}
				}

				$transaction->commit();
			} catch (\Exception $e) {
				$errors[] = $e->getMessage();
				$transaction->rollBack();
			} catch (\Throwable $e) {
				$errors[] = $e->getMessage();
				$transaction->rollBack();
			}
		}

		return [
			'success' => !$errors,
			'errors'  => $errors,
			'IN1_ID'  => $IN1_ID
		];
	}

	public function createFromStoreOrder( $OR1_ID )
    {
        $errors = [];

		$IN1_ID = null;
		if (!$errors) {

			$transaction = Yii::$app->db->beginTransaction();
			try {
			    $orderHeader = \app\models\OrderHeader::findOne(['OR1_ID' => $OR1_ID, 'OR1_DELETE_FLAG' => 0]);

				$invoiceModel = new InvoiceModel();
				$invoiceModel->OR1_ID = $orderHeader->OR1_ID;
				$invoiceModel->CO1_ID = $orderHeader->CO1_ID;
				$invoiceModel->US1_ID = $orderHeader->OR1_ORDERED_BY;
				$invoiceModel->LO1_FROM_ID = $orderHeader->LO1_FROM_ID;
				$invoiceModel->LO1_TO_ID = $orderHeader->LO1_TO_ID;
				$invoiceModel->IN1_NUMBER = strval($orderHeader->OR1_NUMBER);
				$invoiceModel->IN1_STATUS = 0;
				$invoiceModel->IN1_SUM_STATUS = 0;
				$invoiceModel->IN1_TOTAL_VALUE = floatval($orderHeader->OR1_TOTAL_VALUE);
				$invoiceModel->IN1_COMMENT = $orderHeader->OR1_COMMENT;

                if($invoiceModel->save(true)){
                    $IN1_ID = $invoiceModel->IN1_ID;
                    $orderLines = \app\models\OrderLine::findAll(['OR1_ID' => $OR1_ID, 'OR2_DELETE_FLAG' => 0]);
                    foreach( $orderLines as $orderLine){

                        $masterDataLocation = MasterDataLocation::findOne([
                            'MD1_ID' => $orderLine->MD1_ID,
                            'LO1_ID' => $orderLine->LO1_TO_ID
                        ]);

                        if( $orderLine->LO1_TO_ID == $orderLine->LO1_FROM_ID ){
                            $UM1_ID = $masterDataLocation->UM1_RECEIPT_ID;
                        }else{
                            $UM1_ID = $masterDataLocation->UM1_PURCHASE_ID;
                        }

                        $invoiceLineModel = new InvoiceLineModel();
	                    $invoiceLineModel->IN1_ID = $IN1_ID;
	                    $invoiceLineModel->OR2_ID = $orderLine->OR2_ID;
	                    $invoiceLineModel->IN2_LINE = $orderLine->OR2_LINE;
	                    $invoiceLineModel->IN2_STATUS = 0;
	                    $invoiceLineModel->IN2_INVOICE_QTY = $orderLine->OR2_ORDER_QTY;
	                    $invoiceLineModel->MD1_ID = $orderLine->MD1_ID;
	                    $invoiceLineModel->CA1_ID = $orderLine->CA1_ID;
	                    $invoiceLineModel->GL1_ID = $orderLine->GL1_ID;
	                    $invoiceLineModel->CO1_ID = $orderLine->CO1_ID;
	                    $invoiceLineModel->IN2_PART_NUMBER = $orderLine->MD1_PART_NUMBER;
	                    $invoiceLineModel->IN2_DESC1 = $orderLine->MD1_DESC1 ? $orderLine->MD1_DESC1 : '';
	                    $invoiceLineModel->IN2_DESC2 = $orderLine->MD1_DESC2 ? $orderLine->MD1_DESC2 : '';
	                    $invoiceLineModel->UM1_ID = $UM1_ID;
	                    $invoiceLineModel->IN2_UNIT_PRICE = $orderLine->MD1_UNIT_PRICE;
	                    $invoiceLineModel->LO1_ID = $orderLine->LO1_TO_ID;
	                    $invoiceLineModel->IN2_TOTAL_VALUE = $orderLine->OR2_TOTAL_SELL;
	                    $invoiceLineModel->LO1_ID = $invoiceModel->LO1_FROM_ID;
	                    $invoiceLineModel->IN2_UM_SIZE = 0;

                        if(!$invoiceLineModel->save(true)){
                            throw new Exception($invoiceLineModel->getErrorSummary(true));
                        }
                    }
                }else{
                    throw new Exception($invoiceModel->getErrorSummary(true));
                }

                $orderHeader->OR1_INV_STATUS = \app\models\OrderHeader::ORDER_STATUS_INVOICED;
                if(!$orderHeader->save(true)){
                    throw new Exception($orderHeader->getErrorSummary(true));
                }

				$transaction->commit();
			} catch (\Exception $e) {
				$errors[] = $e->getMessage();
				$transaction->rollBack();
			} catch (\Throwable $e) {
				$errors[] = $e->getMessage();
				$transaction->rollBack();
			}
		}

		return [
			'success' => !$errors,
			'errors'  => $errors,
			'IN1_ID'  => $IN1_ID
		];
    }

    public function generateXls($params) {
        $model = new InvoiceModel();

        switch ($params['subtype']) {
            case 'credit-memos':
                $filePrefix = 'CreditMemos_';
                $query = $model->getAllCreditMemosQuery($params);
                break;
            case 'invoices':
            default:
                $filePrefix = 'Invoices_';
                $query = $model->getQuery($params);
                break;
        }

        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $xlsService = new XLSService();

        return $xlsService->generateXls($params, $query, $filename);
    }

	public function generateAllInvoices() {
		$errors = [];

		$orderModel = new \app\models\OrderHeader();
		$orders = $orderModel->getGenerateAllInvoices();
		$IN1_IDs = [];

		if ( $orders ) {
			$timeStamp = strtotime('now');
			foreach($orders as $order) {
				$model = new InvoiceModel();
				$model->load([
					'OR1_ID'              => $order["OR1_ID"],
					'CO1_ID'              => $order["CO1_ID"],
					'US1_ID'              => $order["OR1_ORDERED_BY"],
					'LO1_FROM_ID'         => $order["LO1_FROM_ID"],
					'LO1_TO_ID'           => $order["LO1_TO_ID"],
					'IN1_NUMBER'          => $order["OR1_NUMBER"],
					'IN1_STATUS'          => \app\models\Invoice::INVOICE_STATUS_OPEN,
					'IN1_SUM_STATUS'      => \app\models\Invoice::INVOICE_STATUS_OPEN,
					'IN1_TOTAL_VALUE'     => $order["OR1_TOTAL_VALUE"],
					'IN1_COMMENT'         => $order["OR1_COMMENT"]
				], '');

				if(!$model->save(true)) {
					$errors[] = $model->getErrorSummary(true);
				} else {
					$model = InvoiceModel::findOne(['IN1_ID'=>$model['IN1_ID']]);
					$model->IN1_DELETE_FLAG = $timeStamp;
					$model->save();
				}

				$IN1_IDs[] = $model["IN1_ID"];

				$orderLineModel = new \app\models\OrderLine();
				$orderLines = $orderLineModel->getLinesForGenerateInvoices($order['OR1_ID'], $timeStamp);

				foreach($orderLines as $orderLine) {
					$model = new InvoiceLine();

					$model->load([
						'IN1_ID'            => $orderLine["IN1_ID"],
						'OR2_ID'            => $orderLine["OR2_ID"],
						'IN2_LINE'          => $orderLine["OR2_LINE"],
						'IN2_STATUS'        => \app\models\OrderLine::ORDER_LINE_STATUS_OPEN,
						'IN2_INVOICE_QTY'   => $orderLine["OR2_ORDER_QTY"],
						'MD1_ID'            => $orderLine["MD1_ID"],
						'CA1_ID'            => $orderLine["CA1_ID"],
						'GL1_ID'            => $orderLine["GL1_ID"],
						'CO1_ID'            => $orderLine["CO1_ID"],
						'IN2_PART_NUMBER'   => $orderLine["MD1_PART_NUMBER"],
						'IN2_DESC1'         => $orderLine["MD1_DESC1"],
						'IN2_DESC2'         => $orderLine["MD1_DESC2"],
						'UM1_ID'            => $orderLine["UM1_ID"],
						'IN2_UNIT_PRICE'    => $orderLine["MD1_UNIT_PRICE"],
						'IN2_UM_SIZE'       => 0,
						'IN2_DELETE_FLAG'   => $timeStamp,
						'LO1_ID'            => $orderLine["LO1_TO_ID"],
						'IN2_TOTAL_VALUE'   => $orderLine["OR2_TOTAL_SELL"],
						'TE1_ID'            => '0'
					], '');

					if(!$model->save(true)) {
						$errors[] = $model->getErrorSummary(true);
					} else {
						$model = InvoiceLine::findOne(['IN2_ID'=>$model['IN2_ID']]);
						$model->IN2_DELETE_FLAG = $timeStamp;
						$model->save();
					}
				}

				$orderModel = \app\models\OrderHeader::findOne(['OR1_ID' => $order['OR1_ID']]);
				$orderModel->OR1_INV_STATUS = \app\models\OrderHeader::ORDER_STATUS_INVOICED;
				$orderModel->save();

				return [
					'data' => [
						'success'=> !$errors,
						'errors'=> $errors,
						'IN1_IDs' => $IN1_IDs
					]
				];
			}
		} else {
			return [
				'data' => [
					'success'=> false,
					'errors'=> false,
					'IN1_IDs' => $IN1_IDs
				]
			];
		}
	}
}