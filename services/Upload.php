<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 09/07/2018
 * Time: 10:13
 */

namespace app\services;

use app\models\File;
use Yii;
use yii\base\Component;

class Upload extends Component
{
    public function uploadImage($model, $files)
    {
	    $result = $model->uploadImage();

	    if ($result && !$result['data']['errors'] ) {
		    $file = new File();
		    $file->FI1_FILE_NAME = $result['data']['name'];
		    $file->FI1_FILE_PATH = $result['data']['path'];
		    $file->FI1_FILE_SIZE = $files[0]->size;
		    $file->FI1_FILE_TYPE = $files[0]->type == 'image/png' || $files[0]->type == 'image/jpeg' || $files[0]->type == 'image/gif' ? 1 : 2;

		    if ($file->save()) {
			    $result['data']['FI1_ID'] = $file->FI1_ID;
			    return $result;
		    } else {
			    $result['data']['success'] = false;
			    return $result;
		    }
		    return $result;
	    } else {
		    return $result;
	    }
    }
}