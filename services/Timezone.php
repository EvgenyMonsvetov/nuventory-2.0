<?php
namespace app\services;

use yii\base\Component;
use Yii;
use yii\helpers\ArrayHelper;
use app\models\TimeZone as TimeZoneModel;

class Timezone extends Component
{
    public function getAllTimezones()
    {
        $timezoneModel = new TimeZoneModel();
        $timezones = $timezoneModel->search()->all();
        return $timezones;
    }

}