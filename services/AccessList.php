<?php

namespace app\services;

use app\models\User;
use yii\base\Component;
use app\models\AccessList as AccessListModel;

class AccessList extends Component
{
    const ACCESS_FULL       = 2;
    const ACCESS_RESTRICTED = 1;
    const ACCESS_NO_ACCESS  = 0;

    public function getAccessList( $objectType, $objectId, $CO1_ID=null )
    {
        $users = User::find()->asArray()->where(['status' => User::STATUS_ACTIVE])->all();

        $filters = [
            'AL1_OBJECT_TYPE' => $objectType,
            'AL1_OBJECT_ID'   => $objectId
        ];

        if( $CO1_ID ){
            $filters['CO1_ID'] = $CO1_ID;
        }

        $aclItems = AccessListModel::find()->asArray()->where($filters)->all();

        $results = [];
        foreach( $users as $user){
            $hasAccess = false;
            foreach( $aclItems as $aclItem){
                if( $aclItem['user_id']  == $user['id']){
                    $hasAccess = true;
                    break;
                }
            }

            $results[] = [
                'user_id'   => $user['id'],
                'username'  => $user['username'],
                'email'     => $user['email'],
                'hasAccess' => $hasAccess
            ];
        }

        return $results;
    }

    public function updateAccessList( $objectType, $objectId, $users, $CO1_ID=null )
    {
        $filters = [
            'AL1_OBJECT_TYPE' => $objectType,
            'AL1_OBJECT_ID'   => $objectId
        ];

        if( $CO1_ID ){
            $filters['CO1_ID'] = $CO1_ID;
        }

        $aclItems = AccessListModel::find()->where($filters)->all();

        foreach($aclItems  as $aclItem){
            foreach($users as $user){
                if( $user['user_id'] == $aclItem->user_id){
                    if(!$user['hasAccess']){
                        $aclItem->delete();
                        if( $objectType == AccessListModel::OBJ_TYPE_COMPANY ){
                            $companyItems = AccessListModel::find()->where([
                                'AL1_OBJECT_TYPE' => [
                                    AccessListModel::OBJ_TYPE_CUSTOMER,
                                    AccessListModel::OBJ_TYPE_VENDOR
                                ],
                                'user_id'         => $user['user_id'],
                                'CO1_ID'          => $objectId
                            ])->all();

                            foreach( $companyItems as $companyItem){
                                $companyItem->delete();
                            }
                        }
                    }
                }
            }
        }

        $errors = [];
        foreach( $users as $user1 ){
            $useFound = false;
            foreach( $aclItems as $aclItem1 ){
                if( $user1['user_id'] == $aclItem1->user_id){
                    $useFound = true;
                }
            }

            if(!$useFound && $user1['hasAccess']){
                $accessListModel = new \app\models\AccessList();
                $accessListModel->user_id         = $user1['user_id'];
                $accessListModel->AL1_OBJECT_ID   = $objectId;
                $accessListModel->AL1_OBJECT_TYPE = $objectType;
                $accessListModel->CO1_ID          = $CO1_ID;

                if(!$accessListModel->save()){
                    $errors += $accessListModel->getErrors();
                }
            }
        }

        return $errors;
    }

    public function clear( $userId )
    {
        $aclItems = AccessListModel::find()->where(['user_id' => $userId])->all();
        foreach( $aclItems as $aclItem ){
             $aclItem->delete();
        }
    }
}