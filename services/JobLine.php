<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\JobLine as JobLineModel;

class JobLine extends Component
{
    public function createJobLine($data)
    {
        $errors = [];

        $data = Yii::$app->request->post();


        if (isset($data['JT2_ID']) && (int)$data['JT2_ID'] > 0) {
            $model = JobLineModel::findOne((int)$data['JT2_ID']);
            $model->JT2_ID = (int)$data['JT2_ID'];
            $data['JT2_CREATED_ON'] = $model->JT2_CREATED_ON;
            $data['JT2_MODIFIED_ON'] = $model->JT2_MODIFIED_ON;
        } else {
            unset($data['JT2_ID']);
            $model = new JobTicketModel();
        }

        if ( !($model->load($data, '') && $model->save( true )) ){
            $errors = $model->getErrorSummary(true);
        }

        return $errors;
    }

	public function deleteJobLinesForTicket($id)
    {
        $jobLineModels = JobLineModel::find()->where(["JT1_ID" => $id])->all();

	    foreach ($jobLineModels as $jobLineModel) {
		    $jobLineModel->JT2_DELETE_FLAG = 1;
		    $jobLineModel->save(false);
	    }
    }
}