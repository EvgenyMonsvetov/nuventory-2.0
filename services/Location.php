<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\Location as LocationModel;

class Location extends Component
{
    public function generateXls($params) {
        $filePrefix = 'Shops_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new LocationModel();
	    $query = $model->getSearchQuery($params);

	    $xlsService = new XLSService();
	    return $xlsService->generateXls($params, $query, $filename);
    }

	public function saveMaterialsBudget($params)
	{
		$user = Yii::$app->user->getIdentity();
		$LO1_ID = $user->LO1_ID;
		$errors = [];
		$model = LocationModel::find()->where(["LO1_ID" => $LO1_ID])->one();
		$model['LO1_MOUNTHLY_GROSS_SALE'] = $params['MOUNTHLY_GROSS_SALE'];
		$model['LO1_TARGET_PERCENTAGE'] = $params['TARGET_PERCENTAGE'];

		if ($model->save() === false) {
			$errors[] = $model->getErrors();
		}

		return [
			'data' => [
				'success' => !$errors,
				'errors'  => $errors
			]
		];
	}

	public function getLocationNumber()
	{
		$model = LocationModel::find()->alias('LO1')
			->select([
				'if (MAX(LO1_ID) IS NULL, 1, MAX(LO1_ID)+1) AS LO1_ID',
				'if (MAX(LO1_SHORT_CODE) IS NULL, 10000, MAX(LO1_SHORT_CODE)+1) AS LO1_SHORT_CODE'])
			->asArray()
			->one();

		return $model;
	}
}