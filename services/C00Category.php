<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\C00Category as C00CategoryModel;

class C00Category extends Component
{
    public function generateXls($params) {
        $filePrefix = 'NuventoryCategories_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

        $model = new C00CategoryModel();
	    $query = $model->getSearchQuery($params);

	    $xlsService = new XLSService();
	    return $xlsService->generateXls($params, $query, $filename);
    }
}