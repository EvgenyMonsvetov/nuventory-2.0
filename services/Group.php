<?php

namespace app\services;

use Yii;
use yii\base\Component;
use app\models\Group as GroupModel;
use app\models\User as UserModel;

class Group extends Component
{
    public function generateXls($params) {
        $filePrefix = 'Groups_';
        $currDate = Date('Ymd_His');
        $filename = $filePrefix . $currDate . '.xls' ;

	    $query = GroupModel::find();

	    $xlsService = new XLSService();
	    return $xlsService->generateXls($params, $query, $filename);
    }

    public function deleteGroup($params) {
	    $users = UserModel::findAll(['GR1_ID' => $params['id']]);

	    if (!$users) {
		    $group = GroupModel::findOne(['GR1_ID' => $params['id']]);
		    $group->GR1_DELETE_FLAG = 1;
		    $group->save(false);
	    }

	    return $users;
    }
}