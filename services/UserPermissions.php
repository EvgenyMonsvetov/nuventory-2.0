<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 14/06/2018
 * Time: 11:14
 */

namespace app\services;

use yii\base\Component;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\rbac\Permission;

class UserPermissions extends Component
{
    /**
     * @param $userId
     * @return array
     */
    public function getPermissions( $userId )
    {
        $auth = Yii::$app->getAuthManager();

        $userPermissions = array_map(function(Permission $permission){
            $data = ArrayHelper::toArray($permission);
            $data['inheritsFromRole'] = false;
            return $data;
        }, $auth->getPermissionsByUser($userId));

        $roles = $auth->getRolesByUser($userId);
        foreach($roles as $role){
            $rolePermissions = $auth->getPermissionsByRole( $role->name );
            foreach($rolePermissions as $rolePermission){
                foreach($userPermissions as &$userPermission){
                    if( $rolePermission->name == $userPermission['name']){
                        $userPermission['inheritsFromRole'] = true;
                    }
                }
            }
        }

        $allPermissions = array_map(function(Permission $permission) use ($userPermissions) {
            $item = ArrayHelper::toArray($permission);
            $foundPermission = false;
            foreach($userPermissions as $userPermission){
                if($userPermission['name'] == $item['name']){
                    $foundPermission = $userPermission;
                    break;
                }
            }
            $item['hasPermission']    = $foundPermission ? true : false;
            $item['inheritsFromRole'] = $foundPermission ? $foundPermission['inheritsFromRole'] : false;
            return $item;
        }, $auth->getPermissions());

        usort($allPermissions, function($a, $b){
            return strnatcmp($a['description'],$b['description']);

            /**
             *  if($a['inheritsFromRole'] && !$b['inheritsFromRole']){
                    return -1;
                }elseif($a['inheritsFromRole'] && $b['inheritsFromRole']){
                    return 0;
                }else{
                    return 1;
                }
             */
        });

        return $allPermissions;
    }

    public function updatePermissions( $userId, $permissions )
    {
        $auth = Yii::$app->getAuthManager();
        foreach($permissions as $userPermission){
            if(!$userPermission['inheritsFromRole']){
                $permission = $auth->getPermission($userPermission['name']);
                $hasAccess = $auth->checkAccess($userId, $permission->name);
                if($hasAccess && !$userPermission['hasPermission']){
                    $auth->revoke($permission, $userId);
                }elseif(!$hasAccess && $userPermission['hasPermission']){
                    $auth->assign($permission, $userId);
                }
            }
        }

        return true;
    }
}