<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 05/09/2018
 * Time: 12:11
 */

namespace app\services;

use app\components\DateTimeTrait;
use yii\base\Component;
use \app\models\ImportHistory as ImportHistoryModel;
use Yii;

class ImportHistory extends Component
{
    use DateTimeTrait;


    private function _getFilePath( ImportHistoryModel $model )
    {
        $baseDir = Yii::getAlias('@runtime').'/import';

        if( $model->IH1_TYPE == ImportHistoryModel::TYPE_TRANSACTIONS ){
            $baseDir .= '/transactions';
        }elseif($model->IH1_TYPE == ImportHistoryModel::TYPE_LOGS){
            $baseDir .= '/logs';
        }elseif($model->IH1_TYPE == ImportHistoryModel::TYPE_CUSTOMERS ){
            $baseDir .= '/customers';
        }elseif($model->IH1_TYPE == ImportHistoryModel::TYPE_VENDORS){
            $baseDir .= '/vendors';
        }

        $time = self::dateToInt($model->IH1_CREATED_ON);

        $importDir  = '/' . $model->CO1_ID;
        $importDir .= '/' . date('Y', $time);
        $importDir .= '/' . date('m', $time);
        $importDir .= '/' . date('d', $time);

        if(!is_dir($baseDir . $importDir )){
            mkdir($baseDir . $importDir, 0777, true);
        }

        $filepath = $baseDir . $importDir . DIRECTORY_SEPARATOR . date('h:i:s', $time).'.json';

        return $filepath;
    }
    public function saveFile( $importData, ImportHistoryModel $model )
    {
        $filepath = $this->_getFilePath( $model );

        $f = fopen( $filepath, 'w');
        fwrite( $f, json_encode($importData, JSON_PRETTY_PRINT) );
        fclose( $f );

        return str_replace(Yii::getAlias('@runtime'), '', $filepath);
    }

    public function getImportFileContent( ImportHistoryModel $model )
    {
        $filepath = $this->_getFilePath( $model );
        return file_get_contents( $filepath );
    }
}