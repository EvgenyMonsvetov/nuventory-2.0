<?php

namespace app\commands;

use Yii;
use yii\console\Controller;


class UserController extends Controller
{

    public function actionPasswordHash()
    {
        $users = \app\models\User::find()->where(['DELETED' => 0, 'US1_ACTIVE' => 1, 'US1_PASSWORD_HASH' => ''])->all();
        if(empty($users)) {
            echo "users not found without password hash";
        } else {
            foreach( $users as $user ){
                $user->US1_PASSWORD_HASH = Yii::$app->security->generatePasswordHash($user->US1_PASS);
                $user->US1_AUTH_KEY      = Yii::$app->security->generateRandomString();
                if($user->save()){
                    echo "for {$user->id} added password hash\n";
                }else {
                    Yii::error($user->getErrors());
                }
            }
        }
    }

}
