<?php
namespace app\components;
use Yii;

trait PdfTrait
{
    /**
     * send generated PDF file to the customer
     * @param  string $filename, TCPDF $pdf
     * @return application/pdf file type
     */
    public function sendPdfFile(\TCPDF $pdf, $fileName)
    {
	    header('Content-Type: application/pdf');
	    header('Content-Disposition: attachment;filename=' . $fileName);
	    header('Cache-Control: max-age=0');
	    // If you're serving to IE 9, then the following may be needed
	    header('Cache-Control: max-age=1');

	    // If you're serving to IE over SSL, then the following may be needed
	    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	    header('Pragma: public'); // HTTP/1.0
	    Yii::$app->response->isSent = true;

	    //Close and output PDF document
	    $pdf->Output($fileName, 'I');
    }
}