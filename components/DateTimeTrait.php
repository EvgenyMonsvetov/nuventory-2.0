<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 06/09/2018
 * Time: 12:07
 */


namespace app\components;

/**
 * Trait to handle JWT-authorization process. Should be attached to User model.
 * If there are many applications using user model in different ways - best way
 * is to use this trait only in the JWT related part.
 */
trait DateTimeTrait
{
    public static function dateToInt($dateStr, $timeStr = '', $utc=false)
    {
		$udatetime = 0;
		if ($dateStr) {
			$d = date_parse($dateStr);

           $h = $m = $s = 0;
			if ($timeStr) {
				$tt = strtotime($timeStr);
        		$h  = strftime('%H', $tt);
            	$m  = strftime('%M', $tt);
                $s  = strftime('%S', $tt);
			} else if ($d['hour']!=0||$d['minute']!=0||$d['second']!=0){
        		$h  = $d['hour'];
            	$m  = $d['minute'];
                $s  = $d['second'];
            }

            if($d['year'] == 2099){
                $d['year'] = date('Y', time()) + 20;
            }

            if($utc){
                $udatetime = gmmktime($h, $m, $s, $d['month'], $d['day'], $d['year']);
            }else{
                $udatetime = mktime($h, $m, $s, $d['month'], $d['day'], $d['year']);
            }
		}
		return (int) $udatetime;
    }
}