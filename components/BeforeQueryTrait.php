<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 27/06/2018
 * Time: 13:56
 */

namespace app\components;

trait BeforeQueryTrait{

    static $EVENT_BEFORE_QIUERY = 'EVENT_BEFORE_QIUERY';

    private static function _trigger($activeQuery){
        $model = new static();
        $event = new BeforeQueryEvent([
            'activeQuery' => $activeQuery
        ]);
        $model->trigger(self::$EVENT_BEFORE_QIUERY, $event);
        return $activeQuery;
    }

    public static function find() {
        return self::_trigger( parent::find() );
    }

    public static function findOne($condition) {
        return self::_trigger( parent::findOne($condition) );
    }

    public static function findByCondition($condition){
        return self::_trigger( parent::findByCondition($condition) );
    }
}