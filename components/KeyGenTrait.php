<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 31/10/2018
 * Time: 15:55
 */

namespace app\components;

use yii\db\ActiveRecord;

/**
 * Trait to handle JWT-authorization process. Should be attached to User model.
 * If there are many applications using user model in different ways - best way
 * is to use this trait only in the JWT related part.
 */
trait KeyGenTrait
{
    public function generateKey(ActiveRecord $model, $field)
    {
        $intnce = $model::find()->select(['newkey' => 'max('.$field.') + 1'])->asArray()->one();
        return (int)$intnce['newkey'];
    }
}