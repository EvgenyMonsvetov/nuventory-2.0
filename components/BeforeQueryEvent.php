<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 27/06/2018
 * Time: 14:24
 */

namespace app\components;

use yii\base\Event;
use yii\db\ActiveQuery;

/**
 * This event class is used for Events triggered by the [[User]] class.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BeforeQueryEvent extends Event
{
    /**
     * @var ActiveQuery the activeQuery object associated with this event
     */
    public $activeQuery;

    /**
     * @return ActiveQuery
     */
    public function getActiveQuery(){

        return $this->activeQuery;
    }
}