<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 31/05/2018
 * Time: 12:37
 */

namespace app\components;

use yii\filters\auth\HttpBearerAuth;
use \yii\rest\Controller;
use Yii;
/**
 * Controller yang digunakan di app extend dari \yii\rest\Controller
 *
 * @author Muhamad Alfan <muhamad.alfan01@gmail.com>
 * @since 1.0
 */
class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];
        $behaviors['authenticator']['except'] = ['options'];

        Yii::$app->response->getHeaders()->add('Access-Control-Allow-Origin', '*');
        Yii::$app->response->getHeaders()->add('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT');
        Yii::$app->response->getHeaders()->add('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding');

        return $behaviors;
    }

    public function actionOptions(){}
}