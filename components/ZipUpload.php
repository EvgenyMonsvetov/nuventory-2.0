<?php
namespace app\components;

use yii\base\Component;
use yii\web\UploadedFile;
use Yii;
use yii\helpers\Url;
use yii\helpers\FileHelper;

class ZipUpload extends Component
{
    /**
     * @var UploadedFile
     */
    public $fileKey;

    public function rules()
    {
        return [
            [['fileKey'], 'file', 'maxSize' => Yii::$app->params['maxUploadZipFileSize'], 'tooBig' => 'Limit is ' . Yii::$app->params['maxUploadZipFileSize'], 'skipOnEmpty' => false, 'extensions' => 'zip']
        ];
    }

	public function uploadZipImages()
	{
		$errors = [];
		$filename = $this->fileKey->name;
		$basePath = Yii::getAlias('@uploadImagesArchiveFolder') !== null ? Yii::getAlias('@uploadImagesArchiveFolder') : '';

		if ($basePath) {
			FileHelper::createDirectory($basePath, $mode = 0777, $recursive = true);
		}

		if (!$this->fileKey->saveAs($basePath . DIRECTORY_SEPARATOR . $filename)) {
			$errors = $this->getErrors();
		}

		return [
			'data' => [
				'name' => $filename,
				'path' => $basePath . DIRECTORY_SEPARATOR . $filename,
				'imageUrl' => $errors ? '' : Url::base(true) . DIRECTORY_SEPARATOR . $basePath . $filename,
				'success' => !$errors,
				'errors' => $errors
			]
		];
	}


	public function copyImageToAppImageFolder($folderZip, $imgName, $destinationPath = null)
	{
		$errors = [];
		$currDate = Date('YmdHis');

		if ($destinationPath) {
			$basePath = Yii::getAlias('@partsImagesPath') !== null ? Yii::getAlias('@partsImagesPath') . DIRECTORY_SEPARATOR : '';
		} else {
			$basePath = Yii::getAlias('@partsImagesPath') !== null && isset(Yii::$app->params['imagesFolderName']) ? Yii::getAlias('@partsImagesPath')
				. DIRECTORY_SEPARATOR . Yii::$app->params['imagesFolderName'] . DIRECTORY_SEPARATOR . $currDate . DIRECTORY_SEPARATOR : '';
		}

		if (Yii::getAlias('@partsImagesPath') !== null && !file_exists(Yii::getAlias('@partsImagesPath'))) {
			FileHelper::createDirectory(Yii::getAlias('@partsImagesPath'), $mode = 0777, $recursive = true);
		}

		if ($basePath) {
			FileHelper::createDirectory($basePath, $mode = 0777, $recursive = true);
		}

		if ($destinationPath) {
			FileHelper::unlink(Yii::getAlias('@partsImagesPath') . DIRECTORY_SEPARATOR . $destinationPath );
			$copyResult = copy($folderZip, $basePath . $destinationPath);
		} else {
			$copyResult = copy($folderZip, $basePath . $imgName);
		}

		if (!$copyResult) {
			$errors = ['Cannot save image'];
		}

		return [
			'data' => [
				'name' => $imgName,
				'path' => $destinationPath ? $destinationPath : Yii::$app->params['imagesFolderName'] . DIRECTORY_SEPARATOR . $currDate . DIRECTORY_SEPARATOR . $imgName,
				'imageUrl' => $destinationPath ? Url::base(true) . DIRECTORY_SEPARATOR . $basePath . $destinationPath : Url::base(true) . DIRECTORY_SEPARATOR . $basePath . $imgName,
				'success' => !$errors,
				'errors' => $errors
			]
		];
	}
}