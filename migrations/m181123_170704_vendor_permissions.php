<?php

use yii\db\Migration;

/**
 * Class m181116_152756_languages_permissions
 */
class m181123_170704_vendor_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'VENDORS_ADD',
            'description' => 'Allow to add new vendor',
        ],[
            'name'        => 'VENDORS_EDIT',
            'description' => 'Allow to edit a vendor',
        ],[
            'name'        => 'VENDORS_DELETE',
            'description' => 'Allow to delete vendor',
        ],[
            'name'        => 'VENDORS_READ',
            'description' => 'Allow to read list of vendors',
        ]
    ];

    /**
     * {@inheritdoc}
     */
   public function safeUp()
   {
       $auth = Yii::$app->authManager;
       $role = $auth->getRole('admin');
       foreach ($this->_permissions as $permission) {
           $authPermission = $auth->createPermission($permission['name']);
           $authPermission->description = $permission['description'];
           $auth->remove($authPermission); //remove if already exist
           $auth->add($authPermission);
           $auth->addChild($role, $authPermission);
       }
       return true;
   }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }

}
