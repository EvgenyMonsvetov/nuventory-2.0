<?php

use yii\db\Schema;
use yii\db\Migration;

class m190328_091330_groups_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'GROUPS_ADD',
            'description' => 'Allow to add a new group'
        ],[
            'name'        => 'GROUPS_DELETE',
            'description' => 'Allow to delete a group'
        ],[
            'name'        => 'GROUPS_EDIT',
            'description' => 'Allow to edit a group'
        ],[
            'name'        => 'GROUPS_READ',
            'description' => 'Allow to read list of groups'
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
