<?php

use yii\db\Migration;

/**
 * Class m191024_150426_user_active_status
 */
class m191024_150426_user_active_status extends Migration
{
    /**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->execute('ALTER TABLE `us1_user` ADD COLUMN `US1_ACTIVE` tinyint(1) DEFAULT 1;');
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
    {
        $this->execute('ALTER TABLE `us1_user` DROP COLUMN `US1_ACTIVE`;');
        return true;
    }
}
