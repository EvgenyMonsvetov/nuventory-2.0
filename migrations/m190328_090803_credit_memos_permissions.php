<?php

use yii\db\Schema;
use yii\db\Migration;

class m190328_090803_credit_memos_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'CREDIT_MEMOS_ADD',
            'description' => 'Allow to add a new credit memo'
        ],[
            'name'        => 'CREDIT_MEMOS_DELETE',
            'description' => 'Allow to delete a credit memo'
        ],[
            'name'        => 'CREDIT_MEMOS_READ',
            'description' => 'Allow to read list of credit memos'
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
