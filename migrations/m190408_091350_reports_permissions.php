<?php

use yii\db\Schema;
use yii\db\Migration;

class m190408_091350_reports_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'SALES_READ',
            'description' => 'Allow to read Sales Report',
        ],[
            'name'        => 'GROSS_PROFIT_READ',
            'description' => 'Allow to read Gross Profit Report',
        ],[
            'name'        => 'SUMMARY_READ',
            'description' => 'Allow to read Summary Report',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
