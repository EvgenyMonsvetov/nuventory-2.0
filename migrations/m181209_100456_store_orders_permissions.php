<?php

use yii\db\Schema;
use yii\db\Migration;

class m181209_100456_store_orders_permissions extends Migration
{

    private $_permissions = [
        [
            'name'        => 'STORE_ORDERS_ADD',
            'description' => 'Allow to add new store order',
        ],[
            'name'        => 'STORE_ORDERS_EDIT',
            'description' => 'Allow to edit a store order',
        ],[
            'name'        => 'STORE_ORDERS_DELETE',
            'description' => 'Allow to delete store order',
        ],[
            'name'        => 'STORE_ORDERS_READ',
            'description' => 'Allow to read list of store orders',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
