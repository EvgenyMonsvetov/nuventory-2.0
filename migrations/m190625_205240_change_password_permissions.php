<?php

use yii\db\Migration;

/**
 * Class m190625_205240_change_password_permissions
 */
class m190625_205240_change_password_permissions extends Migration
{
     /**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
	    $auth = Yii::$app->authManager;
		$adminRole = $auth->getRole('admin');

		$changePasswordPermission = $auth->createPermission('CHANGE_PASSWORD');
		$changePasswordPermission->description = 'Gives an access to change password for particular user';
		$auth->add($changePasswordPermission);
		$auth->addChild($adminRole, $changePasswordPermission);

		$adminChangePasswordPermission = $auth->createPermission('ADMIN_CHANGE_PASSWORD');
		$adminChangePasswordPermission->description = 'Gives an access to change password for all user';
		$auth->add($adminChangePasswordPermission);
		$auth->addChild($adminRole, $adminChangePasswordPermission);

		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$auth = Yii::$app->authManager;

		$changePasswordPermission = $auth->getPermission('CHANGE_PASSWORD');
		$adminChangePasswordPermission = $auth->getPermission('ADMIN_CHANGE_PASSWORD');

		$auth->remove($changePasswordPermission);
		$auth->remove($adminChangePasswordPermission);

		return true;
	}
}
