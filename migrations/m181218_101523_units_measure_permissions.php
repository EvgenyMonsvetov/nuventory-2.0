<?php

use yii\db\Migration;

/**
 * Class m181218_101523_units_measure_permissions
 */
class m181218_101523_units_measure_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'UNITS_MEASURE_ADD',
            'description' => 'Allow to add new units of measure',
        ],[
            'name'        => 'UNITS_MEASURE_EDIT',
            'description' => 'Allow to edit units of measure',
        ],[
            'name'        => 'UNITS_MEASURE_DELETE',
            'description' => 'Allow to delete units of measure',
        ],[
            'name'        => 'UNITS_MEASURE_READ',
            'description' => 'Allow to read list units of measure',
        ]
    ];
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
