<?php

use yii\db\Migration;

/**
 * Class m181213_232408_shops_permissions
 */
class m181213_232408_shops_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'SHOPS_ADD',
            'description' => 'Allow to add a new shop',
        ],[
            'name'        => 'SHOPS_EDIT',
            'description' => 'Allow to edit a shop',
        ],[
            'name'        => 'SHOPS_DELETE',
            'description' => 'Allow to delete a shop',
        ],[
            'name'        => 'SHOPS_READ',
            'description' => 'Allow to read list of shops',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
