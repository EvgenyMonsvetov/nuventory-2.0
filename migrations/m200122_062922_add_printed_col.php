<?php

use yii\db\Migration;

/**
 * Class m200122_062922_add_printed_col
 */
class m200122_062922_add_printed_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `jt1_job_ticket` ADD COLUMN `JT1_PRINTED` tinyint(1) NOT NULL DEFAULT 0 AFTER `JT1_HOUR`;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jt1_job_ticket', 'JT1_PRINTED');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200122_062922_add_printed_col cannot be reverted.\n";

        return false;
    }
    */
}
