<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 31/05/2018
 * Time: 15:21
 */

use yii\db\Migration;

class m170823_080921_users extends Migration
{
    public function up()
    {
        $this->execute(
         'ALTER TABLE `us1_user` 
                ADD COLUMN `US1_PASSWORD_HASH` VARCHAR(255) NULL DEFAULT NULL AFTER `US1_PASS`,
                ADD COLUMN `US1_AUTH_KEY` VARCHAR(32) NULL DEFAULT NULL AFTER `US1_PASSWORD_HASH`,
                ADD COLUMN `US1_PASSWORD_RESET_TOKEN` VARCHAR(255) NULL DEFAULT NULL AFTER `US1_AUTH_KEY`;');

        return true;
    }
    public function down()
    {
        return true;
    }
}