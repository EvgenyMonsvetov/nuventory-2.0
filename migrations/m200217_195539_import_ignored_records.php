<?php

use yii\db\Migration;

/**
 * Class m200217_195539_import_ignored_records
 */
class m200217_195539_import_ignored_records extends Migration
{
    /**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		//$this->execute('ALTER TABLE `im1_import` ADD COLUMN `im1_ignored_records` int(11) DEFAULT 0;');
        //$this->execute('ALTER TABLE `im1_import` ADD COLUMN `im1_processed_records` int(11) DEFAULT 0;');
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
    {
        //$this->execute('ALTER TABLE `im1_import` DROP COLUMN `im1_ignored_records`;');
        //$this->execute('ALTER TABLE `im1_import` DROP COLUMN `im1_processed_records`;');
        return true;
    }
}
