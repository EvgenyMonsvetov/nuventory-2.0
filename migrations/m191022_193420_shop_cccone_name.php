<?php

use yii\db\Migration;

/**
 * Class m191022_193420_shop_cccone_name
 */
class m191022_193420_shop_cccone_name extends Migration
{
    /**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->execute('ALTER TABLE `lo1_location` ADD COLUMN `LO1_CCCONE_NAME` varchar(50) NULL;');
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
    {
        $this->execute('ALTER TABLE `lo1_location` DROP COLUMN `LO1_CCCONE_NAME`;');
        return true;
    }
}
