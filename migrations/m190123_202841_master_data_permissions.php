<?php

use yii\db\Migration;

/**
 * Class m190123_202841_master_data_permissions
 */
class m190123_202841_master_data_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'MASTER_DATA_ADD',
            'description' => 'Allow to add a new master_data',
        ],[
            'name'        => 'MASTER_DATA_EDIT',
            'description' => 'Allow to edit a master_data',
        ],[
            'name'        => 'MASTER_DATA_DELETE',
            'description' => 'Allow to delete a master_data',
        ],[
            'name'        => 'MASTER_DATA_READ',
            'description' => 'Allow to read list of master_data',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
