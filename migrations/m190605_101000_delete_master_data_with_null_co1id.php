<?php

use yii\db\Schema;
use yii\db\Migration;

class m190605_101000_delete_master_data_with_null_co1id extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->execute('UPDATE `md1_master_data` SET `MD1_DELETE_FLAG` = 1 WHERE `CO1_ID` IS NULL;');

		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		return true;
	}
}
