<?php

use yii\db\Migration;

/**
 * Class m191024_162701_tech_active_status
 */
class m191024_162701_tech_active_status extends Migration
{
    /**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->execute('ALTER TABLE `te1_technician` ADD COLUMN `TE1_ACTIVE` tinyint(1) DEFAULT 1;');
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
    {
        $this->execute('ALTER TABLE `te1_technician` DROP COLUMN `TE1_ACTIVE`;');
        return true;
    }
}
