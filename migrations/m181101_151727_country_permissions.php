<?php

use yii\db\Migration;

/**
 * Class m181101_151727_country_permissions
 */
class m181101_151727_country_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'COUNTRIES_ADD',
            'description' => 'Allow to add new language',
        ],[
            'name'        => 'COUNTRIES_DELETE',
            'description' => 'Allow to edit a language',
        ],[
            'name'        => 'COUNTRIES_EDIT',
            'description' => 'Allow to delete language',
        ],[
            'name'        => 'COUNTRIES_READ',
            'description' => 'Allow to read list of languages',
        ],[
            'name'        => 'TIMEZONES_READ',
            'description' => 'Allow to read list of timezones',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }

}
