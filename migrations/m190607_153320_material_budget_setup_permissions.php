<?php

use yii\db\Schema;
use yii\db\Migration;

class m190607_153320_material_budget_setup_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'MATERIAL_BUDGET_SETUP_READ',
            'description' => 'Allow to read material budget setup'
        ],
	    [
		    'name'        => 'MATERIAL_BUDGET_SETUP_EDIT',
		    'description' => 'Allow to edit material budget setup'
	    ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
