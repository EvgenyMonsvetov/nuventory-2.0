<?php

use yii\db\Migration;

/**
 * Class m181218_112808_nuventory_categories_permissions
 */
class m181218_112808_nuventory_categories_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'NUVENTORY_CATEGORIES_ADD',
            'description' => 'Allow to add a new nuventory category',
        ],[
            'name'        => 'NUVENTORY_CATEGORIES_EDIT',
            'description' => 'Allow to edit a nuventory category',
        ],[
            'name'        => 'NUVENTORY_CATEGORIES_DELETE',
            'description' => 'Allow to delete a nuventory category',
        ],[
            'name'        => 'NUVENTORY_CATEGORIES_READ',
            'description' => 'Allow to read list of nuventory categories',
        ],[
		    'name'        => 'NUVENTORY_CATEGORIES_ASSIGN',
		    'description' => 'Allow to assign nuventory category'
	    ]
    ];
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
