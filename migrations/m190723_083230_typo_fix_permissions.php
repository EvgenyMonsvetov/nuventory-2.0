<?php

use yii\db\Migration;

/**
 * Class m190723_083230_typo_fix_permissions
 */
class m190723_083230_typo_fix_permissions extends Migration
{
	private $_permissions = [
		[
			'name'        => 'STATEMENTS_DELETE',
			'description' => 'Allow to delete of statement',
		],[
			'name'        => 'STATEMENTS_READ',
			'description' => 'Allow to read list of statements',
		],[
			'name'        => 'STATEMENTS_GENERATE',
			'description' => 'Allow to generate statement'
		],[
			'name'        => 'JOB_INVOICES_READ',
			'description' => 'Allow to read list of job invoices',
		]
	];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $auth = Yii::$app->authManager;
	    foreach ($this->_permissions as $permission) {
		    $authPermission = $auth->getPermission($permission['name']);
		    $authPermission->description = $permission['description'];
		    $auth->update($permission['name'], $authPermission);
	    }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
