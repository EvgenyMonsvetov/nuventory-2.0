<?php

use yii\db\Migration;

/**
 * Class m181016_180337_lang_locale
 */
class m181016_180337_lang_locale extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `la1_language` ADD COLUMN `LA1_LOCALE` VARCHAR(10) NULL AFTER `LA1_ID`;');
        $this->execute('UPDATE `la1_language` SET `LA1_LOCALE`="en-us" WHERE `LA1_ID`="1";');
        $this->execute('UPDATE `la1_language` SET `LA1_LOCALE`="fr-fr" WHERE `LA1_ID`="2";');
        $this->execute('UPDATE `la1_language` SET `LA1_LOCALE`="es-es" WHERE `LA1_ID`="3";');
        $this->execute('UPDATE `la1_language` SET `LA1_LOCALE`="de-de" WHERE `LA1_ID`="4";');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
