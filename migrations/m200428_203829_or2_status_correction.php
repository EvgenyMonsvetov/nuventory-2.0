<?php

use yii\db\Migration;

/**
 * Class m200428_203829_or2_status_correction
 */
class m200428_203829_or2_status_correction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            Update or2_order_line  as or2
               left join or1_order_header as or1 On or1.OR1_ID = or2.OR1_ID 
            SET or2.OR2_STATUS = 2
            Where or1.OR1_STATUS = 2 and or2.OR2_STATUS <> 2
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200428_203829_or2_status_correction cannot be reverted.\n";

        return false;
    }
    */
}
