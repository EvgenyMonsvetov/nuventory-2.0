<?php

use yii\db\Migration;

/**
 * Class m190916_181240_fix_nuventory_category_permissions
 */
class m190916_181240_fix_nuventory_category_permissions extends Migration
{
	private $_permissions = [
		[
			'name'        => 'NUVENTORY_CATEGORY_READ',
			'description' => 'Allow to read nuventory categories'
		],
		[
			'name'        => 'NUVENTORY_CATEGORY_ADD',
			'description' => 'Allow to add new nuventory category'
		],
		[
			'name'        => 'NUVENTORY_CATEGORY_EDIT',
			'description' => 'Allow to edit nuventory category'
		],
		[
			'name'        => 'NUVENTORY_CATEGORY_DELETE',
			'description' => 'Allow to delete nuventory category'
		],
		[
			'name'        => 'NUVENTORY_CATEGORY_ASSIGN',
			'description' => 'Allow to assign nuventory category'
		]
	];

	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$auth = Yii::$app->authManager;

		foreach($this->_permissions as $permission){
			try {
				$authPermission = $auth->getPermission($permission['name']);
				if ($authPermission) {
					$auth->remove($authPermission);
				}
			}
			catch(CDbException $e) {
				\yii\helpers\VarDumper::dump($e->getMessage());
			}
		}

		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$auth = Yii::$app->authManager;
		$role = $auth->getRole('admin');
		foreach ($this->_permissions as $permission) {
			$authPermission = $auth->createPermission($permission['name']);
			$authPermission->description = $permission['description'];
			$auth->remove($authPermission); //remove if already exist
			$auth->add($authPermission);
			$auth->addChild($role, $authPermission);
		}
		return true;
	}
}
