<?php

use yii\db\Schema;
use yii\db\Migration;

class m181127_124825_master_catalog_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'MASTER_CATALOG_ADD',
            'description' => 'Allow to add new master catalog',
        ],[
            'name'        => 'MASTER_CATALOG_EDIT',
            'description' => 'Allow to edit a master catalog',
        ],[
            'name'        => 'MASTER_CATALOG_DELETE',
            'description' => 'Allow to delete master catalog',
        ],[
            'name'        => 'MASTER_CATALOG_READ',
            'description' => 'Allow to read list of master catalogs',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
