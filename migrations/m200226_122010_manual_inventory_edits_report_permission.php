<?php

use yii\db\Migration;

/**
 * Class m200226_122010_manual_inventory_edits_report_permission
 */
class m200226_122010_manual_inventory_edits_report_permission extends Migration
{
	private $_permissions = [
		[
			'name'        => 'MANUAL_INVENTORY_EDITS_READ',
			'description' => 'Allow to show manual inventory edits report'
		]
	];

	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$auth = Yii::$app->authManager;
		$role = $auth->getRole('admin');
		foreach ($this->_permissions as $permission) {
			$authPermission = $auth->createPermission($permission['name']);
			$authPermission->description = $permission['description'];
			$auth->remove($authPermission); //remove if already exist
			$auth->add($authPermission);
			$auth->addChild($role, $authPermission);
		}
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$auth = Yii::$app->authManager;

		foreach($this->_permissions as $permission){
			try {
				$authPermission = $auth->getPermission($permission['name']);
				$auth->remove($authPermission);
			}
			catch(CDbException $e) {
				\yii\helpers\VarDumper::dump($e->getMessage());
			}
		}

		return true;
	}
}
