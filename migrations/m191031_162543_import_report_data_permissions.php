<?php

use yii\db\Migration;

/**
 * Class m191031_162543_import_permissions
 */
class m191031_162543_import_report_data_permissions extends Migration
{
    private $_permissions = [[
        'name'        => 'IMPORT_JOB_COST_SUMMARY',
        'description' => 'Allow to import a job cost summary data',
    ], [
        'name'        => 'IMPORT_SALES_JOURNAL',
        'description' => 'Allow to import a sales journal',
    ], [
        'name'        => 'IMPORT_REPORT_DATA_READ',
        'description' => 'Allow to read list of import report data records',
    ]];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
		$role = $auth->getRole('admin');
		foreach($this->_permissions as $permission){
			$authPermission = $auth->createPermission($permission['name']);
			$authPermission->description = $permission['description'];
			$auth->remove($authPermission); //remove if already exist
			$auth->add($authPermission);
			$auth->addChild($role, $authPermission);
		}
		return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

		foreach($this->_permissions as $permission){
			try {
				$authPermission = $auth->getPermission($permission['name']);
				if($authPermission){
				    $auth->remove($authPermission);
                }
			}
			catch(CDbException $e) {
				\yii\helpers\VarDumper::dump($e->getMessage());
			}
		}

		return true;
    }
}
