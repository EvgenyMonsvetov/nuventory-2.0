<?php

use yii\db\Schema;
use yii\db\Migration;

class m181220_124716_order_lines_permissions extends Migration
{

    private $_permissions = [
        [
            'name'        => 'ORDER_LINE_ADD',
            'description' => 'Allow to add new order line',
        ],[
            'name'        => 'ORDER_LINE_EDIT',
            'description' => 'Allow to edit a order line',
        ],[
            'name'        => 'ORDER_LINE_DELETE',
            'description' => 'Allow to delete order line',
        ],[
            'name'        => 'ORDER_LINE_READ',
            'description' => 'Allow to read list of order lines',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
