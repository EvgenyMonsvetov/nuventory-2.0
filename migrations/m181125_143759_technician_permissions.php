<?php

use yii\db\Migration;

class m181125_143759_technician_permissions extends Migration
{

    private $_permissions = [
        [
            'name'        => 'TECHNICIANS_ADD',
            'description' => 'Allow to add new technician',
        ],[
            'name'        => 'TECHNICIANS_EDIT',
            'description' => 'Allow to edit a technician',
        ],[
            'name'        => 'TECHNICIANS_DELETE',
            'description' => 'Allow to delete technician',
        ],[
            'name'        => 'TECHNICIANS_READ',
            'description' => 'Allow to read list of technicians',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
