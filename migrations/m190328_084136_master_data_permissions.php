<?php

use yii\db\Schema;
use yii\db\Migration;

class m190328_084136_master_data_permissions extends Migration
{

    private $_permissions = [
        [
            'name'        => 'MASTER_DATA_SHOW_ALL_PARTS',
            'description' => 'Allow to show all parts, not only active'
        ],
        [
            'name'        => 'MASTER_DATA_ASSIGN',
            'description' => 'Allow to assign master data parts'
        ],
        [
            'name'        => 'MASTER_DATA_IMPORT',
            'description' => 'Allow to import list of master data'
        ],
        [
            'name'        => 'MASTER_DATA_LINK_CATALOG',
            'description' => 'Allow to link master data catalogs'
        ],
        [
            'name'        => 'MASTER_DATA_LINK_MANUFACTURER_CATALOG',
            'description' => 'Allow to link master data manufacturer catalogs'
        ],
        [
            'name'        => 'MASTER_DATA_UPDATE_LINKED_PARTS',
            'description' => 'Allow to update linked parts master data'
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
