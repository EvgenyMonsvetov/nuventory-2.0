<?php

use yii\db\Migration;

/**
 * Class m191219_195327_import_location_id
 */
class m191219_195327_import_location_id extends Migration
{
    /**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		//$this->execute('ALTER TABLE `im1_import` ADD COLUMN `im1_lo1_id` int(11) DEFAULT NULL;');
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
    {
        //$this->execute('ALTER TABLE `im1_import` DROP COLUMN `im1_lo1_id`;');
        return true;
    }
}
