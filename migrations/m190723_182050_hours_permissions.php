<?php

use yii\db\Migration;

/**
 * Class m190723_182050_hours_permissions
 */
class m190723_182050_hours_permissions extends Migration
{
    private $_permissions = [
	    [
		    'name'        => 'HOURS_ADD',
		    'description' => 'Allow to add a new hour',
	    ],
	    [
		    'name'        => 'HOURS_READ',
		    'description' => 'Allow to read list of hours',
	    ],
	    [
		    'name'        => 'HOURS_EDIT',
		    'description' => 'Allow to edit an hour'
	    ]

    ];

	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$auth = Yii::$app->authManager;
		$role = $auth->getRole('admin');
		foreach($this->_permissions as $permission){
			$authPermission = $auth->createPermission($permission['name']);
			$authPermission->description = $permission['description'];
			$auth->remove($authPermission); //remove if already exist
			$auth->add($authPermission);
			$auth->addChild($role, $authPermission);
		}
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$auth = Yii::$app->authManager;

		foreach($this->_permissions as $permission){
			try {
				$authPermission = $auth->getPermission($permission['name']);
				$auth->remove($authPermission);
			}
			catch(CDbException $e) {
				\yii\helpers\VarDumper::dump($e->getMessage());
			}
		}

		return true;
	}
}
