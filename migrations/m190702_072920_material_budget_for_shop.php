<?php

use yii\db\Migration;

/**
 * Class m190625_205240_change_password_permissions
 */
class m190702_072920_material_budget_for_shop extends Migration
{
     /**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->execute('ALTER TABLE `lo1_location` ADD COLUMN `LO1_MOUNTHLY_GROSS_SALE` DECIMAL(15,5) NOT NULL DEFAULT 0;');
		$this->execute('ALTER TABLE `lo1_location` ADD COLUMN `LO1_TARGET_PERCENTAGE` DECIMAL(15,5) NOT NULL DEFAULT 0;');
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->execute('ALTER TABLE `lo1_location` DROP COLUMN `LO1_MOUNTHLY_GROSS_SALE`;');
		$this->execute('ALTER TABLE `lo1_location` DROP COLUMN `LO1_TARGET_PERCENTAGE`;');
		return true;
	}
}
