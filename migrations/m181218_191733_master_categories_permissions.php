<?php

use yii\db\Migration;

/**
 * Class m181218_191733_master_categories_permissions
 */
class m181218_191733_master_categories_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'MASTER_CATEGORIES_ADD',
            'description' => 'Allow to add a new master category',
        ],[
            'name'        => 'MASTER_CATEGORIES_EDIT',
            'description' => 'Allow to edit a master category',
        ],[
            'name'        => 'MASTER_CATEGORIES_DELETE',
            'description' => 'Allow to delete a master category',
        ],[
            'name'        => 'MASTER_CATEGORIES_READ',
            'description' => 'Allow to read list of master categories',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
