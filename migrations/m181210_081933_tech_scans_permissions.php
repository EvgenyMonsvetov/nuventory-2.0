<?php

use yii\db\Schema;
use yii\db\Migration;

class m181210_081933_tech_scans_permissions extends Migration
{

    private $_permissions = [
        [
            'name'        => 'TECH_SCANS_ADD',
            'description' => 'Allow to add new tech scan',
        ],[
            'name'        => 'TECH_SCANS_EDIT',
            'description' => 'Allow to edit a tech scan',
        ],[
            'name'        => 'TECH_SCANS_DELETE',
            'description' => 'Allow to delete tech scan',
        ],[
            'name'        => 'TECH_SCANS_READ',
            'description' => 'Allow to read list of tech scans',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
