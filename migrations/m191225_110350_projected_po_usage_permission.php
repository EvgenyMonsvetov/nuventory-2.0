<?php

use yii\db\Migration;

/**
 * Class m191225_110350_projected_po_usage_permission
 */
class m191225_110350_projected_po_usage_permission extends Migration
{
    private $_permissions = [
        [
            'name'        => 'PROJECTED_PO_USAGE_READ',
            'description' => 'Allow to Projected PO Usage Report',
        ]
    ];

    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }

}
