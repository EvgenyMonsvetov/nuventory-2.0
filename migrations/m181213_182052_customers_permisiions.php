<?php

use yii\db\Migration;

/**
 * Class m181213_182052_customers_permisiions
 */
class m181213_182052_customers_permisiions extends Migration
{
    private $_permissions = [
        [
            'name' => 'CUSTOMERS_ADD',
            'description' => 'Allow to add new customer',
        ], [
            'name' => 'CUSTOMERS_EDIT',
            'description' => 'Allow to edit a customer',
        ], [
            'name' => 'CUSTOMERS_DELETE',
            'description' => 'Allow to delete customer',
        ], [
            'name' => 'CUSTOMERS_READ',
            'description' => 'Allow to read list of customers',
        ], [
            'name' => 'CUSTOMERS_ACCESS',
            'description' => 'Allow to access on customers',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach ($this->_permissions as $permission) {
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            } catch (CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
