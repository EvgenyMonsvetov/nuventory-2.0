<?php

use yii\db\Migration;

/**
 * Class m200226_125608_user_phone
 */
class m200226_125608_user_phone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('us1_user', 'US1_PHONE', 'VARCHAR(150) AFTER `US1_EMAIL`');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('us1_user', 'US1_PHONE');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200226_125608_user_phone cannot be reverted.\n";

        return false;
    }
    */
}
