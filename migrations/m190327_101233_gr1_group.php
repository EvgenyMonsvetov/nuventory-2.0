<?php

use yii\db\Schema;
use yii\db\Migration;

class m190327_101233_gr1_group extends Migration
{

     public function safeUp()
        {
            $this->execute('ALTER TABLE `gr1_group` ADD COLUMN `ROLE_NAME` VARCHAR(50) NULL AFTER `GR1_DELETE_FLAG`;');
            $this->execute('UPDATE `gr1_group` SET `ROLE_NAME`="admin" WHERE `GR1_ID`="1";');
            return true;
        }

        /**
         * {@inheritdoc}
         */
        public function safeDown()
        {
            $this->execute('ALTER TABLE `gr1_group` DROP COLUMN `ROLE_NAME`;');
            return true;
        }
}
