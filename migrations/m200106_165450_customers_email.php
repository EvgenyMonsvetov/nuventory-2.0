<?php

use yii\db\Migration;

/**
 * Class m200106_165450_customers_email
 */
class m200106_165450_customers_email extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cu1_customer', 'CU1_EMAIL', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('cu1_customer', 'CU1_EMAIL');
    }

}
