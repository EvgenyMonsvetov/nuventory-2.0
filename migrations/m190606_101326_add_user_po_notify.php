<?php

use yii\db\Schema;
use yii\db\Migration;

class m190606_101326_add_user_po_notify extends Migration
{

	public function safeUp()
    {
	    $this->execute('ALTER TABLE `us1_user` ADD COLUMN `US1_PO_NOTIFY` INT(1) NULL;');
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->execute('ALTER TABLE `us1_user` DROP COLUMN `US1_PO_NOTIFY`;');
        return true;
    }
}
