<?php

use yii\db\Migration;

/**
 * Class m200513_150426_fix_permission_spelling
 */
class m200513_150426_fix_permission_spelling extends Migration {

    private $_permissions = [
        [
            'name' => 'BELOW_MINIMUM_REPORT',
            'description' => 'Allow access to Below Minimum Report'
        ],
        [
            'name' => 'RIGHTS_DELETE',
            'description' => 'Allow to delete assignments'
        ],
        [
            'name' => 'COUNTRIES_ADD',
            'description' => 'Allow to add countries'
        ],
        [
            'name' => 'COUNTRIES_DELETE',
            'description' => 'Allow to delete countries'
        ],
        [
            'name' => 'COUNTRIES_EDIT',
            'description' => 'Allow to edit countries'
        ],
        [
            'name' => 'COUNTRIES_READ',
            'description' => 'Allow to read list of countries'
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $auth = Yii::$app->authManager;
        foreach ($this->_permissions as $permission) {
            $authPermission = $auth->getPermission($permission['name']);
            if (!$authPermission) {
                $authPermission = $auth->createPermission($permission['name']);
            }
            $authPermission->description = $permission['description'];
            $auth->update($permission['name'], $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        return true;
    }

}
