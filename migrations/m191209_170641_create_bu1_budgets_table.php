<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bu1_budgets`.
 */
class m191209_170641_create_bu1_budgets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bu1_budgets', [
            'BU1_ID' => $this->primaryKey(),
            'CO1_ID' => $this->integer(10),
            'LO1_ID' => $this->integer(10),
            'BU1_DATE' => $this->date(),
            'BU1_DELETE_FLAG' => $this->integer()->defaultValue(0),
            'BU1_CREATED_BY' => $this->integer(),
            'BU1_MODIFIED_BY' => $this->integer(),
            'BU1_CREATED_ON' => $this->dateTime(),
            'BU1_MODIFIED_ON' => $this->dateTime(),
            'BU1_MONTHLY_GROSS_SALE' => $this->decimal(15,5)->defaultValue(0.00000),
            'BU1_TARGET_PERCENTAGE' => $this->decimal(15,5)->defaultValue(0.00000),
            'BU1_FINAL_RESULT' => $this->decimal(15,5)->defaultValue(000000)
        ]);

        $this->createIndex(
            'idx-bu1_budgets-BU1_CREATED_BY',
            'bu1_budgets',
            'BU1_CREATED_BY'
        );

        $this->addForeignKey(
            'fk-bu1_budgets-BU1_CREATED_BY',
            'bu1_budgets',
            'BU1_CREATED_BY',
            'us1_user',
            'US1_ID'
        );
        $this->createIndex(
            'idx-bu1_budgets-BU1_MODIFIED_BY',
            'bu1_budgets',
            'BU1_MODIFIED_BY'
        );

        $this->addForeignKey(
            'fk-bu1_budgets-BU1_MODIFIED_BY',
            'bu1_budgets',
            'BU1_MODIFIED_BY',
            'us1_user',
            'US1_ID'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-bu1_budgets-BU1_MODIFIED_BY',
            'bu1_budgets'
        );

        $this->dropIndex(
            'idx-bu1_budgets-BU1_MODIFIED_BY',
            'bu1_budgets'
        );
        $this->dropForeignKey(
            'fk-bu1_budgets-BU1_CREATED_BY',
            'bu1_budgets'
        );

        $this->dropIndex(
            'idx-bu1_budgets-BU1_CREATED_BY',
            'bu1_budgets'
        );

        $this->dropTable('bu1_budgets');
    }
}
