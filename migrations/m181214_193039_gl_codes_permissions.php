<?php

use yii\db\Migration;

/**
 * Class m181214_193039_gl_codes_permissions
 */
class m181214_193039_gl_codes_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'GL_CODES_ADD',
            'description' => 'Allow to add a new GL Codes',
        ],[
            'name'        => 'GL_CODES_EDIT',
            'description' => 'Allow to edit a GL Codes',
        ],[
            'name'        => 'GL_CODES_DELETE',
            'description' => 'Allow to delete a GL Codes',
        ],[
            'name'        => 'GL_CODES_READ',
            'description' => 'Allow to read list of GL Codes',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
