<?php

use yii\db\Migration;

/**
 * Class m180928_185431_rbac_init
 */
class m180928_185431_rbac_init extends Migration
{
    private $_permissions = [[
        'name'        => 'ROLES_READ',
        'description' => 'Allow to read roles',
    ],[
        'name'        => 'ROLES_ADD',
        'description' => 'Allow to add new roles',
    ],[
        'name'        => 'ROLES_EDIT',
        'description' => 'Allow to edit a role',
    ],[
        'name'        => 'ROLES_DELETE',
        'description' => 'Allow to delete roles',
    ],[
        'name'        => 'USERS_READ',
        'description' => 'Allow to read list of users',
    ],[
        'name'        => 'USERS_ADD',
        'description' => 'Allow to add new user',
    ],[
        'name'        => 'USERS_EDIT',
        'description' => 'Allow to edit a user',
    ],[
        'name'        => 'USERS_DELETE',
        'description' => 'Allow to delete users',
    ],[
        'name'        => 'PROFILE_READ',
        'description' => 'Allow to read profile',
    ],[
        'name'        => 'PROFILE_EDIT',
        'description' => 'Allow to edit profile',
    ],[
        'name'        => 'PERMISSION_READ',
        'description' => 'Allow to read list of permissions',
    ],[
        'name'        => 'DASHBOARD_READ',
        'description' => 'Allow to view dashboard',
    ],[
        'name'        => 'RIGHTS_READ',
        'description' => 'Allow to read rights',
    ],[
        'name'        => 'RIGHTS_ADD',
        'description' => 'Allow to assign rights for users/roles',
    ],[
        'name'        => 'RIGHTS_EDIT',
        'description' => 'Allow to edit rights for users/roles',
    ],[
        'name'        => 'RIGHTS_DELETE',
        'description' => 'Allow to delete assingments',
    ]];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole('admin');
        $admin->description = 'Super Admin';
        $auth->add($admin);

        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->add($authPermission);
            $auth->addChild($admin, $authPermission);
        }

        $user = \app\models\User::findByUsername('admin');
        $auth->assign($admin, $user->id);
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->getPermission($permission['name']);
            $auth->remove($authPermission);
        }
        $auth->remove($admin);
        return true;
    }
}
