<?php

use yii\db\Migration;

/**
 * Class m190708_145257_remove_customers_access
 */
class m190708_145257_remove_customers_access extends Migration
{
    private $_permissions = [
        [
            'name' => 'CUSTOMERS_ACCESS',
            'description' => 'Allow to access on customers',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        foreach ($this->_permissions as $permission) {
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            } catch (CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
