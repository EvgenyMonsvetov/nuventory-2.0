<?php

use yii\db\Migration;

/**
 * Class m200213_154751_add_emails_to_location
 */
class m200213_154751_add_emails_to_location extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `lo1_location` ADD COLUMN `LO1_EMAILS` varchar(255) NOT NULL DEFAULT '' AFTER `LO1_CCCONE_NAME`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('lo1_location', 'LO1_EMAILS');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200213_154751_add_emails_to_location cannot be reverted.\n";

        return false;
    }
    */
}
