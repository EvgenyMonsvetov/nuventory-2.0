<?php

use yii\db\Migration;

/**
 * Class m190902_154850_spam_permissions
 */
class m190902_154850_spam_permissions extends Migration
{
    private $_permissions = [
	    [
		    'name'        => 'SPAM_ACCESS',
		    'description' => 'Allow to send custom emails to customers',
	    ]
    ];

	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$auth = Yii::$app->authManager;
		$role = $auth->getRole('admin');
		foreach($this->_permissions as $permission){
			$authPermission = $auth->createPermission($permission['name']);
			$authPermission->description = $permission['description'];
			$auth->remove($authPermission); //remove if already exist
			$auth->add($authPermission);
			$auth->addChild($role, $authPermission);
		}
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$auth = Yii::$app->authManager;

		foreach($this->_permissions as $permission){
			try {
				$authPermission = $auth->getPermission($permission['name']);
				$auth->remove($authPermission);
			}
			catch(CDbException $e) {
				\yii\helpers\VarDumper::dump($e->getMessage());
			}
		}

		return true;
	}
}
