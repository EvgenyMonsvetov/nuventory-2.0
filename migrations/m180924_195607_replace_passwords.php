<?php

use yii\db\Migration;

/**
 * Class m180924_195607_replace_passwords
 */
class m180924_195607_replace_passwords extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /*
        $user = \app\models\User::findOne(['US1_ID' => 2082]);
        $user->US1_PASSWORD_HASH = Yii::$app->security->generatePasswordHash($user->US1_PASS);
        $user->US1_AUTH_KEY      = Yii::$app->security->generateRandomString();

        if(!$user->save()){
            \yii\helpers\VarDumper::dump($user->getErrors());
        }
        */

        $users = \app\models\User::find()->where(['DELETED' => 0])->orderBy(['US1_ID' => SORT_ASC])->all();
        $index = 0;
        foreach( $users as $user ){

            $user->US1_PASSWORD_HASH = Yii::$app->security->generatePasswordHash($user->US1_PASS);
            $user->US1_AUTH_KEY      = Yii::$app->security->generateRandomString();

            if(!$user->save()){
                \yii\helpers\VarDumper::dump($user->getErrors());
            }else{
                if($index == 50){
                    echo PHP_EOL;
                    $index = 0;
                }
                echo '.';
                $index ++;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
