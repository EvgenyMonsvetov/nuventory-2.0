<?php

use yii\db\Migration;

/**
 * Class m181213_224944_companies_permissions
 */
class m181213_224944_companies_permissions extends Migration
{
    private $_permissions = [
        [
            'name'        => 'COMPANIES_ADD',
            'description' => 'Allow to add a new company',
        ],[
            'name'        => 'COMPANIES_EDIT',
            'description' => 'Allow to edit a company',
        ],[
            'name'        => 'COMPANIES_DELETE',
            'description' => 'Allow to delete a company',
        ],[
            'name'        => 'COMPANIES_READ',
            'description' => 'Allow to read list of companies',
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        foreach($this->_permissions as $permission){
            $authPermission = $auth->createPermission($permission['name']);
            $authPermission->description = $permission['description'];
            $auth->remove($authPermission); //remove if already exist
            $auth->add($authPermission);
            $auth->addChild($role, $authPermission);
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        foreach($this->_permissions as $permission){
            try {
                $authPermission = $auth->getPermission($permission['name']);
                $auth->remove($authPermission);
            }
            catch(CDbException $e) {
                \yii\helpers\VarDumper::dump($e->getMessage());
            }
        }

        return true;
    }
}
