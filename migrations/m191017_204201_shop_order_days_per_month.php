<?php

use yii\db\Migration;

/**
 * Class m191017_204201_shop_order_days_per_month
 */
class m191017_204201_shop_order_days_per_month extends Migration
{
    /**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->execute('ALTER TABLE `lo1_location` ADD COLUMN `LO1_ORDER_DAYS_PER_MONTH` tinyint(1) NULL;');
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->execute('ALTER TABLE `lo1_location` DROP COLUMN `LO1_ORDER_DAYS_PER_MONTH`;');
		return true;
	}
}
