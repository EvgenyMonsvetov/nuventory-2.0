<?php

use yii\db\Migration;

/**
 * Class m200214_170738_no_inventory_corrections
 */
class m200214_170738_no_inventory_corrections extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            Update md2_location  as md2
               left join vd1_vendor as vd1 on vd1.VD1_ID = md2.VD1_ID 
            SET md2.MD2_NO_INVENTORY = vd1.VD1_NO_INVENTORY
            Where md2.MD2_NO_INVENTORY != vd1.VD1_NO_INVENTORY and
                  md2.MD2_DELETE_FLAG = 0 and 
                  vd1.VD1_DELETE_FLAG = 0
        ');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
