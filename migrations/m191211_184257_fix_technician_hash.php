<?php

use yii\db\Migration;

/**
 * Class m191211_184257_fix_technician_hash
 */
class m191211_184257_fix_technician_hash extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $users = \app\models\User::find()
	        ->where(['is not', 'TE1_ID', new \yii\db\Expression('null')])
	        ->andFilterWhere(['DELETED' => 0])
		    ->andFilterWhere(['or',
			    ['is', 'US1_PASSWORD_HASH', new \yii\db\Expression('null')],
			    ['is', 'US1_AUTH_KEY', new \yii\db\Expression('null')]
		    ])
	        ->all();

        foreach( $users as $user ){
        	if (!isset($user->US1_PASSWORD_HASH) || !$user->US1_PASSWORD_HASH) {
	        	$user->US1_PASSWORD_HASH = Yii::$app->security->generatePasswordHash($user->US1_PASS);
	        }
	        if (!isset($user->US1_AUTH_KEY) || !$user->US1_AUTH_KEY) {
	        	$user->US1_AUTH_KEY = Yii::$app->security->generateRandomString();
	        }

	        $user->GR1_ID = \app\models\Group::GROUP_STANDARD_USER;
	        $user->US1_TYPE = 0;

            if(!$user->save()){
                \yii\helpers\VarDumper::dump($user->getErrors());
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
