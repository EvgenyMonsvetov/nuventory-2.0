<?php
use yii\helpers\Html;
use yii\helpers\Url;
use \app\services\MasterData as MasterDataService;
use Picqer\Barcode\BarcodeGeneratorSVG;
?>

<style>
    .page {
        page-break-after: always;
    }

    @page {
        margin: 0;
        size : 17in 11in;
    }

    @media print {
        @page {
            margin: 0;
            size : 17in 11in;
        }

        .page {
            width: 17in;
            height: 11in;
        }

        thead {display: table-header-group;}
        tfoot {display: table-footer-group;}

        .cancel-button {
            display: none;
            visibility: hidden;
        }

        body {margin: 0;}
    }
</style>

<table class="page" style="width: <?= Html::encode($params['pageW']) ?>px; height: <?= $params['pageH'] * $params['numPages'] ?>px;">
    <tbody>
        <tr>
            <td style="padding: 0">
	            <?php for ($pageIndex = 0; $pageIndex < $params['numPages']; $pageIndex++) {?>
	                    <div class="page" style="width: <?= Html::encode($params['pageW']) ?>px; height: <?= Html::encode($params['pageH']) ?>px;">
	                        <div class="row" style="margin-left: <?= MasterDataService::PADDING_LEFT ?>px; margin-right: <?= MasterDataService::PADDING_LEFT ?>px; align-items: center;">
                                <img src="<?= Url::base(true) . DIRECTORY_SEPARATOR . $params['basePath'] ?>nuventory-large.jpg"
                                     style="width: <?= MasterDataService::IMAGE_WIDTH ?>px;">
                                <div class="col" style="text-align: center; font-size: 28px; font-family: Courier; font-weight: bold; padding-right: <?= MasterDataService::IMAGE_WIDTH ?>px">
                                    Nuventory Schema &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rack <?=floor($pageIndex/4+1)?>, Drawer <?=($pageIndex%4+1);?>
                                </div>
                            </div>

		                    <?php for ($itemsIndex = 0; $itemsIndex < 24; $itemsIndex++) {?>
			                    <?php if($itemsIndex !== 0 && ($itemsIndex) % 6 === 0) { ?>
                                    </div>
                                <?php } ?>
			                    <?php if($itemsIndex === 0 || ($itemsIndex) % 6 === 0) { ?>
                                    <div class="row" style="margin-left: <?= MasterDataService::PADDING_LEFT ?>px; margin-right: <?= MasterDataService::PADDING_LEFT ?>px; height: <?= Html::encode($params['itemHeight']) ?>;">
                                <?php } ?>
                                <?php if($itemsIndex + $pageIndex*24 < sizeof($params['selectedItems']) && isset($params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_ID']) && $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_ID']) { ?>
                                    <div class="col col-md-2" style="margin-left: 0px; margin-right: 0px; padding-top: <?= MasterDataService::TEXT_PADDING_TOP ?>px; padding-left: <?= MasterDataService::TEXT_PADDING_LEFT ?>px; padding-right: <?= MasterDataService::TEXT_PADDING_LEFT ?>px; border: 1pt solid black;">
                                        <div style="font-size: 14px; font-family: Courier; font-weight: bold;">
                                            <?=
                                            ($params['selectedItems'][$itemsIndex+$pageIndex*24]['counter'] > 1 ? $params['selectedItems'][$itemsIndex+$pageIndex*24]['counter'] . "*":'') .
                                            $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD2_RACK'] . "-" . $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD2_DRAWER'] . "-" .
                                            $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD2_BIN'] . "   " . $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER']
                                            ?>
                                        </div>
                                        <div style="font-size: 13px; font-family: Courier; height: 42px">
		                                    <?= substr($params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_DESC1'],0,68) ?>
                                        </div>
	                                    <?php if ($params['selectedItems'][$itemsIndex+$pageIndex*24]['FI1_FILE_PATH'] && $params['selectedItems'][$itemsIndex+$pageIndex*24]['FI1_FILE_PATH'] !== '' && file_exists(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['partsImagesPath']  . DIRECTORY_SEPARATOR . str_replace('/web', '', $params['selectedItems'][$itemsIndex+$pageIndex*24]['FI1_FILE_PATH']))) { ?>
                                            <img src="<?= Url::base(true) . DIRECTORY_SEPARATOR . $params['basePath'] . str_replace('/web', '', $params['selectedItems'][$itemsIndex+$pageIndex*24]['FI1_FILE_PATH'])?>"
                                                 style="width: <?= $params['imageWidth']?>; height: <?= $params['imageHeight']?>px; padding-left: <?= MasterDataService::IMAGE_PADDING_LEFT?>px;">
                                        <?php } else { ?>
                                            <img src="<?= Url::base(true) . DIRECTORY_SEPARATOR . $params['basePath'] ?>noimage.png"
                                                 style="height: <?= $params['imageHeight']?>px; padding-left: <?= MasterDataService::IMAGE_PADDING_LEFT?>px;">
                                        <?php } ?>
	                                    <?php $min = intval($params['selectedItems'][$itemsIndex+$pageIndex*24]['MD2_MIN_QTY']); ?>
                                        <div style="font-size: 13px; font-family: Courier; font-weight: bold; height: <?= MasterDataService::LINE_HEIGHT ?>px;">
                                            MIN: <?= ((!is_nan($min)&&$min>0)?$min:0) ?> &nbsp; <?= $params['selectedItems'][$itemsIndex+$pageIndex*24]['CA1_NAME'] ?>
                                        </div>
	                                    <?php
	                                    if($params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_UPC1'] && $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_UPC1'] !== '') {
		                                    $label = $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_UPC1'];
	                                    } else if($params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER'] && $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER'] !== '') {
		                                    $label = $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER'];
	                                    } else {
		                                    $label = $labelText = "";
	                                    }
	                                    $generator = new BarcodeGeneratorSVG();
	                                    ?>
	                                    <?= $generator->getBarcode($label, $generator::TYPE_CODE_39, 1, MasterDataService::BARCODE_HEIGHT)?>
                                        <div style="font-size: 9px; font-family: Courier; padding-left: 20px;">
                                            <?= $params['selectedItems'][$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER'] ?>
                                        </div>
                                    </div>
			                    <?php } else { ?>
                                    <div class="col col-md-2" style="margin-left: 0px; margin-right: 0px; padding-top: <?= MasterDataService::TEXT_PADDING_TOP ?>px; padding-left: <?= MasterDataService::TEXT_PADDING_LEFT ?>px; padding-right: <?= MasterDataService::TEXT_PADDING_LEFT ?>px; border: 1pt solid black;">
                                        <div class="col col-md-2" style="font-size: 14px; font-family: Courier; font-weight: bold; height:  <?= MasterDataService::LINE_HEIGHT*3 ?>px;">EMPTY</div>
                                        <img src="<?= Url::base(true) . DIRECTORY_SEPARATOR . $params['basePath'] ?>noimage.png"
                                             style="height: <?= $params['imageHeight']?>px; padding-left: <?= MasterDataService::IMAGE_PADDING_LEFT?>px;">
                                    </div>
			                    <?php } ?>
                            <?php } ?>
                        </div>
                        <div class="col" style="text-align: center; font-size: 12px; font-family: Courier">
                            http://www.nuventory.com/ Copyright 2008-2019 Nuventory, All Rights Reserved. +1(833)688-3689
                        </div>
                    </div>
                <?php } ?>
            </td>
        </tr>
    </tbody>
</table>
