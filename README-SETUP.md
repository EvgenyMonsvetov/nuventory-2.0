1. Initial setup

Create Directories
/web/app
/web/assets
/runtime

Connect to database
- Connection to the database /config/db.php
- Database dump - /dump.sql
- Admin User - admin/admin

Set up Angualr
- Angular Directory - /angular
- Angular ENVs - /angular/app/permissions.ts - change environment.API_URL

Install packages
- execute composer update
- execute npm update
- execute npm install -g @angular/cli@latest
- execute ng build -w -env=(test|dev|prod)

Server configuration
 
Ngnix:
$root_path = APPLICATION_PATH/web

fastcgi_param    YII_ENV (test|dev|prod);
fastcgi_param    YII_DEBUG 1|0;

Apache:
document_root = APPLICATION_PATH/web 

.htaccess
SetEnv YII_ENV (test|dev|prod)
SetEnv YII_DEBUG 1|0


2. Setup RBAC

2.1 execute php yii migrate --migrationPath=@yii/rbac/migrations
2.2 execute php yii migrate
