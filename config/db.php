<?php

if (YII_ENV_DEV) {

    $host = getenv('DB_DATABASE') ? 'mysql' : '127.0.0.1';
    $db = getenv('DB_DATABASE') ?: 'cmp_nuventoryflash';
    $user = getenv('DB_USER') ?: 'root';
    $password = getenv('DB_PASSWORD') ?: 'root';

    return [
        'class' => 'yii\db\Connection',
        'dsn' => "mysql:host={$host};dbname={$db}",
        'username' => $user,
        'password' => $password,
        'charset' => 'utf8',

        // Schema cache options (for production environment)
        //'enableSchemaCache' => true,
        //'schemaCacheDuration' => 60,
        //'schemaCache' => 'cache',
    ];
}if (YII_ENV_TEST) {
    return [
        'class'    => 'yii\db\Connection',
        'dsn'      => 'mysql:host=127.0.0.1;dbname=cimtest2.0',
        'username' => 'root',
        'password' => 'siEmuAr7j',
        'charset'  => 'utf8',

        // Schema cache options (for production environment)
        //'enableSchemaCache' => true,
        //'schemaCacheDuration' => 60,
        //'schemaCache' => 'cache',
    ];
}elseif(YII_ENV_PROD){
    return [
        'class'    => 'yii\db\Connection',
        'dsn'      => 'mysql:host=127.0.0.1;dbname=cim2',
        'username' => 'root',
        'password' => 'siEmuAr7j',
        'charset'  => 'utf8',

        // Schema cache options (for production environment)
        //'enableSchemaCache' => true,
        //'schemaCacheDuration' => 60,
        //'schemaCache' => 'cache',
    ];
}

