<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 13/07/2018
 * Time: 13:29
 */

return [
    'language' => 'site/language',
    'login' => 'site/login',
    'PUT refresh-token/<id:.*?>' => 'site/refresh-token',
    'PUT set-company/<id:.*?>' => 'site/set-company',
    'PUT set-location/<id:.*?>' => 'site/set-location',
    [
        'class' => 'yii\rest\UrlRule',
        'tokens' => [
            '{id}' => '<id:.*?>'
        ],
        'controller' => [
            'role',
            'permission',
            'user-roles',
            'user-permissions',
            'c00-category',
            'measurement-unit',
            'gl-code',
            'company',
            'master-category',
            'category',
            'technician',
            'master-vendor',
            'master-catalog',
            'group',
            'order-line',
            'tech-scan',
            'job-ticket',
            'job-line',
            'purchase-order-line',
            'master-data',
            'invoice',
            'invoice-line',
            'job-invoice',
            'job-invoice-line',
            'statement',
            'statement-line',
            'upload',
	        'inventory',
	        'price',
            'raw-sales-data-store-month'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['vendor', 'log'],
        'extraPatterns' => [
            'GET access/<id:.*?>/<CO1_ID:.*?>' => 'access',
            'PUT access/<id:.*?>/<CO1_ID:.*?>' => 'access-update',
            'POST import' => 'import',
            'OPTIONS import' => 'options',
            'GET preload' => 'preload',
            'GET <id:.*?>/<CO1_ID:.*?>' => 'view'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['user'],
        'extraPatterns' => [
            'PUT change-password/<id:.*?>' => 'change-password',
            'PUT admin-change-password/<id:.*?>' => 'admin-change-password',
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['user-profile'],
        'tokens' => [
            '{id}' => '<userId:.*?>'
        ],
        'extraPatterns' => [
            'GET view/{id}' => 'view'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['countries'],
        'extraPatterns' => [
            'GET countries' => 'countries/index',
            'GET countries/<id>' => 'countries/view',
            'POST countries' => 'countries/create',
            'PUT countries/<id>' => 'countries/delete'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['timezones'],
        'extraPatterns' => [
            'GET timezones' => 'timezones/index'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['labels'],
        'extraPatterns' => [
            'GET label' => 'labels/index',
            'GET label/<id>' => 'labels/view',
            'GET label/export/' => 'labels/export',
            'POST label' => 'labels/create'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['languages'],
        'extraPatterns' => [
            'GET languages' => 'languages/index',
            'GET languages/<id>' => 'languages/view',
            'GET languages/export/' => 'languages/export',
            'POST languages' => 'languages/create',
            'PUT languages/<id>' => 'languages/delete'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['master-catalogs'],
        'extraPatterns' => [
            'GET search' => 'master-catalogs/search',
            'GET linked-parts' => 'master-catalogs/linked-parts',
	        'POST delete-multiple' => 'delete-multiple',
	        'POST set-default-images' => 'set-default-images'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['master-vendors'],
        'extraPatterns' => [
	        'POST delete-multiple' => 'delete-multiple'
        ]
    ], [
		'class' => 'yii\rest\UrlRule',
		'controller' => ['master-category'],
		'extraPatterns' => [
			'POST delete-multiple' => 'delete-multiple'
		]
	], [
		'class' => 'yii\rest\UrlRule',
		'controller' => ['c00-category'],
		'extraPatterns' => [
			'POST delete-multiple' => 'delete-multiple'
		]
	], [
		'class' => 'yii\rest\UrlRule',
		'controller' => ['category'],
		'extraPatterns' => [
			'POST delete-multiple' => 'delete-multiple'
		]
	], [
		'class' => 'yii\rest\UrlRule',
		'controller' => ['gl-code'],
		'extraPatterns' => [
			'POST delete-multiple' => 'delete-multiple'
		]
	], [
		'class' => 'yii\rest\UrlRule',
		'controller' => ['measurement-unit'],
		'extraPatterns' => [
			'POST delete-multiple' => 'delete-multiple'
		]
	], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['shop'],
        'extraPatterns' => [
            'GET master-data/<MD1_ID:.*?>' => 'master-data',
            'GET central-locations'    => 'central-locations'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['master-data'],
        'extraPatterns' => [
            'GET master-data/central-order-parts' => 'central-order-parts',
            'GET master-data/part'       => 'part',
            'GET master-data/full-parts' => 'full-parts',
            'GET auto-order-parts'       => 'auto-order-parts',
            'POST delete-multiple'       => 'delete-multiple'
        ]
    ],[
        'class' => 'yii\rest\UrlRule',
        'controller' => ['master-data-location'],
        'extraPatterns' => [
            'POST update-inventory' => 'update-inventory'
        ]
    ],[
        'class' => 'yii\rest\UrlRule',
        'controller' => ['invoice'],
        'extraPatterns' => [
            'GET invoice/credit-memos'     => 'invoice/credit-memos',
            'POST create-from-store-order' => 'create-from-store-order',
	        'POST delete-multiple' => 'delete-multiple'
        ]
    ],[
        'class' => 'yii\rest\UrlRule',
        'controller' => ['store-order'],
        'extraPatterns' => [
            'GET store-order/init-order'    => 'store-order/init-order',
            'GET lines-summary/<OR1_ID:.*?>' => 'lines-summary',
            'POST receive' => 'receive',
            'POST complete' => 'complete',
	        'POST delete-multiple' => 'delete-multiple'
        ]
    ],[
        'class' => 'yii\rest\UrlRule',
        'controller' => ['purchase-order'],
        'extraPatterns' => [
            'POST receive' => 'receive',
            'POST upload' => 'upload',
	        'POST delete-multiple' => 'delete-multiple'
        ]
    ], [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['customer'],
        'extraPatterns' => [
            'GET customer/generate-short-code' => 'generate-short-code'
        ]
    ], [
		'class' => 'yii\rest\UrlRule',
		'controller' => ['statement'],
		'extraPatterns' => [
			'POST delete-multiple' => 'delete-multiple'
		]
	],[
        'class' => 'yii\rest\UrlRule',
        'controller' => ['statement'],
        'extraPatterns' => [
            'POST send' => 'send'
        ]
    ],[
		'class' => 'yii\rest\UrlRule',
		'controller' => ['job-ticket'],
		'extraPatterns' => [
			'POST delete-multiple' => 'delete-multiple'
		]
	],[
		'class' => 'yii\rest\UrlRule',
		'controller' => ['tech-scan'],
		'extraPatterns' => [
			'POST delete-multiple' => 'delete-multiple'
		]
	],[
	    'class' => 'yii\rest\UrlRule',
		'controller' => ['import-report-data'],
		'extraPatterns' => [
			'POST import-sales-journal'    => 'import-sales-journal',
            'POST import-job-cost-summary' => 'import-job-cost-summary'
		]
    ],[
	    'class' => 'yii\rest\UrlRule',
		'controller' => ['hour'],
		'extraPatterns' => [
			'PUT update-multiple' => 'update-multiple',
            'GET tech-hours' => 'tech-hours'
		]
    ],
	[
		'class' => 'yii\rest\UrlRule',
		'controller' => ['budget'],
		'extraPatterns' => [
			'GET budget/<id:.*?>' => 'view',
			'POST delete-multiple' => 'delete-multiple'
		]
	]
];
