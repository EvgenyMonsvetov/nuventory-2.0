<?php
$monitoring = require __DIR__ . '/monitoring.php';

$params = [
    'jwt' => [
        'passwordResetTokenExpire' => 300,
        'secretKey' => 'restApiToken',
        'expiresIn'        => YII_ENV_PROD ? '+8 hours' : '+4 hours',
        'expiresInSeconds' => YII_ENV_PROD ? (3600 * 8) : (3600 * 4)
    ],
    'monitoring' => $monitoring,
    'partsImagesPath' => 'web/images',
    'imagesFolderName' => 'cim-masterdata',
	'uploadImagesArchiveFolder' => 'web/zipUploads',
	'maxUploadZipFileSize' => 20*1024*1024,
    'mailer' => [
        'sender' => 'info@comparatio.com'
    ]
];

if (YII_ENV_DEV) {
    $params['importFolderUseAbsolutePath'] = false;
    $params['importFolderPath'] = 'web/imports';
} else if (YII_ENV_TEST) {
    $params['importFolderUseAbsolutePath'] = true;
    $params['importFolderPath'] = 'C:\UniServerZ\www\nuventorydash\imports';
} else if (YII_ENV_PROD) {
    $params['importFolderUseAbsolutePath'] = true;
    $params['importFolderPath'] = 'C:\UniServerZ\www\nuventorydash\imports';
}

return $params;