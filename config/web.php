<?php

$params     = require __DIR__ . '/params.php';
$db         = require __DIR__ . '/db.php';
$dbdash     = require __DIR__ . '/dbdash.php';
$routes     = require __DIR__ . '/routes.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'app\bootstraps\AppBootstrap'
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
	    '@partsImagesPath' => '@app/web/images',
	    '@uploadImagesArchiveFolder' => '@app/web/zipUploads'
    ],
    'components' => [
        'i18n' => [
            'translations' => [
                'yii2mod.rbac' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/rbac/messages',
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'QUWzmlwcNu2kv2PvNt7bFZ0xtVL0JjDX',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
            'enableSession'   => false
        ],
	    'session' => [
		    'class' => 'yii\web\Session',
		    'cookieParams' => ['httponly' => true, 'lifetime' => $params['jwt']['expiresInSeconds']],
		    'timeout' => $params['jwt']['expiresInSeconds'],
		    'useCookies' => true
	    ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
	        'transport' => [
		        'class' => 'Swift_SmtpTransport',
                'host' => 'webmail.cazarin.com',
                'username' => 'jan.poehland@nuventory.com',
                'password' => '1hjdy5',
//		        'port' => '465',
//		        'encryption' => 'ssl',
	        ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'dbsys' => [
                       'class' => 'yii\db\Connection',
                       'dsn' => 'mysql:host=127.0.0.1;dbname=mysql',
                       'username' => 'root',
                       'password' => 'siEmuAr7j',
                       'charset' => 'utf8'
                   ],
        'dbdash' => $dbdash,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $routes
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ]
    ],
    'modules' => [
        'rbac' =>  [
            'class' => 'yii2mod\rbac\Module',
        ]
    ],
    'params' => $params,
];

if (!YII_ENV_PROD) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
        'generators' => [
            'clickhouseDbModel' => [
                'class' => 'kak\clickhouse\gii\model\Generator'
            ]
        ]
    ];
}

return $config;
