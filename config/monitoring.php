<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 07/09/2018
 * Time: 11:06
 */
if(YII_ENV_PROD){
    return [[
        'name'        => 'ClickHouse Node 1',
        'url'         => 'http://172.31.16.10:80/monitoring.php',
        'securityKey' => ''
    ],[
        'name'        => 'ClickHouse Node 2',
        'url'         => 'http://172.31.16.11:80/monitoring.php',
        'securityKey' => ''
    ]];
}else{
    return [[
        'name'        => 'ClickHouse Node 1',
        'url'         => 'http://18.236.147.118:80/monitoring.php',
        'securityKey' => ''
    ],[
        'name'        => 'ClickHouse Node 2',
        'url'         => 'http://18.237.84.67:80/monitoring.php',
        'securityKey' => ''
    ]];
}