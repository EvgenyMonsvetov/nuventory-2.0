<?php

namespace app\models;

use app\models\scopes\OrderLineQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "or2_order_line".
 *
 * @property int $OR2_ID
 * @property int $OR1_ID
 * @property int $OR2_LINE
 * @property int $OR2_STATUS
 * @property double $OR2_ORDER_QTY
 * @property double $OR2_RECEIVED_QTY
 * @property int $MD1_ID
 * @property int $VD1_ID
 * @property int $CA1_ID
 * @property int $CO1_ID
 * @property int $GL1_ID
 * @property string $OR2_PART_NUMBER
 * @property string $OR2_DESC1
 * @property string $OR2_DESC2
 * @property double $UM1_ID
 * @property string $OR2_UNIT_PRICE
 * @property string $OR2_DELETE_FLAG
 * @property int $OR2_CREATED_BY
 * @property string $OR2_CREATED_ON
 * @property int $OR2_MODIFIED_BY
 * @property string $OR2_MODIFIED_ON
 * @property int $OR2_TYPE
 * @property int $LO1_ID
 * @property int $FI1_ID
 * @property string $MD1_PART_NUMBER
 * @property string $MD1_UPC1
 * @property string $MD1_UPC2
 * @property string $MD1_UPC3
 * @property string $MD1_UM
 * @property int $MD1_ON_HAND_QTY
 * @property int $MD1_AVAILABLE_QTY
 * @property string $MD1_UNIT_PRICE
 * @property string $MD1_DESC1
 * @property string $MD1_DESC2
 * @property double $OR2_TOTAL_VALUE
 * @property string $OR2_TOTAL_SELL
 * @property int $LO1_FROM_ID
 * @property int $LO1_TO_ID
 * @property int $TE1_ID
 */
class OrderLine extends ActiveRecord
{
	CONST ORDER_LINE_STATUS_OPEN = 0;

	protected $_tablePrefix = 'OR2';

	public $OR1_IDs;

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'or2_order_line';
    }

    public static function primaryKey()
    {
        return ['OR2_ID'];
    }

	public function fields() {
		$fields = parent::fields();
		$fields["OR1_IDs"] = function($model){ return $model->OR1_IDs; };
		return $fields;
	}

	/**
     * {@inheritdoc}
     * @return OrderLineQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderLineQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['OR1_ID', 'OR2_LINE', 'OR2_STATUS', 'MD1_ID', 'VD1_ID', 'CA1_ID', 'CO1_ID', 'GL1_ID', 'OR2_CREATED_BY', 'OR2_MODIFIED_BY', 'OR2_TYPE', 'LO1_ID', 'FI1_ID', 'MD1_ON_HAND_QTY', 'MD1_AVAILABLE_QTY', 'LO1_FROM_ID', 'LO1_TO_ID', 'TE1_ID'], 'integer'],
            [['OR2_ORDER_QTY', 'OR2_RECEIVED_QTY', 'UM1_ID', 'OR2_UNIT_PRICE', 'MD1_UNIT_PRICE', 'OR2_TOTAL_VALUE', 'OR2_TOTAL_SELL'], 'number'],
            [['OR2_CREATED_ON', 'OR2_MODIFIED_ON'], 'safe'],
            [['OR2_PART_NUMBER'], 'string', 'max' => 100],
            [['OR2_DESC1', 'OR2_DESC2'], 'string', 'max' => 300],
            [['OR2_DELETE_FLAG'], 'string', 'max' => 1],
            [['MD1_PART_NUMBER', 'MD1_UPC1', 'MD1_UPC2', 'MD1_UPC3', 'MD1_UM'], 'string', 'max' => 45],
            [['MD1_DESC1', 'MD1_DESC2'], 'string', 'max' => 200],
            [['OR1_IDs'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'OR2_ID' => 'Or2  ID',
            'OR1_ID' => 'Or1  ID',
            'OR2_LINE' => 'Or2  Line',
            'OR2_STATUS' => 'Or2  Status',
            'OR2_ORDER_QTY' => 'Or2  Order  Qty',
            'OR2_RECEIVED_QTY' => 'Or2  Received  Qty',
            'MD1_ID' => 'Md1  ID',
            'VD1_ID' => 'Vd1  ID',
            'CA1_ID' => 'Ca1  ID',
            'CO1_ID' => 'Co1  ID',
            'GL1_ID' => 'Gl1  ID',
            'OR2_PART_NUMBER' => 'Or2  Part  Number',
            'OR2_DESC1' => 'Or2  Desc1',
            'OR2_DESC2' => 'Or2  Desc2',
            'UM1_ID' => 'Um1  ID',
            'OR2_UNIT_PRICE' => 'Or2  Unit  Price',
            'OR2_DELETE_FLAG' => 'Or2  Delete  Flag',
            'OR2_CREATED_BY' => 'Or2  Created  By',
            'OR2_CREATED_ON' => 'Or2  Created  On',
            'OR2_MODIFIED_BY' => 'Or2  Modified  By',
            'OR2_MODIFIED_ON' => 'Or2  Modified  On',
            'OR2_TYPE' => 'Or2  Type',
            'LO1_ID' => 'Lo1  ID',
            'FI1_ID' => 'Fi1  ID',
            'MD1_PART_NUMBER' => 'Md1  Part  Number',
            'MD1_UPC1' => 'Md1  Upc1',
            'MD1_UPC2' => 'Md1  Upc2',
            'MD1_UPC3' => 'Md1  Upc3',
            'MD1_UM' => 'Md1  Um',
            'MD1_ON_HAND_QTY' => 'Md1  On  Hand  Qty',
            'MD1_AVAILABLE_QTY' => 'Md1  Available  Qty',
            'MD1_UNIT_PRICE' => 'Md1  Unit  Price',
            'MD1_DESC1' => 'Md1  Desc1',
            'MD1_DESC2' => 'Md1  Desc2',
            'OR2_TOTAL_VALUE' => 'Or2  Total  Value',
            'OR2_TOTAL_SELL' => 'Or2  Total  Sell',
            'LO1_FROM_ID' => 'Lo1  From  ID',
            'LO1_TO_ID' => 'Lo1  To  ID',
            'TE1_ID' => 'Te1  ID',
        ];
    }

    public function getQuery($params)
    {
        $query = OrderLine::find()->alias('OR2')
                    ->select([  'OR2.OR2_ID', 'OR2.OR1_ID', 'OR1.OR1_NUMBER', 'OR2.OR2_LINE', 'OR2.OR2_TYPE', 'OR2.OR2_STATUS',
                                'if(OR2.OR2_STATUS=1, "Received", if(OR2.OR2_STATUS=2, "Completed","Open")) AS OR2_STATUS_TEXT',
                                'OR2.MD1_ID', 'OR2.CO1_ID', 'OR2.LO1_TO_ID', 'OR2.FI1_ID', 'OR2.MD1_PART_NUMBER', 'GL1.GL1_NAME',
                                'OR2.MD1_UPC1', 'OR2.MD1_UPC2', 'OR2.MD1_UPC3',
                                'OR2.MD1_UPC1', 'OR2.MD1_DESC1', 'OR2.MD1_DESC2', 'UM2.UM2_FACTOR',
                                'OR2.OR2_ORDER_QTY*coalesce(UM2.UM2_FACTOR, 1) AS OR2_RECEIVING_QTY',
                                'if((OR2.OR2_ORDER_QTY * coalesce(UM2.UM2_FACTOR, 1) - coalesce(OR2.OR2_RECEIVED_QTY, 0)) < 0, 0,
                                    OR2.OR2_ORDER_QTY * coalesce(UM2.UM2_FACTOR, 1) - coalesce(OR2.OR2_RECEIVED_QTY, 0)) AS QTY',
                                'OR2.OR2_RECEIVED_QTY',
                                'um1_receive.UM1_NAME AS UM1_RECEIVE_NAME',
                                'OR2.MD1_ON_HAND_QTY',
                                'OR2.MD1_AVAILABLE_QTY',
                                'OR2.OR2_ORDER_QTY',
                                'OR2.MD1_UNIT_PRICE',
                                'OR2.OR2_TOTAL_SELL AS OR2_EXTENDED_VALUE',
                                'if (OR2.LO1_TO_ID=OR2.LO1_FROM_ID, um1_receive.UM1_NAME, um1_purchase.UM1_NAME) AS UM1_NAME',
			                    'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('OR2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON'  => 'DATE_FORMAT(' . $this->getLocalDate('OR2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'OR2.OR2_MODIFIED_ON', 'OR2.OR2_CREATED_ON',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("or1_order_header as OR1" , "OR1.OR1_ID = OR2.OR1_ID")
                    ->leftJoin("md1_master_data as MD1" , "MD1.MD1_ID = OR2.MD1_ID")
                    ->leftJoin("md2_location as md2_from" , "md2_from.MD1_ID = OR2.MD1_ID AND md2_from.LO1_ID = OR1.LO1_FROM_ID")
                    ->leftJoin("md2_location as md2_to" , "md2_to.MD1_ID = OR2.MD1_ID AND md2_to.LO1_ID = OR1.LO1_TO_ID")
                    ->leftJoin("um2_conversion as UM2" , "UM2.MD1_ID = OR2.MD1_ID AND UM2.UM1_TO_ID = md2_to.UM1_RECEIPT_ID AND UM2.UM1_FROM_ID = md2_from.UM1_RECEIPT_ID AND UM2.UM2_DELETE_FLAG=0")
                    ->leftJoin("um1_unit" , "MD1.UM1_PRICING_ID = um1_unit.UM1_ID")
                    ->leftJoin("um1_unit AS um1_purchase" , "um1_purchase.UM1_ID = md2_to.UM1_PURCHASE_ID")
                    ->leftJoin("um1_unit AS um1_receive" , "um1_receive.UM1_ID = md2_to.UM1_RECEIPT_ID")
                    ->leftJoin("ca1_category as CA1" , "CA1.CA1_ID = OR2.CA1_ID")
                    ->leftJoin("gl1_glcode as GL1" , "GL1.GL1_ID = MD1.GL1_ID AND GL1.GL1_DELETE_FLAG=0")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = OR1.OR1_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = OR1.OR1_MODIFIED_BY");
        $query->active();
        $query->orderBy(['OR1_NUMBER' => SORT_ASC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->CO1_ID) && $this->CO1_ID != 0) {
            $query->andFilterWhere(['=', 'OR2.CO1_ID', $this->CO1_ID]);
        }

        if(isset($this->LO1_TO_ID)) {
            $query->andFilterWhere(['=', 'OR2.LO1_TO_ID', $this->LO1_TO_ID]);
        }

        if(isset($this->OR1_ID)) {
            $query->andFilterWhere(['=', 'OR2.OR1_ID', $this->OR1_ID]);
        }

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['OR2_LINE'] = SORT_ASC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => false,
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getOrderLine($id)
    {
        $query = $this->getQuery(null);
        $query->andFilterWhere(['=', 'OR2.OR2_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

	public function getPdfQuery($params)
	{
		$query = OrderLine::find()->alias('OR2')
			->select([ 'OR2.OR2_ID', 'OR2.OR1_ID', 'OR2.OR2_LINE', 'OR2.OR2_ORDER_QTY', 'OR2.MD1_PART_NUMBER', 'md1.MD1_VENDOR_PART_NUMBER', 'OR2.CA1_ID',
						'CA1.CA1_NAME', 'OR2.MD1_UPC1', 'OR2.MD1_UPC2', 'OR2.MD1_UPC3', 'OR2.MD1_UNIT_PRICE', 'OR2.OR2_TOTAL_VALUE',
						'OR2.OR2_TOTAL_SELL', 'OR2.MD1_DESC1', 'OR2.MD1_DESC2', 'um1.UM1_SHORT_CODE', 'um1.UM1_SIZE'])
			->leftJoin("md1_master_data as MD1" , "MD1.MD1_ID = OR2.MD1_ID")
			->leftJoin("md2_location as md2" , "md2.MD1_ID = md1.MD1_ID AND md2.LO1_ID = OR2.LO1_TO_ID")
			->leftJoin("um1_unit as um1" , "if(OR2.LO1_TO_ID=OR2.LO1_FROM_ID,md2.UM1_RECEIPT_ID,md2.UM1_PURCHASE_ID)=um1.UM1_ID")
			->leftJoin("ca1_category as CA1" , "CA1.CA1_ID = OR2.CA1_ID");
		$query->where(['=', 'OR2.OR2_DELETE_FLAG', 0]);
		$query->orderBy(['OR2.OR1_ID' => SORT_ASC, 'OR2.OR2_LINE' => SORT_ASC]);

		if ($params && !empty($params)) {
			$this->load($params, '');
		}

		if(isset($this->CO1_ID) && $this->CO1_ID != 0) {
			$query->andFilterWhere(['=', 'OR2.CO1_ID', $this->CO1_ID]);
		}

		if(isset($this->LO1_TO_ID)) {
			$query->andFilterWhere(['=', 'OR2.LO1_TO_ID', $this->LO1_TO_ID]);
		}

		if (isset($this->OR1_IDs)) {
			$query->andFilterWhere(['IN', 'OR2.OR1_ID', explode(',', $this->OR1_IDs)]);
		}

		return $query;
	}

	public function getPdfOrderLines($params)
	{
		$query = $this->getPdfQuery( $params );
		return $query->asArray()->all();
	}

	public function getLinesForGenerateInvoices($OR1_ID, $timeStamp)
	{
		$query = self::find()->alias('or2')
			->select(['in1.IN1_ID', 'or2.OR2_ID', 'or2.OR2_LINE', 'or2.OR2_ORDER_QTY', 'or2.MD1_ID', 'or2.CA1_ID', 'or2.GL1_ID',
				'or2.MD1_DESC1', 'or2.MD1_DESC2', 'if(or2.LO1_TO_ID = or2.LO1_FROM_ID, md2.UM1_RECEIPT_ID, md2.UM1_PURCHASE_ID) AS UM1_ID',
				'or2.LO1_TO_ID', 'or2.OR2_TOTAL_SELL', 'or2.CO1_ID', 'or2.MD1_PART_NUMBER', 'or2.MD1_UNIT_PRICE'])
			->leftJoin("in1_invoice_header AS in1" , "in1.OR1_ID = or2.OR1_ID AND in1.IN1_DELETE_FLAG = " . $timeStamp)
			->leftJoin("md1_master_data AS md1" , "or2.MD1_ID = md1.MD1_ID AND md1.MD1_DELETE_FLAG = 0")
			->leftJoin("md2_location AS md2" , "md2.MD1_ID = md1.MD1_ID AND md2.LO1_ID=or2.LO1_TO_ID")
			->andFilterWhere(['=', 'or2.OR1_ID', $OR1_ID])
			->andFilterWhere(['=', 'or2.OR2_DELETE_FLAG', 0]);

		return $query->asArray()->all();
	}

	public function getLinesSummary( $OR1_ID )
    {
        $query = self::find()->alias($this->_tablePrefix)->select([
                    "sum(if(or2_status='1', 1, 0)) AS received",
                    "sum(if(or2_status='0', 1, 0)) AS unreceived",
                    "count(*) AS total" ])
                ->active()
                ->andFilterWhere(['=', 'or1_id', $OR1_ID]);

        return $query->asArray()->one();
    }

    public function updateStock( $QTY )
    {
        $this->OR2_RECEIVED_QTY += $QTY;
        $this->OR2_STATUS = $this->OR2_RECEIVED_QTY > 0 ? OrderHeader::ORDER_STATUS_RECEIVED : OrderHeader::ORDER_STATUS_OPEN;
        return $this->save(true);
    }

    public function delete()
    {
        $sql = "UPDATE or2_order_line or2 
                    left join or1_order_header or1 on or1.or1_id=or2.or1_id 
                    left join md2_location md2 on md2.md1_id = or2.md1_id and md2.lo1_id = or2.lo1_from_id and md2.md2_delete_flag=0
                SET 
                    or2.OR2_DELETE_FLAG   = '1',
                    md2.MD2_ON_HAND_QTY   = md2.MD2_ON_HAND_QTY + if((coalesce(md2.MD2_NO_INVENTORY,0)=1 OR (or2.LO1_FROM_ID<>or2.LO1_TO_ID AND or1.OR1_STATUS<>" . OrderHeader::ORDER_STATUS_SENT. ")), 0, coalesce(or2.OR2_ORDER_QTY,0)),
                    md2.MD2_AVAILABLE_QTY = md2.MD2_AVAILABLE_QTY + if(coalesce(md2.MD2_NO_INVENTORY,0)=1, 0, coalesce(or2.OR2_ORDER_QTY,0))
                WHERE
                    or2.OR2_ID = :OR2_ID and or2.OR2_DELETE_FLAG=0;";

        $result = Yii::$app->db->createCommand($sql, [
            ':OR2_ID' => $this->OR2_ID
        ])->execute();

        if( $this->LO1_TO_ID == $this->LO1_FROM_ID ){
            $inventory = new Inventory();
            $inventory->removeTechScanItemFromOrder( $this );
        }

        return $result;
    }
}
