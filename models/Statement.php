<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use app\models\scopes\StatementQuery;

/**
 * This is the model class for table "is1_invoice_header".
 *
 * @property int $IS1_ID
 * @property int $GL1_ID
 * @property int $CO1_ID
 * @property int $US1_ID
 * @property int $LO1_FROM_ID
 * @property int $LO1_TO_ID
 * @property string $IS1_NUMBER
 * @property int $IS1_STATUS
 * @property string $IS1_TOTAL_VALUE
 * @property string $IS1_COMMENT
 * @property int $IS1_DELETE_FLAG
 * @property string $IS1_CREATED_ON
 * @property int $IS1_CREATED_BY
 * @property string $IS1_MODIFIED_ON
 * @property int $IS1_MODIFIED_BY
 */
class Statement extends ActiveRecord
{
	const IS1_STATUS_OPEN = 0;
	const IS1_STATUS_PAID = 1;

    protected $_tablePrefix = 'IS1';

    public $MODIFIED_ON;
    public $MODIFIED_BY;
    public $CREATED_BY;
    public $CREATED_ON;
    public $LO1_ID;
    public $FROM_NAME;
    public $TO_NAME;
    public $IS1_STATUS_TEXT;
    public $IS1_IDs;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'is1_invoice_header';
    }

    public static function primaryKey()
    {
        return ['IS1_ID'];
    }

    public static function find()
    {
        return new StatementQuery(get_called_class());
    }

    public function fields() {
        $fields = parent::fields();
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["LO1_ID"] = function($model){ return $model->LO1_ID; };
        $fields["FROM_NAME"] = function($model){ return $model->FROM_NAME; };
        $fields["TO_NAME"] = function($model){ return $model->TO_NAME; };
        $fields["IS1_STATUS_TEXT"] = function($model){ return $model->IS1_STATUS_TEXT; };
        $fields["IS1_IDs"] = function($model){ return $model->IS1_IDs; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['GL1_ID', 'CO1_ID', 'US1_ID', 'LO1_FROM_ID', 'LO1_TO_ID', 'IS1_STATUS', 'IS1_DELETE_FLAG', 'IS1_CREATED_BY', 'IS1_MODIFIED_BY'], 'integer'],
            [['IS1_TOTAL_VALUE', 'LO1_ID'], 'number'],
            [['IS1_COMMENT', 'MODIFIED_ON', 'MODIFIED_BY', 'CREATED_BY', 'CREATED_ON', 'FROM_NAME', 'TO_NAME', 'IS1_STATUS_TEXT', 'IS1_IDs'], 'string'],
            [['IS1_CREATED_ON', 'IS1_MODIFIED_ON', 'IS1_DELETE_FLAG'], 'safe'],
            [['IS1_NUMBER'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IS1_ID' => 'Is1  ID',
            'GL1_ID' => 'Gl1  ID',
            'CO1_ID' => 'Co1  ID',
            'US1_ID' => 'Us1  ID',
            'LO1_FROM_ID' => 'Lo1  From  ID',
            'LO1_TO_ID' => 'Lo1  To  ID',
            'IS1_NUMBER' => 'Is1  Number',
            'IS1_STATUS' => 'Is1  Status',
            'IS1_TOTAL_VALUE' => 'Is1  Total  Value',
            'IS1_COMMENT' => 'Is1  Comment',
            'IS1_DELETE_FLAG' => 'Is1  Delete  Flag',
            'IS1_CREATED_ON' => 'Is1  Created  On',
            'IS1_CREATED_BY' => 'Is1  Created  By',
            'IS1_MODIFIED_ON' => 'Is1  Modified  On',
            'IS1_MODIFIED_BY' => 'Is1  Modified  By',
        ];
    }

    /**
     * @param $params
     * @return StatementQuery|\yii\db\ActiveQuery
     */
    public function getQuery($params)
    {
	    $IS1_STATUS_TEXT_Exp = new Expression('if(IS1.IS1_STATUS = 1, "Paid", "Open")');

	    $query = self::find()->alias('IS1')
                    ->select([  'IS1.IS1_ID', 'IS1.CO1_ID', 'IS1.US1_ID', 'IS1.IS1_COMMENT', 'IS1.IS1_NUMBER', 'IS1.IS1_STATUS',
                                'IS1_STATUS_TEXT' => $IS1_STATUS_TEXT_Exp, 'CO1.CO1_NAME',
                                'IS1.IS1_TOTAL_VALUE', 'IS1.LO1_FROM_ID', 'lo1_from.LO1_NAME AS FROM_NAME',
                                'IS1.LO1_TO_ID', 'lo1_to.LO1_NAME AS TO_NAME', 'IS1.IS1_DELETE_FLAG',
			                    'MODIFIED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('IS1.IS1_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('IS1.IS1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'IS1.IS1_MODIFIED_BY', 'IS1.IS1_CREATED_BY',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("co1_company as CO1" , "CO1.CO1_ID = IS1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
                    ->leftJoin("lo1_location as lo1_from" , "lo1_from.LO1_ID = IS1.LO1_FROM_ID AND lo1_from.LO1_DELETE_FLAG = 0")
                    ->leftJoin("lo1_location as lo1_to" , "lo1_to.LO1_ID = IS1.LO1_TO_ID AND lo1_to.LO1_DELETE_FLAG = 0")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = IS1.IS1_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = IS1.IS1_MODIFIED_BY");
        $query->orderBy(['IS1_NUMBER' => SORT_DESC]);

	    if (isset($params['IS1_IDs'])) {
		    $query->addSelect([ 'IS1_CREATED_ON' => 'DATE_FORMAT(IS1.IS1_CREATED_ON, "%m.%d.%Y %H:%i")',
			    'lo1_to.LO1_NAME AS TO_NAME', 'lo1_to.LO1_ADDRESS1 AS TO_ADDRESS1', 'lo1_to.LO1_ADDRESS2 AS TO_ADDRESS2',
			    'lo1_to.LO1_CITY AS TO_CITY', 'lo1_to.LO1_STATE AS TO_STATE', 'lo1_to.LO1_ZIP AS TO_ZIP',
			    'lo1_to.LO1_PHONE AS TO_PHONE', 'lo1_to.LO1_FAX AS TO_FAX', 'cy1_to.CY1_SHORT_CODE AS TO_COUNTRY',
			    'lo1_from.LO1_NAME AS FROM_NAME', 'lo1_from.LO1_ADDRESS1 AS FROM_ADDRESS1', 'lo1_from.LO1_ADDRESS2 AS FROM_ADDRESS2',
			    'lo1_from.LO1_CITY AS FROM_CITY', 'lo1_from.LO1_STATE AS FROM_STATE', 'lo1_from.LO1_ZIP AS FROM_ZIP',
			    'lo1_from.LO1_PHONE AS FROM_PHONE', 'lo1_from.LO1_FAX AS FROM_FAX', 'cy1_from.CY1_SHORT_CODE AS FROM_COUNTRY']);
		    $query->leftJoin("cy1_country AS cy1_to" , "cy1_to.CY1_ID = lo1_to.CY1_ID");
		    $query->leftJoin("cy1_country AS cy1_from" , "cy1_from.CY1_ID = lo1_from.CY1_ID");
		    $query->andFilterWhere(['IN', 'IS1.IS1_ID', explode(',', $params['IS1_IDs'])]);
	    }

	    if ($params && !empty($params)) {
		    $this->load($params, '');
	    }

        if(isset($this->CO1_ID) && $this->CO1_ID != 0) {
            $query->andFilterWhere(['=', 'IS1.CO1_ID', $this->CO1_ID]);
        }

        if(isset($this->LO1_ID) && $this->LO1_ID != 0) {
            $query->andFilterWhere(['=', 'IS1.LO1_TO_ID', $this->LO1_ID]);
        }

        if(isset($this->IS1_COMMENT)) {
            $query->andFilterWhere(['like', 'IS1.IS1_COMMENT', $this->IS1_COMMENT]);
        }

        if(isset($this->IS1_NUMBER)) {
            $query->andFilterWhere(['like', 'IS1.IS1_NUMBER', $this->IS1_NUMBER]);
        }

        if(isset($this->IS1_TOTAL_VALUE)) {
            $query->andFilterWhere(['like', 'IS1.IS1_TOTAL_VALUE', $this->IS1_TOTAL_VALUE]);
        }

        if(isset($this->FROM_NAME)) {
            $query->andFilterWhere(['=', 'IS1.LO1_FROM_ID', $this->FROM_NAME]);
        }

        if(isset($this->TO_NAME)) {
            $query->andFilterWhere(['=', 'IS1.LO1_TO_ID', $this->TO_NAME]);
        }

        if(isset($this->MODIFIED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(IS1.IS1_MODIFIED_ON, "%m-%d-%Y")', $this->MODIFIED_ON]);
        }

        if(isset($this->MODIFIED_BY)) {
            $query->andFilterWhere(['=', 'us1_modify.US1_ID', $this->MODIFIED_BY]);
        }

        if(isset($this->CREATED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(IS1.IS1_CREATED_ON, "%m-%d-%Y")', $this->CREATED_ON]);
        }

        if(isset($this->CREATED_BY)) {
            $query->andFilterWhere(['=', 'us1_create.US1_ID', $this->CREATED_BY]);
        }

	    if (isset($params['IS1_STATUS_TEXT']) && $params['IS1_STATUS_TEXT'] != '') {
		    $query->andFilterWhere(['=', $IS1_STATUS_TEXT_Exp, $params['IS1_STATUS_TEXT']]);
	    }

	    $query->initScope();

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['IS1_NUMBER'] = SORT_DESC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $params['perPage'],
               'page'     => $params['page']
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getStatement($id)
    {
        $query = $this->getQuery(null);
        $query->where(['=', 'IS1.IS1_ID', $id]);

	    return $query->asArray()->all();
    }

	public function getInvoiceNumber()
	{
		$query = self::find()->alias('IS1')
			->select(['INVOICE_NUMBER' => new Expression('if(MAX(IS1.IS1_NUMBER) is null, 100001, MAX(IS1.IS1_NUMBER)+1)')]);

		return $query->asArray()->scalar();
	}
}
