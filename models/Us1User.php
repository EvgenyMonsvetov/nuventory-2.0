<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 30/05/2018
 * Time: 09:46
 */

namespace app\models;

use yii\db\ActiveRecord;

class Us1User extends ActiveRecord
{
    public static function tableName()
    {
        return 'us1_user';
    }
}