<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "al1_access_list".
 *
 * @property int $AL1_ID
 * @property int $AL1_OBJECT_TYPE
 * @property int $AL1_OBJECT_ID
 * @property int $user_id
 * @property int $CO1_ID
 */
class AccessList extends \yii\db\ActiveRecord
{
    const OBJ_TYPE_CUSTOMER = 1;
    const OBJ_TYPE_VENDOR   = 2;
    const OBJ_TYPE_COMPANY  = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'al1_access_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AL1_OBJECT_TYPE', 'AL1_OBJECT_ID', 'user_id'], 'required'],
            [['AL1_OBJECT_TYPE', 'AL1_OBJECT_ID', 'user_id', 'CO1_ID'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AL1_ID' => 'Al1  ID',
            'AL1_OBJECT_TYPE' => 'Al1  Object  Type',
            'AL1_OBJECT_ID' => 'Al1  Object  ID',
            'user_id' => 'User ID',
            'CO1_ID' => 'Company ID'
        ];
    }
}
