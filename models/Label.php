<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "la2_label".
 *
 * @property int $LA2_ID
 * @property int $LA1_ID
 * @property string $LA2_KEY
 * @property string $LA2_TEXT
 * @property string $LA2_DESCRIPTION
 * @property int $LA2_DELETE_FLAG
 * @property int $LA2_CREATED_BY
 * @property string $LA2_CREATED_ON
 * @property int $LA2_MODIFIED_BY
 * @property string $LA2_MODIFIED_ON
 */
class Label extends ActiveRecord
{
    protected $_tablePrefix = 'LA2';

    public $LA1_NAME;

    public static function primaryKey()
    {
        return ['LA2_ID'];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'la2_label';
    }

    public function getLanguage()
    {
       return $this->hasOne(Language::className(), ['LA1_ID' => 'LA2_ID']);
    }

    public function fields() {
        $fields = parent::fields();
        $fields["LA1_NAME"] = function($model){ return $model->LA1_NAME; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LA1_ID'], 'required'],
            [['LA1_ID', 'LA2_DELETE_FLAG', 'LA2_CREATED_BY', 'LA2_MODIFIED_BY'], 'integer'],
            [['LA2_TEXT', 'LA2_DESCRIPTION'], 'string'],
            [['LA2_CREATED_ON', 'LA2_MODIFIED_ON'], 'safe'],
            [['LA2_KEY', 'LA1_NAME'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LA2_ID' => 'La2  ID',
            'LA1_ID' => 'La1  ID',
            'LA2_KEY' => 'La2  Key',
            'LA2_TEXT' => 'La2  Text',
            'LA2_DESCRIPTION' => 'La2  Description',
            'LA2_DELETE_FLAG' => 'La2  Delete  Flag',
            'LA2_CREATED_BY' => 'La2  Created  By',
            'LA2_CREATED_ON' => 'La2  Created  On',
            'LA2_MODIFIED_BY' => 'La2  Modified  By',
            'LA2_MODIFIED_ON' => 'La2  Modified  On',
        ];
    }

	public function getQuery($params) {
		$query = self::find()->alias("la2_label")
			->select(["la2_label.LA2_KEY as LA2_KEY", "la2_label.LA2_KEY as LA2_ID"])->distinct()
			->leftJoin("la1_language" , "la1_language.LA1_ID = la2_label.LA1_ID");

		if (isset($params['langs'])) {
			$langs = explode(',', $params['langs']);
			foreach ($langs as $lang) {
				$query->leftJoin("la2_label as la2".$lang , "la2".$lang.".LA2_KEY = la2_label.LA2_KEY AND la2".$lang.".LA1_ID = ".$lang);
				$query->addSelect("(la2".$lang. ".LA2_TEXT) as `".$lang."`");
			}
		}

		$query->orderBy(["la2_label.LA2_KEY" => SORT_ASC]);
		$query->andFilterWhere(['=',    'la2_label.LA2_DELETE_FLAG', 0]);

		if (isset($params)) {
			foreach ($params as $key => $value) {
				if ($key !== 'perPage' && $key !== 'page' && $key !== 'langs' && $key !== 'keys') {
					$query->andFilterWhere(['like',    "la2".$key.".LA2_TEXT", $value]);
				}
			}
		}

		return $query;
	}
}
