<?php

namespace app\models;

use app\models\scopes\PurchaseOrderQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "po1_order_header".
 *
 * @property int $PO1_ID
 * @property int $CO1_ID
 * @property int $GL1_ID
 * @property int $LO1_ID
 * @property int $VD1_ID
 * @property int $US1_ID
 * @property string $PO1_NUMBER
 * @property int $PO1_STATUS
 * @property string $PO1_TOTAL_VALUE
 * @property string $PO1_COMMENT
 * @property int $PO1_DELETE_FLAG
 * @property string $PO1_CREATED_ON
 * @property int $PO1_CREATED_BY
 * @property string $PO1_MODIFIED_ON
 * @property int $PO1_MODIFIED_BY
 * @property string $PO1_ORDERED_BY
 * @property int $CA1_ID
 * @property string $PO1_SHIPPING_METHOD
 * @property int $PO1_UPLOAD_STATUS
 * @property string $PO1_CREATED_ON_UTC
 * @property string $PO1_CREATED_ON_LOCAL
 */
class PurchaseOrder extends ActiveRecord
{
    protected $_tablePrefix = 'PO1';

    public $CA1_ID;
    public $CREATED_ON;
    public $ORDERED_BY;
    public $ORDERED_ON;
    public $LO1_NAME;
    public $CO1_NAME;
    public $MODIFIED_BY;
    public $CREATED_BY;
    public $MODIFIED_ON;
    public $PO1_STATUS_TEXT;
    public $PO1_UPLOAD_STATUS_TEXT;
	public $PO1_IDs;
	public $VD1_NAME;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'po1_order_header';
    }

    public static function primaryKey()
    {
        return ['PO1_ID'];
    }

    /**
     * {@inheritdoc}
     * @return PurchaseOrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PurchaseOrderQuery(get_called_class());
    }

    public function fields() {
        $fields = parent::fields();
        $fields["CA1_ID"] = function($model){ return $model->CA1_ID; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["ORDERED_BY"] = function($model){ return $model->ORDERED_BY; };
        $fields["ORDERED_ON"] = function($model){ return $model->ORDERED_ON; };
        $fields["PO1_COMMENT"] = function($model){ return $model->PO1_COMMENT; };
        $fields["LO1_NAME"] = function($model){ return $model->LO1_NAME; };
        $fields["CO1_NAME"] = function($model){ return $model->CO1_NAME; };
        $fields["PO1_STATUS_TEXT"] = function($model){ return $model->PO1_STATUS_TEXT; };
        $fields["PO1_UPLOAD_STATUS_TEXT"] = function($model){ return $model->PO1_UPLOAD_STATUS_TEXT; };
        $fields["PO1_IDs"] = function($model){ return $model->PO1_IDs; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CO1_ID', 'GL1_ID', 'LO1_ID', 'VD1_ID', 'US1_ID', 'PO1_STATUS', 'PO1_ORDERED_BY', 'PO1_DELETE_FLAG', 'PO1_CREATED_BY', 'PO1_MODIFIED_BY', 'CA1_ID', 'PO1_UPLOAD_STATUS', 'CA1_ID'], 'integer'],
            [['PO1_TOTAL_VALUE'], 'number'],
            [['PO1_COMMENT', 'PO1_IDs'], 'string'],
            [['PO1_CREATED_ON', 'PO1_MODIFIED_ON', 'PO1_CREATED_ON_UTC', 'PO1_CREATED_ON_LOCAL'], 'safe'],
            [['PO1_NUMBER'], 'validateNumber', 'skipOnEmpty' => false],
            [['CREATED_ON', 'ORDERED_BY', 'ORDERED_ON', 'PO1_SHIPPING_METHOD', 'PO1_STATUS_TEXT', 'PO1_UPLOAD_STATUS_TEXT', 'LO1_NAME', 'CO1_NAME', 'PO1_COMMENT', 'MODIFIED_BY', 'MODIFIED_ON', 'CREATED_BY'], 'string', 'max' => 50],
            [['PO1_SHIPPING_METHOD'], 'string', 'max' => 100],
        ];
    }

    public function validateNumber($attribute, $params)
    {
        if( $this->isNewRecord ){
            if(empty($this->$attribute) || !preg_match('/^[0-9]+[0-9\-]+$/', $this->$attribute)){
                if(strlen($this->$attribute)>2){
                    $this->addError($attribute, 'Incorrect PO Number.');
                }else{
                    $this->addError($attribute, 'The PO Number must be specified.');
                }
            }else{
                $PO = static::find()
                        ->andWhere(['CO1_ID' => $this->CO1_ID])
                        ->andWhere(['PO1_NUMBER' => $this->PO1_NUMBER])
                        ->andWhere(['PO1_DELETE_FLAG' => 0])
                        ->one();

                if($PO){
                    $this->addError($attribute, Yii::t('app', "That PO Number already exists for current company."));
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PO1_ID' => 'Po1  ID',
            'CO1_ID' => 'Co1  ID',
            'GL1_ID' => 'Gl1  ID',
            'LO1_ID' => 'Lo1  ID',
            'VD1_ID' => 'Vd1  ID',
            'US1_ID' => 'Us1  ID',
            'PO1_NUMBER' => 'Number',
            'PO1_STATUS' => 'Status',
            'PO1_TOTAL_VALUE' => 'Total  Value',
            'PO1_COMMENT' => 'Comment',
            'PO1_DELETE_FLAG' => 'Delete  Flag',
            'PO1_CREATED_ON' => 'Created  On',
            'PO1_CREATED_BY' => 'Created  By',
            'PO1_MODIFIED_ON' => 'Modified  On',
            'PO1_MODIFIED_BY' => 'Modified  By',
            'PO1_ORDERED_BY' => 'Ordered  By',
            'CA1_ID' => 'Ca1  ID',
            'PO1_SHIPPING_METHOD' => 'Shipping  Method',
            'PO1_UPLOAD_STATUS' => 'Upload  Status',
            'PO1_CREATED_ON_UTC' => 'Created  On  Utc',
            'PO1_CREATED_ON_LOCAL' => 'Created  On  Local',
        ];
    }

    public function getQuery($params)
    {
	    $PO1_UPLOAD_STATUS_TEXT_Exp = new Expression('if(PO1.PO1_UPLOAD_STATUS = 4, "Sending", if(PO1.PO1_UPLOAD_STATUS = 1, "In Progress", if(PO1.PO1_UPLOAD_STATUS = 2, "Completed", "None")))');

        $query = PurchaseOrder::find()->alias('PO1')
                    ->select([  'PO1.PO1_ID', 'PO1.PO1_ORDERED_BY', 'PO1.PO1_NUMBER', 'PO1.PO1_SHIPPING_METHOD', 'PO1.PO1_STATUS',
                                'PO1.PO1_UPLOAD_STATUS', 'PO1.PO1_TOTAL_VALUE', 'PO1.PO1_COMMENT', 'PO1.PO1_MODIFIED_BY', 'PO1.PO1_CREATED_BY',
                                'PO1.PO1_DELETE_FLAG', 'LO1.LO1_SHORT_CODE', 'LO1.LO1_NAME', 'VD1.VD1_NAME', 'CA1.CA1_ID', 'CA1.CA1_NAME',
                                "PO1_RECEIVED_LINES" => '(Select count(po2.PO2_ID) From po2_order_line as po2 Where po2.PO1_ID = PO1.PO1_ID and po2.PO2_DELETE_FLAG = 0 and po2.PO2_STATUS = 1)',
                                "PO1_ALL_LINES" => '(Select count(po2.PO2_ID) From po2_order_line as po2 Where po2.PO1_ID = PO1.PO1_ID and po2.PO2_DELETE_FLAG = 0)',
                                'if(PO1.PO1_DELETE_FLAG = 1, "Deleted", if(PO1.PO1_STATUS = 1, "Received", "Open")) AS PO1_STATUS_TEXT', 'CO1.CO1_ID', 'CO1.CO1_NAME',
                                'PO1_UPLOAD_STATUS_TEXT' => $PO1_UPLOAD_STATUS_TEXT_Exp,
			                    'MODIFIED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('PO1_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('PO1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'ORDERED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('PO1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME',
                                'ORDERED_BY' => 'us3_user.US1_NAME'])
                    ->leftJoin("co1_company as CO1" , "CO1.CO1_ID = PO1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
                    ->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = PO1.LO1_ID AND LO1.LO1_DELETE_FLAG = 0")
                    ->leftJoin("vd1_vendor as VD1" , "VD1.VD1_ID = PO1.VD1_ID AND VD1.VD1_DELETE_FLAG = 0")
                    ->leftJoin("ca1_category as CA1" , "CA1.CA1_ID = PO1.CA1_ID AND CA1.CA1_DELETE_FLAG = 0")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = PO1.PO1_MODIFIED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = PO1.PO1_CREATED_BY")
                    ->leftJoin("us1_user as us3_user" , "us3_user.US1_ID = PO1.PO1_ORDERED_BY");

        $query->orderBy(['PO1_MODIFIED_ON' => SORT_DESC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

	    if (isset($this->PO1_IDs)) {
		    $query->addSelect([ 'vd1.VD1_CUSTOMER_NO AS VEND_CUSTOMER_NO', 'VD1.VD1_NAME AS VEND_NAME', 'VD1.VD1_ADDRESS1 AS VEND_ADDRESS1',
			    'VD1.VD1_ADDRESS2 AS VEND_ADDRESS2', 'VD1.VD1_CITY AS VEND_CITY', 'VD1.VD1_STATE AS VEND_STATE', 'VD1.VD1_ZIP AS VEND_ZIP',
			    'VD1.VD1_PHONE AS VEND_PHONE', 'VD1.VD1_FAX AS VEND_FAX', 'cy1_vd1.CY1_SHORT_CODE AS VEND_COUNTRY',
			    'LO1.LO1_SHORT_CODE AS LO1_SHORT_CODE', 'LO1.LO1_NAME AS LO1_NAME', 'LO1.LO1_ADDRESS1 AS LO1_ADDRESS1',
			    'LO1.LO1_ADDRESS2 AS LO1_ADDRESS2', 'LO1.LO1_CITY AS LO1_CITY', 'LO1.LO1_STATE AS LO1_STATE', 'LO1.LO1_ZIP AS LO1_ZIP',
			    'LO1.LO1_PHONE AS LO1_PHONE', 'LO1.LO1_FAX AS LO1_FAX', 'cy1_lo1.CY1_SHORT_CODE AS LO1_COUNTRY']);
		    $query->leftJoin("cy1_country AS cy1_lo1" , "cy1_lo1.CY1_ID = lo1.CY1_ID");
		    $query->leftJoin("cy1_country AS cy1_vd1" , "cy1_vd1.CY1_ID = vd1.CY1_ID");
		    $query->andFilterWhere(['IN', 'PO1.PO1_ID', explode(',', $this->PO1_IDs)]);
	    }

        if(isset($this->CO1_ID) && $this->CO1_ID != 0) {
            $query->andFilterWhere(['=', 'PO1.CO1_ID', $this->CO1_ID]);
        }

        if(isset($this->LO1_ID) && $this->LO1_ID != 0) {
            $query->andFilterWhere(['=', 'PO1.LO1_ID', $this->LO1_ID]);
        }

        if(isset($this->CREATED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(PO1.PO1_CREATED_ON, "%m-%d-%Y")', $this->CREATED_ON]);
        }

        if(isset($this->ORDERED_BY)) {
            $query->andFilterWhere(['=', 'us3_user.US1_ID', $this->ORDERED_BY]);
        }

        if(isset($this->PO1_NUMBER)) {
            $query->andFilterWhere(['like', 'PO1.PO1_NUMBER', $this->PO1_NUMBER]);
        }

        if(isset($this->PO1_SHIPPING_METHOD)) {
            $query->andFilterWhere(['like', 'PO1.PO1_SHIPPING_METHOD', $this->PO1_SHIPPING_METHOD]);
        }

        if(isset($this->PO1_STATUS_TEXT)) {
	        if($this->PO1_STATUS_TEXT == 'open_received') {
		        $query->andFilterWhere(['=', 'PO1.PO1_DELETE_FLAG', 0]);
	        } else if($this->PO1_STATUS_TEXT == 'received') {
                $query->andFilterWhere(['=', 'PO1.PO1_STATUS', 1]);
	            $query->andFilterWhere(['=', 'PO1.PO1_DELETE_FLAG', 0]);
            } else if($this->PO1_STATUS_TEXT == 'open') {
                $query->andFilterWhere(['<>', 'PO1.PO1_STATUS', 1]);
	            $query->andFilterWhere(['=', 'PO1.PO1_DELETE_FLAG', 0]);
            } else if($this->PO1_STATUS_TEXT == 'deleted') {
	            $query->andFilterWhere(['=', 'PO1.PO1_DELETE_FLAG', 1]);
            }
        }

        if(isset($this->PO1_UPLOAD_STATUS_TEXT) && $this->PO1_UPLOAD_STATUS_TEXT != '') {
	        $query->andFilterWhere(['IN', $PO1_UPLOAD_STATUS_TEXT_Exp, explode(',', $this->PO1_UPLOAD_STATUS_TEXT)]);
        }

        if(isset($this->PO1_TOTAL_VALUE)) {
            $query->andFilterWhere(['like', 'PO1.PO1_TOTAL_VALUE', $this->PO1_TOTAL_VALUE]);
        }

        if(isset($this->LO1_NAME)) {
            $query->andFilterWhere(['=', 'PO1.LO1_ID', $this->LO1_NAME]);
        }

        if(isset($this->CO1_NAME)) {
            $query->andFilterWhere(['=', 'PO1.CO1_ID', $this->CO1_NAME]);
        }

        if(isset($this->PO1_COMMENT)) {
            $query->andFilterWhere(['like', 'PO1.PO1_COMMENT', $this->PO1_COMMENT]);
        }

        if(isset($this->MODIFIED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(PO1.PO1_MODIFIED_ON, "%m-%d-%Y")', $this->MODIFIED_ON]);
        }

        if(isset($this->MODIFIED_BY)) {
            $query->andFilterWhere(['=', 'us1_modify.US1_ID', $this->MODIFIED_BY]);
        }

        if(isset($this->CREATED_BY)) {
            $query->andFilterWhere(['=', 'us1_create.US1_ID', $this->CREATED_BY]);
        }

	    if(isset($params['VD1_NAME'])) {
		    $query->andFilterWhere(['=', 'PO1.VD1_ID', $params['VD1_NAME']]);
	    }

        $query->initScope();

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['PO1_NUMBER'] = SORT_ASC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $params['perPage'],
               'page'     => $params['page']
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getPurchaseOrder($id)
    {
        $query = $this->getQuery(null);
        $query->where(['=', 'PO1.PO1_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function receive( $PO1_ID )
    {
        $sql = "
            UPDATE po2_order_line
                LEFT JOIN po1_order_header on po1_order_header.po1_id = po2_order_line.po1_id
                LEFT JOIN md2_location as mfrom on po1_order_header.lo1_id=mfrom.lo1_id and po2_order_line.md1_id = mfrom.md1_id
                LEFT JOIN md1_master_data as m on m.md1_id = mfrom.md1_id
                LEFT JOIN um2_conversion AS um2 ON (um2.MD1_ID=po2_order_line.MD1_ID) AND (um2.UM1_TO_ID=mfrom.UM1_RECEIPT_ID) AND (um2.UM1_FROM_ID=mfrom.UM1_PURCHASE_ID) AND (um2.UM2_DELETE_FLAG=0) 
            SET 
                mfrom.md2_on_hand_qty = if ((mfrom.md2_no_inventory=0), coalesce(mfrom.md2_on_hand_qty, 0) + (po2_order_line.po2_order_qty * coalesce(um2.UM2_FACTOR, 1)), mfrom.md2_on_hand_qty),
                mfrom.md2_available_qty = if ((mfrom.md2_no_inventory=0), coalesce(mfrom.md2_available_qty, 0) + (po2_order_line.po2_order_qty * coalesce(um2.UM2_FACTOR, 1)), mfrom.md2_available_qty),
                po2_order_line.po2_received_qty = coalesce(po2_order_line.po2_order_qty, 0)* coalesce(um2.UM2_FACTOR, 1),
                po2_order_line.po2_status = '1',
                po1_order_header.po1_status = '1'
            WHERE po1_order_header.po1_status='0' and po2_order_line.po2_status='0' and po2_order_line.po1_id= :PO1_ID";

        return Yii::$app->db->createCommand($sql, [
            ':PO1_ID' => $PO1_ID
        ])->execute();
    }

	public function getMaterialsBudgetReport($params)
	{
		$user = Yii::$app->user->getIdentity();

		$query = PurchaseOrder::find()->alias('PO1')
			->select(['SUM(PO1.PO1_TOTAL_VALUE) AS TOTAL_VALUE', 'CREATED_ON' => 'DATE_FORMAT(PO1_CREATED_ON, "%b %d, %Y")', 'DATE_MONTH' => 'DATE_FORMAT(PO1_CREATED_ON, "%b, %Y")'])
			->leftJoin("co1_company as CO1" , "CO1.CO1_ID = PO1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
			->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = PO1.LO1_ID AND LO1.LO1_DELETE_FLAG = 0")
			->andFilterWhere(['=', 'PO1.PO1_DELETE_FLAG', 0]);

		if(isset($user->CO1_ID) && $user->CO1_ID != 0) {
			$query->andFilterWhere(['=', 'PO1.CO1_ID', $user->CO1_ID]);
		}

		if(isset($user->LO1_ID) && $user->LO1_ID != 0) {
			$query->andFilterWhere(['=', 'PO1.LO1_ID', $user->LO1_ID]);
		}

		if(isset($params['START_DATE'])) {
			$query->andFilterWhere(['>=', 'DATE(PO1.PO1_CREATED_ON)', $params['START_DATE']]);
		}

		if(isset($params['END_DATE'])) {
			$query->andFilterWhere(['<=', 'DATE(PO1.PO1_CREATED_ON)', $params['END_DATE']]);
		}

		$query->initScope();

		$query->groupBy(['CREATED_ON']);
		$query->orderBy(['PO1.PO1_CREATED_ON' => SORT_ASC]);

		return $query->asArray()->all();
	}

	public function getPurchaseOrderTotals($params)
	{
		$user = Yii::$app->user->getIdentity();

		$query = PurchaseOrder::find()->alias('PO1')
			->select(['SUM(PO1.PO1_TOTAL_VALUE) AS TOTAL_VALUE',
				'CREATED_ON' => 'DATE_FORMAT(PO1_CREATED_ON, "%b %d, %Y")',
				'MONTH' => 'DATE_FORMAT(PO1_CREATED_ON, "%b %Y")',
				'FORMATTED_DATE' => 'DATE_FORMAT(PO1_CREATED_ON, "%Y%m%d")'])
			->leftJoin("co1_company as CO1" , "CO1.CO1_ID = PO1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
			->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = PO1.LO1_ID AND LO1.LO1_DELETE_FLAG = 0")
			->andFilterWhere(['=', 'PO1.PO1_DELETE_FLAG', 0]);

		if (isset($user->CO1_ID) && $user->CO1_ID != 0) {
			$query->andFilterWhere(['=', 'PO1.CO1_ID', $user->CO1_ID]);
		}

		if (isset($user->LO1_ID) && $user->LO1_ID != 0) {
			$query->andFilterWhere(['=', 'PO1.LO1_ID', $user->LO1_ID]);
		}

		if (isset($params['START_DATE'])) {
			$query->andFilterWhere(['>=', 'DATE(PO1.PO1_CREATED_ON)', $params['START_DATE']]);
		}

		if (isset($params['END_DATE'])) {
			$query->andFilterWhere(['<=', 'DATE(PO1.PO1_CREATED_ON)', $params['END_DATE']]);
		}

		if (isset($params['BY_VENDOR'])) {
			$query->addSelect(['VD1.VD1_NAME']);
			$query->leftJoin("vd1_vendor as VD1" , "VD1.VD1_ID = PO1.VD1_ID AND VD1.VD1_DELETE_FLAG = 0");
			$query->groupBy(['MONTH', 'PO1.VD1_ID']);
			$query->orderBy(["FORMATTED_DATE" => SORT_ASC, "VD1.VD1_NAME" => SORT_ASC]);
		} else {
			$query->groupBy(['MONTH']);
			$query->orderBy(["FORMATTED_DATE" => SORT_ASC]);
		}

		return $query->asArray()->all();
	}
}
