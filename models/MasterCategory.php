<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "ca0_category".
 *
 * @property int $CA0_ID
 * @property int $CO1_ID
 * @property string $CA0_NAME
 * @property string $CA0_DESCRIPTION
 * @property int $CA0_DELETE_FLAG
 * @property int $CA0_CREATED_BY
 * @property string $CA0_CREATED_ON
 * @property int $CA0_MODIFIED_BY
 * @property string $CA0_MODIFIED_ON
 */
class MasterCategory extends ActiveRecord
{
    protected $_tablePrefix = 'CA0';

	public $CA0_CREATED_BY_NAME;
	public $CA0_MODIFIED_BY_NAME;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ca0_category';
    }

	public function fields() {
		$fields = parent::fields();
		$fields["CA0_CREATED_BY_NAME"] = function($model){ return $model->CA0_CREATED_BY_NAME; };
		$fields["CA0_MODIFIED_BY_NAME"] = function($model){ return $model->CA0_MODIFIED_BY_NAME; };
		return $fields;
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CA0_ID', 'CO1_ID', 'CA0_DELETE_FLAG', 'CA0_CREATED_BY', 'CA0_MODIFIED_BY'], 'integer'],
            [['CA0_CREATED_ON', 'CA0_MODIFIED_ON'], 'safe'],
            [['CA0_NAME'], 'string', 'max' => 10],
            [['CA0_DESCRIPTION', 'CA0_CREATED_BY_NAME', 'CA0_MODIFIED_BY_NAME'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CA0_ID' => 'Ca0  ID',
            'CO1_ID' => 'Co1  ID',
            'CA0_NAME' => 'Ca0  Name',
            'CA0_DESCRIPTION' => 'Ca0  Description',
            'CA0_DELETE_FLAG' => 'Ca0  Delete  Flag',
            'CA0_CREATED_BY' => 'Ca0  Created  By',
            'CA0_CREATED_ON' => 'Ca0  Created  On',
            'CA0_MODIFIED_BY' => 'Ca0  Modified  By',
            'CA0_MODIFIED_ON' => 'Ca0  Modified  On',
        ];
    }

    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery( $params )
    {
        $query = self::find()->alias('CA0')->select([
                'CA0.*',
		        'CA0_MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('CA0_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
		        'CA0_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('CA0_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                'CA0_CREATED_BY_NAME'  => 'us1_create.US1_NAME',
                'CA0_MODIFIED_BY_NAME' => 'us1_modify.US1_NAME'
            ])
            ->andFilterWhere(['=', 'CA0_DELETE_FLAG', 0])
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = CA0.CA0_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = CA0.CA0_MODIFIED_BY");

        $this->load($params, '');

	    if (isset($this->CA0_ID)) {
		    $query->andFilterWhere(['=',    'CA0_ID',   $this->CA0_ID]);
	    }
	    if (isset($this->CA0_NAME)) {
            $query->andFilterWhere(['like', 'CA0_NAME', $this->CA0_NAME]);
	    }
	    if (isset($this->CA0_DESCRIPTION)) {
		    $query->andFilterWhere(['like', 'CA0_DESCRIPTION', $this->CA0_DESCRIPTION]);
	    }

        return $query;
    }

    public function search($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['CA0_NAME'] = SORT_ASC;
        }

        $query = $this->getSearchQuery( $params );

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => $params['perPage']
            ],
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }
}
