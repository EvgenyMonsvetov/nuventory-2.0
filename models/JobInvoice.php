<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use app\models\scopes\JobInvoiceQuery;

/**
 * This is the model class for table "ij1_invoice_header".
 *
 * @property int $IJ1_ID
 * @property int $JT1_ID
 * @property int $CO1_ID
 * @property int $GL1_ID
 * @property int $US1_ID
 * @property int $LO1_ID
 * @property int $CU1_ID
 * @property int $IN1_ID
 * @property int $VD1_ID
 * @property int $IJ1_NUMBER
 * @property int $IJ1_STATUS
 * @property int $IJ1_PRINT_FLAG
 * @property string $IJ1_TOTAL_VALUE
 * @property string $IJ1_TOTAL_SELL
 * @property string $IJ1_COMMENT
 * @property int $IJ1_DELETE_FLAG
 * @property string $IJ1_CREATED_ON
 * @property int $IJ1_CREATED_BY
 * @property string $IJ1_MODIFIED_ON
 * @property int $IJ1_MODIFIED_BY
 */
class JobInvoice extends ActiveRecord
{
    protected $_tablePrefix = 'IJ1';

    public $CREATED_ON;
    public $CREATED_BY;
    public $MODIFIED_BY;
    public $MODIFIED_ON;
    public $JT1_NUMBER;
	public $IJ1_IDs;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ij1_invoice_header';
    }

    public static function primaryKey()
    {
        return ['IJ1_ID'];
    }

    public static function find()
    {
        return new JobInvoiceQuery(get_called_class());
    }

    public function fields() {
        $fields = parent::fields();
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        $fields["JT1_NUMBER"] = function($model){ return $model->JT1_NUMBER; };
        $fields["IJ1_IDs"] = function($model){ return $model->IJ1_IDs; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JT1_ID', 'CO1_ID', 'GL1_ID', 'US1_ID', 'LO1_ID', 'CU1_ID', 'IN1_ID', 'VD1_ID', 'IJ1_NUMBER', 'IJ1_STATUS', 'IJ1_PRINT_FLAG', 'IJ1_DELETE_FLAG', 'IJ1_CREATED_BY', 'IJ1_MODIFIED_BY', 'JT1_NUMBER'], 'integer'],
            [['IJ1_NUMBER'], 'required'],
            [['IJ1_TOTAL_VALUE', 'IJ1_TOTAL_SELL'], 'number'],
            [['IJ1_COMMENT', 'CREATED_ON', 'CREATED_BY', 'MODIFIED_BY', 'MODIFIED_ON', 'IJ1_IDs'], 'string'],
            [['IJ1_CREATED_ON', 'IJ1_MODIFIED_ON'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IJ1_ID' => 'Ij1  ID',
            'JT1_ID' => 'Jt1  ID',
            'CO1_ID' => 'Co1  ID',
            'GL1_ID' => 'Gl1  ID',
            'US1_ID' => 'Us1  ID',
            'LO1_ID' => 'Lo1  ID',
            'CU1_ID' => 'Cu1  ID',
            'IN1_ID' => 'In1  ID',
            'VD1_ID' => 'Vd1  ID',
            'IJ1_NUMBER' => 'Ij1  Number',
            'IJ1_STATUS' => 'Ij1  Status',
            'IJ1_PRINT_FLAG' => 'Ij1  Print  Flag',
            'IJ1_TOTAL_VALUE' => 'Ij1  Total  Value',
            'IJ1_TOTAL_SELL' => 'Ij1  Total  Sell',
            'IJ1_COMMENT' => 'Ij1  Comment',
            'IJ1_DELETE_FLAG' => 'Ij1  Delete  Flag',
            'IJ1_CREATED_ON' => 'Ij1  Created  On',
            'IJ1_CREATED_BY' => 'Ij1  Created  By',
            'IJ1_MODIFIED_ON' => 'Ij1  Modified  On',
            'IJ1_MODIFIED_BY' => 'Ij1  Modified  By',
        ];
    }

    public function getQuery($params)
    {
        $query = self::find()->alias('IJ1')
                    ->select([  'IJ1.IJ1_ID', 'IJ1.JT1_ID', 'IJ1.CO1_ID', 'IJ1.IJ1_NUMBER', 'JT1.JT1_NUMBER', 'IJ1.IJ1_PRINT_FLAG', 'IJ1.IJ1_TOTAL_VALUE',
                                'CO1.CO1_SHORT_CODE AS ORDER_BY_COMPANY_CODE', 'CO1.CO1_NAME AS ORDER_BY_COMPANY_NAME', 'IJ1.IJ1_TOTAL_SELL', 'IJ1.IJ1_COMMENT',
                                'IJ1.LO1_ID', 'LO1.LO1_SHORT_CODE AS ORDER_BY_LOCATION_CODE', 'LO1.LO1_NAME AS ORDER_BY_LOCATION_NAME', 'IJ1.IJ1_STATUS',
                                'IJ1.IJ1_CREATED_BY', 'IJ1.IJ1_MODIFIED_BY',
			                    'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IJ1.IJ1_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IJ1.IJ1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("jt1_job_ticket as JT1" , "JT1.JT1_ID = IJ1.JT1_ID AND JT1.JT1_DELETE_FLAG=0")
                    ->leftJoin("co1_company as CO1" , "CO1.CO1_ID = IJ1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
                    ->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = IJ1.LO1_ID AND LO1.LO1_DELETE_FLAG = 0")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = IJ1.IJ1_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = IJ1.IJ1_MODIFIED_BY");
        $query->orderBy(['IJ1_NUMBER' => SORT_DESC]);

	    if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->IJ1_NUMBER)) {
            $query->andFilterWhere(['like', 'IJ1.IJ1_NUMBER', $this->IJ1_NUMBER]);
        }

        if(isset($this->JT1_NUMBER)) {
            $query->andFilterWhere(['like', 'JT1.JT1_NUMBER', $this->JT1_NUMBER]);
        }

        if(isset($this->IJ1_PRINT_FLAG)) {
            if($this->IJ1_PRINT_FLAG == 1) {
                $query->andFilterWhere(['=', 'IJ1.IJ1_PRINT_FLAG', 1]);
            } else {
                $query->andFilterWhere(['=', 'IJ1.IJ1_PRINT_FLAG', 0]);
            }
        }

        if(isset($this->IJ1_TOTAL_VALUE)) {
            $query->andFilterWhere(['like', 'IJ1.IJ1_TOTAL_VALUE', $this->IJ1_TOTAL_VALUE]);
        }

        if(isset($this->IJ1_TOTAL_SELL)) {
            $query->andFilterWhere(['like', 'IJ1.IJ1_TOTAL_SELL', $this->IJ1_TOTAL_SELL]);
        }

        if(isset($this->IJ1_COMMENT)) {
            $query->andFilterWhere(['like', 'IJ1.IJ1_COMMENT', $this->IJ1_COMMENT]);
        }

        if(isset($this->MODIFIED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(IJ1.IJ1_MODIFIED_ON, "%m-%d-%Y")', $this->MODIFIED_ON]);
        }

        if(isset($this->MODIFIED_BY)) {
            $query->andFilterWhere(['=', 'us1_modify.US1_ID', $this->MODIFIED_BY]);
        }

        if(isset($this->CREATED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(IJ1.IJ1_CREATED_ON, "%m-%d-%Y")', $this->CREATED_ON]);
        }

        if(isset($this->CREATED_BY)) {
            $query->andFilterWhere(['=', 'us1_create.US1_ID', $this->CREATED_BY]);
        }

        $query->initScope();

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['IJ1_NUMBER'] = SORT_DESC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $params['perPage'],
               'page'     => $params['page']
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getJobInvoice($id)
    {
        $query = $this->getQuery(null);
        $query->where(['=', 'IJ1.IJ1_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

	public function getPdfJobInvoicesQuery($params)
	{
		$query = self::find()->alias('IJ1')
			->select([ 'JT1.JT1_ID', 'JT1.JT1_NUMBER', 'IJ1.IJ1_ID', 'IJ1.IJ1_NUMBER',
				'IJ1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('ij1.IJ1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
				'JT1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('JT1.JT1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
				'IJ1.IJ1_TOTAL_SELL', 'IJ1.IJ1_TOTAL_VALUE', 'IJ1.IJ1_COMMENT', 'co1.CO1_NAME',
				'LO1.LO1_NAME', 'LO1.LO1_ADDRESS1', 'LO1.LO1_ADDRESS2', 'LO1.LO1_CITY',
				'LO1.LO1_STATE', 'LO1.LO1_ZIP', 'LO1.LO1_PHONE', 'LO1.LO1_FAX',
				'VD1.VD1_NAME', 'VD1.VD1_ADDRESS1', 'VD1.VD1_ADDRESS2', 'VD1.VD1_CITY',
				'VD1.VD1_STATE', 'VD1.VD1_ZIP', 'VD1.VD1_PHONE', 'VD1.VD1_FAX',
				'cu1.CU1_NAME', 'cu1.CU1_ADDRESS1', 'cu1.CU1_ADDRESS2', 'cu1.CU1_CITY',
				'cu1.CU1_STATE', 'cu1.CU1_ZIP', 'cu1.CU1_PHONE', 'cu1.CU1_FAX',
				'cy1.CY1_SHORT_CODE'
			])
			->leftJoin("jt1_job_ticket as JT1" , "JT1.JT1_ID = IJ1.JT1_ID")
			->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = IJ1.LO1_ID")
			->leftJoin("cy1_country AS cy1" , "cy1.CY1_ID = lo1.CY1_ID")
			->leftJoin("vd1_vendor AS VD1" , "VD1.VD1_ID = IJ1.VD1_ID")
			->leftJoin("cy1_country AS cy1_vd1" , "cy1_vd1.CY1_ID = VD1.CY1_ID")
			->leftJoin("cu1_customer AS cu1" , "cu1.CU1_ID = JT1.CU1_ID")
			->leftJoin("cy1_country AS cy1_cu1" , "cy1_cu1.CY1_ID = cu1.CY1_ID")
			->leftJoin("co1_company AS co1" , "co1.CO1_ID = IJ1.CO1_ID");

		if ($params && !empty($params)) {
			$this->load($params, '');
		}

		if (isset($this->IJ1_IDs)) {
			$query->andFilterWhere(['IN', 'IJ1.IJ1_ID', explode(',', $this->IJ1_IDs)]);
		}

		return $query;
	}

	public function getPdfJobInvoices($params)
	{
		$query = $this->getPdfJobInvoicesQuery( $params );
		return $query->asArray()->all();
	}
}
