<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "nu1_number".
 *
 * @property int $NU1_ID
 * @property int $IN1_ID
 * @property int $IS1_ID
 * @property int $JT1_ID
 * @property int $PO1_ID
 * @property int $IJ1_ID
 * @property int $OR1_ID
 * @property int $JT1_NUMBER
 * @property int $OR1_NUMBER
 * @property int $PO1_NUMBER
 * @property string $NU1_LAST_SESSION
 */
class Number extends \yii\db\ActiveRecord
{
    protected $_tablePrefix = 'NU1';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nu1_number';
    }

    public static function primaryKey()
    {
        return ['NU1_ID'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IN1_ID', 'IS1_ID', 'JT1_ID', 'PO1_ID', 'IJ1_ID', 'OR1_ID', 'JT1_NUMBER', 'OR1_NUMBER'], 'required'],
            [['IN1_ID', 'IS1_ID', 'JT1_ID', 'PO1_ID', 'IJ1_ID', 'OR1_ID', 'JT1_NUMBER', 'OR1_NUMBER', 'PO1_NUMBER'], 'integer'],
            [['NU1_LAST_SESSION'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NU1_ID' => 'Nu1  ID',
            'IN1_ID' => 'In1  ID',
            'IS1_ID' => 'Is1  ID',
            'JT1_ID' => 'Jt1  ID',
            'PO1_ID' => 'Po1  ID',
            'IJ1_ID' => 'Ij1  ID',
            'OR1_ID' => 'Or1  ID',
            'JT1_NUMBER' => 'Jt1  Number',
            'OR1_NUMBER' => 'Or1  Number',
            'PO1_NUMBER' => 'Po1  Number',
            'NU1_LAST_SESSION' => 'Nu1  Last  Session',
        ];
    }
    
    public function getOrderNumber()
    {
        $query = self::find()->alias('NU1')
                    ->select(['NU1.NU1_ID', 'NU1.OR1_ID', 'NU1.OR1_NUMBER', 'NU1.NU1_LAST_SESSION']);

        return $query->one();
    }
}
