<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "ccc_repair_order".
 *
 * @property int $CCC_ID
 * @property int $LO1_ID
 * @property string $LO1_CCC_ID
 * @property int $JT1_ID
 * @property string $CCC_RO_NUMBER
 * @property int $CCC_STATUS
 * @property string $CCC_VIN
 * @property string $CCC_MAKE
 * @property string $CCC_MODEL
 * @property int $CCC_YEAR
 * @property int $CCC_DELETE_FLAG
 * @property string $CCC_MODIFIED_ON
 */
class RepairOrder extends ActiveRecord
{

    protected $_tablePrefix = 'CCCC';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ccc_repair_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LO1_ID', 'JT1_ID', 'CCC_STATUS', 'CCC_YEAR', 'CCC_DELETE_FLAG'], 'integer'],
            [['CCC_MODIFIED_ON'], 'safe'],
            [['LO1_CCC_ID', 'CCC_RO_NUMBER', 'CCC_MAKE', 'CCC_MODEL'], 'string', 'max' => 100],
            [['CCC_VIN'], 'string', 'max' => 17],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CCC_ID' => 'Ccc  ID',
            'LO1_ID' => 'Lo1  ID',
            'LO1_CCC_ID' => 'Lo1  Ccc  ID',
            'JT1_ID' => 'Jt1  ID',
            'CCC_RO_NUMBER' => 'Ccc  Ro  Number',
            'CCC_STATUS' => 'Ccc  Status',
            'CCC_VIN' => 'Ccc  Vin',
            'CCC_MAKE' => 'Ccc  Make',
            'CCC_MODEL' => 'Ccc  Model',
            'CCC_YEAR' => 'Ccc  Year',
            'CCC_DELETE_FLAG' => 'Ccc  Delete  Flag',
            'CCC_MODIFIED_ON' => 'Ccc  Modified  On',
        ];
    }

    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery( $params, $fromLocation )
    {
        $query = self::find()->alias('CCC')->select(['CCC.*']);

        $query->andFilterWhere(['=',    'CCC.CCC_DELETE_FLAG', 0]);
        $query->andWhere(['or', ['CCC.LO1_CCC_ID'=>$fromLocation->LO1_CCC_SHOP_ID], ['CCC.LO1_ID'=>$fromLocation->LO1_SHORT_CODE]]);

        return $query;
    }

    public function search($params, $fromLocation)
    {
        $query = $this->getSearchQuery( $params, $fromLocation );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }
}
