<?php

namespace app\models;

use app\models\scopes\MasterVendorQuery;
use Yii;
use yii\data\ActiveDataProvider;
use app\models\MasterCatalog;

/**
 * This is the model class for table "vd0_vendor".
 *
 * @property int $VD0_ID
 * @property int $CO1_ID
 * @property int $VD0_EDI_GL
 * @property string $VD0_SHORT_CODE
 * @property string $VD0_CUSTOMER_NO
 * @property string $VD0_NAME
 * @property string $VD0_ADDRESS1
 * @property string $VD0_ADDRESS2
 * @property string $VD0_CITY
 * @property string $VD0_STATE
 * @property string $VD0_ZIP
 * @property string $VD0_PHONE
 * @property string $VD0_FAX
 * @property string $VD0_EMAIL
 * @property string $VD0_URL
 * @property int $VD0_NO_INVENTORY
 * @property int $VD0_MINIMUM_ORDER
 * @property int $VD0_MANUFACTURER
 * @property int $VD0_SUPPLIER
 * @property int $CY1_ID
 * @property int $VD0_DELETE_FLAG
 * @property int $VD0_CREATED_BY
 * @property string $VD0_CREATED_ON
 * @property int $VD0_MODIFIED_BY
 * @property string $VD0_MODIFIED_ON
 * @property int $VD0_RECEIVE_EDI
 * @property int $VD0_SEND_EDI_PO
 * @property int $VD0_SEND_ACKNOWLEDGEMENT
 * @property int $VD0_PO_FORMAT
 * @property int $VD0_SEND_FTP
 * @property int $VD0_SEND_SFTP
 * @property int $VD0_POST_HTTP
 * @property int $VD0_RECEIVE_FTP
 * @property int $VD0_PICKUP_FTP
 * @property int $VD0_PICKUP_SFTP
 * @property int $VD0_RECEIVE_HTTP
 * @property string $VD0_REMOTE_FTP_SERVER
 * @property string $VD0_REMOTE_FTP_USERNAME
 * @property string $VD0_REMOTE_FTP_PASSWORD
 * @property string $VD0_REMOTE_FTP_DIRECTORY_SEND
 * @property string $VD0_REMOTE_FTP_DIRECTORY_PICKUP
 * @property string $VD0_FTP_USER
 * @property string $VD0_FTP_PASSWORD
 * @property string $VD0_FTP_DIRECTORY
 * @property string $VD0_REMOTE_HTTP_SERVER
 * @property string $VD0_SUPPLIER_CODE
 * @property string $VD0_RECEIVER_QUALIFIER
 * @property string $VD0_RECEIVER_ID
 * @property string $VD0_FACILITY
 * @property string $VD0_TRADING_PARTNER_QUALIFIER
 * @property string $VD0_TRADING_PARTNER_ID
 * @property string $VD0_TRADING_PARTNER_GS_ID
 * @property string $VD0_FLAG
 * @property string $VD0_X12_STANDARD
 * @property string $VD0_EDI_VERSION
 * @property string $VD0_DUNS
 * @property string $VD0_SHARED_SECRET
 * @property int $VD0_POST_AS2
 * @property int $VD0_RECEIVE_AS2
 * @property string $VD0_AS2_CERTIFICATE_FILENAME
 * @property string $VD0_AS2_RECEIVER_ID
 * @property string $VD0_AS2_TRADING_PARTNER_ID
 * @property string $VD0_CXML_PAYLOAD_ID
 * @property string $VD0_PICKUP_DIRECTORY
 * @property int $VD0_AS2_REQUEST_RECEIPT
 * @property int $VD0_AS2_SIGN_MESSAGES
 * @property string $VD0_AS2_KEY_LENGTH
 * @property int $VD0_DISCOUNT
 */
class MasterVendor extends ActiveRecord
{
    protected $_tablePrefix = 'VD0';

    public $MD0_ID;
    public $MD0_IDs;
    public $MASTER_DATA;
    public $CY1_NAME;
    public $CREATED_BY;
    public $MODIFIED_BY;
	public $CREATED_ON;
	public $MODIFIED_ON;


    /**
     * {@inheritdoc}
     * @return MasterVendorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MasterVendorQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vd0_vendor';
    }

    public static function primaryKey()
    {
        return ['VD0_ID'];
    }

    public function fields() {
        $fields = parent::fields();
        $fields["MD0_IDs"] = function($model){ return $model->MD0_IDs; };
        $fields["MASTER_DATA"] = function($model){ return $model->MASTER_DATA; };
        $fields["MD0_ID"] = function($model){ return $model->MD0_ID; };
        $fields["CY1_NAME"] = function($model){ return $model->CY1_NAME; };
        $fields["CY1_NAME"] = function($model){ return $model->CY1_NAME; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['VD0_ID', 'MD0_ID', 'CO1_ID', 'VD0_EDI_GL', 'VD0_NO_INVENTORY', 'VD0_MINIMUM_ORDER', 'VD0_MANUFACTURER', 'VD0_SUPPLIER', 'CY1_ID', 'VD0_DELETE_FLAG', 'VD0_CREATED_BY', 'VD0_MODIFIED_BY', 'VD0_RECEIVE_EDI', 'VD0_SEND_EDI_PO', 'VD0_SEND_ACKNOWLEDGEMENT', 'VD0_PO_FORMAT', 'VD0_SEND_FTP', 'VD0_SEND_SFTP', 'VD0_POST_HTTP', 'VD0_RECEIVE_FTP', 'VD0_PICKUP_FTP', 'VD0_PICKUP_SFTP', 'VD0_RECEIVE_HTTP', 'VD0_POST_AS2', 'VD0_RECEIVE_AS2', 'VD0_AS2_REQUEST_RECEIPT', 'VD0_AS2_SIGN_MESSAGES', 'VD0_DISCOUNT'], 'integer'],
            [['VD0_CREATED_ON', 'VD0_MODIFIED_ON'], 'safe'],
            [['VD0_SHORT_CODE', 'VD0_AS2_KEY_LENGTH'], 'string', 'max' => 10],
            [['VD0_CUSTOMER_NO', 'VD0_NAME', 'VD0_ADDRESS1', 'VD0_ADDRESS2', 'VD0_CITY', 'VD0_EMAIL', 'VD0_REMOTE_FTP_SERVER', 'VD0_REMOTE_FTP_USERNAME', 'VD0_REMOTE_FTP_PASSWORD', 'VD0_REMOTE_FTP_DIRECTORY_SEND', 'VD0_REMOTE_FTP_DIRECTORY_PICKUP', 'VD0_FTP_USER', 'VD0_FTP_PASSWORD', 'VD0_FTP_DIRECTORY', 'VD0_REMOTE_HTTP_SERVER'], 'string', 'max' => 200],
            [['VD0_STATE'], 'string', 'max' => 20],
            [['VD0_ZIP'], 'string', 'max' => 11],
            [['VD0_PHONE', 'VD0_FAX', 'VD0_URL', 'VD0_DUNS'], 'string', 'max' => 50],
            [['VD0_SUPPLIER_CODE', 'VD0_RECEIVER_ID', 'VD0_FACILITY', 'VD0_TRADING_PARTNER_ID', 'VD0_TRADING_PARTNER_GS_ID'], 'string', 'max' => 45],
            [['VD0_RECEIVER_QUALIFIER', 'VD0_TRADING_PARTNER_QUALIFIER'], 'string', 'max' => 2],
            [['VD0_FLAG'], 'string', 'max' => 1],
            [['VD0_X12_STANDARD'], 'string', 'max' => 4],
            [['VD0_EDI_VERSION'], 'string', 'max' => 5],
            [['VD0_SHARED_SECRET'], 'string', 'max' => 100],
            [['VD0_AS2_CERTIFICATE_FILENAME', 'VD0_AS2_RECEIVER_ID', 'VD0_AS2_TRADING_PARTNER_ID', 'VD0_CXML_PAYLOAD_ID', 'VD0_PICKUP_DIRECTORY', 'CY1_NAME', 'CREATED_BY', 'MODIFIED_BY', 'CREATED_ON', 'MODIFIED_ON'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'VD0_ID' => 'Vd0  ID',
            'CO1_ID' => 'Co1  ID',
            'VD0_EDI_GL' => 'Vd0  Edi  Gl',
            'VD0_SHORT_CODE' => 'Vd0  Short  Code',
            'VD0_CUSTOMER_NO' => 'Vd0  Customer  No',
            'VD0_NAME' => 'Vd0  Name',
            'VD0_ADDRESS1' => 'Vd0  Address1',
            'VD0_ADDRESS2' => 'Vd0  Address2',
            'VD0_CITY' => 'Vd0  City',
            'VD0_STATE' => 'Vd0  State',
            'VD0_ZIP' => 'Vd0  Zip',
            'VD0_PHONE' => 'Vd0  Phone',
            'VD0_FAX' => 'Vd0  Fax',
            'VD0_EMAIL' => 'Vd0  Email',
            'VD0_URL' => 'Vd0  Url',
            'VD0_NO_INVENTORY' => 'Vd0  No  Inventory',
            'VD0_MINIMUM_ORDER' => 'Vd0  Minimum  Order',
            'VD0_MANUFACTURER' => 'Vd0  Manufacturer',
            'VD0_SUPPLIER' => 'Vd0  Supplier',
            'CY1_ID' => 'Cy1  ID',
            'VD0_DELETE_FLAG' => 'Vd0  Delete  Flag',
            'VD0_CREATED_BY' => 'Vd0  Created  By',
            'VD0_CREATED_ON' => 'Vd0  Created  On',
            'VD0_MODIFIED_BY' => 'Vd0  Modified  By',
            'VD0_MODIFIED_ON' => 'Vd0  Modified  On',
            'VD0_RECEIVE_EDI' => 'Vd0  Receive  Edi',
            'VD0_SEND_EDI_PO' => 'Vd0  Send  Edi  Po',
            'VD0_SEND_ACKNOWLEDGEMENT' => 'Vd0  Send  Acknowledgement',
            'VD0_PO_FORMAT' => 'Vd0  Po  Format',
            'VD0_SEND_FTP' => 'Vd0  Send  Ftp',
            'VD0_SEND_SFTP' => 'Vd0  Send  Sftp',
            'VD0_POST_HTTP' => 'Vd0  Post  Http',
            'VD0_RECEIVE_FTP' => 'Vd0  Receive  Ftp',
            'VD0_PICKUP_FTP' => 'Vd0  Pickup  Ftp',
            'VD0_PICKUP_SFTP' => 'Vd0  Pickup  Sftp',
            'VD0_RECEIVE_HTTP' => 'Vd0  Receive  Http',
            'VD0_REMOTE_FTP_SERVER' => 'Vd0  Remote  Ftp  Server',
            'VD0_REMOTE_FTP_USERNAME' => 'Vd0  Remote  Ftp  Username',
            'VD0_REMOTE_FTP_PASSWORD' => 'Vd0  Remote  Ftp  Password',
            'VD0_REMOTE_FTP_DIRECTORY_SEND' => 'Vd0  Remote  Ftp  Directory  Send',
            'VD0_REMOTE_FTP_DIRECTORY_PICKUP' => 'Vd0  Remote  Ftp  Directory  Pickup',
            'VD0_FTP_USER' => 'Vd0  Ftp  User',
            'VD0_FTP_PASSWORD' => 'Vd0  Ftp  Password',
            'VD0_FTP_DIRECTORY' => 'Vd0  Ftp  Directory',
            'VD0_REMOTE_HTTP_SERVER' => 'Vd0  Remote  Http  Server',
            'VD0_SUPPLIER_CODE' => 'Vd0  Supplier  Code',
            'VD0_RECEIVER_QUALIFIER' => 'Vd0  Receiver  Qualifier',
            'VD0_RECEIVER_ID' => 'Vd0  Receiver  ID',
            'VD0_FACILITY' => 'Vd0  Facility',
            'VD0_TRADING_PARTNER_QUALIFIER' => 'Vd0  Trading  Partner  Qualifier',
            'VD0_TRADING_PARTNER_ID' => 'Vd0  Trading  Partner  ID',
            'VD0_TRADING_PARTNER_GS_ID' => 'Vd0  Trading  Partner  Gs  ID',
            'VD0_FLAG' => 'Vd0  Flag',
            'VD0_X12_STANDARD' => 'Vd0  X12  Standard',
            'VD0_EDI_VERSION' => 'Vd0  Edi  Version',
            'VD0_DUNS' => 'Vd0  Duns',
            'VD0_SHARED_SECRET' => 'Vd0  Shared  Secret',
            'VD0_POST_AS2' => 'Vd0  Post  As2',
            'VD0_RECEIVE_AS2' => 'Vd0  Receive  As2',
            'VD0_AS2_CERTIFICATE_FILENAME' => 'Vd0  As2  Certificate  Filename',
            'VD0_AS2_RECEIVER_ID' => 'Vd0  As2  Receiver  ID',
            'VD0_AS2_TRADING_PARTNER_ID' => 'Vd0  As2  Trading  Partner  ID',
            'VD0_CXML_PAYLOAD_ID' => 'Vd0  Cxml  Payload  ID',
            'VD0_PICKUP_DIRECTORY' => 'Vd0  Pickup  Directory',
            'VD0_AS2_REQUEST_RECEIPT' => 'Vd0  As2  Request  Receipt',
            'VD0_AS2_SIGN_MESSAGES' => 'Vd0  As2  Sign  Messages',
            'VD0_AS2_KEY_LENGTH' => 'Vd0  As2  Key  Length',
            'VD0_DISCOUNT' => 'Vd0  Discount',
        ];
    }

    public function getQuery($params)
    {
        $query = MasterVendor::find()->alias('VD0');
	    $query->initScope();
	    $query->select(['VD0.VD0_ID', 'VD0.VD0_SHORT_CODE', 'VD0.VD0_NAME', 'VD0.VD0_MANUFACTURER', 'VD0.VD0_ADDRESS1', 'VD0.VD0_ADDRESS2', 'VD0.VD0_EMAIL',
                        'VD0.VD0_CITY', 'VD0.VD0_STATE', 'VD0.VD0_ZIP', 'VD0.VD0_PHONE', 'VD0.VD0_FAX', 'VD0.VD0_URL', 'VD0.CY1_ID', 'VD0.VD0_DELETE_FLAG',
                        'CY1.CY1_SHORT_CODE', 'CY1.CY1_NAME',
	                    'MODIFIED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('VD0_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
	                    'CREATED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('VD0_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
                        'CREATED_BY'  => 'us1_create.US1_NAME',
                        'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("cy1_country as CY1" , "CY1.CY1_ID = VD0.CY1_ID AND CY1.CY1_DELETE_FLAG = 0")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = VD0.VD0_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = VD0.VD0_MODIFIED_BY");
        $query->andFilterWhere(['=', 'VD0.VD0_DELETE_FLAG', 0]);
        $query->orderBy(['VD0_NAME' => SORT_ASC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->VD0_MANUFACTURER) && $this->VD0_MANUFACTURER != 0) {
            $query->andFilterWhere(['=', 'VD0.VD0_MANUFACTURER', $this->VD0_MANUFACTURER]);
        }

        return $query;
    }

    public function getAll($params)
    {
        $query = $this->getQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'  => $params['perPage'],
                'page'      => $params['page']
            ]
        ]);

        return $dataProvider;
    }

    public function getMasterVendor($id)
    {
        $query = $this->getQuery(null);
        $query->addSelect('VD0.*');
        $query->andFilterWhere(['=', 'VD0.VD0_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function getPartsQuery($params)
    {
        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        $query = MasterCatalog::find()->alias('MD0')
                     ->select([  'MD0.MD0_ID', 'MD0.MD0_PART_NUMBER', 'MD0.MD0_DESC1', 'MD0.MD0_UPC1',
                                 'VD0.VD0_ID', 'VD0.VD0_SHORT_CODE', 'VD0.VD0_NAME', 'u.UM1_NAME'])
                     ->leftJoin("vd0_vendor as VD0" , "MD0.VD0_ID = VD0.VD0_ID and MD0.MD0_DELETE_FLAG = 0")
                     ->leftJoin("um1_unit as u" , "u.UM1_ID = MD0.UM1_DEFAULT_PRICING and u.UM1_DELETE_FLAG = 0");
        $query->where(['=', 'VD0.VD0_DELETE_FLAG', 0]);
        $query->andWhere(['=', 'MD0.MD0_DELETE_FLAG', 0]);
        $query->andWhere(['=', 'VD0.VD0_MANUFACTURER', 1]);
        $query->andWhere(['not', ['MD0.MD0_ID' => null]]);

        if(isset($this->VD0_ID)) {
            $query->andWhere(['=', 'VD0.VD0_ID', $this->VD0_ID]);
        }

        if(isset($this->MD0_IDs)) {
            $query->andWhere(['in', 'MD0.MD0_ID', explode(',', $this->MD0_IDs)]);
        }

        return $query;
    }

    public function getAllParts($params)
    {
        $query = $this->getPartsQuery($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }
}
