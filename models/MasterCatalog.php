<?php

namespace app\models;

use app\models\scopes\MasterCatalogQuery;
use Yii;
use yii\data\ActiveDataProvider;
use app\models\MasterVendor;

/**
 * This is the model class for table "md0_master_data".
 *
 * @property int $MD0_ID
 * @property int $CO1_ID
 * @property int $FI1_ID
 * @property int $CA0_ID
 * @property int $CA1_ID
 * @property int $CA2_ID
 * @property int $CA3_ID
 * @property int $CA4_ID
 * @property int $GL1_ID
 * @property string $MD0_PART_NUMBER
 * @property string $MD0_UPC1
 * @property string $MD0_UPC2
 * @property string $MD0_UPC3
 * @property string $MD0_DESC1
 * @property string $MD0_DESC2
 * @property string $MD0_UNIT_PRICE
 * @property int $UM1_PRICING_ID
 * @property int $VD0_ID
 * @property int $MD0_DELETE_FLAG
 * @property int $MD0_CREATED_BY
 * @property string $MD0_CREATED_ON
 * @property int $MD0_MODIFIED_BY
 * @property string $MD0_MODIFIED_ON
 * @property string $MD0_IMAGE
 * @property string $CA0_NAME
 * @property string $VD0_NAME
 * @property string $MD0_UNIT
 * @property int $MD0_PURCHASE_UNIT_SIZE
 * @property int $MD0_CENTRAL_ORDER_FLAG
 * @property string $MD0_VENDOR_PART_NUMBER
 * @property string $MD0_OEM_NUMBER
 * @property double $MD0_MARKUP
 * @property int $MD0_VOC_FLAG
 * @property string $MD0_VOC_MFG
 * @property string $MD0_VOC_CATA
 * @property string $MD0_VOC_VALUE
 * @property int $MD0_VOC_UNIT
 * @property int $MD0_FAVORITE
 * @property int $MD0_TYPE
 * @property int $MD0_BODY
 * @property int $MD0_PAINT
 * @property int $MD0_DETAIL
 * @property int $UM1_DEFAULT_PRICING
 * @property int $UM1_DEFAULT_PURCHASE
 * @property int $UM1_DEFAULT_RECEIVE
 * @property double $UM1_DEFAULT_PURCHASE_FACTOR
 * @property double $UM1_DEFAULT_RECEIVE_FACTOR
 * @property int $MD0_MANUFACTURER
 * @property int $MD0_PARENT_ID
 * @property string $MD0_URL
 * @property string $MD0_SAFETY_URL
 * @property int $C00_ID
 * @property int $MD0_DISCONTINUED
 */
class MasterCatalog extends ActiveRecord
{
    protected $_tablePrefix = 'MD0';

    public $MD0_IDs;
    public $MASTER_DATA;
    public $MD1_ID;
    public $MD1_DESC1;
    public $MD1_BODY;
    public $MD1_DETAIL;
    public $MD1_PAINT;
    public $MD1_UNIT_PRICE;
    public $MD1_MARKUP;
    public $FI1_FILE_PATH;
    public $UM1_PRICE_NAME;
    public $UM1_ID;
    public $C00_NAME;
	public $CA0_NAME;
	public $MD0_PART_NUMBER1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'md0_master_data';
    }

    public static function primaryKey()
    {
        return ['MD0_ID'];
    }

    /**
     * {@inheritdoc}
     * @return MasterCatalogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MasterCatalogQuery(get_called_class());
    }

    public function fields() {
        $fields = parent::fields();
        $fields["MD0_IDs"] = function($model){ return $model->MD0_IDs; };
        $fields["MASTER_DATA"] = function($model){ return $model->MASTER_DATA; };
        $fields["MD1_ID"] = function($model){ return $model->MD1_ID; };
        $fields["MD1_DESC1"] = function($model){ return $model->MD1_DESC1; };
        $fields["MD1_BODY"] = function($model){ return $model->MD1_BODY; };
        $fields["MD1_DETAIL"] = function($model){ return $model->MD1_DETAIL; };
        $fields["MD1_PAINT"] = function($model){ return $model->MD1_PAINT; };
        $fields["MD1_UNIT_PRICE"] = function($model){ return $model->MD1_UNIT_PRICE; };
        $fields["UM1_ID"] = function($model){ return $model->UM1_ID; };
        $fields["MD1_MARKUP"] = function($model){ return $model->MD1_MARKUP; };
        $fields["FI1_FILE_PATH"] = function($model){ return $model->FI1_FILE_PATH; };
        $fields["UM1_PRICE_NAME"] = function($model){ return $model->UM1_PRICE_NAME; };
        $fields["C00_NAME"] = function($model){ return $model->C00_NAME; };
	    $fields["CA0_NAME"] = function($model){ return $model->CA0_NAME; };
	    $fields["MD0_PART_NUMBER1"] = function($model){ return $model->MD0_PART_NUMBER1; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CO1_ID', 'FI1_ID', 'CA0_ID', 'CA1_ID', 'CA2_ID', 'CA3_ID', 'CA4_ID', 'GL1_ID', 'UM1_PRICING_ID', 'VD0_ID', 'MD0_DELETE_FLAG', 'MD0_CREATED_BY', 'MD0_MODIFIED_BY', 'MD0_PURCHASE_UNIT_SIZE', 'MD0_CENTRAL_ORDER_FLAG', 'MD0_VOC_FLAG', 'MD0_VOC_UNIT', 'MD0_FAVORITE', 'MD0_TYPE', 'MD0_BODY', 'MD0_PAINT', 'MD0_DETAIL', 'UM1_ID', 'UM1_DEFAULT_PRICING', 'UM1_DEFAULT_PURCHASE', 'UM1_DEFAULT_RECEIVE', 'MD0_MANUFACTURER', 'MD0_PARENT_ID', 'C00_ID', 'MD0_DISCONTINUED', 'MASTER_DATA', 'MD1_ID', 'MD1_BODY', 'MD1_DETAIL', 'MD1_PAINT'], 'integer'],
            [['MD0_UNIT_PRICE', 'MD0_MARKUP', 'MD0_VOC_VALUE', 'UM1_DEFAULT_PURCHASE_FACTOR', 'UM1_DEFAULT_RECEIVE_FACTOR', 'MD1_UNIT_PRICE', 'MD1_MARKUP'], 'number'],
            [['MD0_CREATED_ON', 'MD0_MODIFIED_ON'], 'safe'],
            [['MD0_URL', 'MD0_SAFETY_URL', 'MD0_IDs', 'C00_NAME'], 'string'],
            [['MD0_PART_NUMBER', 'MD0_PART_NUMBER1', 'CA0_NAME', 'VD0_NAME', 'MD0_VENDOR_PART_NUMBER'], 'string', 'max' => 100],
            [['MD0_UPC1', 'MD0_UPC2', 'MD0_UPC3'], 'string', 'max' => 30],
            [['MD0_DESC1', 'MD0_DESC2', 'MD1_DESC1'], 'string', 'max' => 200],
            [['MD0_IMAGE', 'MD0_OEM_NUMBER', 'FI1_FILE_PATH', 'UM1_PRICE_NAME'], 'string', 'max' => 255],
            [['MD0_UNIT'], 'string', 'max' => 50],
            [['MD0_VOC_MFG', 'MD0_VOC_CATA'], 'string', 'max' => 45]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MD0_ID' => 'Md0  ID',
            'CO1_ID' => 'Co1  ID',
            'FI1_ID' => 'Fi1  ID',
	        'CA0_ID' => 'Ca0  ID',
            'CA1_ID' => 'Ca1  ID',
            'CA2_ID' => 'Ca2  ID',
            'CA3_ID' => 'Ca3  ID',
            'CA4_ID' => 'Ca4  ID',
            'GL1_ID' => 'Gl1  ID',
            'MD0_PART_NUMBER' => 'Md0  Part  Number',
            'MD0_UPC1' => 'Md0  Upc1',
            'MD0_UPC2' => 'Md0  Upc2',
            'MD0_UPC3' => 'Md0  Upc3',
            'MD0_DESC1' => 'Md0  Desc1',
            'MD0_DESC2' => 'Md0  Desc2',
            'MD0_UNIT_PRICE' => 'Md0  Unit  Price',
            'UM1_PRICING_ID' => 'Um1  Pricing  ID',
            'VD0_ID' => 'Vd0  ID',
            'MD0_DELETE_FLAG' => 'Md0  Delete  Flag',
            'MD0_CREATED_BY' => 'Md0  Created  By',
            'MD0_CREATED_ON' => 'Md0  Created  On',
            'MD0_MODIFIED_BY' => 'Md0  Modified  By',
            'MD0_MODIFIED_ON' => 'Md0  Modified  On',
            'MD0_IMAGE' => 'Md0  Image',
	        'CA0_NAME' => 'Ca0  Name',
            'VD0_NAME' => 'Vd0  Name',
            'MD0_UNIT' => 'Md0  Unit',
            'MD0_PURCHASE_UNIT_SIZE' => 'Md0  Purchase  Unit  Size',
            'MD0_CENTRAL_ORDER_FLAG' => 'Md0  Central  Order  Flag',
            'MD0_VENDOR_PART_NUMBER' => 'Md0  Vendor  Part  Number',
            'MD0_OEM_NUMBER' => 'Md0  Oem  Number',
            'MD0_MARKUP' => 'Md0  Markup',
            'MD0_VOC_FLAG' => 'Md0  Voc  Flag',
            'MD0_VOC_MFG' => 'Md0  Voc  Mfg',
            'MD0_VOC_CATA' => 'Md0  Voc  Cata',
            'MD0_VOC_VALUE' => 'Md0  Voc  Value',
            'MD0_VOC_UNIT' => 'Md0  Voc  Unit',
            'MD0_FAVORITE' => 'Md0  Favorite',
            'MD0_TYPE' => 'Md0  Type',
            'MD0_BODY' => 'Md0  Body',
            'MD0_PAINT' => 'Md0  Paint',
            'MD0_DETAIL' => 'Md0  Detail',
            'UM1_DEFAULT_PRICING' => 'Um1  Default  Pricing',
            'UM1_DEFAULT_PURCHASE' => 'Um1  Default  Purchase',
            'UM1_DEFAULT_RECEIVE' => 'Um1  Default  Receive',
            'UM1_DEFAULT_PURCHASE_FACTOR' => 'Um1  Default  Purchase  Factor',
            'UM1_DEFAULT_RECEIVE_FACTOR' => 'Um1  Default  Receive  Factor',
            'MD0_MANUFACTURER' => 'Md0  Manufacturer',
            'MD0_PARENT_ID' => 'Md0  Parent  ID',
            'MD0_URL' => 'Md0  Url',
            'MD0_SAFETY_URL' => 'Md0  Safety  Url',
            'C00_ID' => 'C00  ID',
            'MD0_DISCONTINUED' => 'Md0  Discontinued',
        ];
    }

    public function getQuery($params)
    {
        $query = MasterCatalog::find()->alias('MD0')
	            ->select([  'DISTINCT `MD0`.`MD0_ID`', 'MD0.MD0_IMAGE', 'MD0.FI1_ID', 'MD0.MD0_URL', 'MD0.MD0_SAFETY_URL', 'MD0.MD0_PARENT_ID', 'VD0.VD0_ID', 'VD0.VD0_MANUFACTURER',
                            'VD0.VD0_SHORT_CODE', 'VD0.VD0_NAME', 'MD0.MD0_FAVORITE', 'MD0.MD0_PART_NUMBER', 'MD0.MD0_DESC1', 'MD0.MD0_UNIT_PRICE',
                            'MD0.MD0_UNIT_PRICE*(1+COALESCE(MD0.MD0_MARKUP,0)*0.01) AS MD0_SELL_PRICE', 'MD0.MD0_MARKUP', 'MD0.C00_ID', 'MD0.CA0_ID',
                            'MD0.MD0_UPC1', 'MD0.MD0_BODY', 'MD0.MD0_DETAIL', 'MD0.MD0_PAINT', 'MD0.UM1_DEFAULT_PRICING', 'MD0.UM1_DEFAULT_PURCHASE', 'MD0.UM1_DEFAULT_RECEIVE',
	                        'MD0.UM1_DEFAULT_PURCHASE_FACTOR', 'MD0.UM1_DEFAULT_RECEIVE_FACTOR',
	                        'um1_price.UM1_ID AS UM1_ID',  'um1_price.UM1_NAME AS UM1_PRICE_NAME', 'um1_price.UM1_SIZE AS UM1_PRICE_SIZE',
	                        'um1_price.UM1_ID AS UM1_PRICE_ID', 'c00.C00_NAME', 'CA0.CA0_NAME', 'fi1.FI1_FILE_PATH',
			                'MD0_MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('MD0_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
			                'MD0_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('MD0_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
                            'CA1.CA1_NAME', 'MD0.CA1_ID',
                            'MD0_VOC_FLAG', 'MD0_VOC_MFG', 'MD0_VOC_CATA', 'MD0_VOC_VALUE', 'MD0_VOC_UNIT',
                            'CREATED_BY'  => 'us1_create.US1_NAME',
                            'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                ->leftJoin("fi1_file as fi1" , "fi1.FI1_ID = MD0.FI1_ID AND fi1.FI1_DELETE_FLAG = 0")
                ->leftJoin("c00_category as c00" , "c00.C00_ID = MD0.C00_ID AND c00.C00_DELETE_FLAG = 0")
                ->leftJoin("ca1_category AS CA1", "CA1.CA1_ID = MD0.CA1_ID AND CA1.CA1_DELETE_FLAG=0")
                ->leftJoin("vd0_vendor as VD0" , "VD0.VD0_ID = MD0.VD0_ID AND VD0.VD0_DELETE_FLAG = 0")
			    ->leftJoin("vd1_vendor as VD1" , "VD1.VD0_ID = VD0.VD0_ID AND VD1.VD1_DELETE_FLAG = 0")
			    ->leftJoin("ca0_category as CA0" , "CA0.CA0_ID = MD0.CA0_ID AND CA0.CA0_DELETE_FLAG = 0")
	            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = MD0.MD0_CREATED_BY")
                ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = MD0.MD0_MODIFIED_BY")
                ->leftJoin("um1_unit as um1_price" , "um1_price.UM1_ID = MD0.UM1_DEFAULT_PRICING AND um1_price.UM1_DELETE_FLAG=0");
        $query->andFilterWhere(['=', 'MD0.MD0_DELETE_FLAG', 0]);
	    $user = Yii::$app->user->getIdentity();
	    $query->andFilterWhere(['=', 'VD1.CO1_ID', $user->CO1_ID]);
	    $query->andFilterWhere(['is not', 'VD1.VD1_ID', new \yii\db\Expression('null')]);

	    $this->load($params, '');

	    if(isset($this->FI1_FILE_PATH)) {
		    if ($this->FI1_FILE_PATH == 1) {
			    $query->andFilterWhere(['is not', 'fi1.FI1_FILE_PATH', new \yii\db\Expression('null')]);
		    } else {
			    $query->andFilterWhere(['is', 'fi1.FI1_FILE_PATH', new \yii\db\Expression('null')]);
		    }
	    }

	    if (isset($this->MD0_URL)) {
		    $query->andFilterWhere(['like', 'MD0.MD0_URL', $this->MD0_URL]);
	    }

	    if(isset($this->MD0_SAFETY_URL)) {
		    if ($this->MD0_SAFETY_URL == 1) {
			    $query->andFilterWhere(['>', 'LENGTH(MD0.MD0_SAFETY_URL)', 0]);
		    } else {
			    $query->andFilterWhere(['or',
				    ['is', 'MD0.MD0_SAFETY_URL', new \yii\db\Expression('null')],
				    ['=', 'LENGTH(MD0.MD0_SAFETY_URL)', 0]
			    ]);
		    }
	    }

	    if(isset($this->VD0_NAME)) {
		    $query->andFilterWhere(['=', 'VD0.VD0_ID', $this->VD0_NAME]);
	    }

		if (isset($this->MD0_PART_NUMBER)) {
			$query->andFilterWhere(['like', 'MD0.MD0_PART_NUMBER', $this->MD0_PART_NUMBER]);
		}

		if (isset($this->MD0_DESC1)) {
			$query->andFilterWhere(['like', 'MD0.MD0_DESC1', $this->MD0_DESC1]);
		}

		if (isset($this->MD0_UNIT_PRICE)) {
			$query->andFilterWhere(['like', 'MD0.MD0_UNIT_PRICE', $this->MD0_UNIT_PRICE]);
		}

		if (isset($this->UM1_PRICE_NAME)) {
			$query->andFilterWhere(['=', 'um1_price.UM1_ID', $this->UM1_PRICE_NAME]);
		}

	    if (isset($this->CA0_NAME)) {
		    $query->andFilterWhere(['like', 'CA0.CA0_NAME', $this->CA0_NAME]);
	    }

        if (isset($this->C00_NAME)) {
			$query->andFilterWhere(['like', 'c00.C00_NAME', $this->C00_NAME]);
		}

        if (isset($this->MD0_UPC1)) {
			$query->andFilterWhere(['like', 'MD0.MD0_UPC1', $this->MD0_UPC1]);
		}

	    if(isset($this->MD0_BODY)) {
	    	 if ($this->MD0_BODY == 1) {
		        $query->andFilterWhere(['=', 'MD0.MD0_BODY', 1]);
		     } else {
			     $query->andFilterWhere(['!=', 'MD0.MD0_BODY', 1]);
		    }
	    }

	    if(isset($this->MD0_DETAIL)) {
		    if ($this->MD0_DETAIL == 1) {
			    $query->andFilterWhere(['=', 'MD0.MD0_DETAIL', 1]);
		    } else {
			    $query->andFilterWhere(['!=', 'MD0.MD0_DETAIL', 1]);
		    }
	    }

	    if(isset($this->MD0_PAINT)) {
		    if ($this->MD0_PAINT == 1) {
			    $query->andFilterWhere(['=', 'MD0.MD0_PAINT', 1]);
		    } else {
			    $query->andFilterWhere(['!=', 'MD0.MD0_PAINT', 1]);
		    }
	    }

	    if(isset($params['MD0_IDs'])) {
		    $query->andWhere(['in', 'MD0.MD0_ID', $params['MD0_IDs']]);
	    }

	    $query->initScope();
	    $query->distinct();

        return $query;
    }

    public function getAll($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
//            $defaultOrder['MD0.MD0_PART_NUMBER'] = SORT_ASC;
        }

        $query = $this->getQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => $params['perPage'],
                'page'     => $params['page']
            ],
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

	public function getSearchQuery($params)
	{
		$query = self::find()->alias('MD0')
			->select([  'MD0.MD0_ID', 'MD0.MD0_PART_NUMBER', 'MD0.MD0_DESC1', 'MD0.MD0_UPC1', 'MD0.MD0_VENDOR_PART_NUMBER', 'MD0.MD0_DISCONTINUED',
				'VD0.VD0_ID', 'VD0.VD0_SHORT_CODE', 'VD0.VD0_NAME', 'UM1.UM1_NAME' ])
			->leftJoin("vd0_vendor as VD0" , "MD0.VD0_ID = VD0.VD0_ID AND VD0.VD0_DELETE_FLAG = 0")
			->leftJoin("vd1_vendor as VD1" , "VD1.VD0_ID = VD0.VD0_ID AND VD1.VD1_DELETE_FLAG = 0")
			->leftJoin("um1_unit as UM1" , "UM1.UM1_ID = MD0.UM1_DEFAULT_PRICING AND UM1.UM1_DELETE_FLAG=0");
		$query->andFilterWhere(['=', 'VD0.VD0_DELETE_FLAG', 0]);
		$query->andFilterWhere(['=', 'MD0.MD0_DELETE_FLAG', 0]);
		$query->andFilterWhere(['not', ['MD0.MD0_ID' => null]]);
		$query->andFilterWhere(['not', ['VD1.VD1_ID' => null]]);
//        $query->andFilterWhere(['=', 'VD0.VD0_MANUFACTURER', 1]);

		$user = Yii::$app->user->getIdentity();

		if(isset($user->CO1_ID) && $user->CO1_ID) {
			$query->andFilterWhere(['=', 'VD1.CO1_ID', $user->CO1_ID]);
		}

		if(isset($params['MD0_PART_NUMBER'])) {
			$query->andFilterWhere(['like', 'MD0.MD0_PART_NUMBER', $params['MD0_PART_NUMBER']]);
		}

		if(isset($params['MD0_PART_NUMBER1'])) {
			$query->andFilterWhere(['=', 'MD0.MD0_PART_NUMBER', $params['MD0_PART_NUMBER1']]);
		}
		$query->groupBy('MD0.MD0_ID');

		return $query;
	}

    public function getSearch($params)
    {
        $defaultOrder = [];
        if (!empty($params['sort'])) {
            foreach ($params['sort'] as $sort) {
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        } else {
//            $defaultOrder['MD0.MD0_PART_NUMBER'] = SORT_ASC;
        }

	    $query = $this->getSearchQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
	        'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

	public function getSearchNewParts($params)
	{
		$query = self::find()->alias('MD0')
			->select([
			    'DISTINCT `MD0`.`MD0_ID`',
                'MD0.MD0_PART_NUMBER',
                'MD0.MD0_DESC1',
                'MD0.MD0_UPC1',
                'MD0.MD0_VENDOR_PART_NUMBER',
                'MD0.MD0_DISCONTINUED',
				'MD0.MD0_UNIT_PRICE',
				'MD0.FI1_ID',
				'fi1.FI1_FILE_PATH',
				'VD0.VD0_ID',
                'VD0.VD0_SHORT_CODE',
                'VD0.VD0_NAME',
                'UM1.UM1_NAME' ])
			->leftJoin("vd0_vendor as VD0" , "MD0.VD0_ID = VD0.VD0_ID AND VD0.VD0_DELETE_FLAG = 0")
			->leftJoin("vd1_vendor as VD1" , "VD1.VD0_ID = VD0.VD0_ID AND VD1.VD1_DELETE_FLAG = 0")
			->leftJoin("um1_unit as UM1" , "UM1.UM1_ID = MD0.UM1_DEFAULT_PRICING AND UM1.UM1_DELETE_FLAG=0")
            ->leftJoin("fi1_file as fi1" , "fi1.FI1_ID = MD0.FI1_ID AND fi1.FI1_DELETE_FLAG = 0");

		$user = Yii::$app->user->getIdentity();
		$query->andFilterWhere(['=', 'VD1.CO1_ID', $user->CO1_ID]);
		$query->andFilterWhere(['is not', 'VD1.VD1_ID', new \yii\db\Expression('null')]);

		$query->andFilterWhere(['=', 'VD0.VD0_DELETE_FLAG', 0]);
		$query->andFilterWhere(['=', 'MD0.MD0_DELETE_FLAG', 0]);
		$query->andFilterWhere(['not', ['MD0.MD0_ID' => null]]);

		if(isset($params['MD0_PART_NUMBER'])) {
            $query->andFilterWhere ( [ 'OR' ,
                [ 'like' , 'MD0.MD0_PART_NUMBER' , $params['MD0_PART_NUMBER']],
                [ 'like' , 'MD0.MD0_DESC1' , $params['MD0_DESC1']]
            ] );
		}

		$masterDataQuery = MasterData::find()->alias('md1')->select(['MD0_ID']);
		$masterDataQuery->leftJoin("md2_location as md2_to" , "md2_to.MD1_ID=md1.MD1_ID AND md2_to.MD2_DELETE_FLAG=0");
		$masterDataQuery->andFilterWhere(['=', 'md2_to.LO1_ID', Yii::$app->user->getIdentity()->LO1_ID]);
        $masterDataQuery->leftJoin("vd1_vendor as vd1" , "vd1.VD1_ID = MD1.VD1_ID AND vd1.VD1_DELETE_FLAG = 0");
		$masterDataQuery->andFilterWhere(['=', 'md2_to.CO1_ID', Yii::$app->user->getIdentity()->CO1_ID]);
		$masterDataQuery->andFilterWhere(['is not', 'md1.MD0_ID', new \yii\db\Expression('null')]);

        $masterDataQuery->initScope();

		$query->andWhere('MD0.MD0_ID not in ( '.$masterDataQuery->createCommand()->getRawSql().' )');
		$query->distinct();

		return $query;
	}

    public function getMasterCatalog($id)
    {
        $query = $this->getQuery(null);
	    $ids = explode(',', $id);

	    if (sizeof($ids) > 1) {
		    $query->andFilterWhere(['IN', 'MD0.MD0_ID', $ids]);
	    } else {
		    $query->andFilterWhere(['=', 'MD0.MD0_ID', $id]);
	    }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function getPartsQuery($params)
    {
        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->MASTER_DATA) && $this->MASTER_DATA == 1) {
             $query = MasterCatalog::find()->alias('MD0')
                            ->select([  'MD0.MD0_ID', 'MD1.MD0_ID AS MD1_ID', 'MD0.UM1_DEFAULT_PRICING', 'MD1.UM1_DEFAULT_PRICING AS UM1_PRICING_ID',
                                        'VD1.VD0_ID AS CO1_ID', 'VD1.VD0_SHORT_CODE AS CO1_SHORT_CODE', 'VD1.VD0_NAME AS CO1_NAME',
                                        'MD0.MD0_PART_NUMBER', 'MD1.MD0_PART_NUMBER AS MD1_PART_NUMBER', 'MD0.MD0_BODY', 'MD1.MD0_BODY AS MD1_BODY',
                                        'MD0.MD0_DETAIL', 'MD1.MD0_DETAIL AS MD1_DETAIL', 'MD0.MD0_PAINT', 'MD1.MD0_PAINT AS MD1_PAINT',
                                        'MD0.MD0_BODY', 'MD1.MD0_BODY', 'MD0.MD0_DETAIL', 'MD1.MD0_DETAIL', 'MD0.MD0_PAINT', 'MD1.MD0_PAINT',
                                        'MD0.MD0_DESC1', 'MD1.MD0_DESC1 AS MD1_DESC1', 'u0.UM1_NAME as UM1_DEFAULT_PRICING_NAME',
                                        'u1.UM1_NAME as UM1_PRICING_ID_NAME', 'MD1.MD0_UNIT_PRICE AS MD1_UNIT_PRICE',
                                        'MD0.MD0_UNIT_PRICE', 'MD1.MD0_MARKUP AS MD1_MARKUP', 'MD0.MD0_MARKUP',
                                        'MD1.FI1_ID', 'MD0.FI1_ID AS FI0_ID'])
                            ->leftJoin("md0_master_data as MD1" , "MD1.MD0_PARENT_ID = MD0.MD0_ID AND MD1.MD0_DELETE_FLAG = 0")
                            ->leftJoin("vd0_vendor as VD1" , "VD1.VD0_ID = MD1.VD0_ID AND VD1.VD0_DELETE_FLAG = 0")
                            ->leftJoin("um1_unit as u0" , "u0.UM1_ID = MD0.UM1_DEFAULT_PRICING and u0.UM1_DELETE_FLAG = 0")
                            ->leftJoin("um1_unit as u1" , "u1.UM1_ID = MD1.UM1_DEFAULT_PRICING and u1.UM1_DELETE_FLAG = 0");
                    $query->andFilterWhere(['=', 'MD0.MD0_DELETE_FLAG', 0]);
                    $query->andFilterWhere(['=', 'MD1.MD0_DELETE_FLAG', 0]);
                    $query->andFilterWhere(['not', ['MD1.MD0_ID' => null]]);
        } else {
            $query = MasterCatalog::find()->alias('MD0')
                             ->select([  'MD0.MD0_ID', 'MD1.MD1_ID', 'MD0.UM1_DEFAULT_PRICING', 'MD1.UM1_PRICING_ID',
                                         'CO1.CO1_ID', 'CO1.CO1_SHORT_CODE', 'CO1.CO1_NAME',
                                         'MD0.MD0_PART_NUMBER', 'MD1.MD1_PART_NUMBER', 'MD0.MD0_BODY', 'MD1.MD1_BODY',
                                         'MD0.MD0_DETAIL', 'MD1.MD1_DETAIL', 'MD0.MD0_PAINT', 'MD1.MD1_PAINT',
                                         'MD0.MD0_BODY', 'MD1.MD0_BODY', 'MD0.MD0_DETAIL', 'MD1.MD0_DETAIL', 'MD0.MD0_PAINT', 'MD1.MD0_PAINT',
                                         'MD0.MD0_DESC1', 'MD1.MD1_DESC1', 'u0.UM1_NAME as UM1_DEFAULT_PRICING_NAME',
                                         'u1.UM1_NAME as UM1_PRICING_ID_NAME', 'MD1.MD1_UNIT_PRICE',
                                         'MD0.MD0_UNIT_PRICE', 'MD1.MD1_MARKUP', 'MD0.MD0_MARKUP',
                                         'MD1.FI1_ID', 'MD0.FI1_ID AS FI0_ID'])
                             ->leftJoin("md1_master_data as MD1" , "MD1.MD0_ID = MD0.MD0_ID AND MD1.MD1_DELETE_FLAG = 0")
                             ->leftJoin("co1_company as CO1" , "CO1.CO1_ID = MD1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
                             ->leftJoin("um1_unit as u0" , "u0.UM1_ID = MD0.UM1_DEFAULT_PRICING and u0.UM1_DELETE_FLAG = 0")
                             ->leftJoin("um1_unit as u1" , "u1.UM1_ID = MD1.UM1_PRICING_ID and u1.UM1_DELETE_FLAG = 0");
                     $query->andFilterWhere(['=', 'MD0.MD0_DELETE_FLAG', 0]);
                     $query->andFilterWhere(['=', 'MD1.MD1_DELETE_FLAG', 0]);
                     $query->andFilterWhere(['not', ['MD1.MD1_ID' => null]]);
        }

        if(isset($this->VD0_ID)) {
            $query->andFilterWhere(['=', 'MD0.VD0_ID', $this->VD0_ID]);
        }

        if(isset($this->MD0_IDs)) {
            $query->andWhere(['in', 'MD0.MD0_ID', explode(',', $this->MD0_IDs)]);
        }

        return $query;
    }

    public function getAllParts($params)
    {
        $query = $this->getPartsQuery($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function getLinkedParts($params)
    {
	    $query = MasterData::find()
		    ->alias('MD1')
		    ->select([
			    'DISTINCT `MD1`.`MD1_ID`',
			    'MD1.MD0_ID',
			    'MD1.MD1_PART_NUMBER',
			    'MD1.MD1_DESC1',
			    'MD1.MD1_UPC1',
			    'VD0.VD0_ID',
			    'VD0.VD0_SHORT_CODE',
			    'VD0.VD0_NAME'])
		    ->leftJoin("md0_master_data as MD0" , "MD1.MD0_ID = MD0.MD0_ID AND MD0.MD0_DELETE_FLAG = 0")
		    ->leftJoin("vd0_vendor as VD0" , "VD0.VD0_ID = MD0.VD0_ID AND VD0.VD0_DELETE_FLAG = 0");
	    $query->where(['=', 'MD1.MD1_DELETE_FLAG', 0]);
	    $query->andFilterWhere(['=', 'MD1.MD1_PART_NUMBER', $this->MD0_PART_NUMBER]);

        $this->load( $params, '');

        if(isset($this->MD0_PART_NUMBER)) {
            $query->andFilterWhere(['=', 'MD1.MD1_PART_NUMBER', $this->MD0_PART_NUMBER]);
        }

	    if(isset($this->MD0_ID)) {
		    $query->andFilterWhere(['=', 'MD1.MD0_ID', $this->MD0_ID]);
	    }

        if(isset($this->VD0_ID)) {
            $query->andFilterWhere(['in', 'MD0.VD0_ID', $this->VD0_ID]);
        }

	    $user = Yii::$app->user->getIdentity();
	    if(isset($user->CO1_ID)) {
		    $query->andFilterWhere(['=', 'MD1.MD0_ID', $user->CO1_ID]);
	    }

        $query->distinct();

        return $query->asArray()->all();
    }

	public function deleteMasterVendorParts($params)
	{
		$user = Yii::$app->user->getIdentity();
		Yii::$app->db->createCommand()
			->update('md0_master_data', ['MD0_DELETE_FLAG' => 1, 'MD0_MODIFIED_BY' => $user->US1_ID, 'MD0_MODIFIED_ON' => new \yii\db\Expression('UTC_TIMESTAMP()')], ['=', 'VD0_ID', $params['VD0_ID']])
			->execute();
	}
}
