<?php

namespace app\models;

use app\models\scopes\JobTicketQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "jt1_job_ticket".
 *
 * @property int $JT1_ID
 * @property int $CO1_ID
 * @property int $GL1_ID
 * @property int $US1_ID
 * @property int $LO1_ID
 * @property int $VD1_ID
 * @property int $CU1_ID
 * @property int $IN1_ID
 * @property string $JT1_NUMBER
 * @property string $JT1_MITCHELL_RO_ID
 * @property int $JT1_STATUS
 * @property string $JT1_TOTAL_VALUE
 * @property string $JT1_COMMENT
 * @property string $JT1_HOUR
 * @property bool $JT1_PRINTED
 * @property int $JT1_DELETE_FLAG
 * @property string $JT1_CREATED_ON
 * @property int $JT1_CREATED_BY
 * @property string $JT1_MODIFIED_ON
 * @property int $JT1_MODIFIED_BY
 */
class JobTicket extends ActiveRecord
{
    protected $_tablePrefix = 'JT1';

    public $ORDERED_ON;
    public $MODIFIED_BY;
    public $CREATED_BY;
    public $MODIFIED_ON;
    public $CREATED_ON;
    public $LO1_NAME;
    public $VD1_NAME;
    public $CY1_ID;
    public $CU1_NAME;
    public $CU1_ADDRESS1;
    public $CU1_ADDRESS2;
    public $CU1_CITY;
    public $CU1_STATE;
    public $CU1_ZIP;
    public $CU1_PHONE;
    public $CU1_FAX;
    public $CO1_NAME;
    public $JT1_IDs;
    public $CY1_SHORT_CODE;

    const ORDER_STATUS_OPEN     = 0;
    const ORDER_STATUS_RECEIVED = 1;
    const ORDER_STATUS_SENT     = 2;
    const ORDER_STATUS_INVOICED = 3;
    const ORDER_STATUS_PRINTED  = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jt1_job_ticket';
    }

    public static function primaryKey()
    {
        return ['JT1_ID'];
    }

    /**
     * {@inheritdoc}
     * @return JobTicketQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new JobTicketQuery(get_called_class());
    }

    public function fields() {
        $fields = parent::fields();
        $fields["ORDERED_ON"] = function($model){ return $model->ORDERED_ON; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["LO1_NAME"] = function($model){ return $model->LO1_NAME; };
        $fields["VD1_NAME"] = function($model){ return $model->VD1_NAME; };
        $fields["CY1_ID"] = function($model){ return $model->CY1_ID; };
        $fields["CU1_NAME"] = function($model){ return $model->CU1_NAME; };
        $fields["CU1_ADDRESS1"] = function($model){ return $model->CU1_ADDRESS1; };
        $fields["CU1_ADDRESS2"] = function($model){ return $model->CU1_ADDRESS2; };
        $fields["CU1_CITY"] = function($model){ return $model->CU1_CITY; };
        $fields["CU1_CITY"] = function($model){ return $model->CU1_STATE; };
        $fields["CU1_ZIP"] = function($model){ return $model->CU1_ZIP; };
        $fields["CU1_PHONE"] = function($model){ return $model->CU1_PHONE; };
        $fields["CU1_FAX"] = function($model){ return $model->CU1_FAX; };
        $fields["CO1_NAME"] = function($model){ return $model->CO1_NAME; };
        $fields["JT1_IDs"] = function($model){ return $model->JT1_IDs; };
        $fields["CY1_SHORT_CODE"] = function($model){ return $model->CY1_SHORT_CODE; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CO1_ID', 'GL1_ID', 'US1_ID', 'LO1_ID', 'VD1_ID', 'CU1_ID', 'IN1_ID', 'JT1_STATUS', 'JT1_DELETE_FLAG', 'JT1_CREATED_BY', 'JT1_MODIFIED_BY'], 'integer'],
            [['JT1_TOTAL_VALUE', 'JT1_HOUR'], 'number'],
            [['JT1_PRINTED'], 'boolean'],
            [['JT1_COMMENT', 'ORDERED_ON', 'MODIFIED_BY', 'CREATED_BY', 'MODIFIED_ON', 'CREATED_ON', 'LO1_NAME', 'VD1_NAME', 'CY1_ID', 'CU1_NAME',
            'CU1_ADDRESS1', 'CU1_ADDRESS2', 'CU1_CITY', 'CU1_CITY', 'CU1_ZIP', 'CU1_PHONE', 'CU1_FAX', 'CO1_NAME', 'JT1_IDs', 'CY1_SHORT_CODE'], 'string'],
            [['JT1_CREATED_ON', 'JT1_MODIFIED_ON'], 'safe'],
            [['JT1_NUMBER'], 'string', 'max' => 50],
            [['JT1_MITCHELL_RO_ID'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'JT1_ID' => 'Jt1  ID',
            'CO1_ID' => 'Co1  ID',
            'GL1_ID' => 'Gl1  ID',
            'US1_ID' => 'Us1  ID',
            'LO1_ID' => 'Lo1  ID',
            'VD1_ID' => 'Vd1  ID',
            'CU1_ID' => 'Cu1  ID',
            'CO1_NAME' => 'Company Name',
            'IN1_ID' => 'In1  ID',
            'JT1_NUMBER' => 'Jt1  Number',
            'JT1_MITCHELL_RO_ID' => 'Jt1  Mitchell  Ro  ID',
            'JT1_STATUS' => 'Jt1  Status',
            'JT1_TOTAL_VALUE' => 'Jt1  Total  Value',
            'JT1_COMMENT' => 'Jt1  Comment',
            'JT1_HOUR' => 'Jt1  Hour',
            'JT1_PRINTED' => 'Jt1  Printed',
            'JT1_DELETE_FLAG' => 'Jt1  Delete  Flag',
            'JT1_CREATED_ON' => 'Jt1  Created  On',
            'JT1_CREATED_BY' => 'Jt1  Created  By',
            'JT1_MODIFIED_ON' => 'Jt1  Modified  On',
            'JT1_MODIFIED_BY' => 'Jt1  Modified  By',
        ];
    }

    public function getQuery($params)
    {
        $query = JobTicket::find()->alias('JT1')
            ->select([
                'JT1.JT1_ID',
                'JT1.JT1_NUMBER',
                'VD1.VD1_ID',
                'VD1.VD1_NAME',
                'JT1.JT1_TOTAL_VALUE',
                'JT1.JT1_COMMENT',
                'JT1.CO1_ID',
                'CO1.CO1_SHORT_CODE',
                'CO1.CO1_NAME',
                'JT1.LO1_ID',
                'LO1.LO1_SHORT_CODE',
                'LO1.LO1_NAME',
                'if(COUNT(IJ1.JT1_ID) = SUM(COALESCE(IJ1.IJ1_PRINT_FLAG,0)) AND COUNT(IJ1.JT1_ID) > 0, 4, JT1.JT1_STATUS) AS JT1_STATUS',
                'if(COUNT(IJ1.JT1_ID) = SUM(COALESCE(IJ1.IJ1_PRINT_FLAG,0)) AND COUNT(IJ1.JT1_ID) > 0, "Printed", if(JT1.JT1_STATUS = 3, "Invoiced", "Open")) AS JT1_STATUS_TEXT',
                'JT1.JT1_PRINTED',
                'MODIFIED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('JT1_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                'CREATED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('JT1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                'ORDERED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('JT1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                'JT1.JT1_MODIFIED_BY',
                'JT1.JT1_CREATED_BY',
                'CREATED_BY'  => 'us1_create.US1_NAME',
                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
            ->leftJoin("co1_company as CO1" , "CO1.CO1_ID = JT1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
            ->leftJoin("ij1_invoice_header as IJ1" , "IJ1.JT1_ID = JT1.JT1_ID AND IJ1.IJ1_DELETE_FLAG = 0")
            ->leftJoin("vd1_vendor as VD1" , "VD1.VD1_ID = IJ1.VD1_ID AND VD1.VD1_DELETE_FLAG = 0")
            ->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = JT1.LO1_ID AND LO1.LO1_DELETE_FLAG = 0")
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = JT1.JT1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = JT1.JT1_MODIFIED_BY");

	    if ($params && !empty($params)) {
		    $this->load($params, '');
	    }

	    if (isset($this->JT1_IDs)) {
		    $query->addSelect([ 'ij1.IJ1_ID', 'ij1.IJ1_NUMBER']);
		    $query->leftJoin("cy1_country AS cy1_to" , "cy1_to.CY1_ID = LO2.CY1_ID");
		    $query->leftJoin("cy1_country AS cy1_from" , "cy1_from.CY1_ID = LO1.CY1_ID");
		    $query->leftJoin("cy1_country AS cy1_vd1" , "cy1_vd1.CY1_ID = VD1.CY1_ID");

		    $query->andFilterWhere(['IN', 'JT1.JT1_ID', explode(',', $this->JT1_IDs)]);
	    }

	    $query->groupBy(['JT1.JT1_ID']);
        $query->orderBy(['JT1.JT1_CREATED_ON' => SORT_DESC]);

        if(isset($this->JT1_PRINTED)) {
            $query->andFilterWhere(['=', 'JT1.JT1_PRINTED', $this->JT1_PRINTED]);
        }

        if(isset($this->ORDERED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(JT1.JT1_CREATED_ON, "%m-%d-%Y")', $this->ORDERED_ON]);
        }

        if(isset($this->JT1_NUMBER)) {
            $query->andFilterWhere(['like', 'JT1.JT1_NUMBER', $this->JT1_NUMBER]);
        }

        if(isset($this->VD1_NAME)) {
            $query->andFilterWhere(['=', 'VD1.VD1_ID', $this->VD1_NAME]);
        }

        if(isset($this->JT1_TOTAL_VALUE)) {
            $query->andFilterWhere(['like', 'JT1.JT1_TOTAL_VALUE', $this->JT1_TOTAL_VALUE]);
        }

        if(isset($this->CO1_NAME)) {
            $query->andFilterWhere(['=', 'JT1.CO1_ID', $this->CO1_NAME]);
        }

        if(isset($this->LO1_NAME)) {
            $query->andFilterWhere(['=', 'JT1.LO1_ID', $this->LO1_NAME]);
        }

        if(isset($this->MODIFIED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(JT1.JT1_MODIFIED_ON, "%m-%d-%Y")', $this->MODIFIED_ON]);
        }

        if(isset($this->MODIFIED_BY)) {
            $query->andFilterWhere(['=', 'us1_modify.US1_ID', $this->MODIFIED_BY]);
        }

        if(isset($this->CREATED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(JT1.JT1_CREATED_ON, "%m-%d-%Y")', $this->CREATED_ON]);
        }

        if(isset($this->CREATED_BY)) {
            $query->andFilterWhere(['=', 'us1_create.US1_ID', $this->CREATED_BY]);
        }

        $query->initScope();

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['JT1_NUMBER'] = SORT_ASC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $params['perPage'],
               'page'     => $params['page']
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getJobTicket($id, $params)
    {
        $query = $this->getQuery($params);
        $query->leftJoin("cu1_customer as CU1" , "CU1.CU1_ID = JT1.CU1_ID AND CU1.CU1_DELETE_FLAG = 0");
        $query->leftJoin("cy1_country as CY1" , "CY1.CY1_ID = CU1.CY1_ID AND CY1.CY1_DELETE_FLAG = 0");
        $query->addSelect('CU1.*');
        $query->addSelect('CY1.CY1_NAME', 'CY1.CY1_SHORT_CODE');
        $query->addSelect('JT1.*');
        $query->where(['=', 'JT1.JT1_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

	public function getPdfJobTicketsQuery($params)
	{
		$query = JobTicket::find()->alias('JT1')
			->select([ 'JT1.JT1_ID', 'jt1.JT1_NUMBER', 'ij1.IJ1_ID', 'ij1.IJ1_NUMBER',
				'IJ1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('ij1.IJ1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
				'JT1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('JT1.JT1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
				'ij1.IJ1_TOTAL_SELL', 'ij1.IJ1_TOTAL_VALUE', 'ij1.IJ1_COMMENT', 'co1.CO1_NAME',
				'ij1.LO1_ID', 'LO1.LO1_NAME', 'LO1.LO1_ADDRESS1', 'LO1.LO1_ADDRESS2', 'LO1.LO1_CITY',
				'LO1.LO1_STATE', 'LO1.LO1_ZIP', 'LO1.LO1_PHONE', 'LO1.LO1_FAX',
				'VD1.VD1_NAME', 'VD1.VD1_ADDRESS1', 'VD1.VD1_ADDRESS2', 'VD1.VD1_CITY',
				'VD1.VD1_STATE', 'VD1.VD1_ZIP', 'VD1.VD1_PHONE', 'VD1.VD1_FAX',
				'cu1.CU1_NAME', 'cu1.CU1_ADDRESS1', 'cu1.CU1_ADDRESS2', 'cu1.CU1_CITY',
				'cu1.CU1_STATE', 'cu1.CU1_ZIP', 'cu1.CU1_PHONE', 'cu1.CU1_FAX',
				'cy1.CY1_SHORT_CODE'
			])
			->leftJoin("ij1_invoice_header as ij1" , "JT1.JT1_ID = ij1.JT1_ID")
			->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = ij1.LO1_ID")
			->leftJoin("cy1_country AS cy1" , "cy1.CY1_ID = lo1.CY1_ID")
			->leftJoin("vd1_vendor AS VD1" , "VD1.VD1_ID = ij1.VD1_ID")
			->leftJoin("cy1_country AS cy1_vd1" , "cy1_vd1.CY1_ID = VD1.CY1_ID")
			->leftJoin("cu1_customer AS cu1" , "cu1.CU1_ID = jt1.CU1_ID")
			->leftJoin("cy1_country AS cy1_cu1" , "cy1_cu1.CY1_ID = cu1.CY1_ID")
			->leftJoin("co1_company AS co1" , "co1.CO1_ID = ij1.CO1_ID");

		if ($params && !empty($params)) {
			$this->load($params, '');
		}

		if (isset($this->JT1_IDs)) {
			$query->andFilterWhere(['IN', 'JT1.JT1_ID', explode(',', $this->JT1_IDs)]);
		}

		return $query;
	}

	public function getPdfJobTickets($params)
	{
		$query = $this->getPdfJobTicketsQuery( $params );
		return $query->asArray()->all();
	}

	public function delete()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $errors = [];
        try{
            $this->JT1_DELETE_FLAG = 1;
	        $this->save(false);

	        $jobLines = JobLine::find()->where([
	            "JT1_ID" => $this->JT1_ID,
                "JT2_DELETE_FLAG" => 0])->all();

            foreach ($jobLines as $jobLine) {
                $jobLine->delete();
            }

            $transaction->commit();
        }catch(Exception $e){
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }
        return [
            'success' => !$errors,
            'errors' => $errors
        ];
    }
}
