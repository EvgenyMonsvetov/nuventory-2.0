<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "po2_order_line".
 *
 * @property int $PO2_ID
 * @property int $PO1_ID
 * @property int $PO2_LINE
 * @property int $PO2_STATUS
 * @property double $PO2_ORDER_QTY
 * @property double $PO2_RECEIVED_QTY
 * @property int $MD1_ID
 * @property int $CA1_ID
 * @property int $CO1_ID
 * @property int $GL1_ID
 * @property string $PO2_PART_NUMBER
 * @property string $PO2_DESC1
 * @property string $PO2_DESC2
 * @property double $UM1_ID
 * @property string $PO2_UNIT_PRICE
 * @property string $PO2_DELETE_FLAG
 * @property int $PO2_CREATED_BY
 * @property string $PO2_CREATED_ON
 * @property int $PO2_MODIFIED_BY
 * @property string $PO2_MODIFIED_ON
 * @property int $OR1_ID
 * @property string $PO2_TYPE
 * @property double $PO2_TOTAL_VALUE
 * @property int $LO1_ID
 */
class PurchaseOrderLine extends ActiveRecord
{
    protected $_tablePrefix = 'PO2';

    public $PO1_NUMBER;
    public $CO1_ID;
	public $PO1_IDs;
	public $PO1_STATUS_TEXT;
	public $PO2_RECEIVING_QTY;
	public $QTY;
	public $UM1_RECEIVE_NAME;
	public $CA1_NAME;
	public $CREATED_ON;
	public $CREATED_BY;

	const ORDER_LINE_STATUS_OPEN = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'po2_order_line';
    }

    public static function primaryKey()
    {
        return ['PO2_ID'];
    }

    public function fields() {
        $fields = parent::fields();
        $fields["CO1_ID"] = function($model){ return $model->CO1_ID; };
        $fields["PO1_NUMBER"] = function($model){ return $model->PO1_NUMBER; };
        $fields["PO1_IDs"] = function($model){ return $model->PO1_IDs; };
        $fields["PO1_STATUS_TEXT"] = function($model){ return $model->PO1_STATUS_TEXT; };
        $fields["PO2_RECEIVING_QTY"] = function($model){ return $model->PO2_RECEIVING_QTY; };
        $fields["QTY"] = function($model){ return $model->QTY; };
        $fields["UM1_RECEIVE_NAME"] = function($model){ return $model->UM1_RECEIVE_NAME; };
        $fields["CA1_NAME"] = function($model){ return $model->CA1_NAME; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PO1_ID', 'PO2_LINE', 'PO2_STATUS', 'MD1_ID', 'CA1_ID', 'CO1_ID', 'GL1_ID', 'PO2_CREATED_BY', 'PO2_MODIFIED_BY', 'OR1_ID', 'LO1_ID'], 'integer'],
            [['PO2_RECEIVED_QTY', 'UM1_ID', 'PO2_UNIT_PRICE', 'PO2_TOTAL_VALUE', 'PO2_ORDER_QTY'], 'number'],
            [['PO2_CREATED_ON', 'PO2_MODIFIED_ON'], 'safe'],
            [['PO2_PART_NUMBER', 'PO2_DELETE_FLAG', 'PO1_NUMBER'], 'string', 'max' => 100],
            [['PO2_DESC1', 'PO2_DESC2'], 'string', 'max' => 300],
            [['PO2_DELETE_FLAG'], 'string', 'max' => 1],
	        [['PO1_IDs'], 'string'],
	        [['PO2_TYPE'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PO2_ID' => 'Po2  ID',
            'PO1_ID' => 'Po1  ID',
            'PO2_LINE' => 'Po2  Line',
            'PO2_STATUS' => 'Po2  Status',
            'PO2_ORDER_QTY' => 'Po2  Order  Qty',
            'PO2_RECEIVED_QTY' => 'Po2  Received  Qty',
            'MD1_ID' => 'Md1  ID',
            'CA1_ID' => 'Ca1  ID',
            'CO1_ID' => 'Co1  ID',
            'GL1_ID' => 'Gl1  ID',
            'PO2_PART_NUMBER' => 'Po2  Part  Number',
            'PO2_DESC1' => 'Po2  Desc1',
            'PO2_DESC2' => 'Po2  Desc2',
            'UM1_ID' => 'Um1  ID',
            'PO2_UNIT_PRICE' => 'Po2  Unit  Price',
            'PO2_DELETE_FLAG' => 'Po2  Delete  Flag',
            'PO2_CREATED_BY' => 'Po2  Created  By',
            'PO2_CREATED_ON' => 'Po2  Created  On',
            'PO2_MODIFIED_BY' => 'Po2  Modified  By',
            'PO2_MODIFIED_ON' => 'Po2  Modified  On',
            'OR1_ID' => 'Or1  ID',
            'PO2_TYPE' => 'Po2  Type',
            'PO2_TOTAL_VALUE' => 'Po2  Total  Value',
            'LO1_ID' => 'Lo1  ID',
        ];
    }

    public function getQuery($params)
    {
        $query = PurchaseOrderLine::find()->alias('PO2')
                    ->select([
                        'PO2.*',
                        'PO2.PO2_ORDER_QTY * coalesce(um2_rec.UM2_FACTOR, 1) AS PO2_RECEIVING_QTY',
                        'MD1.MD1_VENDOR_PART_NUMBER',
                        'um1_unit.UM1_NAME',
                        'um1_unit.UM1_SHORT_CODE',
                        'um1_unit.UM1_SIZE',
                        'um1_receive.UM1_NAME AS UM1_RECEIVE_NAME',
                        'md2_from.UM1_RECEIPT_ID',
                        'md2_from.UM1_PURCHASE_ID',
                        'um2_rec.UM2_FACTOR AS UM2_RECEIPT_FACTOR',
                        'PO1.LO1_ID',
                        'GL1.GL1_NAME',
                        'CA1.CA1_ID',
                        'CA1.CA1_NAME',
                        'if(PO2.PO2_STATUS = 1, "Received", "Open") AS PO1_STATUS_TEXT',
                        'if((PO2.PO2_ORDER_QTY * coalesce(um2_rec.UM2_FACTOR, 1) - coalesce(PO2.PO2_RECEIVED_QTY, 0)) < 0, 0,
                                     PO2.PO2_ORDER_QTY * coalesce(um2_rec.UM2_FACTOR, 1) - coalesce(PO2.PO2_RECEIVED_QTY, 0)) AS QTY',
                        'MODIFIED_ON'      => 'DATE_FORMAT(PO2_MODIFIED_ON, "%m.%d.%Y %H:%i")',
                        'CREATED_ON'       => 'DATE_FORMAT(PO2_CREATED_ON, "%m.%d.%Y %H:%i")',
	                    'MODIFIED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('PO2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
	                    'CREATED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('PO2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                        'PO1_MODIFIED_ON',
                        'CREATED_BY'  => 'us1_create.US1_NAME',
                        'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("md1_master_data as MD1" , "MD1.MD1_ID = PO2.MD1_ID")
                    ->leftJoin("po1_order_header as PO1" , "PO1.PO1_ID = PO2.PO1_ID")
                    ->leftJoin("md2_location as md2_from" , "md2_from.LO1_ID = PO1.lo1_id AND md2_from.MD1_ID = PO2.MD1_ID")
                    ->leftJoin("ca1_category as CA1" , "CA1.CA1_ID = PO2.CA1_ID")
                    ->leftJoin("gl1_glcode as GL1" , "GL1.GL1_ID = PO2.GL1_ID AND GL1.GL1_DELETE_FLAG = 0")
                    ->leftJoin("um1_unit as um1_unit" , "um1_unit.UM1_ID = PO2.UM1_ID")
                    ->leftJoin("um1_unit as um1_receive" , "um1_receive.UM1_ID = md2_from.UM1_RECEIPT_ID")
                    ->leftJoin("um2_conversion as um2_rec" ,    "um2_rec.MD1_ID = PO2.MD1_ID AND um2_rec.UM1_TO_ID = md2_from.UM1_RECEIPT_ID AND um2_rec.UM1_FROM_ID = md2_from.UM1_PURCHASE_ID AND um2_rec.UM2_DELETE_FLAG=0")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = PO2.PO2_MODIFIED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = PO2.PO2_CREATED_BY")
                    ->leftJoin("us1_user as us3_user" , "us3_user.US1_ID = PO1.PO1_ORDERED_BY");
        $query->where(['=', 'PO2_DELETE_FLAG', 0]);
        $query->orderBy(['PO1_MODIFIED_ON' => SORT_DESC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->CO1_ID) && $this->CO1_ID != 0) {
			$query->andFilterWhere(['=', 'PO2.CO1_ID', $this->CO1_ID]);
		}

        if(isset($this->PO1_ID)) {
            $query->andFilterWhere(['=', 'PO1.PO1_ID', $this->PO1_ID]);
        }

        if(isset($this->PO2_ID)) {
            $query->andFilterWhere(['=', 'PO2.PO2_ID', $this->PO2_ID]);
        }

        if(isset($this->LO1_TO_ID)) {
			$query->andFilterWhere(['=', 'PO2.LO1_TO_ID', $this->LO1_TO_ID]);
		}

		if (isset($this->PO1_IDs)) {
			$query->andFilterWhere(['IN', 'PO2.PO1_ID', explode(',', $this->PO1_IDs)]);
		}


        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['PO2_LINE'] = SORT_ASC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query' => $query,
           'pagination' => false,
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getPurchaseOrderLine($id)
    {
        $query = $this->getQuery(null);
        $query->where(['=', 'PO2.PO2_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

	public function getPdfOrderLines($params)
	{
		$query = $this->getQuery( $params );
		return $query->asArray()->all();
	}
}
