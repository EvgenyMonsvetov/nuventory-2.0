<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lg2_price_update".
 *
 * @property int $LG2_ID
 * @property int $MD1_ID
 * @property double $LG2_OLD_PRICE
 * @property double $LG2_NEW_PRICE
 * @property string $LG2_CREATED_ON
 */
class Prices extends \yii\db\ActiveRecord
{
	protected $_tablePrefix = 'LG2';

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lg2_price_update';
    }

	public static function primaryKey()
	{
		return ['LG2_ID'];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MD1_ID'], 'required'],
            [['MD1_ID'], 'integer'],
            [['LG2_OLD_PRICE', 'LG2_NEW_PRICE'], 'number'],
            [['LG2_CREATED_ON'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LG2_ID' => 'Lg2  ID',
            'MD1_ID' => 'Md1  ID',
            'LG2_OLD_PRICE' => 'Lg2  Old  Price',
            'LG2_NEW_PRICE' => 'Lg2  New  Price',
            'LG2_CREATED_ON' => 'Lg2  Created  On',
        ];
    }

	public function getQuery($params)
	{
		$query = self::find()->alias('LG2')
			->select([ 'LG2.*', 'md1.MD1_ID', 'md1.MD1_PART_NUMBER', 'md1.MD1_DESC1'])
			->leftJoin("md1_master_data AS md1" , "md1.MD1_ID=lg2.MD1_ID");

		$user = Yii::$app->user->getIdentity();
		if (!empty($user->CO1_ID) && $user->CO1_ID != 0) {
			$query->andFilterWhere(['=', 'MD1.CO1_ID', $user->CO1_ID]);
		}

		if(isset($params['MD1_ID'])){
			$query->andFilterWhere(['=', 'LG2.MD1_ID', $params['MD1_ID']]);
		}

		$query->orderBy(['lg2_created_on'  => SORT_DESC ]);

		return $query;
	}

	public function search($params) {
		$query = $this->getQuery($params);
		return $query->asArray()->all();
	}
}
