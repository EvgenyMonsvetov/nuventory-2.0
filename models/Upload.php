<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;
use yii\helpers\Url;
use yii\helpers\FileHelper;

class Upload extends Model
{
    /**
     * @var UploadedFile
     */
    public $fileKey;

    public function rules()
    {
        return [
            [['fileKey'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function uploadImage()
    {
        $errors = [];
        $currDate = Date('YmdHis');
        $currTime = Date('His');
        $filename = $currTime . '.' . $this->fileKey->extension;
        $basePath = isset(Yii::$app->params['partsImagesPath']) && isset(Yii::$app->params['imagesFolderName']) ? str_replace('web/', '',
                Yii::$app->params['partsImagesPath']) . DIRECTORY_SEPARATOR . Yii::$app->params['imagesFolderName'] .
                DIRECTORY_SEPARATOR . $currDate . DIRECTORY_SEPARATOR : '';

        if (isset(Yii::$app->params['partsImagesPath']) && !file_exists(Yii::$app->params['partsImagesPath'])) {
            FileHelper::createDirectory(Yii::$app->params['partsImagesPath'], $mode = 0777, $recursive = true);
        }

        if ($basePath) {
            FileHelper::createDirectory($basePath, $mode = 0777, $recursive = true);
        }

        if ($this->validate()) {
           if ($this->fileKey->saveAs($basePath . $filename)) {
               return [
                   'data' => [
                       'name' => $filename,
                       'path' => Yii::$app->params['imagesFolderName'] . DIRECTORY_SEPARATOR . $currDate . DIRECTORY_SEPARATOR . $filename,
                       'imageUrl' => Url::base(true) . DIRECTORY_SEPARATOR . $basePath . $filename,
                       'success' => true,
                       'errors' => []
                   ]
               ];
           } else
               $errors = ['Cannot save image'];
        } else {
            $errors = ['Validation failed'];
        }

        return [
            'data' => [
                'name' => $filename,
                'path' => Yii::$app->params['imagesFolderName'] . DIRECTORY_SEPARATOR . $currDate . DIRECTORY_SEPARATOR . $filename,
                'imageUrl' => '',
                'success' => !$errors,
                'errors' => $errors
            ]
        ];
    }

    public function saveImage($path)
    {
        $currDate = Date('YmdHis');
        $currTime = Date('His');
        $filename = $currTime . '.' . pathinfo($path)['extension'];
        $basePath = Yii::$app->params['partsImagesPath'] . DIRECTORY_SEPARATOR . Yii::$app->params['imagesFolderName'] .
            DIRECTORY_SEPARATOR . $currDate . DIRECTORY_SEPARATOR;

        if (isset(Yii::$app->params['partsImagesPath']) && ! file_exists(Yii::$app->params['partsImagesPath'])) {
            FileHelper::createDirectory(Yii::$app->params['partsImagesPath'], $mode = 0777, $recursive = true);
        }

        if ($basePath) {
            FileHelper::createDirectory(\Yii::$app->basePath . DIRECTORY_SEPARATOR . $basePath, $mode = 0777, $recursive = true);
        }

        $result = copy(
            \Yii::$app->basePath . DIRECTORY_SEPARATOR . $path,
            \Yii::$app->basePath . DIRECTORY_SEPARATOR . $basePath . $filename
        );

        if ($result) {
            return [
                'name'     => $filename,
                'path'     => Yii::$app->params['imagesFolderName'] .
                    DIRECTORY_SEPARATOR . $currDate . DIRECTORY_SEPARATOR . $filename,
                'size'     => filesize(\Yii::$app->basePath . DIRECTORY_SEPARATOR . $basePath . $filename),
                'type'     => mime_content_type(\Yii::$app->basePath . DIRECTORY_SEPARATOR . $basePath . $filename),
                'imageUrl' => Url::base(true) . DIRECTORY_SEPARATOR . $basePath . $filename,
                'success'  => true,
                'errors'   => []
            ];
        } else {
            return [
                'name'     => $filename,
                'path'     => Yii::$app->params['imagesFolderName'] .
                    DIRECTORY_SEPARATOR . $currDate . DIRECTORY_SEPARATOR . $filename,
                'imageUrl' => '',
                'success'  => false,
                'errors'   => ['Cannot save image']
            ];
        }
    }
}
