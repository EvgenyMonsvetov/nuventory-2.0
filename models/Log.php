<?php

namespace app\models;
use Yii;
use yii\data\ActiveDataProvider;

/**
* This is the model class for table "logs1".
*
* @property string $DATE
* @property integer $ED1_ID
* @property integer $LOG_ID
* @property string $LOG_DESCRIPTION
* @property integer $LOG_UPDATED_BY
* @property string $LOG_UPDATED_ON
* @property string $LOG_FILENAME
* @property integer $LOG_P21
* @property integer $LOG_CHECKED
* @property string $LOG_FILE_TYPE
* @property integer $CO1_ID
* @property integer $LOG_UPLOAD_TIMESTAMP
*/
class Log extends \kak\clickhouse\ActiveRecord
{
    /**
     * Get table name
     * @return string
     */
    public static function tableName()
    {
        return 'dist_' . (YII_ENV_PROD ? '' : 'test_') . 'logs';
    }

    public static function primaryKey()
    {
        return ['LOG_ID', 'ED1_ID', 'LOG_ID'];
    }

    /**
     * @return \kak\clickhouse\Connection the ClickHouse connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('clickhouse');
    }


    /**
     * @inheritdoc
     * @return Array
     */
    public function rules()
    {
        return [
            [['DATE', 'LOG_UPDATED_ON'], 'safe'],
            [['ED1_ID', 'LOG_ID', 'LOG_UPDATED_BY', 'LOG_P21', 'LOG_CHECKED', 'CO1_ID', 'LOG_UPLOAD_TIMESTAMP'], 'integer'],
            [['LOG_DESCRIPTION', 'LOG_FILENAME', 'LOG_FILE_TYPE'], 'string']
        ];
    }

    /**
     * @inheritdoc
     * @return Array
     */
    public function attributeLabels()
    {
        return [
            'DATE' => 'Date',
            'ED1_ID' => 'Ed1  ID',
            'LOG_ID' => 'Log  ID',
            'LOG_DESCRIPTION' => 'Log  Description',
            'LOG_UPDATED_BY' => 'Log  Updated  By',
            'LOG_UPDATED_ON' => 'Log  Updated  On',
            'LOG_FILENAME' => 'Log  Filename',
            'LOG_P21' => 'Log  P21',
            'LOG_CHECKED' => 'Log  Checked',
            'LOG_FILE_TYPE' => 'Log  File  Type',
            'CO1_ID' => 'Co1  ID',
            'LOG_UPLOAD_TIMESTAMP' => 'Log  Upload  Timestamp',
        ];
    }

    public function search($params)
    {
        $query = self::find()->select(['*', 'LOG_FILE_TYPE' => 'toString(LOG_FILE_TYPE)']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'LOG_UPLOAD_TIMESTAMP' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params, '');

        $query->andFilterWhere([
            'CO1_ID'     => (int)$this->CO1_ID,
            'ED1_ID'     => (int)$this->ED1_ID
        ]);

        return $dataProvider;
    }
}
