<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 05/11/2018
 * Time: 16:25
 */

namespace app\models;

use Yii;
use yii\db\Expression;

class ActiveRecord extends \yii\db\ActiveRecord
{
    protected $_tablePrefix = '';
    protected $createdAtAttribute   = '_CREATED_ON';
    protected $updatedAtAttribute   = '_MODIFIED_ON';
    protected $createdByAttribute   = '_CREATED_BY';
    protected $updatedByAttribute   = '_MODIFIED_BY';
    protected $deletedAtAttribute   = '_DELETE_FLAG';
    protected $useAttributeFullName = false;

	public static $MODEL_ERROR = 10000;

    public function getTablePrefix()
    {
        return $this->_tablePrefix;
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if(Yii::$app instanceof \yii\console\Application){
            return [];
        }

        $updatedAtAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->updatedAtAttribute;
        $createdAtAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->createdAtAttribute;

        $updatedByAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->updatedByAttribute;
        $createdByAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->createdByAttribute;

        $deletedAtAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->deletedAtAttribute;

        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => [
                        $updatedAtAttribute,
                        $createdAtAttribute
                    ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => [
                        $updatedAtAttribute
                    ],
                ],
                'value' => new Expression('UTC_TIMESTAMP()')
            ], [
                'class' => 'yii\behaviors\AttributeBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => [
                        $updatedByAttribute,
                        $createdByAttribute
                    ],
                    ActiveRecord::EVENT_BEFORE_UPDATE => [
                        $updatedByAttribute
                    ],
                ],
                'value' => Yii::$app->user->getId()
            ], [
                'class' => 'yii\behaviors\AttributeBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => [
                        $deletedAtAttribute
                    ],
                ],
                'value' => 0
            ]
        ];
    }

	public function getLocalDate($field, $params)
	{
		$interval = isset($params['hours_offset']) ? $params['hours_offset'] : 0;
		return 'DATE_ADD(' . $field . ', interval ' . $interval . ' hour)';
	}
}
