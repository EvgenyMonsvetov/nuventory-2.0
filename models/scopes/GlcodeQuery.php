<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 28/11/2018
 * Time: 15:42
 */

namespace app\models\scopes;
use Yii;
use app\models\Group;
use yii\db\ActiveQuery;
use app\models\Vendor;

class GlcodeQuery extends ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'GL1_DELETE_FLAG', 0]);

        $user = Yii::$app->user->getIdentity();
        $modelClass = $this->modelClass;
        $model = $modelClass::instance();

        if ($user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ) {

            $this->andFilterWhere(['=', $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
        } elseif ($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){

            $query = new ActiveQuery(Vendor::class);
            $query->select(['VD1.CO1_ID'])->from(['VD1'=>'vd1_vendor'])
                ->joinWith(["company CO1"], true)
                ->where(['=', 'VD1.VD0_ID', $user->VD0_ID])
                ->andWhere("CO1.CO1_DELETE_FLAG = 0");

            $this->andFilterWhere(['IN', $model->getTablePrefix(). '.CO1_ID', $query]);
        } else {
	        $this->andFilterWhere(['=', $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
        }
    }
}