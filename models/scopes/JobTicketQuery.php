<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 27/12/2018
 * Time: 15:19
 */

namespace app\models\scopes;
use Yii;
use app\models\Group;

/**
 * This is the ActiveQuery class for [[MasterData]].
 *
 * @see MasterData
 */
class JobTicketQuery extends \yii\db\ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'JT1.JT1_DELETE_FLAG', 0]);
        $user = Yii::$app->user->getIdentity();

        $modelClass = $this->modelClass;
        $model = $modelClass::instance();

        if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ){
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID', $user->CO1_ID]);
            if($user->LO1_ID){
                $this->andFilterWhere(['=', $model->getTablePrefix() . '.LO1_ID', $user->LO1_ID]);
            }
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_STANDARD_USER ||
                $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER){
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID', $user->CO1_ID]);
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.LO1_ID', $user->LO1_ID]);
        }
    }
}