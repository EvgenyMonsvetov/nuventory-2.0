<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 28/11/2018
 * Time: 17:20
 */

namespace app\models\scopes;
use Yii;
use app\models\Group;
use yii\db\Query;

class VendorQuery extends \yii\db\ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'VD1_DELETE_FLAG', 0]);

        $user = Yii::$app->user->getIdentity();
        $modelClass = $this->modelClass;
        $model = $modelClass::instance();

        if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_STANDARD_USER ||
           $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ){
            $this->andFilterWhere(['=', $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $this->andFilterWhere(['=',  $model->getTablePrefix().  '.CO1_ID', $user->CO1_ID]);
            $this->andFilterWhere(['IN', $model->getTablePrefix() . '.VD0_ID', $user->VD0_ID]);
        }
    }
}