<?php
namespace app\models\scopes;
use Yii;
use app\models\Group;
use yii\db\ActiveQuery;
use app\models\Vendor;

class JobInvoiceQuery extends ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'IJ1_DELETE_FLAG', 0]);

        $user = Yii::$app->user->getIdentity();
        $modelClass = $this->modelClass;
        if( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
            $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
            $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ){

            $model = $modelClass::instance();
            $this->andFilterWhere(['=', $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
            if($user->LO1_ID) {
                $this->andFilterWhere(['=', $model->getTablePrefix(). '.LO1_ID', $user->LO1_ID]);
            }
        }elseif( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $model = $modelClass::instance();

            $query = new ActiveQuery(Vendor::class);
            $query->select(['VD1.CO1_ID'])->from(['VD1'=>'vd1_vendor'])
                ->innerJoinWith(["company CO1"], true)
                ->where(['=', 'VD1.VD0_ID', $user->VD0_ID])
                ->andWhere("CO1.CO1_DELETE_FLAG = 0");

            $this->andFilterWhere(['IN', $model->getTablePrefix(). '.CO1_ID', $query]);
        }
    }
}