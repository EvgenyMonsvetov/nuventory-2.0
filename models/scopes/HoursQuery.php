<?php
namespace app\models\scopes;

use Yii;
use yii\db\ActiveQuery;

class HoursQuery extends ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'HO1_DELETE_FLAG', 0]);

        $user = Yii::$app->user->getIdentity();
        $modelClass = $this->modelClass;
        $model = $modelClass::instance();

        $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID',    $user->CO1_ID]);
        if($user->LO1_ID){
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.LO1_ID',    $user->LO1_ID]);
        }
    }
}