<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 21/11/2018
 * Time: 16:28
 */

namespace app\models\scopes;
use app\models\Group;
use Yii;
use yii\db\Query;


class CompanyQuery extends \yii\db\ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'CO1_DELETE_FLAG', 0]);

        $user = Yii::$app->user->getIdentity();

        if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){

            $query = new Query();
            $query->distinct()->select(['VD1.CO1_ID'])->from(['VD1'=>'vd1_vendor'])
                        ->where(['=', 'VD1.VD0_ID', $user->VD0_ID])
                        ->andWhere("VD1.VD1_DELETE_FLAG = 0");

            $this->andFilterWhere(['IN', 'CO1.CO1_ID', $query]);
        }elseif( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_STANDARD_USER ||
                 $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER ||
                 $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
                 $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER){

            $this->andFilterWhere(['=', 'CO1.CO1_ID', $user->CO1_ID]);
        }
    }
}