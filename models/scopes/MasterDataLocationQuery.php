<?php

namespace app\models\scopes;

/**
 * This is the ActiveQuery class for [[MasterDataLocation]].
 *
 * @see MasterDataLocation
 */
class MasterDataLocationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MasterDataLocation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MasterDataLocation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
