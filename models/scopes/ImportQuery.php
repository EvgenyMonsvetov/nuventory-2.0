<?php

namespace app\models\scopes;

use Yii;
use yii\db\ActiveQuery;

class ImportQuery extends ActiveQuery
{
    public function initScope( $prefix = 'IM1')
    {
        $this->andFilterWhere(['=', $prefix . '.delete_flag', 0]);

        $user = Yii::$app->user->getIdentity();
        $this->andFilterWhere(['=', $prefix . '.im1_co1_id',    $user->CO1_ID]);
        if($user->LO1_ID){
            $this->andFilterWhere(['=', $prefix . '.im1_lo1_id',    $user->LO1_ID]);
        }
    }
}