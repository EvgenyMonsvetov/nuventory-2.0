<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 28/11/2018
 * Time: 16:18
 */

namespace app\models\scopes;
use Yii;
use app\models\Group;
use yii\db\Query;

class UserQuery extends \yii\db\ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'US1.DELETED', 0]);
        $user = Yii::$app->user->getIdentity();

        $modelClass = $this->modelClass;
        $model = $modelClass::instance();

        if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ){
            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
            if($user->LO1_ID){
                $this->andFilterWhere(['=',  $model->getTablePrefix(). '.LO1_ID', $user->LO1_ID]);
            }

            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.US1_TYPE', 0]);
            $this->andFilterWhere(['is', $model->getTablePrefix(). '.TE1_ID', new \yii\db\Expression('null')]);

            $this->andFilterWhere(['>=', 'GR1.GR1_ACCESS_LEVEL', Group::GROUP_COMPANY_MANAGER]);
            $this->andFilterWhere(['<>', 'GR1.GR1_ACCESS_LEVEL', Group::GROUP_MANUFACTURER_MANAGER]);
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER){
            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
            if($user->LO1_ID){
                $this->andFilterWhere(['=',  $model->getTablePrefix(). '.LO1_ID', $user->LO1_ID]);
            }
            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.US1_TYPE', 0]);
            $this->andFilterWhere(['is', $model->getTablePrefix(). '.TE1_ID', new \yii\db\Expression('null')]);
            $this->andFilterWhere(['>=', 'GR1.GR1_ACCESS_LEVEL', Group::GROUP_COMPANY_GROUP_MANAGER]);
        }elseif( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER ){
            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.LO1_ID', $user->LO1_ID]);
            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.US1_TYPE', 0]);
            $this->andFilterWhere(['is', $model->getTablePrefix(). '.TE1_ID', new \yii\db\Expression('null')]);

            $this->andFilterWhere(['in',  'GR1.GR1_ACCESS_LEVEL', [Group::GROUP_STANDARD_USER, Group::GROUP_LOCATION_MANAGER]]);
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){

	        $this->andFilterWhere(['not in', 'GR1.GR1_ACCESS_LEVEL', [Group::GROUP_ADMIN]]);
            $this->andFilterWhere(['is', $model->getTablePrefix(). '.TE1_ID', new \yii\db\Expression('null')]);

            $query = new Query();
            $query->distinct()->select(['VD1.CO1_ID'])->from([
                'VD1'=>'vd1_vendor',
                "CO1" => "co1_company" ])
                ->where(['=', 'VD1.VD0_ID', $user->VD0_ID])
                ->andWhere("CO1.CO1_ID = VD1.CO1_ID")
                ->andWhere("VD1.VD1_DELETE_FLAG = 0")
                ->andWhere("CO1.CO1_DELETE_FLAG = 0");

            $this->andFilterWhere(['IN', $model->getTablePrefix() . '.CO1_ID', $query]);
            if( $user->CO1_ID ){
                $this->andFilterWhere(['=',  $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
            }
            if($user->LO1_ID){
                $this->andFilterWhere(['=',  $model->getTablePrefix(). '.LO1_ID', $user->LO1_ID]);
            }
        }elseif( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN){
            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
            if($user->LO1_ID){
                $this->andFilterWhere(['=',  $model->getTablePrefix(). '.LO1_ID', $user->LO1_ID]);
            }
            $this->andFilterWhere(['=',  $model->getTablePrefix(). '.US1_TYPE', 0]);
            $this->andFilterWhere(['>=', 'GR1.GR1_ACCESS_LEVEL', Group::GROUP_ADMIN]);
            $this->andFilterWhere(['is', $model->getTablePrefix(). '.TE1_ID', new \yii\db\Expression('null')]);
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_STANDARD_USER){
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID', $user->CO1_ID]);
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.LO1_ID', $user->LO1_ID]);
	        $this->andFilterWhere(['is', $model->getTablePrefix(). '.TE1_ID', new \yii\db\Expression('null')]);
        }
    }
}
