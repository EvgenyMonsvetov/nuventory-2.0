<?php

namespace app\models\scopes;
use Yii;
use app\models\Group;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[MasterData]].
 *
 * @see MasterData
 */
class MasterVendorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MasterData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MasterData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function initScope()
    {
        $user = Yii::$app->user->getIdentity();
        if ($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER) {
            $this->andWhere('vd0.VD0_ID in (' . $user->VD0_ID . ')' );
        } elseif ($user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER) {
	        $this->addSelect([new Expression('DISTINCT `VD0`.`VD0_ID`')]);
	        $this->leftJoin("vd1_vendor as VD1" , "VD1.VD0_ID = VD0.VD0_ID AND VD1.VD1_DELETE_FLAG = 0");
	        $this->andFilterWhere(['=', 'VD1.CO1_ID', $user->CO1_ID]);
        }
    }
}