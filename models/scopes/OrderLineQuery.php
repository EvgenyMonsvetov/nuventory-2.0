<?php

namespace app\models\scopes;

/**
 * This is the ActiveQuery class for [[MasterData]].
 *
 * @see MasterData
 */
class OrderLineQuery extends \yii\db\ActiveQuery
{
    public function active( $alias = 'OR2')
    {
        return $this->where(['=', $alias . '.OR2_DELETE_FLAG', 0]);
    }
}