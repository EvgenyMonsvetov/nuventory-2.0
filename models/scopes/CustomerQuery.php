<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 28/11/2018
 * Time: 15:48
 */

namespace app\models\scopes;
use app\models\Group;
use Yii;
use yii\db\Query;
use app\models\Vendor;

class CustomerQuery extends yii\db\ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'CU1_DELETE_FLAG', 0]);

        $user = Yii::$app->user->getIdentity();

        $modelClass = $this->modelClass;
        if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER){
            $query = new Query();
            $query->distinct()->select(['VD1.CO1_ID'])->from([
                'VD1'=>'vd1_vendor',
                "CO1" => "co1_company" ])
                ->where(['=', 'VD1.VD0_ID', $user->VD0_ID])
                ->andWhere("CO1.CO1_ID = VD1.CO1_ID")
                ->andWhere("VD1.VD1_DELETE_FLAG = 0")
                ->andWhere("CO1.CO1_DELETE_FLAG = 0");

            $model = $modelClass::instance();
            $this->andFilterWhere(['IN', $model->getTablePrefix(). '.CO1_ID', $query]);
	        if ($user->CO1_ID) {
		        $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID', $user->CO1_ID]);
	        }
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
                $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
                $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ||
                $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER ){
            $model = $modelClass::instance();
            $this->andFilterWhere(['=', $model->getTablePrefix(). '.CO1_ID', $user->CO1_ID]);
        }
    }
}