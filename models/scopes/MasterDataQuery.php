<?php

namespace app\models\scopes;
use Yii;
use app\models\Group;

/**
 * This is the ActiveQuery class for [[MasterData]].
 *
 * @see MasterData
 */
class MasterDataQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MasterData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MasterData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function initScope()
    {
        $this->andFilterWhere(['=', 'MD1_DELETE_FLAG', 0]);
        $user = Yii::$app->user->getIdentity();
        $modelClass = $this->modelClass;
        $model = $modelClass::instance();

        if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $this->andWhere('vd1.VD0_ID = ' . $user->VD0_ID );
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID', $user->CO1_ID]);
        }elseif( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_STANDARD_USER ||
                 $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER ||
                 $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
                 $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
                 $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER){
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID', $user->CO1_ID]);
        }
    }
}