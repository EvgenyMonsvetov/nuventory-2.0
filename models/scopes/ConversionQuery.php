<?php

namespace app\models\scopes;

/**
 * This is the ActiveQuery class for [[Conversion]].
 *
 * @see Conversion
 */
class ConversionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Conversion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Conversion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
