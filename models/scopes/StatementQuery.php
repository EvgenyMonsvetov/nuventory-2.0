<?php
namespace app\models\scopes;
use app\models\Location;
use Yii;
use app\models\Group;
use yii\db\ActiveQuery;
use app\models\Vendor;

class StatementQuery extends ActiveQuery
{
    public function initScope()
    {
        $this->andFilterWhere(['=', 'IS1_DELETE_FLAG', 0]);

        $user = Yii::$app->user->getIdentity();
        $modelClass = $this->modelClass;
        $model = $modelClass::instance();

        if( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $query = new ActiveQuery(Vendor::class);
            $query->select(['VD1.CO1_ID'])->from(['VD1'=>'vd1_vendor'])
                ->innerJoinWith(["company CO1"], true)
                ->where(['=', 'VD1.VD0_ID', $user->VD0_ID])
                ->andWhere("CO1.CO1_DELETE_FLAG = 0");

            $this->andFilterWhere(['IN', $model->getTablePrefix(). '.CO1_ID', $query]);
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_STANDARD_USER ||
                $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER ){
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID', $user->CO1_ID]);
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.LO1_TO_ID', $user->LO1_ID]);
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
                $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ){
            $this->andFilterWhere(['=', $model->getTablePrefix() . '.CO1_ID',    $user->CO1_ID]);

	        if (isset($user->LO1_ID) && $user->LO1_ID != 0) {
	            $location = Location::findOne(['LO1_ID' => $user->LO1_ID]);
	            if($location->LO1_CENTER_FLAG){
		            $this->andFilterWhere(['=', $model->getTablePrefix() . '.LO1_FROM_ID', $user->LO1_ID]);
	            }else{
		            $this->andFilterWhere(['=', $model->getTablePrefix() . '.LO1_TO_ID', $user->LO1_ID]);
	            }
            }
        }
    }
}