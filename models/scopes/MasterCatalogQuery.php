<?php

namespace app\models\scopes;
use Yii;
use app\models\Group;

/**
 * This is the ActiveQuery class for [[MasterData]].
 *
 * @see MasterData
 */
class MasterCatalogQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return MasterData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MasterData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function initScope()
    {
        $user = Yii::$app->user->getIdentity();
        if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $this->andWhere('vd0.VD0_ID IS NULL or vd0.VD0_ID = ' . $user->VD0_ID );
        }
    }
}