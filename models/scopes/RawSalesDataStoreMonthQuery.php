<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 27/12/2018
 * Time: 15:19
 */

namespace app\models\scopes;
use app\models\Location;
use Yii;
use app\models\Group;

/**
 * This is the ActiveQuery class for [[MasterData]].
 *
 * @see MasterData
 */
class RawSalesDataStoreMonthQuery extends \yii\db\ActiveQuery
{
    public function initScope()
    {
        $user = Yii::$app->user->getIdentity();

        $modelClass = $this->modelClass;
        $model = $modelClass::instance();

	    $this->andFilterWhere(['=', $model->getTablePrefix() . '.r03_co1_id', $user->CO1_ID]);
	    if ($user->LO1_ID) {
		    $location = Location::findOne(['LO1_ID' => $user->LO1_ID]);
		    $this->andFilterWhere(['or',
				    [ $model->getTablePrefix() . '.r03_shop_name' => $location->LO1_CCCONE_NAME],
				    [ $model->getTablePrefix() . '.r03_shop_name' => $location->LO1_NAME]]
		    );
	    }
    }
}