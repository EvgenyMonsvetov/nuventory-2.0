<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 21/11/2018
 * Time: 16:28
 */

namespace app\models\scopes;

use app\models\Group;

class GroupQuery extends \yii\db\ActiveQuery
{
    public function init()
    {
        $this->andFilterWhere(['=', 'GR1_DELETE_FLAG', 0]);
        parent::init();
    }

    public function initScope()
    {
        $user = \Yii::$app->user->getIdentity();
        if( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
            $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ||
            $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ){
            $this->andFilterWhere(['>=', 'GR1_ACCESS_LEVEL', $user->group->GR1_ACCESS_LEVEL ]);
        }elseif( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_LOCATION_MANAGER ){
            $this->andFilterWhere(['in', 'GR1_ACCESS_LEVEL', [
                Group::GROUP_LOCATION_MANAGER,
                Group::GROUP_STANDARD_USER
            ]]);
        }elseif($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER){
            $this->andFilterWhere(['>', 'GR1_ACCESS_LEVEL', Group::GROUP_ADMIN ]);
        }else{
            $this->andFilterWhere(['=', 'GR1_ACCESS_LEVEL', $user->group->GR1_ACCESS_LEVEL]);
        }
    }
}