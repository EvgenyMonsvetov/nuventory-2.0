<?php

namespace app\models;

use app\models\scopes\StoreOrderQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "or1_order_header".
 *
 * @property int $OR1_ID
 * @property int $CO1_ID
 * @property int $GL1_ID
 * @property int $OR1_ORDERED_BY
 * @property int $LO1_FROM_ID
 * @property int $LO1_TO_ID
 * @property int $OR1_NUMBER
 * @property int $OR1_STATUS
 * @property int $OR1_GATE_KEEPER_STATUS
 * @property string $OR1_TOTAL_VALUE
 * @property string $OR1_TOTAL_SELL
 * @property string $OR1_COMMENT
 * @property int $OR1_DELETE_FLAG
 * @property string $OR1_CREATED_ON
 * @property int $OR1_CREATED_BY
 * @property string $OR1_MODIFIED_ON
 * @property int $OR1_MODIFIED_BY
 * @property string $OR1_COMPLETED_ON
 * @property int $OR1_JOB_NUMBER
 * @property int $OR1_TYPE
 * @property string $OR1_SHIPPING_METHOD
 * @property int $VD1_ID
 * @property int $CA1_ID
 * @property int $TE1_ID
 * @property int $OR1_INV_STATUS
 * @property string $OR1_CREATED_ON_UTC
 * @property string $OR1_CREATED_ON_LOCAL
 */
class OrderHeader extends ActiveRecord
{
    protected $_tablePrefix = 'OR1';

    public $ORDERED_BY;
    public $OR1_STATUS_TEXT;
    public $OR1_INV_STATUS_TEXT;
    public $VD1_NAME;
    public $TE1_NAME;
    public $ORDER_FROM_LOCATION_NAME;
    public $ORDER_BY_LOCATION_NAME;
    public $MODIFIED_BY;
    public $CREATED_BY;
    public $MODIFIED_ON;
    public $CREATED_ON;
    public $ORDERED_ON;
    public $COMPLETED_ON;
    public $OR1_IDs;
    public $VEND_NAME;

    CONST ORDER_STATUS_OPEN = 0;
    CONST ORDER_STATUS_RECEIVED = 1;
    CONST ORDER_STATUS_SENT = 2;
    CONST ORDER_STATUS_INVOICED = 3;
    CONST ORDER_STATUS_PICK_TICKET = 4;

    CONST TYPE_STORE_ORDER = 0;
    CONST TYPE_TECH_SCAN   = 1;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'or1_order_header';
    }

    public static function primaryKey()
    {
        return ['OR1_ID'];
    }

    /**
     * {@inheritdoc}
     * @return StoreOrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StoreOrderQuery(get_called_class());
    }

    public function fields() {
        $fields = parent::fields();
        $fields["ORDERED_BY"] = function($model){ return $model->ORDERED_BY; };
        $fields["OR1_STATUS_TEXT"] = function($model){ return $model->OR1_STATUS_TEXT; };
        $fields["OR1_INV_STATUS_TEXT"] = function($model){ return $model->OR1_INV_STATUS_TEXT; };
        $fields["VD1_NAME"] = function($model){ return $model->VD1_NAME; };
        $fields["TE1_NAME"] = function($model){ return $model->TE1_NAME; };
        $fields["ORDER_FROM_LOCATION_NAME"] = function($model){ return $model->ORDER_FROM_LOCATION_NAME; };
        $fields["ORDER_BY_LOCATION_NAME"] = function($model){ return $model->ORDER_BY_LOCATION_NAME; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        $fields["ORDERED_ON"] = function($model){ return $model->ORDERED_ON; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["COMPLETED_ON"] = function($model){ return $model->COMPLETED_ON; };
        $fields["OR1_IDs"] = function($model){ return $model->OR1_IDs; };
        $fields["VEND_NAME"] = function($model){ return $model->VEND_NAME; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['OR1_ID', 'CO1_ID', 'GL1_ID', 'TE1_ID', 'OR1_ORDERED_BY', 'LO1_FROM_ID', 'LO1_TO_ID', 'OR1_NUMBER', 'OR1_STATUS', 'OR1_GATE_KEEPER_STATUS', 'OR1_DELETE_FLAG', 'OR1_CREATED_BY', 'OR1_MODIFIED_BY', 'OR1_JOB_NUMBER', 'OR1_TYPE', 'VD1_ID', 'CA1_ID', 'TE1_ID', 'OR1_INV_STATUS'], 'integer'],
            [['OR1_TOTAL_VALUE', 'OR1_TOTAL_SELL'], 'number'],
            [['OR1_COMMENT', 'ORDERED_BY', 'OR1_STATUS_TEXT', 'OR1_INV_STATUS_TEXT', 'VD1_NAME', 'TE1_NAME', 'ORDER_FROM_LOCATION_NAME', 'ORDER_BY_LOCATION_NAME', 'MODIFIED_BY', 'CREATED_BY', 'MODIFIED_ON', 'CREATED_ON', 'ORDERED_ON', 'COMPLETED_ON', 'OR1_IDs', 'VEND_NAME'], 'string'],
            [['OR1_CREATED_ON', 'OR1_MODIFIED_ON', 'OR1_COMPLETED_ON', 'OR1_CREATED_ON_UTC', 'OR1_CREATED_ON_LOCAL'], 'safe'],
            [['OR1_SHIPPING_METHOD'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'OR1_ID' => 'Or1  ID',
            'CO1_ID' => 'Co1  ID',
            'GL1_ID' => 'Gl1  ID',
            'OR1_ORDERED_BY' => 'Or1  Ordered  By',
            'LO1_FROM_ID' => 'Lo1  From  ID',
            'LO1_TO_ID' => 'Lo1  To  ID',
            'OR1_NUMBER' => 'Or1  Number',
            'OR1_STATUS' => 'Or1  Status',
            'OR1_GATE_KEEPER_STATUS' => 'Or1  Gate  Keeper  Status',
            'OR1_TOTAL_VALUE' => 'Or1  Total  Value',
            'OR1_TOTAL_SELL' => 'Or1  Total  Sell',
            'OR1_COMMENT' => 'Or1  Comment',
            'OR1_DELETE_FLAG' => 'Or1  Delete  Flag',
            'OR1_CREATED_ON' => 'Or1  Created  On',
            'OR1_CREATED_BY' => 'Or1  Created  By',
            'OR1_MODIFIED_ON' => 'Or1  Modified  On',
            'OR1_MODIFIED_BY' => 'Or1  Modified  By',
            'OR1_COMPLETED_ON' => 'Or1  Completed  On',
            'OR1_JOB_NUMBER' => 'Or1  Job  Number',
            'OR1_TYPE' => 'Or1  Type',
            'OR1_SHIPPING_METHOD' => 'Or1  Shipping  Method',
            'VD1_ID' => 'Vd1  ID',
            'CA1_ID' => 'Ca1  ID',
            'TE1_ID' => 'Te1  ID',
            'OR1_INV_STATUS' => 'Or1  Inv  Status',
            'OR1_CREATED_ON_UTC' => 'Or1  Created  On  Utc',
            'OR1_CREATED_ON_LOCAL' => 'Or1  Created  On  Local',
        ];
    }

    public function getQuery($params, $type = self::TYPE_STORE_ORDER )
    {
	    $OR1_STATUS_TEXT_Exp = new Expression('if(OR1.OR1_STATUS=1, "Received", if(OR1.OR1_STATUS=2, "Completed","Open"))');
        $query = OrderHeader::find()->alias('OR1')
            ->select([  'OR1.OR1_ID',
                        'OR1.OR1_NUMBER',
                        'OR1.OR1_TYPE',
                        'OR1.OR1_INV_STATUS',
                        'OR1.OR1_JOB_NUMBER',
                        'if(OR1.OR1_INV_STATUS='.self::ORDER_STATUS_INVOICED.', "Invoiced", if(OR1.OR1_INV_STATUS='.self::ORDER_STATUS_PICK_TICKET.', "Pick Ticket","Open")) AS OR1_INV_STATUS_TEXT',
                        'OR1.OR1_SHIPPING_METHOD',
                        'OR1.OR1_STATUS',
                        'OR1_STATUS_TEXT' => $OR1_STATUS_TEXT_Exp,
                        'OR1.OR1_TOTAL_VALUE',
                        'OR1.OR1_TOTAL_SELL as OR1_EXTENDED_VALUE',
                        'OR1.OR1_COMMENT',
                        'CA1.CA1_NAME',
                        'VD1.VD1_ID',
                        'VD1.VD1_NAME',
                        'OR1.CO1_ID',
                        'CO1.CO1_NAME',
                        'TE1.TE1_ID',
                        'TE1.TE1_SHORT_CODE',
                        'TE1.TE1_NAME',
                        'OR1.LO1_FROM_ID',
                        'LO1.LO1_SHORT_CODE AS ORDER_FROM_LOCATION_CODE',
                        'LO1.LO1_NAME AS ORDER_FROM_LOCATION_NAME',
                        'OR1.LO1_TO_ID',
                        'LO2.LO1_SHORT_CODE AS ORDER_BY_LOCATION_CODE',
                        'LO2.LO1_NAME AS ORDER_BY_LOCATION_NAME',
			            'MODIFIED_ON'  => 'DATE_FORMAT(' . $this->getLocalDate('OR1_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
			            'CREATED_ON'   => 'DATE_FORMAT(' . $this->getLocalDate('OR1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
			            'ORDERED_ON'   => 'DATE_FORMAT(' . $this->getLocalDate('OR1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
			            'COMPLETED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('OR1_COMPLETED_ON', $params) . ', "%Y-%m-%d %H:%i")',
                        'OR1.OR1_MODIFIED_ON', 'OR1.OR1_CREATED_ON', 'OR1.OR1_CREATED_ON', 'OR1.OR1_COMPLETED_ON',
                        'us3_user.US1_NAME AS ORDERED_BY',
                        'CREATED_BY'  => 'us1_create.US1_NAME',
                        'MODIFIED_BY' => 'us1_modify.US1_NAME'])
            ->leftJoin("ca1_category as CA1" , "CA1.CA1_ID = OR1.CA1_ID AND CA1.CA1_DELETE_FLAG = 0")
            ->leftJoin("vd1_vendor as VD1" , "VD1.VD1_ID = OR1.VD1_ID AND VD1.VD1_DELETE_FLAG = 0")
            ->leftJoin("co1_company as CO1" , "CO1.CO1_ID = OR1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
            //->leftJoin("co1_company as CO2" , "CO2.CO1_ID = OR1.CO1_ID AND CO2.CO1_DELETE_FLAG = 0")
            ->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = OR1.LO1_FROM_ID AND LO1.LO1_DELETE_FLAG = 0")
            ->leftJoin("lo1_location as LO2" , "LO2.LO1_ID = OR1.LO1_TO_ID AND LO2.LO1_DELETE_FLAG = 0")
            ->leftJoin("te1_technician as TE1" , "TE1.TE1_ID = OR1.TE1_ID AND TE1.TE1_DELETE_FLAG = 0")
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = OR1.OR1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = OR1.OR1_MODIFIED_BY")
            ->leftJoin("us1_user as us3_user" , "us3_user.US1_ID = OR1.OR1_ORDERED_BY");

	    if ($params && !empty($params)) {
		    $this->load($params, '');
	    }

	    if (isset($this->OR1_IDs)) {
		    $query->addSelect([ 'VD1.VD1_NAME AS VEND_NAME', 'VD1.VD1_ADDRESS1 AS VEND_ADDRESS1', 'VD1.VD1_ADDRESS2 AS VEND_ADDRESS2',
			                    'VD1.VD1_CITY AS VEND_CITY', 'VD1.VD1_STATE AS VEND_STATE', 'VD1.VD1_ZIP AS VEND_ZIP',
		    	                'VD1.VD1_PHONE AS VEND_PHONE', 'VD1.VD1_FAX AS VEND_FAX', 'cy1_vd1.CY1_SHORT_CODE AS VEND_COUNTRY',
		    	                'LO2.LO1_NAME AS TO_NAME', 'LO2.LO1_ADDRESS1 AS TO_ADDRESS1', 'LO2.LO1_ADDRESS2 AS TO_ADDRESS2',
			                    'LO2.LO1_CITY AS TO_CITY', 'LO2.LO1_STATE AS TO_STATE', 'LO2.LO1_ZIP AS TO_ZIP',
			                    'LO2.LO1_PHONE AS TO_PHONE', 'LO2.LO1_FAX AS TO_FAX', 'cy1_to.CY1_SHORT_CODE AS TO_COUNTRY',
			                    'LO1.LO1_NAME AS FROM_NAME', 'LO1.LO1_ADDRESS1 AS FROM_ADDRESS1', 'LO1.LO1_ADDRESS2 AS FROM_ADDRESS2',
			                    'LO1.LO1_CITY AS FROM_CITY', 'LO1.LO1_STATE AS FROM_STATE', 'LO1.LO1_ZIP AS FROM_ZIP',
			                    'LO1.LO1_PHONE AS FROM_PHONE', 'LO1.LO1_FAX AS FROM_FAX', 'cy1_from.CY1_SHORT_CODE AS FROM_COUNTRY']);
		    $query->leftJoin("cy1_country AS cy1_to" , "cy1_to.CY1_ID = LO2.CY1_ID");
		    $query->leftJoin("cy1_country AS cy1_from" , "cy1_from.CY1_ID = LO1.CY1_ID");
		    $query->leftJoin("cy1_country AS cy1_vd1" , "cy1_vd1.CY1_ID = VD1.CY1_ID");
		    $query->andFilterWhere(['IN', 'OR1.OR1_ID', explode(',', $this->OR1_IDs)]);
	    }

        if($type == self::TYPE_STORE_ORDER){
            $query->andFilterWhere(['!=', 'LO1_TO_ID', new Expression('LO1_FROM_ID')]);
        }elseif( $type == self::TYPE_TECH_SCAN ){
            $query->where(['=', 'LO1_TO_ID', new Expression('LO1_FROM_ID')]);
        }

        if(isset($this->ORDERED_BY)) {
            $query->andFilterWhere(['=', 'us3_user.US1_ID', $this->ORDERED_BY]);
        }

        if(isset($this->ORDERED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(OR1.OR1_CREATED_ON, "%m-%d-%Y")', $this->ORDERED_ON]);
        }

        if(isset($this->OR1_NUMBER)) {
            $query->andFilterWhere(['like', 'OR1.OR1_NUMBER', $this->OR1_NUMBER]);
        }

        if( $type == self::TYPE_STORE_ORDER ){
            if(isset($this->OR1_INV_STATUS_TEXT)) {
                if($this->OR1_INV_STATUS_TEXT == 'invoiced') {
                    $query->andFilterWhere(['=', 'OR1.OR1_INV_STATUS', self::ORDER_STATUS_INVOICED]);
                } else if($this->OR1_INV_STATUS_TEXT == 'pick_ticket') {
	                $query->andFilterWhere(['=', 'OR1.OR1_INV_STATUS', self::ORDER_STATUS_PICK_TICKET]);
                } else if($this->OR1_INV_STATUS_TEXT == 'open') {
                    $query->andFilterWhere(['<>', 'OR1.OR1_INV_STATUS', self::ORDER_STATUS_INVOICED]);
                    $query->andFilterWhere(['<>', 'OR1.OR1_INV_STATUS', self::ORDER_STATUS_PICK_TICKET]);
                }
            }

	        if(isset($this->OR1_STATUS_TEXT) && $this->OR1_STATUS_TEXT != '') {
		        $query->andFilterWhere(['IN', $OR1_STATUS_TEXT_Exp, explode(',', $this->OR1_STATUS_TEXT)]);
	        }
        }

        if(isset($this->COMPLETED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(OR1.OR1_COMPLETED_ON, "%m-%d-%Y")', $this->COMPLETED_ON]);
        }

        if(isset($this->OR1_TOTAL_VALUE)) {
            $query->andFilterWhere(['like', 'OR1.OR1_TOTAL_VALUE', $this->OR1_TOTAL_VALUE]);
        }

        if(isset($this->VD1_NAME)) {
            $query->andFilterWhere(['=', 'VD1.VD1_ID', $this->VD1_NAME]);
        }

        if(isset($this->TE1_ID)) {
            $query->andFilterWhere(['like', 'TE1.TE1_ID', $this->TE1_ID]);
        }

        if(isset($this->TE1_NAME)) {
            $query->andFilterWhere(['=', 'TE1.TE1_ID', $this->TE1_NAME]);
        }

        if(isset($this->ORDER_FROM_LOCATION_NAME)) {
            $query->andFilterWhere(['=', 'OR1.LO1_FROM_ID', $this->ORDER_FROM_LOCATION_NAME]);
        }

        if(isset($this->ORDER_BY_LOCATION_NAME)) {
            $query->andFilterWhere(['=', 'OR1.LO1_TO_ID', $this->ORDER_BY_LOCATION_NAME]);
        }

        if(isset($this->MODIFIED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(OR1.OR1_MODIFIED_ON, "%m-%d-%Y")', $this->MODIFIED_ON]);
        }

        if(isset($this->MODIFIED_BY)) {
            $query->andFilterWhere(['=', 'us1_modify.US1_ID', $this->MODIFIED_BY]);
        }

        if(isset($this->CREATED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(OR1.OR1_CREATED_ON, "%m-%d-%Y")', $this->CREATED_ON]);
        }

        if(isset($this->CREATED_BY)) {
            $query->andFilterWhere(['=', 'us1_create.US1_ID', $this->CREATED_BY]);
        }

	    if (isset($this->OR1_IDs)) {
		    $query->andFilterWhere(['IN', 'OR1.OR1_ID', explode(',', $this->OR1_IDs)]);
	    }

	    $query->orderBy(['OR1_NUMBER' => SORT_DESC]);

        $query->initScope();

        return $query;
    }

    public function getOrderHeader($id, $type=OrderHeader::TYPE_STORE_ORDER)
    {
        $query = $this->getQuery(null, $type);
        $query->andFilterWhere(['=', 'OR1.OR1_ID', $id]);

        return $query;
    }

	public function getGenerateAllInvoices()
	{
		$LO1_TO_ID_Exp = new Expression('OR1.LO1_TO_ID');

		$query = self::find()->alias('OR1')
			->select(['*'])
			->andFilterWhere(['<>', 'OR1.or1_inv_status', OrderHeader::ORDER_STATUS_INVOICED])
			->andFilterWhere(['=', 'OR1.or1_delete_flag', OrderHeader::ORDER_STATUS_OPEN])
			->andFilterWhere(['!=', 'OR1.LO1_FROM_ID', $LO1_TO_ID_Exp]);

		$user = Yii::$app->user->getIdentity();

		if(isset($user->CO1_ID) && $user->CO1_ID != 0) {
			$query->andFilterWhere(['=', 'OR1.CO1_ID', $user->CO1_ID]);
		}

		if(isset($user->LO1_ID) && $user->LO1_ID != 0) {
			$query->andFilterWhere(['=', 'OR1.LO1_TO_ID', $user->LO1_ID]);
		}

		return $query->asArray()->all();
	}

	public function delete()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $errors = [];
        try{
            $this->OR1_DELETE_FLAG = 1;
            $this->save(false);

            $orderLines = OrderLine::find()->where([
                "OR1_ID" => $this->OR1_ID,
                "OR2_DELETE_FLAG" => 0 ])->all();

            foreach($orderLines as $orderLine){
                $orderLine->delete();
            }

            $transaction->commit();
        }catch(Exception $e){
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }
        return [
            'success' => !$errors,
            'errors' => $errors
        ];
    }
}
