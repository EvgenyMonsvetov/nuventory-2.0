<?php

namespace app\models;

use Yii;
use DateTime;
use app\models\scopes\MasterDataQuery;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "md1_master_data".
 *
 * @property int $MD1_ID
 * @property int $MD0_ID
 * @property int $CO1_ID
 * @property int $FI1_ID
 * @property int $CA1_ID
 * @property int $CA2_ID
 * @property int $GL1_ID
 * @property string $MD1_PART_NUMBER
 * @property string $MD1_UPC1
 * @property string $MD1_UPC2
 * @property string $MD1_UPC3
 * @property string $MD1_DESC1
 * @property string $MD1_DESC2
 * @property string $MD1_UNIT_PRICE
 * @property int $UM1_PRICING_ID
 * @property int $VD1_ID
 * @property int $MD1_DELETE_FLAG
 * @property int $MD1_CREATED_BY
 * @property string $MD1_CREATED_ON
 * @property int $MD1_MODIFIED_BY
 * @property string $MD1_MODIFIED_ON
 * @property string $MD1_IMAGE
 * @property string $CA1_NAME
 * @property string $VD1_NAME
 * @property string $MD1_UNIT
 * @property int $MD1_PURCHASE_UNIT_SIZE
 * @property int $MD1_CENTRAL_ORDER_FLAG
 * @property string $MD1_VENDOR_PART_NUMBER
 * @property string $MD1_OEM_NUMBER
 * @property double $MD1_MARKUP
 * @property string $MD1_URL
 * @property string $MD1_SAFETY_URL
 * @property int $MD1_VOC_FLAG
 * @property string $MD1_VOC_MFG
 * @property string $MD1_VOC_CATA
 * @property string $MD1_VOC_VALUE
 * @property int $MD1_VOC_UNIT
 * @property int $MD1_FAVORITE
 * @property int $MD1_TYPE
 * @property int $MD1_BODY
 * @property int $MD1_PAINT
 * @property int $MD1_DETAIL
 * @property int $UM1_DEFAULT_PRICING
 * @property int $UM1_DEFAULT_PURCHASE
 * @property int $UM1_DEFAULT_RECEIVE
 * @property double $UM1_DEFAULT_PURCHASE_FACTOR
 * @property double $UM1_DEFAULT_RECEIVE_FACTOR
 * @property int $MD0_MANUFACTURER
 * @property int $MA0_ID
 * @property int $C00_ID
 */
class MasterData extends ActiveRecord
{

    protected $_tablePrefix = 'MD1';

    public $FI1_FILE_PATH;
    public $LO1_ID;
    public $LO1_FROM_ID;
    public $LO1_TO_ID;
    public $LINKED_PART;
    public $MD1_IDs;
    public $MD1_IMAGE;
    public $CHECKOUT_SHEET;
    public $MD2_ACTIVE;
    public $MD2_PRINT_FLAG;
    public $MD2_STOCK_LEVEL;
    public $MD2_ON_HAND_QTY;
    public $MD2_AVAILABLE_QTY;
    public $MD2_MIN_QTY;
    public $MD2_RACK;
    public $UM1_NAME;
    public $C00_NAME;
    public $GL1_NAME;
    public $activeFilter;
    public $isCentral;
    public $VD0_IDs;
    public $QTY;
    public $LO1_NAME;
    public $QTY_DIFF;
    public $PART_STATUS;
    public $UM1_PURCHASE_NAME;
    public $UM2_FACTOR;
    public $MD2_UNIT_PRICE;
    public $EXTENDED_PRICE;
    public $EXTENDED_STOCK;
    public $limitToMinQty;
    public $UM1_PRICE_NAME;
    public $MD1_UM1_PRICE_NAME;
	public $is_edit_inventory;

    CONST AUTO_ORDER_PO = 1;
    CONST AUTO_ORDER_SO = 2;

    const SCENARIO_STANDARD_EDIT = 'STANDARD_EDIT';

    /**
     * {@inheritdoc}
     * @return MasterDataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MasterDataQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'md1_master_data';
    }

    public static function primaryKey()
    {
        return ['MD1_ID'];
    }

    public function fields() {
            $fields = parent::fields();
            $fields["FI1_FILE_PATH"] = function($model){ return $model->FI1_FILE_PATH; };
            $fields["LO1_ID"] = function($model){ return $model->LO1_ID; };
            $fields["LO1_FROM_ID"] = function($model){ return $model->LO1_FROM_ID; };
            $fields["LO1_TO_ID"] = function($model){ return $model->LO1_TO_ID; };
            $fields["LINKED_PART"] = function($model){ return $model->LINKED_PART; };
            $fields["MD1_IDs"] = function($model){ return $model->MD1_IDs; };
            $fields["MD1_IMAGE"] = function($model){ return $model->MD1_IDs; };
            $fields["CHECKOUT_SHEET"] = function($model){ return $model->CHECKOUT_SHEET; };
            $fields["MD2_ACTIVE"] = function($model){ return $model->MD2_ACTIVE; };
            $fields["MD2_PRINT_FLAG"] = function($model){ return $model->MD2_PRINT_FLAG; };
            $fields["MD2_STOCK_LEVEL"] = function($model){ return $model->MD2_STOCK_LEVEL; };
            $fields["MD2_ON_HAND_QTY"] = function($model){ return $model->MD2_ON_HAND_QTY; };
            $fields["MD2_AVAILABLE_QTY"] = function($model){ return $model->MD2_AVAILABLE_QTY; };
            $fields["MD2_MIN_QTY"] = function($model){ return $model->MD2_MIN_QTY; };
            $fields["MD2_RACK"] = function($model){ return $model->MD2_RACK; };
            $fields["UM1_NAME"] = function($model){ return $model->UM1_NAME; };
            $fields["C00_NAME"] = function($model){ return $model->C00_NAME; };
            $fields["GL1_NAME"] = function($model){ return $model->GL1_NAME; };
            $fields["activeFilter"] = function($model){ return $model->activeFilter; };
            $fields["isCentral"] = function($model){ return $model->isCentral; };
            $fields["VD0_IDs"] = function($model){ return $model->VD0_IDs; };
            $fields["QTY"] = function($model){ return $model->QTY; };
            $fields["LO1_NAME"] = function($model){ return $model->LO1_NAME; };
            $fields["QTY_DIFF"] = function($model){ return $model->QTY_DIFF; };
            $fields["PART_STATUS"] = function($model){ return $model->PART_STATUS; };
            $fields["UM1_PURCHASE_NAME"] = function($model){ return $model->UM1_PURCHASE_NAME; };
            $fields["UM2_FACTOR"] = function($model){ return $model->UM2_FACTOR; };
            $fields["MD2_UNIT_PRICE"] = function($model){ return $model->MD2_UNIT_PRICE; };
            $fields["EXTENDED_PRICE"] = function($model){ return $model->EXTENDED_PRICE; };
            $fields["EXTENDED_STOCK"] = function($model){ return $model->EXTENDED_STOCK; };
            $fields["limitToMinQty"] = function($model){ return $model->limitToMinQty; };
            $fields["UM1_PRICE_NAME"] = function($model){ return $model->UM1_PRICE_NAME; };
            return $fields;
        }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MD1_ID', 'MD0_ID', 'CO1_ID', 'LO1_ID', 'FI1_ID', 'CA1_ID', 'CA2_ID', 'GL1_ID', 'VD1_ID', 'MD1_DELETE_FLAG', 'MD1_CREATED_BY', 'MD1_MODIFIED_BY', 'MD1_PURCHASE_UNIT_SIZE', 'MD1_CENTRAL_ORDER_FLAG', 'MD1_VOC_FLAG', 'MD1_VOC_UNIT', 'MD1_FAVORITE', 'MD1_TYPE', 'MD1_BODY', 'MD1_PAINT', 'MD1_DETAIL', 'UM1_DEFAULT_PRICING', 'UM1_DEFAULT_PURCHASE', 'UM1_DEFAULT_RECEIVE', 'MD0_MANUFACTURER', 'MA0_ID', 'C00_ID'], 'integer'],
            [['MD1_PART_NUMBER'], 'required'],
            [['UM1_PRICING_ID'], 'validatePricingUM', 'skipOnEmpty' => false, 'on' => [self::SCENARIO_STANDARD_EDIT]],
            [['MD1_VENDOR_PART_NUMBER'], 'validateVendorPartNumber', 'skipOnEmpty' => true],
            [['MD1_UNIT_PRICE', 'MD1_MARKUP', 'MD1_VOC_VALUE', 'UM1_DEFAULT_PURCHASE_FACTOR', 'UM1_DEFAULT_RECEIVE_FACTOR'], 'number'],
            [['MD1_CREATED_ON', 'MD1_MODIFIED_ON'], 'safe'],
            [['MD1_URL', 'MD1_SAFETY_URL', 'MD1_IDs'], 'string'],
            [['MD1_PART_NUMBER', 'CA1_NAME', 'VD1_NAME', 'MD1_VENDOR_PART_NUMBER', 'MD1_IMAGE', 'LO1_NAME', 'UM1_PRICE_NAME'], 'string', 'max' => 100],
            [['MD1_UPC1', 'MD1_UPC2', 'MD1_UPC3'], 'string', 'max' => 30],
            [['MD1_DESC1', 'MD1_DESC2'], 'string', 'max' => 200],
            [['MD1_IMAGE', 'MD1_OEM_NUMBER', 'FI1_FILE_PATH', 'UM1_NAME', 'C00_NAME', 'GL1_NAME', 'UM1_PURCHASE_NAME'], 'string', 'max' => 255],
            [['MD1_UNIT'], 'string', 'max' => 50],
            [['MD1_VOC_MFG', 'MD1_VOC_CATA', 'MD2_STOCK_LEVEL'], 'string', 'max' => 45],
            [['CO1_ID', 'MD1_PART_NUMBER', 'MD1_DELETE_FLAG'], 'unique', 'targetAttribute' => ['CO1_ID', 'MD1_PART_NUMBER', 'MD1_DELETE_FLAG']],
        ];
    }

	public function beforeSave($insert)
	{
		if (empty($this->is_edit_inventory)) {
			$this->MD1_MODIFIED_ON = new Expression('UTC_TIMESTAMP()');
			$this->MD1_MODIFIED_BY = Yii::$app->user->getId();
		}
		return parent::beforeSave($insert);
	}

	public function behaviors()
	{
		if(Yii::$app instanceof \yii\console\Application){
			return [];
		}

		$updatedAtAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->updatedAtAttribute;
		$createdAtAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->createdAtAttribute;

		$updatedByAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->updatedByAttribute;
		$createdByAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->createdByAttribute;

		$deletedAtAttribute = ($this->useAttributeFullName ? '' : $this->_tablePrefix) . $this->deletedAtAttribute;

		return [
			'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						$updatedAtAttribute,
						$createdAtAttribute
					],
				],
				'value' => new Expression('UTC_TIMESTAMP()')
			], [
				'class' => 'yii\behaviors\AttributeBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						$updatedByAttribute,
						$createdByAttribute
					],
				],
				'value' => Yii::$app->user->getId()
			], [
				'class' => 'yii\behaviors\AttributeBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => [
						$deletedAtAttribute
					],
				],
				'value' => 0
			]
		];
	}

    public function validatePricingUM($attribute, $params)
    {
        if(empty($this->$attribute) || $this->$attribute == 0 || $this->$attribute == '0'){
            $this->addError($attribute, 'The Pricing UM must be specified.');
        }
    }

	public function validateVendorPartNumber($attribute, $params)
    {
        if (!empty($this->$attribute)) {
	        $models = self::findAll(['MD1_VENDOR_PART_NUMBER' => $this->$attribute, 'CO1_ID' => $this->CO1_ID, 'MD1_DELETE_FLAG' => 0]);
	        if (count($models) > 1) {
		        $this->addError($attribute, 'The Vendor Part Number must be unique.');
	        } elseif (count($models) == 1) {
	        	if ($models[0]->MD1_ID != $this->MD1_ID) {
			        $this->addError($attribute, 'The Vendor Part Number must be unique.');
		        }
	        }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MD1_ID' => 'Md1  ID',
            'MD0_ID' => 'Md0  ID',
            'CO1_ID' => 'Co1  ID',
            'FI1_ID' => 'Fi1  ID',
            'CA1_ID' => 'Ca1  ID',
            'CA2_ID' => 'Ca2  ID',
            'GL1_ID' => 'Gl1  ID',
            'MD1_PART_NUMBER' => 'Md1  Part  Number',
            'MD1_UPC1' => 'Md1  Upc1',
            'MD1_UPC2' => 'Md1  Upc2',
            'MD1_UPC3' => 'Md1  Upc3',
            'MD1_DESC1' => 'Md1  Desc1',
            'MD1_DESC2' => 'Md1  Desc2',
            'MD1_UNIT_PRICE' => 'Md1  Unit  Price',
            'UM1_PRICING_ID' => 'Um1  Pricing  ID',
            'VD1_ID' => 'Vd1  ID',
            'MD1_DELETE_FLAG' => 'Md1  Delete  Flag',
            'MD1_CREATED_BY' => 'Md1  Created  By',
            'MD1_CREATED_ON' => 'Md1  Created  On',
            'MD1_MODIFIED_BY' => 'Md1  Modified  By',
            'MD1_MODIFIED_ON' => 'Md1  Modified  On',
            'MD1_IMAGE' => 'Md1  Image',
            'CA1_NAME' => 'Ca1  Name',
            'VD1_NAME' => 'Vd1  Name',
            'MD1_UNIT' => 'Md1  Unit',
            'MD1_PURCHASE_UNIT_SIZE' => 'Md1  Purchase  Unit  Size',
            'MD1_CENTRAL_ORDER_FLAG' => 'Md1  Central  Order  Flag',
            'MD1_VENDOR_PART_NUMBER' => 'Md1  Vendor  Part  Number',
            'MD1_OEM_NUMBER' => 'Md1  Oem  Number',
            'MD1_MARKUP' => 'Md1  Markup',
            'MD1_URL' => 'Md1  Url',
            'MD1_SAFETY_URL' => 'Md1  Safety  Url',
            'MD1_VOC_FLAG' => 'Md1  Voc  Flag',
            'MD1_VOC_MFG' => 'Md1  Voc  Mfg',
            'MD1_VOC_CATA' => 'Md1  Voc  Cata',
            'MD1_VOC_VALUE' => 'Md1  Voc  Value',
            'MD1_VOC_UNIT' => 'Md1  Voc  Unit',
            'MD1_FAVORITE' => 'Md1  Favorite',
            'MD1_TYPE' => 'Md1  Type',
            'MD1_BODY' => 'Md1  Body',
            'MD1_PAINT' => 'Md1  Paint',
            'MD1_DETAIL' => 'Md1  Detail',
            'UM1_DEFAULT_PRICING' => 'Um1  Default  Pricing',
            'UM1_DEFAULT_PURCHASE' => 'Um1  Default  Purchase',
            'UM1_DEFAULT_RECEIVE' => 'Um1  Default  Receive',
            'UM1_DEFAULT_PURCHASE_FACTOR' => 'Um1  Default  Purchase  Factor',
            'UM1_DEFAULT_RECEIVE_FACTOR' => 'Um1  Default  Receive  Factor',
            'MD0_MANUFACTURER' => 'Md0  Manufacturer',
            'MA0_ID' => 'Ma0  ID',
            'C00_ID' => 'C00  ID',
        ];
    }

    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery( $params )
    {
        $user = Yii::$app->user->getIdentity();

        $LO1_ID = $user->LO1_ID;
        if( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN && !empty($params['LO1_ID'])){
            $LO1_ID = $params['LO1_ID'];
        }

	    $MD2_STOCK_LEVEL_Exp = new Expression('(if(md2_to.MD2_ACTIVE=0 OR (coalesce(md2_to.MD2_ON_HAND_QTY,0)=0 AND coalesce(md2_to.MD2_MIN_QTY,0)=0), "0", if(coalesce(md2_to.MD2_ON_HAND_QTY,0)<coalesce(md2_to.MD2_MIN_QTY,0), "3", if((coalesce(md2_to.MD2_ON_HAND_QTY,0)>coalesce(md2_to.MD2_MIN_QTY,0)), "2", "1"))))');

        $fields = [
            "md1.MD1_ID", "md1.MD1_IMAGE", "md1.FI1_ID", "md1.MD1_FAVORITE",
            "md1.MD1_PART_NUMBER", "md1.MD1_VENDOR_PART_NUMBER",
            "md1.MD1_OEM_NUMBER", "md1.MD1_DESC1", "md1.CA1_ID",
            "md1.MD0_ID", "md1.CO1_ID", "md1.GL1_ID", "md1.C00_ID", "md1.UM1_PRICING_ID",
            "md0.MD0_UNIT_PRICE", "md0.MD0_PART_NUMBER", "md0.MD0_DESC1",
            "md0.MD0_UPC1", "md0.MD0_VENDOR_PART_NUMBER", "md0.MD0_DISCONTINUED",
            "MD1_BODY",
            "MD1_DETAIL",
            "MD1_PAINT", "MD1_SAFETY_URL",
            "MD1_VOC_CATA", "MD1_VOC_FLAG", "MD1_VOC_MFG", "MD1_VOC_UNIT", "MD1_VOC_VALUE",
            "md1.MD1_UPC1", "fi1.FI1_FILE_PATH",
            "co1.CO1_SHORT_CODE", "co1.CO1_NAME",
            "NO_JOB_INVENTORY" => "ca1.CA1_NO_INVENTORY",
            "md1.MD1_MARKUP",
            "UM1_PRICE_SIZE" => "um1_price.UM1_SIZE",
            "UM1_PRICE_ID" => "um1_price.UM1_ID",
            "MD1_CENTRAL_ORDER_FLAG" => "coalesce(md1.MD1_CENTRAL_ORDER_FLAG, 0)",
            "MD1_UM1_PRICE_NAME" => "um1_price.UM1_NAME",
            "C00_NAME" => "c00.C00_NAME",
            "ca1.CA1_NAME", "gl1.GL1_NAME",
            'MD1_CREATED_BY_NAME'  => 'us1_create.US1_NAME',
            'MD1_MODIFIED_BY_NAME' => 'us1_modify.US1_NAME',
	        'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('MD1_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
	        'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('MD1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
            "md2_to.VD1_ID",
            "md2_to.MD2_ID",
            "md2_to.MD2_ACTIVE",
            "md2_to.MD2_PRINT_FLAG",
            "md2_to.MD2_ON_HAND_QTY",
            "md2_to.MD2_AVAILABLE_QTY",
            "md2_to.MD2_MIN_QTY",
            "md2_to.MD2_MAX_QTY",
            "md2_to.MD2_RACK",
            "md2_to.MD2_DRAWER",
            "md2_to.MD2_BIN",
            "lo1.LO1_RACKS",
            "md2_to.MD2_PARTIAL",
            "NO_INVENTORY" => "md2_to.MD2_NO_INVENTORY",
            "MD2_STOCK_LEVEL" => $MD2_STOCK_LEVEL_Exp,
            "vd1.VD1_SHORT_CODE", "vd1.VD1_NAME", "vd1.VD1_DISCOUNT",
            "UM1_PURCHASE_ID"    => "um1_pur.UM1_ID",
            "UM1_PURCHASE_NAME"  => "um1_pur.UM1_NAME",
            "UM1_RECEIPT_ID"    => "um1_rec.UM1_ID",
            "UM1_RECEIPT_NAME"  => "um1_rec.UM1_NAME",
            "MD1_ORIGINAL_UNIT_PRICE" => "MD1_UNIT_PRICE",
            "MD1_UNIT_PRICE" => "md1.MD1_UNIT_PRICE/coalesce(um2_price.um2_factor,1)",
            "MD1_REC_PRICE"  => "md1.MD1_UNIT_PRICE/coalesce(um2_rec.um2_factor,1)",
            "MD1_SELL_PRICE" => "md1.MD1_UNIT_PRICE*(1+COALESCE(md1.MD1_MARKUP,0)*0.01)/coalesce(um2_price.um2_factor,1)",
            "um2_rec.UM2_FACTOR",
            "vd0.VD0_SHORT_CODE", "vd0.VD0_NAME", "vd0.VD0_DISCOUNT",
            'um2_price.UM2_FACTOR AS UM2_PRICE_FACTOR',
            'um2_rec.UM2_FACTOR AS UM2_RECEIPT_FACTOR',
        ];

        $queryBelowMin = (isset($params['belowMinimumReport']) && $params['belowMinimumReport'])?' AND md2_to.MD2_ON_HAND_QTY < md2_to.MD2_MIN_QTY':'';

        $query = self::find()->alias('md1')->select($fields)
            ->leftJoin("md0_master_data as md0" ,    "md1.MD0_ID=md0.MD0_ID AND md0.MD0_DELETE_FLAG=0")
            ->leftJoin("fi1_file as fi1" ,           "fi1.FI1_ID=md1.FI1_ID AND fi1.FI1_DELETE_FLAG=0")
            ->leftJoin("c00_category as c00" ,       "c00.C00_ID=md1.C00_ID AND c00.C00_DELETE_FLAG=0")
            ->leftJoin("md2_location as md2_to" ,    "md2_to.MD1_ID=md1.MD1_ID AND md2_to.MD2_DELETE_FLAG=0 {$queryBelowMin}")
            ->leftJoin("lo1_location as lo1" ,       "lo1.LO1_ID=md2_to.LO1_ID AND lo1.LO1_DELETE_FLAG=0")
            ->leftJoin("co1_company as co1" ,        "co1.CO1_ID=md1.CO1_ID AND co1.CO1_DELETE_FLAG=0")
            ->leftJoin("gl1_glcode as gl1" ,         "gl1.GL1_ID=md1.GL1_ID AND gl1.GL1_DELETE_FLAG=0")
            ->leftJoin("vd1_vendor as vd1",          "vd1.VD1_ID=md2_to.VD1_ID AND vd1.VD1_DELETE_FLAG=0")
            ->leftJoin("ca1_category as ca1" ,       "ca1.CA1_ID=md1.CA1_ID AND ca1.CA1_DELETE_FLAG=0")
            ->leftJoin("um1_unit as um1_price",      "um1_price.UM1_ID=md1.UM1_PRICING_ID AND um1_price.UM1_DELETE_FLAG=0")
            ->leftJoin("um1_unit as um1_pur" ,       "um1_pur.UM1_ID=md2_to.UM1_PURCHASE_ID AND um1_pur.UM1_DELETE_FLAG=0")
            ->leftJoin("um1_unit as um1_rec" ,       "um1_rec.UM1_ID=md2_to.UM1_RECEIPT_ID AND um1_rec.UM1_DELETE_FLAG=0")
            ->leftJoin("um2_conversion AS um2_rec",  "um2_rec.MD1_ID = MD1.MD1_ID AND um2_rec.UM1_FROM_ID = md2_to.UM1_PURCHASE_ID AND um2_rec.UM1_TO_ID = md2_to.UM1_RECEIPT_ID AND um2_rec.UM2_DELETE_FLAG=0")
            ->leftJoin("us1_user as us1_create" ,    "us1_create.US1_ID = MD1.MD1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" ,    "us1_modify.US1_ID = MD1.MD1_MODIFIED_BY");

        if(isset($params['editEnventory']) && $params['editEnventory']){
            $query
                ->leftJoin("vd0_vendor as vd0","vd0.VD0_ID=MD0.VD0_ID AND vd0.VD0_DELETE_FLAG=0")
                ->leftJoin("um2_conversion as um2_price",
                    "um2_price.MD1_ID=md1.MD1_ID AND um2_price.UM1_TO_ID=md1.UM1_PRICING_ID AND um2_price.UM1_FROM_ID=md1.UM1_PRICING_ID AND um2_price.UM2_DELETE_FLAG=0");
        }else{
            $query
                ->leftJoin("vd0_vendor as vd0" ,         "vd0.VD0_ID=vd1.VD0_ID AND vd0.VD0_DELETE_FLAG=0")
                ->leftJoin("um2_conversion as um2_price","um2_price.MD1_ID=md1.MD1_ID AND um2_price.UM1_TO_ID=md2_to.UM1_RECEIPT_ID AND um2_price.UM1_FROM_ID=md1.UM1_PRICING_ID AND um2_price.UM2_DELETE_FLAG=0");
        }

        $query->initScope();
        $this->load($params, '');

	    if ($LO1_ID && $LO1_ID !== 0) {
		    $query->andFilterWhere(['=', 'md2_to.LO1_ID', $LO1_ID]);
	    }

        if (isset($params['MD2_ACTIVE'])) {
		    $query->andFilterWhere(['=', 'md2_to.MD2_ACTIVE', $params['MD2_ACTIVE']]);
	    }

        if (isset($this->MD1_IDs)) {
		    $query->andFilterWhere(['IN', 'MD1.MD1_ID', explode(',', $this->MD1_IDs)]);
	    }

        if (isset($params['MD2_IDs'])) {
		    $query->andFilterWhere(['IN', 'md2_to.MD2_ID', explode(',', $params['MD2_IDs'])]);
	    }

        $query->andFilterWhere(['=',    'md1.MD1_ID',          $this->MD1_ID]);
        $query->andFilterWhere(['like', 'md1.MD1_PART_NUMBER', $this->MD1_PART_NUMBER]);

	    if (isset($params['VD1_NAME'])) {
		    $query->andFilterWhere(['=', 'md2_to.VD1_ID', $params['VD1_NAME']]);
	    }

	    if (isset($params['MD2_PRINT_FLAG'])) {
		    $query->andFilterWhere(['=', 'md2_to.MD2_PRINT_FLAG', $params['MD2_PRINT_FLAG']]);
	    }

	    if (isset($params['MD1_PART_NUMBER'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_PART_NUMBER', $params['MD1_PART_NUMBER']]);
	    }

	    if (isset($params['MD1_VENDOR_PART_NUMBER'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_VENDOR_PART_NUMBER', $params['MD1_VENDOR_PART_NUMBER']]);
	    }

	    if (isset($params['MD1_OEM_NUMBER'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_OEM_NUMBER', $params['MD1_OEM_NUMBER']]);
	    }

        if (isset($params['MD1_DESC1'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_DESC1', $params['MD1_DESC1']]);
	    }

        if (isset($params['MD1_UNIT_PRICE'])) {
		    $query->andFilterWhere(['like', 'MD1_UNIT_PRICE', $params['MD1_UNIT_PRICE']]);
	    }

	    if (isset($params['MD2_STOCK_LEVEL']) && $params['MD2_STOCK_LEVEL'] != '') {
	        $query->andFilterWhere(['=', $MD2_STOCK_LEVEL_Exp, $params['MD2_STOCK_LEVEL']]);
	    }

	    if (isset($params['MD2_ON_HAND_QTY'])) {
		    $query->andFilterWhere(['like', 'md2_to.MD2_ON_HAND_QTY', $params['MD2_ON_HAND_QTY']]);
	    }

	    if (isset($params['MD2_AVAILABLE_QTY'])) {
		    $query->andFilterWhere(['like', 'md2_to.MD2_AVAILABLE_QTY', $params['MD2_AVAILABLE_QTY']]);
	    }

	    if (isset($params['MD2_MIN_QTY'])) {
		    $query->andFilterWhere(['like', 'md2_to.MD2_MIN_QTY', $params['MD2_MIN_QTY']]);
	    }

	    if (isset($params['MD2_RACK'])) {
		    $query->andFilterWhere(['or',
			    ['like', 'md2_to.MD2_RACK', $params['MD2_RACK']],
			    ['like', 'md2_to.MD2_DRAWER', $params['MD2_RACK']],
			    ['like', 'md2_to.MD2_BIN', $params['MD2_RACK']]
		    ]);
	    }

	    if (isset($params['UM1_PRICE_NAME'])) {
		    $query->andFilterWhere(['=', 'um1_rec.UM1_ID', $params['UM1_PRICE_NAME']]);
	    }

	    if (isset($params['C00_NAME'])) {
		    $query->andFilterWhere(['=', 'md1.C00_ID', $params['C00_NAME']]);
	    }

	    if (isset($params['CA1_NAME'])) {
		    $query->andFilterWhere(['=', 'md1.CA1_ID', $params['CA1_NAME']]);
	    }

	    if (isset($params['GL1_NAME'])) {
		    $query->andFilterWhere(['like', 'gl1.GL1_NAME', $params['GL1_NAME']]);
	    }

	    if (isset($params['MD1_UPC1'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_UPC1', $params['MD1_UPC1']]);
	    }

	    if (isset($params['MD1_BODY'])) {
		    $query->andFilterWhere(['=', 'MD1_BODY', $params['MD1_BODY']]);
	    }

	    if (isset($params['MD1_DETAIL'])) {
		    $query->andFilterWhere(['=', 'MD1_DETAIL', $params['MD1_DETAIL']]);
	    }

	    if (isset($params['MD1_PAINT'])) {
		    $query->andFilterWhere(['=', 'MD1_PAINT', $params['MD1_PAINT']]);
	    }

	    if (isset($params['MASTER_SEARCH'])) {
		    $query->andFilterWhere(['or',
			    ['like', 'vd1.VD1_NAME', $params['MASTER_SEARCH']],
			    ['like', 'md1.MD1_PART_NUMBER', $params['MASTER_SEARCH']],
			    ['like', 'md1.MD1_VENDOR_PART_NUMBER', $params['MASTER_SEARCH']],
			    ['like', 'md1.MD1_OEM_NUMBER', $params['MASTER_SEARCH']],
			    ['like', 'md1.MD1_DESC1', $params['MASTER_SEARCH']],
			    ['like', 'MD1_UNIT_PRICE', $params['MASTER_SEARCH']],
			    ['like', 'md2_to.MD2_ON_HAND_QTY', $params['MASTER_SEARCH']],
			    ['like', 'md2_to.MD2_AVAILABLE_QTY', $params['MASTER_SEARCH']],
			    ['like', 'md2_to.MD2_MIN_QTY', $params['MASTER_SEARCH']],
			    ['like', 'md2_to.MD2_RACK', $params['MASTER_SEARCH']],
			    ['like', 'md2_to.MD2_DRAWER', $params['MASTER_SEARCH']],
			    ['like', 'md2_to.MD2_BIN', $params['MASTER_SEARCH']],
			    ['like', 'um1_rec.UM1_NAME', $params['MASTER_SEARCH']],
			    ['like', 'C00_NAME', $params['MASTER_SEARCH']],
			    ['like', 'ca1.CA1_NAME', $params['MASTER_SEARCH']],
			    ['like', 'gl1.GL1_NAME', $params['MASTER_SEARCH']],
			    ['like', 'md1.MD1_UPC1', $params['MASTER_SEARCH']]

		    ]);
	    }

	    if (isset($params['SORT_COLUMN'])) {
	    	$sort = $params['SORT_DIRECTION'] == 'ASC' ? SORT_ASC : SORT_DESC;
		    $query->orderBy([$params['SORT_COLUMN'] => $sort]);
	    } else {
		    $query->orderBy(['CO1_NAME' => SORT_ASC, 'VD1_NAME' => SORT_ASC, 'MD1_PART_NUMBER' => SORT_ASC, ]);
	    }

        return $query;
    }

    public function getPartFromAllLocations( $MD1_ID, $CO1_ID )
    {
        $query = self::find()->alias('LO1')->select([
                'LO1.*',
                'md2.*',
                'um2.UM2_FACTOR',
                'UM2_PRICE_FACTOR' => 'um2_price_factor.UM2_FACTOR'
            ])
            ->leftJoin("md2_location   as md2" ,               "md2.LO1_ID = lo1.LO1_ID AND md2.MD1_ID = " . $MD1_ID . " AND md2.MD2_DELETE_FLAG = 0 ")
            ->leftJoin("um2_conversion as um2" ,               "um2.UM1_FROM_ID = md2.UM1_PURCHASE_ID AND um2.UM1_TO_ID = UM1_RECEIPT_ID AND um2.MD1_ID = md2.MD1_ID AND um2.UM2_DELETE_FLAG = 0")
            ->leftJoin("um2_conversion as um2_price_factor" ,  "um2.UM1_FROM_ID = '7' AND um2_price_factor.UM1_TO_ID = UM1_PURCHASE_ID AND um2_price_factor.MD1_ID = md2.MD1_ID AND um2_price_factor.UM2_DELETE_FLAG = 0");

        $query->andFilterWhere(['=', 'LO1.LO1_DELETE_FLAG', '0']);
        $query->andFilterWhere(['=', 'LO1.CO1_ID',   $CO1_ID]);

        $query->orderBy(['LO1.LO1_CENTER_FLAG' => SORT_DESC, 'LO1.LO1_NAME' => SORT_ASC]);
        return $query->all();
    }

    public function getQuickAddParts($params) {

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        $user = Yii::$app->user->getIdentity();
        $query = self::find()->alias('MD1')
                ->select([
                    'MD1.MD1_ID',
                    'MD1.MD1_PART_NUMBER',
                    'MD1.MD1_VENDOR_PART_NUMBER',
                    'MD1.MD1_UPC1',
                    'MD1.MD1_DESC1'])
                ->leftJoin("md2_location as MD2_FROM" ,
                    "MD2_FROM.MD1_ID = MD1.MD1_ID AND 
                         MD2_FROM.MD2_DELETE_FLAG = 0 AND 
                         MD2_FROM.LO1_ID =" . $user->LO1_ID );

        $query->andFilterWhere(['=',    'MD1.CO1_ID', $user->CO1_ID]);
        $query->andFilterWhere(['=',    'MD1.MD1_DELETE_FLAG',        0]);
        $query->andFilterWhere(['like', 'MD1.MD1_PART_NUMBER', $this->MD1_PART_NUMBER]);

	    if(isset($params['MD1_CENTRAL_ORDER_FLAG'])){
		    $query->andFilterWhere(['=', 'coalesce(MD1.MD1_CENTRAL_ORDER_FLAG, 0)', $params['MD1_CENTRAL_ORDER_FLAG']]);
	    }

        $query->orderBy(['MD1_PART_NUMBER' => SORT_ASC]);

        return $query->all();
    }

    public function search($params, $autoOrder=false)
    {
        $query = $this->getSearchQuery( $params, $autoOrder );

        $pagination = ($params['perPage'])?['pageSize' => $params['perPage'],'page' => $params['page']]:false;

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => $pagination
        ]);

        return $dataProvider;
    }

    /**
     * @param array $filters
     * @param integer $autoOrder: 1 - PO, 2 - SO
     * @return MasterDataQuery
     * @throws \Throwable
     */
    public function getNewOrderParts( $filters=[], $autoOrder=0) {

        $fields= [
            'MD1.MD1_ID',
            'MD1.MD1_IMAGE',
            'MD1.MD0_ID',
            'MD1.CO1_ID',
            'MD1.FI1_ID',
            'MD1.MD1_UNIT_PRICE as MD1_ORIGINAL_UNIT_PRICE',
            'MD1.MD1_UNIT_PRICE/coalesce(um2_price.um2_factor,1) AS MD1_UNIT_PRICE',
            'MD1.MD1_UNIT_PRICE*(1+COALESCE(MD1.MD1_MARKUP,0)*0.01)/coalesce(um2_price.um2_factor,1) AS MD1_SELL_PRICE',
            'MD1.MD1_UNIT_PRICE/coalesce(um2_rec.um2_factor,1) AS MD1_REC_PRICE',
            'MD1.MD1_MARKUP',
            'MD1.MD1_FAVORITE',
            'MD1.MD1_PART_NUMBER',
            'MD1.MD1_VENDOR_PART_NUMBER',
            'MD1.MD1_OEM_NUMBER',
            'MD1.MD1_DESC1',
            'MD1.MD1_DESC2',
            'MD1.C00_ID', 'C00.C00_NAME',
            'MD1.CA1_ID', 'CA1.CA1_NO_INVENTORY AS NO_JOB_INVENTORY', 'CA1.CA1_NAME',
            'MD1.GL1_ID', 'GL1.GL1_NAME',
            'MD1.MD1_UPC1',
            'MD1.MD1_BODY',
            'MD1.MD1_DETAIL',
            'MD1.MD1_PAINT',

            'CO1.CO1_SHORT_CODE',
            'CO1.CO1_NAME',

            'VD1.VD1_ID',
            'VD1.VD1_NAME',
            'VD1.VD1_SHORT_CODE',
            'VD1.VD1_NO_INVENTORY',

            'fi1.FI1_FILE_PATH',

            'um1_price.UM1_ID AS UM1_PRICE_ID',
            'um1_price.UM1_SIZE AS UM1_PRICE_SIZE',

            'um1_pur.UM1_ID AS UM1_PURCHASE_ID',
            'um1_pur.UM1_NAME AS UM1_PURCHASE_NAME',

            'um1_rec.UM1_ID AS UM1_RECEIPT_ID',
            'um1_rec.UM1_NAME AS UM1_RECEIPT_NAME',
            "um1_rec.UM1_NAME AS UM1_PRICE_NAME",
            "MD1_UM1_PRICE_NAME" => "um1_price.UM1_NAME",

            'um2_price.UM2_FACTOR AS UM2_PRICE_FACTOR',
            'um2_rec.UM2_FACTOR AS UM2_RECEIPT_FACTOR',

            'MODIFIED_ON' => 'DATE_FORMAT(MD1_MODIFIED_ON, "%m.%d.%Y %H:%i")',
            'CREATED_ON'  => 'DATE_FORMAT(MD1_CREATED_ON, "%m.%d.%Y %H:%i")',
	        'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('MD1_MODIFIED_ON', $filters) . ', "%m.%d.%Y %H:%i")',
	        'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('MD1_CREATED_ON', $filters) . ', "%m.%d.%Y %H:%i")',
            'MD1.MD1_MODIFIED_BY AS MD1_MODIFIED_BY',
            'us1_mod.US1_NAME AS MODIFIED_BY',
            'MD1.MD1_CREATED_BY AS MD1_CREATED_BY',
            'us2_create.US1_NAME AS CREATED_BY'
        ];

        $LO1_FROM_ID = (isset($filters['LO1_FROM_ID']) && $filters['LO1_FROM_ID']) ?  $filters['LO1_FROM_ID'] : null;
        $LO1_TO_ID   = (isset($filters['LO1_TO_ID'])   && $filters['LO1_TO_ID'])   ?  $filters['LO1_TO_ID']   : null;

	    $MD2_STOCK_LEVEL_Exp_from = new Expression('
	        (if(md2_from.MD2_ACTIVE=0 OR 
	            (coalesce(md2_from.MD2_ON_HAND_QTY,0)=0 AND coalesce(md2_from.MD2_MIN_QTY,0)=0), "0", 
	                if(coalesce(md2_from.MD2_ON_HAND_QTY,0) < coalesce(md2_from.MD2_MIN_QTY,0), "3", 
	                    if(coalesce(md2_from.MD2_ON_HAND_QTY,0) > coalesce(md2_from.MD2_MIN_QTY,0), "2", "1"))))');
	    $MD2_STOCK_LEVEL_Exp_to = new Expression("
            (if(md2_to.MD2_ACTIVE = 0 OR
                (coalesce(md2_to.MD2_ON_HAND_QTY, 0) = 0 AND coalesce(md2_to.MD2_MIN_QTY, 0) = 0), '0',
                if(coalesce(md2_to.MD2_ON_HAND_QTY, 0) < coalesce(md2_to.MD2_MIN_QTY, 0), '3',
                    if(coalesce(md2_to.MD2_ON_HAND_QTY, 0) > coalesce(md2_to.MD2_MIN_QTY, 0), '2', '1'))))");

        if ($LO1_FROM_ID) {
            $fields = array_merge( $fields, [
                'md2_to.VD1_ID',
                'md2_to.MD2_ID',
                'md2_to.MD2_ACTIVE',
                'md2_to.MD2_NO_INVENTORY AS NO_INVENTORY',
                'md2_to.MD2_PRINT_FLAG',
                'coalesce(md2_to.MD2_ON_HAND_QTY,0) AS MD2_ON_HAND_QTY_TO',
                'coalesce(md2_to.MD2_AVAILABLE_QTY,0) AS MD2_AVAILABLE_QTY_TO',
                'coalesce(md2_to.MD2_MIN_QTY,0) AS MD2_MIN_QTY_TO',
                'coalesce(md2_to.MD2_MAX_QTY,0) AS MD2_MAX_QTY_TO',
                'md2_to.MD2_RACK',
                'md2_to.MD2_DRAWER',
                'md2_to.MD2_BIN',
                'GREATEST(1,CEILING((coalesce(md2_to.MD2_MIN_QTY,0) - coalesce(md2_to.MD2_AVAILABLE_QTY,0))/coalesce(um2_rec.UM2_FACTOR,1))) AS DEFAULT_ORDER_QTY',
                'coalesce(md2_from.MD2_ON_HAND_QTY,0) AS MD2_ON_HAND_QTY',
                'coalesce(md2_from.MD2_AVAILABLE_QTY,0) AS MD2_AVAILABLE_QTY',
                'coalesce(md2_from.MD2_MIN_QTY,0) AS MD2_MIN_QTY',
                'coalesce(md2_from.MD2_MAX_QTY,0) AS MD2_MAX_QTY',
                'md2_from.MD2_NO_INVENTORY AS NO_INVENTORY_FROM',
	            "MD2_STOCK_LEVEL" => $MD2_STOCK_LEVEL_Exp_from,
                'if(md2_from.LO1_ID=md2_to.LO1_ID AND 1, um1_rec.UM1_NAME,um1_pur.UM1_NAME) AS UM1_PRICE_NAME',
            ]);
        } else {
            $fields = array_merge( $fields, [
                'md2_to.MD2_ID',
                'md2_to.MD2_ACTIVE',
                'md2_to.MD2_NO_INVENTORY AS NO_INVENTORY',
                'md2_to.MD2_PRINT_FLAG',
                'coalesce(md2_to.MD2_ON_HAND_QTY,0) AS MD2_ON_HAND_QTY',
                'coalesce(md2_to.MD2_AVAILABLE_QTY,0) AS MD2_AVAILABLE_QTY',
                'coalesce(md2_to.MD2_MIN_QTY,0) AS MD2_MIN_QTY',
                'coalesce(md2_to.MD2_MAX_QTY,0) AS MD2_MAX_QTY',
                'md2_to.MD2_RACK',
                'md2_to.MD2_DRAWER',
                'md2_to.MD2_BIN',
                'md2_to.MD2_PARTIAL',
	            "MD2_STOCK_LEVEL" => $MD2_STOCK_LEVEL_Exp_to
            ]);
        }

        $query = self::find()->alias('MD1')->select($fields)
            ->leftJoin("md0_master_data  as MD0", "MD1.MD0_ID = MD0.MD0_ID AND MD0.MD0_DELETE_FLAG=0")
            ->leftJoin("fi1_file AS fi1", "fi1.FI1_ID = MD1.FI1_ID AND fi1.FI1_DELETE_FLAG=0")
            ->leftJoin("c00_category AS C00", "C00.C00_ID = MD1.C00_ID AND C00.C00_DELETE_FLAG=0")
            ->leftJoin("md2_location AS md2_to", "md2_to.MD1_ID = MD1.MD1_ID AND md2_to.MD2_DELETE_FLAG=0")
            ->leftJoin("lo1_location AS lo1", "lo1.LO1_ID = md2_to.LO1_ID AND lo1.LO1_DELETE_FLAG=0")
            ->leftJoin("vd1_vendor AS VD1", "VD1.VD1_ID = md2_to.VD1_ID AND VD1.VD1_DELETE_FLAG=0")
            ->leftJoin("um1_unit AS um1_pur", "um1_pur.UM1_ID = md2_to.UM1_PURCHASE_ID AND um1_pur.UM1_DELETE_FLAG=0")
            ->leftJoin("um1_unit AS um1_rec", "um1_rec.UM1_ID = md2_to.UM1_RECEIPT_ID AND um1_rec.UM1_DELETE_FLAG=0")
            ->leftJoin("um2_conversion AS um2_rec", "um2_rec.MD1_ID = MD1.MD1_ID AND um2_rec.UM1_FROM_ID = md2_to.UM1_PURCHASE_ID AND um2_rec.UM1_TO_ID = md2_to.UM1_RECEIPT_ID AND um2_rec.UM2_DELETE_FLAG=0")
            ->leftJoin("co1_company AS CO1", "CO1.CO1_ID = MD1.CO1_ID AND CO1.CO1_DELETE_FLAG=0")
            ->leftJoin("gl1_glcode AS GL1", "GL1.GL1_ID = MD1.GL1_ID AND gl1.GL1_DELETE_FLAG=0")
            ->leftJoin("ca1_category AS CA1", "CA1.CA1_ID = MD1.CA1_ID AND CA1.CA1_DELETE_FLAG=0")
            ->leftJoin("vd0_vendor AS VD0", "VD0.VD0_ID = MD0.VD0_ID AND VD0.VD0_DELETE_FLAG=0")
            ->leftJoin("us1_user AS us1_mod", "us1_mod.US1_ID = MD1.MD1_MODIFIED_BY")
            ->leftJoin("us1_user AS us2_create", "us2_create.US1_ID = MD1.MD1_CREATED_BY")
            ->leftJoin("um1_unit AS um1_price", "um1_price.UM1_ID = MD1.UM1_PRICING_ID AND um1_price.UM1_DELETE_FLAG=0");

	    if ($LO1_TO_ID) {
		    $query->andFilterWhere(['=', 'md2_to.LO1_ID', $LO1_TO_ID]);
	    }

        $nonCentralFilter = !empty($filters['nonCentralFilter']) && $filters['nonCentralFilter'] ? 1 : 0;
        if( $LO1_FROM_ID ){
            $query->leftJoin("md2_location AS md2_from", "md2_from.MD1_ID = MD1.MD1_ID AND md2_from.LO1_ID=" . $LO1_FROM_ID . " AND md2_from.MD2_DELETE_FLAG=0");
            $query->leftJoin("um2_conversion AS um2_price" ,
                "um2_price.MD1_ID = md1.MD1_ID AND
                     um2_price.UM1_TO_ID =
                      if(md2_from.LO1_ID = md2_to.LO1_ID && ".($nonCentralFilter?'0':'1').", 
                            md2_to.UM1_RECEIPT_ID, 
                            md2_to.UM1_PURCHASE_ID) AND
                     um2_price.UM1_FROM_ID = md1.UM1_PRICING_ID AND
                     um2_price.UM2_DELETE_FLAG = 0" );
        }else{
            $query->leftJoin("um2_conversion AS um2_price" ,
                "um2_price.MD1_ID = md1.MD1_ID AND
                     um2_price.UM1_TO_ID = md1.UM1_PRICING_ID AND
                     um2_price.UM1_FROM_ID = md1.UM1_PRICING_ID AND
                     um2_price.UM2_DELETE_FLAG = 0" );
        }

        if( $nonCentralFilter ){
            $query->andFilterWhere(['or',
                    ['=', new Expression('coalesce(lo1.LO1_CENTER_FLAG,0)'), 1],
                    ['=', new Expression('coalesce(md1.MD1_CENTRAL_ORDER_FLAG,0)'), 0]
            ]);
        }

        $user = Yii::$app->user->getIdentity();

        if( $autoOrder ){
            if( $autoOrder == self::AUTO_ORDER_PO){
                $query->leftJoin("po1_order_header AS po1",
	                "po1.PO1_DELETE_FLAG = 0 AND po1.PO1_STATUS = 0 AND po1.LO1_ID = md2_to.LO1_ID" )
	                ->leftJoin("po2_order_line AS po2" ,
                        "po2.MD1_ID = md1.MD1_ID AND po1.PO1_ID = po2.PO1_ID AND po2.PO2_DELETE_FLAG = 0 AND po2.LO1_ID = md2_to.LO1_ID AND po2.PO2_STATUS = 0" );

                $query->addSelect(['CEILING((coalesce(md2_to.MD2_MIN_QTY, 0)  - (coalesce(md2_to.MD2_ON_HAND_QTY, 0) + (sum(coalesce(po2.PO2_ORDER_QTY, 0)) * coalesce(um2_rec.UM2_FACTOR,1))))/coalesce(um2_rec.UM2_FACTOR,1)) as AUTO_ORDER_QTY']);
                if( !empty($filters['VD1_IDs']) && $filters['VD1_IDs'] ){
                    $query->andFilterWhere(['in', 'vd1.VD1_ID', $filters['VD1_IDs']]);
                }
                $query->andFilterWhere(['=', 'VD1.VD1_NO_INVENTORY', 0]);
            }elseif( $autoOrder == self::AUTO_ORDER_SO ){
                $query
                    ->leftJoin("(Select  
                                        o.or1_id,
				                        o.md1_id, 
				                        o.lo1_to_id,
				                        o.OR2_ORDER_QTY
				                    from or2_order_line as o 
                                    WHERE o.lo1_to_id = {$LO1_TO_ID} and 
                                          o.CO1_ID = {$user->CO1_ID} and 
                                          o.OR2_DELETE_FLAG = 0 and 
                                          o.OR2_STATUS = 0
                                    ) AS or2",
					"or2.md1_id = md1.md1_id AND or2.lo1_to_id = md2_to.lo1_id")
                    ->leftJoin("or1_order_header AS or1",
                        "or1.OR1_DELETE_FLAG = 0 AND 
                             or1.OR1_STATUS = 0 AND 
                             or1.LO1_TO_ID = md2_to.LO1_ID AND 
                             or1.or1_type = 0 AND 
                             or1.or1_id = or2.or1_id");

                $query->addSelect(['CEILING( (coalesce(md2_to.MD2_MIN_QTY, 0)  - (coalesce(md2_to.MD2_ON_HAND_QTY, 0) + CEILING(sum(coalesce(or2.OR2_ORDER_QTY, 0)) * coalesce(um2_rec.UM2_FACTOR,1))) )/coalesce(um2_rec.UM2_FACTOR,1)) as AUTO_ORDER_QTY']);
                $query->andFilterWhere(['=', 'coalesce(MD1.MD1_CENTRAL_ORDER_FLAG, 0)', 1]);
                $query->andFilterWhere(['=', 'coalesce(lo1.LO1_CENTER_FLAG, 0)', 0]);
            }

            $query->andWhere('coalesce(md2_to.MD2_AVAILABLE_QTY,0) < coalesce(md2_to.MD2_MIN_QTY,0)');
            $query->groupBy(['MD1.MD1_ID']);
            $query->having(new Expression('AUTO_ORDER_QTY > 0'));
        }

        $this->load($filters, '');
        $query->andFilterWhere(['=', 'MD1.CO1_ID', $user->CO1_ID]);

	    if (isset($filters['MD0_ID'])) {
		    $query->andFilterWhere(['=', 'MD1.MD0_ID', $filters['MD0_ID']]);
		    $query->addSelect(['md2_to.MD2_PRINT_FLAG']);
	    }

	    if (isset($filters['MD2_ACTIVE'])) {
		    $query->andFilterWhere(['=', 'md2_to.MD2_ACTIVE', $filters['MD2_ACTIVE']]);
	    }
	    
        $query->andFilterWhere(['=', 'MD1.MD1_DELETE_FLAG', 0]);
        if (isset($filters['search']) && $filters['search']) {
	        $query->andFilterWhere(['or',
		        ['like', 'MD1.MD1_PART_NUMBER', $this->MD1_PART_NUMBER],
		        ['like', 'MD1.MD1_DESC1', $this->MD1_DESC1]
	        ]);
        }

	    if (isset($filters['MD2_PRINT_FLAG'])) {
		    $query->andFilterWhere(['=', 'md2_to.MD2_PRINT_FLAG', $filters['MD2_PRINT_FLAG']]);
	    }

	    if (isset($this->MD1_IDs)) {
		    $query->andFilterWhere(['IN', 'MD1.MD1_ID', explode(',', $this->MD1_IDs)]);
	    }

	    if (isset($filters['MD1_ID'])) {
		    $query->andFilterWhere(['=', 'md1.MD1_ID', $filters['MD1_ID']]);
	    }

	    if (isset($filters['VD1_NAME'])) {
		    if($LO1_FROM_ID) {
			    $query->andFilterWhere(['=', 'md2_from.VD1_ID', $filters['VD1_NAME']]);
		    } else {
			    $query->andFilterWhere(['=', 'md2_to.VD1_ID', $filters['VD1_NAME']]);
		    }
	    }

	    if (isset($filters['MD2_PRINT_FLAG'])) {
		    if($LO1_FROM_ID) {
			    $query->andFilterWhere(['=', 'md2_from.MD2_PRINT_FLAG', $filters['MD2_PRINT_FLAG']]);
		    } else {
			    $query->andFilterWhere(['=', 'md2_to.MD2_PRINT_FLAG', $filters['MD2_PRINT_FLAG']]);
		    }
	    }

	    if (isset($filters['MD1_PART_NUMBER'])) {
		    $query->andFilterWhere(['=', 'md1.MD1_PART_NUMBER', $filters['MD1_PART_NUMBER']]);
	    }

	    if (isset($filters['MD1_VENDOR_PART_NUMBER'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_VENDOR_PART_NUMBER', $filters['MD1_VENDOR_PART_NUMBER']]);
	    }

	    if (isset($filters['MD1_OEM_NUMBER'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_OEM_NUMBER', $filters['MD1_OEM_NUMBER']]);
	    }

	    if (isset($filters['MD1_DESC1'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_DESC1', $filters['MD1_DESC1']]);
	    }

	    if (isset($filters['MD1_UNIT_PRICE'])) {
		    $query->andFilterWhere(['like', 'MD1_UNIT_PRICE', $filters['MD1_UNIT_PRICE']]);
	    }

	    if (isset($filters['MD2_STOCK_LEVEL']) && $filters['MD2_STOCK_LEVEL'] != '') {
		    if ($LO1_FROM_ID) {
			    $query->andFilterWhere(['=', $MD2_STOCK_LEVEL_Exp_from, $filters['MD2_STOCK_LEVEL']]);
		    } else {
			    $query->andFilterWhere(['=', $MD2_STOCK_LEVEL_Exp_to, $filters['MD2_STOCK_LEVEL']]);
		    }
	    }

	    if (isset($filters['MD2_ON_HAND_QTY'])) {
		    if($LO1_FROM_ID) {
			    $query->andFilterWhere(['like', 'md2_from.MD2_ON_HAND_QTY', $filters['MD2_ON_HAND_QTY']]);
		    } else {
			    $query->andFilterWhere(['like', 'md2_to.MD2_ON_HAND_QTY', $filters['MD2_ON_HAND_QTY']]);
		    }
	    }

	    if (isset($filters['MD2_AVAILABLE_QTY'])) {
		    if($LO1_FROM_ID) {
			    $query->andFilterWhere(['like', 'md2_from.MD2_AVAILABLE_QTY', $filters['MD2_AVAILABLE_QTY']]);
		    } else {
			    $query->andFilterWhere(['like', 'md2_to.MD2_AVAILABLE_QTY', $filters['MD2_AVAILABLE_QTY']]);
		    }
	    }

	    if (isset($filters['MD2_MIN_QTY'])) {
		    if($LO1_FROM_ID) {
			    $query->andFilterWhere(['like', 'md2_from.MD2_MIN_QTY', $filters['MD2_MIN_QTY']]);
		    } else {
			    $query->andFilterWhere(['like', 'md2_to.MD2_MIN_QTY', $filters['MD2_MIN_QTY']]);
		    }
	    }

	    if (isset($filters['MD2_RACK'])) {
		    $query->andFilterWhere(['or',
			    ['like', 'md2_to.MD2_RACK', $filters['MD2_RACK']],
			    ['like', 'md2_to.MD2_DRAWER', $filters['MD2_RACK']],
			    ['like', 'md2_to.MD2_BIN', $filters['MD2_RACK']]
		    ]);
	    }

	    if (isset($filters['UM1_PRICE_NAME'])) {
		    $query->andFilterWhere(['=', 'um1_rec.UM1_ID', $filters['UM1_PRICE_NAME']]);
	    }

	    if (isset($filters['C00_NAME'])) {
		    $query->andFilterWhere(['=', 'md1.C00_ID', $filters['C00_NAME']]);
	    }

	    if (isset($filters['CA1_NAME'])) {
		    $query->andFilterWhere(['=', 'md1.CA1_ID', $filters['CA1_NAME']]);
	    }

	    if (isset($filters['GL1_NAME'])) {
		    $query->andFilterWhere(['like', 'gl1.GL1_NAME', $filters['GL1_NAME']]);
	    }

	    if (isset($filters['MD1_UPC1'])) {
		    $query->andFilterWhere(['like', 'md1.MD1_UPC1', $filters['MD1_UPC1']]);
	    }

	    if (isset($filters['MD1_BODY'])) {
		    $query->andFilterWhere(['=', 'MD1_BODY', $filters['MD1_BODY']]);
	    }

	    if (isset($filters['MD1_DETAIL'])) {
		    $query->andFilterWhere(['=', 'MD1_DETAIL', $filters['MD1_DETAIL']]);
	    }

	    if (isset($filters['MD1_PAINT'])) {
		    $query->andFilterWhere(['=', 'MD1_PAINT', $filters['MD1_PAINT']]);
	    }

        if (!empty($filters['sort'])) {
            $query->orderBy($filters['sort']);
        } else{
            $query->orderBy(['CO1_NAME' => SORT_DESC, 'VD1_NAME' => SORT_ASC, 'MD1_PART_NUMBER' => SORT_DESC]);
        }

        return $query;
     }

     public function getEditInventoryParts($params) {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['MD1_PART_NUMBER'] = SORT_ASC;
        }

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        $user = Yii::$app->user->getIdentity();
        $query = self::find()->alias('MD1')
                ->select([
                    'MD1.MD1_ID',
                    'MD1.MD1_PART_NUMBER',
                    'MD1.MD1_VENDOR_PART_NUMBER',
                    'if(1=1,null,null) as VD0_NAME',
                    'MD1.MD1_UPC1',
                    'MD1.MD1_DESC1'])
                ->leftJoin("md2_location as MD2_FROM", "MD2_FROM.MD1_ID = MD1.MD1_ID AND MD2_FROM.MD2_DELETE_FLAG = 0 AND MD2_FROM.LO1_ID =" . $user->LO1_ID );

        $query->andFilterWhere(['=', 'MD1.CO1_ID', $user->CO1_ID]);
        $query->andFilterWhere(['=', 'MD1.MD1_DELETE_FLAG', 0]);
        $query->andFilterWhere(['=', 'MD2_FROM.MD2_ACTIVE', 1]);

        if(isset($this->MD1_PART_NUMBER)) {
            $query->andFilterWhere(['=', 'MD1.MD1_PART_NUMBER', $this->MD1_PART_NUMBER]);
        }

        if(isset($this->MD1_CENTRAL_ORDER_FLAG)) {
            $query->andFilterWhere(['=', 'MD1.MD1_CENTRAL_ORDER_FLAG', $this->MD1_CENTRAL_ORDER_FLAG]);
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

     public function getFullPartForCreditMemo( $filters=[] ) {
         $user = Yii::$app->user->getIdentity();
         $this->load($filters, '');

         $query = self::find()->alias('MD1')->select([
             'MD1.MD1_ID', 'MD1.MD1_IMAGE',
             'MD1.FI1_ID', 'fi1.FI1_FILE_PATH',
             'MD1.MD0_ID',
             'MD1.CO1_ID', 'CO1.CO1_SHORT_CODE', 'CO1.CO1_NAME',
             'md2_to.VD1_ID', 'md2_to.MD2_ID', 'md2_to.MD2_ACTIVE',
             'md2_to.MD2_NO_INVENTORY AS NO_INVENTORY', 'md2_to.MD2_PRINT_FLAG',
             'VD1.VD1_SHORT_CODE', 'VD1.VD1_NAME',
             'CA1.CA1_NO_INVENTORY AS NO_JOB_INVENTORY',
             'MD1.MD1_FAVORITE', 'MD1.MD1_PART_NUMBER', 'MD1.MD1_VENDOR_PART_NUMBER',
             'MD1.MD1_OEM_NUMBER', 'MD1.MD1_DESC1',
             'MD1.MD1_UNIT_PRICE/coalesce(um2_price.um2_factor,1) AS MD1_UNIT_PRICE',
             'MD1.MD1_UNIT_PRICE*(1+COALESCE(MD1.MD1_MARKUP,0)*0.01)/coalesce(um2_price.um2_factor,1) AS MD1_SELL_PRICE',
             'MD1.MD1_UNIT_PRICE/coalesce(um2_rec.um2_factor,1) AS MD1_REC_PRICE',
             'MD1.MD1_MARKUP AS MD1_MARKUP',
             'um1_price.UM1_SIZE AS UM1_PRICE_SIZE',
             'um1_price.UM1_ID AS UM1_PRICE_ID',
             '(if(md2_from.MD2_ACTIVE=0 OR (coalesce(md2_to.MD2_ON_HAND_QTY,0)=0 AND coalesce(md2_from.MD2_MIN_QTY,0)=0), "0",
             if(coalesce(md2_from.MD2_ON_HAND_QTY,0)<coalesce(md2_from.MD2_MIN_QTY,0), "3",
             if((coalesce(md2_from.MD2_ON_HAND_QTY,0)>coalesce(md2_from.MD2_MIN_QTY,0)), "2", "1")))) AS MD2_STOCK_LEVEL',
             'md2_from.MD2_ON_HAND_QTY',
             'md2_from.MD2_AVAILABLE_QTY',
             'md2_from.MD2_MIN_QTY',
             'md2_from.MD2_MAX_QTY',
             'CONCAT(md2_from.MD2_RACK, "-", md2_from.MD2_DRAWER, "-", md2_from.MD2_BIN) as MD2_RACK',
             'md2_to.MD2_ON_HAND_QTY AS MD2_ON_HAND_QTY_TO',
             'md2_to.MD2_AVAILABLE_QTY AS MD2_AVAILABLE_QTY_TO',
             'md2_to.MD2_MIN_QTY AS MD2_MIN_QTY_TO',
             'md2_to.MD2_MAX_QTY AS MD2_MAX_QTY_TO',
             'GREATEST(1,CEILING((coalesce(md2_to.MD2_MIN_QTY,0) - coalesce(md2_to.MD2_AVAILABLE_QTY,0))/coalesce(um2_rec.UM2_FACTOR,1))) AS DEFAULT_ORDER_QTY',
             'if(md2_from.LO1_ID=md2_to.LO1_ID AND 1, um1_rec.UM1_NAME,um1_pur.UM1_NAME) AS UM1_PRICE_NAME',
             'md2_from.MD2_NO_INVENTORY AS NO_INVENTORY_FROM',
             'MD1.C00_ID',
             'C00.C00_NAME',
             'MD1.CA1_ID',
             'CA1.CA1_NAME',
             'MD1.GL1_ID',
             'GL1.GL1_NAME',
             'MD1.MD1_UPC1',
             'if(MD1.MD1_BODY = 1, "Y", "-") as MD1_BODY',
             'if(MD1.MD1_DETAIL = 1, "Y", "-") as MD1_DETAIL',
             'if(MD1.MD1_PAINT = 1, "Y", "-") as MD1_PAINT',
             'um1_pur.UM1_ID AS UM1_PURCHASE_ID',
             'um1_pur.UM1_NAME AS UM1_PURCHASE_NAME',
             'um1_rec.UM1_ID AS UM1_RECEIPT_ID',
             'um1_rec.UM1_NAME AS UM1_RECEIPT_NAME',
             'um2_price.UM2_FACTOR AS UM2_PRICE_FACTOR',
             'um2_rec.UM2_FACTOR AS UM2_RECEIPT_FACTOR',
	         'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('MD1_MODIFIED_ON', $filters) . ', "%m.%d.%Y %H:%i")',
	         'CREATED_ON'  => 'DATE_FORMAT(' . $this->getLocalDate('MD1_CREATED_ON', $filters) . ', "%m.%d.%Y %H:%i")',
             'MD1.MD1_MODIFIED_BY AS MD1_MODIFIED_BY',
             'us1_mod.US1_NAME AS MODIFIED_BY',
             'MD1.MD1_CREATED_BY AS MD1_CREATED_BY',
             'us2_create.US1_NAME AS CREATED_BY'
         ])
         ->leftJoin("md0_master_data  as MD0", "MD1.MD0_ID = MD0.MD0_ID AND MD0.MD0_DELETE_FLAG=0")
         ->leftJoin("fi1_file AS fi1", "fi1.FI1_ID = MD1.FI1_ID AND fi1.FI1_DELETE_FLAG=0")
         ->leftJoin("c00_category AS C00", "C00.C00_ID = MD1.C00_ID AND C00.C00_DELETE_FLAG=0")
         ->leftJoin("md2_location AS md2_from", "md2_from.MD1_ID = MD1.MD1_ID AND md2_from.LO1_ID=" . $filters['LO1_FROM_ID'] . " AND md2_from.MD2_DELETE_FLAG=0")
         ->leftJoin("md2_location AS md2_to", "md2_to.MD1_ID = MD1.MD1_ID AND md2_to.LO1_ID=" . $filters['LO1_FROM_ID'] . " AND md2_to.MD2_DELETE_FLAG=0")
         ->leftJoin("lo1_location AS lo1", "lo1.LO1_ID = md2_to.LO1_ID AND lo1.LO1_DELETE_FLAG=0")
         ->leftJoin("vd1_vendor AS VD1", "VD1.VD1_ID = md2_to.VD1_ID AND VD1.VD1_DELETE_FLAG=0")
         ->leftJoin("um1_unit AS um1_pur", "um1_pur.UM1_ID = md2_to.UM1_PURCHASE_ID AND um1_pur.UM1_DELETE_FLAG=0")
         ->leftJoin("um1_unit AS um1_rec", "um1_rec.UM1_ID = md2_to.UM1_RECEIPT_ID AND um1_rec.UM1_DELETE_FLAG=0")
         ->leftJoin("um2_conversion AS um2_rec", "um2_rec.MD1_ID = MD1.MD1_ID AND um2_rec.UM1_FROM_ID = md2_to.UM1_PURCHASE_ID AND um2_rec.UM1_TO_ID = md2_to.UM1_RECEIPT_ID AND um2_rec.UM2_DELETE_FLAG=0")
         ->leftJoin("co1_company AS CO1", "CO1.CO1_ID = MD1.CO1_ID AND CO1.CO1_DELETE_FLAG=0")
         ->leftJoin("gl1_glcode AS GL1", "GL1.GL1_ID = MD1.GL1_ID AND gl1.GL1_DELETE_FLAG=0")
         ->leftJoin("ca1_category AS CA1", "CA1.CA1_ID = MD1.CA1_ID AND CA1.CA1_DELETE_FLAG=0")
         ->leftJoin("vd0_vendor AS VD0", "VD0.VD0_ID = MD0.VD0_ID AND VD0.VD0_DELETE_FLAG=0")
         ->leftJoin("us1_user AS us1_mod", "us1_mod.US1_ID = MD1.MD1_MODIFIED_BY")
         ->leftJoin("us1_user AS us2_create", "us2_create.US1_ID = MD1.MD1_CREATED_BY")
         ->leftJoin("um1_unit AS um1_price", "um1_price.UM1_ID = MD1.UM1_PRICING_ID AND um1_price.UM1_DELETE_FLAG=0")
         ->leftJoin("um2_conversion AS um2_price", "um2_price.MD1_ID = MD1.MD1_ID AND (um2_price.UM1_TO_ID=if(md2_from.LO1_ID=md2_to.LO1_ID, md2_to.UM1_RECEIPT_ID,md2_to.UM1_PURCHASE_ID)) AND (um2_price.UM1_FROM_ID=MD1.UM1_PRICING_ID) AND (um2_price.UM2_DELETE_FLAG=0) ");

         $query->andFilterWhere(['=', 'MD1.CO1_ID', $user['CO1_ID']]);
         $query->andFilterWhere(['=', 'MD1.MD1_DELETE_FLAG', 0]);
         $query->andFilterWhere(['=', 'md2_from.MD2_ACTIVE', 1]);
         $query->andFilterWhere(['=', 'md2_to.MD2_ACTIVE', 1]);
         $query->andFilterWhere(['=', 'MD1.MD1_PART_NUMBER', $this->MD1_PART_NUMBER]);

         if (isset($this->PURCHASE_ORDER) && $this->PURCHASE_ORDER == 1) {
            $query->andFilterWhere(['or',
                                    ['coalesce(lo1.LO1_CENTER_FLAG,0)=1'],
                                    ['coalesce(md1.MD1_CENTRAL_ORDER_FLAG,0) = 0']
                                ]);
         }

         if (isset($this->MD1_IDs)) {
            $query->andFilterWhere(['NOT IN', 'MD1.MD1_ID', explode(',', $this->MD1_IDs)]);
         }

         if (isset($filters['LINKED_PART']) && $filters['LINKED_PART'] == '1') {
            $query->andWhere(['=', 'MD1.MD1_ID', $this->MD1_ID]);
         } else {
            $query->andFilterWhere(['or',
                                    ['MD1.MD1_PART_NUMBER'=>$this->MD1_PART_NUMBER],
                                    ['MD1.MD1_UPC1'=>$this->MD1_PART_NUMBER]
                                ]);
         }

         $query->orderBy(['CO1.CO1_NAME' => SORT_DESC, 'VD1.VD1_NAME' => SORT_ASC, 'MD1.MD1_PART_NUMBER' => SORT_DESC]);

         $dataProvider = new ActiveDataProvider([
             'query'      => $query,
             'pagination' => false
         ]);

         return $dataProvider;
      }

	public function getRack( $params ) {
		$user = Yii::$app->user->getIdentity();
		$this->load($params, '');
        $queryBelowMin = (isset($params['belowMinimumReport']) && $params['belowMinimumReport'])?' AND m2.MD2_ON_HAND_QTY < m2.MD2_MIN_QTY':'';
		$query = self::find()->alias('MD1')->select([
			'MD1.MD1_ID', 'm2.MD2_ID',
			'MD1.MD1_PART_NUMBER',
			'MD1.MD1_VENDOR_PART_NUMBER',
			'MD1.MD1_DESC1', 'MD1.MD1_UPC1',
			'VD1.VD1_ID', 'VD1.VD1_NAME',
			'CA1.CA1_ID', 'CA1.CA1_NAME',
			'COALESCE(MD1.MD1_IMAGE, MD0.MD0_IMAGE, md00.MD0_IMAGE) AS MD1_IMAGE',
			'fi1.FI1_FILE_PATH',
			'u1.UM1_NAME',
			'm2.MD2_MIN_QTY', 'm2.MD2_RACK',
			'm2.MD2_DRAWER', 'm2.MD2_BIN'
		])
			->leftJoin("md0_master_data  as MD0", "MD1.MD0_ID = MD0.MD0_ID AND MD0.MD0_DELETE_FLAG=0")
			->leftJoin("md0_master_data  as MD00", "MD00.MD0_ID = MD0.MD0_PARENT_ID AND MD00.MD0_DELETE_FLAG = 0")
			->leftJoin("fi1_file AS fi1", "fi1.FI1_ID = MD1.FI1_ID AND fi1.FI1_DELETE_FLAG=0")
			->leftJoin("md2_location AS m2", "m2.MD1_ID = MD1.MD1_ID AND m2.MD2_DELETE_FLAG=0 {$queryBelowMin}")
			->leftJoin("vd1_vendor AS VD1", "VD1.VD1_ID = m2.VD1_ID AND VD1.VD1_DELETE_FLAG=0")
			->leftJoin("lo1_location AS lo1", "lo1.LO1_ID = m2.LO1_ID AND lo1.LO1_DELETE_FLAG=0")
			->leftJoin("ca1_category AS CA1", "CA1.CA1_ID = MD1.CA1_ID AND CA1.CA1_DELETE_FLAG=0")
			->leftJoin("um1_unit AS u1", "u1.um1_id = m2.um1_receipt_id AND u1.UM1_DELETE_FLAG=0")
			->leftJoin("um2_conversion AS f", "f.um1_from_id = MD1.um1_pricing_id AND f.um1_to_id = m2.um1_receipt_id AND f.md1_id = MD1.md1_id");

		$query->andFilterWhere(['=', 'MD1.MD1_DELETE_FLAG', 0]);
		$query->andFilterWhere(['=', 'm2.MD2_DELETE_FLAG', 0]);

		if (isset($user['CO1_ID'])) {
			$query->andFilterWhere(['=', 'MD1.CO1_ID', $user['CO1_ID']]);
		}

		if (isset($user['LO1_ID'])) {
			$query->andFilterWhere(['=', 'm2.LO1_ID', $user['LO1_ID']]);
		}

		if (isset($this->MD1_BODY) && $this->MD1_BODY)
			$query->andFilterWhere(['=', 'MD1.MD1_BODY', $this->MD1_BODY]);
		if (isset($this->MD1_DETAIL) && $this->MD1_DETAIL)
			$query->andFilterWhere(['=', 'MD1.MD1_DETAIL', $this->MD1_DETAIL]);
		if (isset($this->MD1_PAINT) && $this->MD1_PAINT)
			$query->andFilterWhere(['=', 'MD1.MD1_PAINT', $this->MD1_PAINT]);
		if (isset($this->MD1_FAVORITE) && $this->MD1_FAVORITE)
			$query->andFilterWhere(['=', 'MD1.MD1_FAVORITE', $this->MD1_FAVORITE]);

		if (isset($this->CHECKOUT_SHEET)) {
			$query->orderBy(['MD1.MD1_PART_NUMBER' => SORT_ASC]);
			$query->limit(80);
		}

		return $query->asArray()->all();
	}

	public function getInventoryOptimizationQuery( $params, $totals = false )
	{
		$user = Yii::$app->user->getIdentity();
		$this->load($params, '');

        $months = isset($params['months']) ? $params['months'] : 1;
		$MD2_UNIT_PRICE_Exp = new Expression('ROUND(m.MD1_UNIT_PRICE/COALESCE(f.um2_factor, 1), 2)');
		$EXTENDED_STOCK_Exp = new Expression('ROUND(SUM(coalesce(m2.MD2_ON_HAND_QTY,0)*m.md1_unit_price/COALESCE(f.um2_factor, 1)), 2)');
        $AVERAGE_USAGE_Exp = new Expression('SUM(coalesce(o2.OR2_ORDER_QTY, 0)+coalesce(j.JT2_ORDER_QTY, 0))/' . $months );
        $AVERAGE_MIN_QTY = new Expression('CEILING((SUM(coalesce(o2.OR2_ORDER_QTY, 0)+coalesce(j.JT2_ORDER_QTY, 0))/' . $months . ')/coalesce(l.LO1_ORDER_DAYS_PER_MONTH,1))' );
		$QTY_DIFF_Exp = new Expression('ROUND((coalesce(MD2_ON_HAND_QTY, 0) - CEILING((SUM(coalesce(o2.OR2_ORDER_QTY, 0)+coalesce(j.JT2_ORDER_QTY, 0)))/' . $months . '))/coalesce(l.LO1_ORDER_DAYS_PER_MONTH,1))');
		$PART_STATUS_Exp = new Expression(
		    'if ( CEILING((SUM(coalesce(o2.OR2_ORDER_QTY, 0)+coalesce(j.JT2_ORDER_QTY, 0))/' . $months . ')/coalesce(l.LO1_ORDER_DAYS_PER_MONTH,1)) < MD2_MIN_QTY , 2, 
		                    if( CEILING((SUM(coalesce(o2.OR2_ORDER_QTY, 0)+coalesce(j.JT2_ORDER_QTY, 0))/' . $months . ')/coalesce(l.LO1_ORDER_DAYS_PER_MONTH,1)) > MD2_MIN_QTY, 1, 0))' );
		$EXTENDED_PRICE_Exp = new Expression('ROUND(SUM((coalesce(o2.OR2_ORDER_QTY,0)+coalesce(j.JT2_ORDER_QTY,0))*m.md1_unit_price/COALESCE(f.um2_factor, 1)), 2)');

		$fields = [
            'l.LO1_NAME',
            'm.MD1_ID',
            'm.MD1_PART_NUMBER',
            'm.MD1_DESC1',
            'v.VD1_NAME',
            'c.CA1_NAME',
            'g.GL1_NAME',
            'coalesce(m2.MD2_ON_HAND_QTY,0) AS MD2_ON_HAND_QTY',
            'coalesce(m2.MD2_MIN_QTY,0) AS MD2_MIN_QTY',
            'u1.UM1_NAME',
            'u2.UM1_NAME AS UM1_PURCHASE_NAME',
            'f.UM2_FACTOR',
            'l.LO1_ORDER_DAYS_PER_MONTH',
            'm.MD1_UNIT_PRICE',
            'MD2_UNIT_PRICE' => $MD2_UNIT_PRICE_Exp,
            'EXTENDED_STOCK' => $EXTENDED_STOCK_Exp
        ];

        if (isset($params['REPORT_TYPE']) && $params['REPORT_TYPE'] == 'inventoryOptimization') {
            $fields = array_merge($fields, [
                'AVERAGE_USAGE' => $AVERAGE_USAGE_Exp,
                'QTY_DIFF' => $QTY_DIFF_Exp,
                'AVERAGE_MIN_QTY' => $AVERAGE_MIN_QTY,
                'PART_STATUS' => $PART_STATUS_Exp,
                'EXTENDED_PRICE' => $EXTENDED_PRICE_Exp,
                'm2.MD2_ACTIVE'
            ]);
        }

		$LO1_ID = $user->LO1_ID;
		if( ($user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
             $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ) && !empty($params['LO1_ID'])){
            $LO1_ID = $params['LO1_ID'];
        }

		$CO1_ID = $user->CO1_ID;
		if( ($user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
             $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ) && !empty($params['CO1_ID'])){
			$CO1_ID = $params['CO1_ID'];
		}

		$query = self::find()->alias('m')->select($fields)
            ->leftJoin("md2_location m2","m2.md1_id = m.md1_id AND md2_active=1 AND " . ($LO1_ID ? "m2.lo1_id = " . $LO1_ID . " AND" : "") . " m2.md2_delete_flag = 0 ")
            ->leftJoin("vd1_vendor v","v.vd1_id = m2.vd1_id AND v.vd1_delete_flag = 0")
            ->leftJoin("gl1_glcode g","g.gl1_id = m.gl1_id AND g.gl1_delete_flag = 0")
            ->leftJoin("lo1_location l","l.lo1_id = m2.lo1_id AND l.lo1_delete_flag = 0")
            ->leftJoin("ca1_category c","c.ca1_id = m.ca1_id AND c.ca1_delete_flag = 0")
            ->leftJoin("um1_unit u1","u1.um1_id = m2.um1_receipt_id AND u1.um1_delete_flag = 0")
            ->leftJoin("um1_unit u2","u2.um1_id = m2.um1_purchase_id AND u2.um1_delete_flag = 0")
            ->leftJoin("um2_conversion f","f.um1_from_id = m.um1_pricing_id AND f.um1_to_id = m2.um1_receipt_id AND f.md1_id = m.md1_id");

		if (isset($params['REPORT_TYPE']) &&
           ($params['REPORT_TYPE'] == 'inventoryOptimization')) {
			$query->leftJoin("jt2_job_line j",
				"j.md1_id = m.md1_id AND 
                     j.lo1_id = m2.lo1_id AND 
                     j.jt2_delete_flag = 0 and 
                     (j.JT2_CREATED_ON IS NOT NULL) AND 
                     j.JT2_CREATED_ON BETWEEN CAST('{$params['from']}' AS DATE) AND CAST('{$params['to']}' AS DATE)")
				->leftJoin("(Select 
				                        o.OR2_ORDER_QTY, 
				                        o.md1_id, 
				                        o.lo1_to_id,
				                        o.LO1_FROM_ID
				                    from or2_order_line as o 
                                    WHERE o.lo1_to_id = {$LO1_ID} and 
                                          o.CO1_ID = {$CO1_ID} and 
                                          o.or2_delete_flag = 0 and
                                         (o.OR2_CREATED_ON IS NOT NULL) AND
                                          o.OR2_CREATED_ON BETWEEN CAST('{$params['from']}' AS DATE) AND CAST('{$params['to']}' AS DATE) 
                                    ) AS o2",
					"o2.md1_id = m.md1_id AND o2.lo1_to_id = m2.lo1_id");
			$query->andFilterWhere(['or',
				['>=', 'coalesce(m2.md2_min_qty,0)', 0],
				['>', 'o2.OR2_ORDER_QTY', 0],
				['>', 'j.JT2_ORDER_QTY', 0]
			]);
		}else{
		    $query->andFilterWhere(['=', 'm2.MD2_ACTIVE', 1]);
        }

		$query->andFilterWhere(['=', 'm.md1_delete_flag', 0]);
		$query->andFilterWhere(['=', 'm2.md2_delete_flag', 0]);

		$isCentral = isset($user->location) && strval($user->location->LO1_CENTER_FLAG) == '1' ? true : false;
		if( $isCentral && $params['REPORT_TYPE'] == 'inventoryOptimization' ){
		    $query->andFilterWhere([ ($isCentral ? '=' : '<>'), 'o2.LO1_FROM_ID', 'o2.LO1_TO_ID']);
        }

		if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $query->andFilterWhere(['=', 'v.VD0_ID', $user->VD0_ID]);
        }

		if ($LO1_ID) {
			$query->andFilterWhere(['=', 'm2.lo1_id', $LO1_ID]);
		}

		if ($CO1_ID) {
			$query->andFilterWhere(['=', 'm.CO1_ID', $CO1_ID]);
		}

		if (isset($params['limitToMinQty'])) {
		    $limitToMinQtyExp = new Expression('if((coalesce(m2.MD2_ON_HAND_QTY, 0) = 0 AND coalesce(m2.MD2_MIN_QTY, 0) = 0), "0", "1")');
			$query->andFilterWhere(['=', $limitToMinQtyExp, 1]);
		}

		if (isset($params['LO1_NAME'])) {
			$query->andFilterWhere(['=', 'm2.lo1_id', $params['LO1_NAME']]);
		}

		if (isset($params['MD1_PART_NUMBER'])) {
			$query->andFilterWhere(['like', 'm.MD1_PART_NUMBER', $params['MD1_PART_NUMBER']]);
		}

		if (isset($params['MD1_DESC1'])) {
			$query->andFilterWhere(['like', 'm.MD1_DESC1', $params['MD1_DESC1']]);
		}

		if (isset($params['VD1_NAME'])) {
			$query->andFilterWhere(['=', 'm2.VD1_ID', intval($params['VD1_NAME'])]);
		}

		if (isset($params['CA1_NAME'])) {
			$query->andFilterWhere(['=', 'm.CA1_ID', $params['CA1_NAME']]);
		}

		if (isset($params['GL1_NAME'])) {
			$query->andFilterWhere(['like', 'g.GL1_NAME', $params['GL1_NAME']]);
		}

		if (isset($params['MD2_ON_HAND_QTY'])) {
			$query->andFilterWhere(['=', 'm2.MD2_ON_HAND_QTY', $params['MD2_ON_HAND_QTY']]);
		}

		if (isset($params['MD2_MIN_QTY'])) {
			$query->andFilterWhere(['=', 'm2.MD2_MIN_QTY', $params['MD2_MIN_QTY']]);
		}

		if (isset($params['AVERAGE_USAGE'])) {
			$query->andHaving(['=', $AVERAGE_USAGE_Exp, $params['AVERAGE_USAGE']]);
		}

		if (isset($params['QTY_DIFF'])) {
			$query->andHaving(['=', $QTY_DIFF_Exp, $params['QTY_DIFF']]);
		}

		if(isset($params['PART_STATUS']) && $params['PART_STATUS'] != '') {
			$query->andHaving(['IN', $PART_STATUS_Exp, explode(',', $params['PART_STATUS'])]);
		}

		if (isset($params['UM1_NAME'])) {
			$query->andFilterWhere(['=', 'm2.um1_receipt_id', $params['UM1_NAME']]);
		}

		if (isset($params['UM1_PURCHASE_NAME'])) {
			$query->andFilterWhere(['=', 'm2.um1_purchase_id', $params['UM1_PURCHASE_NAME']]);
		}

		if (isset($params['UM2_FACTOR'])) {
			$query->andFilterWhere(['like', 'f.UM2_FACTOR', $params['UM2_FACTOR']]);
		}

		if (isset($params['MD2_UNIT_PRICE'])) {
			$query->andHaving(['like', $MD2_UNIT_PRICE_Exp, $params['MD2_UNIT_PRICE']]);
		}

		if (isset($params['EXTENDED_PRICE'])) {
			$query->andHaving(['like', $EXTENDED_PRICE_Exp, $params['EXTENDED_PRICE']]);
		}

		if (isset($params['EXTENDED_STOCK'])) {
			$query->andHaving(['like', $EXTENDED_STOCK_Exp, $params['EXTENDED_STOCK']]);
		}

		$query->groupBy(['m.MD1_ID']);

		if ($totals){
            if (isset($params['REPORT_TYPE']) && $params['REPORT_TYPE'] == 'inventoryOptimization') {
				$fields = [
				    'EXTENDED_PRICE' => 'SUM(T.EXTENDED_PRICE)',
					'EXTENDED_STOCK' => 'SUM(T.EXTENDED_STOCK)',
				];
			} else { //physicalInventory
				$fields = [
				    'EXTENDED_STOCK' => 'SUM(T.EXTENDED_STOCK)',
                ];
			}
			$query = self::find()->from(['T' => $query])->select($fields);
        }else{
		    if (isset($params['SORT_COLUMN'])) {
			    $query->orderBy([$params['SORT_COLUMN'] => ($params['SORT_DIRECTION'] == 'ASC' ? SORT_ASC : SORT_DESC)]);
		    } else {
			    $query->orderBy(['m.MD1_PART_NUMBER' => SORT_ASC]);
		    }
        }

		return $query;
	}

	public function checkDuplicate( $params ){

        $user = Yii::$app->user->getIdentity();
		$this->load($params, '');

		$query = self::find()->alias('MD1')
			->select(['count(*) as NUM'])
			->leftJoin("md0_master_data as MD0" , "MD1.MD0_ID = MD0.MD0_ID AND MD0.MD0_DELETE_FLAG = 0");

		$query->andFilterWhere(['=',  'MD1.MD1_DELETE_FLAG', 0]);
		$query->andFilterWhere(['=',  'MD1.CO1_ID', $user->CO1_ID]);
		$query->andFilterWhere(['=',  'MD1.MD1_PART_NUMBER', $this->MD1_PART_NUMBER]);

		if (isset($params['VD0_ID'])) {
			$query->andFilterWhere(['=',  'MD0.VD0_ID', $params['VD0_ID']]);
		}

		if (isset($params['MD1_ID'])) {
			$query->andFilterWhere(['<>', 'MD1.MD1_ID', $params['MD1_ID']]);
		}
		
		return $query->asArray()->one();
    }

	public function getTechUsage( $params ) {

		$user = Yii::$app->user->getIdentity();

    	$orderLinesQuery = OrderLine::find()->alias('o2')->select([
    	    'o2.TE1_ID',
			'te1.TE1_NAME',
			'lo1.LO1_NAME',
			'o2.LO1_TO_ID AS LO1_ID'
		])
            ->leftJoin("or1_order_header as OR1" , "OR1.OR1_ID = o2.OR1_ID")
            ->leftJoin("lo1_location lo1",         "lo1.lo1_id = o2.LO1_TO_ID AND lo1.lo1_delete_flag = 0")
            ->leftJoin("te1_technician as te1" ,   "te1.TE1_ID = OR1.TE1_ID")
		    ->andFilterWhere(['=', 'o2.OR2_DELETE_FLAG', 0])
			->andFilterWhere(['is not', 'o2.OR2_CREATED_ON', new \yii\db\Expression('null')])
			->andFilterWhere(['>=', 'DATE(o2.OR2_CREATED_ON)', $params['startDate']])
			->andFilterWhere(['<=', 'DATE(o2.OR2_CREATED_ON)', $params['endDate']])
            ->andFilterWhere(['=',  'o2.LO1_FROM_ID', new \yii\db\Expression('o2.LO1_TO_ID')])
            ->andFilterWhere(['is not', 'te1.TE1_ID', new \yii\db\Expression('null')]);

		if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $orderLinesQuery->leftJoin('vd1_vendor vd1',  "vd1.VD1_ID = OR1.VD1_ID AND vd1.VD1_DELETE_FLAG = 0");
            $orderLinesQuery->andFilterWhere(['=', 'vd1.VD0_ID', $user->VD0_ID]);
        }

		if (!empty($params['LO1_IDs'])) {
			$orderLinesQuery->andFilterWhere(['IN', 'o2.LO1_TO_ID', explode(',', $params['LO1_IDs'])]);
		} else if (!empty($user->LO1_ID) && $user->LO1_ID > 0) {
			$orderLinesQuery->andFilterWhere(['=', 'o2.LO1_TO_ID', $user->LO1_ID]);
		}

		$jobLinesQuery = JobLine::find()->alias('JT2')->select([
		    'JT2.TE1_ID',
			'te1.TE1_NAME',
			'lo1.LO1_NAME',
			'JT2.LO1_ID'
		])
            ->leftJoin("te1_technician as te1" , "te1.TE1_ID = JT2.TE1_ID")
            ->leftJoin("lo1_location lo1","lo1.lo1_id = JT2.LO1_ID AND lo1.lo1_delete_flag = 0")
			->andFilterWhere(['=', 'JT2.JT2_DELETE_FLAG', 0])
			->andFilterWhere(['is not', 'JT2.JT2_CREATED_ON', new \yii\db\Expression('null')])
			->andFilterWhere(['>=', 'DATE(JT2.JT2_CREATED_ON)', $params['startDate']])
			->andFilterWhere(['<=', 'DATE(JT2.JT2_CREATED_ON)', $params['endDate']])
            ->andFilterWhere(['is not', 'te1.TE1_ID', new \yii\db\Expression('null')]);

		if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $jobLinesQuery->leftJoin('vd1_vendor vd1',  "vd1.VD1_ID = JT2.VD1_ID AND vd1.VD1_DELETE_FLAG = 0");
            $jobLinesQuery->andFilterWhere(['=', 'vd1.VD0_ID', $user->VD0_ID]);
        }

		if (!empty($params['LO1_IDs'])) {
			$jobLinesQuery->andFilterWhere(['IN', 'JT2.LO1_ID', explode(',', $params['LO1_IDs'])]);
		} else if (!empty($user->LO1_ID) && $user->LO1_ID > 0) {
			$jobLinesQuery->andFilterWhere(['=', 'JT2.LO1_ID', $user->LO1_ID]);
		}

		if (!empty($params['TE1_ID'])) {
			$orderLinesQuery->andFilterWhere(['=', 'te1.TE1_ID', $params['TE1_ID']]);
			$jobLinesQuery->andFilterWhere(['=', 'te1.TE1_ID', $params['TE1_ID']]);
		}

		if (isset($params['TECH_USAGE_BY_MONTHS'])) {
			$orderLinesQuery->addSelect([
				'o2.MD1_PART_NUMBER AS MD1_PART_NUMBER', 'o2.MD1_DESC1 AS MD1_DESC1',
				'CREATED_ON' => 'DATE_FORMAT(OR2_CREATED_ON, "%b/%Y")', 'o2.CA1_ID', 'ca1.CA1_NAME',
				'OR_CREATED_ON' => 'DATE_FORMAT(o2.OR2_CREATED_ON, "%m.%d.%Y %H:%i")',
				'o2.OR2_CREATED_BY AS CREATED_BY', 'us1_create.US1_NAME AS CREATED_BY_NAME', 'o2.OR2_ID',
				'COALESCE(o2.OR2_ORDER_QTY,0) AS TOTAL_QTY',
				'ROUND(o2.OR2_TOTAL_VALUE,2) AS TOTAL_PRICE',
			]);
			$orderLinesQuery->leftJoin("ca1_category as ca1","ca1.CA1_ID = o2.CA1_ID AND ca1.CA1_DELETE_FLAG=0");
			$orderLinesQuery->leftJoin("us1_user AS us1_create", "us1_create.US1_ID = o2.OR2_CREATED_BY");

			$jobLinesQuery->addSelect([
				'JT2.JT2_PART_NUMBER AS MD1_PART_NUMBER', 'JT2.JT2_DESC1 AS MD1_DESC1',
				'CREATED_ON' => 'DATE_FORMAT(JT2_CREATED_ON, "%b/%Y")', 'JT2.CA1_ID', 'ca1.CA1_NAME',
				'OR_CREATED_ON' => 'DATE_FORMAT(JT2.JT2_CREATED_ON, "%m.%d.%Y %H:%i")',
				'JT2.JT2_CREATED_BY AS CREATED_BY', 'us1_create.US1_NAME AS CREATED_BY_NAME', 'JT2.JT2_ID',
				'COALESCE(JT2.JT2_ORDER_QTY,0) AS TOTAL_QTY',
				'ROUND(JT2.JT2_TOTAL_VALUE,2) AS TOTAL_PRICE'
			]);
			$jobLinesQuery->leftJoin("ca1_category as ca1", "ca1.CA1_ID = JT2.CA1_ID AND ca1.CA1_DELETE_FLAG=0");
			$jobLinesQuery->leftJoin("us1_user AS us1_create", "us1_create.US1_ID = JT2.JT2_CREATED_BY");
		} elseif (isset($params['PROJECTED_PO_USAGE'])) {
			$orderLinesQuery->select([
				'DAY' => 'DATE_FORMAT(OR2_CREATED_ON, "%d")',
				'ROUND(SUM(o2.OR2_TOTAL_VALUE),2) AS TOTAL_PRICE'
			]);
			$orderLinesQuery->groupBy(['DAY']);
			$orderLinesQuery->orderBy(['DAY' => SORT_ASC]);

			$jobLinesQuery->select([
				'DAY' => 'DATE_FORMAT(JT2_CREATED_ON, "%d")',
				'ROUND(SUM(JT2.JT2_TOTAL_VALUE),2) AS TOTAL_PRICE'
			]);
			$jobLinesQuery->groupBy(['DAY']);
			$jobLinesQuery->orderBy(['DAY' => SORT_ASC]);
		} else {
			$orderLinesQuery->addSelect([
				'SUM(COALESCE(o2.OR2_ORDER_QTY,0)) AS TOTAL_QTY',
				'ROUND(SUM(o2.OR2_TOTAL_VALUE),2) AS TOTAL_PRICE',
			]);
			$orderLinesQuery->groupBy(['o2.LO1_TO_ID', 'o2.TE1_ID']);
			$jobLinesQuery->addSelect([
				'SUM(COALESCE(JT2.JT2_ORDER_QTY,0)) AS TOTAL_QTY',
				'ROUND(SUM(JT2.JT2_TOTAL_VALUE),2) AS TOTAL_PRICE'
			]);
			$jobLinesQuery->groupBy(['JT2.LO1_ID', 'JT2.TE1_ID']);
		}

		$orderLinesQuery->union( $jobLinesQuery );

		return $orderLinesQuery->asArray()->all();
	}

	public function unlinkMasterVendorParts( $params ) {
		$user = Yii::$app->user->getIdentity();
		$db = Yii::$app->db;

		$sql = "UPDATE md1_master_data AS MD1 ";
		$sql .=	"LEFT JOIN md0_master_data as MD0 ";
		$sql .=	"ON MD1.MD0_ID = MD0.MD0_ID AND MD0.MD0_DELETE_FLAG=0 ";
		$sql .=	"SET ";
		$sql .=	"MD1.MD0_ID = 0, ";
		$sql .=	"MD1.MD1_MODIFIED_BY = " . $user->US1_ID;
		$sql .=	", MD1.MD1_MODIFIED_ON = " . new \yii\db\Expression('UTC_TIMESTAMP()');
		$sql .=	"WHERE MD0.VD0_ID = " . $params['VD0_ID'];
		$sql .=	" AND MD1.MD0_ID != 0 AND MD1.MD1_DELETE_FLAG = 0";
		$db->createCommand($sql)->execute();
	}

	public function getMasterVendorParts( $params ) {
		$query = self::find()->alias('m')->select([
			'*'
		])->leftJoin("md2_location m2","m2.md1_id = m.md1_id")
			->leftJoin("vd1_vendor v","v.vd1_id = m2.vd1_id");

		$query->andFilterWhere(['=',  'v.VD0_ID', $params['VD0_ID']]);
		$query->andFilterWhere(['is not',  'm.MD1_PART_NUMBER', new \yii\db\Expression('null')]);
		if (isset($params['DELETE_IMAGES'])) {
			$query->andFilterWhere(['IS NOT',  'm.FI1_ID', new \yii\db\Expression('null')]);
			$query->andFilterWhere(['>',  'm.FI1_ID', 0]);
		}

		return $query->asArray()->all();
	}

	public function getVOCUsageQuery( $params ) {
		$coName_Exp = new Expression("coalesce(CO1.CO1_NAME, 'N/A')");
		$loName_Exp = new Expression("coalesce(LO1.LO1_NAME, 'N/A')");
		$partName_Exp = new Expression("coalesce(MD1.MD1_PART_NUMBER, 'N/A')");
		$all_Exp = new Expression("'All'");
		$empty_Exp = new Expression("''");
		$isCentral = isset($params['isCentral']) && $params['isCentral'] ? true : false;
		$isVOC = isset($params['isVOC']) && $params['isVOC'] ? true : false;
		$noUsage = isset($params['noUsage']) && $params['noUsage'] ? true : false;
		$isTechUsage = isset($params['isTechUsage']) && $params['isTechUsage'] ? true : false;

		if (isset($params['TREE_ENABLED'])) {
			if (isset($params['systemMethod']) && $params['systemMethod'] == 'getexcel') {
				if ($isCentral) {
					$params['TREE_LEVEL'] = 4;
				} else {
					$params['TREE_LEVEL'] = 3;
				}
			}

			if (isset($params['systemMethod']) && $params['systemMethod'] == 'getpdf') {
				$fields = [
					'd.LINE_TYPE',
					'd.CREATED_ON',
					'd.HEADER_NUMBER',
					'd.LINE_NUMBER',
					'TE1.TE1_NAME',
					'MD1.MD1_VOC_CATA',
					'MD1.MD1_VOC_VALUE',
					'MD1.MD1_VOC_UNIT',
					'MD1.MD1_VOC_MFG',
					'MD1.MD1_PART_NUMBER',
					'MD1.MD1_DESC1',
					'VD1.VD1_NAME',
					'CA1.CA1_ID',
					'CA1.CA1_NAME',
					'UM1.UM1_NAME',
					'LINE_QTY' => $noUsage ? 'MD2.MD2_ON_HAND_QTY' : 'd.LINE_QTY',
					'UNIT_PRICE' => $noUsage ? 'MD1.md1_unit_price/COALESCE(UM2.um2_factor, 1)' : 'd.UNIT_PRICE',
					'TOTAL_VALUE' => $noUsage ? 'MD1.md1_unit_price/COALESCE(UM2.um2_factor, 1)*MD2.MD2_ON_HAND_QTY' : 'd.TOTAL_VALUE'
				];
			} else {
				switch ($params['TREE_LEVEL']) {
					case 0:
						$fields = [
							'TREE_HEADER' => $coName_Exp,
							'TE1_NAME' => $all_Exp,
							'MD1_DESC1' => $all_Exp,
							'MD1_PART_NUMBER' => $all_Exp,
							'MD1_VOC_CATA' => $all_Exp,
							'MD1_VOC_MFG' => $all_Exp,
							'COATING' => $empty_Exp,
							'BY_WEIGHT' => $empty_Exp,
							'MD1_VOC_VALUE' => $empty_Exp,
							'MD1_VOC_UNIT_NAME' => $all_Exp,
							'MD1_TOTAL_HITS' => 'count(d.LINE_ID)',
							'TREE_LEVEL' => new Expression(0),
							'CO1.CO1_ID'
						];
						break;
					case 1:
						$fields = [
							'TREE_HEADER' => $loName_Exp,
							'TE1_NAME' => $all_Exp,
							'MD1_DESC1' => $all_Exp,
							'MD1_PART_NUMBER' => $all_Exp,
							'MD1_VOC_CATA' => $all_Exp,
							'MD1_VOC_MFG' => $all_Exp,
							'COATING' => $empty_Exp,
							'BY_WEIGHT' => $empty_Exp,
							'MD1_VOC_VALUE' => $empty_Exp,
							'MD1_VOC_UNIT_NAME' => $all_Exp,
							'MD1_TOTAL_HITS' => 'count(d.LINE_ID)',
							'TREE_LEVEL' => new Expression(1),
							'CO1.CO1_ID',
							'LO1.LO1_ID'
						];
						break;
					case 2:
						$fields = [
							'TREE_HEADER' => $partName_Exp,
							'TE1_NAME' => $all_Exp,
							'MD1_DESC1' => 'MD1.MD1_DESC1',
							'MD1_PART_NUMBER' => 'MD1.MD1_PART_NUMBER',
							'MD1_VOC_CATA' => 'MD1.MD1_VOC_CATA',
							'MD1_VOC_MFG' => 'MD1.MD1_VOC_MFG',
							'COATING' => 'COUNT(d.LINE_ID)',
							'BY_WEIGHT' => 'COUNT(d.LINE_ID)*2',
							'MD1_VOC_VALUE' => 'SUM(MD1.MD1_VOC_VALUE)',
							'MD1_VOC_UNIT_NAME' => "if(MD1.MD1_VOC_unit=0, 'G/L', 'LBS')",
							'MD1_TOTAL_QTY' => 'SUM(d.LINE_ID)',
							'MD1_TOTAL_HITS' => 'count(d.LINE_ID)',
							'TREE_LEVEL' => new Expression(2),
							'CO1.CO1_ID',
							'LO1.LO1_ID',
							'MD1.MD1_ID'
						];
						break;
					case 3:
						$partName_Exp = new Expression("coalesce(CONCAT(CONVERT(d.LINE_TYPE, CHAR), '#', CONVERT(d.HEADER_NUMBER, CHAR), '-', CONVERT(d.LINE_NUMBER, CHAR), '-', CONVERT(MD1.MD1_PART_NUMBER, CHAR)), 'N/A')");
						$fields = [
							'TREE_HEADER' => $partName_Exp,
							'TE1_NAME' => 'TE1.TE1_NAME',
							'MD1_DESC1' => 'MD1.MD1_DESC1',
							'MD1_PART_NUMBER' => 'MD1.MD1_PART_NUMBER',
							'MD1_VOC_CATA' => 'MD1.MD1_VOC_CATA',
							'MD1_VOC_MFG' => 'MD1.MD1_VOC_MFG',
							'COATING' => new Expression(1),
							'BY_WEIGHT' => new Expression(2),
							'MD1_VOC_VALUE' => 'MD1.MD1_VOC_VALUE',
							'MD1_VOC_UNIT_NAME' => "if(MD1.MD1_VOC_unit=0, 'G/L', 'LBS')",
							'MD1_TOTAL_HITS' => new Expression(1),
							'MD1_TOTAL_QTY' => 'd.LINE_QTY',
							'TREE_LEVEL' => new Expression(3),
						];
						break;
					default:
						break;
				}
			}


			$orders_Exp = new Expression($isCentral ? "'Shop Order'" : "'Tech Scan'");

			$ordersQuery = OrderLine::find()->alias('OR2')->select([
				'LINE_TYPE' => $orders_Exp,
				'or2_id as LINE_ID',
				'OR2.or1_id as HEADER_ID',
				'or2_line as LINE_NUMBER',
				'or2_STATUS AS LINE_STATUS',
				'or2_ORDER_QTY AS LINE_QTY',
				'OR2.MD1_ID',
				'OR2.CA1_ID',
				'OR2.CO1_ID',
				'OR2.GL1_ID',
				'MD1_PART_NUMBER AS PART_NUMBER',
				'MD1_DESC1 AS PART_DESC',
				'UM1_ID',
				'MD1_UNIT_PRICE AS UNIT_PRICE',
				'CREATED_ON'  => 'DATE_FORMAT(' . $this->getLocalDate('OR2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
				'OR2_CREATED_BY AS CREATED_BY',
				'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('OR2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
				'OR2_CREATED_BY AS MODIFIED_BY',
				'OR1.TE1_ID',
				'OR2_TOTAL_VALUE AS TOTAL_VALUE',
				'OR2.LO1_TO_ID AS LO1_ID',
				'OR2.LO1_FROM_ID AS LO1_FROM_ID',
				'OR2.VD1_ID',
				'OR1_NUMBER AS HEADER_NUMBER',
				'OR1_STATUS AS HEADER_STATUS'
			])
				->leftJoin("or1_order_header as OR1" , "OR2.or1_id = OR1.or1_id")
				->andFilterWhere(['=', 'OR2.OR2_DELETE_FLAG', 0])
				->andFilterWhere(['is not', 'OR2.OR2_CREATED_ON', new \yii\db\Expression('null')])
				->andFilterWhere(['>=', 'DATE(OR2.OR2_CREATED_ON)', $params['START_DATE']])
				->andFilterWhere(['<=', 'DATE(OR2.OR2_CREATED_ON)', $params['END_DATE']]);

			if ($isCentral) {
				$ordersQuery->andFilterWhere(['!=', 'OR2.LO1_FROM_ID', ]);
			} else {
				$ordersQuery->andFilterWhere(['=', 'OR2.LO1_FROM_ID', new \yii\db\Expression('OR2.LO1_TO_ID')]);
			}

			if (!$isCentral) {
				$JobTicket_Exp = new Expression("'Job Ticket'");

				$jobLinesQuery = JobLine::find()->alias('JT2')->select([
					'LINE_TYPE' => $JobTicket_Exp,
					'jt2_id as LINE_ID',
					'JT2.jt1_id as HEADER_ID',
					'jt2_line as LINE_NUMBER',
					'JT2_STATUS AS LINE_STATUS',
					'JT2_ORDER_QTY AS LINE_QTY',
					'JT2.MD1_ID',
					'JT2.CA1_ID',
					'JT2.CO1_ID',
					'JT2.GL1_ID',
					'JT2_PART_NUMBER AS PART_NUMBER',
					'JT2_DESC1 AS PART_DESC',
					'UM1_ID',
					'JT2_UNIT_PRICE AS UNIT_PRICE',
					'CREATED_ON'  => 'DATE_FORMAT(' . $this->getLocalDate('JT2.JT2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
					'JT2_CREATED_BY AS CREATED_BY',
					'MODIFIED_ON'  => 'DATE_FORMAT(' . $this->getLocalDate('JT2.JT2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
					'JT2_CREATED_BY AS MODIFIED_BY',
					'TE1_ID',
					'JT2_TOTAL_VALUE AS TOTAL_VALUE',
					'JT2.LO1_ID',
					'JT2.LO1_ID AS LO1_FROM_ID',
					'JT2.VD1_ID',
					'JT1_NUMBER AS HEADER_NUMBER',
					'JT1_STATUS AS HEADER_STATUS'
				])
					->leftJoin("jt1_job_ticket as JT1" , "JT2.jt1_id = JT1.jt1_id")
					->andFilterWhere(['=', 'JT2.JT2_DELETE_FLAG', 0])
					->andFilterWhere(['is not', 'JT2.JT2_CREATED_ON', new \yii\db\Expression('null')])
					->andFilterWhere(['>=', 'DATE(JT2.JT2_CREATED_ON)', $params['START_DATE']])
					->andFilterWhere(['<=', 'DATE(JT2.JT2_CREATED_ON)', $params['END_DATE']]);
				$ordersQuery->union( $jobLinesQuery );
			}

			$query = self::find()->alias('MD1')->select($fields)
				->leftJoin(['d' => $ordersQuery],"d.MD1_ID = MD1.MD1_ID")
				->leftJoin("co1_company as CO1","CO1.co1_id = MD1.co1_id AND CO1.co1_delete_flag = 0")
				->leftJoin("md2_location as MD2","MD2.md1_id = MD1.md1_id AND MD2.LO1_ID=d.LO1_ID AND MD2.md2_delete_flag = 0")
				->leftJoin("te1_technician TE1","TE1.te1_id = d.te1_id AND TE1.te1_delete_flag = 0")
				->leftJoin("vd1_vendor VD1","VD1.vd1_id = MD2.vd1_id AND VD1.vd1_delete_flag = 0")
				->leftJoin("lo1_location LO1","LO1.lo1_id = MD2.lo1_id AND LO1.lo1_delete_flag = 0")
				->leftJoin("lo1_location LO_FROM","LO_FROM.lo1_id = d.lo1_from_id AND LO_FROM.lo1_delete_flag = 0")
				->leftJoin("ca1_category CA1","CA1.ca1_id = MD1.ca1_id AND CA1.ca1_delete_flag = 0")
				->leftJoin("um1_unit UM1","UM1.um1_id = MD2.um1_receipt_id AND UM1.um1_delete_flag = 0")
				->leftJoin("um2_conversion UM2","UM2.um1_from_id = MD1.um1_pricing_id AND UM2.um1_to_id = MD2.um1_receipt_id AND UM2.md1_id = MD1.md1_id")
				->andFilterWhere(['=', 'MD1.MD1_VOC_FLAG', 1])
				->andFilterWhere(['=', 'MD1.md1_delete_flag', 0])
				->andFilterWhere(['=', 'MD2.md2_delete_flag', 0]);


			if (isset($params['systemMethod']) && $params['systemMethod'] !== 'getpdf') {
				if($isVOC) {
					$query->andFilterWhere(['=', 'MD1.MD1_VOC_FLAG', 1]);
				}

				if($noUsage) {
					$query->andFilterWhere(['is', 'd.MD1_ID', new \yii\db\Expression('null')]);
				} else {
					$query->andFilterWhere(['is not', 'd.MD1_ID', new \yii\db\Expression('null')]);
				}
			}

			$user = Yii::$app->user->getIdentity();

			if ($user->CO1_ID && $user->CO1_ID > 0) {
				$query->andFilterWhere(['=', 'MD1.CO1_ID', $user->CO1_ID]);
			}

			if (isset($params['VD0_IDs'])) {
				$query->andWhere(['in', 'VD1.VD0_ID', $params['VD0_IDs']]);
			}

			if (isset($params['SEARCH'])) {
				$query->andFilterWhere(['or',
					['like', 'CO1.CO1_NAME', $params['SEARCH']],
					['like', 'LO1.LO1_NAME', $params['SEARCH']],
					['like', 'MD1.MD1_PART_NUMBER', $params['SEARCH']],
					['like', 'MD1.MD1_DESC1', $params['SEARCH']],
					['like', 'VD1.VD1_NAME', $params['SEARCH']],
					['like', 'CA1.CA1_NAME', $params['SEARCH']],
					['like', 'UM1.UM1_NAME', $params['SEARCH']]
				]);
			}

			if (isset($params['systemMethod']) && $params['systemMethod'] == 'getpdf') {
				if ($isTechUsage) {
					$query->orderBy(['CA1.CA1_ID' => SORT_ASC, 'MD1.MD1_ID' => SORT_ASC]);
				} else if($isVOC) {
					$query->orderBy(['MD1.MD1_ID' => SORT_ASC, 'd.CREATED_ON' => SORT_ASC]);
				}
			} else if (isset($params['TREE_ENABLED'])) {
				switch ($params['TREE_LEVEL']) {
					case 0:
						$query->groupBy(['MD1.CO1_ID']);
						break;
					case 1:
						$query->andWhere(['=', 'MD1.CO1_ID', $params['CO1_ID']]);
						$query->groupBy(['MD2.LO1_ID']);
						break;
					case 2:
						$query->andWhere(['=', 'MD1.CO1_ID', $params['CO1_ID']]);
						$query->andWhere(['=', 'LO1.LO1_ID', $params['LO1_ID']]);
						$query->groupBy(['MD1.MD1_ID']);
						break;
					case 4:
						$query->andWhere(['=', 'MD1.CO1_ID', $params['CO1_ID']]);
						$query->andWhere(['=', 'LO1.LO1_ID', $params['LO1_ID']]);
						$query->andWhere(['=', 'MD1.MD1_ID', $params['MD1_ID']]);
					default:
						break;
				}
			}

			$query->orderBy(['MD1.md1_part_number' => SORT_ASC, 'd.CREATED_ON' => SORT_DESC]);
		} else {
			$fields = [
				'LO1.LO1_NAME',
				'MD1.MD1_PART_NUMBER',
				'MD1.MD1_DESC1',
				'VD1.VD1_NAME',
				'CA1.CA1_NAME',
				'SUM(coalesce(OR2.OR2_ORDER_QTY, 0)+coalesce(JT2.JT2_ORDER_QTY, 0)) AS QTY',
				'UM1.UM1_NAME',
				'MD1.md1_unit_price/COALESCE(UM2.um2_factor, 1) AS MD2_UNIT_PRICE',
				'SUM((coalesce(OR2.OR2_ORDER_QTY,0)+coalesce(JT2.JT2_ORDER_QTY,0))*MD1.md1_unit_price/COALESCE(UM2.um2_factor, 1)) AS EXTENDED_PRICE'
				];
			$query = self::find()->alias('MD1')->select($fields)
				->leftJoin("co1_company as CO1","CO1.co1_id = MD1.co1_id AND CO1.co1_delete_flag = 0")
				->leftJoin("md2_location as MD2","MD2.md1_id = MD1.md1_id AND MD2.md2_delete_flag = 0")
				->leftJoin("jt2_job_line as JT2","JT2.md1_id = MD1.md1_id AND JT2.lo1_id = MD2.lo1_id AND JT2.jt2_delete_flag = 0 AND (JT2.JT2_CREATED_ON IS NOT NULL) AND DATE(JT2.JT2_CREATED_ON) >= " . $params['START_DATE'] . " AND DATE(JT2.JT2_CREATED_ON) <= " . $params['END_DATE'])
				->leftJoin("or2_order_line as OR2","OR2.md1_id = MD1.md1_id AND OR2.lo1_from_id = MD2.lo1_id AND OR2.lo1_to_id = MD2.lo1_id AND OR2.or2_delete_flag = 0 AND (OR2.OR2_CREATED_ON IS NOT NULL) AND DATE(OR2.OR2_CREATED_ON) >= " . $params['START_DATE'] . " AND DATE(OR2.OR2_CREATED_ON) <= " . $params['END_DATE'])
				->leftJoin("vd1_vendor VD1","VD1.vd1_id = MD2.vd1_id AND VD1.vd1_delete_flag = 0")
				->leftJoin("lo1_location LO1","LO1.lo1_id = MD2.lo1_id AND LO1.lo1_delete_flag = 0")
				->leftJoin("ca1_category CA1","CA1.ca1_id = MD1.ca1_id AND CA1.ca1_delete_flag = 0")
				->leftJoin("um1_unit UM1","UM1.um1_id = MD2.um1_receipt_id AND UM1.um1_delete_flag = 0")
				->leftJoin("um2_conversion UM2","UM2.um1_from_id = MD1.um1_pricing_id AND UM2.um1_to_id = MD2.um1_receipt_id AND UM2.md1_id = MD1.md1_id")
				->andFilterWhere(['=', 'MD1.md1_delete_flag', 0])
				->andFilterWhere(['=', 'MD2.md2_delete_flag', 0])
				->andFilterWhere(['or',
					['>', 'JT2.jt2_order_qty', 0],
					['>', 'OR2.or2_order_qty', 0]
				]);

			if (isset($params['SEARCH'])) {
				$query->andFilterWhere(['or',
					['like', 'CO1.CO1_NAME', $params['SEARCH']],
					['like', 'LO1.LO1_NAME', $params['SEARCH']],
					['like', 'MD1.MD1_PART_NUMBER', $params['SEARCH']],
					['like', 'MD1.MD1_DESC1', $params['SEARCH']],
					['like', 'VD1.VD1_NAME', $params['SEARCH']],
					['like', 'CA1.CA1_NAME', $params['SEARCH']],
					['like', 'UM1.UM1_NAME', $params['SEARCH']]
				]);
			}

			$query->groupBy(['MD1.MD1_ID']);
		}
		return $query;
	}
}
