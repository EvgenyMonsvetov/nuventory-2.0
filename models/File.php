<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "fi1_file".
 *
 * @property int $FI1_ID
 * @property int $CO1_ID
 * @property int $MD1_ID
 * @property string $FI1_FILE_NAME
 * @property int $FI1_FILE_TYPE
 * @property int $FI1_FILE_SIZE
 * @property string $FI1_FILE_PATH
 * @property int $FI1_DELETE_FLAG
 * @property int $FI1_CREATED_BY
 * @property string $FI1_CREATED_ON
 * @property int $FI1_MODIFIED_BY
 * @property string $FI1_MODIFIED_ON
 */
class File extends ActiveRecord
{
    protected $_tablePrefix = 'FI1';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fi1_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CO1_ID', 'MD1_ID', 'FI1_FILE_TYPE', 'FI1_FILE_SIZE', 'FI1_DELETE_FLAG', 'FI1_CREATED_BY', 'FI1_MODIFIED_BY'], 'integer'],
            [['FI1_CREATED_ON', 'FI1_MODIFIED_ON'], 'safe'],
            [['FI1_FILE_NAME'], 'string', 'max' => 100],
            [['FI1_FILE_PATH'], 'string', 'max' => 300],
	        [['MD1_ID'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'FI1_ID' => 'Fi1  ID',
	        'CO1_ID' => 'Co1  ID',
	        'MD1_ID' => 'Md1  ID',
            'FI1_FILE_NAME' => 'Fi1  File  Name',
            'FI1_FILE_TYPE' => 'Fi1  File  Type',
            'FI1_FILE_SIZE' => 'Fi1  File  Size',
            'FI1_FILE_PATH' => 'Fi1  File  Path',
            'FI1_DELETE_FLAG' => 'Fi1  Delete  Flag',
            'FI1_CREATED_BY' => 'Fi1  Created  By',
            'FI1_CREATED_ON' => 'Fi1  Created  On',
            'FI1_MODIFIED_BY' => 'Fi1  Modified  By',
            'FI1_MODIFIED_ON' => 'Fi1  Modified  On',
        ];
    }

	public function beforeDelete()
	{
		$url =  str_replace($this->FI1_FILE_NAME, '', $this->FI1_FILE_PATH);
		$filePath = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['partsImagesPath'] . DIRECTORY_SEPARATOR . $url;
		if (file_exists($filePath)) {
			FileHelper::removeDirectory($filePath);
		}
		return parent::beforeDelete();
	}

	public function afterDelete()
	{
		$masterDataModel = MasterData::findOne(['MD1_ID' => $this->MD1_ID]);
		if ($masterDataModel) {
			$masterDataModel->FI1_ID = 0;
			if (!$masterDataModel->save(true)) {
				throw new Exception('', $masterDataModel->getErrors(), MasterData::$MODEL_ERROR);
			}
		}

		return parent::afterDelete();
	}

    public function getQuery($params)
    {
        $query = File::find()->alias('FI1')
                    ->select([  'FI1.FI1_ID', 'FI1.CO1_ID', 'FI1.MD1_ID', 'FI1.FI1_FILE_NAME', 'FI1.FI1_FILE_PATH', 'FI1.FI1_DELETE_FLAG']);
        $query->where(['=', 'FI1_DELETE_FLAG', 0]);
        $query->orderBy(['FI1_ID' => SORT_DESC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['FI1_ID'] = SORT_DESC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $params['perPage']
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getFile($id)
    {
        $query = $this->getQuery(null);
        $query->where(['=', 'FI1.FI1_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }
}
