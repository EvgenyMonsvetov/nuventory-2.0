<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "in2_invoice_line".
 *
 * @property int $IN2_ID
 * @property int $IN1_ID
 * @property int $OR2_ID
 * @property int $GL1_ID
 * @property int $IN2_LINE
 * @property int $IN2_STATUS
 * @property double $IN2_INVOICE_QTY
 * @property int $MD1_ID
 * @property int $CA1_ID
 * @property int $CO1_ID
 * @property string $IN2_PART_NUMBER
 * @property string $IN2_DESC1
 * @property string $IN2_DESC2
 * @property double $UM1_ID
 * @property string $IN2_UNIT_PRICE
 * @property int $IN2_DELETE_FLAG
 * @property int $IN2_CREATED_BY
 * @property string $IN2_CREATED_ON
 * @property int $IN2_MODIFIED_BY
 * @property string $IN2_MODIFIED_ON
 * @property int $LO1_ID
 * @property int $IN2_UM_SIZE
 * @property string $IN2_TOTAL_VALUE
 * @property int $IN2_LINK
 */
class InvoiceLine extends ActiveRecord
{
    protected $_tablePrefix = 'IN2';

    public $COMPLETED_ON;
    public $MODIFIED_BY;
    public $CREATED_BY;
    public $CREATED_ON;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'in2_invoice_line';
    }

    public static function primaryKey()
    {
        return ['IN2_ID'];
    }

    public function fields() {
        $fields = parent::fields();
        $fields["COMPLETED_ON"] = function($model){ return $model->COMPLETED_ON; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IN1_ID', 'OR2_ID', 'GL1_ID', 'IN2_LINE', 'IN2_STATUS', 'MD1_ID', 'CA1_ID', 'CO1_ID', 'IN2_DELETE_FLAG', 'IN2_CREATED_BY', 'IN2_MODIFIED_BY', 'LO1_ID', 'IN2_UM_SIZE', 'IN2_LINK'], 'integer'],
            [['IN2_INVOICE_QTY', 'UM1_ID', 'IN2_UNIT_PRICE', 'IN2_TOTAL_VALUE'], 'number'],
            [['IN2_CREATED_ON', 'IN2_MODIFIED_ON'], 'safe'],
	        [['LO1_ID', 'IN2_UM_SIZE', 'IN2_TOTAL_VALUE'], 'required'],
            [['IN2_PART_NUMBER', 'COMPLETED_ON', 'MODIFIED_BY', 'CREATED_BY', 'CREATED_ON'], 'string', 'max' => 100],
            [['IN2_DESC1', 'IN2_DESC2'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IN2_ID' => 'In2  ID',
            'IN1_ID' => 'In1  ID',
            'OR2_ID' => 'Or2  ID',
            'GL1_ID' => 'Gl1  ID',
            'IN2_LINE' => 'In2  Line',
            'IN2_STATUS' => 'In2  Status',
            'IN2_INVOICE_QTY' => 'In2  Invoice  Qty',
            'MD1_ID' => 'Md1  ID',
            'CA1_ID' => 'Ca1  ID',
            'CO1_ID' => 'Co1  ID',
            'IN2_PART_NUMBER' => 'In2  Part  Number',
            'IN2_DESC1' => 'In2  Desc1',
            'IN2_DESC2' => 'In2  Desc2',
            'UM1_ID' => 'Um1  ID',
            'IN2_UNIT_PRICE' => 'In2  Unit  Price',
            'IN2_DELETE_FLAG' => 'In2  Delete  Flag',
            'IN2_CREATED_BY' => 'In2  Created  By',
            'IN2_CREATED_ON' => 'In2  Created  On',
            'IN2_MODIFIED_BY' => 'In2  Modified  By',
            'IN2_MODIFIED_ON' => 'In2  Modified  On',
            'LO1_ID' => 'Lo1  ID',
            'IN2_UM_SIZE' => 'In2  Um  Size',
            'IN2_TOTAL_VALUE' => 'In2  Total  Value',
            'IN2_LINK' => 'In2  Link',
        ];
    }

    public function getQuery($params)
    {
        $query = InvoiceLine::find()->alias('IN2')
                    ->select([  'IN2.IN2_ID', 'IN2.IN1_ID', 'IN1.IN1_NUMBER', 'IN2.IN2_LINE', 'IN2.IN2_STATUS', 'IN2.MD1_ID', 'IN2.CO1_ID',
                                'CONCAT(i1.IN1_NUMBER, " - ", i2.IN2_LINE) AS IN2_LINK_FROM', 'IN2.IN2_DELETE_FLAG',
                                'if(IN2.IN2_STATUS=1, "Received", "Open") AS IN2_INVOICE_STATUS_TEXT',
                                'IN2.LO1_ID', 'IN2.CA1_ID', 'IN1.GL1_ID', 'IN2.IN2_PART_NUMBER', 'IN2.IN2_DESC1', 'IN2.IN2_DESC2', 'UM1.UM1_NAME',
                                'CA1.CA1_NAME', 'GL1.GL1_NAME', 'IN2.IN2_UM_SIZE', 'IN2.IN2_INVOICE_QTY', 'IN2.IN2_UNIT_PRICE',
                                '(IN2.IN2_INVOICE_QTY*IN2.IN2_UNIT_PRICE) AS IN2_EXTENDED_VALUE', 'IN2.IN2_TOTAL_VALUE', 'IN1.IN1_STATUS',
			                    'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IN2.IN2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IN2.IN2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'IN2.IN2_MODIFIED_BY', 'IN2.IN2_CREATED_BY',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("in1_invoice_header as IN1" , "IN1.IN1_ID = IN2.IN1_ID")
                    ->leftJoin("in2_invoice_line as i2" , "i2.IN2_ID = IN2.IN2_LINK")
                    ->leftJoin("in1_invoice_header as i1" , "i2.IN1_ID = i1.IN1_ID")
                    ->leftJoin("ca1_category as CA1" , "CA1.CA1_ID = IN2.CA1_ID")
                    ->leftJoin("gl1_glcode as GL1" , "GL1.GL1_ID = IN2.GL1_ID")
                    ->leftJoin("um1_unit as UM1" , "UM1.UM1_ID = IN2.UM1_ID")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = IN2.IN2_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = IN2.IN2_MODIFIED_BY");
        $query->where(['=', 'IN2.IN2_DELETE_FLAG', 0]);
        $query->orderBy(['IN2_LINE' => SORT_ASC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->IN1_ID)) {
            $query->andFilterWhere(['=', 'IN2.IN1_ID', $this->IN1_ID]);
        }

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['IN2_LINE'] = SORT_ASC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getInvoiceLines($params)
    {
        $query = self::find()->alias('IN2')
                    ->select([
                    	'IN2.*',
	                    'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IN2.IN2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
	                    'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IN2.IN2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")'
                    ])
                    ->leftJoin("in2_invoice_line as I2" , "I2.IN2_ID = IN2.IN2_LINK AND I2.IN2_DELETE_FLAG=0");
        $query->where(['=',  'IN2.IN2_DELETE_FLAG', 0]);
        $query->where(['is', 'I2.IN2_ID', new \yii\db\Expression('null')]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->IN1_ID)) {
            $query->andFilterWhere(['=', 'IN2.IN1_ID', $this->IN1_ID]);
        }

	    return $query->all();
    }

	public function getPdfQuery($params)
	{
		$query = InvoiceLine::find()->alias('IN2')
			->select(['IN2.IN2_ID', 'IN2.IN1_ID', 'IN2.IN2_LINE', 'IN2.IN2_INVOICE_QTY', 'IN2.CA1_ID', 'IN2.IN2_UNIT_PRICE',
				'IN2.IN2_TOTAL_VALUE', 'CONCAT(link1.IN1_NUMBER," - ", link2.IN2_LINE) AS IN2_LINK_FROM',
				'CA1.CA1_NAME', 'MD1.MD1_PART_NUMBER', 'MD1.MD1_VENDOR_PART_NUMBER', 'MD1.MD1_DESC1',
				'MD1.MD1_DESC2', 'um1.UM1_SHORT_CODE', 'coalesce(um1.UM1_SIZE, 1) AS UM1_SIZE',
				'IN1.IN1_ID', 'IN1.OR1_ID'])
			->leftJoin("or2_order_line as OR2", "OR2.OR2_ID = IN2.OR2_ID")
			->leftJoin("in1_invoice_header as IN1", "IN1.IN1_ID = IN2.IN1_ID")
			->leftJoin("in2_invoice_line as link2", "link2.IN2_ID = IN2.IN2_LINK")
			->leftJoin("in1_invoice_header as link1", "link1.IN1_ID = link2.IN1_ID")
			->leftJoin("md1_master_data as MD1", "MD1.MD1_ID = IN2.MD1_ID")
			->leftJoin("md2_location as MD2", "MD2.MD1_ID = MD1.MD1_ID AND MD2.LO1_ID = OR2.LO1_TO_ID")
			->leftJoin("um1_unit as um1", "if(OR2.LO1_TO_ID = OR2.LO1_FROM_ID, MD2.UM1_RECEIPT_ID, MD2.UM1_PURCHASE_ID) = um1.UM1_ID")
			->leftJoin("ca1_category as CA1", "CA1.CA1_ID = IN2.CA1_ID");
		$query->where(['=', 'IN2.IN2_DELETE_FLAG', 0]);
		$query->orderBy(['IN2.IN1_ID' => SORT_ASC, 'IN2.IN2_LINE' => SORT_ASC]);

		if ($params && !empty($params)) {
			$this->load($params, '');
		}

		if (isset($params['IN1_IDs'])) {
			$query->andFilterWhere(['IN', 'IN1.IN1_ID', explode(',', $params['IN1_IDs'])]);
		}

		if (isset($params['OR1_IDs'])) {
			$query->andFilterWhere(['IN', 'IN1.OR1_ID', explode(',', $params['OR1_IDs'])]);
		}

		return $query;
	}

	public function getPdfInvoiceLines($params)
	{
		$query = $this->getPdfQuery( $params );

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => false
		]);

		return $dataProvider;
	}
}
