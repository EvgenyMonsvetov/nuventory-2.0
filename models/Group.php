<?php

namespace app\models;

use Yii;
use app\models\scopes\GroupQuery;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "gr1_group".
 *
 * @property int $GR1_ID
 * @property string $GR1_NAME
 * @property string $GR1_DESC
 * @property int $GR1_ACCESS_LEVEL
 * @property int $GR1_DELETE_FLAG
 * @property string $ROLE_NAME
 */
class Group extends \yii\db\ActiveRecord
{
    const GROUP_ADMIN = 0;
    const GROUP_COMPANY_GROUP_MANAGER = 1;
    const GROUP_COMPANY_MANAGER = 2;
    const GROUP_STANDARD_USER = 3;
    const GROUP_LOCATION_MANAGER = 4;
    const GROUP_MANUFACTURER_MANAGER = 5;

    protected $_tablePrefix = 'GR1';

    public static function find()
    {
        return new GroupQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gr1_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['GR1_ID', 'GR1_ACCESS_LEVEL', 'GR1_DELETE_FLAG'], 'integer'],
            [['GR1_NAME', 'GR1_DESC', 'ROLE_NAME'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'GR1_ID' => 'Gr1  ID',
            'GR1_NAME' => 'Gr1  Name',
            'GR1_DESC' => 'Gr1  Desc',
            'GR1_ACCESS_LEVEL' => 'Gr1  Access  Level',
            'GR1_DELETE_FLAG' => 'Gr1  Delete  Flag',
            'ROLE_NAME' => 'Role Name',
        ];
    }

    public function search($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['GR1_NAME'] = SORT_ASC;
        }

        $query= self::find();
        $query->initScope();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => !empty($params['perPage']) ? $params['perPage'] : null
            ],
            'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }
}
