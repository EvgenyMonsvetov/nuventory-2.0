<?php

namespace app\models;

use app\models\scopes\HoursQuery;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "ho1_hour".
 *
 * @property int $HO1_ID
 * @property int $CO1_ID
 * @property int $LO1_ID
 * @property int $TE1_ID
 * @property string $HO1_HOURS
 * @property string $HO1_DATE
 * @property int $HO1_DELETE_FLAG
 * @property int $HO1_CREATED_BY
 * @property string $HO1_CREATED_ON
 * @property int $HO1_MODIFIED_BY
 * @property string $HO1_MODIFIED_ON
 */
class Hour extends ActiveRecord
{
	protected $_tablePrefix = 'HO1';

	CONST SCENARIO_ADD = 'add';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ho1_hour';
    }

    public static function find()
   {
        return new HoursQuery(get_called_class());
   }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CO1_ID', 'LO1_ID', 'TE1_ID', 'HO1_DELETE_FLAG', 'HO1_CREATED_BY', 'HO1_MODIFIED_BY'], 'integer'],
            [['HO1_HOURS'], 'number'],
            [['HO1_DATE', 'HO1_CREATED_ON', 'HO1_MODIFIED_ON', 'HO1_FORMATTED_DATE'], 'safe'],
	        [['TE1_ID', 'HO1_DATE', 'HO1_DELETE_FLAG'], 'unique', 'targetAttribute' => ['TE1_ID', 'HO1_DATE', 'HO1_DELETE_FLAG']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'HO1_ID' => 'Ho1  ID',
            'CO1_ID' => 'Co1  ID',
            'LO1_ID' => 'Lo1  ID',
            'TE1_ID' => 'Te1  ID',
            'HO1_HOURS' => 'Ho1  Hours',
            'HO1_DATE' => 'Ho1  Date',
            'HO1_DELETE_FLAG' => 'Ho1  Delete  Flag',
            'HO1_CREATED_BY' => 'Ho1  Created  By',
            'HO1_CREATED_ON' => 'Ho1  Created  On',
            'HO1_MODIFIED_BY' => 'Ho1  Modified  By',
            'HO1_MODIFIED_ON' => 'Ho1  Modified  On',
        ];
    }

	public function getQuery($params)
	{
		$query = self::find()->alias('HO1')
			->select([  'HO1.*',
				'HO1_FORMATTED_DATE' => 'DATE_FORMAT(HO1_DATE, "%m.%d.%Y")',
				'HO1_MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('HO1_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
				'HO1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('HO1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
				'CREATED_BY'  => 'us1_create.US1_NAME',
				'MODIFIED_BY' => 'us1_modify.US1_NAME'])
			->leftJoin("us1_user as us1_create", "us1_create.US1_ID = HO1.HO1_CREATED_BY")
			->leftJoin("us1_user as us1_modify", "us1_modify.US1_ID = HO1.HO1_MODIFIED_BY");

		if (isset($params['TECHNICIAN_DETAILS']) && $params['TECHNICIAN_DETAILS']) {
			$query->addSelect(['TE1.TE1_NAME', 'CO1_NAME', 'LO1_NAME']);
			$query->leftJoin("te1_technician as TE1", "TE1.TE1_ID = HO1.TE1_ID AND TE1.TE1_DELETE_FLAG = 0")
                  ->leftJoin("co1_company as CO1",  "CO1.CO1_ID = HO1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
                  ->leftJoin("lo1_location as LO1", "LO1.LO1_ID = HO1.LO1_ID AND LO1.LO1_DELETE_FLAG = 0");
			$query->andFilterWhere(['=', 'TE1.TE1_DELETE_FLAG', 0]);
		}

		if (isset($params['group_by_technican'])){
		    $query->addSelect(['TE1.TE1_NAME']);
			$query->leftJoin("te1_technician as TE1", "TE1.TE1_ID = HO1.TE1_ID AND TE1.TE1_DELETE_FLAG = 0");
            $query->andFilterWhere(['=', 'TE1.TE1_DELETE_FLAG', 0]);

			$params['endDate']   = date('Y-m-d');
			$params['startDate'] = date('Y-m-d', strtotime(date('Y-m-d').' -11 month'));
		}

		if (isset($params['TE1_NAME'])) {
			$query->andFilterWhere(['=', 'TE1.TE1_ID', $params['TE1_NAME']]);
		}

		$this->load($params, '');
		$query->initScope();

		if (isset($this->TE1_ID)) {
			$query->andFilterWhere(['=', 'HO1.TE1_ID', $this->TE1_ID]);
		}

		if (isset($params['TE1_IDs'])) {
			$query->andFilterWhere(['in', 'HO1.TE1_ID', $params['TE1_IDs']]);
		}

		if (isset($params['startDate'])) {
			$query->andFilterWhere(['>=', 'DATE(HO1.HO1_DATE)', $params['startDate']]);
		}

		if (isset($params['endDate'])){
            $query->andFilterWhere(['<=', 'DATE(HO1.HO1_DATE)', $params['endDate']]);
        }

        return $query;
	}

	public function getAllHours($params)
	{
		$query = $this->getQuery($params);

		$defaultOrder = [];
		if (!empty($params['sort'])) {
			foreach( $params['sort'] as $sort){
				list($prop, $dir) = explode(',', $sort);
				$defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
			}
		} else {
			$defaultOrder['HO1_DATE'] = SORT_DESC;
		}

		if (isset($params['perPage']) && isset($params['page'])) {
			$pagination = [
				'pageSize' => $params['perPage'],
				'page'     => $params['page']
			];
		} else {
			$pagination = false;
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => $pagination,
			'sort' => [
				'defaultOrder' => $defaultOrder
			]
		]);

		return $dataProvider;
	}

    public function getTechHours($startDate, $endDate, $params)
    {
        $query = Technician::find()->alias('TE1')
            ->select([
                'HO1.*',
                'HO1_DATE' => 'DATE_FORMAT(HO1_DATE, "%m.%d.%Y")',
                'HO1_MODIFIED_ON' => 'DATE_FORMAT(HO1_MODIFIED_ON, "%m.%d.%Y %H:%i")',
                'HO1_CREATED_ON' => 'DATE_FORMAT(HO1_CREATED_ON, "%m.%d.%Y %H:%i")',
	            'HO1_MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('HO1_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
	            'HO1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('HO1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                'TE1.TE1_NAME',
                'TE1.TE1_ID',
            ])
            ->leftJoin("ho1_hour as HO1" , "HO1.TE1_ID = TE1.TE1_ID AND DATE_FORMAT(HO1_DATE, '%Y-%m-%d') >= '$startDate' AND DATE_FORMAT(HO1_DATE, '%Y-%m-%d') <= '$endDate'");

        $query->initScope();

        return $query;
    }

}
