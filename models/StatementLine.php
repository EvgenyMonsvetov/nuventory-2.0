<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "is2_invoice_line".
 *
 * @property int $IS2_ID
 * @property int $IS1_ID
 * @property int $IN1_ID
 * @property int $GL1_ID
 * @property int $IS2_LINE
 * @property int $IS2_STATUS
 * @property string $IS2_INVOICED_AMOUNT
 * @property int $CA1_ID
 * @property int $CO1_ID
 * @property string $IS2_DELETE_FLAG
 * @property int $IS2_CREATED_BY
 * @property string $IS2_CREATED_ON
 * @property int $IS2_MODIFIED_BY
 * @property string $IS2_MODIFIED_ON
 */
class StatementLine extends ActiveRecord
{
	const IS2_STATUS_OPEN = 0;

    protected $_tablePrefix = 'IS2';

    public $GL1_NAME;
    public $MODIFIED_ON;
    public $MODIFIED_BY;
    public $CREATED_BY;
    public $CREATED_ON;
    public $IS2_DELETE_FLAG;
    public $IS1_IDs;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'is2_invoice_line';
    }

    public static function primaryKey()
    {
        return ['IS2_ID'];
    }

    public function fields() {
        $fields = parent::fields();
        $fields["GL1_NAME"] = function($model){ return $model->GL1_NAME; };
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["IS2_DELETE_FLAG"] = function($model){ return $model->IS2_DELETE_FLAG; };
        $fields["IS1_IDs"] = function($model){ return $model->IS1_IDs; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IS1_ID', 'IN1_ID', 'GL1_ID', 'IS2_LINE', 'IS2_STATUS', 'CA1_ID', 'CO1_ID', 'IS2_CREATED_BY', 'IS2_MODIFIED_BY'], 'integer'],
            [['IS2_INVOICED_AMOUNT'], 'number'],
            [['IS2_CREATED_ON', 'IS2_MODIFIED_ON'], 'safe'],
            [['IS1_IDs'], 'string'],
            [['IS2_DELETE_FLAG', 'GL1_NAME', 'MODIFIED_ON', 'MODIFIED_BY', 'CREATED_BY', 'CREATED_ON'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IS2_ID' => 'Is2  ID',
            'IS1_ID' => 'Is1  ID',
            'IN1_ID' => 'In1  ID',
            'GL1_ID' => 'Gl1  ID',
            'IS2_LINE' => 'Is2  Line',
            'IS2_STATUS' => 'Is2  Status',
            'IS2_INVOICED_AMOUNT' => 'Is2  Invoiced  Amount',
            'CA1_ID' => 'Ca1  ID',
            'CO1_ID' => 'Co1  ID',
            'IS2_DELETE_FLAG' => 'Is2  Delete  Flag',
            'IS2_CREATED_BY' => 'Is2  Created  By',
            'IS2_CREATED_ON' => 'Is2  Created  On',
            'IS2_MODIFIED_BY' => 'Is2  Modified  By',
            'IS2_MODIFIED_ON' => 'Is2  Modified  On',
        ];
    }

    public function getQuery($params)
    {
        $query = StatementLine::find()->alias('IS2')
                    ->select([  'IS2.IS2_ID', 'IS2.IS1_ID', 'IS2.IN1_ID', 'IN1.IN1_NUMBER', 'IS2.IS2_LINE', 'GL1.GL1_ID', 'GL1.GL1_NAME',
                                'IS2.IS2_DELETE_FLAG', 'IS2.CO1_ID', 'IN1.IN1_TOTAL_VALUE',
			                    'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IS2.IS2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON'  => 'DATE_FORMAT(' . $this->getLocalDate('IS2.IS2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'IS2.IS2_MODIFIED_BY', 'IS2.IS2_CREATED_BY',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("in1_invoice_header as IN1" , "IN1.IN1_ID = IS2.IN1_ID")
                    ->leftJoin("gl1_glcode as GL1" , "GL1.GL1_ID = IS2.GL1_ID")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = IS2.IS2_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = IS2.IS2_MODIFIED_BY");
        $query->where(['=', 'IS2.IS2_DELETE_FLAG', 0]);
        $query->orderBy(['IS2_LINE' => SORT_ASC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->IS1_ID)) {
            $query->andFilterWhere(['=', 'IS2.IS1_ID', $this->IS1_ID]);
        }

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['IS2_LINE'] = SORT_ASC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

	public function getPdfQuery($params)
	{
		$query = self::find()->alias('IS2')
			->select(['IS2.IS1_ID', 'IS2.IS2_ID', 'IS2.IS2_LINE', 'IS2.IS2_INVOICED_AMOUNT', 'IS2.CA1_ID', 'IS2.GL1_ID',
				'CA1.CA1_NAME', 'gl1.GL1_NAME', 'gl1.GL1_DESCRIPTION', 'in1.IN1_NUMBER', 'lfrom.LO1_NAME AS LO1_FROM_NAME',
				'lto.LO1_NAME AS LO1_TO_NAME', 'in1.IN1_TOTAL_VALUE', 'in1.IN1_COMMENT', 'in1.IN1_STATUS', 'in1.IN1_SUM_STATUS',
                'IN1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('in1.IN1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',

			])
			->leftJoin("is1_invoice_header AS is1", "is1.IS1_ID = IS2.IS1_ID")
			->leftJoin("in1_invoice_header AS in1", "in1.IN1_ID = IS2.IN1_ID")
			->leftJoin("lo1_location AS lfrom", "lfrom.LO1_ID = in1.LO1_FROM_ID")
			->leftJoin("lo1_location AS lto", "lto.LO1_ID=in1.LO1_TO_ID")
			->leftJoin("ca1_category AS ca1", "ca1.CA1_ID = is2.CA1_ID")
			->leftJoin("gl1_glcode AS gl1", "gl1.GL1_ID = is2.GL1_ID");
		$query->where(['=', 'IS2.IS2_DELETE_FLAG', 0]);
		$query->orderBy(['IS2.IS1_ID' => SORT_ASC, 'IS2.IS2_LINE' => SORT_ASC]);

		if ($params && !empty($params)) {
			$this->load($params, '');
		}

		if (isset($this->IS1_IDs)) {
			$query->andFilterWhere(['IN', 'IS1.IS1_ID', explode(',', $this->IS1_IDs)]);
		}

		return $query;
	}

	public function getPdfStatementLines($params)
	{
		$query = $this->getPdfQuery( $params );
		return $query->asArray()->all();
	}

	public function getInvoiceGLCodesQuery($params)
	{
		$query = self::find()->alias('is2')
			->select(['IS1.IS1_ID', 'is2.IS2_ID', 'gl1.GL1_ID',
				'coalesce(GL1_NAME, "None") AS GL1_NAME',
				'coalesce(GL1_DESCRIPTION, "No GL Code") AS GL1_DESCRIPTION',
				'SUM(in2.IN2_TOTAL_VALUE) AS GL1_SUBTOTAL'])
			->leftJoin("is1_invoice_header IS1", "IS1.IS1_ID = is2.IS1_ID")
			->leftJoin("in2_invoice_line in2", "in2.IN1_ID = is2.IN1_ID")
			->leftJoin("gl1_glcode gl1", "gl1.GL1_ID = in2.GL1_ID");
		$query->groupBy(['IS1.IS1_ID', 'gl1.GL1_ID']);

		if ($params && !empty($params)) {
			$this->load($params, '');
		}

		if (isset($this->IS1_IDs)) {
			$query->andFilterWhere(['IN', 'IS1.IS1_ID', explode(',', $this->IS1_IDs)]);
		}

		return $query;
	}

	public function getInvoiceGLCodes($params)
	{
		$query = $this->getInvoiceGLCodesQuery( $params );
		$query->indexBy = function($array, $default){
		    return $array['IS1_ID'] . '_' . $array['GL1_ID'];
        };
		return $query->asArray()->all();
	}
}
