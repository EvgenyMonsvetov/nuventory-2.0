<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "jt2_job_line".
 *
 * @property int $JT2_ID
 * @property int $JT1_ID
 * @property int $JT2_LINE
 * @property int $JT2_STATUS
 * @property double $JT2_ORDER_QTY
 * @property int $MD1_ID
 * @property int $CA1_ID
 * @property int $CO1_ID
 * @property int $GL1_ID
 * @property string $JT2_PART_NUMBER
 * @property string $JT2_DESC1
 * @property string $JT2_DESC2
 * @property double $UM1_ID
 * @property string $JT2_UNIT_PRICE
 * @property string $JT2_DELETE_FLAG
 * @property int $JT2_CREATED_BY
 * @property string $JT2_CREATED_ON
 * @property int $JT2_MODIFIED_BY
 * @property string $JT2_MODIFIED_ON
 * @property int $TE1_ID
 * @property string $JT2_TOTAL_VALUE
 * @property int $LO1_ID
 * @property int $VD1_ID
 */
class JobLine extends ActiveRecord
{
    protected $_tablePrefix = 'JT2';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jt2_job_line';
    }

    public static function primaryKey()
    {
        return ['JT2_ID'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JT1_ID', 'JT2_LINE', 'JT2_STATUS', 'MD1_ID', 'CA1_ID', 'CO1_ID', 'GL1_ID', 'JT2_CREATED_BY', 'JT2_MODIFIED_BY', 'TE1_ID', 'LO1_ID', 'VD1_ID'], 'integer'],
            [['JT2_ORDER_QTY', 'UM1_ID', 'JT2_UNIT_PRICE', 'JT2_TOTAL_VALUE'], 'number'],
            [['JT2_CREATED_ON', 'JT2_MODIFIED_ON'], 'safe'],
            [['JT2_PART_NUMBER'], 'string', 'max' => 100],
            [['JT2_DESC1', 'JT2_DESC2'], 'string', 'max' => 300],
            [['JT2_DELETE_FLAG'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'JT2_ID' => 'Jt2  ID',
            'JT1_ID' => 'Jt1  ID',
            'JT2_LINE' => 'Jt2  Line',
            'JT2_STATUS' => 'Jt2  Status',
            'JT2_ORDER_QTY' => 'Jt2  Order  Qty',
            'MD1_ID' => 'Md1  ID',
            'CA1_ID' => 'Ca1  ID',
            'CO1_ID' => 'Co1  ID',
            'GL1_ID' => 'Gl1  ID',
            'JT2_PART_NUMBER' => 'Jt2  Part  Number',
            'JT2_DESC1' => 'Jt2  Desc1',
            'JT2_DESC2' => 'Jt2  Desc2',
            'UM1_ID' => 'Um1  ID',
            'JT2_UNIT_PRICE' => 'Jt2  Unit  Price',
            'JT2_DELETE_FLAG' => 'Jt2  Delete  Flag',
            'JT2_CREATED_BY' => 'Jt2  Created  By',
            'JT2_CREATED_ON' => 'Jt2  Created  On',
            'JT2_MODIFIED_BY' => 'Jt2  Modified  By',
            'JT2_MODIFIED_ON' => 'Jt2  Modified  On',
            'TE1_ID' => 'Te1  ID',
            'JT2_TOTAL_VALUE' => 'Jt2  Total  Value',
            'LO1_ID' => 'Lo1  ID',
            'VD1_ID' => 'Vd1  ID',
        ];
    }

    public function getQuery($params)
    {
        $query = JobLine::find()->alias('JT2')
                    ->select([  'JT2.JT2_ID', 'JT2.JT1_ID', 'JT1.JT1_NUMBER', 'JT2.JT2_LINE', 'JT2.JT2_STATUS', 'JT2.MD1_ID', 'JT2.CO1_ID',
                                'JT2.JT2_PART_NUMBER', 'JT2.JT2_DESC1', 'JT2.JT2_DESC2', 'JT2.JT2_ORDER_QTY', 'JT2.JT2_UNIT_PRICE', 'JT2.JT2_TOTAL_VALUE',
                                'TE1.TE1_ID', 'TE1.TE1_SHORT_CODE', 'TE1.TE1_NAME', 'GL1.GL1_NAME', 'um1_receive.UM1_NAME AS UM1_RECEIVE_NAME', 'um1_unit.UM1_NAME',
                                'if(JT2.JT2_STATUS = 0, "Received", if(JT2.JT2_STATUS = 3, "Invoiced", "Open")) AS JT2_STATUS_TEXT',
			                    'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('JT2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('JT2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'JT2.JT2_MODIFIED_BY', 'JT2.JT2_CREATED_BY',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("jt1_job_ticket as JT1" , "JT1.JT1_ID = JT2.JT1_ID")
                    ->leftJoin("te1_technician as TE1" , "TE1.TE1_ID = JT2.TE1_ID")
                    ->leftJoin("md1_master_data as MD1" , "MD1.MD1_ID = JT2.MD1_ID")
                    ->leftJoin("gl1_glcode as GL1" , "GL1.GL1_ID = MD1.GL1_ID AND GL1.GL1_DELETE_FLAG = 0")
                    ->leftJoin("md2_location as MD2" , "MD2.MD1_ID = JT2.MD1_ID AND MD2.LO1_ID = JT1.LO1_ID")
                    ->leftJoin("um1_unit" , "um1_unit.UM1_ID = MD1.UM1_PRICING_ID")
                    ->leftJoin("um1_unit AS um1_receive" , "um1_receive.UM1_ID = MD2.UM1_RECEIPT_ID")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = JT2.JT2_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = JT2.JT2_MODIFIED_BY");
        $query->where(['=', 'JT2.JT2_DELETE_FLAG', 0]);
        $query->orderBy(['JT1_NUMBER' => SORT_DESC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->JT1_ID)) {
            $query->andFilterWhere(['=', 'JT2.JT1_ID', $this->JT1_ID]);
        }

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['JT2_LINE'] = SORT_ASC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query' => $query,
           'pagination' => false,
           'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getJobLine($id, $params)
    {
        $query = JobLine::find()->alias('JT2')
            ->select([
            	'*',
	            'JT2_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('JT2.JT2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
	            'JT2_MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('JT2.JT2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")'
            ])
            ->leftJoin("md1_master_data as MD1" , "MD1.MD1_ID = JT2.MD1_ID")
            ->leftJoin("jt1_job_ticket as JT1" , "JT1.JT1_ID = JT2.JT1_ID")
            ->leftJoin("md2_location as MD2" , "MD2.MD1_ID = JT2.MD1_ID AND MD2.LO1_ID = JT1.LO1_ID")
            ->leftJoin("te1_technician as TE1" , "TE1.TE1_ID = JT2.TE1_ID")
            ->leftJoin("um1_unit" , "um1_unit.UM1_ID = MD1.UM1_PRICING_ID");
         $query->where(['=', 'JT2.JT2_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function beforeDelete()
    {
        $sql = "UPDATE jt2_job_line jt2
                    left join md2_location md2 on md2.MD1_ID = jt2.MD1_ID and md2.LO1_ID = jt2.LO1_ID and md2.md2_delete_flag=0
                SET 
                    jt2.JT2_DELETE_FLAG = '1',
                    md2.MD2_ON_HAND_QTY   = md2.MD2_ON_HAND_QTY + if(coalesce(md2.MD2_NO_INVENTORY,0)=1, 0, coalesce(jt2.JT2_ORDER_QTY,0)), 
                    md2.MD2_AVAILABLE_QTY = md2.MD2_AVAILABLE_QTY + if(coalesce(md2.MD2_NO_INVENTORY,0)=1, 0, coalesce(jt2.JT2_ORDER_QTY,0))
                WHERE
                    jt2.JT2_ID = :JT2_ID and jt2.JT2_DELETE_FLAG=0;";

        $result = Yii::$app->db->createCommand($sql, [
            ':JT2_ID' => $this->JT2_ID
        ])->execute();

        return parent::beforeDelete();
    }
}
