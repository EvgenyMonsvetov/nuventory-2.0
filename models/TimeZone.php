<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "time_zone_name".
 *
 * @property string $Name
 * @property int $Time_zone_id
 */
class TimeZone extends ActiveRecord
{
    public $TimezoneName;
    public $Region;
    public $OFFSET;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'time_zone_name';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbsys');
    }

    public function fields() {
        $fields = parent::fields();
        $fields["TzName"] = function($model){ return $model->TzName; };
        $fields["Region"] = function($model){ return $model->Region; };
        $fields["OFFSET"] = function($model){ return $model->OFFSET; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Name', 'Time_zone_id'], 'required'],
            [['Time_zone_id'], 'integer'],
            [['Name'], 'string', 'max' => 64],
            [['Region'], 'string', 'max' => 64],
            [['Name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Name' => 'Name',
            'Time_zone_id' => 'Time Zone ID',
            'TzName' => 'Time Zone Name',
            'Region' => 'Region',
            'OFFSET' => 'Offset'
        ];
    }

    public function search()
    {
        $query = (new \yii\db\Query)->select([
            'Name'          =>"SUBSTRING_INDEX(SUBSTRING_INDEX(name , '/', 1), '/', -1)",
            'TzName'        => "TRIM( SUBSTR(name , LOCATE('/', name )+1) )",
            'OFFSET'        => "(unix_timestamp(CONVERT_TZ(utc_timestamp(), 'UTC', NAME))-unix_timestamp(utc_timestamp()))/60",
            'Time_zone_id'  => 'Time_zone_id'
        ])->from(['TZ'=>'mysql.time_zone_name']);

        return $query;
    }
}