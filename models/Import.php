<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\scopes\ImportQuery;

/**
 * This is the model class for table 'im1_import'.
 *
 * The followings are the available columns in table 'im1_import':
 * @property int $id
 * @property string $im1_file_name
 * @property string $im1_file_path
 * @property int $im1_new_records
 * @property int $im1_updated_records
 * @property int $im1_ignored_records
 * @property int $im1_processed_records
 * @property int $im1_total_records
 * @property string $im1_type
 * @property int $created_by
 * @property string $created_on
 * @property int $modified_by
 * @property string $modified_on
 * @property int $delete_flag
 * @property int $update_flag
 * @property int $im1_co1_id
 * @property int $im1_lo1_id
 *
 *
 * The followings are the available model relations:
 * @property Profile $cprofile
 * @property Profile $mprofile
 */

class Import extends ActiveRecord
{
    protected $createdAtAttribute   = 'created_on';
    protected $updatedAtAttribute   = 'modified_on';
    protected $createdByAttribute   = 'created_by';
    protected $updatedByAttribute   = 'modified_by';
    protected $deletedAtAttribute   = 'delete_flag';
    protected $useAttributeFullName = true;

    const JOB_COST_SUMMARY = 1;
    const SALES_JOURNAL_REPORT = 2;

    protected $_tablePrefix = 'IM1';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'im1_import';
    }

    /**
     * @return ImportQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new ImportQuery(get_called_class());
    }

    /**
	 * @return \yii\db\Connection the database connection used by this AR class.
	 */
	public static function getDb()
	{
		return Yii::$app->get('dbdash');
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array(
            [['im1_type'], 'required'],
            [['delete_flag', 'update_flag', 'im1_co1_id', 'im1_lo1_id', 'im1_ignored_records', 'im1_processed_records'], 'integer'],
            [['im1_type', 'created_by', 'modified_by'], 'integer'],
            //[['import_file', 'modified_on', ''], 'safe'],
            //[['import_file'], 'validateFile'],
            [['id', 'im1_file_name', 'im1_file_path', 'im1_new_records', 'im1_updated_records', 'im1_total_records', 'im1_type', 'created_by', 'created_on', 'modified_by', 'modified_on', 'delete_flag', 'update_flag', 'cprofile_search', 'mprofile_search'], 'safe', 'on' => 'search'],
        );
    }

    /**
     * Sets the validator model to look for the file types xlsx
     * $author Alex Lappen
     * $date 9/1/2016
     * @param $attribute
     */
    public function validateFile($attribute) {
        /*
        $importUploadFile = CUploadedFile::getInstance($this, $attribute);
        if(isset($importUploadFile)){
            $validator = new CFileValidator;
            $validator->attributes = array($attribute);
            $validator->types = 'xlsx';
            $validator->validate($this,array($attribute));
        }*/
        return true;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'im1_file_name' => 'File Name',
            'im1_file_path' => 'File Name',
            'im1_new_records' => 'New Records',
            'im1_updated_records' => 'Updated Records',
            'im1_total_records' => 'Total Records',
            'im1_ignored_records' => 'Ignored Records',
            'im1_type' => 'Import Type',
            'im1_lo1_id' => 'Location Id',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'modified_by' => 'Imported By',
            'modified_on' => 'Imported On',
            'delete_flag' => 'Delete Flag',
            'update_flag' => 'Update Flag',
            'cprofile_search' => 'Created By',
            'mprofile_search' => 'Imported By'
        ];
    }

    /**
     * REQUIRED
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search($params=[])
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['id'] = SORT_DESC;
        }

        $query = self::find()->alias('IM1')->select([
            'IM1.*'
        ]);

        $query->initScope();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => !empty($params['perPage']) ? $params['perPage'] : null
            ],
            'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }
}

