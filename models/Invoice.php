<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use app\models\scopes\InvoiceQuery;

/**
 * This is the model class for table "in1_invoice_header".
 *
 * @property int $IN1_ID
 * @property int $OR1_ID
 * @property int $GL1_ID
 * @property int $CO1_ID
 * @property int $US1_ID
 * @property int $LO1_FROM_ID
 * @property int $LO1_TO_ID
 * @property string $IN1_NUMBER
 * @property int $IN1_STATUS
 * @property int $IN1_SUM_STATUS
 * @property string $IN1_TOTAL_VALUE
 * @property string $IN1_COMMENT
 * @property int $IN1_DELETE_FLAG
 * @property string $IN1_CREATED_ON
 * @property int $IN1_CREATED_BY
 * @property string $IN1_MODIFIED_ON
 * @property int $IN1_MODIFIED_BY
 */
class Invoice extends ActiveRecord
{
	CONST INVOICE_STATUS_OPEN = 0;

	protected $_tablePrefix = 'IN1';

    public $COMPLETED_ON;
    public $MODIFIED_BY;
    public $CREATED_BY;
    public $CREATED_ON;
    public $LO1_ID;
    public $LO1_FROM_NAME;
    public $LO1_TO_NAME;
    public $IN1_IDs;
    public $OR1_IDs;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'in1_invoice_header';
    }

    public static function primaryKey()
    {
        return ['IN1_ID'];
    }

    public static function find()
    {
        return new InvoiceQuery(get_called_class());
    }

    public function fields() {
        $fields = parent::fields();
        $fields["COMPLETED_ON"] = function($model){ return $model->COMPLETED_ON; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["LO1_ID"] = function($model){ return $model->LO1_ID; };
        $fields["LO1_FROM_NAME"] = function($model){ return $model->LO1_FROM_NAME; };
        $fields["LO1_TO_NAME"] = function($model){ return $model->LO1_TO_NAME; };
        $fields["IN1_IDs"] = function($model){ return $model->IN1_IDs; };
        $fields["OR1_IDs"] = function($model){ return $model->OR1_IDs; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['OR1_ID', 'GL1_ID', 'CO1_ID', 'US1_ID', 'LO1_FROM_ID', 'LO1_TO_ID', 'IN1_STATUS', 'IN1_SUM_STATUS', 'IN1_DELETE_FLAG', 'IN1_CREATED_BY', 'IN1_MODIFIED_BY'], 'integer'],
            [['IN1_TOTAL_VALUE', 'LO1_ID', 'LO1_FROM_NAME' , 'LO1_TO_NAME'], 'number'],
            [['IN1_COMMENT', 'COMPLETED_ON', 'MODIFIED_BY', 'CREATED_BY', 'CREATED_ON', 'IN1_IDs', 'OR1_IDs'], 'string'],
            [['IN1_CREATED_ON', 'IN1_MODIFIED_ON'], 'safe'],
            [['IN1_NUMBER'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IN1_ID' => 'In1  ID',
            'OR1_ID' => 'Or1  ID',
            'GL1_ID' => 'Gl1  ID',
            'CO1_ID' => 'Co1  ID',
            'US1_ID' => 'Us1  ID',
            'LO1_FROM_ID' => 'Lo1  From  ID',
            'LO1_TO_ID' => 'Lo1  To  ID',
            'IN1_NUMBER' => 'In1  Number',
            'IN1_STATUS' => 'In1  Status',
            'IN1_SUM_STATUS' => 'In1  Sum  Status',
            'IN1_TOTAL_VALUE' => 'In1  Total  Value',
            'IN1_COMMENT' => 'In1  Comment',
            'IN1_DELETE_FLAG' => 'In1  Delete  Flag',
            'IN1_CREATED_ON' => 'In1  Created  On',
            'IN1_CREATED_BY' => 'In1  Created  By',
            'IN1_MODIFIED_ON' => 'In1  Modified  On',
            'IN1_MODIFIED_BY' => 'In1  Modified  By',
        ];
    }

    public function getQuery($params)
    {
        $query = self::find()->alias($this->getTablePrefix())
                    ->select([  'IN1.IN1_ID', 'IN1.OR1_ID', 'IN1.CO1_ID', 'IN1.IN1_NUMBER', 'OR1.OR1_NUMBER', 'IN1.IN1_TOTAL_VALUE', 'IN1.IN1_COMMENT',
                                'CO1.CO1_SHORT_CODE', 'CO1.CO1_NAME', 'IN1.LO1_FROM_ID', 'LO1.LO1_SHORT_CODE  AS LO1_FROM_SHORT_CODE', 'LO1.LO1_NAME AS LO1_FROM_NAME',
                                'IN1.LO1_TO_ID', 'LO2.LO1_SHORT_CODE AS LO1_TO_SHORT_CODE', 'LO2.LO1_NAME AS LO1_TO_NAME', 'IN1.IN1_DELETE_FLAG',
	                            'IN1.IN1_STATUS',
	                            'INVOICEED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IN1.IN1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
	                            'COMPLETED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('OR1.OR1_COMPLETED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IN1.IN1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IN1.IN1_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'IN1.IN1_MODIFIED_BY', 'IN1.IN1_CREATED_BY',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME',
                                'ORDERED_BY' => 'us3_user.US1_NAME'])
                    ->leftJoin("or1_order_header as OR1" , "OR1.OR1_ID = IN1.OR1_ID AND OR1.OR1_DELETE_FLAG = 0")
                    ->leftJoin("co1_company as CO1" , "CO1.CO1_ID = IN1.CO1_ID AND CO1.CO1_DELETE_FLAG = 0")
                    ->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = IN1.LO1_FROM_ID AND LO1.LO1_DELETE_FLAG = 0")
                    ->leftJoin("lo1_location as LO2" , "LO2.LO1_ID = IN1.LO1_TO_ID AND LO2.LO1_DELETE_FLAG = 0")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = IN1.IN1_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = IN1.IN1_MODIFIED_BY")
                    ->leftJoin("us1_user as us3_user" , "us3_user.US1_ID = IN1.US1_ID");
        $query->orderBy(['IN1_MODIFIED_ON' => SORT_DESC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

	    if (isset($params['IN1_IDs']) || isset($params['OR1_IDs'])) {
		    $query->addSelect([ 'OR1.OR1_SHIPPING_METHOD', 'OR1.OR1_COMMENT',
			    'OR1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('OR1.OR1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			    'LO2.LO1_NAME AS TO_NAME', 'LO2.LO1_ADDRESS1 AS TO_ADDRESS1', 'LO2.LO1_ADDRESS2 AS TO_ADDRESS2',
			    'LO2.LO1_CITY AS TO_CITY', 'LO2.LO1_STATE AS TO_STATE', 'LO2.LO1_ZIP AS TO_ZIP',
			    'LO2.LO1_PHONE AS TO_PHONE', 'LO2.LO1_FAX AS TO_FAX', 'cy1_to.CY1_SHORT_CODE AS TO_COUNTRY',
			    'LO1.LO1_NAME AS FROM_NAME', 'LO1.LO1_ADDRESS1 AS FROM_ADDRESS1', 'LO1.LO1_ADDRESS2 AS FROM_ADDRESS2',
			    'LO1.LO1_CITY AS FROM_CITY', 'LO1.LO1_STATE AS FROM_STATE', 'LO1.LO1_ZIP AS FROM_ZIP',
			    'LO1.LO1_PHONE AS FROM_PHONE', 'LO1.LO1_FAX AS FROM_FAX', 'cy1_from.CY1_SHORT_CODE AS FROM_COUNTRY']);
		    $query->leftJoin("cy1_country AS cy1_to" , "cy1_to.CY1_ID = LO2.CY1_ID");
		    $query->leftJoin("cy1_country AS cy1_from" , "cy1_from.CY1_ID = LO1.CY1_ID");

		    if (isset($params['IN1_IDs']))
		        $query->andFilterWhere(['IN', 'IN1.IN1_ID', explode(',', $params['IN1_IDs'])]);
		    if (isset($params['OR1_IDs']))
		        $query->andFilterWhere(['IN', 'IN1.OR1_ID', explode(',', $params['OR1_IDs'])]);
	    }

        if(isset($this->CO1_ID) && $this->CO1_ID != 0) {
            $query->andFilterWhere(['=', 'IN1.CO1_ID', $this->CO1_ID]);
        }

        if(isset($this->LO1_ID) && $this->LO1_ID != 0) {
            $query->andFilterWhere(['=', 'IN1.LO1_FROM_ID', $this->LO1_ID]);
        }

        if(isset($this->IN1_NUMBER)) {
            $query->andFilterWhere(['like', 'IN1.IN1_NUMBER', $this->IN1_NUMBER]);
        }

        if(isset($this->IN1_TOTAL_VALUE)) {
            $query->andFilterWhere(['like', 'IN1.IN1_TOTAL_VALUE', $this->IN1_TOTAL_VALUE]);
        }

        if(isset($this->IN1_COMMENT)) {
            $query->andFilterWhere(['like', 'IN1.IN1_COMMENT', $this->IN1_COMMENT]);
        }

        if(isset($this->LO1_FROM_NAME)) {
            $query->andFilterWhere(['=', 'IN1.LO1_FROM_ID', $this->LO1_FROM_NAME]);
        }

        if(isset($this->LO1_TO_NAME)) {
            $query->andFilterWhere(['=', 'IN1.LO1_TO_ID', $this->LO1_TO_NAME]);
        }

        if(isset($this->COMPLETED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(OR1.OR1_COMPLETED_ON, "%m-%d-%Y")', $this->COMPLETED_ON]);
        }

        if(isset($this->MODIFIED_BY)) {
            $query->andFilterWhere(['=', 'us1_modify.US1_ID', $this->MODIFIED_BY]);
        }

        if(isset($this->CREATED_ON)) {
            $query->andFilterWhere(['like', 'DATE_FORMAT(IN1.IN1_CREATED_ON, "%m-%d-%Y")', $this->CREATED_ON]);
        }

        if(isset($this->CREATED_BY)) {
            $query->andFilterWhere(['=', 'us1_create.US1_ID', $this->CREATED_BY]);
        }

        $query->initScope();

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['IN1_NUMBER'] = SORT_DESC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $params['perPage'],
               'page'     => $params['page']
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getInvoice($id)
    {
        $query = $this->getQuery(null);
        $query->where(['=', 'IN1.IN1_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function getAllCreditMemosQuery($params)
    {
        $query = $this->getQuery( $params );
        $query->andFilterWhere(['<', 'IN1.IN1_TOTAL_VALUE', 0]);

        return $query;
    }

    public function getAllCreditMemos($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['IN1_NUMBER'] = SORT_DESC;
       }

       $query = $this->getAllCreditMemosQuery( $params );

       $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $params['perPage'],
               'page'     => $params['page']
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

    public function getLinkHeaders($params)
    {
        $query = self::find()->alias('IN1')
                    ->select(['IN1.IN1_ID', 'IN1.IN1_NUMBER'])
                    ->leftJoin("or1_order_header as OR1" , "OR1.OR1_ID = IN1.OR1_ID")
                    ->andFilterWhere(['=', 'IN1.IN1_DELETE_FLAG', 0])
                    ->andFilterWhere(['<>', 'OR1.OR1_STATUS', 0])
                    ->andFilterWhere(['<>', 'IN1.LO1_FROM_ID', 'IN1.LO1_TO_ID'])
                    ->andFilterWhere(['>=', 'IN1.IN1_TOTAL_VALUE', 0]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->CO1_ID) && $this->CO1_ID != 0) {
            $query->andFilterWhere(['=', 'IN1.CO1_ID', $this->CO1_ID]);
        }

        if(isset($this->LO1_TO_ID) && $this->LO1_ID != 0) {
            $query->andFilterWhere(['=', 'IN1.LO1_TO_ID', $this->LO1_TO_ID]);
        }

        if(isset($this->LO1_FROM_ID)) {
            $query->andFilterWhere(['=', 'IN1.LO1_FROM_ID', $this->LO1_FROM_ID]);
        }

        $defaultOrder['IN1_ID'] = SORT_DESC;

        $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => false,
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
        ]);

        return $dataProvider;
    }

	public function getInvoicesWithoutStatements($from, $to)
	{
		$query = self::find()->alias('IN1')
			->select(['*', 'IN1.LO1_FROM_ID', 'IN1.LO1_TO_ID'])
			->leftJoin("or1_order_header as o" , "o.OR1_ID = IN1.OR1_ID")
			->andFilterWhere(['=', 'IN1.IN1_DELETE_FLAG', 0])
			->andFilterWhere(['<>', 'IN1.LO1_FROM_ID', 'IN1.LO1_TO_ID']);

		$user = Yii::$app->user->getIdentity();

		if(isset($user->CO1_ID) && $user->CO1_ID != 0) {
			$query->andFilterWhere(['=', 'IN1.CO1_ID', $user->CO1_ID]);
		}

		if(isset($user->LO1_ID) && $user->LO1_ID != 0) {
			$query->andFilterWhere(['=', 'IN1.LO1_TO_ID', $user->LO1_ID]);
		}

		$query->andFilterWhere(['or',
			['AND',
                ['is not', 'o.OR1_COMPLETED_ON', new \yii\db\Expression('null')],
                ['>=', 'o.OR1_COMPLETED_ON', $from],
                ['<=', 'o.OR1_COMPLETED_ON', $to . ' 23:59:59']],
			['AND',
                ['<', 'IN1.IN1_TOTAL_VALUE', 0],
                ['>=', 'IN1.IN1_CREATED_ON', $from],
                ['<=', 'IN1.IN1_CREATED_ON', $to . ' 23:59:59']]
		]);

		return $query->all();
	}

}
