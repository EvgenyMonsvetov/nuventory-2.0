<?php

namespace app\models;

use app\models\scopes\LocationQuery;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "lo1_location".
 *
 * @property int $LO1_ID
 * @property int $CO1_ID
 * @property string $LO1_SHORT_CODE
 * @property string $LO1_NAME
 * @property int $LO1_TYPE
 * @property int $LO1_RACKS
 * @property string $LO1_ADDRESS1
 * @property string $LO1_ADDRESS2
 * @property string $LO1_CITY
 * @property string $LO1_STATE
 * @property string $LO1_ZIP
 * @property string $LO1_PHONE
 * @property string $LO1_FAX
 * @property int $lO1_TIMEZONE
 * @property string $LO1_MITCHELL_SHOP_ID
 * @property string $LO1_CCC_RFID
 * @property string $LO1_CCC_SHOP_ID
 * @property int $CY1_ID
 * @property int $LO1_DELETE_FLAG
 * @property int $LO1_CREATED_BY
 * @property string $LO1_CREATED_ON
 * @property int $LO1_MODIFIED_BY
 * @property string $LO1_MODIFIED_ON
 * @property int $LO1_CENTER_FLAG
 * @property int $LO1_ORDER_DAYS_PER_MONTH
 * @property string $LO1_CCCONE_NAME
 * @property string $LO1_EMAILS
 */
class Location extends ActiveRecord
{
    protected $_tablePrefix = 'LO1';

	public $CO1_NAME;
	public $CY1_SHORT_CODE;
	public $masterDataLocation;
	public $CREATED_ON;

    public static function find()
    {
        return new LocationQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lo1_location';
    }

	public function fields() {
		$fields = parent::fields();
		$fields["CO1_NAME"] = function($model){ return $model->CO1_NAME; };
		$fields["CY1_SHORT_CODE"] = function($model){ return $model->CY1_SHORT_CODE; };
		return $fields;
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LO1_ID', 'CO1_ID', 'LO1_TYPE', 'LO1_RACKS', 'lO1_TIMEZONE', 'CY1_ID', 'LO1_DELETE_FLAG', 'LO1_CREATED_BY', 'LO1_MODIFIED_BY', 'LO1_CENTER_FLAG', 'LO1_ORDER_DAYS_PER_MONTH'], 'integer'],
            [['LO1_CREATED_ON', 'LO1_MODIFIED_ON'], 'safe'],
            [['LO1_SHORT_CODE', 'LO1_CITY', 'LO1_STATE'], 'string', 'max' => 50],
            [['LO1_NAME'], 'string', 'max' => 300],
            [['LO1_ADDRESS1', 'LO1_ADDRESS2'], 'string', 'max' => 150],
            [['LO1_ZIP', 'LO1_PHONE', 'LO1_FAX'], 'string', 'max' => 20],
            [['LO1_MITCHELL_SHOP_ID', 'LO1_CCC_RFID', 'LO1_CCC_SHOP_ID', 'CO1_NAME', 'CY1_SHORT_CODE', 'LO1_CCCONE_NAME'], 'string', 'max' => 100],
            [['LO1_EMAILS'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LO1_ID' => 'ID',
            'CO1_ID' => 'Company ID',
            'CO1_NAME' => 'Company Name', // need for export
            'LO1_SHORT_CODE' => 'Short Code',
            'LO1_NAME' => 'Shop Name',
            'LO1_TYPE' => 'Type',
            'LO1_RACKS' => 'Racks',
            'LO1_ADDRESS1' => 'Address1',
            'LO1_ADDRESS2' => 'Address2',
            'LO1_CITY' => 'City',
            'LO1_STATE' => 'State',
            'LO1_ZIP' => 'Zip',
            'LO1_PHONE' => 'Phone',
            'LO1_FAX' => 'Fax',
            'lO1_TIMEZONE' => 'Timezone',
            'LO1_MITCHELL_SHOP_ID' => 'Mitchell Shop ID',
            'LO1_CCC_RFID' => 'Ccc Rfid',
            'LO1_CCC_SHOP_ID' => 'Ccc Shop ID',
            'CY1_ID' => 'Country ID',
            'CY1_SHORT_CODE' => 'Country Short Code', // need for export
            'LO1_DELETE_FLAG' => 'Deleted',
            'LO1_CREATED_BY' => 'Created By',
            'LO1_CREATED_ON' => 'Created On',
            'LO1_MODIFIED_BY' => 'Modified By',
            'LO1_MODIFIED_ON' => 'Modified On',
            'LO1_CENTER_FLAG' => 'Center Flag',
            'LO1_ORDER_DAYS_PER_MONTH' => 'Order Days Per Month',
            'LO1_CCCONE_NAME' => 'CCCOne NAME',
            'LO1_EMAILS' => 'Emails'
        ];
    }

    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery( $params )
    {
        $query = self::find()->alias('LO1')->select([
                'LO1.*',
                'LO1_CREATED_BY_NAME'  => 'us1_create.US1_NAME',
                'LO1_MODIFIED_BY_NAME' => 'us1_modify.US1_NAME',
                'CO1.CO1_SHORT_CODE',
                'CO1.CO1_NAME',
                'CY1.CY1_NAME',
                'CY1.CY1_SHORT_CODE'
            ])
            ->leftJoin("co1_company  as CO1" ,    "CO1.CO1_ID = LO1.CO1_ID AND CO1.CO1_DELETE_FLAG=0")
            ->leftJoin("cy1_country as CY1",      "CY1.CY1_ID = LO1.CY1_ID AND CY1.CY1_DELETE_FLAG=0")
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = LO1.LO1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = LO1.LO1_MODIFIED_BY");

        $this->load($params, '');

	    if (isset($this->CO1_NAME)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_NAME', $this->CO1_NAME]);
	    }
	    if (isset($this->CO1_ID)) {
		    $query->andFilterWhere(['=', 'LO1.CO1_ID', $this->CO1_ID]);
	    }
	    if (isset($params['CO1_IDs'])) {
		    $query->andFilterWhere(['IN', 'LO1.CO1_ID', explode(',', $params['CO1_IDs'])]);
	    }
	    if (isset($this->LO1_ID)) {
		    $query->andFilterWhere(['=', 'LO1.LO1_ID', $this->LO1_ID]);
	    }
	    if (isset($this->LO1_NAME)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_NAME', $this->LO1_NAME]);
	    }
	    if (isset($this->LO1_SHORT_CODE)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_SHORT_CODE', $this->LO1_SHORT_CODE]);
	    }
	    if (isset($this->LO1_CENTER_FLAG)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_CENTER_FLAG', $this->LO1_CENTER_FLAG]);
	    }
        if (isset($this->LO1_MITCHELL_SHOP_ID)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_MITCHELL_SHOP_ID', $this->LO1_MITCHELL_SHOP_ID]);
	    }
        if (isset($this->LO1_CCC_SHOP_ID)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_CCC_SHOP_ID', $this->LO1_CCC_SHOP_ID]);
	    }
        if (isset($this->LO1_ADDRESS1)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_ADDRESS1', $this->LO1_ADDRESS1]);
	    }
        if (isset($this->LO1_ADDRESS2)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_ADDRESS2', $this->LO1_ADDRESS2]);
	    }
        if (isset($this->LO1_CITY)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_CITY', $this->LO1_CITY]);
	    }
        if (isset($this->LO1_STATE)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_STATE', $this->LO1_STATE]);
	    }
	    if (isset($this->CY1_ID)) {
		    $query->andFilterWhere(['=', 'CY1.CY1_ID', $this->CY1_ID]);
	    }
	    if (isset($this->LO1_ZIP)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_ZIP', $this->LO1_ZIP]);
	    }
	    if (isset($this->LO1_PHONE)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_PHONE', $this->LO1_PHONE]);
	    }
	    if (isset($this->LO1_FAX)) {
		    $query->andFilterWhere(['like', 'LO1.LO1_FAX', $this->LO1_FAX]);
	    }

        $query->initScope();
        return $query;
    }

    public function search($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['CO1_ID']   = SORT_ASC;
            $defaultOrder['LO1_NAME'] = SORT_ASC;
        }

        $query = $this->getSearchQuery( $params );

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => isset($params['perPage']) ? $params['perPage'] : false
            ],
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function getMasterDataItems( $CO1_ID, MasterData $masterData )
    {
    	$UM1_PRICING_ID = $masterData ? $masterData->UM1_PRICING_ID : '';
	    $MD1_ID = $masterData && isset($masterData->MD1_ID) ? $masterData->MD1_ID : '';
        $query = self::find()->alias('lo1')->select([
                'um2.UM2_FACTOR',
                'UM2_PRICE_FACTOR' => 'um2_price_factor.UM2_FACTOR',
                'md2.*',
                'lo1.*'
            ])
            ->leftJoin("md2_location as md2" ,       "md2.LO1_ID = lo1.LO1_ID AND md2.MD1_ID='".$MD1_ID."' AND md2.MD2_DELETE_FLAG=0")
            ->leftJoin("um2_conversion as um2" ,     "um2.UM1_FROM_ID = md2.UM1_PURCHASE_ID AND um2.UM1_TO_ID = md2.UM1_RECEIPT_ID AND um2.MD1_ID = md2.MD1_ID AND um2.UM2_DELETE_FLAG = 0")
            ->leftJoin("um2_conversion as um2_price_factor" , "um2_price_factor.UM1_FROM_ID = '".$UM1_PRICING_ID."' AND um2_price_factor.UM1_TO_ID = UM1_PURCHASE_ID AND um2_price_factor.MD1_ID = md2.MD1_ID AND um2_price_factor.UM2_DELETE_FLAG = 0");

        $query->andFilterWhere(['=', 'lo1.LO1_DELETE_FLAG', 0]);
	    if ($CO1_ID) {
		    $query->andFilterWhere(['=', 'lo1.CO1_ID', $CO1_ID]);
	    } else if($masterData && $masterData->CO1_ID) {
		    $query->andFilterWhere(['=', 'lo1.CO1_ID', $masterData->CO1_ID]);
	    }

        $query->orderBy(['lo1.LO1_CENTER_FLAG' => SORT_DESC, 'lo1.LO1_NAME' => SORT_ASC]);

        return $query->asArray()->all();
    }


    public function beforeValidate()
    {
        if( isset( $this->CA1_NO_INVENTORY )){
            $this->CA1_NO_INVENTORY = $this->CA1_NO_INVENTORY ? 1 : 0;
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public static function getCentralLocation( $CO1_ID )
    {
        $query = self::find()->alias('lo1')->select(['*']);

        $query->andFilterWhere(['=', 'lo1.LO1_DELETE_FLAG', 0]);
        $query->andFilterWhere(['=', 'lo1.CO1_ID',          $CO1_ID]);
        $query->andFilterWhere(['=', 'lo1.LO1_CENTER_FLAG', 1]);

        return $query->one();
    }

    public function getCentralLocations( $CO1_ID, $LO1_ID=null )
    {
        $query = self::find()->alias('lo1')->select([
            'lo1.*',
            'CO1.CO1_SHORT_CODE',
            'CO1.CO1_NAME' ])
            ->leftJoin("co1_company  as CO1" ,
                "CO1.CO1_ID = LO1.CO1_ID AND CO1.CO1_DELETE_FLAG=0");

        $query->andFilterWhere(['=', 'lo1.LO1_DELETE_FLAG', 0]);
        $query->andFilterWhere(['=', 'lo1.CO1_ID',  $CO1_ID]);

        if( $LO1_ID ){
            $query->andFilterWhere(['or',
                ['lo1.LO1_CENTER_FLAG' => 1],
                ['lo1.LO1_ID' => $LO1_ID]]);
        }else{
            $query->andFilterWhere(['=', 'lo1.LO1_CENTER_FLAG', 1]);
        }

        return $query->asArray()->all();
    }

	public function getAllLocations( $params )
	{
		$query = self::find()->alias('LO1')
            ->select([ 'LO1.*',  'CO1.CO1_NAME' ])
            ->leftJoin("co1_company  as CO1" ,    "CO1.CO1_ID = LO1.CO1_ID AND CO1.CO1_DELETE_FLAG=0")
			->andFilterWhere(['<>', 'LO1.LO1_DELETE_FLAG', 1]);

		if (isset($params['CO1_ID'])) {
			$query->andFilterWhere(['=', 'LO1.CO1_ID', $params['CO1_ID']]);
		}

		if (isset($params['CO1_IDs'])) {
			$query->andFilterWhere(['IN', 'LO1.CO1_ID', explode(',', $params['CO1_IDs'])]);
			$query->orderBy(['CO1.CO1_NAME' => SORT_ASC, 'LO1.LO1_NAME' => SORT_ASC]);
		}

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => false
		]);

		return $dataProvider;
	}


	public function getLocations( $params )
	{
		$query = self::find()->alias('LO1')->select([
			'LO1.*'
		]);

		if(isset($params['LO1_IDs'])) {
			$query->andFilterWhere(['IN', 'LO1_ID', explode(',', $params['LO1_IDs'])]);
		}

		if(isset($params['LO1_SHORT_CODEs'])) {
			$query->andFilterWhere(['IN', 'LO1_SHORT_CODE', explode(',', $params['LO1_SHORT_CODEs'])]);
		}

		return $query->asArray()->all();
	}

	public function getLocationsCountries( $shops )
	{
		$query = self::find()->alias('LO1')->select([
			'LO1.*'
		])
			->andFilterWhere(['<>', 'LO1.LO1_DELETE_FLAG', 1]);;

		if (isset($shops)) {
			$query->andFilterWhere(['IN', 'LO1.LO1_ID', explode(',', $shops)]);
		}

		return $query->groupBy(['LO1.CO1_ID'])->asArray()->all();
	}

	public function getLocationLastShortCode()
	{
		return self::find()->alias('LO1')->select(['LO1.LO1_ID', 'MAX(LO1_SHORT_CODE) AS LO1_SHORT_CODE'])->one();
	}
}
