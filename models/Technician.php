<?php

namespace app\models;

use app\models\scopes\TechnicianQuery;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "te1_technician".
 *
 * @property int $TE1_ID
 * @property string $TE1_SHORT_CODE
 * @property string $TE1_NAME
 * @property int $CO1_ID
 * @property int $LO1_ID
 * @property int $TE1_BODY
 * @property int $TE1_PAINT
 * @property int $TE1_DETAIL
 * @property int $TE1_DELETE_FLAG
 * @property int $TE1_CREATED_BY
 * @property string $TE1_CREATED_ON
 * @property int $TE1_MODIFIED_BY
 * @property string $TE1_MODIFIED_ON
 * @property int $TE1_ACTIVE
 */
class Technician extends ActiveRecord
{
    protected $_tablePrefix = 'TE1';

    CONST LAST_SHORT_CODE_START = 100;

    CONST SCENARIO_ADD = 'add';

    public static function find()
    {
        return new TechnicianQuery(get_called_class());
    }

    public $US1_ID;
    public $US1_LOGIN;
    public $US1_PASS;
    public $US1_NAME;
    public $CO1_NAME;
    public $LO1_NAME;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'te1_technician';
    }

    public function beforeValidate()
    {
        if (is_int($this->TE1_SHORT_CODE)) {
            $this->TE1_SHORT_CODE = (string)$this->TE1_SHORT_CODE;
        }
        return parent::beforeValidate();
    }

    public static function primaryKey()
    {
        return ['TE1_ID'];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['TE1_ID' => 'TE1_ID']);
    }

    public function relations()
    {
        return array(
            'shows' => array(self::HAS_MANY, 'User', 'TE1_ID'),
        );
    }

    public function fields() {
        $fields = parent::fields();
        $fields['US1_ID'] = function($model){ return $model->US1_ID; };
        $fields['US1_NAME'] = function($model){ return $model->US1_NAME; };
        $fields['CO1_NAME'] = function($model){ return $model->CO1_NAME; };
        $fields['LO1_NAME'] = function($model){ return $model->LO1_NAME; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TE1_SHORT_CODE', 'TE1_NAME', 'US1_LOGIN', 'US1_PASS', 'CO1_ID', 'LO1_ID'], 'required'],
            [['CO1_ID', 'LO1_ID', 'US1_ID', 'TE1_ID', 'TE1_BODY', 'TE1_PAINT', 'TE1_DETAIL', 'TE1_DELETE_FLAG', 'TE1_CREATED_BY', 'TE1_MODIFIED_BY', 'TE1_ACTIVE'], 'integer'],
            [['TE1_CREATED_ON', 'TE1_MODIFIED_ON'], 'safe'],
            [['TE1_SHORT_CODE'], 'string', 'max' => 10],
            [['TE1_NAME', 'US1_NAME', 'CO1_NAME', 'LO1_NAME'], 'string', 'max' => 200]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TE1_ID'            => 'Te1  ID',
            'TE1_SHORT_CODE'    => 'Te1  Short  Code',
            'TE1_NAME'          => 'Te1  Name',
            'CO1_ID'            => 'Co1  ID',
            'LO1_ID'            => 'Lo1  ID',
            'US1_ID'            => 'Us1  ID',
            'TE1_BODY'          => 'Te1  Body',
            'TE1_PAINT'         => 'Te1  Paint',
            'TE1_DETAIL'        => 'Te1  Detail',
            'TE1_DELETE_FLAG'   => 'Te1  Delete  Flag',
            'TE1_CREATED_BY'    => 'Te1  Created  By',
            'TE1_CREATED_ON'    => 'Te1  Created  On',
            'TE1_MODIFIED_BY'   => 'Te1  Modified  By',
            'TE1_MODIFIED_ON'   => 'Te1  Modified  On',
            'TE1_ACTIVE'        => 'Active'
        ];
    }

    public function getQuery($params=[])
    {
        $query = Technician::find()->alias('TE1')
                    ->select([  'TE1.*', 'CO.CO1_ID', 'CO.CO1_NAME', 'CO.CO1_SHORT_CODE',
                                'US.US1_ID', 'US.US1_LOGIN', 'US.US1_ALLOW_MOBILE_JOB_CREATE', 'US.US1_NAME',
                                'LO1.LO1_SHORT_CODE', 'LO1.LO1_NAME',
			                    'TE1_MODIFIED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('TE1_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
			                    'TE1_CREATED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('TE1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = TE1.TE1_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = TE1.TE1_MODIFIED_BY")
                    ->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = TE1.LO1_ID")
                    ->leftJoin("us1_user as US" , "US.TE1_ID = TE1.TE1_ID and US.DELETED = 0")
                    ->leftJoin("co1_company as CO" , "CO.CO1_ID = TE1.CO1_ID");
        $query->andFilterWhere(['=', 'CO.CO1_DELETE_FLAG', 0]);
        $query->andFilterWhere(['=', 'LO1.LO1_DELETE_FLAG', 0]);
        $query->andFilterWhere(['=', 'TE1.TE1_DELETE_FLAG', 0]);
        $query->initScope();

	    $this->load($params, '');

        $query->andFilterWhere(['=',    'TE1.CO1_ID',         $this->CO1_ID]);
	    $query->andFilterWhere(['=',    'TE1.LO1_ID',         $this->LO1_ID]);
	    $query->andFilterWhere(['like', 'TE1.TE1_SHORT_CODE', $this->TE1_SHORT_CODE]);
	    $query->andFilterWhere(['like', 'TE1.TE1_NAME',       $this->TE1_NAME]);
	    $query->andFilterWhere(['like', 'US.US1_NAME',        $this->US1_NAME]);
	    $query->andFilterWhere(['=',    'TE1.TE1_ACTIVE',     $this->TE1_ACTIVE]);

	    if (isset($params['SHORT_CODE'])) {
		    $query->andFilterWhere(['=', 'TE1.TE1_SHORT_CODE', $params['SHORT_CODE']]);
	    }

	    if (isset($params['TE1_IDs'])) {
		    $query->andFilterWhere(['IN', 'TE1.TE1_ID', explode(',', $params['TE1_IDs'])]);
	    }

        return $query;
    }

    public function getAllTechnicians($params)
    {
        $query = $this->getQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'  => $params['perPage'],
                'page'      => $params['page']
            ]
        ]);

        return $dataProvider;
    }

    public function getTechnician($id)
    {
        $query = $this->getQuery(null);

        $query->joinWith(["user US1"], true);
        $query->addSelect('US.US1_PASS');
        $query->andFilterWhere(['=', 'TE1.TE1_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function delete()
    {
        $this->TE1_DELETE_FLAG = 1;
        return $this->save(false);
    }
}
