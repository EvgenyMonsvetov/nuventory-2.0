<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "cy1_country".
 *
 * @property int $CY1_ID
 * @property string $CY1_SHORT_CODE
 * @property string $CY1_NAME
 * @property int $CY1_DELETE_FLAG
 * @property int $CY1_CREATED_BY
 * @property string $CY1_CREATED_ON
 * @property int $CY1_MODIFIED_BY
 * @property string $CY1_MODIFIED_ON
 */
class Country extends ActiveRecord
{
    protected $_tablePrefix = 'CY1';

    CONST SCENARIO_ADD = 'add';

    public $CREATED_BY;
    public $MODIFIED_BY;
    public $CREATED_ON;
    public $MODIFIED_ON;

    public static function primaryKey()
    {
        return ['CY1_ID'];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cy1_country';
    }

    public function fields() {
        $fields = parent::fields();
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        return $fields;
    }

    /*public function extraFields()
    {
        return ['CREATED_BY', 'MODIFIED_BY'];
    }*/

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CY1_SHORT_CODE'], 'required'],
            [['CY1_SHORT_CODE'], 'unique'],
            [['CY1_ID', 'CY1_DELETE_FLAG', 'CY1_CREATED_BY', 'CY1_MODIFIED_BY'], 'integer'],
            [['CY1_CREATED_ON', 'CY1_MODIFIED_ON'], 'safe'],
            [['CY1_SHORT_CODE'], 'string', 'max' => 10],
            [['CY1_NAME', 'CREATED_ON', 'MODIFIED_ON'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CY1_ID' => 'Country ID',
            'CY1_SHORT_CODE' => 'Short  Code',
            'CY1_NAME' => 'Name',
            'CY1_DELETE_FLAG' => 'Delete  Flag',
            'CY1_CREATED_BY' => 'Created  By',
            'CY1_CREATED_ON' => 'Created  On',
            'CY1_MODIFIED_BY' => 'Modified  By',
            'CY1_MODIFIED_ON' => 'Modified  On',
            'CREATED_BY' => 'Created  By',
            'MODIFIED_BY' => 'Modified  By'
        ];
    }

    public function getSearchQuery($params)
    {
        $query = self::find()->alias('CY1')->select([
            'CY1.*',
	        'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('CY1_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
	        'CREATED_ON'  => 'DATE_FORMAT(' . $this->getLocalDate('CY1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
            'CREATED_BY'  => 'us1_create.US1_NAME',
            'MODIFIED_BY' => 'us1_modify.US1_NAME'
        ])
        ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = CY1.CY1_CREATED_BY")
        ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = CY1.CY1_MODIFIED_BY");

        $this->load($params, '');

//        if (!empty($params['ALL'])) {
//            $query->andFilterWhere(['=',    'CY1.CY1_DELETE_FLAG', 0]);
//            $query->andFilterWhere(['or', ['=', 'CY1.CY1_ID', $params['ALL']], ['like', 'CY1.CY1_SHORT_CODE', $params['ALL']],
//                                    ['like', 'CY1.CY1_NAME', $params['ALL']], ['like', 'us1_create.US1_NAME', $params['ALL']],
//                                    ['like', 'us1_modify.US1_NAME', $params['ALL']]]);
//        } else {
            $query->andFilterWhere(['=',    'CY1.CY1_DELETE_FLAG', 0]);
            if(isset($this->CY1_ID))
                $query->andFilterWhere(['=',    'CY1.CY1_ID',          $this->CY1_ID]);
            if(isset($this->CY1_SHORT_CODE))
                $query->andFilterWhere(['like', 'CY1.CY1_SHORT_CODE',  $this->CY1_SHORT_CODE]);
            if(isset($this->CY1_NAME))
                $query->andFilterWhere(['like', 'CY1.CY1_NAME',        $this->CY1_NAME]);
            if(isset($this->CREATED_BY))
                $query->andFilterWhere(['like', 'us1_create.US1_NAME', $this->CREATED_BY]);
            if(isset($this->MODIFIED_BY))
                $query->andFilterWhere(['like', 'us1_modify.US1_NAME', $this->MODIFIED_BY]);
//        }

        return $query;
    }

    public function search($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['CY1_NAME'] = SORT_ASC;
        }

        $query = $this->getSearchQuery( $params );

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => !empty($params['perPage']) ? $params['perPage'] : null
            ]
            ,
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }
}
