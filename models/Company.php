<?php

namespace app\models;

use app\models\scopes\CompanyQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "co1_company".
 *
 * @property int $CO1_ID
 * @property string $CO1_SHORT_CODE
 * @property string $CO1_NAME
 * @property string $CO1_ADDRESS1
 * @property string $CO1_ADDRESS2
 * @property string $CO1_CITY
 * @property string $CO1_STATE
 * @property string $CO1_ZIP
 * @property string $CO1_PHONE
 * @property string $CO1_FAX
 * @property int $CO1_MITCHELL_FLAG
 * @property int $CO1_FM_FLAG
 * @property int $CY1_ID
 * @property int $CO1_DELETE_FLAG
 * @property int $CO1_CREATED_BY
 * @property string $CO1_CREATED_ON
 * @property int $CO1_MODIFIED_BY
 * @property string $CO1_MODIFIED_ON
 * @property int $CO1_CCC_FLAG
 * @property int $CO1_DANGELO
 * @property int $CO1_TIMEZONE
 * @property string $CO1_SUMMARY_REPORT_EMAIL
 * @property int $CO1_LITE
 */
class Company extends ActiveRecord
{
    protected $_tablePrefix = 'CO1';

    CONST SCENARIO_ADD = 'add';

	public $CY1_SHORT_CODE;

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public static function primaryKey()
    {
        return ['CO1_ID'];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'co1_company';
    }

	public function fields() {
		$fields = parent::fields();
		$fields["CY1_SHORT_CODE"] = function($model){ return $model->CY1_SHORT_CODE; };
		return $fields;
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CO1_ID', 'CO1_MITCHELL_FLAG', 'CO1_FM_FLAG', 'CY1_ID', 'CO1_DELETE_FLAG', 'CO1_CREATED_BY', 'CO1_MODIFIED_BY', 'CO1_CCC_FLAG', 'CO1_DANGELO', 'CO1_TIMEZONE', 'CO1_LITE'], 'integer'],
            [['CO1_CREATED_ON', 'CO1_MODIFIED_ON'], 'safe'],
            [['CO1_SHORT_CODE', 'CO1_CITY', 'CO1_STATE'], 'string', 'max' => 50],
            [['CO1_NAME', 'CO1_ADDRESS1', 'CO1_ADDRESS2'], 'string', 'max' => 150],
            [['CO1_ZIP', 'CO1_PHONE', 'CO1_FAX'], 'string', 'max' => 20],
            [['CO1_SUMMARY_REPORT_EMAIL', 'CY1_SHORT_CODE'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CO1_ID' => 'Co1  ID',
            'CO1_SHORT_CODE' => 'Company Short  Code',
            'CO1_NAME' => 'Company  Name',
            'CO1_ADDRESS1' => 'Co1  Address1',
            'CO1_ADDRESS2' => 'Co1  Address2',
            'CO1_CITY' => 'Co1  City',
            'CO1_STATE' => 'Co1  State',
            'CO1_ZIP' => 'Co1  Zip',
            'CO1_PHONE' => 'Co1  Phone',
            'CO1_FAX' => 'Co1  Fax',
            'CO1_MITCHELL_FLAG' => 'Co1  Mitchell  Flag',
            'CO1_FM_FLAG' => 'Co1  Fm  Flag',
            'CY1_ID' => 'Cy1  ID',
            'CO1_DELETE_FLAG' => 'Co1  Delete  Flag',
            'CO1_CREATED_BY' => 'Co1  Created  By',
            'CO1_CREATED_ON' => 'Co1  Created  On',
            'CO1_MODIFIED_BY' => 'Co1  Modified  By',
            'CO1_MODIFIED_ON' => 'Co1  Modified  On',
            'CO1_CCC_FLAG' => 'Co1  Ccc  Flag',
            'CO1_DANGELO' => 'Co1  Dangelo',
            'CO1_TIMEZONE' => 'Co1  Timezone',
            'CO1_SUMMARY_REPORT_EMAIL' => 'Co1  Summary  Report  Email',
            'CO1_LITE' => 'Co1  Lite',
        ];
    }

    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery($params)
    {
        $query = Company::find()->alias('CO1')->select([
            'CO1.*',
	        'CO1_MODIFIED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('CO1_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
	        'CO1_CREATED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('CO1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
            'CO1_CREATED_BY_NAME'  => 'us1_create.US1_NAME',
            'CO1_MODIFIED_BY_NAME' => 'us1_modify.US1_NAME',
            'CY1.CY1_ID',
            'CY1.CY1_SHORT_CODE',
            'CY1.CY1_NAME'
        ])
        ->leftJoin("cy1_country as CY1"    ,  "CY1.CY1_ID = CO1.CY1_ID AND CY1.CY1_DELETE_FLAG=0")
        ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = CO1.CO1_CREATED_BY")
        ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = CO1.CO1_MODIFIED_BY");

        $this->load($params, '');

	    if (isset($this->CO1_ID)) {
		    $query->andFilterWhere(['=', 'CO1.CO1_ID', $this->CO1_ID]);
	    }

	    if (isset($this->CO1_NAME)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_NAME', $this->CO1_NAME]);
	    }

	    if (isset($this->CO1_SHORT_CODE)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_SHORT_CODE', $this->CO1_SHORT_CODE]);
	    }

	    if (isset($this->CO1_ADDRESS1)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_ADDRESS1', $this->CO1_ADDRESS1]);
	    }

	    if (isset($this->CO1_CITY)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_CITY', $this->CO1_CITY]);
	    }

        if (isset($this->CO1_STATE)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_STATE', $this->CO1_STATE]);
	    }

        if (isset($this->CY1_ID)) {
		    $query->andFilterWhere(['=', 'CY1.CY1_ID', $this->CY1_ID]);
	    }

        if (isset($this->CO1_ZIP)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_ZIP', $this->CO1_ZIP]);
	    }

        if (isset($this->CO1_PHONE)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_PHONE', $this->CO1_PHONE]);
	    }

        if (isset($this->CO1_FAX)) {
		    $query->andFilterWhere(['like', 'CO1.CO1_FAX', $this->CO1_FAX]);
	    }

        if (isset($this->CO1_MITCHELL_FLAG)) {
		    $query->andFilterWhere(['=', 'CO1.CO1_MITCHELL_FLAG', $this->CO1_MITCHELL_FLAG]);
	    }

        if (isset($this->CO1_CCC_FLAG)) {
		    $query->andFilterWhere(['=', 'CO1.CO1_CCC_FLAG', $this->CO1_CCC_FLAG]);
	    }

        if (isset($this->CO1_LITE)) {
		    $query->andFilterWhere(['=', 'CO1.CO1_LITE', $this->CO1_LITE]);
	    }

        $query->initScope();

        return $query;
    }

    public function search($params=[])
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['CO1_NAME'] = SORT_ASC;
        }

        $query = $this->getSearchQuery( $params );

        $pagination = false;
        if( !empty($params['perPage'])){
            $pagination = [
                'pageSize' => $params['perPage'],
                'page'     => $params['page']
            ];
        }
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => $pagination,
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

    public function beforeValidate()
    {
        if( isset( $this->CO1_MITCHELL_FLAG )){
            $this->CO1_MITCHELL_FLAG = $this->CO1_MITCHELL_FLAG ? 1 : 0;
        }
        if( isset( $this->CO1_CCC_FLAG )){
            $this->CO1_CCC_FLAG = $this->CO1_CCC_FLAG ? 1 : 0;
        }
        if( isset( $this->CO1_LITE )){
            $this->CO1_LITE = $this->CO1_LITE ? 1 : 0;
        }
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

	public function getCompanyLocation($params)
	{
		$query = $this->getSearchQuery($params);

		$query->addSelect("LO1.*")
			->leftJoin("lo1_location as LO1", "CO1.CO1_ID = LO1.CO1_ID AND LO1.LO1_DELETE_FLAG = 0");
		$query->andFilterWhere(['<>', 'CO1.CO1_DELETE_FLAG', 1]);
		$query->orderBy(['CO1.CO1_NAME' => SORT_ASC, 'LO1.LO1_NAME' => SORT_ASC]);

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => false
		]);

		return $dataProvider;
	}

	public function getCompanyLastShortCode()
	{
		return self::find()->alias('CO1')->select(['CO1.CO1_ID', 'MAX(CAST(CO1_SHORT_CODE AS UNSIGNED)) AS CO1_SHORT_CODE'])->one();
	}

}
