<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 06/08/2018
 * Time: 13:42
 */

namespace app\models;

class vLog extends Log
{
    /**
    * Get table name
    * @return string
    */
    public static function tableName()
    {
        return 'v_dist_' . (YII_ENV_PROD ? '' : 'test_') . 'logs';
    }
}