<?php

namespace app\models;

use app\models\scopes\VendorQuery;
use Yii;
use yii\data\ActiveDataProvider;
use app\models\MasterCatalog;

/**
 * This is the model class for table "vd1_vendor".
 *
 * @property int $VD1_ID
 * @property int $VD0_ID
 * @property string $VD1_SHORT_CODE
 * @property string $VD1_CUSTOMER_NO
 * @property string $VD1_NAME
 * @property string $VD1_ADDRESS1
 * @property string $VD1_ADDRESS2
 * @property string $VD1_CITY
 * @property string $VD1_STATE
 * @property string $VD1_ZIP
 * @property string $VD1_PHONE
 * @property string $VD1_FAX
 * @property string $VD1_EMAIL
 * @property string $VD1_URL
 * @property int $VD1_NO_INVENTORY
 * @property int $CY1_ID
 * @property int $VD1_DELETE_FLAG
 * @property int $VD1_CREATED_BY
 * @property string $VD1_CREATED_ON
 * @property int $VD1_MODIFIED_BY
 * @property string $VD1_MODIFIED_ON
 * @property int $CO1_ID
 * @property int $VD1_EDI_GL
 * @property int $VD1_MINIMUM_ORDER
 * @property string $VD1_DISCOUNT
 */
class Vendor extends ActiveRecord
{
    protected $_tablePrefix = 'VD1';

    public $VD0_SHORT_CODE;
    public $CO1_NAME;
    public $CREATED_BY;
    public $MODIFIED_BY;
    public $CREATED_ON;
    public $MODIFIED_ON;

    public static function find()
    {
        return new VendorQuery(get_called_class());
    }

    CONST SCENARIO_ADD = 'add';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vd1_vendor';
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['CO1_ID' => 'CO1_ID']);
    }

    public static function primaryKey()
    {
        return ['VD1_ID'];
    }

    public function fields() {
        $fields = parent::fields();
        $fields["VD0_SHORT_CODE"] = function($model){ return $model->VD0_SHORT_CODE; };
        $fields["CO1_NAME"] = function($model){ return $model->CO1_NAME; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['VD1_SHORT_CODE', 'VD1_NAME', 'CO1_ID'], 'required'],
            [['VD0_ID', 'VD1_NO_INVENTORY', 'CY1_ID', 'VD1_DELETE_FLAG', 'VD1_CREATED_BY', 'VD1_MODIFIED_BY', 'CO1_ID', 'VD1_EDI_GL', 'VD1_MINIMUM_ORDER'], 'integer'],
            [['VD1_CREATED_ON', 'VD1_MODIFIED_ON'], 'safe'],
            [['VD1_DISCOUNT'], 'number'],
            [['VD1_SHORT_CODE'], 'string', 'max' => 10],
            [['VD1_CUSTOMER_NO', 'VD1_NAME', 'VD1_ADDRESS1', 'VD1_ADDRESS2', 'VD1_CITY', 'VD1_EMAIL', 'VD0_SHORT_CODE', 'CO1_NAME', 'CREATED_BY', 'MODIFIED_BY', 'CREATED_ON', 'MODIFIED_ON'], 'string', 'max' => 200],
            [['VD1_STATE'], 'string', 'max' => 20],
            [['VD1_ZIP'], 'string', 'max' => 11],
            [['VD1_PHONE', 'VD1_FAX', 'VD1_URL'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'VD1_ID' => 'Vd1  ID',
            'VD0_ID' => 'Vd0  ID',
            'VD1_SHORT_CODE' => 'Vd1  Short  Code',
            'VD1_CUSTOMER_NO' => 'Vd1  Customer  No',
            'VD1_NAME' => 'Vd1  Name',
            'VD1_ADDRESS1' => 'Vd1  Address1',
            'VD1_ADDRESS2' => 'Vd1  Address2',
            'VD1_CITY' => 'Vd1  City',
            'VD1_STATE' => 'Vd1  State',
            'VD1_ZIP' => 'Vd1  Zip',
            'VD1_PHONE' => 'Vd1  Phone',
            'VD1_FAX' => 'Vd1  Fax',
            'VD1_EMAIL' => 'Vd1  Email',
            'VD1_URL' => 'Vd1  Url',
            'VD1_NO_INVENTORY' => 'Vd1  No  Inventory',
            'CY1_ID' => 'Cy1  ID',
            'VD1_DELETE_FLAG' => 'Vd1  Delete  Flag',
            'VD1_CREATED_BY' => 'Vd1  Created  By',
            'VD1_CREATED_ON' => 'Vd1  Created  On',
            'VD1_MODIFIED_BY' => 'Vd1  Modified  By',
            'VD1_MODIFIED_ON' => 'Vd1  Modified  On',
            'CO1_ID' => 'Co1  ID',
            'VD1_EDI_GL' => 'Vd1  Edi  Gl',
            'VD1_MINIMUM_ORDER' => 'Vd1  Minimum  Order',
            'VD1_DISCOUNT' => 'Vd1  Discount',
        ];
    }

    public function getQuery($params)
    {
        $query = Vendor::find()->alias('VD1')
                    ->select([  'VD1.*', 'co.CO1_ID', 'co.CO1_NAME', 'co.CO1_SHORT_CODE',
                                'CY1.CY1_SHORT_CODE', 'CY1.CY1_NAME', 'VD0.VD0_SHORT_CODE', 'VD0.VD0_NAME',
			                    'MODIFIED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('VD1_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
			                    'CREATED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('VD1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("cy1_country as CY1" , "CY1.CY1_ID = VD1.CY1_ID and CY1.CY1_DELETE_FLAG = 0")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = VD1.VD1_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = VD1.VD1_MODIFIED_BY")
                    ->leftJoin("vd0_vendor as VD0" , "VD0.VD0_ID = VD1.VD0_ID")
	                ->leftJoin("co1_company as co" , "co.CO1_ID = VD1.CO1_ID");
        $query->andFilterWhere(['=',    'co.CO1_DELETE_FLAG', 0]);
        $query->initScope();

        $this->load($params, '');
        $query->andFilterWhere(['=', 'VD1.VD0_ID', $this->VD0_ID]);
        $query->andFilterWhere(['=', 'VD1.CO1_ID', $this->CO1_ID]);

        return $query;
    }

    public function getAllVendors($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['VD1_NAME'] = SORT_ASC;
        }

        $query = $this->getQuery($params);
        $query->addOrderBy(['co.CO1_ID' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => $params['perPage'],
                'page'     => $params['page']
            ],
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

    public function getVendor($id)
    {
        $query = $this->getQuery(null);
        $query->andFilterWhere(['=', 'VD1.VD1_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function getAllMasterVendorsQuery($params)
    {
        $query = Vendor::find()->alias('VD1')
                    ->select([ 'VD1.VD1_ID', 'VD0.VD0_ID', 'VD0.VD0_NAME', 'VD0.VD0_SHORT_CODE'])
                    ->leftJoin("vd0_vendor as VD0" , "VD0.VD0_ID = VD1.VD0_ID AND VD0.VD0_DELETE_FLAG = 0");
        $query->andFilterWhere(['=', 'VD1.VD1_DELETE_FLAG', 0]);

        $this->load($params, '');
        if(isset($this->CO1_ID)) {
            $query->andWhere(['=', 'VD1.CO1_ID', $this->CO1_ID]);
        }
        $query->groupBy(['VD0.VD0_ID']);
        $query->addOrderBy(['VD0.VD0_SHORT_CODE' => SORT_ASC]);

        return $query;
    }

    public function getAllMasterVendors($params)
    {
        $query = $this->getAllMasterVendorsQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function getPartsQuery($params)
    {
        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        $query = MasterCatalog::find()->alias('MD0')
                     ->select([  'MD0.MD0_ID', 'MD0.MD0_PART_NUMBER', 'MD0.MD0_DESC1', 'MD0.MD0_UPC1', 'MD0_VENDOR_PART_NUMBER',
                                 'VD0.VD0_ID', 'VD0.VD0_SHORT_CODE', 'VD0.VD0_NAME', 'u.UM1_NAME'])
                     ->leftJoin("vd0_vendor as VD0" , "MD0.VD0_ID = VD0.VD0_ID and VD0.VD0_DELETE_FLAG = 0")
                     ->leftJoin("vd1_vendor as VD1" , "VD1.VD0_ID = VD0.VD0_ID and VD1.VD1_DELETE_FLAG = 0")
                     ->leftJoin("um1_unit as u" , "u.UM1_ID = MD0.UM1_DEFAULT_PRICING and u.UM1_DELETE_FLAG = 0");
        $query->where(['=', 'VD1.VD1_DELETE_FLAG', 0]);
        $query->andWhere(['=', 'MD0.MD0_DELETE_FLAG', 0]);
        $query->andWhere(['=', 'VD0.VD0_MANUFACTURER', 1]);
        $query->andWhere(['=', 'MD0.MD0_DISCONTINUED', 0]);
        $query->andWhere(['not', ['MD0.MD0_ID' => null]]);
        $query->groupBy(['MD0.MD0_ID']);
        $query->orderBy(['MD0.MD0_PART_NUMBER' => SORT_ASC]);

        if(isset($this->CO1_ID)) {
            $query->andWhere(['=', 'VD1.CO1_ID', $this->CO1_ID]);
        }

        if(isset($this->VD0_ID)) {
            $query->andWhere(['=', 'VD0.VD0_ID', $this->VD0_ID]);
        }

        if(isset($this->MD0_IDs)) {
            $query->andWhere(['in', 'MD0.MD0_ID', explode(',', $this->MD0_IDs)]);
        }

        return $query;
    }

    public function getAllParts($params)
    {
        $query = $this->getPartsQuery($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }
}
