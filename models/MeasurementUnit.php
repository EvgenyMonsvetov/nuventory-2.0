<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "um1_unit".
 *
 * @property int $UM1_ID
 * @property string $UM1_SHORT_CODE
 * @property string $UM1_NAME
 * @property int $UM1_SIZE
 * @property int $UM1_DELETE_FLAG
 * @property int $UM1_CREATED_BY
 * @property string $UM1_CREATED_ON
 * @property int $UM1_MODIFIED_BY
 * @property string $UM1_MODIFIED_ON
 */
class MeasurementUnit extends ActiveRecord
{

    protected $_tablePrefix = 'UM1';

	public $UM1_CREATED_BY_NAME;
	public $UM1_MODIFIED_BY_NAME;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'um1_unit';
    }

	public function fields() {
		$fields = parent::fields();
		$fields["UM1_CREATED_BY_NAME"] = function($model){ return $model->UM1_CREATED_BY_NAME; };
		$fields["UM1_MODIFIED_BY_NAME"] = function($model){ return $model->UM1_MODIFIED_BY_NAME; };
		return $fields;
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UM1_ID', 'UM1_SIZE', 'UM1_DELETE_FLAG', 'UM1_CREATED_BY', 'UM1_MODIFIED_BY'], 'integer'],
            [['UM1_CREATED_ON', 'UM1_MODIFIED_ON'], 'safe'],
            [['UM1_SHORT_CODE'], 'string', 'max' => 10],
            [['UM1_NAME', 'UM1_CREATED_BY_NAME', 'UM1_MODIFIED_BY_NAME'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UM1_ID' => 'Um1  ID',
            'UM1_SHORT_CODE' => 'Um1  Short  Code',
            'UM1_NAME' => 'Um1  Name',
            'UM1_SIZE' => 'Um1  Size',
            'UM1_DELETE_FLAG' => 'Um1  Delete  Flag',
            'UM1_CREATED_BY' => 'Um1  Created  By',
            'UM1_CREATED_ON' => 'Um1  Created  On',
            'UM1_MODIFIED_BY' => 'Um1  Modified  By',
            'UM1_MODIFIED_ON' => 'Um1  Modified  On',
        ];
    }

    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery( $params )
    {
        $query = self::find()->alias('UM1')->select([
                'UM1.*',
		        'UM1_CREATED_ON'       => 'DATE_FORMAT(' . $this->getLocalDate('UM1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
		        'UM1_MODIFIED_ON'      => 'DATE_FORMAT(' . $this->getLocalDate('UM1_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
                'UM1_CREATED_BY_NAME'  => 'us1_create.US1_NAME',
                'UM1_MODIFIED_BY_NAME' => 'us1_modify.US1_NAME'
            ])
            ->andFilterWhere(['=', 'UM1_DELETE_FLAG', 0])
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = UM1.UM1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = UM1.UM1_MODIFIED_BY");

        $this->load($params, '');

	    if (isset($this->UM1_ID)) {
		    $query->andFilterWhere(['=',    'UM1_ID',   $this->UM1_ID]);
	    }

	    if (isset($this->UM1_SHORT_CODE)) {
		    $query->andFilterWhere(['like',    'UM1_SHORT_CODE',   $this->UM1_SHORT_CODE]);
	    }

	    if (isset($this->UM1_NAME)) {
		    $query->andFilterWhere(['like',    'UM1_NAME',   $this->UM1_NAME]);
	    }

	    if (isset($this->UM1_SIZE)) {
		    $query->andFilterWhere(['like',    'UM1_SIZE',   $this->UM1_SIZE]);
	    }

	    $query->andFilterWhere(['=', 'UM1.UM1_DELETE_FLAG', 0]);
        $query->andFilterWhere(['>', 'LENGTH(UM1.UM1_NAME)', 0]);

        return $query;
    }

    public function search($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['UM1_NAME'] = SORT_ASC;
        }

        $query = $this->getSearchQuery( $params );

	    $pagination = false;
	    if( !empty($params['perPage'])){
		    $pagination = [
			    'pageSize' => $params['perPage'],
			    'page'     => $params['page']
		    ];
	    }
	    $dataProvider = new ActiveDataProvider([
		    'query'      => $query,
		    'pagination' => $pagination,
		    'sort' => [
			    'defaultOrder' => $defaultOrder
		    ]
	    ]);

        return $dataProvider;
    }
}
