<?php

namespace app\models;

use Yii;
use app\models\Label as LabelModel;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "la1_language".
 *
 * @property int $LA1_ID
 * @property string $LA1_SHORT_CODE
 * @property string $LA1_NAME
 * @property int $LA1_DELETE_FLAG
 * @property int $LA1_CREATED_BY
 * @property string $LA1_CREATED_ON
 * @property int $LA1_MODIFIED_BY
 * @property string $LA1_MODIFIED_ON
 */
class Language extends ActiveRecord
{
    protected $_tablePrefix = 'LA1';

	public $CREATED_BY;
	public $MODIFIED_BY;
	public $CREATED_ON;
	public $MODIFIED_ON;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'la1_language';
    }

	public function fields() {
		$fields = parent::fields();
		$fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
		$fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
		$fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
		$fields["MODIFIED_ON"] = function($model){ return $model->MODIFIED_ON; };
		return $fields;
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LA1_ID', 'LA1_DELETE_FLAG', 'LA1_CREATED_BY', 'LA1_MODIFIED_BY'], 'integer'],
            [['LA1_CREATED_ON', 'LA1_MODIFIED_ON'], 'safe'],
            [['LA1_SHORT_CODE'], 'string', 'max' => 10],
            [['LA1_NAME', 'CREATED_BY', 'MODIFIED_BY', 'CREATED_ON', 'MODIFIED_ON'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LA1_ID' => 'La1  ID',
            'LA1_SHORT_CODE' => 'La1  Short  Code',
            'LA1_NAME' => 'La1  Name',
            'LA1_DELETE_FLAG' => 'La1  Delete  Flag',
            'LA1_CREATED_BY' => 'La1  Created  By',
            'LA1_CREATED_ON' => 'La1  Created  On',
            'LA1_MODIFIED_BY' => 'La1  Modified  By',
            'LA1_MODIFIED_ON' => 'La1  Modified  On',
        ];
    }

    public function getVocabulary( $locale )
    {
        $langQuery = Language::find()->select('LA1_ID')
                        ->where(['=', 'LA1_LOCALE', $locale]);

        $vocabulary = LabelModel::find()->alias('lbl')
            ->select(['lbl.LA2_KEY', 'lbl.LA2_TEXT'])
            ->where(['IN', 'LA1_ID', $langQuery])
            ->orderBy(["lbl.LA2_TEXT" => SORT_ASC])
            ->asArray()->all();

        return $vocabulary;
    }

    public function getQuery($params)
    {
        $query = Language::find()->alias('la1_language')->select([
                'la1_language.*',
		        'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('LA1_MODIFIED_ON', $params) . ', "%Y-%m-%d %H:%i")',
		        'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('LA1_CREATED_ON', $params) . ', "%Y-%m-%d %H:%i")',
                'CREATED_BY'  => 'us1_create.US1_NAME',
                'MODIFIED_BY' => 'us1_modify.US1_NAME'
            ])
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = la1_language.LA1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = la1_language.LA1_MODIFIED_BY");

        $query->andFilterWhere(['=',    'la1_language.LA1_DELETE_FLAG', 0]);

        return $query;
    }

    public function getAllLanguages($params)
    {
        $query = $this->getQuery($params);

        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => false
            ]);

        $dataProvider->query->asArray();

        return $dataProvider;
    }

    public function getLanguage($id)
    {
        $query = $this->getQuery(null);
        $query->andFilterWhere(['=', 'la1_language.LA1_ID', $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        $dataProvider->query->asArray();

        return $dataProvider;
    }
}
