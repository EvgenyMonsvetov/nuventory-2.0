<?php

namespace app\models;

use app\models\scopes\UserQuery;
use yii\data\ActiveDataProvider;
use app\components\JwtTrait;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "us1_user".
 *
 * @property int $US1_ID
 * @property int $TE1_ID
 * @property int $CO1_ID
 * @property int $CG1_ID
 * @property int $LO1_ID
 * @property int $VD0_ID
 * @property int $LA1_ID
 * @property int $GR1_ID
 * @property int $US1_GATEKEEPER
 * @property int $﻿US1_PO_NOTIFY
 * @property string $US1_LOGIN
 * @property string $US1_PASS
 * @property string $US1_PASSWORD_HASH
 * @property string $US1_AUTH_KEY
 * @property string $US1_PASSWORD_RESET_TOKEN
 * @property string $US1_NAME
 * @property string $US1_EMAIL
 * @property string $US1_PHONE
 * @property int $US1_TYPE
 * @property int $US1_CORPORATE_REPORT
 * @property int $US1_INVENTORY_REPORT
 * @property int $US1_ALLOW_MOBILE_JOB_CREATE
 * @property int $DELETED
 * @property int $US1_CREATED_BY
 * @property string $US1_CREATED_ON
 * @property int $US1_MODIFIED_BY
 * @property string $US1_MODIFIED_ON
 * @property string $US1_AGREED_ON
 * @property string $CO1_IDS
 * @property int $US1_ACTIVE
 */
class User extends ActiveRecord implements IdentityInterface
{
    protected $_tablePrefix = 'US1';

    use JwtTrait;

    public $token;
    public $exp;
    public $timezone;
    public $permissions;
    public $old_password;
	public $new_password;
	public $repeat_password;
	public $password;
	public $GR1_ACCESS_LEVEL;
	public $centralLocation;

    const STATUS_DELETED  = 0;
    const STATUS_NOACTIVE = 1;
    const STATUS_ACTIVE   = 10;

    const SCENARIO_CHANGE_PASSWORD       = 'ChangePassword';
    const SCENARIO_ADD_USER              = 'AddUser';
    const SCENARIO_CHANGE_USER           = 'ChangeUser';
    const SCENARIO_ADMIN_CHANGE_PASSWORD = 'AdminChangePassword';

    protected $createdAtAttribute   = 'US1_CREATED_ON';
    protected $updatedAtAttribute   = 'US1_MODIFIED_ON';
    protected $createdByAttribute   = 'US1_CREATED_BY';
    protected $updatedByAttribute   = 'US1_MODIFIED_BY';
    protected $deletedAtAttribute   = 'DELETED';
    protected $useAttributeFullName = true;

	public $CO1_NAME;
	public $LO1_NAME;
	public $US1_ID_search;

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    public static function tableName()
    {
        return 'us1_user';
    }

    public static function primaryKey()
    {
       return ['US1_ID'];
    }

    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['GR1_ID' => 'GR1_ID']);
    }

    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['LO1_ID' => 'LO1_ID']);
    }

    public function relations()
    {
        return array(
            'season' => array(self::BELONGS_TO, 'Technician', 'TE1_ID'),
        );
    }

    public function getTechnician()
    {
        return $this->hasOne(Technician::className(), ['TE1_ID' => 'TE1_ID']);
    }

    public function rules()
    {
        return [
            [['US1_NAME', 'US1_LOGIN', 'DELETED', 'GR1_ID'], 'required', 'on' => [self::SCENARIO_CHANGE_USER, self::SCENARIO_ADD_USER]],
            [['password', 'repeat_password'], 'required', 'on'=>self::SCENARIO_ADD_USER],
            [['repeat_password'], 'compare', 'compareAttribute'=>'password', 'on'=>self::SCENARIO_ADD_USER],
            [['old_password', 'new_password', 'repeat_password'], 'required', 'on' => self::SCENARIO_CHANGE_PASSWORD],
            [['old_password'], 'validateOldPassword', 'on' => self::SCENARIO_CHANGE_PASSWORD],
            [['repeat_password'], 'compare', 'compareAttribute'=>'new_password', 'on'=>self::SCENARIO_CHANGE_PASSWORD],
            [['new_password', 'repeat_password'], 'required', 'on' => self::SCENARIO_ADMIN_CHANGE_PASSWORD],
            [['repeat_password'], 'compare', 'compareAttribute'=>'new_password', 'on'=>self::SCENARIO_ADMIN_CHANGE_PASSWORD],

            [['US1_NAME'], 'string', 'max' => 100],
            [['US1_LOGIN'], 'validateLogin'],
            [['US1_LOGIN'], 'match', 'pattern' => '/^[\#\sA-Za-z0-9!&_@\/,\'\|\-.]+$/u', 'message' => Yii::t('app', "Incorrect symbols {value} (A-z0-9).")],
            [['US1_LOGIN'], 'string', 'min' => 1, 'max' => 60,  'message' => Yii::t('app', "Incorrect username (length between 2 and 20 characters).")],
            [['US1_PASS'], 'string',  'min' => 1, 'max' => 128, 'message' => Yii::t('app', "Incorrect password (minimal length 1 symbols).")],
            [['US1_EMAIL'], 'email'],
            [['US1_PHONE'], 'string'],
            [['US1_PASSWORD_RESET_TOKEN'], 'unique'],

            [['US1_ID', 'TE1_ID', 'CO1_ID', 'CG1_ID', 'LO1_ID', 'VD0_ID', 'LA1_ID', 'GR1_ID', 'US1_GATEKEEPER', 'US1_PO_NOTIFY', 'US1_TYPE', 'US1_CORPORATE_REPORT', 'US1_INVENTORY_REPORT', 'US1_ALLOW_MOBILE_JOB_CREATE', 'DELETED', 'US1_CREATED_BY', 'US1_MODIFIED_BY', 'US1_ACTIVE'], 'integer'],
            [['US1_CREATED_ON', 'US1_MODIFIED_ON', 'US1_AGREED_ON'], 'safe'],
            [['CO1_IDS', 'CO1_NAME', 'LO1_NAME', 'US1_ID_search'], 'string'],
            [['US1_AUTH_KEY'], 'string', 'max' => 32],
            [['US1_NAME'], 'string', 'max' => 150],
            [['TE1_ID'], 'unique']
        ];
    }

    public function validateLogin($attribute, $params)
    {
        $user = static::find()
                    ->where(["= BINARY","US1_LOGIN",$this->US1_LOGIN])
                    ->andWhere(['DELETED' => 0])
                    ->one();

        if($user){
            if( $this->isNewRecord ){
                $this->addError($attribute, Yii::t('app', "That username is taken."));
            }elseif( $this->US1_ID != $user->US1_ID ){
                $this->addError($attribute, Yii::t('app', "That username is taken."));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
    /**
     * @inheritdoc
    */
    public function fields()
    {
        return [
            'US1_ID', 'US1_NAME', 'US1_LOGIN', 'US1_EMAIL', 'US1_PHONE', 'US1_AUTH_KEY', 'US1_PASSWORD_HASH',
            'US1_PASSWORD_RESET_TOKEN', 'US1_MODIFIED_ON', 'US1_CREATED_ON', 'DELETED', 'US1_ACTIVE',
            'token', 'permissions', 'exp', 'timezone', 'GR1_ACCESS_LEVEL', 'TE1_ID', 'CO1_ID', 'LO1_ID', 'CO1_NAME', 'LO1_NAME', 'US1_ID_search', 'GR1_ID', 'centralLocation'
        ];
    }


	/**
     * Finds user by username
     *
     * @param string $username
     * @return User
     */

    public static function findByUsername($username)
    {
        $user = static::find()->alias('US1')
            ->select([
                'US1_ID', 'US1_NAME', 'US1_LOGIN', 'US1_EMAIL', 'US1_PHONE', 'US1_PASSWORD_HASH', 'CO1_ID', 'LO1_ID',
                'US1_AUTH_KEY', 'US1_CREATED_ON', 'US1_MODIFIED_ON', 'GR1_ID', 'VD0_ID', 'US1_ACTIVE'])
            ->andWhere(["= BINARY","US1_LOGIN",$username])
            ->andWhere(['=', 'US1.DELETED', 0])
            ->one();

        return $user;
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'US1_PASSWORD_RESET_TOKEN' => $token
        ]);
    }

    public static function findByTE1_ID($TE1_ID)
    {
        $user = static::find()->alias('US1')
                    ->select([
                        'US1_ID', 'US1_NAME', 'US1_LOGIN', 'US1_EMAIL', 'US1_PHONE', 'US1_PASSWORD_HASH',
                        'US1_AUTH_KEY', 'US1_CREATED_ON', 'US1_MODIFIED_ON', 'TE1_ID', 'US1_ACTIVE'])
                    ->andWhere(['TE1_ID' => $TE1_ID])
                    ->one();

        return $user;
    }

    //matching the old password with your existing password.
	public function validateOldPassword($attribute, $params)
	{
		if (!$this->validatePassword($this->old_password)){
		    $this->addError($attribute, 'Old password is incorrect.');
        }
	}

	public function isAtLeastCompanyManager()
    {
        $result = false;
        if ( $this->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
             $this->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ){
            $result = true;
        }
        return $result;
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['jwt']['passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public static function afterLogin($event)
    {
        $event->identity->generateCredentionals($event->identity->getId());
    }

    public function generateCredentionals( $userId )
    {
        Yii::$app->session->regenerateID();

        $jwt = $this->generateToken();
        $this->token    = $jwt['token'];
        $this->exp      = Yii::$app->formatter->asDate($jwt['exp'], 'php:Y-m-d H:i:s');
        $this->timezone = date_default_timezone_get();
        $this->GR1_ACCESS_LEVEL = $this->group->GR1_ACCESS_LEVEL;
        $this->setCurrentUserId($userId);

        $roleName = $this->group->ROLE_NAME;
        if($roleName){
            $permissions = Yii::$app->authManager->getPermissionsByRole($roleName);
        }else{
            $permissions = Yii::$app->authManager->getPermissionsByUser($userId);
        }
        $this->permissions = array_keys(ArrayHelper::toArray($permissions));

        if(Yii::$app->session->get('LOGIN_ACTION')){
            Yii::$app->session->set('CO1_ID', $this->CO1_ID);
            Yii::$app->session->set('LO1_ID', $this->LO1_ID);
            Yii::$app->session->remove('LOGIN_ACTION');
        }elseif( $this->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
                 $this->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $this->CO1_ID = Yii::$app->session->get('CO1_ID');
            $this->LO1_ID = Yii::$app->session->get('LO1_ID');
        }elseif( $this->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
                 $this->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ){
            $this->LO1_ID = Yii::$app->session->get('LO1_ID');
        }

        $this->centralLocation = Location::getCentralLocation($this->CO1_ID);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function setCurrentUserId( $userId )
    {
        Yii::$app->db->createCommand('SET @user_id = :user_id', [':user_id' => $userId])
                ->execute();
    }
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->US1_AUTH_KEY;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->US1_PASSWORD_HASH);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->US1_PASSWORD_HASH = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->US1_AUTH_KEY = Yii::$app->security->generateRandomString();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->US1_PASSWORD_RESET_TOKEN = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->US1_PASSWORD_RESET_TOKEN = null;
    }

    /**
     * @param $params
     * @return UserQuery
     */
    public function getSearchQuery( $params )
    {
        $query = self::find()->alias('US1')->select([
                'US1.*',
                'US1_CREATED_BY_NAME'  => 'us1_create.US1_NAME',
                'US1_MODIFIED_BY_NAME' => 'us1_modify.US1_NAME',
                'CO1.CO1_NAME',
                'CO1.CO1_SHORT_CODE',
                'LO1.LO1_NAME',
                'LO1.LO1_SHORT_CODE',
                'VD0.VD0_NAME',
	            'GR1.GR1_NAME'
            ])
            ->leftJoin("co1_company  as CO1" ,    "CO1.CO1_ID = US1.CO1_ID AND CO1.CO1_DELETE_FLAG=0")
            ->leftJoin("lo1_location as LO1" ,    "LO1.LO1_ID = US1.LO1_ID AND LO1.LO1_DELETE_FLAG=0")
            ->leftJoin("vd0_vendor as VD0" ,      "VD0.VD0_ID = US1.VD0_ID AND VD0.VD0_DELETE_FLAG=0")
            ->leftJoin("gr1_group as GR1" ,       "GR1.GR1_ID = US1.GR1_ID")
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = US1.US1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = US1.US1_MODIFIED_BY");

        $this->load($params, '');

        $query->andFilterWhere(['=',    'US1.US1_ID',     $this->US1_ID]);
        $query->andFilterWhere(['like', 'US1.US1_NAME',   $this->US1_NAME]);
        $query->andFilterWhere(['like', 'US1.US1_LOGIN',  $this->US1_LOGIN]);
        $query->andFilterWhere(['like', 'US1.US1_EMAIL',  $this->US1_EMAIL]);
        $query->andFilterWhere(['=',    'LO1.CO1_ID',     $this->CO1_ID]);
        $query->andFilterWhere(['=',    'LO1.LO1_ID',     $this->LO1_ID]);
        $query->andFilterWhere(['=',    'US1.GR1_ID',     $this->GR1_ID]);
        $query->andFilterWhere(['=',    'US1.US1_ACTIVE', $this->US1_ACTIVE]);

	    if (isset($this->US1_ID_search)) {
		    $query->andFilterWhere(['like', 'US1.US1_ID',   $this->US1_ID_search]);
	    }

	    if (isset($params['CO1_IDs']) || isset($params['LO1_IDs']) || isset($params['GR1_IDs'])) {
		    if (isset($params['CO1_IDs'])) {
			    $query->andFilterWhere(['IN', 'US1.CO1_ID', explode(',', $params['CO1_IDs'])]);
		    }
		    if (isset($params['LO1_IDs'])) {
			    $query->andFilterWhere(['IN', 'US1.LO1_ID', explode(',', $params['LO1_IDs'])]);
		    }
		    if (isset($params['GR1_IDs'])) {
			    $query->andFilterWhere(['IN', 'US1.GR1_ID', explode(',', $params['GR1_IDs'])]);
		    }
		    $query->andFilterWhere(['is not', 'US1.US1_EMAIL', new \yii\db\Expression('null')]);
		    $query->andFilterWhere(['>', 'LENGTH(US1.US1_EMAIL)', 0]);
		    $query->andFilterWhere(['=', 'US1.DELETED', 0]);
	    }
        return $query;
    }

    public function search($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['US1_NAME'] = SORT_ASC;
        }

        $query = $this->getSearchQuery( $params );
        $query->initScope();

	    if (isset($params['perPage']) && isset($params['page'])) {
		    $pagination = [
			    'pageSize' => $params['perPage'],
			    'page'     => $params['page']
		    ];
	    } else {
		    $pagination = false;
	    }

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => $pagination,
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

    public function beforeSave($insert)
    {
        if( empty($this->US1_PO_NOTIFY)){
            $this->US1_PO_NOTIFY = 0;
        }

        $this->US1_LOGIN = trim($this->US1_LOGIN);
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function delete()
    {
        $this->DELETED = 1;
        return $this->save(false);
    }
}
