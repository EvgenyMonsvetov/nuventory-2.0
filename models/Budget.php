<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "bu1_budgets".
 *
 * @property int $BU1_ID
 * @property int $CO1_ID
 * @property int $LO1_ID
 * @property string $BU1_DATE
 * @property int $BU1_DELETE_FLAG
 * @property int $BU1_CREATED_BY
 * @property int $BU1_MODIFIED_BY
 * @property string $BU1_CREATED_ON
 * @property string $BU1_MODIFIED_ON
 * @property string $BU1_MONTHLY_GROSS_SALE
 * @property string $BU1_TARGET_PERCENTAGE
 * @property string $BU1_FINAL_RESULT
 *
 * @property Us1User $bU1CREATEDBY
 * @property Us1User $bU1MODIFIEDBY
 */
class Budget extends ActiveRecord
{
    protected $validationId;

    protected $_tablePrefix = 'BU1';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bu1_budgets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['BU1_DATE', 'validateDate'],
            [['BU1_MONTHLY_GROSS_SALE', 'BU1_TARGET_PERCENTAGE'], 'double', 'min' => 0],
            [['BU1_TARGET_PERCENTAGE'], 'double', 'max' => 100],
            [['BU1_DATE', 'BU1_MONTHLY_GROSS_SALE', 'BU1_TARGET_PERCENTAGE'], 'required'],
            [['CO1_ID', 'LO1_ID', 'BU1_DELETE_FLAG', 'BU1_CREATED_BY', 'BU1_MODIFIED_BY'], 'integer'],
            [['BU1_DATE', 'BU1_CREATED_ON', 'BU1_MODIFIED_ON'], 'safe'],
            [['BU1_MONTHLY_GROSS_SALE', 'BU1_TARGET_PERCENTAGE', 'BU1_FINAL_RESULT'], 'number'],
            [['BU1_CREATED_BY'], 'exist', 'skipOnError' => true, 'targetClass' => Us1User::className(), 'targetAttribute' => ['BU1_CREATED_BY' => 'US1_ID']],
            [['BU1_MODIFIED_BY'], 'exist', 'skipOnError' => true, 'targetClass' => Us1User::className(), 'targetAttribute' => ['BU1_MODIFIED_BY' => 'US1_ID']],
        ];
    }

    public function validateDate($attribute, $params)
    {
        $user = Yii::$app->user->getIdentity();
        $budget = static::find()
                    ->where(["=","BU1_DATE",date('Y-m-d', (strtotime($this->BU1_DATE)))])
                    ->andWhere(['CO1_ID' => $user->CO1_ID])
                    ->andWhere(['LO1_ID' => $user->LO1_ID])
                    ->andWhere(['BU1_DELETE_FLAG' => 0])
                    ->one();

        if($budget){
            if($this->isNewRecord || $this->BU1_ID != $budget->BU1_ID){
                $this->addError($attribute, Yii::t('app', "That budget is existed."));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'BU1_ID' => 'Bu1  ID',
            'CO1_ID' => 'Co1  ID',
            'LO1_ID' => 'Lo1  ID',
            'BU1_DATE' => 'Bu1  Date',
            'BU1_DELETE_FLAG' => 'Bu1  Delete  Flag',
            'BU1_CREATED_BY' => 'Bu1  Created  By',
            'BU1_MODIFIED_BY' => 'Bu1  Modified  By',
            'BU1_CREATED_ON' => 'Bu1  Created  On',
            'BU1_MODIFIED_ON' => 'Bu1  Modified  On',
            'BU1_MONTHLY_GROSS_SALE' => 'Bu1  Monthly  Gross  Sale',
            'BU1_TARGET_PERCENTAGE' => 'Bu1  Target  Percentage',
            'BU1_FINAL_RESULT' => 'Bu1  Final  Result',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBU1CREATEDBY()
    {
        return $this->hasOne(Us1User::className(), ['US1_ID' => 'BU1_CREATED_BY']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBU1MODIFIEDBY()
    {
        return $this->hasOne(Us1User::className(), ['US1_ID' => 'BU1_MODIFIED_BY']);
    }

    public function search($params)
    {
        $user = Yii::$app->user->getIdentity();
        $this->LO1_ID = $user->LO1_ID;
        $this->CO1_ID = $user->CO1_ID;

        $defaultOrder['BU1_DATE']   = SORT_ASC;

        $query = self::find()->alias('BU1')->select([
            'BU1.*',
            'BU1_CREATED_BY'  => 'us1_create.US1_NAME',
            'BU1_MODIFIED_BY' => 'us1_modify.US1_NAME',
            'CO1.CO1_NAME',
            'LO1.LO1_NAME',
	        'MONTH' => 'DATE_FORMAT(BU1_DATE, "%b %Y")',
	        'FORMATTED_DATE' => 'DATE_FORMAT(BU1_DATE, "%Y%m%d")'
        ])
            ->leftJoin("co1_company  as CO1" ,    "CO1.CO1_ID = BU1.CO1_ID AND CO1.CO1_DELETE_FLAG=0")
            ->leftJoin("lo1_location as LO1", "LO1.LO1_ID = BU1.LO1_ID AND LO1.LO1_DELETE_FLAG = 0")
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = BU1.BU1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = BU1.BU1_MODIFIED_BY");
        $this->load($params, '');


        if (isset($params['BU1_ID'])) {
            $query->andFilterWhere(['=', 'BU1.BU1_ID', $params['BU1_ID']]);
        }
        if (isset($this->CO1_ID)) {
            $query->andFilterWhere(['=', 'BU1.CO1_ID', $this->CO1_ID]);
        }
        if (isset($this->LO1_ID)) {
            $query->andFilterWhere(['=', 'BU1.LO1_ID', $this->LO1_ID]);
        }
	    if (isset($params['DASHBOARD_REPORT']) || isset($params['PROJECTED_PO_USAGE']) || isset($params['MATERIAL_BUDGET_REPORT'])) {
		    $query->andFilterWhere(['>=', 'DATE(BU1.BU1_DATE)', $params['START_DATE']]);
		    $query->andFilterWhere(['<=', 'DATE(BU1.BU1_DATE)', $params['END_DATE']]);
		    if (isset($params['DASHBOARD_REPORT'])) {
			    $query->orderBy(["FORMATTED_DATE" => SORT_ASC]);
		    }
	    }
	    $query->andFilterWhere(['BU1.BU1_DELETE_FLAG' => 0]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => isset($params['perPage']) ? $params['perPage'] : false
            ],
            'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

    public function beforeSave($insert)
    {
        $user = Yii::$app->user->getIdentity();
        $this->LO1_ID = $user->LO1_ID;
        $this->CO1_ID = $user->CO1_ID;

        return parent::beforeSave($insert);
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->validationId = $this->BU1_ID;
            return true;
        }
        return false;
    }

    public function deleteItem() {
        $this->BU1_DELETE_FLAG = 1;
        $this->save();
        return $this;
    }
}
