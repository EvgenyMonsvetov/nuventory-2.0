<?php

namespace app\models;

use app\models\scopes\CustomerQuery;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "cu1_customer".
 *
 * @property int $CU1_ID
 * @property int $CO1_ID
 * @property string $CU1_SHORT_CODE
 * @property string $CU1_NAME
 * @property string $CU1_ADDRESS1
 * @property string $CU1_ADDRESS2
 * @property string $CU1_CITY
 * @property string $CU1_STATE
 * @property string $CU1_ZIP
 * @property string $CU1_PHONE
 * @property string $CU1_FAX
 * @property string $CU1_EMAIL
 * @property int $CY1_ID
 * @property int $CU1_DELETE_FLAG
 * @property int $CU1_CREATED_BY
 * @property string $CU1_CREATED_ON
 * @property int $CU1_MODIFIED_BY
 * @property string $CU1_MODIFIED_ON
 * @property int $LO1_CREATED
 */

class Customer extends ActiveRecord
{
   protected $_tablePrefix = 'CU1';

   public $CO1_NAME;
   public $CY1_NAME;

   public static function find()
   {
        return new CustomerQuery(get_called_class());
   }

   /**
    * {@inheritdoc}
    */
   public static function tableName()
   {
       return 'cu1_customer';
   }

   public static function primaryKey()
   {
       return ['CU1_ID', 'CO1_ID'];
   }

   public function getCompany()
   {
       return $this->hasOne(Company::className(), ['CO1_ID' => 'CO1_ID']);
   }

	public function fields() {
		$fields = parent::fields();
		$fields["CO1_NAME"] = function($model){ return $model->CO1_NAME; };
		$fields["CY1_NAME"] = function($model){ return $model->CY1_NAME; };
		return $fields;
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CU1_ID', 'CO1_ID', 'CY1_ID', 'CU1_DELETE_FLAG', 'CU1_CREATED_BY', 'CU1_MODIFIED_BY', 'LO1_CREATED'], 'integer'],
            [['CU1_CREATED_ON', 'CU1_MODIFIED_ON'], 'safe'],
            [['CU1_SHORT_CODE', 'CU1_CITY', 'CU1_STATE'], 'string', 'max' => 50],
            [['CU1_NAME', 'CU1_ADDRESS1', 'CU1_ADDRESS2', 'CO1_NAME', 'CY1_NAME'], 'string', 'max' => 150],
            [['CU1_ZIP', 'CU1_PHONE', 'CU1_FAX'], 'string', 'max' => 20],
            [['CU1_EMAIL'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CU1_ID' => 'Cu1  ID',
            'CO1_ID' => 'Co1  ID',
            'CU1_SHORT_CODE' => 'Cu1  Short  Code',
            'CU1_NAME' => 'Cu1  Name',
            'CU1_ADDRESS1' => 'Cu1  Address1',
            'CU1_ADDRESS2' => 'Cu1  Address2',
            'CU1_CITY' => 'Cu1  City',
            'CU1_STATE' => 'Cu1  State',
            'CU1_ZIP' => 'Cu1  Zip',
            'CU1_PHONE' => 'Cu1  Phone',
            'CU1_FAX' => 'Cu1  Fax',
            'CY1_ID' => 'Cy1  ID',
            'CU1_DELETE_FLAG' => 'Cu1  Delete  Flag',
            'CU1_CREATED_BY' => 'Cu1  Created  By',
            'CU1_CREATED_ON' => 'Cu1  Created  On',
            'CU1_MODIFIED_BY' => 'Cu1  Modified  By',
            'CU1_MODIFIED_ON' => 'Cu1  Modified  On',
            'LO1_CREATED' => 'Lo1  Created',
        ];
    }

    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery( $params=[] )
    {
        $query = self::find()->alias('CU1')->select([
                'CU1.*',
                'CU1_CREATED_BY_NAME'  => 'us1_create.US1_NAME',
                'CU1_MODIFIED_BY_NAME' => 'us1_modify.US1_NAME',
	            'CU1_MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('CU1_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
	            'CU1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('CU1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                'CY1.CY1_NAME',
                'CO1.CO1_NAME'
            ])
            ->leftJoin("cy1_country as CY1" ,     "CY1.CY1_ID = CU1.CY1_ID AND CY1.CY1_DELETE_FLAG=0")
            ->leftJoin("co1_company as CO1" ,     "CO1.CO1_ID = CU1.CO1_ID AND CO1.CO1_DELETE_FLAG=0")
            ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = CU1.CU1_CREATED_BY")
            ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = CU1.CU1_MODIFIED_BY");

        $this->load($params, '');

        $query->andFilterWhere([
            'CU1.CU1_ID'           => $this->CU1_ID,
            'CU1.CO1_ID'           => $this->CO1_ID
        ]);
        $query->andFilterWhere(['like', 'CU1_NAME', $this->CU1_NAME]);
        $query->initScope();

        return $query;
    }

    public function search($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['CO1_ID']   = SORT_ASC;
            $defaultOrder['CU1_NAME'] = SORT_ASC;
        }

        $query = $this->getSearchQuery( $params );

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => $params['perPage']
            ],
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return $dataProvider;
    }

    public static function findByID($CU1_ID)
    {
        $customer = static::find()->alias('CU1')
                    ->select([
                        '*'])
                    ->andWhere(['CU1_ID' => $CU1_ID])
                    ->one();

        return $customer;
    }
}
