<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table 'ta1_target_entry'.
 *
 * The followings are the available columns in table 'ta1_target_entry':
 * @property int $id
 * @property int $CO1_ID
 * @property string $ta1_allied
 * @property string $ta1_allied_filler
 * @property string $ta1_allied_paper
 * @property string $ta1_allied_tape
 * @property string $ta1_allied_fine_sand_paper
 * @property string $ta1_allied_rough_sand_paper
 * @property string $ta1_allied_detail
 * @property string $ta1_allied_pps_cups
 * @property string $ta1_liquid
 * @property string $ta1_liquid_clear
 * @property string $ta1_liquid_hardener
 * @property string $ta1_liquid_toner
 * @property string $ta1_liquid_primer
 * @property string $ta1_liquid_sealer
 * @property string $ta1_liquid_thinner
 * @property string $ta1_liquid_solvent
 * @property string $ta1_misc
 * @property string $ta1_misc_safety
 * @property string $ta1_pm_sold_per_ro
 * @property string $ta1_pm_cost_per_ro
 * @property string $ta1_clear_per_ro
 * @property string $ta1_toner_per_ro
 * @property string $ta1_allied_per_ro
 * @property string $ta1_misc_per_ro
 * @property string $ta1_gp_percent
 * @property string $ta1_paint_sold_percent_to_sales
 * @property string $ta1_paint_and_matl_sold_percent
 * @property string $ta1_material_cost_vs_sales
 * @property string $ta1_liquid_vs_sales
 * @property string $ta1_material_vs_paint_labor
 * @property string $ta1_ref_labor_vs_sales
 * @property string $ta1_paint_labor_vs_sales
 * @property string $ta1_ref_and_paint_labor_vs_sales
 * @property string $ta1_allied_per_ro_p
 * @property string $ta1_average_ro
 * @property string $ta1_clear_per_ro_p
 * @property string $ta1_clear_vs_paint_labor_p
 * @property string $ta1_cost_per_paint_hour_allied
 * @property string $ta1_cost_per_paint_hour_liquid
 * @property string $ta1_cost_per_paint_hour_total
 * @property string $ta1_hdnr_vs_paint_labor_p
 * @property string $ta1_pmr_vs_paint_labor_p
 * @property string $ta1_ppg_liq_vs_paint_and_ref_labor_p
 * @property string $ta1_ppg_liq_vs_sales_p
 * @property string $ta1_ppg_vs_all_grand_total_p
 * @property string $ta1_ppg_vs_paint_labor_p
 * @property string $ta1_paint_labor_per_ro
 * @property string $ta1_tape_vs_paint_labor_p
 * @property string $ta1_thinner_vs_paint_labor_p
 * @property string $ta1_toner_per_ro_p
 * @property string $ta1_toner_vs_paint_labor_p
 * @property string $ta1_paint_and_matl_sold_per_ro
 * @property string $ta1_material_target_p
 * @property string $ta1_material_target_total
 * @property int $created_by
 * @property string $created_on
 * @property int $modified_by
 * @property string $modified_on
 * @property int $delete_flag
 * @property int $update_flag
 * The followings are the available model relations:
 * @property Profile $cprofile
 * @property Profile $mprofile
 */
class TargetEntry extends \yii\db\ActiveRecord
{
	/**
	 * @return \yii\db\Connection the database connection used by this AR class.
	 */
	public static function getDb()
	{
		return Yii::$app->get('dbdash');
	}

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ta1_target_entry';
    }

	public static function primaryKey()
	{
		return ['LG2_ID'];
	}

    /**
     * {@inheritdoc}
     */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
//			['', 'required'],
			[ ['delete_flag', 'update_flag'], 'number', 'integerOnly' => true],
			[['CO1_ID', 'created_by', 'modified_by'], 'string', 'max' => 20],
			[['CO1_ID'], 'unique','message' => 'The company has already been assigned target entries'],
			[['ta1_material_target_p', 'ta1_paint_and_matl_sold_per_ro', 'ta1_cost_per_paint_hour_allied', 'ta1_cost_per_paint_hour_liquid', 'ta1_allied_per_ro_p', 'ta1_allied', 'ta1_allied_filler', 'ta1_allied_paper', 'ta1_allied_tape', 'ta1_allied_fine_sand_paper', 'ta1_allied_rough_sand_paper', 'ta1_allied_detail', 'ta1_allied_pps_cups', 'ta1_liquid', 'ta1_liquid_clear', 'ta1_liquid_hardener', 'ta1_liquid_toner', 'ta1_liquid_primer', 'ta1_liquid_sealer', 'ta1_liquid_thinner', 'ta1_liquid_solvent', 'ta1_misc', 'ta1_misc_safety', 'ta1_pm_sold_per_ro', 'ta1_pm_cost_per_ro', 'ta1_clear_per_ro', 'ta1_toner_per_ro', 'ta1_allied_per_ro', 'ta1_misc_per_ro', 'ta1_gp_percent', 'ta1_paint_sold_percent_to_sales', 'ta1_paint_and_matl_sold_percent', 'ta1_material_cost_vs_sales', 'ta1_liquid_vs_sales', 'ta1_material_vs_paint_labor', 'ta1_ref_labor_vs_sales', 'ta1_paint_labor_vs_sales', 'ta1_ref_and_paint_labor_vs_sales', 'ta1_average_ro', 'ta1_clear_per_ro_p', 'ta1_clear_vs_paint_labor_p', 'ta1_cost_per_paint_hour_total', 'ta1_hdnr_vs_paint_labor_p', 'ta1_pmr_vs_paint_labor_p', 'ta1_ppg_liq_vs_paint_and_ref_labor_p', 'ta1_ppg_liq_vs_sales_p', 'ta1_ppg_vs_all_grand_total_p', 'ta1_ppg_vs_paint_labor_p', 'ta1_paint_labor_per_ro', 'ta1_tape_vs_paint_labor_p', 'ta1_thinner_vs_paint_labor_p', 'ta1_toner_per_ro_p', 'ta1_toner_vs_paint_labor_p'], 'string', 'min' => '1', 'max' => 10],
			[['modified_on'], 'safe'],

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			[['id', 'CO1_ID', 'ta1_material_target_p', 'ta1_allied', 'ta1_allied_filler', 'ta1_allied_paper', 'ta1_allied_tape', 'ta1_allied_fine_sand_paper', 'ta1_allied_rough_sand_paper', 'ta1_allied_detail', 'ta1_allied_pps_cups', 'ta1_liquid', 'ta1_liquid_clear', 'ta1_liquid_hardener', 'ta1_liquid_toner', 'ta1_liquid_primer', 'ta1_liquid_sealer', 'ta1_liquid_thinner', 'ta1_liquid_solvent', 'ta1_misc', 'ta1_misc_safety', 'ta1_pm_sold_per_ro', 'ta1_pm_cost_per_ro', 'ta1_clear_per_ro', 'ta1_toner_per_ro', 'ta1_allied_per_ro', 'ta1_misc_per_ro', 'ta1_gp_percent', 'ta1_paint_sold_percent_to_sales', 'ta1_paint_and_matl_sold_percent', 'ta1_material_cost_vs_sales', 'ta1_liquid_vs_sales', 'ta1_material_vs_paint_labor', 'ta1_ref_labor_vs_sales', 'ta1_paint_labor_vs_sales', 'ta1_ref_and_paint_labor_vs_sales', 'created_by', 'created_on', 'modified_by', 'modified_on', 'delete_flag', 'update_flag', 'cprofile_search', 'mprofile_search'], 'safe', 'on' => 'search'],

			// Rules for relations
			//array('REQUIRED_COLUMNS_ONLY_FOR_RELATION_SEPARATED_BY_COMMA', 'required', 'on' => 'relation'),
		];
	}

	/**
	 * REQUIRED
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'CO1_ID' => 'Company',

			'ta1_material_target_p' => 'Material Target %',
			'ta1_gp_percent' => 'Paint GP %',
			'ta1_paint_sold_percent_to_sales' => 'Paint Sold to Sales %',
			'ta1_paint_and_matl_sold_percent' => 'Paint and Materials Sold %',
			'ta1_cost_per_paint_hour_total' => 'Cost per Paint Hour Total',
			'ta1_cost_per_paint_hour_liquid' => 'Cost per Paint Hour Liquid',
			'ta1_cost_per_paint_hour_allied' => 'Cost per Paint Hour Allied',
			'ta1_ppg_vs_all_grand_total_p' => 'Liquid vs All Grand Total %',
			'ta1_ppg_liq_vs_sales_p' => 'Liquid vs Sales %',
			'ta1_material_vs_paint_labor' => 'Materials vs Paint Labor %',
			'ta1_ref_labor_vs_sales' => 'Ref Labor vs Sales %',
			'ta1_paint_labor_vs_sales' => 'Paint Labor vs Sales %',
			'ta1_ref_and_paint_labor_vs_sales' => 'Ref and Paint Labor vs Sales %',
			'ta1_clear_vs_paint_labor_p' => 'Clear vs Paint Labor %',
			'ta1_hdnr_vs_paint_labor_p' => 'HDNR vs Paint Labor %',
			'ta1_toner_vs_paint_labor_p' => 'Toner vs Paint Labor %',
			'ta1_pmr_vs_paint_labor_p' => 'PMR vs Paint Labor %',
			'ta1_tape_vs_paint_labor_p' => 'Tape vs Paint Labor %',
			'ta1_thinner_vs_paint_labor_p' => 'Thinner vs Paint Labor %',
			'ta1_ppg_vs_paint_labor_p' => 'Liquid vs Paint Labor %',
			'ta1_ppg_liq_vs_paint_and_ref_labor_p' => 'Liquid vs Paint and Ref Labor %',
			'ta1_average_ro' => 'Average RO',
			'ta1_paint_labor_per_ro' => 'Paint Labor per RO',
			'ta1_pm_cost_per_ro' => 'PM Cost per RO',
			'ta1_clear_per_ro' => 'Clear per RO',
			'ta1_toner_per_ro' => 'Toner per RO',
			'ta1_paint_and_matl_sold_per_ro' => 'Paint and Materials Sold per RO',
			'ta1_misc_per_ro' => 'Misc per RO',
			'ta1_clear_per_ro_p' => 'Clear per RO',
			'ta1_toner_per_ro_p' => 'Toner per RO',
			'ta1_allied_per_ro' => 'Allied per RO',
			'ta1_material_cost_vs_sales' => 'Materials Cost vs Sales %',

			'ta1_allied_per_ro_p' => 'Allied per RO',
			'r03_c_pm_cost_per_ro' => '',
			'r03_c_clear_per_ro' => '',
			'r03_c_toner_per_ro' => '',
			'r03_c_allied_per_ro' => '',
			'r03_c_paint_and_materials_sold_per_ro' => '',
			'r03_c_misc_per_ro' => '',
			'r03_c_clear_per_ro_p' => '',
			'r03_c_toner_per_ro_p' => '',
			'r03_c_allied_per_ro_p' => '',
			'r03_c_materials_cost_vs_sales_p' => '',

			'ta1_allied' => 'Allied',
			'ta1_allied_filler' => 'Allied Filler',
			'ta1_allied_paper' => 'Allied Paper',
			'ta1_allied_tape' => 'Allied Tape',
			'ta1_allied_fine_sand_paper' => 'Allied Fine Sand Paper',
			'ta1_allied_rough_sand_paper' => 'Allied Rough Sand Paper',
			'ta1_allied_detail' => 'Allied Detail',
			'ta1_allied_pps_cups' => 'Allied PPS Cups',
			'ta1_liquid' => 'Liquid',
			'ta1_liquid_clear' => 'Liquid Clear',
			'ta1_liquid_hardener' => 'Liquid Hardener',
			'ta1_liquid_toner' => 'Liquid Toner',
			'ta1_liquid_primer' => 'Liquid Primer',
			'ta1_liquid_sealer' => 'Liquid Sealer',
			'ta1_liquid_thinner' => 'Liquid Thinner',
			'ta1_liquid_solvent' => 'Liquid Solvent',
			'ta1_misc' => 'Misc',
			'ta1_misc_safety' => 'Misc Safety',
			'ta1_pm_sold_per_ro' => 'PM Sold per Ro',
			'ta1_liquid_vs_sales' => 'Liquid Vs Sales',

			'created_by' => 'Created By',
			'created_on' => 'Created On',
			'modified_by' => 'Modified By',
			'modified_on' => 'Modified On',
			'delete_flag' => 'Delete Flag',
			'update_flag' => 'Update Flag',
			'cprofile_search' => Yii::t('app', 'Created By'),
			'mprofile_search' => Yii::t('app', 'Modified By'),
		);
	}

	/**
	 * Generates new Target Entries for company based on all other companies average
	 * @return array targetEntry
	 */
	public function generateTargets($CO1_ID){
		$query = self::find()->alias('TA1')
			->select(['AVG(ta1_material_target_p)', 'AVG(ta1_material_target_total)', 'AVG(ta1_allied)', 'AVG(ta1_allied_filler)', 'AVG(ta1_allied_paper)',
				'AVG(ta1_allied_tape)', 'AVG(ta1_allied_fine_sand_paper)', 'AVG(ta1_allied_rough_sand_paper)', 'AVG(ta1_allied_detail)', 'AVG(ta1_allied_pps_cups)',
				'AVG(ta1_liquid)', 'AVG(ta1_liquid_hardener)', 'AVG(ta1_liquid_toner)', 'AVG(ta1_liquid_sealer)', 'AVG(ta1_liquid_thinner)',
				'AVG(ta1_liquid_solvent)', 'AVG(ta1_misc)', 'AVG(ta1_misc_safety)', 'AVG(ta1_pm_sold_per_ro)', 'AVG(ta1_pm_cost_per_ro)',
				'AVG(ta1_clear_per_ro)', 'AVG(ta1_toner_per_ro)', 'AVG(ta1_allied_per_ro)', 'AVG(ta1_misc_per_ro)', 'AVG(ta1_gp_percent)',
				'AVG(ta1_paint_sold_percent_to_sales)', 'AVG(ta1_paint_and_matl_sold_percent)', 'AVG(ta1_material_cost_vs_sales)', 'AVG(ta1_liquid_vs_sales)',
				'AVG(ta1_material_vs_paint_labor)', 'AVG(ta1_ref_labor_vs_sales)', 'AVG(ta1_paint_labor_vs_sales)', 'AVG(ta1_ref_and_paint_labor_vs_sales)',
				'AVG(ta1_allied_per_ro_p)', 'AVG(ta1_average_ro)', 'AVG(ta1_clear_per_ro_p)', 'AVG(ta1_clear_vs_paint_labor_p)', 'AVG(ta1_cost_per_paint_hour_total)',
				'AVG(ta1_hdnr_vs_paint_labor_p)', 'AVG(ta1_pmr_vs_paint_labor_p)', 'AVG(ta1_ppg_liq_vs_paint_and_ref_labor_p)', 'AVG(ta1_ppg_liq_vs_sales_p)',
				'AVG(ta1_ppg_vs_all_grand_total_p)', 'AVG(ta1_ppg_vs_paint_labor_p)', 'AVG(ta1_paint_labor_per_ro)', 'AVG(ta1_tape_vs_paint_labor_p)',
				'AVG(ta1_thinner_vs_paint_labor_p)', 'AVG(ta1_toner_per_ro_p)', 'AVG(ta1_toner_vs_paint_labor_p)', 'AVG(ta1_cost_per_paint_hour_allied)',
				'AVG(ta1_cost_per_paint_hour_liquid)', 'AVG(ta1_paint_and_matl_sold_per_ro)']);

		$avg = $query->asArray()->one();

		if(isset($avg)){

			$model = new TargetEntry();
			$model->CO1_ID = $CO1_ID;
			$model->ta1_material_target_p = strval($avg['AVG(ta1_material_target_p)']);
			$model->ta1_material_target_total = strval($avg['AVG(ta1_material_target_total)']);
			$model->ta1_allied = strval($avg['AVG(ta1_allied)']);
			$model->ta1_allied_filler = strval($avg['AVG(ta1_allied_filler)']);
			$model->ta1_allied_paper = strval($avg['AVG(ta1_allied_paper)']);
			$model->ta1_allied_tape = strval($avg['AVG(ta1_allied_tape)']);
			$model->ta1_allied_fine_sand_paper = strval($avg['AVG(ta1_allied_fine_sand_paper)']);
			$model->ta1_allied_rough_sand_paper = strval($avg['AVG(ta1_allied_rough_sand_paper)']);
			$model->ta1_allied_detail = strval($avg['AVG(ta1_allied_detail)']);
			$model->ta1_allied_pps_cups = strval($avg['AVG(ta1_allied_pps_cups)']);
			$model->ta1_liquid = strval($avg['AVG(ta1_liquid)']);
			$model->ta1_liquid_hardener = strval($avg['AVG(ta1_liquid_hardener)']);
			$model->ta1_liquid_toner = strval($avg['AVG(ta1_liquid_toner)']);
			$model->ta1_liquid_sealer = strval($avg['AVG(ta1_liquid_sealer)']);
			$model->ta1_liquid_thinner = strval($avg['AVG(ta1_liquid_thinner)']);
			$model->ta1_liquid_solvent = strval($avg['AVG(ta1_liquid_solvent)']);
			$model->ta1_misc = strval($avg['AVG(ta1_misc)']);
			$model->ta1_misc_safety = strval($avg['AVG(ta1_misc_safety)']);
			$model->ta1_pm_sold_per_ro = strval($avg['AVG(ta1_pm_sold_per_ro)']);
			$model->ta1_pm_cost_per_ro = strval($avg['AVG(ta1_pm_cost_per_ro)']);
			$model->ta1_clear_per_ro = strval($avg['AVG(ta1_clear_per_ro)']);
			$model->ta1_toner_per_ro = strval($avg['AVG(ta1_toner_per_ro)']);
			$model->ta1_allied_per_ro = strval($avg['AVG(ta1_allied_per_ro)']);
			$model->ta1_misc_per_ro = strval($avg['AVG(ta1_misc_per_ro)']);
			$model->ta1_gp_percent = strval($avg['AVG(ta1_gp_percent)']);
			$model->ta1_paint_sold_percent_to_sales = strval($avg['AVG(ta1_paint_sold_percent_to_sales)']);
			$model->ta1_paint_and_matl_sold_percent = strval($avg['AVG(ta1_paint_and_matl_sold_percent)']);
			$model->ta1_material_cost_vs_sales = strval($avg['AVG(ta1_material_cost_vs_sales)']);
			$model->ta1_liquid_vs_sales = strval($avg['AVG(ta1_liquid_vs_sales)']);
			$model->ta1_material_vs_paint_labor = strval($avg['AVG(ta1_material_vs_paint_labor)']);
			$model->ta1_ref_labor_vs_sales = strval($avg['AVG(ta1_ref_labor_vs_sales)']);
			$model->ta1_paint_labor_vs_sales = strval($avg['AVG(ta1_paint_labor_vs_sales)']);
			$model->ta1_ref_and_paint_labor_vs_sales = strval($avg['AVG(ta1_ref_and_paint_labor_vs_sales)']);
			$model->ta1_allied_per_ro_p = strval($avg['AVG(ta1_allied_per_ro_p)']);
			$model->ta1_average_ro = strval($avg['AVG(ta1_average_ro)']);
			$model->ta1_clear_per_ro_p = strval($avg['AVG(ta1_clear_per_ro_p)']);
			$model->ta1_clear_vs_paint_labor_p = strval($avg['AVG(ta1_clear_vs_paint_labor_p)']);
			$model->ta1_cost_per_paint_hour_total = strval($avg['AVG(ta1_cost_per_paint_hour_total)']);
			$model->ta1_hdnr_vs_paint_labor_p = strval($avg['AVG(ta1_hdnr_vs_paint_labor_p)']);
			$model->ta1_pmr_vs_paint_labor_p = strval($avg['AVG(ta1_pmr_vs_paint_labor_p)']);
			$model->ta1_ppg_liq_vs_paint_and_ref_labor_p = strval($avg['AVG(ta1_ppg_liq_vs_paint_and_ref_labor_p)']);
			$model->ta1_ppg_liq_vs_sales_p = strval($avg['AVG(ta1_ppg_liq_vs_sales_p)']);
			$model->ta1_ppg_vs_all_grand_total_p = strval($avg['AVG(ta1_ppg_vs_all_grand_total_p)']);
			$model->ta1_ppg_vs_paint_labor_p = strval($avg['AVG(ta1_ppg_vs_paint_labor_p)']);
			$model->ta1_paint_labor_per_ro = strval($avg['AVG(ta1_paint_labor_per_ro)']);
			$model->ta1_tape_vs_paint_labor_p = strval($avg['AVG(ta1_tape_vs_paint_labor_p)']);
			$model->ta1_thinner_vs_paint_labor_p = strval($avg['AVG(ta1_thinner_vs_paint_labor_p)']);
			$model->ta1_toner_per_ro_p = strval($avg['AVG(ta1_toner_per_ro_p)']);
			$model->ta1_toner_vs_paint_labor_p = strval($avg['AVG(ta1_toner_vs_paint_labor_p)']);
			$model->ta1_cost_per_paint_hour_allied = strval($avg['AVG(ta1_cost_per_paint_hour_allied)']);
			$model->ta1_cost_per_paint_hour_liquid = strval($avg['AVG(ta1_cost_per_paint_hour_liquid)']);
			$model->ta1_paint_and_matl_sold_per_ro = strval($avg['AVG(ta1_paint_and_matl_sold_per_ro)']);
			$model->validate();
			$model->save(false);
			return $model;
		}
		return false;

	}

	//Targets
	public function getTargetEntries($shops){
		$location =  Location::find()
			->where(['IN', 'LO1_SHORT_CODE', explode(',', $shops)])
			->one();

		if(isset($location['CO1_ID'])){
			$targets =  TargetEntry::find()
				->where(['=', 'CO1_ID', $location['CO1_ID']])
				->one();
		}
		if(!isset($targets)){
			$targets = $this->generateTargets($location['CO1_ID']);
		}

		return $targets;
	}

	public function getTargetEntry($params) {
		$query = self::find()->alias('TA1')->select([
			'TA1.*'
		]);

		if(isset($params['CO1_ID'])) {
			$query->andFilterWhere(['=', 'CO1_ID', $params['CO1_ID']]);
		}

		return $query->asArray()->all();
	}

	/**
	 * Returns the target attribute for a passed attribute, typically a calculated attribute
	 * @param string $attribute RawSalesDataStoreMonth attribute
	 * @return string The TargetEntry target, else false if not found
	 */
	public function getTargetAttribute($attribute)
	{
		$array = array(
			'r03_c_gp_percent' => 'ta1_gp_percent',
			'r03_c_paint_sold_to_sales_p' => 'ta1_paint_sold_percent_to_sales',
			'r03_c_paint_and_materials_sold_p' => 'ta1_paint_and_matl_sold_percent',
			'r03_c_cost_per_paint_hour_total' => 'ta1_cost_per_paint_hour_total',
			'r03_c_cost_per_paint_hour_liquid' => 'ta1_cost_per_paint_hour_liquid',
			'r03_c_cost_per_paint_hour_allied' => 'ta1_cost_per_paint_hour_allied',
			'r03_c_ppg_vs_all_grand_total_p' => 'ta1_ppg_vs_all_grand_total_p',
			'r03_c_ppg_liq_vs_sales_p' => 'ta1_ppg_liq_vs_sales_p',
			'r03_c_materials_vs_paint_labor_p' => 'ta1_material_vs_paint_labor',
			'r03_c_ref_labor_vs_sales_p' => 'ta1_ref_labor_vs_sales',
			'r03_c_paint_labor_vs_sales_p' => 'ta1_paint_labor_vs_sales',
			'r03_c_ref_and_paint_labor_vs_sales_p' => 'ta1_ref_and_paint_labor_vs_sales',
			'r03_c_clear_vs_paint_labor_p' => 'ta1_clear_vs_paint_labor_p',
			'r03_c_hdnr_vs_paint_labor_p' => 'ta1_hdnr_vs_paint_labor_p',
			'r03_c_toner_vs_paint_labor_p' => 'ta1_toner_vs_paint_labor_p',
			'r03_c_pmr_vs_paint_labor_p' => 'ta1_pmr_vs_paint_labor_p',
			'r03_c_tape_vs_paint_labor_p' => 'ta1_tape_vs_paint_labor_p',
			'r03_c_thinner_vs_paint_labor_p' => 'ta1_thinner_vs_paint_labor_p',
			'r03_c_ppg_vs_paint_labor_p' => 'ta1_ppg_vs_paint_labor_p',
			'r03_c_ppg_liq_vs_paint_and_ref_labor_p' => 'ta1_ppg_liq_vs_paint_and_ref_labor_p',
			'r03_c_average_ro' => 'ta1_average_ro',
			'r03_c_paint_labor_per_ro' => 'ta1_paint_labor_per_ro',
			'r03_c_pm_cost_per_ro' => 'ta1_pm_cost_per_ro',
			'r03_c_clear_per_ro' => 'ta1_clear_per_ro',
			'r03_c_toner_per_ro' => 'ta1_toner_per_ro',
			'r03_c_allied_per_ro' => 'ta1_allied_per_ro',
			'r03_c_paint_and_materials_sold_per_ro' => 'ta1_paint_and_matl_sold_per_ro',
			'r03_c_misc_per_ro' => 'ta1_misc_per_ro',
			'r03_c_clear_per_ro_p' => 'ta1_clear_per_ro_p',
			'r03_c_toner_per_ro_p' => 'ta1_toner_per_ro_p',
			'r03_c_allied_per_ro_p' => 'ta1_allied_per_ro_p',
			'r03_c_materials_cost_vs_sales_p' => 'ta1_material_cost_vs_sales',
			'r03_po_spent_vs_materials_target' => 'ta1_material_target_total',
			'r03_c_paint_and_refinish_labor_total' => 'ta1_c_paint_and_refinish_labor_total',//TODO: may not exist
		);
		foreach ($array as $key => $value) {
			if ($key == $attribute) {
				return $value;
			}
		}
		return false;
	}
}
