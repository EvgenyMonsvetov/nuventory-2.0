<?php

namespace app\models;

use app\models\scopes\MasterDataLocationQuery;
use Yii;

/**
 * This is the model class for table "md2_location".
 *
 * @property int $MD2_ID
 * @property int $CO1_ID
 * @property int $MD1_ID
 * @property int $LO1_ID
 * @property int $MD2_ACTIVE
 * @property int $MD2_RACK
 * @property int $MD2_DRAWER
 * @property int $MD2_BIN
 * @property double $MD2_ON_HAND_QTY
 * @property double $MD2_AVAILABLE_QTY
 * @property double $MD2_MIN_QTY
 * @property double $MD2_MAX_QTY
 * @property double $MD2_REORDER_QTY
 * @property int $UM1_RECEIPT_ID
 * @property int $UM1_PURCHASE_ID
 * @property int $VD1_ID
 * @property int $MD2_DELETE_FLAG
 * @property int $MD2_CREATED_BY
 * @property string $MD2_CREATED_ON
 * @property int $MD2_MODIFIED_BY
 * @property string $MD2_MODIFIED_ON
 * @property int $MD2_PRINT_FLAG
 * @property int $MD2_NO_INVENTORY
 * @property int $MD2_PARTIAL
 */
class MasterDataLocation extends ActiveRecord
{
    protected $_tablePrefix = 'MD2';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'md2_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CO1_ID', 'MD1_ID', 'LO1_ID', 'MD2_ACTIVE', 'MD2_RACK', 'MD2_DRAWER', 'MD2_BIN', 'UM1_RECEIPT_ID', 'UM1_PURCHASE_ID', 'VD1_ID', 'MD2_DELETE_FLAG', 'MD2_CREATED_BY', 'MD2_MODIFIED_BY', 'MD2_PRINT_FLAG', 'MD2_NO_INVENTORY', 'MD2_PARTIAL'], 'integer'],
            [['MD2_ON_HAND_QTY', 'MD2_AVAILABLE_QTY', 'MD2_MIN_QTY', 'MD2_MAX_QTY', 'MD2_REORDER_QTY'], 'number'],
            [['MD2_CREATED_ON', 'MD2_MODIFIED_ON'], 'safe'],
            [['CO1_ID', 'MD1_ID', 'LO1_ID', 'MD2_DELETE_FLAG'], 'unique', 'targetAttribute' => ['CO1_ID', 'MD1_ID', 'LO1_ID', 'MD2_DELETE_FLAG']],
            [['LO1_ID', 'MD1_ID', 'MD2_RACK', 'MD2_DRAWER', 'MD2_BIN', 'MD2_DELETE_FLAG'], 'unique', 'targetAttribute' => ['LO1_ID', 'MD1_ID', 'MD2_RACK', 'MD2_DRAWER', 'MD2_BIN', 'MD2_DELETE_FLAG']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'MD2_ID' => 'Md2  ID',
            'CO1_ID' => 'Co1  ID',
            'MD1_ID' => 'Md1  ID',
            'LO1_ID' => 'Lo1  ID',
            'MD2_ACTIVE' => 'Md2  Active',
            'MD2_RACK' => 'Md2  Rack',
            'MD2_DRAWER' => 'Md2  Drawer',
            'MD2_BIN' => 'Md2  Bin',
            'MD2_ON_HAND_QTY' => 'Md2  On  Hand  Qty',
            'MD2_AVAILABLE_QTY' => 'Md2  Available  Qty',
            'MD2_MIN_QTY' => 'Md2  Min  Qty',
            'MD2_MAX_QTY' => 'Md2  Max  Qty',
            'MD2_REORDER_QTY' => 'Md2  Reorder  Qty',
            'UM1_RECEIPT_ID' => 'Um1  Receipt  ID',
            'UM1_PURCHASE_ID' => 'Um1  Purchase  ID',
            'VD1_ID' => 'Vd1  ID',
            'MD2_DELETE_FLAG' => 'Md2  Delete  Flag',
            'MD2_CREATED_BY' => 'Md2  Created  By',
            'MD2_CREATED_ON' => 'Md2  Created  On',
            'MD2_MODIFIED_BY' => 'Md2  Modified  By',
            'MD2_MODIFIED_ON' => 'Md2  Modified  On',
            'MD2_PRINT_FLAG' => 'Md2  Print  Flag',
            'MD2_NO_INVENTORY' => 'Md2  No  Inventory',
            'MD2_PARTIAL' => 'Md2  Partial',
        ];
    }

    /**
     * {@inheritdoc}
     * @return MasterDataLocationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MasterDataLocationQuery(get_called_class());
    }

    public function updateOrderLineStock( OrderLine $orderLine, $QTY )
    {
        $sql = "
            UPDATE or2_order_line
                LEFT JOIN or1_order_header ON or2_order_line.OR1_ID = or1_order_header.OR1_ID 
                LEFT JOIN md1_master_data as m ON m.MD1_ID=or2_order_line.MD1_ID 
                LEFT JOIN md2_location as md2_from ON md2_from.MD1_ID = or2_order_line.MD1_ID AND md2_from.LO1_ID = or1_order_header.LO1_FROM_ID 
                LEFT JOIN md2_location as md2_to ON md2_to.MD1_ID = or2_order_line.MD1_ID AND md2_to.LO1_ID = or1_order_header.LO1_TO_ID 
                LEFT JOIN um2_conversion AS um2 ON (um2.MD1_ID=or2_order_line.MD1_ID) AND (um2.UM1_TO_ID=md2_to.UM1_RECEIPT_ID) AND (um2.UM1_FROM_ID=md2_from.UM1_RECEIPT_ID) AND (um2.UM2_DELETE_FLAG=0) 
            SET 
                md2_to.MD2_ON_HAND_QTY   = coalesce(md2_to.MD2_ON_HAND_QTY, 0)   + if((md2_to.md2_no_inventory = 0), :qty, 0), 
                md2_to.MD2_AVAILABLE_QTY = coalesce(md2_to.MD2_AVAILABLE_QTY, 0) + if((md2_to.md2_no_inventory = 0), :qty, 0)
            WHERE (OR2_ID=:OR2_ID)";

        return Yii::$app->db->createCommand($sql, [
            ':qty'    => $QTY,
            ':OR2_ID' => $orderLine->OR2_ID
        ])->execute();
    }

    public function updatePurchaseOrderLineStock( PurchaseOrderLine $orderLine, $QTY )
    {
        $sql = "
            UPDATE po2_order_line
                LEFT JOIN po1_order_header ON po2_order_line.PO1_ID = po1_order_header.PO1_ID 
                LEFT JOIN md1_master_data m ON m.MD1_ID = po2_order_line.MD1_ID 
                LEFT JOIN md2_location ON md2_location.MD1_ID = po2_order_line.MD1_ID AND md2_location.LO1_ID = po1_order_header.LO1_ID 
            SET 
                md2_location.MD2_ON_HAND_QTY = IF(md2_location.md2_no_inventory<>0, md2_location.MD2_ON_HAND_QTY, coalesce(md2_location.MD2_ON_HAND_QTY, 0) + :qty),
                md2_location.MD2_AVAILABLE_QTY = IF(md2_location.md2_no_inventory<>0, md2_location.MD2_AVAILABLE_QTY, coalesce(md2_location.MD2_AVAILABLE_QTY, 0) + :qty)
            WHERE (PO2_ID=:PO2_ID)";

        return Yii::$app->db->createCommand($sql, [
            ':qty'    => $QTY,
            ':PO2_ID' => $orderLine->PO2_ID
        ])->execute();
    }

    public function updateStockWhenOrderIsComplete($OR1_ID)
    {
        $sql = "UPDATE or1_order_header o1 
                    left join or2_order_line o2 on o2.or1_id=o1.or1_id and o2.or2_delete_flag=0
                    left join md2_location md2 on md2.LO1_ID=o1.LO1_FROM_ID and md2.MD1_ID=o2.MD1_ID
                SET
                    md2.md2_on_hand_qty= if ( coalesce(md2.md2_no_inventory,0)=0, md2.md2_on_hand_qty - o2.or2_order_qty, md2.md2_on_hand_qty)
                WHERE
                    o1.or1_id = :or1_id and o1.or1_delete_flag=0;";

        $result = Yii::$app->db->createCommand($sql, [
            ':or1_id' => $OR1_ID
        ])->execute();

        return $result;
    }
}
