<?php

namespace app\models;

use app\models\scopes\RawSalesDataStoreMonthQuery;
use Yii;

/**
 * This is the model class for table 'r03_raw_sales_data_store_month'.
 *
 * The followings are the available columns in table 'r03_raw_sales_data_store_month':
 * @property string $id
 * @property string $r03_type
 * @property int $r03_manual_import
 * @property string $r03_created_on
 * @property string $r03_co1_id
 * @property string $r03_co1_name
 * @property string $r03_date
 * @property string $r03_month
 * @property string $r03_year
 * @property string $r03_shop_name
 * @property string $r03_shop_code
 * @property string $r03_work_days
 * @property string $r03_weeks
 * @property string $r03_ro_count
 * @property string $r03_part_revenue
 * @property string $r03_part_expense
 * @property string $r03_materials_target
 * @property string $r03_liquid_expense
 * @property string $r03_body_labor_hours
 * @property string $r03_body_labor_amount
 * @property string $r03_body_labor_cost
 * @property string $r03_refinish_labor_hours
 * @property string $r03_refinish_amount
 * @property string $r03_refinish_cost
 * @property string $r03_frame_hours
 * @property string $r03_frame_amount
 * @property string $r03_frame_cost
 * @property string $r03_mechanical_hours
 * @property string $r03_mechanical_amount
 * @property string $r03_mechanical_cost
 * @property string $r03_paint_materials_amount
 * @property string $r03_sublet_amount
 * @property string $r03_other_hours
 * @property string $r03_other_amount
 * @property string $r03_other_cost
 * @property string $r03_parts_total
 * @property string $r03_labor_total
 * @property string $r03_gross_amount
 * @property string $r03_net_amount
 * @property string $created_by
 * @property string $created_on
 * @property string $modified_by
 * @property string $modified_on
 * @property integer $delete_flag
 * @property integer $update_flag
 * @property string $r03_clear
 * @property string $r03_hardener
 * @property string $r03_paper
 * @property string $r03_primer_and_sealer
 * @property string $r03_fm_misc
 * @property string $r03_tape
 * @property string $r03_thinner
 * @property string $r03_toner
 * @property string $r03_fm_grand_total
 * @property string $r03_ppg_products
 * @property string $r03_misc
 * @property string $r03_all_materials_total
 * @property string $r03_paint_and_materials_profit
 * @property string $r03_c_gp_percent
 * @property string $r03_c_paint_sold_to_sales_p
 * @property string $r03_c_paint_and_materials_sold_p
 * @property string $r03_c_cost_per_paint_hour_total
 * @property string $r03_c_cost_per_paint_hour_liquid
 * @property string $r03_c_cost_per_paint_hour_allied
 * @property string $r03_c_ppg_vs_all_grand_total_p
 * @property string $r03_c_material_cost_vs_sales_p
 * @property string $r03_c_ppg_liq_vs_sales_p
 * @property string $r03_c_materials_vs_paint_labor_p
 * @property string $r03_c_ref_labor_vs_sales_p
 * @property string $r03_c_paint_labor_vs_sales_p
 * @property string $r03_c_ref_and_paint_labor_vs_sales_p
 * @property string $r03_c_clear_vs_paint_labor_p
 * @property string $r03_c_hdnr_vs_paint_labor_p
 * @property string $r03_c_toner_vs_paint_labor_p
 * @property string $r03_c_pmr_vs_paint_labor_p
 * @property string $r03_c_tape_vs_paint_labor_p
 * @property string $r03_c_thinner_vs_paint_labor_p
 * @property string $r03_c_ppg_vs_paint_labor_p
 * @property string $r03_c_ppg_liq_vs_paint_and_ref_labor_p
 * @property string $r03_c_average_ro
 * @property string $r03_c_paint_labor_per_ro
 * @property string $r03_c_pm_cost_per_ro
 * @property string $r03_c_clear_per_ro
 * @property string $r03_c_toner_per_ro
 * @property string $r03_c_allied_per_ro
 * @property string $r03_c_paint_and_materials_sold_per_ro
 * @property string $r03_c_misc_per_ro
 * @property string $r03_c_clear_per_ro_p
 * @property string $r03_c_toner_per_ro_p
 * @property string $r03_c_allied_per_ro_p
 * @property string $r03_c_materials_cost_vs_sales_p
 * @property string $r03_paint_hours
 * @property string $r03_paint_rate
 * @property string $r03_paint_amount
 * @property string $r03_body_materials_amount
 * @property string $r03_po_spent_vs_materials_target
 *
 * @property string $r03_solvent
 * @property string $r03_rough_sand
 * @property string $r03_fine_sand
 * @property string $r03_equipment
 * @property string $r03_mixing_supplies
 * @property string $r03_filler
 * @property string $r03_safety
 * @property string $r03_detail
 * @property string $r03_hardware
 * @property string $r03_parts_additional
 * @property string $material_target_p
 *
 * The followings are the available model relations:
 * @property Profile $cprofile
 * @property Profile $mprofile
 */
class RawSalesDataStoreMonth extends ActiveRecord
{
	protected $_tablePrefix = 'R03';

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'r03_raw_sales_data_store_month';
    }

	public static function primaryKey()
	{
		return ['id'];
	}

	/**
     * {@inheritdoc}
     * @return RawSalesDataStoreMonthQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RawSalesDataStoreMonthQuery(get_called_class());
    }

	/**
	 * @return \yii\db\Connection the database connection used by this AR class.
	 */
	public static function getDb()
	{
		return Yii::$app->get('dbdash');
	}

	public function behaviors()
	{
		return [];
	}

	public $r03_short_month;
	public $r03_short_year;
	public $r03_short_month_and_year;
	public $r03_c_paint_materials_total;
	public $r03_po_spent_vs_materials_target;
	public $r03_c_liquid_materials_total;
	public $r03_c_paint_and_refinish_labor_total;
	public $r03_c_paint_and_refinish_labor_hours;
	public $material_target_p;
	public $r03_materials_target;
	public $shop_name_month_and_year;

	public $cprofile_search;
	public $mprofile_search;
	public $full_date;
	public $month_and_year;

	const SHEET_REPORT_TYPE_AMOUNTS = 1;
	const SHEET_REPORT_TYPE_COMBO = 3;

	const PRIMARY = 1;
	const SECONDARY = 2;
	const CALCULATED = 3;

	const AUTOMATIC_IMPORT = 0;
	const MANUAL_IMPORT = 1;

	const REPORT_TYPE_PART_REVENUE = 1;
	const REPORT_TYPE_PART_EXPENSE = 2;
	const REPORT_TYPE_LIQUID_EXPENSE = 3;
	const REPORT_TYPE_BODY_LABOR_HOURS = 4;
	const REPORT_TYPE_BODY_LABOR_AMOUNT = 5;
	const REPORT_TYPE_BODY_LABOR_COST = 6;
	const REPORT_TYPE_REFINISH_LABOR_HOURS = 7;
	const REPORT_TYPE_REFINISH_AMOUNT = 8;
	const REPORT_TYPE_REFINISH_COST = 9;
	const REPORT_TYPE_FRAME_HOURS = 10;
	const REPORT_TYPE_FRAME_AMOUNT = 11;
	const REPORT_TYPE_FRAME_COST = 12;
	const REPORT_TYPE_MECHANICAL_HOURS = 13;
	const REPORT_TYPE_MECHANICAL_AMOUNT = 14;
	const REPORT_TYPE_MECHANICAL_COST = 15;
	const REPORT_TYPE_PAINT_MATERIALS_AMOUNT = 16;
	const REPORT_TYPE_SUBLET_AMOUNT = 17;
	const REPORT_TYPE_OTHER_HOURS = 18;
	const REPORT_TYPE_OTHER_AMOUNT = 19;
	const REPORT_TYPE_OTHER_COST = 20;
	const REPORT_TYPE_PARTS_TOTAL = 21;
	const REPORT_TYPE_LABOR_TOTAL = 22;
	const REPORT_TYPE_GROSS_AMOUNT = 23;
	const REPORT_TYPE_NET_AMOUNT = 24;
	const REPORT_TYPE_GROSS_PROFIT_PERCENT = 25;
	const REPORT_TYPE_MATERIALS_COST_VS_SALES_P = 26;
	const REPORT_TYPE_AVERAGE_RO = 27;
	const REPORT_TYPE_COST_PER_PAINT_HOUR_TOTAL = 28;
	const REPORT_TYPE_PAINT_MATERIALS_TOTAL = 29;
	const REPORT_TYPE_PO_VS_MATERIALS_TARGET = 30;

	//These are used for generating the data in the MySQL database
	const BY_MONTH = 1;
	const BY_YEAR = 2;
	const BY_RANGE = 3;

	const TOTAL = 1;
	const TARGET = 2;

	const HARDENER = 610;
	const SOLVENT = 614;
	const TONER = 611;
	const TAPE = 617;
	const PRIMER_SEALER = 612;
	const ROUGH_SAND = 619;
	const FINE_SAND = 618;
	const CLEAR = 609;
	const EQUIPMENT = 622;
	const HARDWARE = 623;
	const MISC = 624;
	const MIXING_SUPPLIES = 625;
	const PAPER = 616;
	const THINNER = 613;
	const FILLER = 615;
	const SAFETY = 621;
	const DETAIL = 620;


	/**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['MD1_ID'], 'required'],
            [['MD1_ID'], 'integer'],
            [['LG2_OLD_PRICE', 'LG2_NEW_PRICE'], 'number'],
            [['LG2_CREATED_ON'], 'safe'],
        ];
    }

	public function fields() {
		$fields = parent::fields();
		$fields["r03_short_month"] = function($model){ return $model->r03_short_month; };
		$fields["r03_short_year"] = function($model){ return $model->r03_short_year; };
		$fields["r03_short_month_and_year"] = function($model){ return $model->r03_short_month_and_year; };
		$fields["r03_c_paint_materials_total"] = function($model){ return $model->r03_c_paint_materials_total; };
		$fields["r03_po_spent_vs_materials_target"] = function($model){ return $model->r03_po_spent_vs_materials_target; };
		$fields["r03_c_liquid_materials_total"] = function($model){ return $model->r03_c_liquid_materials_total; };
		$fields["r03_c_paint_and_refinish_labor_total"] = function($model){ return $model->r03_c_paint_and_refinish_labor_total; };
		$fields["r03_c_paint_and_refinish_labor_hours"] = function($model){ return $model->r03_c_paint_and_refinish_labor_hours; };
		$fields["material_target_p"] = function($model){ return $model->material_target_p; };
		$fields["r03_materials_target"] = function($model){ return $model->r03_materials_target; };
		$fields["shop_name_month_and_year"] = function($model){ return $model->shop_name_month_and_year; };
		$fields["cprofile_search"] = function($model){ return $model->cprofile_search; };
		$fields["mprofile_search"] = function($model){ return $model->mprofile_search; };
		$fields["full_date"] = function($model){ return $model->full_date; };
		$fields["month_and_year"] = function($model){ return $model->month_and_year; };
		return $fields;
	}

    /**
     * {@inheritdoc}
     */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'r03_type' => 'Type',
			'r03_manual_import' => 'Manual Import',
			'r03_created_on' => 'Created On',
			'r03_co1_id' => 'Company ID',
			'r03_co1_name' => 'Company Name',
			'r03_date' => 'Date',
			'r03_month' => 'Month',
			'r03_year' => 'Year',
			'r03_shop_name' => 'Shop Name',
			'r03_shop_code' => 'Shop Code',
			'r03_clear' => 'Clear',
			'r03_hardener' => 'Hardener',
			'r03_paper' => 'Paper',
			'r03_primer_and_sealer' => 'Primer and Sealer',
			'r03_fm_misc' => 'FM Misc',
			'r03_tape' => 'Tape',
			'r03_thinner' => 'Thinner',
			'r03_toner' => 'Toner',
			'r03_fm_grand_total' => 'FM Grand Total',
			'r03_ppg_products' => 'Products',//MANF PRODUCTS
			'r03_misc' => 'Misc',
			'r03_c_paint_materials_total' => 'Paint & Refinish Materials Total',
			'r03_part_expense' => 'PO Total',
			'r03_materials_target' => 'Materials Target',
			'r03_paint_and_materials_profit' => 'Paint and Materials Profit',
			'r03_c_gp_percent' => 'Paint & Materials GP %',
			'r03_c_paint_sold_to_sales_p' => 'Paint Sold to Sales %',
			'r03_c_paint_and_materials_sold_p' => 'Paint and Materials Sold %',
			'r03_c_cost_per_paint_hour_total' => 'Cost per Paint Hour Total',
			'r03_c_cost_per_paint_hour_liquid' => 'Cost per Paint Hour Liquid',
			'r03_c_cost_per_paint_hour_allied' => 'Cost per Paint Hour Allied',
			'r03_c_ppg_vs_all_grand_total_p' => 'Liquid vs All Grand Total %',//MANF
			'r03_c_material_cost_vs_sales_p' => 'Materials Cost vs Sales %',
			'r03_c_ppg_liq_vs_sales_p' => 'Liquid vs Sales %',//MANF
			'r03_c_materials_vs_paint_labor_p' => 'Materials vs Paint & Ref Labor %',
			'r03_c_ref_labor_vs_sales_p' => 'Ref Labor vs Sales %',
			'r03_c_paint_labor_vs_sales_p' => 'Paint Labor vs Sales %',
			'r03_c_ref_and_paint_labor_vs_sales_p' => 'Ref and Paint Labor vs Sales %',
			'r03_c_clear_vs_paint_labor_p' => 'Clear vs Paint & Ref Labor %',
			'r03_c_hdnr_vs_paint_labor_p' => 'HDNR vs Paint & Ref Labor %',
			'r03_c_toner_vs_paint_labor_p' => 'Toner vs Paint & Ref Labor %',
			'r03_c_pmr_vs_paint_labor_p' => 'PMR vs Paint & Ref Labor %',
			'r03_c_tape_vs_paint_labor_p' => 'Tape vs Paint & Ref Labor %',
			'r03_c_thinner_vs_paint_labor_p' => 'Thinner vs Paint & Ref Labor %',
			'r03_c_ppg_vs_paint_labor_p' => 'Liquid vs Paint & Ref Labor %',//MANF
			'r03_c_ppg_liq_vs_paint_and_ref_labor_p' => 'Liquid vs Paint and Ref Labor %',//MANF
			'r03_c_average_ro' => 'Average RO',
			'r03_c_paint_labor_per_ro' => 'Paint Labor per RO',
			'r03_c_pm_cost_per_ro' => 'PM Cost per RO',
			'r03_c_clear_per_ro' => 'Clear per RO',
			'r03_c_toner_per_ro' => 'Toner per RO',
			'r03_c_allied_per_ro' => 'Allied per RO',
			'r03_c_paint_and_materials_sold_per_ro' => 'Paint and Materials Sold per RO',
			'r03_c_misc_per_ro' => 'Misc per RO',
			'r03_c_clear_per_ro_p' => 'Clear per RO %',
			'r03_c_toner_per_ro_p' => 'Toner per RO %',
			'r03_c_allied_per_ro_p' => 'Allied per RO %',
			'r03_c_materials_cost_vs_sales_p' => 'Materials Cost vs Sales %',
			'r03_c_liquid_materials_total' => 'Liquid Materials Total',
			'r03_c_paint_and_refinish_labor_total' => 'Paint and Refinish Labor',
			'r03_c_paint_and_refinish_labor_hours' => 'Paint and Refinish Hours',
			'r03_po_spent_vs_materials_target' => 'PO vs Materials Target %',


			'r03_solvent' => 'Solvent',
			'r03_rough_sand' => 'Rough Sandpaper',
			'r03_fine_sand' => 'Fine Sandpaper',
			'r03_equipment' => 'Equipment',
			'r03_mixing_supplies' => 'Mixing Supplies',
			'r03_filler' => 'Filler',
			'r03_safety' => 'Safety',
			'r03_detail' => 'Detail',
			'r03_hardware' => 'Hardware',


			//Changed

			'r03_gross_amount' => 'Sales',
			'r03_body_labor_cost' => 'Body Labor Cost',
			'r03_body_labor_amount' => 'Body Labor',
			'r03_refinish_amount' => 'Refinish Labor', //THIS ONE IS FOR LABOR
			'r03_refinish_cost' => 'Refinish Cost',
			'r03_paint_materials_amount' => 'Paint Sold',
			'r03_parts_total' => 'Parts Body Material Sold',
			'r03_refinish_labor_hours' => 'Refinish Labor Hours',
			'r03_ro_count' => 'RO Count',

			'r03_paint_hours' => 'Paint Labor Hours',
			'r03_paint_rate' => 'Paint Rate',
			'r03_paint_amount' => 'Paint Labor',
			'r03_body_materials_amount' => 'Body Materials',
			'r03_parts_additional' => 'Parts',

			'created_by' => 'Created By',
			'created_on' => 'Created On',
			'modified_by' => 'Modified By',
			'modified_on' => 'Modified On',
			'delete_flag' => 'Delete Flag',
			'update_flag' => 'Update Flag',
			'cprofile_search' => Yii::t('app', 'Created By'),
			'mprofile_search' => Yii::t('app', 'Modified By'),
		);
	}

	public function afterFind()
	{
		self::generateCalculatedValues();
		if (isset($this->r03_month, $this->r03_year) && strlen($this->r03_month) > 0 && strlen($this->r03_year) > 0) {
			$this->month_and_year = $this->r03_month . ' ' . $this->r03_year;
		}

		if (isset($this->r03_month, $this->r03_year, $this->r03_shop_name) && strlen($this->r03_month) > 0 && strlen($this->r03_year) > 0 && strlen($this->r03_shop_name) > 0) {
			$this->shop_name_month_and_year = $this->r03_shop_name . ' ' . $this->r03_month . ' ' . $this->r03_year;
			$this->r03_short_month = date('M', strtotime($this->r03_month));
			$this->r03_short_year = date('y', strtotime('01-01-' . $this->r03_year));
			$this->r03_short_month_and_year = $this->r03_short_month . ' ' . $this->r03_short_year;
		}
		return parent::afterFind();
	}

	public function beforeSave($insert)
	{
		self::replaceNotAvailableValues();
		return parent::beforeSave($insert);
	}

	/**
	 * Returns the value of the corresponding report and what measurement is used
	 * @param integer $report The value to return is based on this report
	 * @return string Containing the attribute and measurement for the report passed, else return null
	 */
	public static function getValuesForReport($report)
	{
		if ($report == self::REPORT_TYPE_PART_REVENUE) {
			$result['attribute'] = 'r03_part_revenue';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_PART_EXPENSE) {
			$result['attribute'] = 'r03_part_expense';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_LIQUID_EXPENSE) {
			$result['attribute'] = 'r03_liquid_expense';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_BODY_LABOR_HOURS) {
			$result['attribute'] = 'r03_body_labor_hours';
			$result['yax'] = 'Hours';
		} elseif ($report == self::REPORT_TYPE_BODY_LABOR_AMOUNT) {
			$result['attribute'] = 'r03_body_labor_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_BODY_LABOR_COST) {
			$result['attribute'] = 'r03_body_labor_cost';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_REFINISH_LABOR_HOURS) {
			$result['attribute'] = 'r03_refinish_labor_hours';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_REFINISH_AMOUNT) {
			$result['attribute'] = 'r03_refinish_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_REFINISH_COST) {
			$result['attribute'] = 'r03_refinish_cost';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_FRAME_HOURS) {
			$result['attribute'] = 'r03_frame_hours';
			$result['yax'] = 'Hours';
		} elseif ($report == self::REPORT_TYPE_FRAME_AMOUNT) {
			$result['attribute'] = 'r03_frame_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_FRAME_COST) {
			$result['attribute'] = 'r03_frame_cost';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_MECHANICAL_HOURS) {
			$result['attribute'] = 'r03_mechanical_hours';
			$result['yax'] = 'Hours';
		} elseif ($report == self::REPORT_TYPE_MECHANICAL_AMOUNT) {
			$result['attribute'] = 'r03_mechanical_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_MECHANICAL_COST) {
			$result['attribute'] = 'r03_mechanical_cost';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_PAINT_MATERIALS_AMOUNT) {
			$result['attribute'] = 'r03_paint_materials_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_SUBLET_AMOUNT) {
			$result['attribute'] = 'r03_sublet_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_OTHER_HOURS) {
			$result['attribute'] = 'r03_other_hours';
			$result['yax'] = 'Hours';
		} elseif ($report == self::REPORT_TYPE_OTHER_AMOUNT) {
			$result['attribute'] = 'r03_other_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_OTHER_COST) {
			$result['attribute'] = 'r03_other_cost';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_PARTS_TOTAL) {
			$result['attribute'] = 'r03_parts_total';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_LABOR_TOTAL) {
			$result['attribute'] = 'r03_labor_total';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_GROSS_AMOUNT) {
			$result['attribute'] = 'r03_gross_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_NET_AMOUNT) {
			$result['attribute'] = 'r03_net_amount';
			$result['yax'] = 'Dollars';
		} elseif ($report == self::REPORT_TYPE_GROSS_PROFIT_PERCENT) {
			$result['attribute'] = 'r03_c_gp_percent';
			$result['yax'] = '%';
		} elseif ($report == self::REPORT_TYPE_MATERIALS_COST_VS_SALES_P) {
			$result['attribute'] = 'r03_c_materials_cost_vs_sales_p';
			$result['yax'] = '%';
		} elseif ($report == self::REPORT_TYPE_PO_VS_MATERIALS_TARGET) {
			$result['attribute'] = 'r03_po_spent_vs_materials_target';
			$resutlt['yax'] = '%';
		} elseif ($report == self::REPORT_TYPE_AVERAGE_RO) {
			$result['attribute'] = 'r03_c_average_ro';
			$result['yax'] = '';
		} elseif ($report == self::REPORT_TYPE_COST_PER_PAINT_HOUR_TOTAL) {
			$result['attribute'] = 'r03_c_cost_per_paint_hour_total';
			$result['yax'] = '$';
		} elseif ($report == self::REPORT_TYPE_PAINT_MATERIALS_TOTAL) {
			$result['attribute'] = 'r03_c_paint_materials_total';
			$result['yax'] = '$';
		} else {
			$result = null;
		}
		if ($result != null) {
			$result['title'] = (new RawSalesDataStoreMonth)->getAttributeLabel($result['attribute']);
		}
		return $result;
	}

	public function generateCalculatedValues()
	{
		//define('MATERIAL_TARGET_P', 0.07);
		define('MATERIAL_TARGET_P',RawSalesDataStoreMonth::getMaterialTargetP($this->r03_co1_id));

//       if(isset($this->r03_fm_grand_total, $this->r03_misc)){
//          $this->r03_all_materials_total = $this->r03_fm_grand_total + $this->r03_misc;
//        }else{
//            $this->r03_all_materials_total = "Not Available";
//        }
		error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);


		if (!isset($this->r03_paint_rate)) {
			$this->r03_paint_rate = rand(24, 29);
		}
//        if(!isset($this->r03_paint_hours)){
//            $this->r03_paint_hours = rand(7000,8000);
//        }

		//Paint & Refinish Materials Total
		$this->r03_c_paint_materials_total = $this->r03_hardener +
			$this->r03_solvent + $this->r03_toner + $this->r03_tape +
			$this->r03_primer_and_sealer + $this->r03_rough_sand +
			$this->r03_fine_sand + $this->r03_clear + $this->r03_misc +
			$this->r03_mixing_supplies + $this->r03_paper + $this->r03_thinner +
			$this->r03_filler + $this->r03_safety + $this->r03_detail;

		if (isset($this->r03_clear)) {
			$this->r03_c_liquid_materials_total = $this->r03_hardener + $this->r03_toner + $this->r03_clear + $this->r03_primer_and_sealer + $this->r03_solvent + $this->r03_thinner;
		} else {
			$this->r03_c_liquid_materials_total = "Not Available";
		}

		// "Right now it’s coming from GROSS_AMOUNT but that includes tax and needs to be removed. NET_AMOUNT has the right value for that."
//        if(isset($this->r03_gross_amount, $this->r03_parts_additional) && $this->r03_parts_additional != 0){
//            $this->r03_gross_amount = $this->r03_gross_amount + $this->r03_parts_additional;
//        }
		if (isset($this->r03_net_amount, $this->r03_parts_additional) && $this->r03_parts_additional != 0) {
			$this->r03_net_amount = $this->r03_net_amount + $this->r03_parts_additional;
		}

//        if(isset($this->r03_refinish_labor_hours, $this->r03_paint_hours)){
//            $this->r03_c_paint_and_refinish_labor_hours = ($this->r03_paint_hours + $this->r03_refinish_labor_hours);
//        }else{
//            $this->r03_c_paint_and_refinish_labor_hours = "Not Available";
//        }


		// Paint and Refinish Hours
		if (isset($this->r03_paint_hours)) {
			$this->r03_c_paint_and_refinish_labor_hours = $this->r03_paint_hours;
		} elseif (isset($this->r03_refinish_labor_hours)) {
			$this->r03_c_paint_and_refinish_labor_hours = $this->r03_refinish_labor_hours;
			$this->r03_paint_hours = $this->r03_refinish_labor_hours;
		} else {
			$this->r03_c_paint_and_refinish_labor_hours = "Not Available";
		}

//        if(isset($this->r03_c_paint_and_refinish_labor_total)){
		if (isset($this->r03_paint_rate, $this->r03_c_paint_and_refinish_labor_hours)) {
			$this->r03_c_paint_and_refinish_labor_total = ($this->r03_c_paint_and_refinish_labor_hours * $this->r03_paint_rate);
		} else {
			$this->r03_c_paint_and_refinish_labor_total = "Not Available";
		}
//        }

		//r03_paint_materials_amount = Paint Sold
		if (isset($this->r03_paint_materials_amount, $this->r03_body_materials_amount, $this->r03_all_materials_total)) {
			$this->r03_paint_and_materials_profit = ($this->r03_paint_materials_amount + $this->r03_body_materials_amount) - $this->r03_c_paint_materials_total;
		} else {
			$this->r03_paint_and_materials_profit = "Not Available";
		}

		if (isset($this->r03_gross_amount, $this->r03_part_expense) && $this->r03_gross_amount != 0) {
			$this->r03_materials_target = $this->r03_gross_amount * MATERIAL_TARGET_P;
			$this->r03_po_spent_vs_materials_target = ($this->r03_part_expense / $this->r03_materials_target) * 100;
		} else {
			$this->r03_po_spent_vs_materials_target = "Not Available";
		}


		if (isset($this->r03_paint_materials_amount, $this->r03_gross_amount) && $this->r03_gross_amount != 0) {
			$this->r03_c_paint_sold_to_sales_p = ($this->r03_paint_materials_amount / $this->r03_gross_amount) * 100;
		} else {
			$this->r03_c_paint_sold_to_sales_p = "Not Available";
		}

		if (isset($this->r03_paint_materials_amount, $this->r03_body_materials_amount, $this->r03_gross_amount) && $this->r03_gross_amount != 0) {
			$this->r03_c_paint_and_materials_sold_p = (($this->r03_paint_materials_amount + $this->r03_body_materials_amount) / $this->r03_gross_amount) * 100;
		} else {
			$this->r03_c_paint_and_materials_sold_p = "Not Available";
		}
		//Cost per Paint Hour Total
		if (isset($this->r03_c_paint_materials_total, $this->r03_paint_hours) && $this->r03_paint_hours != 0) {
			$this->r03_c_cost_per_paint_hour_total = $this->r03_c_paint_materials_total / $this->r03_paint_hours;
		} else {
			$this->r03_c_cost_per_paint_hour_total = !isset($this->r03_paint_hours) ? "Paint Hours Not Available" : "Not Available";;
		}
		//Cost per Paint Hour Liquid
		if (isset($this->r03_c_liquid_materials_total, $this->r03_paint_hours) && $this->r03_paint_hours != 0) {
			$this->r03_c_cost_per_paint_hour_liquid = $this->r03_c_liquid_materials_total / $this->r03_paint_hours;
		} else {
			$this->r03_c_cost_per_paint_hour_liquid = !isset($this->r03_paint_hours) ? "Paint Hours Not Available" : "Not Available";;
		}

		// Cost Per Paint Hour Allied
		if (isset($this->r03_c_paint_materials_total, $this->r03_c_liquid_materials_total, $this->r03_paint_hours) && $this->r03_paint_hours != 0) {
			$this->r03_c_cost_per_paint_hour_allied = ($this->r03_c_paint_materials_total - $this->r03_c_liquid_materials_total) / $this->r03_paint_hours;
		} else {
			$this->r03_c_cost_per_paint_hour_allied = !isset($this->r03_paint_hours) ? "Paint Hours Not Available" : "Not Available";
		}

		if (isset($this->r03_c_liquid_materials_total, $this->r03_c_paint_materials_total) && $this->r03_c_paint_materials_total != 0) {
			$this->r03_c_ppg_vs_all_grand_total_p = ($this->r03_c_liquid_materials_total / $this->r03_c_paint_materials_total) * 100;
		} else {
			$this->r03_c_ppg_vs_all_grand_total_p = "Not Available";
		}

		if (isset($this->r03_c_paint_materials_total, $this->r03_gross_amount) && $this->r03_gross_amount != 0) {
			$this->r03_c_material_cost_vs_sales_p = ($this->r03_c_paint_materials_total / $this->r03_gross_amount) * 100;
		} else {
			$this->r03_c_material_cost_vs_sales_p = "Not Available";
		}

		if (isset($this->r03_c_liquid_materials_total, $this->r03_gross_amount) && $this->r03_gross_amount != 0) {
			$this->r03_c_ppg_liq_vs_sales_p = ($this->r03_c_liquid_materials_total / $this->r03_gross_amount) * 100;
		} else {
			$this->r03_c_ppg_liq_vs_sales_p = "Not Available";
		}

		// Materials vs Paint & Ref Labor %
		// OLD
//        if(isset($this->r03_c_paint_materials_total, $this->r03_c_paint_and_refinish_labor_total) && $this->r03_c_paint_and_refinish_labor_total != 0){
//            $this->r03_c_materials_vs_paint_labor_p = ($this->r03_c_paint_materials_total / $this->r03_c_paint_and_refinish_labor_total)*100;
//        }else{
//            $this->r03_c_materials_vs_paint_labor_p = "Not Available";
//        }
		if (isset($this->r03_c_paint_materials_total, $this->r03_c_paint_and_refinish_labor_total) && $this->r03_c_paint_and_refinish_labor_total != 0) {
			$this->r03_c_materials_vs_paint_labor_p = ($this->r03_c_paint_materials_total / ($this->r03_c_paint_and_refinish_labor_total + $this->r03_refinish_amount)) * 100;
		} else {
			$this->r03_c_materials_vs_paint_labor_p = "Not Available";
		}

		// Ref Labor vs Sales %
		if (isset($this->r03_refinish_amount, $this->r03_gross_amount) && $this->r03_gross_amount != 0) {
			$this->r03_c_ref_labor_vs_sales_p = ($this->r03_refinish_amount / $this->r03_gross_amount) * 100;
		} else {
			$this->r03_c_ref_labor_vs_sales_p = "Not Available";
		}

		if ($this->r03_c_ref_labor_vs_sales_p > 100) {
			$l = 1;
		}

		// Paint Labor vs Sales %
		if (isset($this->r03_c_paint_and_refinish_labor_total, $this->r03_gross_amount) && $this->r03_gross_amount != 0) {
			$this->r03_c_paint_labor_vs_sales_p = ($this->r03_c_paint_and_refinish_labor_total / $this->r03_gross_amount) * 100;
		} else {
			$this->r03_c_paint_labor_vs_sales_p = "Not Available";
		}

		if (isset($this->r03_c_paint_and_refinish_labor_total, $this->r03_refinish_amount)) {
			$this->r03_c_paint_and_refinish_labor_total = $this->r03_c_paint_and_refinish_labor_total + $this->r03_refinish_amount;
		} else {
			$this->r03_c_paint_and_refinish_labor_total = "Not Available";
		}

		// Ref and Paint Labor vs Sales %
		// r03_refinish_amount
		// r03_c_paint_and_refinish_labor_total
		if (isset($this->r03_refinish_amount, $this->r03_c_paint_and_refinish_labor_total, $this->r03_gross_amount) && $this->r03_gross_amount != 0) {
			$this->r03_c_ref_and_paint_labor_vs_sales_p = (($this->r03_refinish_amount + $this->r03_c_paint_and_refinish_labor_total) / $this->r03_gross_amount) * 100;
		} else {
			$this->r03_c_ref_and_paint_labor_vs_sales_p = "Not Available";
		}

		//Clear vs Paint Labor %
		if (isset($this->r03_clear, $this->r03_c_paint_and_refinish_labor_total) && $this->r03_c_paint_and_refinish_labor_total != 0) {
			$this->r03_c_clear_vs_paint_labor_p = ($this->r03_clear / $this->r03_c_paint_and_refinish_labor_total) * 100;
		} else {
			$this->r03_c_clear_vs_paint_labor_p = "Not Available";
		}

		if (isset($this->r03_hardener, $this->r03_c_paint_and_refinish_labor_total)) {
			if ($this->r03_c_paint_and_refinish_labor_total != 0) {
				$this->r03_c_hdnr_vs_paint_labor_p = ($this->r03_hardener / $this->r03_c_paint_and_refinish_labor_total) * 100;
			} else {
				$this->r03_c_hdnr_vs_paint_labor_p = 0;
			}
		} else {
//            $this->r03_c_hdnr_vs_paint_labor_p = "Not Available";
			$this->r03_c_hdnr_vs_paint_labor_p = 0;
		}

		if (isset($this->r03_toner, $this->r03_c_paint_and_refinish_labor_total) && $this->r03_c_paint_and_refinish_labor_total != 0) {
			$this->r03_c_toner_vs_paint_labor_p = ($this->r03_toner / $this->r03_c_paint_and_refinish_labor_total) * 100;
		} else {
			$this->r03_c_toner_vs_paint_labor_p = "Not Available";
		}

		if (isset($this->r03_primer_and_sealer, $this->r03_c_paint_and_refinish_labor_total) && $this->r03_c_paint_and_refinish_labor_total != 0) {
			$this->r03_c_pmr_vs_paint_labor_p = ($this->r03_primer_and_sealer / $this->r03_c_paint_and_refinish_labor_total) * 100;
		} else {
			$this->r03_c_pmr_vs_paint_labor_p = "Not Available";
		}

		if (isset($this->r03_tape, $this->r03_c_paint_and_refinish_labor_total) && $this->r03_c_paint_and_refinish_labor_total != 0) {
			$this->r03_c_tape_vs_paint_labor_p = ($this->r03_tape / $this->r03_c_paint_and_refinish_labor_total) * 100;
		} else {
			$this->r03_c_tape_vs_paint_labor_p = "Not Available";
		}

		if (isset($this->r03_thinner, $this->r03_c_paint_and_refinish_labor_total) && $this->r03_c_paint_and_refinish_labor_total != 0) {
			$this->r03_c_thinner_vs_paint_labor_p = ($this->r03_thinner / $this->r03_c_paint_and_refinish_labor_total) * 100;
		} else {
			$this->r03_c_thinner_vs_paint_labor_p = "Not Available";
		}

		if (isset($this->r03_c_liquid_materials_total, $this->r03_c_paint_and_refinish_labor_total) && $this->r03_c_paint_and_refinish_labor_total != 0) {
			$this->r03_c_ppg_vs_paint_labor_p = ($this->r03_c_liquid_materials_total / $this->r03_c_paint_and_refinish_labor_total) * 100;
		} else {
			$this->r03_c_ppg_vs_paint_labor_p = "Not Available";
		}

		if (isset($this->r03_c_liquid_materials_total, $this->r03_refinish_amount, $this->r03_c_paint_and_refinish_labor_total) && ($this->r03_refinish_amount + $this->r03_c_paint_and_refinish_labor_total) != 0) {
			$this->r03_c_ppg_liq_vs_paint_and_ref_labor_p = ($this->r03_c_liquid_materials_total) / ($this->r03_refinish_amount + $this->r03_c_paint_and_refinish_labor_total) * 100;
		} else {
			$this->r03_c_ppg_liq_vs_paint_and_ref_labor_p = "Not Available";
		}

		if (isset($this->r03_gross_amount, $this->r03_ro_count) && $this->r03_ro_count != 0) {
			$this->r03_c_average_ro = $this->r03_gross_amount / $this->r03_ro_count;
		} else {
			$this->r03_c_average_ro = "Not Available";
		}

		if (isset($this->r03_c_paint_and_refinish_labor_total, $this->r03_ro_count) && $this->r03_ro_count != 0) {
			$this->r03_c_paint_labor_per_ro = $this->r03_c_paint_and_refinish_labor_total / $this->r03_ro_count;
		} else {
			$this->r03_c_paint_labor_per_ro = "Not Available";
		}

		if (isset($this->r03_c_paint_materials_total, $this->r03_ro_count) && $this->r03_ro_count != 0) {
			$this->r03_c_pm_cost_per_ro = $this->r03_c_paint_materials_total / $this->r03_ro_count;
		} else {
			$this->r03_c_pm_cost_per_ro = "Not Available";
		}

		if (isset($this->r03_clear, $this->r03_ro_count) && $this->r03_ro_count != 0) {
			$this->r03_c_clear_per_ro = $this->r03_clear / $this->r03_ro_count;
		} else {
			$this->r03_c_clear_per_ro = "Not Available";
		}


		if (isset($this->r03_toner, $this->r03_ro_count) && $this->r03_ro_count != 0) {
			$this->r03_c_toner_per_ro = $this->r03_toner / $this->r03_ro_count;
		} else {
			$this->r03_c_toner_per_ro = "Not Available";
		}


		if (isset($this->r03_c_paint_materials_total, $this->r03_c_liquid_materials_total, $this->r03_ro_count) && $this->r03_ro_count != 0) {
			$this->r03_c_allied_per_ro = ($this->r03_c_paint_materials_total - $this->r03_c_liquid_materials_total) / $this->r03_ro_count;
		} else {
			$this->r03_c_allied_per_ro = "Not Available";
		}

		// Paint and Materials sold per RO
//        if(isset($this->r03_c_paint_materials_total, $this->r03_ro_count) && $this->r03_ro_count != 0){
//            $this->r03_c_paint_and_materials_sold_per_ro = $this->r03_c_paint_materials_total / $this->r03_ro_count;
		if (isset($this->r03_paint_materials_amount, $this->r03_ro_count) && $this->r03_ro_count != 0) {
			$this->r03_c_paint_and_materials_sold_per_ro = $this->r03_paint_materials_amount / $this->r03_ro_count;
		} else {
			$this->r03_c_paint_and_materials_sold_per_ro = "Not Available";
		}

		if (isset($this->r03_misc, $this->r03_ro_count) && $this->r03_ro_count != 0) {
			$this->r03_c_misc_per_ro = ($this->r03_misc) / $this->r03_ro_count;
		} else {
			$this->r03_c_misc_per_ro = "Not Available";
		}

		if (isset($this->r03_c_clear_per_ro, $this->r03_c_average_ro) && $this->r03_c_average_ro != 0) {
			$this->r03_c_clear_per_ro_p = ($this->r03_c_clear_per_ro / $this->r03_c_average_ro) * 100;
		} else {
			$this->r03_c_clear_per_ro_p = "Not Available";
		}

		if (isset($this->r03_c_toner_per_ro, $this->r03_c_average_ro) && $this->r03_c_average_ro != 0) {
			$this->r03_c_toner_per_ro_p = ($this->r03_c_toner_per_ro / $this->r03_c_average_ro) * 100;
		} else {
			$this->r03_c_toner_per_ro_p = "Not Available";
		}

		if (isset($this->r03_c_allied_per_ro, $this->r03_c_average_ro) && $this->r03_c_average_ro != 0) {
			$this->r03_c_allied_per_ro_p = ($this->r03_c_allied_per_ro / $this->r03_c_average_ro) * 100;
		} else {
			$this->r03_c_allied_per_ro_p = "Not Available";
		}


		if (isset($this->r03_c_pm_cost_per_ro, $this->r03_c_average_ro) && $this->r03_c_average_ro != 0) {
			$this->r03_c_materials_cost_vs_sales_p = ($this->r03_c_pm_cost_per_ro / $this->r03_c_average_ro) * 100;
		} else {
			$this->r03_c_materials_cost_vs_sales_p = "Not Available";
		}

		//GP Percent
		if (isset($this->r03_c_paint_materials_total, $this->r03_paint_materials_amount) && $this->r03_paint_materials_amount != 0) {
			$this->r03_c_gp_percent = (($this->r03_paint_materials_amount - $this->r03_c_paint_materials_total) / $this->r03_paint_materials_amount) * 100;
		} else {
			$this->r03_c_gp_percent = "Not Available";
		}

		if ($this->r03_c_gp_percent < 0) {
			$this->r03_c_gp_percent = 0;
		}

	}

	public function replaceNotAvailableValues() {
		foreach ($this as $key => $value) {
			if ($value == "Not Available") {
				$this->$key = 0;
			}
		}
	}

	public static function getMaterialTargetP($co1_id = null, $shops = null) {
		$targetModel = new TargetEntry();
		if(isset($co1_id)) {
			$shopTargets = $targetModel->generateTargets($co1_id);
		} elseif (isset($shops)) {
			$shopTargets = $targetModel->getTargetEntries($shops);
		} else {
			return 0.07;
		}
		if(isset($shopTargets->ta1_material_target_p)) {
			$material_target = $shopTargets->ta1_material_target_p/100;
		} else {
			$material_target = 0.07;
		}
		return $material_target;
	}

	public function getQuery($params)
	{
		$query = self::find()->alias('r03')
			->select([ 'r03.*']);

		$query->initScope();

		if (isset($params['LO1_SHORT_CODEs'])) {
			$query->andFilterWhere(['IN', 'r03.r03_shop_code', explode(',', $params['LO1_SHORT_CODEs'])]);
		}
		if (isset($params['startDate'])) {
			$query->andFilterWhere(['>=', 'DATE(r03.r03_date)', $params['startDate']]);
		}
		if (isset($params['endDate'])) {
			$query->andFilterWhere(['<=', 'DATE(r03.r03_date)', $params['endDate']]);
		}
		return $query;
	}

	public static function generateReportsByRangeByShop($params)
	{
//		Write a query that groups all the shops together and provides the sum of each column
		$query = self::find()->alias('r03')
			->select(['r03.r03_shop_code', 'r03.r03_shop_name', 'r03.r03_co1_id', 'r03.r03_co1_name',
				'SUM(r03_work_days) AS r03_work_days', 'SUM(r03_weeks) AS r03_weeks', 'SUM(r03_ro_count) AS r03_ro_count', 'AVG(r03_ro_count) AS r03_ro_average',
				'SUM(r03_part_expense) AS r03_part_expense', 'SUM(r03_part_revenue) AS r03_part_revenue', 'SUM(r03_liquid_expense) AS r03_liquid_expense',
				'SUM(r03_body_labor_hours) AS r03_body_labor_hours', 'SUM(r03_body_labor_amount) AS r03_body_labor_amount',
				'SUM(r03_body_labor_cost) AS r03_body_labor_cost', 'SUM(r03_refinish_labor_hours) AS r03_refinish_labor_hours',
				'SUM(r03_refinish_amount) AS r03_refinish_amount', 'SUM(r03_refinish_cost) AS r03_refinish_cost', 'SUM(r03_frame_hours) AS r03_frame_hours',
				'SUM(r03_frame_amount) AS r03_frame_amount', 'SUM(r03_frame_cost) AS r03_frame_cost', 'SUM(r03_mechanical_hours) AS r03_mechanical_hours',
				'SUM(r03_mechanical_amount) AS r03_mechanical_amount', 'SUM(r03_mechanical_cost) AS r03_mechanical_cost',
				'SUM(r03_paint_materials_amount) AS r03_paint_materials_amount', 'SUM(r03_sublet_amount) AS r03_sublet_amount',
				'SUM(r03_other_hours) AS r03_other_hours', 'SUM(r03_other_amount) AS r03_other_amount', 'SUM(r03_other_cost) AS r03_other_cost',
				'SUM(r03_parts_total) AS r03_parts_total', 'SUM(r03_labor_total) AS r03_labor_total', 'SUM(r03_gross_amount) AS r03_gross_amount',
				'SUM(r03_net_amount) AS r03_net_amount', 'SUM(r03_paint_hours) AS r03_paint_hours', 'SUM(r03_body_materials_amount) AS r03_body_materials_amount',
				'SUM(r03_paint_amount) AS r03_paint_amount', 'SUM(r03_clear) AS r03_clear', 'SUM(r03_hardener) AS r03_hardener',
				'SUM(r03_paper) AS r03_paper', 'SUM(r03_primer_and_sealer) AS r03_primer_and_sealer', 'SUM(r03_fm_misc) AS r03_fm_misc',
				'SUM(r03_tape) AS r03_tape', 'SUM(r03_thinner) AS r03_thinner', 'SUM(r03_toner) AS r03_toner', 'SUM(r03_fm_grand_total) AS r03_fm_grand_total',
				'SUM(r03_ppg_products) AS r03_ppg_products', 'SUM(r03_misc) AS r03_misc', 'SUM(r03_all_materials_total) AS r03_all_materials_total',
				'SUM(r03_paint_and_materials_profit) AS r03_paint_and_materials_profit', 'SUM(r03_solvent) AS r03_solvent',
				'SUM(r03_rough_sand) AS r03_rough_sand', 'SUM(r03_fine_sand) AS r03_fine_sand', 'SUM(r03_equipment) AS r03_equipment',
				'SUM(r03_mixing_supplies) AS r03_mixing_supplies', 'SUM(r03_filler) AS r03_filler', 'SUM(r03_safety) AS r03_safety',
				'SUM(r03_detail) AS r03_detail', 'SUM(r03_hardware) AS r03_hardware']);
		$query->groupBy(['r03.r03_shop_code', 'r03.r03_shop_name', 'r03.r03_co1_id', 'r03.r03_co1_name']);

		if (isset($params['LO1_SHORT_CODEs'])) {
			$query->andFilterWhere(['IN', 'r03.r03_shop_code', explode(',', $params['LO1_SHORT_CODEs'])]);
		}
		if (isset($params['startDate'])) {
			$query->andFilterWhere(['>=', 'DATE(r03.r03_date)', $params['startDate']]);
		}
		if (isset($params['endDate'])) {
			$query->andFilterWhere(['<=', 'DATE(r03.r03_date)', $params['endDate']]);
		}

		return $query->all();
	}

	public function getAll($params) {
		$query = $this->getQuery($params);
		return $query->all();
	}
}
