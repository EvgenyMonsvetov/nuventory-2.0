<?php

namespace app\models;

use app\models\scopes\InventoryQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "lg1_inventory".
 *
 * @property int $LG1_ID
 * @property int $CO1_ID
 * @property int $LO1_ID
 * @property int $TE1_ID
 * @property int $MD0_ID
 * @property int $MD1_ID
 * @property int $MD2_ID
 * @property int $JT2_ID
 * @property int $OR2_ID
 * @property int $PO2_ID
 * @property string $MD1_PART_NUMBER
 * @property string $MD1_DESC1
 * @property string $LG1_CREATED_ON
 * @property int $LG1_CREATED_BY
 * @property double $LG1_UNIT_COST
 * @property double $LG1_EXTENDED_COST
 * @property double $LG1_OLD_QTY
 * @property double $LG1_NEW_QTY
 * @property double $LG1_ADJ_QTY
 * @property int $LG1_DELETE_FLAG
 * @property string $LG1_DESC
 * @property int $LG1_TYPE
 */
class Inventory extends ActiveRecord
{
    protected $_tablePrefix = 'LG1';

	public $LO1_NAME;
	public $CREATED_BY;
	public $TE1_NAME;
	public $LTYPE;
	public $NUMBER;
	public $LINE;
	public $LG1_MODIFIED_ON;
	public $LG1_MODIFIED_BY;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lg1_inventory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CO1_ID', 'LO1_ID', 'TE1_ID', 'MD0_ID', 'MD1_ID', 'MD2_ID', 'JT2_ID', 'OR2_ID', 'PO2_ID', 'LG1_CREATED_BY', 'LG1_MODIFIED_BY', 'LG1_DELETE_FLAG', 'LG1_TYPE'], 'integer'],
            [['LG1_CREATED_ON', 'LG1_MODIFIED_ON'], 'safe'],
            [['LG1_UNIT_COST', 'LG1_EXTENDED_COST', 'LG1_OLD_QTY', 'LG1_NEW_QTY', 'LG1_ADJ_QTY'], 'number'],
            [['MD1_PART_NUMBER', 'MD1_DESC1', 'LG1_DESC'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'LG1_ID' => 'Lg1  ID',
            'CO1_ID' => 'Co1  ID',
            'LO1_ID' => 'Lo1  ID',
            'TE1_ID' => 'Te1  ID',
            'MD0_ID' => 'Md0  ID',
            'MD1_ID' => 'Md1  ID',
            'MD2_ID' => 'Md2  ID',
            'JT2_ID' => 'Jt2  ID',
            'OR2_ID' => 'Or2  ID',
            'PO2_ID' => 'Po2  ID',
            'MD1_PART_NUMBER' => 'Md1  Part  Number',
            'MD1_DESC1' => 'Md1  Desc1',
            'LG1_CREATED_ON' => 'Lg1  Created  On',
            'LG1_CREATED_BY' => 'Lg1  Created  By',
            'LG1_UNIT_COST' => 'Lg1  Unit  Cost',
            'LG1_EXTENDED_COST' => 'Lg1  Extended  Cost',
            'LG1_OLD_QTY' => 'Lg1  Old  Qty',
            'LG1_NEW_QTY' => 'Lg1  New  Qty',
            'LG1_ADJ_QTY' => 'Lg1  Adj  Qty',
            'LG1_DELETE_FLAG' => 'Lg1  Delete  Flag',
            'LG1_DESC' => 'Lg1  Desc',
            'LG1_TYPE' => 'Lg1  Type',
        ];
    }

    /**
     * {@inheritdoc}
     * @return Lg1InventoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InventoryQuery(get_called_class());
    }

    public function addTechScanFromOrder( $OR1_ID )
    {
        $user = Yii::$app->user->getIdentity();
        $sql = "
        INSERT INTO lg1_inventory 
           (CO1_ID, LO1_ID, TE1_ID, MD1_ID, MD2_ID, JT2_ID, OR2_ID, PO2_ID,
            MD1_PART_NUMBER, MD1_DESC1,
            LG1_DESC, LG1_UNIT_COST, LG1_EXTENDED_COST, LG1_OLD_QTY,
            LG1_NEW_QTY, LG1_ADJ_QTY, LG1_CREATED_ON, LG1_CREATED_BY,
            LG1_DELETE_FLAG)
        SELECT 
            o.CO1_ID,
            o1.LO1_FROM_ID,
            o1.TE1_ID,
            m.MD1_ID,
            m2.MD2_ID,
            '' AS JT2_ID, 
            o.OR2_ID, 
            '' AS PO2_ID, 
            m.MD1_PART_NUMBER,
            m.MD1_DESC1,
            'Tech Scan Deduction' AS LG1_DESC,
            (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1)) AS LG1_UNIT_COST,
            (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1))*OR2_ORDER_QTY AS LG1_EXTENDED_COST,
            m2.MD2_ON_HAND_QTY AS LG1_OLD_QTY,
            m2.MD2_ON_HAND_QTY-OR2_ORDER_QTY AS LG1_NEW_QTY,
            (-OR2_ORDER_QTY) AS LG1_ADJ_QTY,
            UTC_TIMESTAMP() AS LG1_CREATED_ON,
            " . $user->US1_ID . " AS LG1_CREATED_BY,
            0 AS LG1_DELETE_FLAG 
        FROM
            or2_order_line o
            LEFT JOIN or1_order_header o1 ON o1.OR1_ID=o.OR1_ID
            LEFT JOIN md1_master_data m ON m.MD1_ID=o.MD1_ID
            LEFT JOIN md2_location m2 ON m2.MD1_ID=o.MD1_ID AND m2.LO1_ID=o1.LO1_TO_ID
            LEFT JOIN um2_conversion fp ON fp.MD1_ID=o.MD1_ID AND fp.UM1_TO_ID=m2.UM1_RECEIPT_ID AND fp.UM1_FROM_ID=m.UM1_PRICING_ID AND fp.UM2_DELETE_FLAG<>1
        WHERE
            o.OR1_ID = " . $OR1_ID . " AND o1.LO1_TO_ID=o1.LO1_FROM_ID AND coalesce(m2.md2_no_inventory,0)=0";

        $result = Yii::$app->db->createCommand($sql)->execute();
        return $result;
    }

    public function removeTechScanItemFromOrder( OrderLine $orderLine )
    {
        $sql = "
            INSERT INTO lg1_inventory 
                (CO1_ID,LO1_ID,TE1_ID,MD1_ID,MD2_ID,JT2_ID,OR2_ID,PO2_ID,MD1_PART_NUMBER,MD1_DESC1,
                 LG1_DESC,LG1_UNIT_COST,LG1_EXTENDED_COST,LG1_OLD_QTY,LG1_NEW_QTY,LG1_ADJ_QTY,
                 LG1_CREATED_ON,LG1_CREATED_BY,LG1_DELETE_FLAG) 
            SELECT 
                o1.CO1_ID,
                o1.LO1_FROM_ID as LO1_ID,
                o.TE1_ID,
                m.MD1_ID,
                m2.MD2_ID,
                '0' AS JT2_ID, 
                o.OR2_ID,
                '0' AS PO2_ID, 
                m.MD1_PART_NUMBER,
                m.MD1_DESC1,
                'Tech Scan Line Deleted' AS LG1_DESC,
                (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1)) AS LG1_UNIT_COST,
                (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1)) * :qty AS LG1_EXTENDED_COST,
                m2.MD2_ON_HAND_QTY - :qty AS LG1_OLD_QTY,
                m2.MD2_ON_HAND_QTY AS LG1_NEW_QTY,
                :qty AS LG1_ADJ_QTY,
                UTC_TIMESTAMP() AS LG1_CREATED_ON,
                :US1_ID AS LG1_CREATED_BY,
                0 AS LG1_DELETE_FLAG 
            FROM 
                or2_order_line o 
                LEFT JOIN or1_order_header o1 ON o1.OR1_ID=o.OR1_ID 
                LEFT JOIN md1_master_data m ON m.MD1_ID=o.MD1_ID 
                LEFT JOIN md2_location m2 ON m2.MD1_ID=o.MD1_ID AND m2.LO1_ID=o1.LO1_TO_ID 
                LEFT JOIN um2_conversion fp ON fp.MD1_ID=o.MD1_ID AND fp.UM1_TO_ID=m2.UM1_RECEIPT_ID AND fp.UM1_FROM_ID=m.UM1_PRICING_ID AND fp.UM2_DELETE_FLAG<>1 
            WHERE 
                o.OR2_ID= :OR2_ID AND m2.md2_no_inventory=0 ";

        return Yii::$app->db->createCommand($sql, [
            ':qty'    => $orderLine->OR2_ORDER_QTY,
            ':OR2_ID' => $orderLine->OR2_ID,
            ':US1_ID' => Yii::$app->user->getIdentity()->US1_ID
        ])->execute();
    }

    public function addJobTicketFromOrder( $JT1_ID )
    {
        $user = Yii::$app->user->getIdentity();
        $sql = "
        INSERT INTO lg1_inventory 
           (CO1_ID, LO1_ID, TE1_ID, MD1_ID, MD2_ID, JT2_ID, OR2_ID, PO2_ID,
            MD1_PART_NUMBER, MD1_DESC1,
            LG1_DESC, LG1_UNIT_COST, LG1_EXTENDED_COST, LG1_OLD_QTY,
            LG1_NEW_QTY, LG1_ADJ_QTY, LG1_CREATED_ON, LG1_CREATED_BY,
            LG1_DELETE_FLAG)
        SELECT 
            j.CO1_ID,
            j1.LO1_ID,
            j.TE1_ID,
            m.MD1_ID,
            m2.MD2_ID,
            j.JT2_ID,
            0 AS OR2_ID, 
            0 AS PO2_ID, 
            m.MD1_PART_NUMBER,
            m.MD1_DESC1,
            'Job Ticket Deduction' AS LG1_DESC,
            (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1)) AS LG1_UNIT_COST,
            (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1))*JT2_ORDER_QTY AS LG1_EXTENDED_COST,
            m2.MD2_ON_HAND_QTY AS LG1_OLD_QTY,
            floor(m2.MD2_ON_HAND_QTY-JT2_ORDER_QTY) AS LG1_NEW_QTY,
            (-JT2_ORDER_QTY) AS LG1_ADJ_QTY,
            UTC_TIMESTAMP() AS LG1_CREATED_ON,
            " . $user->US1_ID . " AS LG1_CREATED_BY,
            0 AS LG1_DELETE_FLAG
        FROM 
            jt2_job_line j  
            LEFT JOIN  jt1_job_ticket j1 ON j1.JT1_ID=j.JT1_ID
            LEFT JOIN md1_master_data m ON m.MD1_ID=j.MD1_ID  
            LEFT JOIN ca1_category c ON c.CA1_ID=m.CA1_ID 
            LEFT JOIN md2_location m2 ON m2.MD1_ID=j.MD1_ID AND m2.LO1_ID=j1.LO1_ID 
            LEFT JOIN um2_conversion fp ON 
                          fp.MD1_ID=j.MD1_ID AND 
                          fp.UM1_TO_ID=m2.UM1_RECEIPT_ID AND 
                          fp.UM1_FROM_ID=m.UM1_PRICING_ID AND 
                          fp.UM2_DELETE_FLAG<>1 
        WHERE 
            j.JT1_ID= " . $JT1_ID . " AND coalesce(m2.md2_no_inventory,0)=0 AND c.ca1_no_inventory=0";

        $result = Yii::$app->db->createCommand($sql)->execute();
        return $result;
    }

	public function getQuery($params)
	{
		$LTYPE_Exp = new Expression('IF(LG1.JT2_ID > 0, "Job",         IF(LG1.OR2_ID > 0, "Order",       IF(LG1.PO2_ID > 0, "PO",          "-")))');
		$NUMBER_Exp = new Expression('IF(LG1.JT2_ID > 0, j1.JT1_NUMBER, IF(LG1.OR2_ID > 0, o1.OR1_NUMBER, IF(LG1.PO2_ID > 0, p1.PO1_NUMBER, "-")))');
		$LINE_Exp = new Expression('IF(LG1.JT2_ID > 0, j.JT2_LINE,    IF(LG1.OR2_ID > 0, o.OR2_LINE,    IF(LG1.PO2_ID > 0, p.PO2_LINE,    "-")))');

		$query = self::find()->alias('LG1')
			->select([
			    'LG1.LG1_ID', 'LG1.CO1_ID', 'LG1.LO1_ID', 'LG1.TE1_ID',
                'LG1.MD1_ID', 'LG1.MD2_ID', 'LG1.JT2_ID',
				'LG1.OR2_ID', 'LG1.PO2_ID',
                'lo.LO1_NAME',
                't.TE1_NAME',
                'LG1.LG1_DESC',
				'LG1.MD1_PART_NUMBER', 'LG1.MD1_DESC1', 'LG1.LG1_UNIT_COST',
                'LG1.LG1_EXTENDED_COST',
				'LG1.LG1_OLD_QTY', 'LG1.LG1_NEW_QTY', 'LG1.LG1_ADJ_QTY',
				'LTYPE' => $LTYPE_Exp,
				'NUMBER' => $NUMBER_Exp,
				'LINE' => $LINE_Exp,
				'LG1_CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('LG1.LG1_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
				'CREATED_BY' => 'u.US1_NAME',
				])

            ->leftJoin("lo1_location as lo" , "lo.lo1_id = LG1.lo1_id AND lo.lo1_delete_flag = 0")
            ->leftJoin("te1_technician as t" , "t.te1_id = LG1.te1_id AND t.te1_delete_flag = 0")
            ->leftJoin("us1_user as u" , "u.us1_id = LG1.lg1_created_by")

			->leftJoin("or2_order_line as o" , "o.or2_id = LG1.or2_id")
            ->leftJoin("or1_order_header o1" , "o1.or1_id = o.or1_id")

			->leftJoin("po2_order_line p" ,    "p.po2_id = LG1.po2_id")
            ->leftJoin("po1_order_header p1" , "p1.po1_id = p.po1_id")

			->leftJoin("jt2_job_line j" , "j.jt2_id = LG1.jt2_id")
			->leftJoin("jt1_job_ticket j1" , "j1.jt1_id = j.jt1_id");

		$user = Yii::$app->user->getIdentity();

		if($user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $query->leftJoin('vd1_vendor vd1',  "vd1.VD1_ID = o1.VD1_ID AND vd1.VD1_DELETE_FLAG = 0");
            $query->leftJoin('vd1_vendor vd2',  "vd2.VD1_ID = p1.VD1_ID AND vd2.VD1_DELETE_FLAG = 0");
            $query->leftJoin('vd1_vendor vd3',  "vd3.VD1_ID = j1.VD1_ID AND vd3.VD1_DELETE_FLAG = 0");
            $query->andWhere('IF(LG1.JT2_ID > 0, vd3.VD0_ID, IF(LG1.OR2_ID > 0, vd1.VD0_ID, vd2.VD0_ID)) = ' . $user->VD0_ID);
        }

		$query->andFilterWhere(['=', 'LG1_DELETE_FLAG', 0]);
		$query->orderBy(['LG1.LG1_CREATED_ON' => SORT_DESC, 'NUMBER' => SORT_ASC, 'LINE' => SORT_ASC]);

		if(isset($user->CO1_ID) && $user->CO1_ID != 0) {
			$query->andFilterWhere(['=', 'LG1.CO1_ID', $user->CO1_ID]);
		}

		if(isset($user->LO1_ID) && $user->LO1_ID != 0) {
			$query->andFilterWhere(['=', 'LG1.LO1_ID', $user->LO1_ID]);
		}

		if ($params && !empty($params)) {
			$this->load($params, '');
		}

		if(isset($params['CO1_ID']) && $params['CO1_ID'] != 0) {
			$query->andFilterWhere(['=', 'LG1.CO1_ID', $params['CO1_ID']]);
		}

		if(isset($params['LO1_ID']) && $params['LO1_ID'] != 0) {
			$query->andFilterWhere(['=', 'LG1.LO1_ID', $params['LO1_ID']]);
		}

		if(isset($params['from'])) {
			$query->andFilterWhere(['>=', 'LG1_CREATED_ON', $params['from']]);
		}

		if(isset($params['to'])) {
			$query->andFilterWhere(['<=', 'LG1_CREATED_ON', $params['to']]);
		}

		if(isset($this->CREATED_ON)) {
			$query->andFilterWhere(['like', 'DATE_FORMAT(IN1.IN1_CREATED_ON, "%m-%d-%Y")', $this->CREATED_ON]);
		}

		if(isset($params['LG1_CREATED_ON'])) {
			$query->andFilterWhere(['like', 'DATE_FORMAT(LG1_CREATED_ON, "%m-%d-%Y")', $params['LG1_CREATED_ON']]);
		}

		if(isset($params['LO1_NAME']) && $params['LO1_NAME'] != 0) {
			$query->andFilterWhere(['=', 'LG1.LO1_ID', $params['LO1_NAME']]);
		}

		if(isset($params['CREATED_BY'])) {
			$query->andFilterWhere(['=', 'u.US1_ID', $params['CREATED_BY']]);
		}

		if(isset($params['TE1_NAME'])) {
			$query->andFilterWhere(['=', 'LG1.TE1_ID', $params['TE1_NAME']]);
		}

		if(isset($params['LG1_DESC'])) {
			$query->andFilterWhere(['like', 'LG1.LG1_DESC', $params['LG1_DESC']]);
		}

		if(isset($params['LTYPE'])) {
			$query->andFilterWhere(['like', $LTYPE_Exp, $params['LTYPE']]);
		}

		if(isset($params['NUMBER'])) {
			$query->andFilterWhere(['like', $NUMBER_Exp, $params['NUMBER']]);
		}

		if(isset($params['LINE'])) {
			$query->andFilterWhere(['like', $LINE_Exp, $params['LINE']]);
		}

		if(isset($params['MD1_PART_NUMBER'])) {
			$query->andFilterWhere(['like', 'LG1.MD1_PART_NUMBER', $params['MD1_PART_NUMBER']]);
		}

		if(isset($params['MD1_DESC1'])) {
			$query->andFilterWhere(['like', 'LG1.MD1_DESC1', $params['MD1_DESC1']]);
		}

		if(isset($params['LG1_UNIT_COST'])) {
			$query->andFilterWhere(['like', 'ROUND(LG1.LG1_UNIT_COST, 2)', $params['LG1_UNIT_COST']]);
		}

		if(isset($params['LG1_EXTENDED_COST'])) {
			$query->andFilterWhere(['like', 'ROUND(LG1.LG1_EXTENDED_COST, 2)', $params['LG1_EXTENDED_COST']]);
		}

		if(isset($params['LG1_OLD_QTY'])) {
			$query->andFilterWhere(['like', 'LG1.LG1_OLD_QTY', $params['LG1_OLD_QTY']]);
		}

		if(isset($params['LG1_NEW_QTY'])) {
			$query->andFilterWhere(['like', 'LG1.LG1_NEW_QTY', $params['LG1_NEW_QTY']]);
		}

		if(isset($params['LG1_ADJ_QTY'])) {
			$query->andFilterWhere(['like', 'LG1.LG1_ADJ_QTY', $params['LG1_ADJ_QTY']]);
		}

		if (isset($params['SORT_COLUMN'])) {
			$query->orderBy([$params['SORT_COLUMN'] => ($params['SORT_DIRECTION'] == 'ASC' ? SORT_ASC : SORT_DESC)]);
		}

		return $query;
	}

	public function getActivityLogs($params)
	{
		$query = $this->getQuery( $params );

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => [
				'pageSize' => $params['perPage'],
				'page'     => $params['page']
			]
		]);

		return $dataProvider;
	}

	public function getActivityLogsForXLS($params)
	{
		$query = $this->getQuery( $params );
		return $query->asArray()->all();
	}

	public function updateOrderLineStock( OrderLine $orderLine, $QTY )
    {
        $user = Yii::$app->user->getIdentity();
        $sql = "INSERT INTO lg1_inventory 
            (CO1_ID,LO1_ID,TE1_ID,MD1_ID,MD2_ID,JT2_ID,OR2_ID,PO2_ID,MD1_PART_NUMBER,MD1_DESC1,
             LG1_DESC,LG1_UNIT_COST,LG1_EXTENDED_COST,LG1_OLD_QTY,LG1_NEW_QTY,LG1_ADJ_QTY,
             LG1_CREATED_ON,LG1_CREATED_BY,LG1_DELETE_FLAG) 
            SELECT 
                o1.CO1_ID,
                o1.LO1_TO_ID as LO1_ID,
                o.TE1_ID,m.MD1_ID,m2.MD2_ID,
                '0' AS JT2_ID, o.OR2_ID,
                '0' AS PO2_ID, m.MD1_PART_NUMBER,m.MD1_DESC1,
                'Shop Order Receiving - Receipient' AS LG1_DESC,
                (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1)) AS LG1_UNIT_COST,
                (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1)) * :qty AS LG1_EXTENDED_COST,
                m2.MD2_ON_HAND_QTY - :qty AS LG1_OLD_QTY,
                m2.MD2_ON_HAND_QTY AS LG1_NEW_QTY,
                :qty AS LG1_ADJ_QTY,
                UTC_TIMESTAMP() AS LG1_CREATED_ON,
                :US1_ID AS LG1_CREATED_BY,
                0 AS LG1_DELETE_FLAG 
            FROM 
                or2_order_line o 
                LEFT JOIN or1_order_header o1 ON o1.OR1_ID=o.OR1_ID 
                LEFT JOIN md1_master_data m ON m.MD1_ID=o.MD1_ID 
                LEFT JOIN md2_location m2 ON m2.MD1_ID=o.MD1_ID AND m2.LO1_ID=o1.LO1_TO_ID 
                LEFT JOIN um2_conversion fp ON fp.MD1_ID=o.MD1_ID AND fp.UM1_TO_ID=m2.UM1_RECEIPT_ID AND fp.UM1_FROM_ID=m.UM1_PRICING_ID AND fp.UM2_DELETE_FLAG<>1 
            WHERE 
                o.OR2_ID= :OR2_ID AND m2.md2_no_inventory=0";

        return Yii::$app->db->createCommand($sql, [
            ':qty'    => $QTY,
            ':OR2_ID' => $orderLine->OR2_ID,
            ':US1_ID' => $user->US1_ID
        ])->execute();
    }

    public function updatePurchaseOrderLineStock(PurchaseOrderLine $orderLine, $QTY)
    {
        $user = Yii::$app->user->getIdentity();
        $sql = "INSERT INTO lg1_inventory
                    (CO1_ID,LO1_ID,TE1_ID,MD1_ID,MD2_ID,JT2_ID,OR2_ID,PO2_ID,MD1_PART_NUMBER,MD1_DESC1,
                    LG1_DESC,LG1_UNIT_COST,LG1_EXTENDED_COST,LG1_OLD_QTY,LG1_NEW_QTY,LG1_ADJ_QTY,
                    LG1_CREATED_ON,LG1_CREATED_BY,LG1_DELETE_FLAG)
                SELECT
                    p1.CO1_ID,
                    p1.LO1_ID,
                    '0' AS TE1_ID,
                    m.MD1_ID,
                    m2.MD2_ID,
                    '0' AS JT2_ID,
                    '0' AS OR2_ID,
                    p.PO2_ID, 
                    m.MD1_PART_NUMBER,
                    m.MD1_DESC1,
                    'Purchase Order Receiving' AS LG1_DESC,
                    (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1)) AS LG1_UNIT_COST,
                    (m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1))* :qty AS LG1_EXTENDED_COST,
                    IF(m2.md2_no_inventory<>0, m2.MD2_ON_HAND_QTY, m2.MD2_ON_HAND_QTY- :qty) AS LG1_OLD_QTY,
                    m2.MD2_ON_HAND_QTY AS LG1_NEW_QTY,
                    IF(m2.md2_no_inventory<>0, 0, :qty) AS LG1_ADJ_QTY,
                    UTC_TIMESTAMP() AS LG1_CREATED_ON,
                    :US1_ID AS LG1_CREATED_BY,
                    0 AS LG1_DELETE_FLAG 
                FROM po2_order_line p 
                    LEFT JOIN po1_order_header p1 ON p1.PO1_ID=p.PO1_ID
                    LEFT JOIN md1_master_data m ON m.MD1_ID=p.MD1_ID
                    LEFT JOIN md2_location m2 ON m2.MD1_ID=p.MD1_ID AND m2.LO1_ID=p1.LO1_ID
                    LEFT JOIN um2_conversion fp ON fp.MD1_ID=p.MD1_ID AND fp.UM1_TO_ID=m2.UM1_RECEIPT_ID AND fp.UM1_FROM_ID=m.UM1_PRICING_ID AND fp.UM2_DELETE_FLAG<>1
                WHERE p.PO2_ID= :PO2_ID";

        $command = Yii::$app->db->createCommand($sql, [
            ':qty'    => $QTY,
            ':PO2_ID' => $orderLine->PO2_ID,
            ':US1_ID' => $user->US1_ID
        ]);

        return $command->execute();
    }

    public function updateStockWhenOrderIsComplete( $OR1_ID )
    {
        $user = Yii::$app->user->getIdentity();

        $sql = "INSERT INTO lg1_inventory
                    (CO1_ID,LO1_ID,TE1_ID,MD1_ID,MD2_ID,JT2_ID,OR2_ID,PO2_ID,MD1_PART_NUMBER,MD1_DESC1,
                    LG1_DESC,LG1_UNIT_COST,LG1_EXTENDED_COST,LG1_OLD_QTY,LG1_NEW_QTY,LG1_ADJ_QTY,
                    LG1_CREATED_ON,LG1_CREATED_BY,LG1_DELETE_FLAG)
	            SELECT
					o1.CO1_ID,
					o1.LO1_FROM_ID AS LO1_ID, 
					'0' AS TE1_ID,
					m.MD1_ID,
					m2.MD2_ID, 
					'0' AS JT2_ID, 
					o.OR2_ID, 
					'0' AS PO2_ID, 
					m.MD1_PART_NUMBER,
					m.MD1_DESC1,
					'Shop order completed' AS LG1_DESC,
					(m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1)) AS LG1_UNIT_COST,
					(m.MD1_UNIT_PRICE/coalesce(fp.UM2_FACTOR,1))*OR2_ORDER_QTY AS LG1_EXTENDED_COST,
					m2.MD2_ON_HAND_QTY+OR2_ORDER_QTY AS LG1_OLD_QTY,
					m2.MD2_ON_HAND_QTY AS LG1_NEW_QTY,
					(-OR2_ORDER_QTY) AS LG1_ADJ_QTY,
					UTC_TIMESTAMP() AS LG1_CREATED_ON,
					:US1_ID AS LG1_CREATED_BY,
					0 AS LG1_DELETE_FLAG 
				FROM or2_order_line o
					LEFT JOIN or1_order_header o1 ON o1.OR1_ID=o.OR1_ID
					LEFT JOIN md1_master_data m ON m.MD1_ID=o.MD1_ID
					LEFT JOIN md2_location m2 ON m2.MD1_ID=o.MD1_ID AND m2.LO1_ID=o1.LO1_FROM_ID
					LEFT JOIN um2_conversion fp ON fp.MD1_ID=o.MD1_ID AND fp.UM1_TO_ID=m2.UM1_RECEIPT_ID AND fp.UM1_FROM_ID=m.UM1_PRICING_ID AND fp.UM2_DELETE_FLAG<>1
				WHERE
					m2.md2_no_inventory=0 AND o.OR1_ID = :OR1_ID";

        $command = Yii::$app->db->createCommand($sql, [
            ':OR1_ID' => $OR1_ID,
            ':US1_ID' => $user->US1_ID
        ]);

        return $command->execute();
    }

	public function getManualInventoryEdits( $params, $isDrilldown = false ) {
		$query = self::find()->alias('LG1')
			->select([
				'LG1.LG1_ID',
				'LG1.CO1_ID',
				'LG1.LO1_ID',
				'LO1.LO1_SHORT_CODE',
				'LO1.LO1_NAME',
				'LG1.MD2_ID',
				'LG1.JT2_ID',
				'LO1.LO1_NAME',
				'COUNT(*) as manual_edits',
				'ROUND(sum(LG1.LG1_EXTENDED_COST),2) as cost'
			])
			->leftJoin("co1_company as CO1" , "CO1.CO1_ID = LG1.CO1_ID")
			->leftJoin("lo1_location as LO1" , "LO1.LO1_ID = LG1.LO1_ID")
			->andFilterWhere(['>=', 'DATE(LG1.LG1_CREATED_ON)', $params['startDate']])
			->andFilterWhere(['<=', 'DATE(LG1.LG1_CREATED_ON)', $params['endDate']])
			->andFilterWhere(['like', 'LG1.LG1_DESC', 'Inventory edited manually']);

		if (!empty($params['LO1_IDs'])) {
			$query->andFilterWhere(['in', 'LO1.LO1_ID', explode(',', $params['LO1_IDs'])]);
		}

		if ($isDrilldown) {
			$query->addSelect(["US1.US1_NAME"]);
			$query->leftJoin("us1_user as US1" , "LG1.LG1_CREATED_BY = US1.US1_ID");
			$query->groupBy(['LG1.LG1_CREATED_BY', 'LG1.LO1_ID']);
		} else {
			$query->groupBy(['LG1.LO1_ID']);
		}

		return $query->asArray()->all();
	}

}
