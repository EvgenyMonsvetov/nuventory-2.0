<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "up1_user_profile".
 *
 * @property int $UP1_ID
 * @property int $user_id
 * @property int $UP1_RESTRICT_CUSTOMERS
 * @property int $UP1_RESTRICT_VENDORS
 * @property int $UP1_RESTRICT_COMPANIES
 * @property int $UP1_INCOGNITO_MODE
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'up1_user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'UP1_RESTRICT_CUSTOMERS', 'UP1_RESTRICT_VENDORS',
              'UP1_RESTRICT_COMPANIES', 'UP1_INCOGNITO_MODE'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UP1_ID' => 'Up1  ID',
            'user_id' => 'User ID',
            'UP1_RESTRICT_CUSTOMERS' => 'Up1  Restrict  Customers',
            'UP1_RESTRICT_VENDORS' => 'Up1  Restrict  Vendors',
            'UP1_RESTRICT_COMPANIES' => 'Up1  Restrict  Companies',
            'UP1_INCOGNITO_MODE' => 'Up1  Incognito  Mode',
        ];
    }
}
