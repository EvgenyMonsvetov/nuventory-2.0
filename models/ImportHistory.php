<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ih1_import_history".
 *
 * @property int $IH1_ID
 * @property int $IH1_TYPE 1 - transactions; 2 - logs;
 * @property int $CO1_ID
 * @property string $IH1_CREATED_ON
 * @property string $IH1_MODIFIED_ON
 * @property string $IH1_FILEPATH
 * @property string $IH1_REMOTE_IP
 * @property int $IH1_ADDED
 * @property int $IH1_UPDATED
 * @property int $IH1_ERRORS
 * @property int $IH1_ALL
 */
class ImportHistory extends ActiveRecord
{
    const TYPE_TRANSACTIONS = 1;
    const TYPE_LOGS         = 2;
    const TYPE_CUSTOMERS    = 3;
    const TYPE_VENDORS      = 4;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ih1_import_history';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['IH1_MODIFIED_ON'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['IH1_MODIFIED_ON'],
                ],
                'value' => date('Y-m-d H:i:s')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IH1_TYPE', 'CO1_ID', 'IH1_FILEPATH', 'IH1_REMOTE_IP', 'IH1_ADDED', 'IH1_UPDATED', 'IH1_ALL', 'IH1_ERRORS'], 'required'],
            [['IH1_ID', 'IH1_TYPE', 'CO1_ID', 'IH1_ADDED', 'IH1_UPDATED', 'IH1_ERRORS', 'IH1_ALL'], 'integer'],
            [['IH1_CREATED_ON', 'IH1_MODIFIED_ON'], 'safe'],
            //[['IH1_FILEPATH'],  'string', 'max' => 30],
            [['IH1_REMOTE_IP'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IH1_ID' => 'Ih1  ID',
            'IH1_TYPE' => 'Ih1  Type',
            'CO1_ID' => 'Co1  ID',
            'IH1_CREATED_ON' => 'Ih1  Created  On',
            'IH1_MODIFIED_ON' => 'Ih1  Modified  On',
            'IH1_FILEPATH' => 'Ih1  Filepath',
            'IH1_REMOTE_IP' => 'Ih1  Remote  Ip',
            'IH1_ADDED' => 'Ih1  Added',
            'IH1_ERRORS' => 'Ih1  Errors',
        ];
    }

    public function search($params)
    {
        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['IH1_ID']   = SORT_DESC;
        }

        $query = ImportHistory::find();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => $params['perPage']
            ],
             'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        $this->load($params, '');

        $query->andFilterWhere([
            'IH1_ID'   => $this->IH1_ID,
            'CO1_ID'   => $this->CO1_ID,
            'IH1_TYPE' => $this->IH1_TYPE
        ]);

        return $dataProvider;
    }
}
