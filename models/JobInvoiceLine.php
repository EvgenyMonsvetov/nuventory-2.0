<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * This is the model class for table "ij2_invoice_line".
 *
 * @property int $IJ2_ID
 * @property int $IJ1_ID
 * @property int $JT2_ID
 * @property int $IJ2_LINE
 * @property int $IJ2_STATUS
 * @property double $IJ2_INVOICE_QTY
 * @property int $MD1_ID
 * @property int $CA1_ID
 * @property int $CO1_ID
 * @property int $GL1_ID
 * @property string $IJ2_PART_NUMBER
 * @property string $IJ2_DESC1
 * @property string $IJ2_DESC2
 * @property double $UM1_ID
 * @property string $IJ2_UNIT_PRICE
 * @property string $IJ2_SELL_PRICE
 * @property int $IJ2_DELETE_FLAG
 * @property int $IJ2_CREATED_BY
 * @property string $IJ2_CREATED_ON
 * @property int $IJ2_MODIFIED_BY
 * @property string $IJ2_MODIFIED_ON
 * @property int $LO1_ID
 * @property string $IJ2_TOTAL_VALUE
 * @property string $IJ2_TOTAL_SELL
 * @property double $IJ2_UNIT_SIZE
 */
class JobInvoiceLine extends ActiveRecord
{
    protected $_tablePrefix = 'IJ2';

    public $COMPLETED_ON;
    public $MODIFIED_BY;
    public $CREATED_BY;
    public $CREATED_ON;
    public $IJ2_DELETE_FLAG;
    public $JT1_IDs;
    public $IJ1_IDs;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ij2_invoice_line';
    }

    public static function primaryKey()
    {
        return ['IJ2_ID'];
    }

    public function fields() {
        $fields = parent::fields();
        $fields["COMPLETED_ON"] = function($model){ return $model->COMPLETED_ON; };
        $fields["MODIFIED_BY"] = function($model){ return $model->MODIFIED_BY; };
        $fields["CREATED_BY"] = function($model){ return $model->CREATED_BY; };
        $fields["CREATED_ON"] = function($model){ return $model->CREATED_ON; };
        $fields["IJ2_DELETE_FLAG"] = function($model){ return $model->IJ2_DELETE_FLAG; };
        $fields["JT1_IDs"] = function($model){ return $model->JT1_IDs; };
        $fields["IJ1_IDs"] = function($model){ return $model->IJ1_IDs; };
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IJ1_ID', 'JT2_ID', 'IJ2_LINE', 'IJ2_STATUS', 'MD1_ID', 'CA1_ID', 'CO1_ID', 'GL1_ID', 'IJ2_DELETE_FLAG', 'IJ2_CREATED_BY', 'IJ2_MODIFIED_BY', 'LO1_ID'], 'integer'],
            [['IJ2_INVOICE_QTY', 'UM1_ID', 'IJ2_UNIT_PRICE', 'IJ2_SELL_PRICE', 'IJ2_TOTAL_VALUE', 'IJ2_TOTAL_SELL', 'IJ2_UNIT_SIZE'], 'number'],
            [['IJ2_CREATED_ON', 'IJ2_MODIFIED_ON'], 'safe'],
            [['IJ2_PART_NUMBER'], 'string', 'max' => 100],
            [['IJ2_DESC1', 'IJ2_DESC2'], 'string', 'max' => 300],
            [['JT1_IDs', 'IJ1_IDs'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IJ2_ID' => 'Ij2  ID',
            'IJ1_ID' => 'Ij1  ID',
            'JT2_ID' => 'Jt2  ID',
            'IJ2_LINE' => 'Ij2  Line',
            'IJ2_STATUS' => 'Ij2  Status',
            'IJ2_INVOICE_QTY' => 'Ij2  Invoice  Qty',
            'MD1_ID' => 'Md1  ID',
            'CA1_ID' => 'Ca1  ID',
            'CO1_ID' => 'Co1  ID',
            'GL1_ID' => 'Gl1  ID',
            'IJ2_PART_NUMBER' => 'Ij2  Part  Number',
            'IJ2_DESC1' => 'Ij2  Desc1',
            'IJ2_DESC2' => 'Ij2  Desc2',
            'UM1_ID' => 'Um1  ID',
            'IJ2_UNIT_PRICE' => 'Ij2  Unit  Price',
            'IJ2_SELL_PRICE' => 'Ij2  Sell  Price',
            'IJ2_DELETE_FLAG' => 'Ij2  Delete  Flag',
            'IJ2_CREATED_BY' => 'Ij2  Created  By',
            'IJ2_CREATED_ON' => 'Ij2  Created  On',
            'IJ2_MODIFIED_BY' => 'Ij2  Modified  By',
            'IJ2_MODIFIED_ON' => 'Ij2  Modified  On',
            'LO1_ID' => 'Lo1  ID',
            'IJ2_TOTAL_VALUE' => 'Ij2  Total  Value',
            'IJ2_TOTAL_SELL' => 'Ij2  Total  Sell',
            'IJ2_UNIT_SIZE' => 'Ij2  Unit  Size',
        ];
    }

    public function getQuery($params)
    {
        $query = JobInvoiceLine::find()->alias('IJ2')
                    ->select([  'IJ2.IJ2_ID', 'IJ2.IJ1_ID', 'IJ1.IJ1_NUMBER', 'IJ2.IJ2_LINE', 'IJ2.IJ2_STATUS', 'IJ2.MD1_ID', 'IJ2.CO1_ID',
                                'IJ2.IJ2_DELETE_FLAG', 'IJ2.LO1_ID', 'IJ2.CA1_ID', 'IJ2.GL1_ID', 'IJ2.IJ2_DESC1', 'IJ2.IJ2_DESC2',
                                'if(IJ2.IJ2_STATUS= 1 , "Received", "Open") AS IJ2_INVOICE_STATUS_TEXT', 'IJ1.LO1_ID',
                                'UM1.UM1_NAME', 'CA1.CA1_NAME', 'GL1.GL1_NAME', 'VD1.VD1_NAME', 'IJ2.IJ2_INVOICE_QTY', 'IJ2.IJ2_UNIT_PRICE',
                                '(IJ2.IJ2_INVOICE_QTY * IJ2.IJ2_UNIT_PRICE) AS IJ2_EXTENDED_VALUE',
			                    'MODIFIED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IJ2.IJ2_MODIFIED_ON', $params) . ', "%m.%d.%Y %H:%i")',
			                    'CREATED_ON' => 'DATE_FORMAT(' . $this->getLocalDate('IJ2.IJ2_CREATED_ON', $params) . ', "%m.%d.%Y %H:%i")',
                                'IJ2.IJ2_MODIFIED_BY', 'IJ2.IJ2_CREATED_BY',
                                'CREATED_BY'  => 'us1_create.US1_NAME',
                                'MODIFIED_BY' => 'us1_modify.US1_NAME'])
                    ->leftJoin("md2_location as MD2" , "MD2.MD1_ID = IJ2.MD1_ID AND MD2.LO1_ID = IJ2.LO1_ID AND MD2.CO1_ID = IJ2.CO1_ID")
                    ->leftJoin("vd1_vendor as VD1" , "VD1.VD1_ID = MD2.VD1_ID AND VD1.VD1_DELETE_FLAG=0")
                    ->leftJoin("ij1_invoice_header as IJ1" , "IJ1.IJ1_ID = IJ2.IJ1_ID")
                    ->leftJoin("ca1_category as CA1" , "CA1.CA1_ID = IJ2.CA1_ID")
                    ->leftJoin("gl1_glcode as GL1" , "GL1.GL1_ID = IJ2.GL1_ID")
                    ->leftJoin("um1_unit as UM1" , "UM1.UM1_ID = IJ2.UM1_ID")
                    ->leftJoin("us1_user as us1_create" , "us1_create.US1_ID = IJ2.IJ2_CREATED_BY")
                    ->leftJoin("us1_user as us1_modify" , "us1_modify.US1_ID = IJ2.IJ2_MODIFIED_BY");
        $query->where(['=', 'IJ2.IJ2_DELETE_FLAG', 0]);
        $query->orderBy(['IJ2_LINE' => SORT_ASC]);

        if ($params && !empty($params)) {
            $this->load($params, '');
        }

        if(isset($this->CO1_ID)) {
            $query->andFilterWhere(['=', 'IJ2.CO1_ID', $this->CO1_ID]);
        }

        if(isset($this->LO1_ID)) {
            $query->andFilterWhere(['=', 'IJ1.LO1_ID', $this->LO1_ID]);
        }

        if(isset($this->IJ1_ID)) {
            $query->andFilterWhere(['=', 'IJ2.IJ1_ID', $this->IJ1_ID]);
        }

        return $query;
    }

    public function getAll($params)
    {
       $defaultOrder = [];
       if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
       } else {
           $defaultOrder['IJ2_LINE'] = SORT_ASC;
       }

       $query = $this->getQuery( $params );

       $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
       ]);

        return $dataProvider;
    }

	public function getPdfQuery($params)
	{
		$query = JobInvoiceLine::find()->alias('IJ2')
			->select([ 'IJ2.IJ2_ID', 'IJ2.IJ1_ID', 'IJ2.IJ2_INVOICE_QTY', 'IJ2.IJ2_PART_NUMBER', 'IJ2.CA1_ID',
				'IJ2.IJ2_UNIT_PRICE', 'IJ2.IJ2_SELL_PRICE', 'IJ2.IJ2_TOTAL_VALUE', 'IJ2.IJ2_TOTAL_SELL',
				'IJ2.IJ2_DESC1', 'IJ2.IJ2_DESC1', 'IJ2.IJ2_LINE', 'IJ1.IJ1_ID',
				'md1.MD1_VENDOR_PART_NUMBER', 'ca1.CA1_NAME', 'MD2.VD1_ID', 'vd1.VD1_NAME',
				'um1.UM1_SHORT_CODE', 'um1.UM1_SIZE', 'te1.TE1_SHORT_CODE', 'te1.TE1_NAME'])
			->leftJoin("ij1_invoice_header as IJ1" , "IJ1.IJ1_ID = IJ2.IJ1_ID")
			->leftJoin("md1_master_data as md1" , "md1.MD1_ID = IJ2.MD1_ID")
			->leftJoin("md2_location as MD2" , "MD2.MD1_ID = md1.MD1_ID AND MD2.LO1_ID = IJ2.LO1_ID AND MD2.CO1_ID = IJ2.CO1_ID")
			->leftJoin("vd1_vendor as VD1" , "VD1.VD1_ID = MD2.VD1_ID")
			->leftJoin("jt2_job_line as jt2" , "jt2.JT2_ID = IJ2.JT2_ID")
			->leftJoin("jt1_job_ticket as jt1" , "jt1.JT1_ID = jt2.JT1_ID")
			->leftJoin("um1_unit as UM1" , "md1.UM1_PRICING_ID = UM1.UM1_ID")
			->leftJoin("ca1_category as CA1" , "CA1.CA1_ID = IJ2.CA1_ID")
			->leftJoin("te1_technician AS te1" , "te1.TE1_ID = jt2.TE1_ID");
		$query->where(['=', 'IJ2.IJ2_DELETE_FLAG', 0]);
		$query->orderBy(['IJ2.IJ1_ID' => SORT_ASC, 'IJ2.IJ2_LINE' => SORT_ASC]);

		if ($params && !empty($params)) {
			$this->load($params, '');
		}

		if(isset($this->CO1_ID)) {
			$query->andFilterWhere(['=', 'IJ2.CO1_ID', $this->CO1_ID]);
		}

		if(isset($this->LO1_ID)) {
			$query->andFilterWhere(['=', 'IJ1.LO1_ID', $this->LO1_ID]);
		}

		if (isset($this->JT1_IDs)) {
			$query->andFilterWhere(['IN', 'jt1.JT1_ID', explode(',', $this->JT1_IDs)]);
		}

		if (isset($this->IJ1_IDs)) {
			$query->andFilterWhere(['IN', 'IJ1.IJ1_ID', explode(',', $this->IJ1_IDs)]);
		}

		return $query;
	}

	public function getPdfLines($params)
	{
		$query = $this->getPdfQuery( $params );
		return $query->asArray()->all();
	}
}
