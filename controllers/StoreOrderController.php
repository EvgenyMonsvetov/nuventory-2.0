<?php

namespace app\controllers;

use app\models\Company;
use app\models\Location;
use app\models\MasterData;
use app\models\OrderHeader;
use app\models\OrderLine;
use \Yii;
use app\components\AuthController;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use app\services\OrderHeader as StoreOrderService;
use app\components\PdfTrait;
use app\pdfs\Pdf;

class StoreOrderController extends AuthController
{
	use PdfTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'delete', 'delete-multiple', 'preload', 'export', 'print-pdf', 'complete', 'pick-ticket'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'preload', 'export', 'print-pdf', 'complete', 'pick-ticket'],
                'allow'   => true,
                'roles'   => ['STORE_ORDERS_READ']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['STORE_ORDERS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;

        $defaultOrder = [];
        if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
        } else {
           $defaultOrder['OR1_NUMBER'] = SORT_DESC;
        }

        $orderHeaderModel = new OrderHeader();
        $query = $orderHeaderModel->getQuery( $request->getQueryParams() );
        $query->asArray();

        $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $request->get('perPage'),
               'page'     => $request->get('page')
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
        ]);

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $orderHeaderModel = new OrderHeader();
        $query = $orderHeaderModel->getOrderHeader($id);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
        $dataProvider->query->asArray();
        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $orderHeader = OrderHeader::find()->where(["OR1_ID" => $id])->one();
        $result = $orderHeader->delete();

        return [
            'data' => [
                'success'=> $result['success'],
                'errors' => $result['errors']
            ]
        ];
    }

	public function actionDeleteMultiple()
	{
		$OR1_IDs = Yii::$app->getRequest()->post('OR1_IDs');

		foreach ( $OR1_IDs as $OR1_ID ) {
		    $orderHeader = OrderHeader::find()->where(["OR1_ID" => $OR1_ID])->one();
			$orderHeader->delete();
		}

		return [
			'success' => true
		];
	}

    public function actionInitOrder()
    {
        $filters = Yii::$app->request->getQueryParams();

        $user = Yii::$app->user->getIdentity();

        $company = Company::findOne(['CO1_ID' => $user->CO1_ID]);

        if( !empty($filters['isTechScan']) && $filters['isTechScan']){
            $fromLocation = Location::findOne(['LO1_ID' => $user->LO1_ID]);
            $toLocation   = $fromLocation;
        }else{
            $fromLocation = Location::getCentralLocation($user->CO1_ID);
            $toLocation   = Location::findOne(['LO1_ID' => $user->LO1_ID]);
        }

        $masterData = new MasterData();
        $parts = $masterData->getQuickAddParts($filters);

        $numberService = new \app\services\Number();
        $orderNumber = $numberService->getOrderNumber();

        return [
            'company'         => $company,
            'fromLocation'    => $fromLocation,
            'toLocation'      => $toLocation,
            'parts'           => $parts,
            'OR1_NUMBER'      => $orderNumber->OR1_NUMBER,
            'OR1_ID'          => $orderNumber->OR1_ID
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $orderHeaderService = new \app\services\OrderHeader();
        $errors = [];
        foreach($data['orders'] as $order){
            $result = $orderHeaderService->create( $order );
            if($result['errors']){
                $errors += $result['errors'];
            }
        }

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

    public function actionPreload()
    {
        $request = Yii::$app->getRequest();

        $user = new \app\models\User();
        $users = $user->search( $request->getQueryParams() );
        $users->query->asArray();

        $vendor = new \app\models\Vendor();
        $vendors = $vendor->getAllVendors( $request->getQueryParams() );
        $vendors->query->asArray();

        $location = new \app\models\Location();
        $locations = $location->search( $request->getQueryParams() );
        $locations->query->asArray();

        return [
            'users'       => $users->getModels(),
            'vendors'     => $vendors->getModels(),
            'shops'       => $locations->getModels(),
        ];
    }

    public function actionExport()
    {
        $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $orderHeaderService = new \app\services\OrderHeader();

        switch ($params['type']) {
            case 'xls':
            default:
                return $orderHeaderService->generateXls($params);
        }
    }

	public function actionPrintPdf()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printStoreOrder($params, OrderHeader::TYPE_STORE_ORDER);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}

	public function actionLinesSummary( $OR1_ID )
    {
        $model = new OrderLine();
        $data = $model->getLinesSummary($OR1_ID);

        return [
            'data' => $data
        ];
    }

    public function actionReceive()
    {
        $data = Yii::$app->getRequest()->post();

        $OR1_ID = $data['OR1_ID'];
        $orderLines = $data['orderLines'];

        $storeOrderService = new StoreOrderService();
        $result = $storeOrderService->receive($OR1_ID, $orderLines);

        return [
            'data' => $result
        ];
    }

    public function actionComplete()
    {
        $data = Yii::$app->getRequest()->post();

	    $storeOrderService = new StoreOrderService();
	    $result = $storeOrderService->complete($data);

	    return [
		    'data' => $result
	    ];
    }

	public function actionPickTicket()
	{
		$data = Yii::$app->getRequest()->post();

		$OR1_IDs = $data['OR1_IDs'];

		$errors = [];
		foreach( $OR1_IDs as $OR1_ID ){
			$storeOrderService = new StoreOrderService();
			$result = $storeOrderService->pickTicket($OR1_ID);
			if(!$result['success']){
				$errors += $result['errors'];
			}
		}

		return [
			'data' => [
				'success' => !$errors,
				'errors'  => $errors
			]
		];
	}
}