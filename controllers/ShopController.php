<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 12/11/2018
 * Time: 16:14
 */

namespace app\controllers;

use app\components\AuthController;
use app\models\Location;
use app\models\MasterData;
use app\services\Location as LocationService;
use yii\filters\AccessControl;
use Yii;

class ShopController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'delete', 'export', 'preload', 'all', 'create-shops', 'save-material-budget', 'get-material-budget', 'generate-shop-short-code'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'export', 'preload', 'all', 'create-shops', 'save-material-budget', 'get-material-budget'],
                'allow'   => true,
                'roles'   => ['SHOPS_READ']
            ],[
                'actions' => ['create', 'generate-shop-short-code'],
                'allow'   => true,
                'roles'   => ['SHOPS_ADD']
            ],[
                'actions' => ['update'],
                'allow'   => true,
                'roles'   => ['SHOPS_EDIT']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['SHOPS_DELETE']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $location = new Location();
        $dataProvider = $location->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

	public function actionAll()
	{
		$request = Yii::$app->getRequest();

		$location = new Location();
		$dataProvider = $location->getAllLocations( $request->getQueryParams() );
		$dataProvider->query->asArray();

		return [
			'data'  => $dataProvider->getModels(),
			'count' => $dataProvider->getTotalCount()
		];
	}

    public function actionView($id)
    {
        $location = new Location();
        $query = $location->getSearchQuery(['LO1_ID' => $id]);
        $query->asArray();
        $data = $query->one();

        return [
            'data' => $data
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        if( isset($data['LO1_ID'])){
            unset($data['LO1_ID']);
        }

        $location = new Location();

        if( $location->load($data, '') && $location->save( true ) ){
            return [
                'success' => true,
                'data'    => $location
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $location->getErrorSummary(true)
            ];
        }
    }

	public function actionGenerateShopShortCode()
	{
		$service = new LocationService();
		return $service->getLocationNumber();
	}

    public function actionCreateShops()
    {
	    $errors = [];
	    $items = [];
        $data = Yii::$app->request->post();

	    foreach ($data as $item) {
		    $location = new Location();
		    if( isset($item['LO1_ID'])){
			    unset($item['LO1_ID']);
		    }

		    if ( !($location->load($item, '') && $location->save( true )) ) {
			    $errors[] = $location->getErrorSummary(true);
			    $item['errors'] = $location->getErrorSummary(true);
		    }
		    $items[] = $item;
	    }

	    return [
		    'data' => $errors ? $items : [],
		    'success' => !$errors,
		    'errors'    => $errors
	    ];
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $location = Location::findOne(['LO1_ID' => $data['LO1_ID']]);

        if( $location->load($data, '') && $location->save( true ) ){
            return [
                'success' => true,
                'data'    => $location
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $location->getErrorSummary(true)
            ];
        }
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $location = Location::findOne(['LO1_ID' => $data['id']]);
        $location->LO1_DELETE_FLAG = 1;
        $location->save(false);

        return [
            'success' => true
        ];
    }

    public function actionMasterData( $MD1_ID )
    {
        $user = Yii::$app->user->getIdentity();

        $masterData = MasterData::findOne(['MD1_ID' => $MD1_ID]);
	    if (!$masterData) {
		    $masterData = new MasterData();
	    }
        $location = new Location();
        $items = $location->getMasterDataItems( $user->CO1_ID, $masterData );

        return [
            'data'  => $items
        ];
    }

    public function actionCentralLocations()
    {
        $user = Yii::$app->user->getIdentity();
        $LO1_ID = Yii::$app->request->getQueryParam('LO1_ID');

        $location = new Location();
        $items = $location->getCentralLocations( $user->CO1_ID, $LO1_ID );

        return [
            'data'  => $items
        ];
    }

	public function actionPreload()
	{
		$request = Yii::$app->getRequest();

		$company = new \app\models\Company();
		$companies = $company->search( $request->getQueryParams() );
		$companies->query->asArray();

		$location = new \app\models\Location();
		$locations = $location->search( $request->getQueryParams() );
		$locations->query->asArray();

		$country = new \app\models\Country();
		$countries = $country->search( $request->getQueryParams() );
		$countries->query->asArray();

		return [
			'companies' => $companies->getModels(),
			'shops'   => $locations->getModels(),
			'countries' => $countries->getModels()
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new LocationService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}

	public function actionLastShortCode()
	{
		$location = new Location();
		$model = $location->getLocationLastShortCode();

		return [
			'LO1_SHORT_CODE'  => $model['LO1_SHORT_CODE']
		];
	}

	public function actionSaveMaterialBudget()
	{
		$data = Yii::$app->getRequest()->post();

		$service = new LocationService();
		return $service->saveMaterialsBudget($data);
	}

	public function actionGetMaterialBudget()
	{
		$user = Yii::$app->user->getIdentity();
		$LO1_ID = $user->LO1_ID;

		$location = new Location();
		$query = $location->getSearchQuery(['LO1_ID' => $LO1_ID]);
		$query->asArray();
		$result = $query->one();

		return [
			'MOUNTHLY_GROSS_SALE'=> isset($result['LO1_MOUNTHLY_GROSS_SALE']) ? $result['LO1_MOUNTHLY_GROSS_SALE'] : 0,
			'TARGET_PERCENTAGE'=> isset($result['LO1_TARGET_PERCENTAGE']) ? $result['LO1_TARGET_PERCENTAGE'] : 0
		];
	}
}
