<?php

namespace app\controllers;

use Yii;
use app\components\AuthController;
use app\models\Inventory;
use app\services\Inventory as InventoryService;
use yii\filters\AccessControl;
use app\components\PdfTrait;
use app\pdfs\Pdf;

class InventoryController extends AuthController
{
	use PdfTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['activity-log', 'export', 'print-pdf', 'preload', 'manual-inventory-edits'], //only be applied to
            'rules' => [[
                'actions' => ['activity-log', 'export', 'print-pdf', 'preload'],
                'allow' => true,
                'roles' => ['DASHBOARD_READ']
            ],[
	            'actions' => ['manual-inventory-edits'],
	            'allow'   => true,
	            'roles'   => ['MANUAL_INVENTORY_EDITS_READ']
            ]]
        ];

        return $behaviors;
    }

    public function actionActivityLog()
    {
	    $request = Yii::$app->request;
	    $model = new Inventory();
	    $dataProvider = $model->getActivityLogs($request->getQueryParams());
	    $dataProvider->query->asArray();

	    return [
		    'data' => [
			    'currentVocabulary' => $dataProvider->getModels(),
			    'count' => $dataProvider->getTotalCount()
		    ]
	    ];
    }

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		switch ($params['type']) {
			case 'xls':
			default:
				$service = new InventoryService();
				return $service->generateXls($params);
		}
	}

	public function actionPrintPdf()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printActivities($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}

	public function actionPreload()
	{
		$request = Yii::$app->getRequest();

		$user = new \app\models\User();
		$users = $user->search( $request->getQueryParams() );
		$users->query->asArray();

		$location = new \app\models\Location();
		$locations = $location->getAllLocations( $request->getQueryParams() );
		$locations->query->asArray();

		$technician = new \app\models\Technician();
		$technicians = $technician->getAllTechnicians( $request->getQueryParams() );
		$technicians->query->asArray();

		return [
			'users'       => $users->getModels(),
			'shops'       => $locations->getModels(),
			'technicians' => $technicians->getModels()
		];
	}

	public function actionManualInventoryEdits()
	{
		$request = Yii::$app->getRequest();

		$inventoryService = new InventoryService();
		$result = $inventoryService->getManualInventoryEdits( $request->getQueryParams() );

		return [
			'status' => '200',
			'statusText' => 'success',
			'series' => isset($result['data']) ? $result['data']: [],
			'drilldown' => isset($result['drilldown']) ? $result['drilldown']: [],
			'title' => isset($result['name']) ? $result['name'] : '',
		];
	}
}
