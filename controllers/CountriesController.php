<?php

namespace app\controllers;

use yii\filters\AccessControl;
use app\components\AuthController;
use app\services\Country as CountryService;
use app\models\Country as CountryModel;
use Yii;

class CountriesController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'delete', 'export'],
            'rules' => [[
                'actions' => ['index', 'view', 'export'],
                'allow'   => true,
                'roles'   => ['COUNTRIES_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['COUNTRIES_ADD']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['COUNTRIES_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $countryModel = new CountryModel();
        $dataProvider = $countryModel->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $countryModel = new CountryModel();
        $query = $countryModel->getSearchQuery(['CY1_ID' => (int)$id]);
        $query->asArray();
        $data = $query->one();

        return [
            'data'  => $data
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $countryService = new CountryService();
        $errors = $countryService->createCountry( $data );

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $countryService = new CountryService();
        $errors = $countryService->deleteCountry( $id );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new CountryService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}
