<?php

namespace app\controllers;

use app\models\OrderHeader;
use \Yii;
use app\components\AuthController;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use app\models\Company;
use app\models\Location;
use app\models\MasterData;
use app\services\OrderHeader as StoreOrderService;
use app\components\PdfTrait;
use app\pdfs\Pdf;

class TechScanController extends AuthController
{
	use PdfTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'delete', 'delete-multiple', 'preload', 'export', 'print-pdf'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'preload', 'export', 'print-pdf'],
                'allow'   => true,
                'roles'   => ['TECH_SCANS_READ']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['TECH_SCANS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;

        $defaultOrder = [];
        if (!empty($params['sort'])) {
           foreach( $params['sort'] as $sort){
               list($prop, $dir) = explode(',', $sort);
               $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
           }
        } else {
           $defaultOrder['OR1_NUMBER'] = SORT_DESC;
        }

        $orderHeaderModel = new OrderHeader();
        $query = $orderHeaderModel->getQuery( $request->getQueryParams(), OrderHeader::TYPE_TECH_SCAN );
        $query->asArray();

        $dataProvider = new ActiveDataProvider([
           'query'      => $query,
           'pagination' => [
               'pageSize' => $request->get('perPage'),
               'page'     => $request->get('page')
           ],
            'sort' => [
               'defaultOrder' => $defaultOrder
           ]
        ]);

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new OrderHeader();
        $query = $model->getOrderHeader($id, OrderHeader::TYPE_TECH_SCAN );
        $query->asArray();

        return [
            'data' =>  $query->one()
        ];
    }

	public function actionDelete()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');

		$orderHeader = OrderHeader::find()->where(["OR1_ID" => $id])->one();
		$result = $orderHeader->delete();

		return [
			'data' => [
				'success'=> $result['success'],
				'errors' => $result['errors']
			]
		];
	}

	public function actionDeleteMultiple()
	{
		$OR1_IDs = Yii::$app->getRequest()->post('OR1_IDs');

		foreach ( $OR1_IDs as $OR1_ID ) {
		    $orderHeader = OrderHeader::find()->where(["OR1_ID" => $OR1_ID])->one();
			$orderHeader->delete();
		}

		return [
			'success' => true
		];
	}

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $orderHeaderService = new \app\services\OrderHeader();
        $errors = [];
        foreach($data['orders'] as $order){
            $result = $orderHeaderService->create( $order );
            if($result['errors']){
                $errors += $result['errors'];
            }
        }

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

    public function actionInitOrder()
    {
        $filters = Yii::$app->request->getQueryParams();

        $user = Yii::$app->user->getIdentity();

        $company = Company::findOne(['CO1_ID' => $user->CO1_ID]);

        $currentLocation = Location::findOne(['LO1_ID' => $user->LO1_ID]);

        $masterData = new MasterData();
        $parts = $masterData->getQuickAddParts($filters);

        $numberService = new \app\services\Number();
        $orderNumber = $numberService->getOrderNumber();

        return [
            'company'         => $company,
            'fromLocation'    => $currentLocation,
            'toLocation'      => $currentLocation,
            'parts'           => $parts,
            'OR1_NUMBER'      => $orderNumber->OR1_NUMBER,
            'OR1_ID'          => $orderNumber->OR1_ID
        ];
    }

    public function actionPreload()
    {
        $request = Yii::$app->getRequest();

        $user = new \app\models\User();
        $users = $user->search( $request->getQueryParams() );
        $users->query->asArray();

        $vendor = new \app\models\Vendor();
        $vendors = $vendor->getAllVendors( $request->getQueryParams() );
        $vendors->query->asArray();

        $technician = new \app\models\Technician();
        $technicians = $technician->getAllTechnicians( $request->getQueryParams() );
        $technicians->query->asArray();

        $location = new \app\models\Location();
        $locations = $location->search( $request->getQueryParams() );
        $locations->query->asArray();

        return [
            'users'     => $users->getModels(),
            'vendors'   => $vendors->getModels(),
            'technicians'   => $technicians->getModels(),
            'shops'   => $locations->getModels(),
        ];
    }

    public function actionExport()
    {
        $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $orderHeaderService = new \app\services\OrderHeader();

        switch ($params['type']) {
            case 'xls':
            default:
                return $orderHeaderService->generateXls($params, OrderHeader::TYPE_TECH_SCAN );
        }
    }

	public function actionPrintPdf()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printStoreOrder($params, OrderHeader::TYPE_TECH_SCAN);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}
}