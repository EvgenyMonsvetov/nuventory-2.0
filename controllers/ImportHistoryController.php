<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 29/05/2018
 * Time: 15:43
 */

namespace app\controllers;

use app\models\Company;
use app\models\ImportHistory;
use \Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use yii\helpers\Json;

class ImportHistoryController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'preload'],
                'allow'   => true,
                'roles'   => ['IMPORT_HISTORY_READ']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $importHistory = new ImportHistory();
        $dataProvider = $importHistory->search( $request->getQueryParams() );

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionView($id)
    {
        $importHistory = ImportHistory::find()->where(['IH1_ID' => $id])->one();

        $importHistoryService = new \app\services\ImportHistory();
        $content = $importHistoryService->getImportFileContent( $importHistory );

        return [
            'data'  => $content
        ];
    }

    public function actionPreload()
    {
        return [
            'companies' => Company::find()->select(['CO1_NAME', 'CO1_ID'])->all()
        ];
    }
}