<?php

namespace app\controllers;

use app\components\AuthController;
use app\models\Glcode;
use app\services\Glcode as GlcodeService;
use yii\filters\AccessControl;
use Yii;

class GlCodeController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'delete', 'delete-multiple', 'export'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'export'],
                'allow'   => true,
                'roles'   => ['GL_CODES_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['GL_CODES_ADD']
            ],[
                'actions' => ['update'],
                'allow'   => true,
                'roles'   => ['GL_CODES_EDIT']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['GL_CODES_DELETE']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $glcode = new Glcode();
        $dataProvider = $glcode->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionView($id)
    {
        $glcode = new Glcode();
        $query = $glcode->getSearchQuery(['GL1_ID' => $id]);
        $query->asArray();
        $data = $query->one();

        return [
            'data' => $data
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        if( isset($data['GL1_ID'])){
            unset($data['GL1_ID']);
        }

        $glcode = new Glcode();

        if( $glcode->load($data, '') && $glcode->save( true ) ){
            return [
                'success' => true,
                'data'    => $glcode
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $glcode->getErrorSummary(true)
            ];
        }
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $glcode = Glcode::findOne(['GL1_ID' => $data['GL1_ID']]);

        if( $glcode->load($data, '') && $glcode->save( true ) ){
            return [
                'success' => true,
                'data'    => $glcode
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $glcode->getErrorSummary(true)
            ];
        }
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $glcode = Glcode::findOne(['UM1_ID' => $data['id']]);
        $glcode->GL1_DELETE_FLAG = 1;
        $glcode->save(false);

        return [
            'success' => true
        ];
    }

	public function actionDeleteMultiple()
	{
		$GL1_IDs = Yii::$app->getRequest()->post('GL1_IDs');

		foreach ( $GL1_IDs as $GL1_ID ) {
			$glcodeModel = Glcode::findOne(['GL1_ID' => $GL1_ID]);
			$glcodeModel->GL1_DELETE_FLAG = 1;
			$glcodeModel->save(false);
		}

		return [
			'success' => true
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new GlcodeService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}
