<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 13/06/2018
 * Time: 17:10
 */
namespace app\controllers;

use app\models\UserProfile;
use Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use app\services\AccessList;
use app\models\AccessList as AccessListModel;

class UserProfileController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['update', 'view'], //only be applied to
            'rules' => [
                [
                    'actions' => ['update'],
                    'allow' => true,
                    'roles' => ['PROFILE_EDIT']
                ],[
                    'actions' => ['view'],
                    'allow' => true,
                    'roles' => ['PROFILE_READ']
                ],
            ]
        ];

        return $behaviors;
    }

    public function actionView($userId)
    {
        $userProfile = UserProfile::findOne(['user_id' => $userId]);

        $userService = new \app\services\User();
        $tree = $userService->getAccessTree( $userId );

        return [
            'data' => [
                'profile' => $userProfile,
                'access'  => $tree['data']
            ]
        ];
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $userProfile = UserProfile::findOne(['UP1_ID' => $data['UP1_ID']]);
        $userProfile->load($data, '');
        if($userProfile->save()){

            $accessList = new AccessList();
            if($data['UP1_RESTRICT_COMPANIES']){
                $accessItems = Json::decode( $data['access'] );
                foreach( $accessItems as $accessItem){
                    if($accessItem['access'] == AccessList::ACCESS_NO_ACCESS ){
                        $accessList->updateAccessList(
                            AccessListModel::OBJ_TYPE_COMPANY,
                            $accessItem['CO1_ID'],
                            [['user_id' => $data['user_id'], 'hasAccess' => false]]);
                    }elseif( $accessItem['access'] == AccessList::ACCESS_RESTRICTED ){

                        $accessList->updateAccessList(
                            AccessListModel::OBJ_TYPE_COMPANY,
                            $accessItem['CO1_ID'],
                            [['user_id' => $data['user_id'], 'hasAccess' => true]]);

                        foreach( $accessItem['customers'] as $customer ){
                           $accessList->updateAccessList(
                            AccessListModel::OBJ_TYPE_CUSTOMER,
                                $customer['CU1_ID'],
                                [['user_id' => $data['user_id'], 'hasAccess' => $customer['hasAccess']]],
                                $accessItem['CO1_ID'] );
                        }

                        foreach( $accessItem['vendors'] as $vendor ){
                            $accessList->updateAccessList(
                            AccessListModel::OBJ_TYPE_VENDOR,
                                $vendor['VD1_ID'],
                                [['user_id' => $data['user_id'], 'hasAccess' => $vendor['hasAccess']]],
                                $accessItem['CO1_ID'] );
                        }
                    }else if( $accessItem['access'] == AccessList::ACCESS_FULL ){
                        $accessList->updateAccessList(
                            AccessListModel::OBJ_TYPE_COMPANY,
                            $accessItem['CO1_ID'],
                            [['user_id' => $data['user_id'], 'hasAccess' => false]]);

                        $accessList->updateAccessList(
                            AccessListModel::OBJ_TYPE_COMPANY,
                            $accessItem['CO1_ID'],
                            [['user_id' => $data['user_id'], 'hasAccess' => true]]);
                    }
                }
            }else{

                $accessList->clear( $data['user_id'] );
            }

            return [
                'data' => $userProfile
            ];
        }else{
            return [
                'errors' => array_values($userProfile->getErrors())
            ];
        }
    }
}
