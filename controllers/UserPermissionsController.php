<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 13/06/2018
 * Time: 17:10
 */
namespace app\controllers;

use app\services\UserPermissions;
use Yii;
use app\components\AuthController;
use yii\filters\AccessControl;

class UserPermissionsController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['update', 'view'], //only be applied to
            'rules' => [
                [
                    'actions' => ['update', 'view'],
                    'allow' => true,
                    'roles' => ['USERS_EDIT']
                ],
            ]
        ];

        return $behaviors;
    }

    public function actionView($id)
    {
        $userPermissionsService = new UserPermissions();
        $permissions = $userPermissionsService->getPermissions( $id );

        return [
            'data' => array_values($permissions)
        ];
    }


    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $userPermissionsService = new UserPermissions();
        $success = $userPermissionsService->updatePermissions($data['userid'], $data['permissions']);

        return [
            'success' => $success
        ];
    }
}
