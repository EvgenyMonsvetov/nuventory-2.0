<?php

namespace app\controllers;

use yii\filters\AccessControl;
use app\components\AuthController;
use app\services\Label as LabelService;
use Yii;

class LabelsController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'export'],
            'rules' => [[
                'actions' => ['index', 'view', 'export'],
                'allow'   => true,
                'roles'   => ['DASHBOARD_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['DASHBOARD_READ']
            ],
            ]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $labelService = new LabelService();

        if(!empty($params) && !empty($params['keys'])) {
            $dataProvider = $labelService->getVocabulary($params);
        } else {
            $dataProvider = $labelService->getAllVocabularies($params);
        }
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $labelService = new LabelService();

        if(!empty($params) && !empty($params['keys'])) {
            $currentVocabulary = $labelService->getVocabulary($params);
        } else {
            $currentVocabulary = $labelService->getAllVocabularies($params);
        }

        return [
            'data' => [
                'currentVocabulary' => $currentVocabulary
            ]
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();
        $labelService = new LabelService();
        $errors = $labelService->createLabel( $data );

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new LabelService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}