<?php

namespace app\controllers;

use app\models\Location;
use \Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use app\services\Statement as StatementService;
use app\models\Statement;
use app\components\PdfTrait;
use app\pdfs\Pdf;

class StatementController extends AuthController
{
	use PdfTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'delete-multiple', 'preload', 'export', 'print-pdf', 'generate-statement'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'preload', 'export', 'print-pdf', 'generate-statement'],
                'allow'   => true,
                'roles'   => ['STATEMENTS_READ']
            ],[
                'actions' => ['delete-multiple'],
                'allow'   => true,
                'roles'   => ['STATEMENTS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new Statement();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        $model = new Statement();

        return [
            'data' => $model->getStatement($id)
        ];
    }

	public function actionDeleteMultiple()
	{
		$IS1_IDs = Yii::$app->getRequest()->post('IS1_IDs');

		$statementService = new StatementService();
		return $statementService->deleteMultiple($IS1_IDs);
	}

    public function actionPreload()
    {
        $request = Yii::$app->getRequest();

        $user = new \app\models\User();
        $users = $user->search( $request->getQueryParams() );
        $users->query->asArray();

        $location = new \app\models\Location();
        $locations = $location->search( $request->getQueryParams() );
        $locations->query->asArray();

        return [
            'users'     => $users->getModels(),
            'shops'   => $locations->getModels()
        ];
    }

    public function actionExport()
    {
	    $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $service = new StatementService();

        switch ($params['type']) {
            case 'xls':
            default:
                return $service->generateXls($params);
        }
    }

	public function actionPrintPdf()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printStatement($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}

	public function actionGenerateStatement()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$statementService = new StatementService();
		$results = $statementService->generateStatements($params);

		return [
		    'data' => [
		        'success' => $results['success'],
                'errors'  => array_values($results['errors']),
                'IS1_IDs' => $results['IS1_IDs']
            ]
        ];
	}

    public function actionSend()
    {
        $IS1_IDs = Yii::$app->getRequest()->post('IS1_IDs');
        $hours_offset = Yii::$app->getRequest()->post('hours_offset');
        $statementService = new StatementService();
        $sentAndMissing = $statementService->send($IS1_IDs, $hours_offset);
        $missingEmails = [];
        foreach ($sentAndMissing['missing'] as $LO1_ID) {
            $location=Location::findOne(['LO1_ID',$LO1_ID]);
            $missingEmails[]=$location['LO1_NAME'];
        }
        return [
            'data' => [
                'success' => (bool)count($sentAndMissing['sent']),
                'missing' => implode(',',$missingEmails)
            ]
        ];
    }
}