<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 12/11/2018
 * Time: 16:14
 */

namespace app\controllers;

use Yii;
use app\components\AuthController;
use app\services\Mail as MailService;
use yii\filters\AccessControl;

class MailController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['send'], //only be applied to
            'rules' => [[
                'actions' => ['send'],
                'allow'   => true,
                'roles'   => ['SPAM_ACCESS']
            ]]
        ];

        return $behaviors;
    }

	public function actionSend()
	{
		$params = Yii::$app->getRequest()->post();
		$mailService = new MailService();

		return [
			'success' => $mailService->sendMail($params)
		];
	}
}
