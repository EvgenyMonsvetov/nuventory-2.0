<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 29/05/2018
 * Time: 15:43
 */

namespace app\controllers;

use app\models\Company;
use app\models\Location;
use app\models\Vendor;
use \Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use app\services\Vendor as VendorService;

class VendorController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'edit', 'delete', 'master-vendors', 'parts', 'export'], //only be applied to
            'rules' => [[
                'actions' => ['view', 'master-vendors', 'parts', 'export'],
                'allow'   => true,
                'roles'   => ['VENDORS_READ']
            ],[
                'actions' => ['index'],
                'allow'   => true,
                'roles'   => ['EDIT_INVENTORY', 'VENDORS_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['VENDORS_ADD']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['VENDORS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $vendor = new Vendor();
        $dataProvider = $vendor->getAllVendors($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'items' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $vendor = new Vendor();
        $dataProvider = $vendor->getVendor($id);
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $vendorService = new VendorService();
        $errors = $vendorService->createVendor( $data );

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $vendorService = new VendorService();
        $errors = $vendorService->deleteVendor( $id );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

    public function actionMasterVendors()
    {
        $request = Yii::$app->request;
        $vendor = new Vendor();
        $dataProvider = $vendor->getAllMasterVendors($request->getQueryParams());
        $dataProvider->query->asArray();

        $user = Yii::$app->user->getIdentity();
        $company = Company::findOne(['CO1_ID' => $user->CO1_ID]);
        $fromLocation = Location::findOne(['LO1_ID' => $user->LO1_ID]);

        return [
            'data' => [
                'company' => $company,
                'fromLocation' => $fromLocation,
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionParts()
    {
        $request = Yii::$app->request;
        $model = new Vendor();
        $dataProvider = $model->getAllParts($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new VendorService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}