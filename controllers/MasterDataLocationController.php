<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 08/11/2018
 * Time: 11:06
 */

namespace app\controllers;

use app\components\AuthController;
use app\models\Inventory;
use app\models\Location;
use app\models\MasterData;
use app\models\MasterDataLocation;
use app\models\Vendor;
use yii\filters\AccessControl;
use Yii;

class MasterDataLocationController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'], //only be applied to
            'rules' => [[
                'actions' => ['index'],
                'allow'   => true,
                'roles'   => ['DASHBOARD_READ']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $masterDataLocation = new MasterDataLocation();
        $dataProvider = $masterDataLocation->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionUpdateInventory()
    {
        $items = Yii::$app->request->post();

        $user = Yii::$app->user->getIdentity();

        $location = Location::findOne(['LO1_ID' => $user->LO1_ID]);

        $errors = [];
        foreach($items as $item){

            $masterDataLocation = MasterDataLocation::findOne([
                'MD1_ID' => $item['MD1_ID'],
                'LO1_ID' => $user->LO1_ID,
                'MD2_DELETE_FLAG' => 0]);

            if(!$masterDataLocation){
                $masterDataLocation = new MasterDataLocation();
                $masterDataLocation->LO1_ID          = $user->LO1_ID;
                $masterDataLocation->CO1_ID          = $user->CO1_ID;
                $masterDataLocation->load($item);
            }

            $ORIGINAL_ON_HAND_QTY = $masterDataLocation->MD2_ON_HAND_QTY;
            $MD2_AVAILABLE_QTY = $location->LO1_CENTER_FLAG ?
                $masterDataLocation->MD2_AVAILABLE_QTY + ($item['MD2_ON_HAND_QTY'] - $ORIGINAL_ON_HAND_QTY) :
                $item['MD2_ON_HAND_QTY'];

            $vendor = Vendor::findOne(['VD1_ID' => $item['VD1_ID']]);
            $masterDataLocation->load([
                'VD1_ID'            => $item['VD1_ID'],
                'MD2_ACTIVE'        => $item['MD2_ACTIVE'] ? '1' : '0',
                'MD2_ON_HAND_QTY'   => $item['MD2_ON_HAND_QTY'],
                'MD2_MIN_QTY'       => $item['MD2_MIN_QTY'],
                'MD2_AVAILABLE_QTY' => $MD2_AVAILABLE_QTY,
                'MD2_RACK'          => $item['MD2_RACK'],
                'MD2_DRAWER'        => $item['MD2_DRAWER'],
                'MD2_BIN'           => $item['MD2_BIN'],
                'MD2_NO_INVENTORY'  => $vendor ? $vendor->VD1_NO_INVENTORY : 0
            ], '');

            if(!$masterDataLocation->save(true)){
                $errors += $masterDataLocation->getErrorSummary(true);
            }else{
                $LG1_ADJ_QTY = $masterDataLocation->MD2_ON_HAND_QTY - $ORIGINAL_ON_HAND_QTY;
                if($LG1_ADJ_QTY != 0){
                    $inventory = new Inventory();
                    $inventory->CO1_ID = $user->CO1_ID;
                    $inventory->LO1_ID = $user->LO1_ID;
                    $inventory->TE1_ID = '';
                    $inventory->MD1_ID = $item['MD1_ID'];
                    $inventory->MD2_ID = $masterDataLocation->MD2_ID;
                    $inventory->JT2_ID = '';
                    $inventory->OR2_ID = '';
                    $inventory->PO2_ID = '';
                    $inventory->MD1_PART_NUMBER = $item['MD1_PART_NUMBER'];
                    $inventory->MD1_DESC1 = $item['MD1_DESC1'];
                    $inventory->LG1_DESC  = 'Inventory edited manually';
                    $inventory->LG1_UNIT_COST = $item['MD1_REC_PRICE'];
                    $inventory->LG1_EXTENDED_COST = abs($LG1_ADJ_QTY) * $item['MD1_REC_PRICE'];
                    $inventory->LG1_OLD_QTY = $ORIGINAL_ON_HAND_QTY;
                    $inventory->LG1_NEW_QTY = $masterDataLocation->MD2_ON_HAND_QTY;
                    $inventory->LG1_ADJ_QTY = $LG1_ADJ_QTY;
                    $inventory->save();
                }
            }

	        $masterData = MasterData::findOne([
		        'MD1_ID' => $item['MD1_ID'],
		        'MD1_DELETE_FLAG' => 0]);

	        if($masterData){
		        $masterData->C00_ID = $item['C00_ID'];
		        $masterData->CA1_ID = $item['CA1_ID'];
		        $masterData->MD1_UNIT_PRICE = $item['MD1_UNIT_PRICE'];
		        $masterData->is_edit_inventory = true;
	        }

	        if(!$masterData->save(true)){
		        $errors += $masterData->getErrorSummary(true);
	        }
        }

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }
}
