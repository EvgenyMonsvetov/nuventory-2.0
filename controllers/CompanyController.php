<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 29/05/2018
 * Time: 15:43
 */

namespace app\controllers;

use app\models\Company;
use app\services\Company as CompanyService;
use Yii;
use app\components\AuthController;
use yii\filters\AccessControl;

class CompanyController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'delete', 'export', 'preload', 'company-location', 'last-short-code', 'create-companies'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'export', 'preload', 'company-location', 'last-short-code', 'create-companies'],
                'allow'   => true,
                'roles'   => ['COMPANIES_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['COMPANIES_ADD']
            ],[
                'actions' => ['update'],
                'allow'   => true,
                'roles'   => ['COMPANIES_EDIT']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['COMPANIES_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionView($id)
    {
        $company = new Company();
        $query = $company->getSearchQuery(['CO1_ID' => $id]);
        $query->asArray();
        $data = $query->one();

        return [
            'data' => $data
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $company = new Company();
        $dataProvider = $company->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();
        $company = new Company();
        //$company->setScenario(Company::SCENARIO_ADD );

        if( isset($data['CO1_ID'])){
            unset($data['CO1_ID']);
        }

        if( $company->load($data, '') && $company->save( true ) ){
            return [
                'success' => true,
                'data'    => $company
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $company->getErrorSummary(true)
            ];
        }
    }

	public function actionCreateCompanies()
	{
		$errors = [];
		$items = [];
		$data = Yii::$app->request->post();

		foreach ($data as $item) {
			$company = new Company();
			if( isset($item['CO1_ID'])){
				unset($item['CO1_ID']);
			}

			if ( !($company->load($item, '') && $company->save( true )) ) {
				$errors[] = $company->getErrorSummary(true);
				$item['errors'] = $company->getErrorSummary(true);
			}
			$items[] = $item;
		}

		return [
			'data' => $errors ? $items : [],
			'success' => !$errors,
			'errors'    => $errors
		];
	}

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $company = Company::findOne(['CO1_ID' => $data['CO1_ID']]);

        if( $company->load($data, '') && $company->save( true ) ){
            return [
                'success' => true,
                'data'    => $company
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $company->getErrorSummary(true)
            ];
        }
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $company = Company::findOne(['CO1_ID' => $data['id']]);
        $company->CO1_DELETE_FLAG = 1;
        $company->save(false);

        return [
            'success' => true
        ];
    }

	public function actionPreload()
	{
		$request = Yii::$app->getRequest();

		$country = new \app\models\Country();
		$countries = $country->search( $request->getQueryParams() );
		$countries->query->asArray();

		return [
			'countries' => $countries->getModels()
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new CompanyService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}

	public function actionCompanyLocation()
	{
		$request = Yii::$app->getRequest();

		$company = new Company();
		$dataProvider = $company->getCompanyLocation( $request->getQueryParams() );
		$dataProvider->query->asArray();

		return [
			'data'  => $dataProvider->getModels()
		];
	}

	public function actionLastShortCode()
	{
		$company = new Company();
		$model = $company->getCompanyLastShortCode();

		return [
			'CO1_SHORT_CODE'  => $model['CO1_SHORT_CODE']
		];
	}
}