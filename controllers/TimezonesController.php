<?php

namespace app\controllers;

use yii\filters\AccessControl;
use app\components\AuthController;
use Yii;

class TimezonesController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'],
            'rules' => [[
                'actions' => ['index'],
                'allow'   => true,
                'roles'   => ['TIMEZONES_READ']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $timezoneService = new \app\services\Timezone();
        $timezones = $timezoneService->getAllTimezones();

        return $timezones;
    }
}
