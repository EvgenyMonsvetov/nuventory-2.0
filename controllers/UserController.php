<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 30/05/2018
 * Time: 09:45
 */

namespace app\controllers;

use app\components\AuthController;
use app\models\Group;
use app\models\User;
use app\services\User as UserService;
use yii\filters\AccessControl;
use Yii;
use yii\web\ForbiddenHttpException;

class UserController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'change-password', 'admin-change-password', 'delete', 'access-tree', 'export', 'preload', 'create-users'],
            'rules' => [[
                'actions' => ['index', 'export', 'preload', 'create-users'],
                'allow'   => true,
                'roles'   => ['USERS_READ'],
            ],[
                'actions' => ['view'],
                'allow'   => true,
                'roles'   => ['@'],
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['USERS_ADD'],
            ],[
                'actions' => ['update', 'change-password'],
                'allow'   => true,
                'roles'   => ['@'],
            ],[
                'actions' => ['update', 'admin-change-password'],
                'allow'   => true,
                'roles'   => ['USERS_EDIT'],
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['USERS_DELETE']
            ],[
                'actions' => ['access-tree'],
                'allow'   => true,
                'roles'   => ['PROFILE_READ']
            ]]
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $user = new User();
        $dataProvider = $user->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    /**
     * @SWG\Get(path="/users/{id}",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *          name="id",
     *           required=true,
     *          in="path",
     *           description="UserId",
     *          type="integer"
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Returns user info"
     *     )
     * )
     */
    public function actionView($id)
    {
	    $userId = Yii::$app->user->identity->getId();

	    if (intval($id) === $userId || Yii::$app->user->can('PROFILE_READ')) {
		    $user = new User();
		    $query = $user->getSearchQuery(['US1_ID' => $id]);
		    $query->asArray();

		    return [
			    'data' => $query->one()
		    ];
	    } else {
		    throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
	    }
    }

    private function _prepareUser($user)
    {
        unset($user->US1_PASS);
        unset($user->US1_AUTH_KEY);
        unset($user->US1_PASSWORD_HASH);
        unset($user->US1_PASSWORD_RESET_TOKEN);

        return $user;
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $user = new User();
        $user->setScenario(User::SCENARIO_ADD_USER);
	    $errors = [];

	    $currentUser = Yii::$app->user->getIdentity();
	    $data['US1_CREATED_BY'] = $currentUser->US1_ID;
	    $data['US1_MODIFIED_BY'] = $currentUser->US1_ID;

        if( $user->load($data, '') && $user->validate() ){
            $user->setPassword($user->password);
            $user->generateAuthKey();

            if($user->save( false )){
                $this->_prepareUser($user);

                /*
                $userProfile = new UserProfile();
                $userProfile->load([
                    'user_id' => $user->getId(),
                    'UP1_RESTRICT_COMPANIES' => 1,
                    'UP1_RESTRICT_CUSTOMERS' => 0,
                    'UP1_RESTRICT_VENDORS'   => 0
                ], '');
                if($userProfile->save()){
                    if(!empty($data['role'])){
                        $authManager = Yii::$app->getAuthManager();
                        $role = $authManager->getRole($data['role']);
                        $authManager->assign($role, $user->getId());
                    }
                    return $this->_prepareUser($user);
                }else{
                    return [
                        'success' => false,
                        'errors'  => $userProfile->getErrors()
                    ];
                }
                */
            } else {
	            $errors = $user->getErrorSummary(true);
            }
        } else {
	        $errors = $user->getErrorSummary(true);
        }

	    return [
		    'success' => !$errors,
		    'errors'  => $errors
	    ];
    }

    public function actionAdminChangePassword()
    {
        $data = Yii::$app->request->post();

        $user = User::findOne(['US1_ID' => $data['userid']]);
        $user->setScenario(User::SCENARIO_ADMIN_CHANGE_PASSWORD);
        if($user->load($data, '') && $user->validate()) {
            $user->setPassword($user->new_password);
            if($user->save(false)) {
                if(Yii::$app->request->post('notify')) {
                    Yii::$app->mailer->compose()
                        ->setFrom( Yii::$app->params['mailer']['sender'] )
                        ->setTo($user->US1_EMAIL )
                        ->setSubject(Yii::$app->request->post('subject'))
                        ->setTextBody(Yii::$app->request->post('body'))
                        ->send();
                }
                return $this->_prepareUser($user);
            }
        } else {
            return [
                'success' => false,
                'errors'  => $user->getErrors()
            ];
        }
    }

    public function actionCreateUsers()
    {
	    $errors = [];
	    $items = [];
	    $data = Yii::$app->request->post();

	    foreach ($data as $item) {
		    $user = new User();
		    $user->setScenario(User::SCENARIO_ADD_USER);

		    if( $user->load($item, '') && $user->validate() ){
			    $user->setPassword($user->password);
			    $user->generateAuthKey();

			    if(!$user->save( false )) {
				    $errors[] = $user->getErrors();
				    $item['errors'] =  $user->getErrors();
			    }
		    } else {
			    $errors[] = $user->getErrors();
			    $item['errors'] =  $user->getErrors();
		    }
		    $items[] = $item;
	    }

	    return [
	    	'data' => $errors ? $items : [],
		    'success' => !$errors,
		    'errors'    => $errors
	    ];
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $user = User::findOne(['US1_ID' => $data['US1_ID']]);
        $user->setScenario(User::SCENARIO_CHANGE_USER);

        $oldGroup = Group::findOne(['GR1_ID' => $user->GR1_ID]);

        if( $user->load($data, '') && $user->validate() ){
            if($user->save( false )){
            	$auth = Yii::$app->getAuthManager();
            	$role = $auth->getRole($oldGroup['ROLE_NAME']);
            	if ($role) {
		            $auth->revoke($role, $user->US1_ID);
	            }

	            $newGroup = Group::findOne(['GR1_ID' => $data['GR1_ID']]);
	            $role = $auth->getRole($newGroup['ROLE_NAME']);
	            if ($role) {
		            $auth->assign($role, $user->US1_ID);
	            }

	            return [
                    'success' => true,
                    'data'    => $this->_prepareUser($user)
                ];
            }
        }

        return [
            'success' => false,
            'errors'  => $user->errors
        ];
    }

    public function actionChangePassword()
    {
        $data = Yii::$app->request->post();

        $user =User::findOne(['US1_ID'=>$data['userid']]);
        $user->setScenario(User::SCENARIO_CHANGE_PASSWORD);

	    $currentUserId = Yii::$app->user->identity->getId();
	    
	    if (intval($user['US1_ID']) === $currentUserId || Yii::$app->user->can('USERS_EDIT')) {
		    if( $user->load($data, '') && $user->validate() ){
			    $user->setPassword($user->new_password);
			    if($user->save( false )){
				    return $this->_prepareUser($user);
			    }
		    }else{
			    return [
				    'success' => false,
				    'errors'  => $user->getErrors()
			    ];
		    }
	    } else {
		    throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
	    }
    }

    public function actionDelete()
    {
	    $data = Yii::$app->request->getQueryParams();

        $auth = Yii::$app->getAuthManager();
        $auth->revokeAll( $data['id'] );

        $user = User::findOne(['US1_ID' => $data['id']]);
        $user->delete();

        return [
            'success' => true
        ];
    }

	public function actionPreload()
	{
		$request = Yii::$app->getRequest();

		$company = new \app\models\Company();
		$companies = $company->search( $request->getQueryParams() );
		$companies->query->asArray();

		$location = new \app\models\Location();
		$locations = $location->search( $request->getQueryParams() );
		$locations->query->asArray();

		$group = new \app\models\Group();
		$groups = $group->search( $request->getQueryParams() );
		$groups->query->asArray();

		return [
			'companies' => $companies->getModels(),
			'shops'   => $locations->getModels(),
			'groups' => $groups->getModels()
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new UserService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}