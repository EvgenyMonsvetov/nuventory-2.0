<?php

namespace app\controllers;

use app\services\JobTicket as JobTicketService;
use \Yii;
use app\components\AuthController;
use app\services\AccessList;
use app\models\AccessList as AccessListModel;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\UploadedFile;
use app\services\JobLine as JobLineService;
use app\models\JobLine;

class JobLineController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'delete'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view'],
                'allow'   => true,
                'roles'   => ['JOB_TICKETS_READ']
            ],[
                'actions' => ['create'],
                'allow' => true,
                'roles' => ['JOB_TICKETS_EDIT']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['JOB_TICKETS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new JobLine();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new JobLine();
        $dataProvider = $model->getJobLine($id, $request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $service = new JobLineService();
        $errors = $service->createJobLine( $data );

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $transaction = Yii::$app->db->beginTransaction();
        $errors = [];
        try{
            $jobLineModel = JobLine::find()->where(["JT2_ID" => $id])->one();
            if ($jobLineModel->delete() === false) {
                throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
            } else {
                $jobTickerService = new JobTicketService();
	            $result = $jobTickerService->updateTotals($jobLineModel['JT1_ID']);
	            $errors = $result['errors'];
            }
            $transaction->commit();
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
            $transaction->rollBack();
        }
        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }
}