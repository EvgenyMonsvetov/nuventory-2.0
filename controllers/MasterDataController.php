<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 08/11/2018
 * Time: 11:06
 */

namespace app\controllers;

use app\components\AuthController;
use app\models\C00Category;
use app\models\Category;
use app\models\MasterData;
use app\models\MeasurementUnit;
use app\models\Number;
use DateTime;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use Yii;
use yii\helpers\Url;
use app\services\MasterData as MasterDataService;
use app\components\PdfTrait;
use app\pdfs\Pdf;

class MasterDataController extends AuthController
{
	use PdfTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'create-multiple', 'update', 'delete', 'delete-multiple', 'update-parts', 'print-pdf', 'print-qty-sheet', 'print-rack', 'print-checkout-sheet', 'print-inventory-optimization', 'export', 'inventory-optimization', 'preload', 'tech-usage', 'tech-usage-by-months', 'quick-add-parts', 'print-below-minimum', 'voc-usage', 'print-usage', 'projected-po-usage'],
            'rules' => [[
                'actions' => ['index', 'view', 'print-pdf', 'print-qty-sheet', 'print-rack', 'print-checkout-sheet', 'print-inventory-optimization', 'export', 'inventory-optimization', 'preload', 'tech-usage', 'tech-usage-by-months'],
                'allow'   => true,
                'roles'   => ['MASTER_DATA_READ']
            ],[
                'actions' => ['create', 'create-multiple', 'update-parts'],
                'allow'   => true,
                'roles'   => ['MASTER_DATA_ADD']
            ],[
                'actions' => ['quick-add-parts'],
                'allow'   => true,
                'roles'   => ['EDIT_INVENTORY', 'MASTER_DATA_READ']
            ],[
                'actions' => ['update'],
                'allow'   => true,
                'roles'   => ['MASTER_DATA_EDIT']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['MASTER_DATA_DELETE']
            ],[
	            'actions' => ['print-below-minimum'],
	            'allow'   => true,
	            'roles'   => ['BELOW_MINIMUM_REPORT']
            ],[
	            'actions' => ['voc-usage', 'print-usage'],
	            'allow'   => true,
	            'roles'   => ['VOC_USAGE_READ']
            ],[
	            'actions' => ['projected-po-usage'],
	            'allow'   => true,
	            'roles'   => ['PROJECTED_PO_USAGE_READ']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $masterData = new MasterData();
        $dataProvider = $masterData->search( $request->getQueryParams() );
        $dataProvider->query->asArray();
        $models = $dataProvider->getModels();

        $arr = array();
        foreach ($models as $model) {
            $arr[] = $this->populate($model);
	    }

        return [
            'data'  => $arr,
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionView($id)
    {
        $masterData = new MasterData();
        $query = $masterData->getSearchQuery(['MD1_ID' => $id, 'editEnventory' => 1]);
        $query->asArray();
        $data = $query->one();

        return [
            'data' => $this->populate($data)
        ];
    }

    public function populate($data) {
        $basePath = isset(Yii::$app->params['partsImagesPath']) ? Yii::$app->params['partsImagesPath'] . DIRECTORY_SEPARATOR : '';
	    $basePath = str_replace('web/', '', $basePath);

        if ( $data['FI1_FILE_PATH']) {
        	if (file_exists( $basePath . $data['FI1_FILE_PATH'])) {
		        $data['FI1_FILE_PATH_EXIST'] = true;
	        } else {
		        $data['FI1_FILE_PATH_EXIST'] = false;
	        }
	        $data['FI1_FILE_PATH'] = Url::base(true) . DIRECTORY_SEPARATOR . $basePath . $data['FI1_FILE_PATH'];
        } else {
	        $data['FI1_FILE_PATH'] = '';
	        $data['FI1_FILE_PATH_EXIST'] = false;
        }

        return $data;
    }

	public function actionPreload()
	{
		$request = Yii::$app->getRequest();

		$vendor = new \app\models\Vendor();
		$vendors = $vendor->getAllVendors( $request->getQueryParams() );
		$vendors->query->asArray();

		$unit= new MeasurementUnit();
		$units = $unit->search([]);
		$units->query->asArray();

		$c00Category= new C00Category();
		$masterCategories = $c00Category->search( $request->getQueryParams() );
		$masterCategories->query->asArray();

		$сategory= new Category();
		$categories = $сategory->search($request->getQueryParams());
		$categories->query->asArray();

		$location = new \app\models\Location();
		$locations = $location->search( $request->getQueryParams() );
		$locations->query->asArray();

		return [
			'vendors' => $vendors->getModels(),
			'units' => $units->getModels(),
			'masterCategories' => $masterCategories->getModels(),
			'categories' => $categories->getModels(),
			'shops' => $locations->getModels()
		];
	}

    public function actionCreate()
    {
        $data = Yii::$app->request->post();
        $MD1_ID = Yii::$app->request->post('MD1_ID');

        $masterDataService = new \app\services\MasterData();
        $errors = [];

        $result = $masterDataService->save( $data, $MD1_ID );
        if($result['errors']){
            $errors += $result['errors'];
        }

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

	public function actionCreateMultiple()
	{
		$data = Yii::$app->request->post();
		$masterDataService = new MasterDataService();
		$result = $masterDataService->createMultiple($data['MD0_IDs']);

		return [
			'success' => !$result['errors'],
			'errors' => $result['errors']
		];
	}

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();
        $MD1_ID = Yii::$app->request->post('MD1_ID');

        $masterDataService = new \app\services\MasterData();
        $errors = [];

        $result = $masterDataService->save($data, $MD1_ID);
        if($result['errors']){
            $errors += $result['errors'];
        }

        return [
            'data' => [
                'success' => !$errors,
                'data' => $result['data'],
                'errors' => $errors
            ]
        ];
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $masterData = MasterData::findOne(['MD1_ID' => $data['id']]);
        $masterData->MD1_DELETE_FLAG = 1;
        $masterData->save(false);

        return [
            'success' => true
        ];
    }

    public function actionCentralOrderParts() {
        $request = Yii::$app->request;

        $masterData = new MasterData();
        $parts = $masterData->getCentralOrderParts($request->getQueryParams());
        $parts->query->asArray();

        $number = new Number();
        $num = $number->getNumber();
        $num->query->asArray();

        return [
            'parts'      => $parts->getModels(),
            'OR1_NUMBER' => $num->getModels()[0]['OR1_NUMBER']
        ];
    }

    public function actionAutoOrderParts() {

        $request = Yii::$app->request;
        $filters = $request->getQueryParams();

        $autoOrder = $filters['AUTO_ORDER_TYPE'];
        $masterData = new MasterData();
        $query = $masterData->getNewOrderParts($filters, $autoOrder);
        $query->asArray();

        $pagination = false;
        if((bool)$request->getQueryParam('usePagination')){
            $pagination = [
                'pageSize' => $request->getQueryParam('perPage'),
                'page'     => $request->getQueryParam('page')
            ];
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => $pagination
        ]);

        $models = $dataProvider->getModels();
        $parts = array();
        foreach ($models as $model) {
            $parts[] = $this->populate($model);
        }

        return [
            'parts' => $parts
        ];
    }

    public function actionFullParts() {
        $request = Yii::$app->request;

        $masterData = new MasterData();
        $query = $masterData->getNewOrderParts($request->getQueryParams());
        $query->asArray();

        $pagination = false;
        if((bool)$request->getQueryParam('usePagination')){
            $pagination = [
                'pageSize' => $request->getQueryParam('perPage'),
                'page'     => $request->getQueryParam('page')
            ];
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => $pagination
        ]);

        $models = $dataProvider->getModels();
        $parts = array();
        foreach ($models as $model) {
            $parts[] = $this->populate($model);
        }

        return [
            'data'  => $parts,
            'count' => $dataProvider->getTotalCount()
        ];
    }

	public function actionQuickAddParts()
	{
		$filters = Yii::$app->request->getQueryParams();

		$masterData = new MasterData();
		$parts = $masterData->getQuickAddParts($filters);

		return [
			'parts' => $parts
		];
	}

    public function actionEditInventoryFullPart() {
       $request = Yii::$app->request;

        $masterData = new MasterData();
        $dataProvider = $masterData->getFullPartForEditInventory($request->getQueryParams());
        $dataProvider->query->asArray();

        $models = $dataProvider->getModels();
        $parts = array();
        foreach ($models as $model) {
            $parts[] = $this->populate($model);
        }

        return [
            'data'  => $parts
        ];
    }

    public function actionCreditMemoFullPart() {
       $request = Yii::$app->request;

        $masterData = new MasterData();
        $dataProvider = $masterData->getFullPartForCreditMemo($request->getQueryParams());
        $dataProvider->query->asArray();

        $models = $dataProvider->getModels();
        $parts = array();
        foreach ($models as $model) {
            $parts[] = $this->populate($model);
        }

        return [
            'data'  => $parts
        ];
    }

    public function actionUpdateParts()
    {
        $data = Yii::$app->request->post();

        $masterCatalogService = new MasterDataService();
        $errors = $masterCatalogService->updateParts( $data );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

    public function actionPrintPdf()
    {
        $request = Yii::$app->getRequest();
	    $params = $request->getQueryParams();

	    $pdfService = new Pdf();
	    $pdf = $pdfService->printMasterData($params);

	    $filePrefix = str_replace('.pdf', '', $params['fileName']);
	    $currDate = Date('Ymd_His');
	    $filename = $filePrefix . '_' . $currDate . '.pdf';

	    $this->sendPdfFile($pdf, $filename);
    }

    public function actionPrintQtySheet()
    {
	    $request = Yii::$app->getRequest();
	    $params = $request->getQueryParams();

	    $pdfService = new Pdf();
	    $pdf = $pdfService->generatePrintQtySheet($params);

	    $filePrefix = str_replace('.pdf', '', $params['fileName']);
	    $currDate = Date('Ymd_His');
	    $filename = $filePrefix . '_' . $currDate . '.pdf';

	    $this->sendPdfFile($pdf, $filename);
    }

	public function actionPrintRack()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$masterDataService = new MasterDataService();
		return $masterDataService->printRackHTML($params);
	}

	public function actionPrintCheckoutSheet() {
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printCheckoutSheets($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}

	public function actionPrintInventoryOptimization() {
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printInventoryOptimization($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}

	public function actionPrintBelowMinimum() {
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->prinBelowMinimum($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}

	public function actionPrintUsage()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printUsage($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new MasterDataService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}

	public function actionInventoryOptimization()
    {
		$request = Yii::$app->getRequest();

		$params = $request->getQueryParams();

        if (isset($params['REPORT_TYPE']) && $params['REPORT_TYPE'] == 'inventoryOptimization') {
            $startDate = new DateTime($params['from']);
            $endDate   = new DateTime($params['to']);
            $dateDiff  = $startDate->diff($endDate);
            $months = $dateDiff->y * 12 + $dateDiff->m + 1;
            $params['months'] = $months;
        }

		$masterData = new MasterData();
		$query = $masterData->getInventoryOptimizationQuery( $params );
		$query->asArray();

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => [
				'pageSize' => $params['perPage'],
				'page'     => $params['page']
			]
		]);

		$totalsQuery = $masterData->getInventoryOptimizationQuery( $params, true);
		$totals = $totalsQuery->asArray()->one();

		return [
			'data'  => $dataProvider->getModels(),
			'count' => $dataProvider->getTotalCount(),
			'totalValue' => $totals['EXTENDED_STOCK'],
			'totalUsage' => isset($totals['EXTENDED_PRICE']) ? $totals['EXTENDED_PRICE'] : 0
		];
	}

	public function actionCheckDuplicate()
    {
        $request = Yii::$app->getRequest();

		$masterData = new MasterData();
		$num = $masterData->checkDuplicate( $request->getQueryParams() );

		return [
			'num' => $num['NUM']
		];
    }

	public function actionTechUsage()
	{
		$request = Yii::$app->getRequest();

		$masterDataService = new MasterDataService();
		$result = $masterDataService->getTechUsage( $request->getQueryParams() );

		return [
			'status' => '200',
			'statusText' => 'success',
			'series' => isset($result['data']) ? $result['data']: [],
			'drilldown' => isset($result['drilldown']) ? $result['drilldown']: [],
			'title' => isset($result['title']) ? $result['title'] : '',
		];
	}

	public function actionTechUsageByMonths()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$masterDataService = new MasterDataService();
		$result = $masterDataService->getTechUsageByMonths($params);

		return [
			'data'  => $result
		];
	}

	public function actionDeleteMultiple()
    {
        $MD1_IDs = Yii::$app->getRequest()->post('MD1_IDs');

        foreach( $MD1_IDs as $MD1_ID ){
            $masterData = MasterData::findOne(['MD1_ID' => $MD1_ID]);
            $masterData->MD1_DELETE_FLAG = 1;
            $masterData->save(false);
        }

        return [
            'success' => true
        ];
    }

	public function actionVocUsage()
	{
		$request = Yii::$app->getRequest();

		$params = $request->getQueryParams();

		$masterData = new MasterData();
		$query = $masterData->getVOCUsageQuery( $params );

		return [
			'data' => $query->asArray()->all()
		];
	}

	public function actionProjectedPoUsage()
	{
		$request = Yii::$app->getRequest();

		$masterDataService = new MasterDataService();
		$result = $masterDataService->getProjectedPOUsage( $request->getQueryParams() );

		return [
			'status' => '200',
			'statusText' => 'success',
			'series' => isset($result['data']) ? $result['data']: [],
			'today' => isset($result['today']) ? $result['today']: 31,
			'totalDays' => isset($result['totalDays']) ? $result['totalDays']: 31,
			'budget' => isset($result['budget']) ? $result['budget']: 0,
			'projectedAmount' => isset($result['projectedAmount']) ? $result['projectedAmount']: 0,
			'correlationData' => isset($result['correlationData']) ? $result['correlationData']: [],
		];
	}
}
