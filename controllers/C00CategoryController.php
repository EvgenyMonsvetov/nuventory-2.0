<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 24/10/2018
 * Time: 15:04
 */

namespace app\controllers;

use app\models\C00Category;
use app\services\C00Category as C00CategoryService;
use Yii;
use app\components\AuthController;
use yii\filters\AccessControl;

class C00CategoryController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'delete', 'delete-multiple', 'export'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'export'],
                'allow'   => true,
                'roles'   => ['NUVENTORY_CATEGORIES_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['NUVENTORY_CATEGORIES_ADD']
            ],[
                'actions' => ['update'],
                'allow'   => true,
                'roles'   => ['NUVENTORY_CATEGORIES_EDIT']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['NUVENTORY_CATEGORIES_DELETE']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $c00Category = new C00Category();
        $dataProvider = $c00Category->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionView($id)
    {
        $c00Category = new C00Category();
        $query = $c00Category->getSearchQuery(['C00_ID' => $id]);
        $query->asArray();
        $data = $query->one();

        return [
            'data' => $data
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        if( isset($data['C00_ID'])){
            unset($data['C00_ID']);
        }

        $c00Category = new C00Category();
        $c00Category->setScenario(C00Category::SCENARIO_ADD );

        if( $c00Category->load($data, '') && $c00Category->save( true ) ){
            return [
                'success' => true,
                'data'    => $c00Category
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $c00Category->getErrorSummary(true)
            ];
        }
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $c00Category = C00Category::findOne(['C00_ID' => $data['C00_ID']]);

        if( $c00Category->load($data, '') && $c00Category->save( true ) ){
            return [
                'success' => true,
                'data'    => $c00Category
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $c00Category->getErrorSummary(true)
            ];
        }
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $measurementUnit = C00Category::findOne(['C00_ID' => $data['id']]);
        $measurementUnit->C00_DELETE_FLAG = 1;
        $measurementUnit->save(false);

        return [
            'success' => true
        ];
    }

	public function actionDeleteMultiple()
	{
		$C00_IDs = Yii::$app->getRequest()->post('C00_IDs');

		foreach ( $C00_IDs as $C00_ID ) {
			$c00CategoryModel = C00Category::findOne(['C00_ID' => $C00_ID]);
			$c00CategoryModel->C00_DELETE_FLAG = 1;
			$c00CategoryModel->save(false);
		}

		return [
			'success' => true
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new C00CategoryService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}