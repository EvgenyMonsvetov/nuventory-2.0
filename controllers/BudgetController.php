<?php

namespace app\controllers;

use app\components\AuthController;
use app\models\Budget;
use yii\filters\AccessControl;
use Yii;

class BudgetController extends AuthController
{
	public function behaviors()
	{
		$behaviors = parent::behaviors();

		$behaviors['access'] = [
			'class' => AccessControl::className(),
			'only' => ['dashboard-charts', 'materials-budget', 'index', 'create', 'update', 'view', 'delete-multiple'],
			'rules' => [
                [
                    'actions' => ['index', 'view'],
                    'allow'   => true,
                    'roles'   => ['BUDGETS_READ']
                ],
                [
                    'actions' => ['create'],
                    'allow'   => true,
                    'roles'   => ['BUDGETS_ADD']
                ],
                [
                    'actions' => ['update'],
                    'allow'   => true,
                    'roles'   => ['BUDGETS_EDIT']
                ],
                [
                    'actions' => ['delete-multiple'],
                    'allow'   => true,
                    'roles'   => ['BUDGETS_DELETE']
                ]
            ]
		];

		return $behaviors;
	}

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $budget = new Budget();
        $dataProvider = $budget->search( $request->getQueryParams() );
        $dataProvider->query->asArray();
        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionCreate() {
        $data = Yii::$app->request->post();
        $budget = new Budget();
        $data['BU1_FINAL_RESULT'] = ($data['BU1_TARGET_PERCENTAGE'] > 100)?100:($data['BU1_MONTHLY_GROSS_SALE']*$data['BU1_TARGET_PERCENTAGE']/100);
        $budget->load($data, '');

        $errors = [];
        if(!$budget->save( true ) ){
            $errors =  $budget->getErrorSummary(true);
        }
        return [
            'data' => [
                'success' => !$errors,
                'errors'  => $errors
            ]
        ];
    }


    public function actionUpdate() {
        $data = Yii::$app->request->post();
        $budget = Budget::find()->where(['BU1_ID' => $data['BU1_ID']])->one();
        $data['BU1_FINAL_RESULT'] = $data['BU1_MONTHLY_GROSS_SALE']*$data['BU1_TARGET_PERCENTAGE']/100;
        $budget->load($data, '');

        $errors = [];
        if(!$budget->update( true ) ){
            $errors =  $budget->getErrorSummary(true);
        }
        return [
            'data' => [
                'success' => !$errors,
                'errors'  => $errors
            ]
        ];
    }

    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        return [
            'data' => Budget::find()->where(['BU1_ID' => $id])->one()
        ];
    }

    public function actionDeleteMultiple() {
	    $success = false;
        $BU1_IDs = Yii::$app->getRequest()->post('BU1_IDs');
        foreach ($BU1_IDs as $BU1_ID) {
            $budget = Budget::findOne(['BU1_ID' => $BU1_ID]);
            if ($budget) {
                $budget = $budget->deleteItem();
                if (!$budget->hasErrors() && !$success) {
                    $success = true;
                }
            }
        }
        return [
            'success' => $success
        ];
    }
	
}
