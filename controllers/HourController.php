<?php

namespace app\controllers;

use app\components\AuthController;
use app\models\Hour;
use app\models\Technician;
use app\services\Hour as HourService;
use yii\filters\AccessControl;
use Yii;

class HourController extends AuthController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'create', 'update', 'preload', 'delete'],
            'rules' => [[
                'actions' => ['index', 'preload'],
                'allow'   => true,
                'roles'   => ['HOURS_READ']
            ],[
                'actions' => ['create', 'update-multiple', 'delete'],
                'allow'   => true,
                'roles'   => ['HOURS_EDIT']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $hour = new Hour();
        $dataProvider = $hour->getAllHours( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionCreate()
    {
	    $params = Yii::$app->request->post();

	    $hourService = new HourService();
	    return $hourService->createHours( $params );
    }

    public function actionTechHours()
    {
	    $request = Yii::$app->getRequest();
        $endDate   = date('Y-m-t');
		$startDate = date('Y-m-d', strtotime(date('Y-m-d').' -11 month'));

        $technicianModel = new Hour();
        $query = $technicianModel->getTechHours($startDate, $endDate, $request->getQueryParams());

        return [
            'data'  => $query->asArray()->all()
        ];
    }

    public function actionUpdateMultiple()
    {
	    $params = Yii::$app->request->post();
        $errors = [];

	    if(!empty($params['data']) && is_array($params['data'])){
            $hourService = new \app\services\Hour();
            $result = $hourService->saveMultiple( $params['data']);

            if(!$result['success']){
                $errors = $result['errors'];
            }
        }

        return [
			'success' => !$errors,
            'errors'  => $errors
		];
    }

	public function actionPreload()
	{
		$request = Yii::$app->getRequest();

		$technician = new \app\models\Technician();
		$technicians = $technician->getAllTechnicians( $request->getQueryParams() );
		$technicians->query->asArray();

		return [
			'technicians' => $technicians->getModels()
		];
	}

    public function actionDelete()
    {
        $id = Yii::$app->request->post('id');
        $hourService = new HourService();
        $errors = $hourService->delete( $id );

        if(!$errors){
            Yii::$app->getResponse()->setStatusCode(204);
        }
        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }
}
