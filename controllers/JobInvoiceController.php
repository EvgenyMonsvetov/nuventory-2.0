<?php

namespace app\controllers;

use \Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use app\services\JobInvoice as JobInvoiceService;
use app\models\JobInvoice;
use app\components\PdfTrait;
use app\pdfs\Pdf;

class JobInvoiceController extends AuthController
{
	use PdfTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'delete', 'preload', 'export', 'print-pdf'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'preload', 'export', 'print-pdf'],
                'allow'   => true,
                'roles'   => ['JOB_INVOICES_READ']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['JOB_INVOICES_READ']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new JobInvoice();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new JobInvoice();
        $dataProvider = $model->getJobInvoice($id);
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $service = new JobInvoiceService();
        $errors = $service->deleteJobInvoice( $id );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

    public function actionPreload()
    {
        $request = Yii::$app->getRequest();

        $user = new \app\models\User();
        $users = $user->search( $request->getQueryParams() );
        $users->query->asArray();

        $location = new \app\models\Location();
        $locations = $location->search( $request->getQueryParams() );
        $locations->query->asArray();

        return [
            'users'     => $users->getModels(),
            'shops'   => $locations->getModels()
        ];
    }

    public function actionExport()
    {
        $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $service = new JobInvoiceService();

        switch ($params['type']) {
            case 'xls':
            default:
                return $service->generateXls($params);
        }
    }

	public function actionPrintPdf()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printJobInvoices($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}
}