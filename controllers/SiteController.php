<?php

namespace app\controllers;

use app\models\Company;
use app\models\Group;
use app\models\Language;
use app\components\AuthController;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use app\models\LoginForm;


/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="54.189.248.193",
 *     host="nuventory",
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="EDI API 2",
 *         description="Api description...",
 *         termsOfService="",
 *     ),
 *     @SWG\SecurityScheme(
 *         securityDefinition="Bearer",
 *         type="apiKey",
 *         name="Authorization",
 *         in="header"
 *     )
 * )
 */


class SiteController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $protectedActions = ['refresh-token', 'set-company', 'set-location'];
        if( !in_array($this->action->id, $protectedActions)){
            unset($behaviors['authenticator']);
        }

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'logout', 'index', 'login',
                'language', 'country',
                'refresh-token', 'set-company', 'set-location'
            ],
            'rules' => [
                [
                    'actions' => ['index', 'login', 'language', 'country'],
                    'allow'   => true,
                    'roles'   => ['?'],
                ], [
                    'actions' => ['logout', 'refresh-token', 'set-company', 'set-location'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'only' => ['login', 'language', 'country', 'refresh-token', 'set-company', 'set-location'],
            'formats' => [
               'application/json' => Response::FORMAT_JSON
            ]];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $this->enableCsrfValidation = false;

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
         $this->layout = 'app';
         return $this->render('angular', [
             'angularPath' => Yii::getAlias('@webroot')
                 . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'index.php'
         ]);
    }

    /**
     * @SWG\Post(path="/login",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="username",
     *           required=true,
     *          in="formData",
     *           description="Username",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *           name="password",
     *            required=true,
     *           in="formData",
     *            description="Password",
     *           type="string"
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Authorization Token"
     *     )
     * )
     */
    public function actionLogin()
    {
        Yii::$app->response->getHeaders()->add(
            'Access-Control-Allow-Origin', '//nuventory'
        );

        $post = Yii::$app->request->post();

        $loginForm = new LoginForm();
        $loginForm->load($post, '');

        Yii::$app->session->set('LOGIN_ACTION', 1);
        if ( $loginForm->login()) {
            $user = Yii::$app->user->getIdentity();
            $this->_prepareUser($user);

            $company = new Company();
            $companyProvider = $company->search();
            $companyProvider->query->asArray();

            return [
                'user'      => $user,
                'companies' => $companyProvider->getModels()
            ];
        }else{
            return [
                'error' => '"User Name" or "Password" you provided is incorrect. Please try again."'
            ];
        }
    }

    public function actionRefreshToken()
    {
        $user = Yii::$app->user->getIdentity();
        $user->generateCredentionals( Yii::$app->user->getId() );
        $this->_prepareUser($user);
        return $user;
    }

    private function _prepareUser($user){
        unset($user->US1_PASS);
        unset($user->US1_AUTH_KEY);
        unset($user->US1_PASSWORD_HASH);
        unset($user->US1_PASSWORD_RESET_TOKEN);
    }

    public function actionSetCompany()
    {
        $data = Yii::$app->request->post();
        $user = Yii::$app->user->getIdentity();
        $CO1_ID = isset($data['CO1_ID']) ? $data['CO1_ID'] : null;

        if( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
            $user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ){
            $session = Yii::$app->session;
            $session->set('CO1_ID', $CO1_ID);
            $session->set('LO1_ID', null);
        }

        return ['success' => true];
    }

    public function actionSetLocation()
    {
        $data = Yii::$app->request->post();
        $user = Yii::$app->user->getIdentity();
        $LO1_ID = isset($data['LO1_ID']) ? $data['LO1_ID'] : null;

        if( $user->group->GR1_ACCESS_LEVEL == Group::GROUP_ADMIN ||
            $user->group->GR1_ACCESS_LEVEL == Group::GROUP_MANUFACTURER_MANAGER ||
            $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_MANAGER ||
            $user->group->GR1_ACCESS_LEVEL == Group::GROUP_COMPANY_GROUP_MANAGER ){
            $session = Yii::$app->session;
            $session->set('LO1_ID', $LO1_ID);
        }

        return ['success' => true];
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionTest()
    {

    }

    public function actionLanguage()
    {
        $languages = Language::find()
                ->select(['LA1_NAME', 'LA1_ID', 'LA1_LOCALE'])
                ->where(['=', 'LA1_DELETE_FLAG', 0])->asArray()->all();

        $currentLocale = Yii::$app->getRequest()->post('locale');
        if(!$currentLocale || $currentLocale == 'null' ){
            $preferredLanguages = array_map( function($item){
                return $item['LA1_LOCALE'];
            }, $languages);
            $currentLocale = Yii::$app->getRequest()->getPreferredLanguage($preferredLanguages);
        }

        $language = new Language();
        $currentVocabulary = $language->getVocabulary( $currentLocale );

        $defaultLocale = 'en-us';
        $defaultVocabulary = [];
        if($currentLocale != $defaultLocale){
            $defaultVocabulary = $language->getVocabulary( $defaultLocale );
        }

        return [
            'data' => [
                'languages'         => $languages,
                'defaultLocale'     => $defaultLocale,
                'currentLocale'     => $currentLocale,
                'currentVocabulary' => $currentVocabulary,
                'defaultVocabulary' => $defaultVocabulary
            ]
        ];
    }

}
