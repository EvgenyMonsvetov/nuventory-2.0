<?php

namespace app\controllers;

use app\components\AuthController;
use app\models\Prices;
use app\services\Price as PriceService;
use yii\filters\AccessControl;
use Yii;

class PriceController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'export'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'export'],
                'allow'   => true,
                'roles'   => ['DASHBOARD_READ']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $price = new Prices();
        $models = $price->search( $request->getQueryParams() );

        return [
            'data' => $models
        ];
    }

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new PriceService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}
