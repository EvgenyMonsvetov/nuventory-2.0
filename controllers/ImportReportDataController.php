<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 12/11/2018
 * Time: 16:14
 */

namespace app\controllers;

use app\components\AuthController;
use app\models\Import;
use app\models\Upload;
use yii\filters\AccessControl;
use Yii;
use yii\web\UploadedFile;

class ImportReportDataController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'], //only be applied to
            'rules' => [[
                'actions' => ['index'],
                'allow'   => true,
                'roles'   => ['IMPORT_REPORT_DATA_READ']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $import = new Import();
        $dataProvider = $import->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionImportJobCostSummary()
    {
        $errors = [];

        $files = UploadedFile::getInstancesByName('fileKey');
        $post  = Yii::$app->request->post();

        if ( $files && $files[0] ) {
            $import = new \app\services\Import();
		    $import->importReportData($files[0], $post['fileName'], $post, Import::JOB_COST_SUMMARY);
        }else {
		    $errors = ['Nothing to save'];
	    }

        return [
            'data' => [
                'imageUrl' => '',
                'success' => !$errors,
                'errors' => $errors
            ]
        ];
    }

    public function actionImportSalesJournal()
    {
        $errors = [];

        $files    = UploadedFile::getInstancesByName('fileKey');
        $data     = Yii::$app->request->post();

	    if ( $files && $files[0] ) {
		    $importService = new \app\services\Import();
		    $result = $importService->importReportData($files[0], $data['fileName'], $data, Import::SALES_JOURNAL_REPORT);
		    if ($result['errors']) {
			    $errors = $result['errors'];
		    }
	    } else {
		    $errors = ['Nothing to save'];
	    }

        return [
            'data' => [
                'success' => !$errors,
                'errors' => $errors
            ]
        ];
    }
}
