<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 13/06/2018
 * Time: 14:16
 */

namespace app\controllers;

use Yii;
use app\components\AuthController;
use yii\filters\AccessControl;

class UserRolesController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create'],
            'rules' => [
                [
                    'actions' => ['index', 'view', 'create'],
                    'allow' => true,
                    'roles' => ['USERS_EDIT']
                ],
            ]
        ];

        return $behaviors;
    }

    public function actionView($id)
    {
        $roles = Yii::$app->getAuthManager()->getRolesByUser($id);
        return [
            'data' => array_values($roles)
        ];
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $auth = Yii::$app->getAuthManager();

        $role = $auth->getRole($data['rolename']);
        $auth->revoke($role, $data['userid']);

        $roles = Yii::$app->getAuthManager()->getRolesByUser($data['userid']);
        return [
            'data' => array_values($roles)
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $auth = Yii::$app->getAuthManager();
        $role = $auth->getRole($data['rolename']);

        $auth->assign($role, $data['userid']);
        $roles = Yii::$app->getAuthManager()->getRolesByUser($data['userid']);

        return [
            'data' => array_values($roles)
        ];
    }
}
