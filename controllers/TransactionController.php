<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 29/05/2018
 * Time: 15:43
 */

namespace app\controllers;

use app\models\Company;
use app\models\EdiType;
use app\models\vLog;
use app\models\vTransaction;
use app\models\Vendor;
use Yii;
use app\components\AuthController;
use app\models\Customer;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

class TransactionController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'report', 'details-report', 'in-out-bound-report', 'trading-partners-report', 'preload', 'logs', 'resending', 'resend', 'import'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'report', 'details-report', 'in-out-bound-report', 'trading-partners-report', 'preload', 'logs'],
                'allow'   => true,
                'roles'   => ['TRANSACTIONS_READ']
            ],[
                'actions' => ['resend'],
                'allow'   => true,
                'roles'   => ['TRANSACTIONS_RESEND']
            ],[
                'actions' => ['import', 'resending'],
                'allow'   => true,
                'roles'   => ['IMPORT_DATA']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $params = $request->getQueryParams();
        if( !empty($params['CU1_ID']) ){
            foreach( $params['CU1_ID'] as $index=>$value ){
                $params['CU1_ID'][$index] = Json::decode( $value );
            }
        }

        if( !empty($params['VD1_ID']) ){
            foreach( $params['VD1_ID'] as $index=>$value ){
                $params['VD1_ID'][$index] = Json::decode( $value );
            }
        }

        $transaction = new vTransaction();
        $transaction->setScenario(vTransaction::SCENARIO_REPORT);

        $dataProvider = $transaction->search( $params );

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    /**
     * @SWG\Post(path="/transactions/import",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *          name="data",
     *           required=true,
     *          in="body",
     *           description="Data upload",
     *          @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Import transactions"
     *     )
     * )
     */
    public function actionImport()
    {
        $data = Yii::$app->getRequest()->post();

        $vendorService = new \app\services\Transaction();
        $result = $vendorService->import( $data );

        return [
            'data' => $result
        ];
    }

    /**
     * @SWG\Post(path="/transactions/resending",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *           name="data",
     *           required=true,
     *           in="body",
     *           description="Data upload",
     *           @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Get resending transactions"
     *     )
     * )
     */
    public function actionResending()
    {
        $data = Yii::$app->getRequest()->post();

        $companyService = new \app\services\Company();
        $company = $companyService->registerCompany( $data['CompanyName'] );

        $transactionService = new \app\services\Transaction();
        $result = $transactionService->getResending( $company->CO1_ID );

        return [
            'data'  => $result,
            'count' => count($result)
        ];
    }

    public function actionReport()
    {
        $request = Yii::$app->getRequest();

        $transactionService = new \app\services\Transaction();
        $data = $transactionService->getDetailsReport( $request->getQueryParams() );

        return [
            'data' => $data
        ];
    }

    public function actionSeries()
    {
        $request = Yii::$app->getRequest();

        $data = $request->post();

        foreach( $data['serie'] as &$serie){
            $transactionService = new \app\services\Transaction();
            $items = $transactionService->getSerie( $serie );
            $serie['data'] = $items;
        }

        return $data;
    }

    public function actionDetailsReport()
    {
        $request = Yii::$app->getRequest();

        $transactionService = new \app\services\Transaction();
        $data = $transactionService->getDetailsReport( $request->getQueryParams() );

        return [
            'data' => $data
        ];
    }

    public function actionInOutBoundReport()
    {
        $request = Yii::$app->getRequest();

        $transactionService = new \app\services\Transaction();
        $data = $transactionService->getInOutBoundReport( $request->getQueryParams() );

        return [
            'data' => array_values($data)
        ];
    }

    public function actionTradingPartnersReport()
    {
        $request = Yii::$app->getRequest();

        $transactionService = new \app\services\Transaction();
        $data = $transactionService->getTradingPartnersReport( $request->getQueryParams() );

        return [
            'data' => array_values($data)
        ];
    }

    public function actionPreload()
    {
        $profile = Yii::$app->user->getIdentity()->userProfile;

        $customerService = new \app\services\Customer();
        $customers = $customerService->getAll( $profile->UP1_INCOGNITO_MODE );

        $vendorService = new \app\services\Vendor();
        $vendors = $vendorService->getAll( $profile->UP1_INCOGNITO_MODE );

        $companyService = new \app\services\Company();
        $companies = $companyService->getAll( $profile->UP1_INCOGNITO_MODE );

        return [
            'ediTypes'  => EdiType::find()->orderBy(['ET1_NAME' => SORT_ASC])->asArray()->all(),
            'customers' => $customers,
            'vendors'   => $vendors,
            'companies' => ArrayHelper::toArray($companies)
        ];
    }

    public function actionLogs()
    {
        $log = new vLog();
        $dataProvider = $log->search( Yii::$app->getRequest()->getQueryParams() );

        return [
            'data' => $dataProvider->getModels()
        ];
    }

    public function actionResend()
    {
        $data = Yii::$app->getRequest()->post();
        $items = Json::decode($data['items']);

        $transactionService = new \app\services\Transaction();
        $result = $transactionService->resend( $items );

        return [
            'data' => $result
        ];
    }
}