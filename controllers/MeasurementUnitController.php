<?php

namespace app\controllers;

use app\components\AuthController;
use app\models\MeasurementUnit;
use app\services\MeasurementUnit as MeasurementUnitService;
use yii\filters\AccessControl;
use Yii;

class MeasurementUnitController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create','update', 'delete', 'delete-multiple', 'export'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'export'],
                'allow'   => true,
                'roles'   => ['UNITS_MEASURE_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['UNITS_MEASURE_ADD']
            ],[
                'actions' => ['update'],
                'allow'   => true,
                'roles'   => ['UNITS_MEASURE_ADD']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['UNITS_MEASURE_DELETE']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $measurementUnit = new MeasurementUnit();
        $dataProvider = $measurementUnit->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionView($id)
    {
        $measurementUnit = new MeasurementUnit();
        $query = $measurementUnit->getSearchQuery(['UM1_ID' => $id]);
        $query->asArray();
        $data = $query->one();

        return [
            'data' => $data
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        if( isset($data['UM1_ID'])){
            unset($data['UM1_ID']);
        }

        $measurementUnit = new MeasurementUnit();

        if( $measurementUnit->load($data, '') && $measurementUnit->save( true ) ){
            return [
                'success' => true,
                'data'    => $measurementUnit
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $measurementUnit->getErrorSummary(true)
            ];
        }
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $measurementUnit = MeasurementUnit::findOne(['UM1_ID' => $data['UM1_ID']]);

        if( $measurementUnit->load($data, '') && $measurementUnit->save( true ) ){
            return [
                'success' => true,
                'data'    => $measurementUnit
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $measurementUnit->getErrorSummary(true)
            ];
        }
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $measurementUnit = MeasurementUnit::findOne(['UM1_ID' => $data['id']]);
        $measurementUnit->UM1_DELETE_FLAG = 1;
        $measurementUnit->save(false);

        return [
            'success' => true
        ];
    }

	public function actionDeleteMultiple()
	{
		$UM1_IDs = Yii::$app->getRequest()->post('UM1_IDs');

		foreach ( $UM1_IDs as $UM1_ID ) {
			$orderHeaderModel = MeasurementUnit::findOne(['UM1_ID' => $UM1_ID]);
			$orderHeaderModel->UM1_DELETE_FLAG = 1;
			$orderHeaderModel->save(false);
		}

		return [
			'success' => true
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new MeasurementUnitService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}
