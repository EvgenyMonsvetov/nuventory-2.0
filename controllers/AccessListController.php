<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 17/08/2018
 * Time: 15:36
 */


namespace app\controllers;

use Yii;
use app\components\AuthController;
use app\services\AccessList;
use app\models\AccessList as AccessListModel;
use yii\filters\AccessControl;

class AccessListController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'access', 'access-update'], //only be applied to
            'rules' => [[
                'actions' => ['index'],
                'allow'   => true,
                'roles'   => ['COMPANIES_READ']
            ],[
                'actions' => ['access', 'access-update'],
                'allow'   => true,
                'roles'   => ['COMPANIES_ACCESS']
            ]]
        ];

        return $behaviors;
    }


    public function actionAccess($id)
    {
        $accessListService = new AccessList();
        $items = $accessListService->getAccessList( AccessListModel::OBJ_TYPE_COMPANY, $id );

        return [
            'data' => $items
        ];
    }

    public function actionAccessUpdate( $id )
    {
        $users = Yii::$app->getRequest()->post();

        $accessListService = new AccessList();
        $errors = $accessListService->updateAccessList( AccessListModel::OBJ_TYPE_COMPANY, $id, $users );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }
}