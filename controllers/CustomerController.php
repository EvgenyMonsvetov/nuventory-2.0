<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 29/05/2018
 * Time: 15:43
 */

namespace app\controllers;

use app\models\Customer;
use Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use app\services\AccessList;
use app\services\Customer as CustomerService;
use app\models\AccessList as AccessListModel;

class CustomerController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'delete', 'access', 'access-update', 'export', 'generate-short-code'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'export'],
                'allow'   => true,
                'roles'   => ['CUSTOMERS_READ']
            ],[
                'actions' => ['create', 'generate-short-code'],
                'allow'   => true,
                'roles'   => ['CUSTOMERS_ADD']
            ],[
                'actions' => ['update'],
                'allow'   => true,
                'roles'   => ['CUSTOMERS_EDIT']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['CUSTOMERS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $customer = new Customer();
        $dataProvider = $customer->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionView($id)
    {
        $сustomer = new Customer();
        $query = $сustomer->getSearchQuery(['CU1_ID' => $id]);
        $query->asArray();
        $data = $query->one();

        return [
            'data' => $data
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        if( isset($data['CU1_ID'])){
            unset($data['CU1_ID']);
        }

        $сustomer = new Customer();
        if( $сustomer->load($data, '') && $сustomer->save( true ) ){
            return [
                'success' => true,
                'data'    => $сustomer
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $сustomer->getErrorSummary(true)
            ];
        }
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $сustomer = Customer::findOne(['CU1_ID' => $data['CU1_ID']]);

        if( $сustomer->load($data, '') && $сustomer->save( true ) ){
            return [
                'success' => true,
                'data'    => $сustomer
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $сustomer->getErrorSummary(true)
            ];
        }
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $сustomer = Customer::findOne(['CU1_ID' => $data['id']]);
        $сustomer->CU1_DELETE_FLAG = 1;
        $сustomer->save(false);

        return [
            'success' => true
        ];
    }

    public function actionGenerateShortCode()
    {
        $customerService = new CustomerService();
        $CU1_SHORT_CODE = $customerService->generateShortCode();
        return [
            'data' => [
                'CU1_SHORT_CODE' => $CU1_SHORT_CODE
            ]
        ];
    }

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new CustomerService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}