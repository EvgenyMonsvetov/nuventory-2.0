<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 11/06/2018
 * Time: 11:20
 */

namespace app\controllers;

use Yii;
use app\components\AuthController;
use yii\filters\AccessControl;

class PermissionController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'], //only be applied to
            'rules' => [
                [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['@']
                ],
            ]
        ];

        return $behaviors;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $permissions = Yii::$app->getAuthManager()->getPermissions();
        return [
            'data'  => array_values($permissions),
            'count' => count($permissions)
        ];
    }
}