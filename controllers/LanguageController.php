<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 16/07/2018
 * Time: 11:41
 */

namespace app\controllers;

use app\models\Log;
use yii\data\ActiveDataProvider;
use Yii;
use app\components\AuthController;

class LanguageController extends AuthController
{
    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $query = Log::find()->limit($request->get('perPage'));
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $request->get('perPage')
            ],
            'sort' => [
                'defaultOrder' => [
                    'LOG_ID' => SORT_DESC
                ]
            ]
        ]);

        return [
            'data' => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    /**
     * @SWG\Post(path="/logs/import",
     *     produces={"application/json"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(
     *          name="data",
     *           required=true,
     *          in="body",
     *           description="Data upload",
     *          @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "Import transactions"
     *     )
     * )
     */
    public function actionImport()
    {
        $data = Yii::$app->getRequest()->post();

        $logService = new \app\services\Log();
        $result = $logService->import($data);

        return [
            'data' => $result
        ];
    }
}