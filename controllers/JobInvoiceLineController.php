<?php

namespace app\controllers;

use \Yii;
use app\components\AuthController;
use app\services\AccessList;
use app\models\AccessList as AccessListModel;
use yii\filters\AccessControl;
use app\models\JobInvoiceLine;

class JobInvoiceLineController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index'], //only be applied to
            'rules' => [[
                'actions' => ['index'],
                'allow'   => true,
                'roles'   => ['INVOICES_READ']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new JobInvoiceLine();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

}