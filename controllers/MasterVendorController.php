<?php

namespace app\controllers;

use app\models\MasterVendor;
use \Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use app\services\MasterVendor as MasterVendorService;
use yii\web\UploadedFile;

class MasterVendorController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'edit', 'delete', 'delete-multiple', 'parts', 'export', 'delete-master-vendor-parts', 'delete-all-images', 'upload-archive-images'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'parts', 'export', 'delete-master-vendor-parts', 'delete-all-images', 'upload-archive-images'],
                'allow'   => true,
                'roles'   => ['MASTER_VENDORS_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['MASTER_VENDORS_ADD']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['MASTER_VENDORS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new MasterVendor();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new MasterVendor();
        $dataProvider = $model->getMasterVendor($id);
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $service = new MasterVendorService();
        $errors = $service->createMasterVendor( $data );

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $service = new MasterVendorService();
        $errors = $service->deleteMasterVendor( $id );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

	public function actionDeleteMultiple()
	{
		$VD0_IDs = Yii::$app->getRequest()->post('VD0_IDs');

		foreach ( $VD0_IDs as $VD0_ID ) {
			$masterVendorModel = MasterVendor::findOne(['VD0_ID' => $VD0_ID]);
			$masterVendorModel->VD0_DELETE_FLAG = 1;
			$masterVendorModel->save(false);
		}

		return [
			'success' => true
		];
	}

    public function actionParts()
    {
        $request = Yii::$app->request;
        $model = new MasterVendor();
        $dataProvider = $model->getAllParts($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new MasterVendorService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}

	public function actionDeleteMasterVendorParts()
	{
		$data = Yii::$app->request->post();
		$masterVendorService = new MasterVendorService();

		return $masterVendorService->deleteMasterVendorParts($data);
	}

	public function actionDeleteAllImages()
	{
		$data = Yii::$app->request->post();
		$masterVendorService = new MasterVendorService();

		return $masterVendorService->deleteAllImages($data);
	}

	public function actionUploadArchiveImages()
	{
		$zipFiles = UploadedFile::getInstancesByName('fileKey');
		$data = Yii::$app->request->post();
		$masterVendorService = new MasterVendorService();

		return $masterVendorService->uploadArchiveImages($data, $zipFiles[0]);
	}
}