<?php

namespace app\controllers;

use app\components\PdfTrait;
use app\models\Company;
use app\models\Technician;
use app\pdfs\Pdf;
use \Yii;
use app\components\AuthController;
use app\services\AccessList;
use app\models\AccessList as AccessListModel;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\UploadedFile;
use app\services\Technician as TechnicianService;

class TechnicianController extends AuthController
{
	use PdfTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'edit', 'delete', 'export', 'preload', 'print-pdf', 'last-short-code'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'export', 'preload', 'print-pdf', 'last-short-code'],
                'allow'   => true,
                'roles'   => ['TECHNICIANS_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['TECHNICIANS_ADD']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['TECHNICIANS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $technician = new Technician();
        $dataProvider = $technician->getAllTechnicians($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $technician = new Technician();
        $dataProvider = $technician->getTechnician($id);
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $technicianService = new TechnicianService();
        return $technicianService->createTechnician( $data );
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $technicianService = new TechnicianService();
        $errors = $technicianService->deleteTechnician( $id );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

	public function actionPreload()
	{
		$request = Yii::$app->getRequest();

		$company = new \app\models\Company();
		$companies = $company->search( $request->getQueryParams() );
		$companies->query->asArray();

		$location = new \app\models\Location();
		$locations = $location->search( $request->getQueryParams() );
		$locations->query->asArray();

		return [
			'companies' => $companies->getModels(),
			'shops'   => $locations->getModels()
		];
	}

	public function actionLastShortCode()
	{
		$user = Yii::$app->user->getIdentity();
		$technician = Technician::find()->select(['MAX(CAST(TE1_SHORT_CODE AS UNSIGNED)) AS TE1_SHORT_CODE'])->where(['=', 'CO1_ID', $user->CO1_ID])->asArray()->one();
		if($technician && $technician['TE1_SHORT_CODE']){
		    $TE1_SHORT_CODE = $technician['TE1_SHORT_CODE'];
        }else{
		    $TE1_SHORT_CODE = Technician::LAST_SHORT_CODE_START;
		}
		return ['TE1_SHORT_CODE' => $TE1_SHORT_CODE];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new TechnicianService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}

	public function actionPrintPdf()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printTechnician($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}
}