<?php

namespace app\controllers;

use app\services\Role;
use Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;

class RoleController extends AuthController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'delete', 'update', 'create'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view'],
                'allow' => true,
                'roles' => ['ROLES_READ']
            ],[
                'actions' => ['update'],
                'allow' => true,
                'roles' => ['ROLES_EDIT']
            ],[
                'actions' => ['create'],
                'allow' => true,
                'roles' => ['ROLES_ADD']
            ],[
                'actions' => ['delete'],
                'allow' => true,
                'roles' => ['ROLES_DELETE']
            ]]
        ];

        return $behaviors;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $roles = Yii::$app->getAuthManager()->getRoles();
        return [
            'data'  => array_values($roles),
            'count' => count($roles)
        ];
    }

    public function actionView($id)
    {
        $roleService = new Role();
        $role = $roleService->getRole($id);

        return [
            'data' => $role
        ];
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $auth = Yii::$app->getAuthManager();
        $role = $auth->getRole( trim($data['name']) );
        $role->description = $data['description'];
        $auth->update($data['name'], $role);

        $auth->removeChildren($role);

        foreach($data['permissions'] as $permission){
            if($permission['hasPermission']){
                $authPermission = $auth->getPermission($permission['name']);
                $auth->addChild($role, $authPermission);
            }
        }

        $roleService = new Role();
        $updatedRole = $roleService->getRole($data['name']);

        return [
            'data' => $updatedRole
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $auth = Yii::$app->getAuthManager();
        $role = $auth->createRole( trim($data['name']) );
        $role->description = $data['description'];
        $auth->add($role);

        foreach($data['permissions'] as $permission){
            if($permission['hasPermission']){
                $authPermission = $auth->getPermission($permission['name']);
                $auth->addChild($role, $authPermission);
            }
        }

        $roleService = new Role();
        $updatedRole = $roleService->getRole($data['name']);

        return [
            'data' => $updatedRole
        ];
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();
        $auth = Yii::$app->getAuthManager();
        $role = $auth->getRole($data['id']);
        $auth->remove( $role );

        return [
            'success' => true
        ];
    }
}
