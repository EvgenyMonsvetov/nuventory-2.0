<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 16/07/2018
 * Time: 11:41
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\AuthController;
use app\models\Language as LanguageModel;
use app\services\Language as LanguageService;

class LanguagesController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'delete', 'export', 'export'],
            'rules' => [[
                'actions' => ['index', 'view', 'export', 'export'],
                'allow'   => true,
                'roles'   => ['LANGUAGES_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['LANGUAGES_ADD']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['LANGUAGES_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $language = new LanguageModel();

        if(!empty($params) && !empty($params['ids'])) {
            $keys = $params['ids'];
            $dataProvider = $language->getLanguage($keys);
        } else {
            $dataProvider = $language->getAllLanguages($params);
        }

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                 'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $language = new LanguageModel();
        $dataProvider = $language->getLanguage($id);
        $data = $dataProvider->query->one();

        return [
            'data' => [
                $data
            ]
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $languageService = new LanguageService();
        $errors = $languageService->createLanguage( $data );

        return [
            'data' => [
                'success'=> !$errors,
                'data' => $data,
                'errors' => $errors
            ]
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $languageService = new LanguageService();
        $errors = $languageService->deleteLanguage( $id );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new LanguageService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}