<?php

namespace app\controllers;

use app\models\OrderLine;
use app\services\OrderHeader as OrderHeaderService;
use \Yii;
use app\components\AuthController;
use yii\filters\AccessControl;

class OrderLineController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'delete', 'preload', 'update'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'preload'],
                'allow'   => true,
                'roles'   => ['ORDER_LINE_READ']
            ],[
                'actions' => ['delete', 'update'],
                'allow'   => true,
                'roles'   => ['ORDER_LINE_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new OrderLine();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new OrderLine();
        $dataProvider = $model->getOrderLine($id);
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
	    $orderLineModel = OrderLine::find()->where(["OR2_ID" => $id])->one();
	    if ($orderLineModel->delete()) {
		    $orderHeaderService = new OrderHeaderService();
		    $result = $orderHeaderService->updateTotals($orderLineModel['OR1_ID']);
		    $errors = $result['errors'];
	    } else {
		    $errors = $orderLineModel->getErrorSummary(true);
	    }

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

    public function actionUpdate() {
        $data = Yii::$app->request->post();
        $category = OrderLine::findOne(['OR2_ID' => $data['OR2_ID']]);
        $um2Factor = $data['UM2_FACTOR']??1;
        $data['OR2_ORDER_QTY'] = $data['OR2_RECEIVING_QTY']/$um2Factor;

        if( $category->load($data, '') && $category->save( true ) ){
            return [
                'success' => true,
                'data'    => $category
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $category->getErrorSummary(true)
            ];
        }
    }

}