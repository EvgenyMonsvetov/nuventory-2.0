<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 12/11/2018
 * Time: 16:14
 */

namespace app\controllers;

use app\components\AuthController;
use app\models\RawSalesDataStoreMonth as RawSalesDataStoreMonthModel;
use app\services\RawSalesDataStoreMonth as RawSalesDataService;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use Yii;

class RawSalesDataStoreMonthController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'sales', 'gross-profit', 'summary', 'summary-chart', 'pm-gross-profit', 'high-bar-chart', 'average-circle-chart'], //only be applied to
            'rules' => [[
                'actions' => ['index'],
                'allow'   => true,
                'roles'   => ['IMPORT_REPORT_DATA_READ']
            ],
            [
	            'actions' => ['sales'],
	            'allow'   => true,
	            'roles'   => ['SALES_READ']
            ],
            [
	            'actions' => ['gross-profit'],
	            'allow'   => true,
	            'roles'   => ['GROSS_PROFIT_READ']
            ],
            [
	            'actions' => ['summary', 'summary-chart'],
	            'allow'   => true,
	            'roles'   => ['SUMMARY_READ']
            ],
            [
	            'actions' => ['pm-gross-profit'],
	            'allow'   => true,
	            'roles'   => ['PM_GROSS_PROFIT_BY_SHOP']
            ],
            [
	            'actions' => ['high-bar-chart'],
	            'allow'   => true,
	            'roles'   => ['PAINT_MATERIALS_USAGE_TOTAL']
            ],
            [
	            'actions' => ['average-circle-chart'],
	            'allow'   => true,
	            'roles'   => ['PAINT_LABOR_VS_SALES_READ', 'PAINT_MATERIALS_GP_READ']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $params = Yii::$app->getRequest()->getQueryParams();

        $defaultOrder = [];
        if(!empty($params['sort'])){
            foreach( $params['sort'] as $sort){
                list($prop, $dir) = explode(',', $sort);
                $defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
            }
        }else{
            $defaultOrder['id'] = SORT_DESC;
        }

        $rawSalesDataStoreMonthModel = new RawSalesDataStoreMonthModel();
        $query = $rawSalesDataStoreMonthModel->getQuery( $params );

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => !empty($params['perPage']) ? $params['perPage'] : null
            ],
            'sort' => [
                'defaultOrder' => $defaultOrder
            ]
        ]);

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

	public function actionSales()
	{
		$request = Yii::$app->getRequest();

		$reportValues = RawSalesDataStoreMonthModel::getValuesForReport(RawSalesDataStoreMonthModel::REPORT_TYPE_GROSS_AMOUNT);
		$attributeForReport = $reportValues['attribute'];

		$rawSalesDataService = new RawSalesDataService();
		$result = $rawSalesDataService->getMonthlyChart( $request->getQueryParams(), $attributeForReport );

		return [
			'status' => '200',
			'statusText' => 'success',
			'categories' => $result['categories'],
			'series' => $result['series'],
			'target' => $result['target'],
			'attributeType' => $result['attributeType'],
			'attributeTypeSymbol' => $result['attributeTypeSymbol'],
			'title' => isset($result['title']) ? $result['title'] : '',
			'yax' => isset($result['yax']) ? $result['yax'] : ''
		];
	}

	public function actionGrossProfit()
	{
		$request = Yii::$app->getRequest();

		$reportValues = RawSalesDataStoreMonthModel::getValuesForReport(RawSalesDataStoreMonthModel::REPORT_TYPE_GROSS_PROFIT_PERCENT);
		$attributeForReport = $reportValues['attribute'];

		$rawSalesDataService = new RawSalesDataService();
		$result = $rawSalesDataService->getMonthlyChart( $request->getQueryParams(), $attributeForReport );

		return [
			'status' => '200',
			'statusText' => 'success',
			'categories' => $result['categories'],
			'series' => $result['series'],
			'target' => $result['target'],
			'attributeType' => $result['attributeType'],
			'attributeTypeSymbol' => $result['attributeTypeSymbol'],
			'title' => isset($result['title']) ? $result['title'] : '',
			'yax' => isset($result['yax']) ? $result['yax'] : ''
		];
	}

	public function actionSummary()
	{
		$request = Yii::$app->getRequest();
		$rawSalesDataService = new RawSalesDataService();
		return $rawSalesDataService->getSummaryReportSheetData($request->getQueryParams());
	}

	public function actionSummaryChart()
	{

		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$attributeForReport = $params['attribute_code'];

		$rawSalesDataService = new RawSalesDataService();
		$result = $rawSalesDataService->getMonthlyChart($params , $attributeForReport );

		return [
			'status' => '200',
			'statusText' => 'success',
			'categories' => $result['categories'],
			'series' => $result['series'],
			'target' => $result['target'],
			'attributeType' => $result['attributeType'],
			'attributeTypeSymbol' => $result['attributeTypeSymbol'],
			'title' => isset($result['title']) ? $result['title'] : '',
			'yax' => isset($result['yax']) ? $result['yax'] : ''
		];
	}

	public function actionPmGrossProfit()
	{
		$request = Yii::$app->getRequest();

		$rawSalesDataService = new RawSalesDataService();
		$result = $rawSalesDataService->getPieChartDataInRange( $request->getQueryParams(), 'r03_paint_and_materials_profit');

		return [
			'status' => '200',
			'statusText' => 'success',
			'series' => $result
		];
	}

	public function actionHighBarChart()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$attributeForReport = $params['attribute_code'];

		$rawSalesDataService = new RawSalesDataService();
		$result = $rawSalesDataService->getMonthlyChart($params , $attributeForReport );

		return [
			'status' => '200',
			'statusText' => 'success',
			'categories' => $result['categories'],
			'series' => $result['series'],
			'target' => $result['target'],
			'attributeType' => $result['attributeType'],
			'attributeTypeSymbol' => $result['attributeTypeSymbol'],
			'title' => isset($result['title']) ? $result['title'] : '',
			'yax' => isset($result['yax']) ? $result['yax'] : ''
		];
	}

	public function actionAverageCircleChart()
	{
		$request = Yii::$app->getRequest();

		$rawSalesDataService = new RawSalesDataService();
		$result = $rawSalesDataService->getAverageValueForCircleChart($request->getQueryParams());

		return [
			'status' => '200',
			'statusText' => 'success',
			'series' => $result['series'],
			'data' => $result['data']
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$rawSalesDataService = new RawSalesDataService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $rawSalesDataService->generateXls($params, $params['subtype']);
		}
	}
}
