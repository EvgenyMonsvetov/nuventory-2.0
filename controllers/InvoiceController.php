<?php

namespace app\controllers;

use \Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use app\services\Invoice as InvoiceService;
use app\models\Invoice;
use app\models\Company;
use app\models\Location;
use app\models\MasterData;
use app\services\Number as NumberService;
use app\components\PdfTrait;
use app\pdfs\Pdf;

class InvoiceController extends AuthController
{
	use PdfTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'delete', 'delete-multiple', 'preload', 'credit-memos', 'credit-memo', 'export', 'print-pdf', 'generate-all-invoices'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'preload', 'credit-memos', 'credit-memo', 'export', 'print-pdf', 'generate-all-invoices'],
                'allow'   => true,
                'roles'   => ['INVOICES_READ']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['INVOICES_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new Invoice();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new Invoice();
        $dataProvider = $model->getInvoice($id);
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

	public function actionCreate()
	{
		$data = Yii::$app->request->post();
		$IN1_NUMBER = $data['IN1_NUMBER'];
		$invoiceService = new \app\services\Invoice();
		$errors = [];
		foreach($data['orders'] as $order){
			$result = $invoiceService->create( $order, $IN1_NUMBER );
			return $result;
			if($result['errors']){
				$errors += $result['errors'];
			}
		}

		return [
			'data' => [
				'success'=> !$errors,
				'data' => $data,
				'errors' => $errors
			]
		];
	}

    public function actionCreditMemos()
    {
        $request = Yii::$app->request;
        $model = new Invoice();
        $dataProvider = $model->getAllCreditMemos($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionLinkHeaders()
    {
        $request = Yii::$app->request;
        $model = new Invoice();
        $dataProvider = $model->getLinkHeaders($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels()
            ]
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $invoiceService = new InvoiceService();
        $errors = $invoiceService->delete( $id );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

	public function actionDeleteMultiple()
	{
		$IN1_IDs = Yii::$app->getRequest()->post('IN1_IDs');

		$invoiceService = new InvoiceService();
		foreach ( $IN1_IDs as $IN1_ID ) {
			$invoiceService->delete( $IN1_ID );
		}

		return [
			'success' => true
		];
	}

    public function actionInitInvoice()
    {
        $filters = Yii::$app->request->getQueryParams();

        $user = Yii::$app->user->getIdentity();

        $company = Company::findOne(['CO1_ID' => $user->CO1_ID]);

        $fromLocation    = Location::findOne(['LO1_ID' => $user->LO1_ID]);
        $centralLocation = Location::getCentralLocation($user->CO1_ID);

        $masterData = new MasterData();
        $parts = $masterData->getEditInventoryParts($filters);
        $parts->query->asArray();

        $number = new NumberService();
        $orderNumber = $number->getOrderNumber();

        return [
            'company'         => $company,
            'fromLocation'    => $fromLocation,
            'centralLocation' => $centralLocation,
            'parts'           => $parts->getModels(),
            'number'          => $orderNumber
        ];
    }

    public function actionPreload()
    {
        $request = Yii::$app->getRequest();

        $user = new \app\models\User();
        $users = $user->search( $request->getQueryParams() );
        $users->query->asArray();

        $location = new \app\models\Location();
        $locations = $location->search( $request->getQueryParams() );
        $locations->query->asArray();

        return [
            'users'     => $users->getModels(),
            'shops'   => $locations->getModels()
        ];
    }

    public function actionExport()
    {
        $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();

        switch ($params['type']) {
            case 'xls':
            default:
                $service = new InvoiceService();
                return $service->generateXls($params);
        }
    }

	public function actionPrintPdf()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printInvoices($params);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);
	}

	public function actionGenerateAllInvoices()
	{
		$service = new InvoiceService();
		return $service->generateAllInvoices();
	}

	public function actionCreateFromStoreOrder()
    {
        $data = Yii::$app->getRequest()->post();

        $errors = [];
        $IN1_IDs = [];
        foreach( $data['OR1_IDs'] as $OR1_ID){
            $invoice = new \app\services\Invoice();
            $result = $invoice->createFromStoreOrder( $OR1_ID );
            if(!$result['success']){
                $errors[] = $result['errors'];
            }else{
                $IN1_IDs[] = $result['IN1_ID'];
            }
        }

        return [
            'data' => [
                'success' => !$errors,
                'errors'  => $errors,
                'IN1_IDs' => $IN1_IDs,
                'OR1_IDs' => $data['OR1_IDs']
            ]
        ];
    }
}