<?php

namespace app\controllers;

use \Yii;
use app\components\AuthController;
use yii\filters\AccessControl;
use app\services\JobTicket as JobTicketService;
use app\models\JobTicket;
use app\models\Company;
use app\models\Location;
use app\models\MasterData;
use app\components\PdfTrait;
use app\pdfs\Pdf;

class JobTicketController extends AuthController
{
	use PdfTrait;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

	    $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create', 'update', 'delete', 'delete-multiple', 'preload', 'init-ticket', 'export', 'print-pdf'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'preload', 'init-ticket', 'export', 'print-pdf'],
                'allow'   => true,
                'roles'   => ['JOB_TICKETS_READ']
            ],[
                'actions' => ['create', 'update'],
                'allow' => true,
                'roles' => ['JOB_TICKETS_EDIT']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['JOB_TICKETS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new JobTicket();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new JobTicket();
        $dataProvider = $model->getJobTicket($id, $request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $jobTicketService = new JobTicketService();

        $errors = [];
        foreach($data['orders'] as $order){
            $result = $jobTicketService->create( $order );
            if($result['errors']){
                $errors += $result['errors'];
            }
        }

        return [
            'data' => [
                'success'=> !$errors,
                'data'   => $data,
                'errors' => $errors
            ]
        ];
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();
        $jobTicketModel = JobTicket::findOne(['JT1_ID' => $data['JT1_ID']]);
        if( $jobTicketModel->load($data, '') && $jobTicketModel->save( true ) ){
            return [
                'success' => true,
                'data'    => $jobTicketModel
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $data
            ];
        }
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $jobTicket = JobTicket::find()->where(["JT1_ID" => $id])->one();
	    $result = $jobTicket->delete();

        return [
            'data' => [
                'success'=> $result['success'],
                'errors' => $result['errors']
            ]
        ];
    }

	public function actionDeleteMultiple()
	{
		$JT1_IDs = Yii::$app->getRequest()->post('JT1_IDs');

		foreach ( $JT1_IDs as $JT1_ID ) {
		    $jobTicket = JobTicket::find()->where(["JT1_ID" => $JT1_ID])->one();
	        $jobTicket->delete();
		}

		return [
			'success' => true
		];
	}

    public function actionPreload()
    {
        $request = Yii::$app->getRequest();

        $user = new \app\models\User();
        $users = $user->search( $request->getQueryParams() );
        $users->query->asArray();

        $vendor = new \app\models\Vendor();
        $vendors = $vendor->getAllVendors( $request->getQueryParams() );
        $vendors->query->asArray();

        $company = new \app\models\Company();
        $companies = $company->search( $request->getQueryParams() );
        $companies->query->asArray();

        $location = new \app\models\Location();
        $locations = $location->search( $request->getQueryParams() );
        $locations->query->asArray();

        return [
            'users'     => $users->getModels(),
            'vendors'   => $vendors->getModels(),
            'companies' => $companies->getModels(),
            'shops'   => $locations->getModels()
        ];
    }

    public function actionInitTicket()
    {
        $filters = Yii::$app->request->getQueryParams();

        $user = Yii::$app->user->getIdentity();

        $company      = Company::findOne(['CO1_ID' => $user->CO1_ID]);
        $fromLocation = Location::findOne(['LO1_ID' => $user->LO1_ID]);

        $masterData = new MasterData();
        $parts = $masterData->getQuickAddParts($filters);

        $repair = new \app\models\RepairOrder();
        $repairs = $repair->search( $filters, $fromLocation);
        $repairs->query->asArray();

        $technician = new \app\models\Technician();
        $technicians = $technician->getAllTechnicians( $filters );
        $technicians->query->asArray();

        return [
            'company'         => $company,
            'fromLocation'    => $fromLocation,
            'parts'           => $parts,
            'technicians'     => $technicians->getModels(),
            'repair'          => $repairs->getModels()
        ];
    }

    public function actionExport()
    {
	    $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $service = new JobTicketService();

        switch ($params['type']) {
            case 'xls':
            default:
                return $service->generateXls($params);
        }
    }

	public function actionPrintPdf()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();

		$pdfService = new Pdf();
		$pdf = $pdfService->printJobInvoices($params, 1);

		$filePrefix = str_replace('.pdf', '', $params['fileName']);
		$currDate = Date('Ymd_His');
		$filename = $filePrefix . '_' . $currDate . '.pdf';

		$this->sendPdfFile($pdf, $filename);

		$ids = explode(',', $params['JT1_IDs']);
		$models = JobTicket::find()->where(['JT1_ID' => $ids])->all();
        foreach ($models as $obTicket) {
            $obTicket->JT1_PRINTED = true;
            $obTicket->save();
		}
	}
}
