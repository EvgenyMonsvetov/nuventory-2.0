<?php
namespace app\controllers;

use app\components\AuthController;
use app\models\Upload;
use app\services\Upload as UploadService;
use app\services\Import as ImportService;
use yii\filters\AccessControl;
use Yii;
use yii\web\UploadedFile;

class UploadController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['create'], //only be applied to
            'rules' => [[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['DASHBOARD_READ']
            ]]
        ];

        return $behaviors;
    }

    public function actionCreate()
    {
        $errors = [];

        $files = UploadedFile::getInstancesByName('fileKey');
        $data = Yii::$app->request->post();
	    $type = $data['type'];
	    $fileName = $data['fileName'];

	    if ( $files && $files[0] ) {
		    if ($files && $files[0]) {
			    $model = new Upload();
			    $model->fileKey = $files[0];
		    } else {
			    $errors = ['Nothing to save'];
		    }

		    if ($model->fileKey) {
			    switch ($type) {
				    case 'image':
					    $upload = new UploadService();
					    return $upload->uploadImage($model, $files);
					    break;
				    case 'xls':
					    $import = new ImportService();
					    return $import->importXls($files[0], $fileName, $data);
					    break;
			    }
		    } else {
			    $errors = ['Nothing to save'];
		    }
	    } else {
		    $errors = ['Nothing to save'];
	    }

        return [
            'data' => [
                'imageUrl' => '',
                'success' => !$errors,
                'errors' => $errors
            ]
        ];
    }

}
