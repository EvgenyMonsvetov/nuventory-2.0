<?php

namespace app\controllers;

use app\services\PurchaseOrder as PurchaseOrderService;
use \Yii;
use app\components\AuthController;
use app\services\AccessList;
use app\models\AccessList as AccessListModel;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\UploadedFile;
use app\services\PurchaseOrderLine as PurchaseOrderLineService;
use app\models\PurchaseOrderLine;
use app\models\Vendor;
use app\models\Customer;

class PurchaseOrderLineController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'delete'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view'],
                'allow'   => true,
                'roles'   => ['PURCHASE_ORDERS_READ']
            ],[
                'actions' => ['delete'],
                'allow'   => true,
                'roles'   => ['PURCHASE_ORDERS_DELETE']
            ]]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new PurchaseOrderLine();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new PurchaseOrderLine();
        $dataProvider = $model->getPurchaseOrderLine($id);
        $dataProvider->query->asArray();

        return [
            'data' =>  $dataProvider->getModels()
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $purchaseOrderLineService = new PurchaseOrderLineService();
        $errors = $purchaseOrderLineService->delete( $id );

        return [
            'data' => [
                'success' => !$errors,
                'errors' => $errors
            ]
        ];


    }

}