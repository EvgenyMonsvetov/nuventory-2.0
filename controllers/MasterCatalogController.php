<?php

namespace app\controllers;

use app\models\Company;
use app\models\Location;
use \Yii;
use app\components\AuthController;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use app\models\MasterCatalog;
use app\models\MeasurementUnit;
use app\services\MasterCatalog as MasterCatalogService;
use yii\helpers\Url;

class MasterCatalogController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
                    'class' => AccessControl::className(),
                    'only' => ['index', 'view', 'delete', 'delete-multiple', 'update', 'create', 'search', 'search-new-parts', 'parts', 'update-parts', 'export', 'preload', 'set-default-images'], //only be applied to
                    'rules' => [[
                        'actions' => ['index', 'view', 'search', 'search-new-parts', 'parts', 'export', 'preload'],
                        'allow' => true,
                        'roles' => ['MASTER_CATALOG_READ']
                    ],[
                        'actions' => ['update', 'update-parts', 'set-default-images'],
                        'allow' => true,
                        'roles' => ['MASTER_CATALOG_EDIT']
                    ],[
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['MASTER_CATALOG_ADD']
                    ],[
                        'actions' => ['delete', 'delete-multiple'],
                        'allow' => true,
                        'roles' => ['MASTER_CATALOG_DELETE']
                    ]]
                ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $model = new MasterCatalog();
        $dataProvider = $model->getAll($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionSearch()
    {
        $request = Yii::$app->request;
        $model = new MasterCatalog();
        $dataProvider = $model->getSearch($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionLinkedParts()
    {
        $model = new MasterCatalog();
        $params = Yii::$app->request->getQueryParams();

        return [
            'data' => $model->getLinkedParts($params)
        ];
    }

    public function actionView()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = new MasterCatalog();
        $dataProvider = $model->getMasterCatalog($id);
        $dataProvider->query->asArray();
	    $models = $dataProvider->getModels();

	    $arr = array();
	    foreach ($models as $model) {
		    $arr[] = $this->populate($model);
	    }

        return [
            'data' =>  $arr
        ];
    }

	public function populate($data) {
		$basePath = isset(Yii::$app->params['partsImagesPath']) ? Yii::$app->params['partsImagesPath'] . DIRECTORY_SEPARATOR : '';
		$basePath = str_replace('web/', '', $basePath);

		if ( $data['FI1_FILE_PATH']) {
			if (file_exists( $basePath . $data['FI1_FILE_PATH'])) {
				$data['FI1_FILE_PATH_EXIST'] = true;
			} else {
				$data['FI1_FILE_PATH_EXIST'] = false;
			}
			$data['FI1_FILE_PATH'] = Url::base(true) . DIRECTORY_SEPARATOR . $basePath . $data['FI1_FILE_PATH'];
		} else {
			$data['FI1_FILE_PATH'] = '';
			$data['FI1_FILE_PATH_EXIST'] = false;
		}

		return $data;
	}

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        $masterCatalogService = new MasterCatalogService();
        $result = $masterCatalogService->createMasterCatalog( $data );

        return [
            'data' => [
                'success'             => empty($result['errors']),
                'data'                => $data,
                'errors'              => $result['errors'],
                'masterdatawithimage' => $result['masterdatawithimage']
            ]
        ];
    }

    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $masterCatalogService = new MasterCatalogService();
        $errors = $masterCatalogService->deleteMasterCatalog( $id );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

	public function actionDeleteMultiple()
	{
		$MD0_IDs = Yii::$app->getRequest()->post('MD0_IDs');

		foreach ( $MD0_IDs as $MD0_ID ) {
			$masterCatalogModel = MasterCatalog::findOne(['MD0_ID' => $MD0_ID]);
			$masterCatalogModel->MD0_DELETE_FLAG = 1;
			$masterCatalogModel->save(false);
		}

		return [
			'success' => true
		];
	}

    public function actionParts()
    {
        $request = Yii::$app->request;
        $model = new MasterCatalog();
        $dataProvider = $model->getAllParts($request->getQueryParams());
        $dataProvider->query->asArray();

        return [
            'data' => [
                'currentVocabulary' => $dataProvider->getModels(),
                'count' => $dataProvider->getTotalCount()
            ]
        ];
    }

    public function actionUpdateParts()
    {
        $data = Yii::$app->request->post();

        $masterCatalogService = new MasterCatalogService();
        $errors = $masterCatalogService->updateParts( $data );

        return [
            'data' => [
                'success'=> !$errors,
                'errors' => $errors
            ]
        ];
    }

	public function actionPreload()
	{
		$request = Yii::$app->getRequest();

		$vendor = new \app\models\MasterVendor();
		$vendors = $vendor->getAll( $request->getQueryParams() );
		$vendors->query->asArray();

		$unit= new MeasurementUnit();
		$units = $unit->search([]);
		$units->query->asArray();

		return [
			'masterVendors' => $vendors->getModels(),
			'units' => $units->getModels()
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new MasterCatalogService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}

	public function actionSearchNewParts()
	{
		$params = Yii::$app->request->getQueryParams();

		$defaultOrder = [];
		if (!empty($params['sort'])) {
			foreach ($params['sort'] as $sort) {
				list($prop, $dir) = explode(',', $sort);
				$defaultOrder[$prop] = $dir == 'asc' ? SORT_ASC : SORT_DESC;
			}
		}

		if (isset($params['usePagination']) && $params['usePagination']) {
			$pagination = [
				'pageSize' => $params['perPage'],
				'page'     => $params['page']
			];
		} else {
			$pagination = false;
		}

		$masterCatalogModel = new MasterCatalog();
		$query = $masterCatalogModel->getSearchNewParts($params);

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => $pagination,
			'sort' => [
				'defaultOrder' => $defaultOrder
			]
		]);

		$dataProvider->query->asArray();
		$masterCatalogModels = $dataProvider->getModels();

		$populatedMasterCatalogModels = array();
		foreach ($masterCatalogModels as $model) {
			$populatedMasterCatalogModels[] = $this->populate($model);
		}

		return [
			'data' => $populatedMasterCatalogModels,
			'count' => $dataProvider->getTotalCount()
		];
	}

    public function actionSetDefaultImages()
    {
        $data = Yii::$app->request->post();

        $masterCatalogService = new MasterCatalogService();

        return $masterCatalogService->setDefaultImages( $data['MD0_ID'], $data['MD1_IDS']);
    }
}
