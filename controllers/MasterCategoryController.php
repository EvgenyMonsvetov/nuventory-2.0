<?php
/**
 * Created by PhpStorm.
 * User: e.monsvetov
 * Date: 08/11/2018
 * Time: 11:06
 */

namespace app\controllers;

use app\components\AuthController;
use app\models\MasterCategory;
use yii\filters\AccessControl;
use app\services\MasterCategory as MasterCategoryService;
use Yii;

class MasterCategoryController extends AuthController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['index', 'view', 'create','update', 'delete', 'delete-multiple', 'export'], //only be applied to
            'rules' => [[
                'actions' => ['index', 'view', 'export'],
                'allow'   => true,
                'roles'   => ['MASTER_CATEGORIES_READ']
            ],[
                'actions' => ['create'],
                'allow'   => true,
                'roles'   => ['MASTER_CATEGORIES_ADD']
            ],[
                'actions' => ['update'],
                'allow'   => true,
                'roles'   => ['MASTER_CATEGORIES_EDIT']
            ],[
                'actions' => ['delete', 'delete-multiple'],
                'allow'   => true,
                'roles'   => ['MASTER_CATEGORIES_DELETE']
            ]]
        ];

        return $behaviors;
    }


    public function actionIndex()
    {
        $request = Yii::$app->getRequest();

        $masterCategory = new MasterCategory();
        $dataProvider = $masterCategory->search( $request->getQueryParams() );
        $dataProvider->query->asArray();

        return [
            'data'  => $dataProvider->getModels(),
            'count' => $dataProvider->getTotalCount()
        ];
    }

    public function actionView($id)
    {
        $masterCategory = new MasterCategory();
        $query = $masterCategory->getSearchQuery(['CA0_ID' => $id]);
        $query->asArray();
        $data = $query->one();

        return [
            'data' => $data
        ];
    }

    public function actionCreate()
    {
        $data = Yii::$app->request->post();

        if( isset($data['CA0_ID'])){
            unset($data['CA0_ID']);
        }

        $masterCategory = new MasterCategory();

        if( $masterCategory->load($data, '') && $masterCategory->save( true ) ){
            return [
                'success' => true,
                'data'    => $masterCategory
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $masterCategory->getErrorSummary(true)
            ];
        }
    }

    public function actionUpdate()
    {
        $data = Yii::$app->request->post();

        $masterCategory = MasterCategory::findOne(['CA0_ID' => $data['CA0_ID']]);

        if( $masterCategory->load($data, '') && $masterCategory->save( true ) ){
            return [
                'success' => true,
                'data'    => $masterCategory
            ];
        }else{
            return [
                'success' => false,
                'errors'  => $masterCategory->getErrorSummary(true)
            ];
        }
    }

    public function actionDelete()
    {
        $data = Yii::$app->request->getQueryParams();

        $masterCategory = MasterCategory::findOne(['CA0_ID' => $data['id']]);
        $masterCategory->CA0_DELETE_FLAG = 1;
        $masterCategory->save(false);

        return [
            'success' => true
        ];
    }

	public function actionDeleteMultiple()
	{
		$CA0_IDs = Yii::$app->getRequest()->post('CA0_IDs');

		foreach( $CA0_IDs as $CA0_ID ){
			$masterCategoryModel = MasterCategory::findOne(['CA0_ID' => $CA0_ID]);
			$masterCategoryModel->CA0_DELETE_FLAG = 1;
			$masterCategoryModel->save(false);
		}

		return [
			'success' => true
		];
	}

	public function actionExport()
	{
		$request = Yii::$app->getRequest();
		$params = $request->getQueryParams();
		$service = new MasterCategoryService();

		switch ($params['type']) {
			case 'xls':
			default:
				return $service->generateXls($params);
		}
	}
}
