-- MySQL dump 10.13  Distrib 5.7.12, for osx10.11 (x86_64)
--
-- Host: 127.0.0.1    Database: edi2
-- ------------------------------------------------------
-- Server version	5.6.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `co1_company`
--

DROP TABLE IF EXISTS `co1_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `co1_company` (
  `CO1_ID` bigint(20) NOT NULL DEFAULT '0',
  `CO1_NAME` varchar(100) DEFAULT NULL,
  `CO1_SHOW_DEFAULT` varchar(1) NOT NULL DEFAULT 'X',
  `CO1_IP` varchar(45) DEFAULT NULL,
  `CO1_CREATED_ON` datetime DEFAULT NULL,
  `CO1_MODIFIED_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`CO1_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `co1_company`
--

LOCK TABLES `co1_company` WRITE;
/*!40000 ALTER TABLE `co1_company` DISABLE KEYS */;
INSERT INTO `co1_company` (`CO1_ID`, `CO1_NAME`, `CO1_SHOW_DEFAULT`, `CO1_IP`, `CO1_CREATED_ON`, `CO1_MODIFIED_ON`) VALUES (1,'Control Group','1','127.0.0.1',NULL,NULL);
/*!40000 ALTER TABLE `co1_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cu1_customer`
--

DROP TABLE IF EXISTS `cu1_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cu1_customer` (
  `CU1_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CORP_ADDRESS_ID` varchar(10) DEFAULT NULL,
  `CU1_NAME` varchar(100) NOT NULL,
  `CU1_CREATED_BY` int(11) NOT NULL DEFAULT '0',
  `CU1_CREATED_ON` datetime DEFAULT NULL,
  `CU1_MODIFIED_BY` int(11) NOT NULL DEFAULT '0',
  `CU1_MODIFIED_ON` datetime DEFAULT NULL,
  `CU1_SHOW_DEFAULT` varchar(1) NOT NULL DEFAULT 'X',
  `CU1_TEST_MODE` int(11) DEFAULT '0',
  `CU1_COMPANY_ID` varchar(20) DEFAULT '',
  `CU1_RECEIVE_EDI` int(11) NOT NULL DEFAULT '0',
  `CU1_SEND_EDI_INVOICES` int(11) DEFAULT '0',
  `CU1_SEND_EDI_ASN` varchar(45) NOT NULL DEFAULT '0',
  `CU1_SEND_EDI_ORDERS` int(11) DEFAULT '0',
  `CU1_SEND_EDI_ORDER_CONFIRMATIONS` int(11) DEFAULT '0',
  `CU1_SEND_ACKNOWLEDGEMENT` int(11) DEFAULT '0',
  `CU1_ORDER_TYPE` varchar(45) NOT NULL DEFAULT '0',
  `CU1_ORDER_FORMAT` int(11) DEFAULT '0',
  `CU1_INVOICE_FORMAT` int(11) DEFAULT '0',
  `CU1_ASN_FORMAT` int(11) DEFAULT NULL,
  `CU1_TXT_APPROVED` int(10) NOT NULL DEFAULT '0',
  `CU1_SEND_FTP` int(10) NOT NULL DEFAULT '0',
  `CU1_SEND_SFTP` int(11) DEFAULT '0',
  `CU1_POST_HTTP` int(11) NOT NULL DEFAULT '0',
  `CU1_RECEIVE_FTP` int(10) NOT NULL DEFAULT '0',
  `CU1_PICKUP_FTP` int(10) NOT NULL DEFAULT '0',
  `CU1_RECEIVE_HTTP` int(11) NOT NULL DEFAULT '0',
  `CU1_PICKUP_SFTP` int(11) DEFAULT '0',
  `CU1_REMOTE_FTP_SERVER` varchar(200) DEFAULT NULL,
  `CU1_REMOTE_FTP_USERNAME` varchar(200) DEFAULT NULL,
  `CU1_REMOTE_FTP_PASSWORD` varchar(200) DEFAULT NULL,
  `CU1_REMOTE_FTP_DIRECTORY_SEND` varchar(200) DEFAULT NULL,
  `CU1_REMOTE_FTP_DIRECTORY_PICKUP` varchar(200) DEFAULT NULL,
  `CU1_FTP_USER` varchar(200) DEFAULT NULL,
  `CU1_FTP_PASSWORD` varchar(200) DEFAULT NULL,
  `CU1_FTP_DIRECTORY` varchar(200) DEFAULT NULL,
  `CU1_REMOTE_HTTP_SERVER` varchar(200) DEFAULT NULL,
  `CU1_REMOTE_HTTP_USERNAME` varchar(100) DEFAULT '',
  `CU1_REMOTE_HTTP_PASSWORD` varchar(100) DEFAULT '',
  `CU1_SUPPLIER_CODE` varchar(45) DEFAULT NULL,
  `CU1_RECEIVER_QUALIFIER` varchar(2) DEFAULT NULL,
  `CU1_RECEIVER_ID` varchar(45) DEFAULT NULL,
  `CU1_FACILITY` varchar(45) DEFAULT NULL,
  `CU1_TRADING_PARTNER_QUALIFIER` varchar(2) DEFAULT NULL,
  `CU1_TRADING_PARTNER_ID` varchar(45) DEFAULT NULL,
  `CU1_TRADING_PARTNER_GS_ID` varchar(100) DEFAULT '',
  `CU1_ASN_TRADING_PARTNER_ID` varchar(45) DEFAULT NULL,
  `CU1_CONSOLIDATE_ASN` int(11) DEFAULT NULL,
  `CU1_FLAG` varchar(1) DEFAULT NULL,
  `CU1_X12_STANDARD` varchar(4) DEFAULT NULL,
  `CU1_EDI_VERSION` varchar(5) DEFAULT NULL,
  `CU1_DUNS` varchar(50) DEFAULT NULL,
  `CU1_SHARED_SECRET` varchar(100) DEFAULT NULL,
  `CU1_REJECT_INVALID_ITEM_ORDERS` int(10) DEFAULT '0',
  `CU1_INVALID_ITEM_SUBSTITUTE` varchar(45) DEFAULT NULL,
  `CU1_USE_CONTRACT` int(11) DEFAULT '0',
  `CU1_SEND_CUSTOMERS_AND_ITEMS` int(11) DEFAULT '0',
  `CU1_STOP_IMPORT_WITH_ERRORS` int(11) DEFAULT '0',
  `CU1_USE_CLASS_ID` int(11) DEFAULT '0',
  `CU1_CLASS_ID` varchar(50) DEFAULT NULL,
  `CU1_MAP` varchar(50) DEFAULT NULL,
  `CU1_ORDER_PRICE_OVERRIDE` int(11) DEFAULT '0',
  `CU1_SEND_CREDIT_INVOICES` int(11) DEFAULT '0',
  `CU1_ONLY_SEND_CREDIT_INVOICES` int(11) DEFAULT '0',
  `CU1_SEND_PAID_INVOICES` int(11) DEFAULT '0',
  `CU1_852_IMPORT_FOLDER` varchar(255) DEFAULT NULL,
  `CU1_ALWAYS_SEND_ORDER_CONFIRMATIONS` int(11) DEFAULT '0',
  `CU1_COMPLETE_SHIP_TO_NAME` int(11) DEFAULT '0',
  `CU1_ALWAYS_SEND_ASNS` int(11) DEFAULT '0',
  `CU1_IMPORT_FREIGHT_CODES` int(11) DEFAULT '0',
  `CU1_POST_AS2` int(11) DEFAULT '0',
  `CU1_RECEIVE_AS2` int(11) DEFAULT '0',
  `CU1_CXML_PAYLOAD_ID` varchar(255) DEFAULT NULL,
  `CU1_AS2_CERTIFICATE_FILENAME` varchar(255) DEFAULT NULL,
  `CU1_AS2_RECEIVER_ID` varchar(255) DEFAULT NULL,
  `CU1_AS2_TRADING_PARTNER_ID` varchar(255) DEFAULT NULL,
  `CU1_ASN_IMPORT_FOLDER` varchar(255) DEFAULT NULL,
  `CU1_PICKUP_DIRECTORY` varchar(255) DEFAULT NULL,
  `CU1_SEND_INVENTORY` int(11) DEFAULT '0',
  `CU1_INVENTORY_FORMAT` int(11) DEFAULT '0',
  `CU1_AS2_REQUEST_RECEIPT` int(11) DEFAULT '0',
  `CU1_AS2_SIGN_MESSAGES` int(11) DEFAULT '0',
  `CU1_AS2_KEY_LENGTH` varchar(10) DEFAULT '',
  `CU1_AS2_ENCRYPTION_ALGORITHM` varchar(15) DEFAULT '',
  `CU1_AS2_COMPATIBILITY_MODE` int(11) DEFAULT '0',
  `CU1_SEND_PRICE_CATALOG` int(11) DEFAULT '0',
  `CU1_PRICE_CATALOG_FORMAT` int(11) DEFAULT '0',
  `CU1_ORDER_FORWARDING_FOLDER` varchar(255) DEFAULT '',
  `CU1_PRICE_CATALOG_IMPORT_FOLDER` varchar(255) DEFAULT '',
  `CU1_INVOICE_IMPORT_FOLDER` varchar(255) DEFAULT NULL,
  `CU1_PICKUP_EBAY` int(11) DEFAULT '0',
  `CU1_EBAY_TOKEN` text,
  `CU1_EBAY_DEV_NAME` varchar(255) DEFAULT '',
  `CU1_EBAY_APP_NAME` varchar(255) DEFAULT '',
  `CU1_EBAY_CERT_NAME` varchar(255) DEFAULT '',
  `CU1_SEND_EDI_PICK_TICKETS` int(11) DEFAULT '0',
  `CU1_PICK_TICKET_FORMAT` int(11) DEFAULT '0',
  `CU1_PICK_TICKET_LOCATION_ID` varchar(100) DEFAULT '',
  `CU1_SAVE_867_IN_DATABASE` int(11) DEFAULT '0',
  `CU1_SEND_ORDER_CHECK` int(11) DEFAULT '0',
  `CU1_SHIP_TO_FILTER` varchar(100) DEFAULT '',
  `CU1_REMOTE_FTP_DIRECTORY_SEND_INVENTORY` varchar(255) DEFAULT '',
  `CU1_PICKUP_AMAZON` int(11) DEFAULT '0',
  `CU1_AMAZON_MWS_MARKET_ID_US` varchar(100) DEFAULT '',
  `CU1_AWS_ACCESS_KEY` varchar(100) DEFAULT '',
  `CU1_AWS_SECRET_KEY` varchar(100) DEFAULT '',
  `CU1_AWS_SELLER_ID` varchar(100) DEFAULT '',
  `CU1_AWS_MARKETPLACE_ID` varchar(100) DEFAULT '',
  `CU1_AWS_UTC_HOUR_OFFSET` varchar(100) DEFAULT '',
  `CU1_CUSTOMER_SENDS_P21_SHIP_TO_ID` int(11) DEFAULT '0',
  `CU1_ALLOW_DUPLICATE_PO_NUMBERS` int(11) DEFAULT '0',
  `CU1_USE_P21_SHIP_TO_DATA` int(11) DEFAULT '0',
  `CU1_PICKUP_BILLTRUST` int(11) DEFAULT '0',
  `CU1_BILLTRUST_DEVELOPER_GUID` varchar(100) DEFAULT '',
  `CU1_BILLTRUST_CLIENT_GUID` varchar(100) DEFAULT '',
  `CU1_SEND_EDI_QUOTES` int(11) DEFAULT '0',
  `CU1_QUOTE_FORMAT` int(11) DEFAULT '0',
  `CU1_FTP_DOWNLOAD_FILTER` varchar(255) DEFAULT '',
  `CU1_CHECK_ACKNOWLEDGEMENTS` int(11) DEFAULT '1',
  `CU1_830_FORWARDING_FOLDER` varchar(255) DEFAULT '',
  `CU1_PICKUP_CHANNEL_ADVISOR` int(11) DEFAULT '0',
  `CU1_CHANNEL_ADVISOR_DEV_KEY` varchar(255) DEFAULT '',
  `CU1_CHANNEL_ADVISOR_PASSWORD` varchar(255) DEFAULT '',
  `CU1_CHANNEL_ADVISOR_LOCAL_ID` varchar(255) DEFAULT '',
  `CU1_SFTP_PRIVATE_KEY_FILENAME` varchar(255) DEFAULT '',
  `CU1_ALWAYS_SEND_INVOICES` int(11) DEFAULT '1',
  `CU1_QUOTE_IMPORT_FOLDER` varchar(255) DEFAULT '',
  `CU1_SEND_SINGLE_LINE_INVOICES` int(11) DEFAULT '0',
  `CU1_INVOICE_FORWARDING_FOLDER` varchar(255) DEFAULT '',
  `CU1_PICKUP_EMAIL` int(11) DEFAULT '0',
  `CU1_EMAIL_SERVER` varchar(255) DEFAULT '',
  `CU1_EMAIL_USER` varchar(100) DEFAULT '',
  `CU1_EMAIL_PASSWORD` varchar(100) DEFAULT '',
  `CU1_EMAIL_PORT` int(11) DEFAULT '110',
  `CU1_EMAIL_REQUIRES_TLS` int(11) DEFAULT '0',
  `CU1_PICKUP_SELLER_CLOUD` int(11) DEFAULT '0',
  `CU1_SELLER_CLOUD_USERNAME` varchar(255) DEFAULT '',
  `CU1_SELLER_CLOUD_PASSWORD` varchar(100) DEFAULT '',
  `CU1_ASN_FORWARDING_FOLDER` varchar(255) DEFAULT '',
  `CU1_SEND_CONTRACT_ORDERS` int(11) DEFAULT '0',
  `CU1_SEND_EDI_TRANSFERS` int(11) DEFAULT '0',
  `CU1_TRANSFER_FORMAT` int(11) DEFAULT '0',
  `CU1_TRANSFER_LOCATION_ID` varchar(20) DEFAULT '',
  `CU1_SEND_INVENTORY_RECEIPT_ORDERS` int(11) DEFAULT '0',
  `CU1_SEND_EDI_ROUTING_REQUESTS` int(11) DEFAULT '0',
  `CU1_ROUTING_REQUEST_FORMAT` int(11) DEFAULT '0',
  `CU1_PICKUP_MAGENTO` int(11) DEFAULT '0',
  `CU1_MAGENTO_USERNAME` varchar(100) DEFAULT '',
  `CU1_MAGENTO_API_KEY` varchar(255) DEFAULT '',
  `CU1_ORDER_CONFIRMATION_IMPORT_FOLDER` varchar(255) DEFAULT '',
  `CO1_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CU1_ID`),
  KEY `CU1_SHORT_CODE` (`CORP_ADDRESS_ID`),
  KEY `CU1_NAME` (`CU1_NAME`),
  KEY `CU1_TXT_APPROVED` (`CU1_TXT_APPROVED`),
  KEY `CU1_SHOW_DEFAULT` (`CU1_SHOW_DEFAULT`),
  KEY `CU1_CREATED_BY` (`CU1_CREATED_BY`),
  KEY `CU1_CREATED_ON` (`CU1_CREATED_ON`),
  KEY `CU1_MODIFIED_BY` (`CU1_MODIFIED_BY`),
  KEY `CU1_MODIFIED_ON` (`CU1_MODIFIED_ON`),
  KEY `CU1_INVOICE_TYPE_CXML` (`CU1_SEND_FTP`),
  KEY `CU1_INVOICE_TYPE_EXCEL` (`CU1_RECEIVE_FTP`),
  KEY `CU1_SEND_PDF_COPY` (`CU1_PICKUP_FTP`),
  KEY `CU1_ORDER_FTP_USER` (`CU1_FTP_USER`),
  KEY `CU1_SEND_EDI_ORDERS` (`CU1_SEND_EDI_ORDERS`),
  KEY `CU1_ORDER_FORMAT` (`CU1_ORDER_FORMAT`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cu1_customer`
--

LOCK TABLES `cu1_customer` WRITE;
/*!40000 ALTER TABLE `cu1_customer` DISABLE KEYS */;
INSERT INTO `cu1_customer` (`CU1_ID`, `CORP_ADDRESS_ID`, `CU1_NAME`, `CU1_CREATED_BY`, `CU1_CREATED_ON`, `CU1_MODIFIED_BY`, `CU1_MODIFIED_ON`, `CU1_SHOW_DEFAULT`, `CU1_TEST_MODE`, `CU1_COMPANY_ID`, `CU1_RECEIVE_EDI`, `CU1_SEND_EDI_INVOICES`, `CU1_SEND_EDI_ASN`, `CU1_SEND_EDI_ORDERS`, `CU1_SEND_EDI_ORDER_CONFIRMATIONS`, `CU1_SEND_ACKNOWLEDGEMENT`, `CU1_ORDER_TYPE`, `CU1_ORDER_FORMAT`, `CU1_INVOICE_FORMAT`, `CU1_ASN_FORMAT`, `CU1_TXT_APPROVED`, `CU1_SEND_FTP`, `CU1_SEND_SFTP`, `CU1_POST_HTTP`, `CU1_RECEIVE_FTP`, `CU1_PICKUP_FTP`, `CU1_RECEIVE_HTTP`, `CU1_PICKUP_SFTP`, `CU1_REMOTE_FTP_SERVER`, `CU1_REMOTE_FTP_USERNAME`, `CU1_REMOTE_FTP_PASSWORD`, `CU1_REMOTE_FTP_DIRECTORY_SEND`, `CU1_REMOTE_FTP_DIRECTORY_PICKUP`, `CU1_FTP_USER`, `CU1_FTP_PASSWORD`, `CU1_FTP_DIRECTORY`, `CU1_REMOTE_HTTP_SERVER`, `CU1_REMOTE_HTTP_USERNAME`, `CU1_REMOTE_HTTP_PASSWORD`, `CU1_SUPPLIER_CODE`, `CU1_RECEIVER_QUALIFIER`, `CU1_RECEIVER_ID`, `CU1_FACILITY`, `CU1_TRADING_PARTNER_QUALIFIER`, `CU1_TRADING_PARTNER_ID`, `CU1_TRADING_PARTNER_GS_ID`, `CU1_ASN_TRADING_PARTNER_ID`, `CU1_CONSOLIDATE_ASN`, `CU1_FLAG`, `CU1_X12_STANDARD`, `CU1_EDI_VERSION`, `CU1_DUNS`, `CU1_SHARED_SECRET`, `CU1_REJECT_INVALID_ITEM_ORDERS`, `CU1_INVALID_ITEM_SUBSTITUTE`, `CU1_USE_CONTRACT`, `CU1_SEND_CUSTOMERS_AND_ITEMS`, `CU1_STOP_IMPORT_WITH_ERRORS`, `CU1_USE_CLASS_ID`, `CU1_CLASS_ID`, `CU1_MAP`, `CU1_ORDER_PRICE_OVERRIDE`, `CU1_SEND_CREDIT_INVOICES`, `CU1_ONLY_SEND_CREDIT_INVOICES`, `CU1_SEND_PAID_INVOICES`, `CU1_852_IMPORT_FOLDER`, `CU1_ALWAYS_SEND_ORDER_CONFIRMATIONS`, `CU1_COMPLETE_SHIP_TO_NAME`, `CU1_ALWAYS_SEND_ASNS`, `CU1_IMPORT_FREIGHT_CODES`, `CU1_POST_AS2`, `CU1_RECEIVE_AS2`, `CU1_CXML_PAYLOAD_ID`, `CU1_AS2_CERTIFICATE_FILENAME`, `CU1_AS2_RECEIVER_ID`, `CU1_AS2_TRADING_PARTNER_ID`, `CU1_ASN_IMPORT_FOLDER`, `CU1_PICKUP_DIRECTORY`, `CU1_SEND_INVENTORY`, `CU1_INVENTORY_FORMAT`, `CU1_AS2_REQUEST_RECEIPT`, `CU1_AS2_SIGN_MESSAGES`, `CU1_AS2_KEY_LENGTH`, `CU1_AS2_ENCRYPTION_ALGORITHM`, `CU1_AS2_COMPATIBILITY_MODE`, `CU1_SEND_PRICE_CATALOG`, `CU1_PRICE_CATALOG_FORMAT`, `CU1_ORDER_FORWARDING_FOLDER`, `CU1_PRICE_CATALOG_IMPORT_FOLDER`, `CU1_INVOICE_IMPORT_FOLDER`, `CU1_PICKUP_EBAY`, `CU1_EBAY_TOKEN`, `CU1_EBAY_DEV_NAME`, `CU1_EBAY_APP_NAME`, `CU1_EBAY_CERT_NAME`, `CU1_SEND_EDI_PICK_TICKETS`, `CU1_PICK_TICKET_FORMAT`, `CU1_PICK_TICKET_LOCATION_ID`, `CU1_SAVE_867_IN_DATABASE`, `CU1_SEND_ORDER_CHECK`, `CU1_SHIP_TO_FILTER`, `CU1_REMOTE_FTP_DIRECTORY_SEND_INVENTORY`, `CU1_PICKUP_AMAZON`, `CU1_AMAZON_MWS_MARKET_ID_US`, `CU1_AWS_ACCESS_KEY`, `CU1_AWS_SECRET_KEY`, `CU1_AWS_SELLER_ID`, `CU1_AWS_MARKETPLACE_ID`, `CU1_AWS_UTC_HOUR_OFFSET`, `CU1_CUSTOMER_SENDS_P21_SHIP_TO_ID`, `CU1_ALLOW_DUPLICATE_PO_NUMBERS`, `CU1_USE_P21_SHIP_TO_DATA`, `CU1_PICKUP_BILLTRUST`, `CU1_BILLTRUST_DEVELOPER_GUID`, `CU1_BILLTRUST_CLIENT_GUID`, `CU1_SEND_EDI_QUOTES`, `CU1_QUOTE_FORMAT`, `CU1_FTP_DOWNLOAD_FILTER`, `CU1_CHECK_ACKNOWLEDGEMENTS`, `CU1_830_FORWARDING_FOLDER`, `CU1_PICKUP_CHANNEL_ADVISOR`, `CU1_CHANNEL_ADVISOR_DEV_KEY`, `CU1_CHANNEL_ADVISOR_PASSWORD`, `CU1_CHANNEL_ADVISOR_LOCAL_ID`, `CU1_SFTP_PRIVATE_KEY_FILENAME`, `CU1_ALWAYS_SEND_INVOICES`, `CU1_QUOTE_IMPORT_FOLDER`, `CU1_SEND_SINGLE_LINE_INVOICES`, `CU1_INVOICE_FORWARDING_FOLDER`, `CU1_PICKUP_EMAIL`, `CU1_EMAIL_SERVER`, `CU1_EMAIL_USER`, `CU1_EMAIL_PASSWORD`, `CU1_EMAIL_PORT`, `CU1_EMAIL_REQUIRES_TLS`, `CU1_PICKUP_SELLER_CLOUD`, `CU1_SELLER_CLOUD_USERNAME`, `CU1_SELLER_CLOUD_PASSWORD`, `CU1_ASN_FORWARDING_FOLDER`, `CU1_SEND_CONTRACT_ORDERS`, `CU1_SEND_EDI_TRANSFERS`, `CU1_TRANSFER_FORMAT`, `CU1_TRANSFER_LOCATION_ID`, `CU1_SEND_INVENTORY_RECEIPT_ORDERS`, `CU1_SEND_EDI_ROUTING_REQUESTS`, `CU1_ROUTING_REQUEST_FORMAT`, `CU1_PICKUP_MAGENTO`, `CU1_MAGENTO_USERNAME`, `CU1_MAGENTO_API_KEY`, `CU1_ORDER_CONFIRMATION_IMPORT_FOLDER`, `CO1_ID`) VALUES (1,'12668','Office Depot - Retail',1,'2013-05-14 20:47:51',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','officeR9','TrabnoX1','officedepot','','','','1487','01','9082723200','','08','6123410000','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Office Depot Retail',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(2,'13141','Standard Register',1,'2013-05-14 20:47:51',0,NULL,'X',0,'',1,0,'1',0,0,1,'0',0,0,1,0,0,0,0,1,0,0,0,'','','','','','stdregW2','OrklumT6','standardregister','','','','CONTROLINC','01','9082723200','','01','004277893','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Standard Register',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(3,'13117','Staples',1,'2013-05-14 20:47:51',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','staplesU1','PrerbH7','staples','','','','503511','01','9082723200','','01','015106482','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Staples',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,1,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(4,'12039','CVS Caremark',1,'2013-06-14 20:47:51',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','cvs','Urop99','cvs','','','','30457','12','9082723200','','ZZ','CVS','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','CVS',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(5,'40061','Winn Dixie',1,'2013-06-14 20:47:51',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','winndixie','Dixx99','winndixie','','','','CONTROLINC','01','9082723200','','08','9259890000','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(6,'99999','Web Order Entry',1,'2013-06-01 20:47:51',0,NULL,'X',0,'',1,1,'0',0,1,1,'0',5,1,1,0,0,0,0,1,0,0,0,'','','','','','testedi','Uq5#kl11','web','https://shop.controltekusa.com','','','CONTROLINC','01','9082723200','','01','','','',0,'P','4010','00200','9082723200','Jh77QQwP',0,'',0,1,0,0,'','',0,0,0,1,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'comparatio','','',1),(7,'12669','Office Depot - Virtual Warehouse',1,'2013-06-21 20:47:51',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','officeVW9','TrabnoX2','officedepot','','','','721529','01','9082723200','','08','6123410000VW','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Office Depot VW',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(8,'40689','C & S WG',1,'2013-08-02 20:47:51',0,NULL,'X',0,'',1,1,'0',0,0,1,'0',0,2,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','cswg1','CSgggQ1','cswg','','','','CONTROLINC','01','9082723200','','08','9269070010','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','C & S WG',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(9,'43655','Staples',1,'2014-02-13 20:47:51',0,NULL,'X',0,'',1,1,'1',0,1,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','staplesU2','PrerbH8','staples','','','','503511','01','9082723200','','12','5082535000CH','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Staples Advantage',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,1,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(10,'46279','HiTouch',1,'2015-01-16 20:47:51',0,NULL,'X',0,'',1,1,'1',0,1,1,'0',0,0,1,0,1,0,0,0,1,0,0,'Lb-ftp.logictec.com','ControlGroupUser','C0nt0lGr0upUs3rL!v3','OUT','IN','','','hitouch','','','','V01725','12','9082723200','','ZZ','LBHITOUCHEDI','','',0,'P','4010','00401','080622582','',0,'',1,0,0,0,'','Logicbroker',0,1,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','INVENTORY',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(11,'47959','Office Max',1,'2015-07-02 20:47:51',0,NULL,'X',0,'',1,1,'1',0,1,1,'0',0,0,1,0,1,0,0,0,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','oficemax','Gurt71','officemax','','','','004922','12','9082723200','','14','185122629OMX','','',0,'P','5010','00501','080622582','',0,'',1,0,0,0,'','OfficeMax',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(12,'00000','CTL',1,'2016-02-13 20:47:51',0,NULL,'X',0,'',1,0,'0',0,0,1,'0',0,0,1,0,0,0,0,0,0,0,1,'ftp.ctlglobalsolutions.com','controltek@controltek','gOQDDaUKbAxt5ikn20s3','INBOUND','OUTBOUND','ctl1','XerSbH8','ctl','http://api-prod.ctlglobalsolutions.com','','','','12','9082723200','','ZZ','7654321','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,2,'48900',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(13,'00000','CTL',1,'2016-02-13 20:47:51',0,NULL,'X',0,'',1,0,'0',0,0,1,'0',0,0,1,0,0,0,0,0,0,0,1,'ftp.ctlglobalsolutions.com','controltek@controltek','gOQDDaUKbAxt5ikn20s3','INBOUND','OUTBOUND','ctl2','XerSbH7','ctl','http://api-prod.ctlglobalsolutions.com','','','','12','9082723200','','ZZ','CTL2','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,2,'41753',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(14,'47043','Publix',1,'2016-07-12 20:47:51',0,NULL,'X',0,'',1,1,'0',0,0,1,'0',0,2,1,0,1,0,0,0,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','publix','Gurt71S','publix','','','','','12','9082723200','','08','9262390000','8136881188','',0,'P','5010','00501','080622582','',0,'',1,0,0,0,'','Publix',0,1,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(15,'11793','Coupa',1,'2016-07-22 20:47:52',0,NULL,'X',0,'',1,0,'0',0,0,1,'0',0,0,0,0,0,0,0,0,0,1,0,'','','','','','','','coupa','','','','','12','9082723200','','ZZ','COUPA','','',0,'T','4010','00401','080622582','DJakjh712SS',0,'',0,0,0,0,'','',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(16,'00000','CTL Test 1',1,'2016-02-13 20:47:52',0,NULL,'X',1,'',1,0,'0',0,0,1,'0',0,0,1,0,0,0,0,0,0,0,0,'ftp.ctlglobalsolutions.com','controltek@controltek','gOQDDaUKbAxt5ikn20s3','INBOUND','OUTBOUND','ctl1','XerSbH8','ctl','http://api-dev.ctlglobalsolutions.com','','','','12','9082723200','','ZZ','7654321','','',0,'T','4010','00200','080622582','',0,'',1,0,0,0,'','',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,2,'48900',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(17,'00000','CTL Test 2',1,'2016-02-13 20:47:52',0,NULL,'X',1,'',1,0,'0',0,0,1,'0',0,0,1,0,0,0,0,0,0,0,0,'ftp.ctlglobalsolutions.com','controltek@controltek','gOQDDaUKbAxt5ikn20s3','INBOUND','OUTBOUND','ctl1','XerSbH8','ctl','http://api-prod.ctlglobalsolutions.com','','','','12','9082723200','','ZZ','7654321','','',0,'T','4010','00200','080622582','',0,'',1,0,0,0,'','',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,2,'41753',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(18,'55270','Magento2',1,'2013-06-01 20:47:52',0,NULL,'X',0,'',1,1,'0',0,1,0,'0',14,14,1,0,0,0,0,0,0,0,0,'','','','','','testediM','Uq5#kl11','magento','https://shop.controltekusa.com','','','CONTROLINC','01','9082723200','','01','','','',0,'P','4010','00200','9082723200','JHLGDAUI6JHG',0,'',0,0,0,0,'','',0,0,0,1,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,1,'comparatio','','',1),(19,'13116','Staples',1,'2014-02-13 20:47:52',0,NULL,'X',0,'',1,0,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','staplesU2','PrerbH8y','staples','','','','503511','12','9082723200','','01','015106482SE','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Staples Advantage',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,1,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(20,'11111','Ariba Connection',1,'2013-06-01 20:47:52',0,NULL,'X',1,'',1,1,'0',0,1,1,'0',0,0,1,0,0,0,0,0,0,0,0,'','','','','','','','ariba','https://testacig.ariba.com/as2/as2','P000341','K33pCTS3cure$','','12','9082723200','','ZZ','ARIBA','AN01000018306-T','',0,'T','4010','00200','','',0,'',0,0,0,1,'ARIBA','Ariba',0,0,0,0,'',0,0,0,0,1,1,'','e0397-iflmap.hcisbt.eu1.hana.ondemand.com.pem','CONTROLTEKAS2','ZZARIBATEST','','',0,0,1,1,'','SHA256',1,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(21,'55745','Petsmart',1,'2018-03-08 20:47:52',0,NULL,'X',1,'',1,1,'1',0,1,1,'0',0,0,1,0,1,0,0,0,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','test1','test123','Petsmart','','','','','12','9082723200','','01','173808684','','',0,'T','6010','00200','080622582','',0,'',0,0,0,0,'','',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(22,'12668','Office Depot - Retail',1,'2013-05-14 20:49:07',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','officeR9','TrabnoX1','officedepot','','','','1487','01','9082723200','','08','6123410000','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Office Depot Retail',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(23,'13141','Standard Register',1,'2013-05-14 20:49:07',0,NULL,'X',0,'',1,0,'1',0,0,1,'0',0,0,1,0,0,0,0,1,0,0,0,'','','','','','stdregW2','OrklumT6','standardregister','','','','CONTROLINC','01','9082723200','','01','004277893','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Standard Register',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(24,'13117','Staples',1,'2013-05-14 20:49:08',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','staplesU1','PrerbH7','staples','','','','503511','01','9082723200','','01','015106482','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Staples',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,1,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(25,'12039','CVS Caremark',1,'2013-06-14 20:49:08',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','cvs','Urop99','cvs','','','','30457','12','9082723200','','ZZ','CVS','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','CVS',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(26,'40061','Winn Dixie',1,'2013-06-14 20:49:08',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','winndixie','Dixx99','winndixie','','','','CONTROLINC','01','9082723200','','08','9259890000','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(27,'99999','Web Order Entry',1,'2013-06-01 20:49:08',0,NULL,'X',0,'',1,1,'0',0,1,1,'0',5,1,1,0,0,0,0,1,0,0,0,'','','','','','testedi','Uq5#kl11','web','https://shop.controltekusa.com','','','CONTROLINC','01','9082723200','','01','','','',0,'P','4010','00200','9082723200','Jh77QQwP',0,'',0,1,0,0,'','',0,0,0,1,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'comparatio','','',1),(28,'12669','Office Depot - Virtual Warehouse',1,'2013-06-21 20:49:08',0,NULL,'X',0,'',1,1,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','officeVW9','TrabnoX2','officedepot','','','','721529','01','9082723200','','08','6123410000VW','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Office Depot VW',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(29,'40689','C & S WG',1,'2013-08-02 20:49:08',0,NULL,'X',0,'',1,1,'0',0,0,1,'0',0,2,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','cswg1','CSgggQ1','cswg','','','','CONTROLINC','01','9082723200','','08','9269070010','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','C & S WG',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(30,'43655','Staples',1,'2014-02-13 20:49:08',0,NULL,'X',0,'',1,1,'1',0,1,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','staplesU2','PrerbH8','staples','','','','503511','01','9082723200','','12','5082535000CH','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Staples Advantage',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,1,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(31,'46279','HiTouch',1,'2015-01-16 20:49:08',0,NULL,'X',0,'',1,1,'1',0,1,1,'0',0,0,1,0,1,0,0,0,1,0,0,'Lb-ftp.logictec.com','ControlGroupUser','C0nt0lGr0upUs3rL!v3','OUT','IN','','','hitouch','','','','V01725','12','9082723200','','ZZ','LBHITOUCHEDI','','',0,'P','4010','00401','080622582','',0,'',1,0,0,0,'','Logicbroker',0,1,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','INVENTORY',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(32,'47959','Office Max',1,'2015-07-02 20:49:08',0,NULL,'X',0,'',1,1,'1',0,1,1,'0',0,0,1,0,1,0,0,0,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','oficemax','Gurt71','officemax','','','','004922','12','9082723200','','14','185122629OMX','','',0,'P','5010','00501','080622582','',0,'',1,0,0,0,'','OfficeMax',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(33,'00000','CTL',1,'2016-02-13 20:49:08',0,NULL,'X',0,'',1,0,'0',0,0,1,'0',0,0,1,0,0,0,0,0,0,0,1,'ftp.ctlglobalsolutions.com','controltek@controltek','gOQDDaUKbAxt5ikn20s3','INBOUND','OUTBOUND','ctl1','XerSbH8','ctl','http://api-prod.ctlglobalsolutions.com','','','','12','9082723200','','ZZ','7654321','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,2,'48900',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(34,'00000','CTL',1,'2016-02-13 20:49:08',0,NULL,'X',0,'',1,0,'0',0,0,1,'0',0,0,1,0,0,0,0,0,0,0,1,'ftp.ctlglobalsolutions.com','controltek@controltek','gOQDDaUKbAxt5ikn20s3','INBOUND','OUTBOUND','ctl2','XerSbH7','ctl','http://api-prod.ctlglobalsolutions.com','','','','12','9082723200','','ZZ','CTL2','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,2,'41753',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(35,'47043','Publix',1,'2016-07-12 20:49:08',0,NULL,'X',0,'',1,1,'0',0,0,1,'0',0,2,1,0,1,0,0,0,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','publix','Gurt71S','publix','','','','','12','9082723200','','08','9262390000','8136881188','',0,'P','5010','00501','080622582','',0,'',1,0,0,0,'','Publix',0,1,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(36,'11793','Coupa',1,'2016-07-22 20:49:08',0,NULL,'X',0,'',1,0,'0',0,0,1,'0',0,0,0,0,0,0,0,0,0,1,0,'','','','','','','','coupa','','','','','12','9082723200','','ZZ','COUPA','','',0,'T','4010','00401','080622582','DJakjh712SS',0,'',0,0,0,0,'','',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(37,'00000','CTL Test 1',1,'2016-02-13 20:49:08',0,NULL,'X',1,'',1,0,'0',0,0,1,'0',0,0,1,0,0,0,0,0,0,0,0,'ftp.ctlglobalsolutions.com','controltek@controltek','gOQDDaUKbAxt5ikn20s3','INBOUND','OUTBOUND','ctl1','XerSbH8','ctl','http://api-dev.ctlglobalsolutions.com','','','','12','9082723200','','ZZ','7654321','','',0,'T','4010','00200','080622582','',0,'',1,0,0,0,'','',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,2,'48900',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(38,'00000','CTL Test 2',1,'2016-02-13 20:49:08',0,NULL,'X',1,'',1,0,'0',0,0,1,'0',0,0,1,0,0,0,0,0,0,0,0,'ftp.ctlglobalsolutions.com','controltek@controltek','gOQDDaUKbAxt5ikn20s3','INBOUND','OUTBOUND','ctl1','XerSbH8','ctl','http://api-prod.ctlglobalsolutions.com','','','','12','9082723200','','ZZ','7654321','','',0,'T','4010','00200','080622582','',0,'',1,0,0,0,'','',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,2,'41753',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(39,'55270','Magento2',1,'2013-06-01 20:49:08',0,NULL,'X',0,'',1,1,'0',0,1,0,'0',14,14,1,0,0,0,0,0,0,0,0,'','','','','','testediM','Uq5#kl11','magento','https://shop.controltekusa.com','','','CONTROLINC','01','9082723200','','01','','','',0,'P','4010','00200','9082723200','JHLGDAUI6JHG',0,'',0,0,0,0,'','',0,0,0,1,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,1,'comparatio','','',1),(40,'13116','Staples',1,'2014-02-13 20:49:08',0,NULL,'X',0,'',1,0,'1',0,0,1,'0',0,0,1,0,1,0,0,1,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','staplesU2','PrerbH8y','staples','','','','503511','12','9082723200','','01','015106482SE','','',0,'P','4010','00200','080622582','',0,'',1,0,0,0,'','Staples Advantage',1,0,0,0,'',0,0,0,0,0,0,'','','','','','',1,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,1,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(41,'11111','Ariba Connection',1,'2013-06-01 20:49:08',0,NULL,'X',1,'',1,1,'0',0,1,1,'0',0,0,1,0,0,0,0,0,0,0,0,'','','','','','','','ariba','https://testacig.ariba.com/as2/as2','P000341','K33pCTS3cure$','','12','9082723200','','ZZ','ARIBA','AN01000018306-T','',0,'T','4010','00200','','',0,'',0,0,0,1,'ARIBA','Ariba',0,0,0,0,'',0,0,0,0,1,1,'','e0397-iflmap.hcisbt.eu1.hana.ondemand.com.pem','CONTROLTEKAS2','ZZARIBATEST','','',0,0,1,1,'','SHA256',1,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1),(42,'55745','Petsmart',1,'2018-03-08 20:49:08',0,NULL,'X',1,'',1,1,'1',0,1,1,'0',0,0,1,0,1,0,0,0,1,0,0,'ftp.ecgrid.com','Node1018','78Rxqw$tv','mbx82109059/out','mbx82109059/in','test1','test123','Petsmart','','','','','12','9082723200','','01','173808684','','',0,'T','6010','00200','080622582','',0,'',0,0,0,0,'','',0,0,0,0,'',0,0,0,0,0,0,'','','','','','',0,0,0,0,'','',0,0,0,'','','',0,'','','','',0,0,'',0,0,'','',0,'','','','','','',0,0,0,0,'','',0,0,'',1,'',0,'','','','',1,'',0,'',0,'','','',110,0,0,'','','',0,0,0,'',0,0,0,0,'','','',1);
/*!40000 ALTER TABLE `cu1_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ed1_edi`
--

DROP TABLE IF EXISTS `ed1_edi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ed1_edi` (
  `ED1_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ED1_TYPE` varchar(45) DEFAULT NULL,
  `ED1_DOCUMENT_NO` varchar(50) DEFAULT '',
  `ED1_FILENAME` varchar(255) DEFAULT NULL,
  `ED1_STATUS` int(11) NOT NULL DEFAULT '0',
  `CU1_ID` int(11) NOT NULL DEFAULT '0',
  `VD1_ID` int(11) NOT NULL DEFAULT '0',
  `ED1_MODIFIED_ON` datetime DEFAULT NULL,
  `ED1_MODIFIED_BY` int(11) NOT NULL DEFAULT '0',
  `ED1_CREATED_ON` datetime DEFAULT NULL,
  `ED1_CREATED_BY` int(11) NOT NULL DEFAULT '0',
  `ED1_SHOW_DEFAULT` varchar(1) NOT NULL DEFAULT 'X',
  `ED1_IN_OUT` int(11) DEFAULT '0',
  `ED1_RESEND` int(11) DEFAULT '0',
  `ED1_ACKNOWLEDGED` int(11) DEFAULT '0',
  `ED1_TEST_MODE` int(11) DEFAULT '0',
  `CO1_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ED1_ID`),
  KEY `CU1_ID` (`CU1_ID`),
  KEY `VD1_ID` (`VD1_ID`),
  KEY `ED1_STATUS` (`ED1_STATUS`),
  KEY `ED1_DOCUMENT_NO` (`ED1_DOCUMENT_NO`),
  KEY `ED1_IN_OUT` (`ED1_IN_OUT`),
  KEY `ED1_RESEND` (`ED1_RESEND`),
  KEY `ED1_TYPE` (`ED1_TYPE`),
  KEY `ED1_TEST_MODE` (`ED1_TEST_MODE`)
) ENGINE=InnoDB AUTO_INCREMENT=1486512 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ed1_edi`
--

LOCK TABLES `ed1_edi` WRITE;
/*!40000 ALTER TABLE `ed1_edi` DISABLE KEYS */;
INSERT INTO `ed1_edi` (`ED1_ID`, `ED1_TYPE`, `ED1_DOCUMENT_NO`, `ED1_FILENAME`, `ED1_STATUS`, `CU1_ID`, `VD1_ID`, `ED1_MODIFIED_ON`, `ED1_MODIFIED_BY`, `ED1_CREATED_ON`, `ED1_CREATED_BY`, `ED1_SHOW_DEFAULT`, `ED1_IN_OUT`, `ED1_RESEND`, `ED1_ACKNOWLEDGED`, `ED1_TEST_MODE`, `CO1_ID`) VALUES (1486445,'cXML Invoice','3087932','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3087932.xml',2,1,0,'2017-01-09 08:39:20',1,'2017-01-09 08:39:20',1,'X',1,0,0,1,0),(1486446,'cXML Invoice','3087946','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3087946.xml',2,1,0,'2017-01-09 08:39:20',1,'2017-01-09 08:39:20',1,'X',1,0,0,1,0),(1486447,'cXML Invoice','3087998','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3087998.xml',2,1,0,'2017-01-09 08:39:20',1,'2017-01-09 08:39:20',1,'X',1,0,0,1,0),(1486448,'cXML Invoice','3087999','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3087999.xml',2,1,0,'2017-01-09 08:39:20',1,'2017-01-09 08:39:20',1,'X',1,0,0,1,0),(1486449,'cXML Invoice','3088116','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088116.xml',2,1,0,'2017-01-09 08:39:21',1,'2017-01-09 08:39:21',1,'X',1,0,0,1,0),(1486450,'cXML Invoice','3088220','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088220.xml',2,1,0,'2017-01-09 08:39:21',1,'2017-01-09 08:39:21',1,'X',1,0,0,1,0),(1486451,'cXML Invoice','3088221','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088221.xml',2,1,0,'2017-01-09 08:39:21',1,'2017-01-09 08:39:21',1,'X',1,0,0,1,0),(1486452,'cXML Invoice','3088274','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3088274.xml',2,1,0,'2017-01-09 08:39:21',1,'2017-01-09 08:39:21',1,'X',1,0,0,1,0),(1486453,'cXML Invoice','3088275','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3088275.xml',2,1,0,'2017-01-09 08:39:21',1,'2017-01-09 08:39:21',1,'X',1,0,0,1,0),(1486454,'cXML Invoice','3088285','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3088285.xml',2,1,0,'2017-01-09 08:39:21',1,'2017-01-09 08:39:21',1,'X',1,0,0,1,0),(1486455,'cXML Invoice','3088309','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3088309.xml',2,1,0,'2017-01-09 08:39:22',1,'2017-01-09 08:39:22',1,'X',1,0,0,1,0),(1486456,'cXML Invoice','3088359','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3088359.xml',2,1,0,'2017-01-09 08:39:22',1,'2017-01-09 08:39:22',1,'X',1,0,0,1,0),(1486457,'cXML Invoice','3088393','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088393.xml',2,1,0,'2017-01-09 08:39:22',1,'2017-01-09 08:39:22',1,'X',1,0,0,1,0),(1486458,'cXML Invoice','3088493','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088493.xml',2,1,0,'2017-01-09 08:39:22',1,'2017-01-09 08:39:22',1,'X',1,0,0,1,0),(1486459,'cXML Invoice','3088537','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088537.xml',2,1,0,'2017-01-09 08:39:22',1,'2017-01-09 08:39:22',1,'X',1,0,0,1,0),(1486460,'cXML Invoice','3088566','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088566.xml',2,1,0,'2017-01-09 08:39:22',1,'2017-01-09 08:39:22',1,'X',1,0,0,1,0),(1486461,'cXML Invoice','3088647','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088647.xml',2,1,0,'2017-01-09 08:39:22',1,'2017-01-09 08:39:22',1,'X',1,0,0,1,0),(1486462,'cXML Invoice','3088665','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3088665.xml',2,1,0,'2017-01-09 08:39:23',1,'2017-01-09 08:39:23',1,'X',1,0,0,1,0),(1486463,'cXML Invoice','3088666','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3088666.xml',2,1,0,'2017-01-09 08:39:23',1,'2017-01-09 08:39:23',1,'X',1,0,0,1,0),(1486464,'cXML Invoice','3088669','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088669.xml',2,1,0,'2017-01-09 08:39:23',1,'2017-01-09 08:39:23',1,'X',1,0,0,1,0),(1486465,'cXML Invoice','3088771','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3088771.xml',2,1,0,'2017-01-09 08:39:23',1,'2017-01-09 08:39:23',1,'X',1,0,0,1,0),(1486466,'cXML Invoice','3088772','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3088772.xml',2,1,0,'2017-01-09 08:39:23',1,'2017-01-09 08:39:23',1,'X',1,0,0,1,0),(1486467,'cXML Invoice','3088777','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3088777.xml',2,1,0,'2017-01-09 08:39:23',1,'2017-01-09 08:39:23',1,'X',1,0,0,1,0),(1486468,'cXML Invoice','3088778','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3088778.xml',2,1,0,'2017-01-09 08:39:23',1,'2017-01-09 08:39:23',1,'X',1,0,0,1,0),(1486469,'cXML Invoice','3088881','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3088881.xml',2,1,0,'2017-01-09 08:39:24',1,'2017-01-09 08:39:24',1,'X',1,0,0,1,0),(1486470,'cXML Invoice','3088882','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_CustomBlastService_3088882.xml',2,1,0,'2017-01-09 08:39:24',1,'2017-01-09 08:39:24',1,'X',1,0,0,1,0),(1486471,'cXML Invoice','3088976','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3088976.xml',2,1,0,'2017-01-09 08:39:24',1,'2017-01-09 08:39:24',1,'X',1,0,0,1,0),(1486472,'cXML Invoice','3089021','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089021.xml',2,1,0,'2017-01-09 08:39:24',1,'2017-01-09 08:39:24',1,'X',1,0,0,1,0),(1486473,'cXML Invoice','3089024','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089024.xml',2,1,0,'2017-01-09 08:39:24',1,'2017-01-09 08:39:24',1,'X',1,0,0,1,0),(1486474,'cXML Invoice','3089025','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089025.xml',2,1,0,'2017-01-09 08:39:24',1,'2017-01-09 08:39:24',1,'X',1,0,0,1,0),(1486475,'cXML Invoice','3089067','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089067.xml',2,1,0,'2017-01-09 08:39:24',1,'2017-01-09 08:39:24',1,'X',1,0,0,1,0),(1486476,'cXML Invoice','3089081','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089081.xml',2,1,0,'2017-01-09 08:39:25',1,'2017-01-09 08:39:25',1,'X',1,0,0,1,0),(1486477,'cXML Invoice','3089091','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089091.xml',2,1,0,'2017-01-09 08:39:25',1,'2017-01-09 08:39:25',1,'X',1,0,0,1,0),(1486478,'cXML Invoice','3089101','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089101.xml',2,1,0,'2017-01-09 08:39:25',1,'2017-01-09 08:39:25',1,'X',1,0,0,1,0),(1486479,'cXML Invoice','3089107','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089107.xml',2,1,0,'2017-01-09 08:39:25',1,'2017-01-09 08:39:25',1,'X',1,0,0,1,0),(1486480,'cXML Invoice','3089108','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089108.xml',2,1,0,'2017-01-09 08:39:25',1,'2017-01-09 08:39:25',1,'X',1,0,0,1,0),(1486481,'cXML Invoice','3089170','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089170.xml',2,1,0,'2017-01-09 08:39:25',1,'2017-01-09 08:39:25',1,'X',1,0,0,1,0),(1486482,'cXML Invoice','3089204','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089204.xml',2,1,0,'2017-01-09 08:39:25',1,'2017-01-09 08:39:25',1,'X',1,0,0,1,0),(1486483,'cXML Invoice','3089386','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089386.xml',2,1,0,'2017-01-09 08:39:26',1,'2017-01-09 08:39:26',1,'X',1,0,0,1,0),(1486484,'cXML Invoice','3089415','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089415.xml',2,1,0,'2017-01-09 08:39:26',1,'2017-01-09 08:39:26',1,'X',1,0,0,1,0),(1486485,'cXML Invoice','3089436','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089436.xml',2,1,0,'2017-01-09 08:39:26',1,'2017-01-09 08:39:26',1,'X',1,0,0,1,0),(1486486,'cXML Invoice','3089516','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089516.xml',2,1,0,'2017-01-09 08:39:26',1,'2017-01-09 08:39:26',1,'X',1,0,0,1,0),(1486487,'cXML Invoice','3089517','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3089517.xml',2,1,0,'2017-01-09 08:39:26',1,'2017-01-09 08:39:26',1,'X',1,0,0,1,0),(1486488,'cXML Invoice','3089518','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3089518.xml',2,1,0,'2017-01-09 08:39:26',1,'2017-01-09 08:39:26',1,'X',1,0,0,1,0),(1486489,'cXML Invoice','3089556','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3089556.xml',2,1,0,'2017-01-09 08:39:27',1,'2017-01-09 08:39:27',1,'X',1,0,0,1,0),(1486490,'cXML Invoice','3089564','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089564.xml',2,1,0,'2017-01-09 08:39:27',1,'2017-01-09 08:39:27',1,'X',1,0,0,1,0),(1486491,'cXML Invoice','3089588','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089588.xml',2,1,0,'2017-01-09 08:39:27',1,'2017-01-09 08:39:27',1,'X',1,0,0,1,0),(1486492,'cXML Invoice','3089636','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089636.xml',2,1,0,'2017-01-09 08:39:27',1,'2017-01-09 08:39:27',1,'X',1,0,0,1,0),(1486493,'cXML Invoice','3089721','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089721.xml',2,1,0,'2017-01-09 08:39:27',1,'2017-01-09 08:39:27',1,'X',1,0,0,1,0),(1486494,'cXML Invoice','3089840','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089840.xml',2,1,0,'2017-01-09 08:39:27',1,'2017-01-09 08:39:27',1,'X',1,0,0,1,0),(1486495,'cXML Invoice','3089852','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089852.xml',2,1,0,'2017-01-09 08:39:27',1,'2017-01-09 08:39:27',1,'X',1,0,0,1,0),(1486496,'cXML Invoice','3089886','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_3089886.xml',2,1,0,'2017-01-09 08:39:27',1,'2017-01-09 08:39:27',1,'X',1,0,0,1,0),(1486497,'cXML Invoice','3089906','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089906.xml',2,1,0,'2017-01-09 08:39:28',1,'2017-01-09 08:39:28',1,'X',1,0,0,1,0),(1486498,'cXML Invoice','3089913','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089913.xml',2,1,0,'2017-01-09 08:39:28',1,'2017-01-09 08:39:28',1,'X',1,0,0,1,0),(1486499,'cXML Invoice','3089914','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089914.xml',2,1,0,'2017-01-09 08:39:28',1,'2017-01-09 08:39:28',1,'X',1,0,0,1,0),(1486500,'cXML Invoice','3089919','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089919.xml',2,1,0,'2017-01-09 08:39:28',1,'2017-01-09 08:39:28',1,'X',1,0,0,1,0),(1486501,'cXML Invoice','3089922','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089922.xml',2,1,0,'2017-01-09 08:39:28',1,'2017-01-09 08:39:28',1,'X',1,0,0,1,0),(1486502,'cXML Invoice','3089933','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_3089933.xml',2,1,0,'2017-01-09 08:39:29',1,'2017-01-09 08:39:29',1,'X',1,0,0,1,0),(1486503,'cXML Invoice','3089974','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3089974.xml',2,1,0,'2017-01-09 08:39:31',1,'2017-01-09 08:39:31',1,'X',1,0,0,1,0),(1486504,'cXML Invoice','3089975','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockIndustrialServices_3089975.xml',2,1,0,'2017-01-09 08:39:31',1,'2017-01-09 08:39:31',1,'X',1,0,0,1,0),(1486505,'cXML Invoice','9900188112','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_9900188112.xml',2,1,0,'2017-01-09 08:54:03',1,'2017-01-09 08:54:03',1,'X',1,0,0,1,0),(1486506,'cXML Invoice','9900190297','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-West_9900190297.xml',2,1,0,'2017-01-09 08:54:03',1,'2017-01-09 08:54:03',1,'X',1,0,0,1,0),(1486507,'cXML Invoice','9900196185','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_9900196185.xml',2,1,0,'2017-01-09 08:54:03',1,'2017-01-09 08:54:03',1,'X',1,0,0,1,0),(1486508,'cXML Invoice','9900196186','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_9900196186.xml',2,1,0,'2017-01-09 08:54:04',1,'2017-01-09 08:54:04',1,'X',1,0,0,1,0),(1486509,'cXML Invoice','9900202785','C:\\Comparatio EDI\\EDI Converter\\ftp\\Invoice_BrockServicesLLC-South_9900202785.xml',2,1,0,'2017-01-09 08:54:04',1,'2017-01-09 08:54:04',1,'X',1,0,0,1,0),(1486510,'cXML Order','','C:\\Comparatio EDI\\EDI Converter\\ftp\\Order_cXML_20170112130917950.xml',2,1,0,'2017-01-12 13:09:17',1,'2017-01-12 13:09:17',1,'X',0,0,0,1,0),(1486511,'cXML Order','','C:\\Comparatio EDI\\EDI Converter\\ftp\\Order_cXML_20170112131036731.xml',2,1,0,'2017-01-12 13:10:36',1,'2017-01-12 13:10:36',1,'X',0,0,0,1,0);
/*!40000 ALTER TABLE `ed1_edi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lg1_log`
--

DROP TABLE IF EXISTS `lg1_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lg1_log` (
  `LOG_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `LOG_DESCRIPTION` text,
  `LOG_UPDATED_BY` int(11) NOT NULL DEFAULT '0',
  `LOG_UPDATED_ON` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LOG_SHOW_DEFAULT` varchar(1) NOT NULL DEFAULT 'X',
  `CU1_ID` int(11) NOT NULL,
  `VD1_ID` int(11) NOT NULL,
  `ED1_ID` int(15) NOT NULL,
  `US1_ID` int(15) NOT NULL,
  `LOG_FILENAME` varchar(255) DEFAULT NULL,
  `LOG_P21` int(11) DEFAULT '0',
  `LOG_CHECKED` int(11) DEFAULT '0',
  `LOG_FILE_TYPE` varchar(20) DEFAULT NULL,
  `CO1_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`LOG_ID`),
  KEY `LOG_UPDATED_BY` (`LOG_UPDATED_BY`),
  KEY `LOG_UPDATED_ON` (`LOG_UPDATED_ON`),
  KEY `AD1_ID` (`VD1_ID`),
  KEY `CO1_ID` (`ED1_ID`),
  KEY `CU1_ID` (`US1_ID`),
  KEY `DELETE_FLAG` (`LOG_SHOW_DEFAULT`) USING BTREE,
  KEY `MD1_ID` (`CU1_ID`),
  KEY `LOG_DESCRIPTION` (`LOG_DESCRIPTION`(250))
) ENGINE=InnoDB AUTO_INCREMENT=1626731 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lg1_log`
--

LOCK TABLES `lg1_log` WRITE;
/*!40000 ALTER TABLE `lg1_log` DISABLE KEYS */;
INSERT INTO `lg1_log` (`LOG_ID`, `LOG_DESCRIPTION`, `LOG_UPDATED_BY`, `LOG_UPDATED_ON`, `LOG_SHOW_DEFAULT`, `CU1_ID`, `VD1_ID`, `ED1_ID`, `US1_ID`, `LOG_FILENAME`, `LOG_P21`, `LOG_CHECKED`, `LOG_FILE_TYPE`, `CO1_ID`) VALUES (1626594,'User logged in',1,'2016-12-12 08:30:47','X',0,0,0,1,NULL,0,0,NULL,0),(1626595,'User logged in',1,'2017-01-06 12:38:46','X',0,0,0,1,NULL,0,0,NULL,0),(1626596,'PO #: 336473, EDI file submitted for transfer',1,'2017-01-09 08:39:20','X',1,0,1486445,0,NULL,0,0,NULL,0),(1626597,'PO #: 01-336684-82327-110, EDI file submitted for transfer',1,'2017-01-09 08:39:20','X',1,0,1486446,0,NULL,0,0,NULL,0),(1626598,'PO #: 01-336685-51008-101, EDI file submitted for transfer',1,'2017-01-09 08:39:20','X',1,0,1486447,0,NULL,0,0,NULL,0),(1626599,'PO #: 01-R30877-51008-101, EDI file submitted for transfer',1,'2017-01-09 08:39:20','X',1,0,1486448,0,NULL,0,0,NULL,0),(1626600,'PO #: 03-336773, EDI file submitted for transfer',1,'2017-01-09 08:39:21','X',1,0,1486449,0,NULL,0,0,NULL,0),(1626601,'PO #: NEED PO, EDI file submitted for transfer',1,'2017-01-09 08:39:21','X',1,0,1486450,0,NULL,0,0,NULL,0),(1626602,'PO #: NEED PO, EDI file submitted for transfer',1,'2017-01-09 08:39:21','X',1,0,1486451,0,NULL,0,0,NULL,0),(1626603,'PO #: 624970, EDI file submitted for transfer',1,'2017-01-09 08:39:21','X',1,0,1486452,0,NULL,0,0,NULL,0),(1626604,'PO #: 624244, EDI file submitted for transfer',1,'2017-01-09 08:39:21','X',1,0,1486453,0,NULL,0,0,NULL,0),(1626605,'PO #: 624387, EDI file submitted for transfer',1,'2017-01-09 08:39:21','X',1,0,1486454,0,NULL,0,0,NULL,0),(1626606,'PO #: 624930, EDI file submitted for transfer',1,'2017-01-09 08:39:22','X',1,0,1486455,0,NULL,0,0,NULL,0),(1626607,'PO #: 624387, EDI file submitted for transfer',1,'2017-01-09 08:39:22','X',1,0,1486456,0,NULL,0,0,NULL,0),(1626608,'PO #: 03-328534, EDI file submitted for transfer',1,'2017-01-09 08:39:22','X',1,0,1486457,0,NULL,0,0,NULL,0),(1626609,'PO #: 03-336442, EDI file submitted for transfer',1,'2017-01-09 08:39:22','X',1,0,1486458,0,NULL,0,0,NULL,0),(1626610,'PO #: 03-336952, EDI file submitted for transfer',1,'2017-01-09 08:39:22','X',1,0,1486459,0,NULL,0,0,NULL,0),(1626611,'PO #: 03-336871, EDI file submitted for transfer',1,'2017-01-09 08:39:22','X',1,0,1486460,0,NULL,0,0,NULL,0),(1626612,'PO #: 03-336952, EDI file submitted for transfer',1,'2017-01-09 08:39:22','X',1,0,1486461,0,NULL,0,0,NULL,0),(1626613,'PO #: 01-333991-81606-100, EDI file submitted for transfer',1,'2017-01-09 08:39:23','X',1,0,1486462,0,NULL,0,0,NULL,0),(1626614,'PO #: 01-336897-81606-300, EDI file submitted for transfer',1,'2017-01-09 08:39:23','X',1,0,1486463,0,NULL,0,0,NULL,0),(1626615,'PO #: 336237, EDI file submitted for transfer',1,'2017-01-09 08:39:23','X',1,0,1486464,0,NULL,0,0,NULL,0),(1626616,'PO #: 625132, EDI file submitted for transfer',1,'2017-01-09 08:39:23','X',1,0,1486465,0,NULL,0,0,NULL,0),(1626617,'PO #: 625133, EDI file submitted for transfer',1,'2017-01-09 08:39:23','X',1,0,1486466,0,NULL,0,0,NULL,0),(1626618,'PO #: 01-335776-05CE1931, EDI file submitted for transfer',1,'2017-01-09 08:39:23','X',1,0,1486467,0,NULL,0,0,NULL,0),(1626619,'PO #: 01-336837-51008-101, EDI file submitted for transfer',1,'2017-01-09 08:39:24','X',1,0,1486468,0,NULL,0,0,NULL,0),(1626620,'PO #: 625094, EDI file submitted for transfer',1,'2017-01-09 08:39:24','X',1,0,1486469,0,NULL,0,0,NULL,0),(1626621,'PO #: 102671, EDI file submitted for transfer',1,'2017-01-09 08:39:24','X',1,0,1486470,0,NULL,0,0,NULL,0),(1626622,'PO #: 08-R30438, EDI file submitted for transfer',1,'2017-01-09 08:39:24','X',1,0,1486471,0,NULL,0,0,NULL,0),(1626623,'PO #: 03-336878, EDI file submitted for transfer',1,'2017-01-09 08:39:24','X',1,0,1486472,0,NULL,0,0,NULL,0),(1626624,'PO #: 03-337025, EDI file submitted for transfer',1,'2017-01-09 08:39:24','X',1,0,1486473,0,NULL,0,0,NULL,0),(1626625,'PO #: 03-33-6873, EDI file submitted for transfer',1,'2017-01-09 08:39:24','X',1,0,1486474,0,NULL,0,0,NULL,0),(1626626,'PO #: 03-337047, EDI file submitted for transfer',1,'2017-01-09 08:39:25','X',1,0,1486475,0,NULL,0,0,NULL,0),(1626627,'PO #: 03-336437, EDI file submitted for transfer',1,'2017-01-09 08:39:25','X',1,0,1486476,0,NULL,0,0,NULL,0),(1626628,'PO #: 03-336865, EDI file submitted for transfer',1,'2017-01-09 08:39:25','X',1,0,1486477,0,NULL,0,0,NULL,0),(1626629,'PO #: 03-337086, EDI file submitted for transfer',1,'2017-01-09 08:39:25','X',1,0,1486478,0,NULL,0,0,NULL,0),(1626630,'PO #: 01-337230-81606-300, EDI file submitted for transfer',1,'2017-01-09 08:39:25','X',1,0,1486479,0,NULL,0,0,NULL,0),(1626631,'PO #: NEED PO, EDI file submitted for transfer',1,'2017-01-09 08:39:25','X',1,0,1486480,0,NULL,0,0,NULL,0),(1626632,'PO #: 03-336952, EDI file submitted for transfer',1,'2017-01-09 08:39:25','X',1,0,1486481,0,NULL,0,0,NULL,0),(1626633,'PO #: 01-R30439-20074-200, EDI file submitted for transfer',1,'2017-01-09 08:39:26','X',1,0,1486482,0,NULL,0,0,NULL,0),(1626634,'PO #: 02-335864, EDI file submitted for transfer',1,'2017-01-09 08:39:26','X',1,0,1486483,0,NULL,0,0,NULL,0),(1626635,'PO #: 337285, EDI file submitted for transfer',1,'2017-01-09 08:39:26','X',1,0,1486484,0,NULL,0,0,NULL,0),(1626636,'PO #: 03-336952, EDI file submitted for transfer',1,'2017-01-09 08:39:26','X',1,0,1486485,0,NULL,0,0,NULL,0),(1626637,'PO #: 03-337147, EDI file submitted for transfer',1,'2017-01-09 08:39:26','X',1,0,1486486,0,NULL,0,0,NULL,0),(1626638,'PO #: 625230, EDI file submitted for transfer',1,'2017-01-09 08:39:26','X',1,0,1486487,0,NULL,0,0,NULL,0),(1626639,'PO #: 625230, EDI file submitted for transfer',1,'2017-01-09 08:39:26','X',1,0,1486488,0,NULL,0,0,NULL,0),(1626640,'PO #: 625152, EDI file submitted for transfer',1,'2017-01-09 08:39:27','X',1,0,1486489,0,NULL,0,0,NULL,0),(1626641,'PO #: 01-337499-82413-300, EDI file submitted for transfer',1,'2017-01-09 08:39:27','X',1,0,1486490,0,NULL,0,0,NULL,0),(1626642,'PO #: 03-337282, EDI file submitted for transfer',1,'2017-01-09 08:39:27','X',1,0,1486491,0,NULL,0,0,NULL,0),(1626643,'PO #: NEED PO, EDI file submitted for transfer',1,'2017-01-09 08:39:27','X',1,0,1486492,0,NULL,0,0,NULL,0),(1626644,'PO #: 337408, EDI file submitted for transfer',1,'2017-01-09 08:39:27','X',1,0,1486493,0,NULL,0,0,NULL,0),(1626645,'PO #: 01-337593-51725-101, EDI file submitted for transfer',1,'2017-01-09 08:39:27','X',1,0,1486494,0,NULL,0,0,NULL,0),(1626646,'PO #: 03-337476, EDI file submitted for transfer',1,'2017-01-09 08:39:27','X',1,0,1486495,0,NULL,0,0,NULL,0),(1626647,'PO #: 03-336095, EDI file submitted for transfer',1,'2017-01-09 08:39:28','X',1,0,1486496,0,NULL,0,0,NULL,0),(1626648,'PO #: 01-336105-81606-100, EDI file submitted for transfer',1,'2017-01-09 08:39:28','X',1,0,1486497,0,NULL,0,0,NULL,0),(1626649,'PO #: 01-R30865-81606-100, EDI file submitted for transfer',1,'2017-01-09 08:39:28','X',1,0,1486498,0,NULL,0,0,NULL,0),(1626650,'PO #: 01-R30863-81606-100, EDI file submitted for transfer',1,'2017-01-09 08:39:28','X',1,0,1486499,0,NULL,0,0,NULL,0),(1626651,'PO #: 01-R30864-81606-100, EDI file submitted for transfer',1,'2017-01-09 08:39:28','X',1,0,1486500,0,NULL,0,0,NULL,0),(1626652,'PO #: 01-R30866-81606-100, EDI file submitted for transfer',1,'2017-01-09 08:39:29','X',1,0,1486501,0,NULL,0,0,NULL,0),(1626653,'PO #: 01-337084-51008-101, EDI file submitted for transfer',1,'2017-01-09 08:39:29','X',1,0,1486502,0,NULL,0,0,NULL,0),(1626654,'PO #: 625291, EDI file submitted for transfer',1,'2017-01-09 08:39:31','X',1,0,1486503,0,NULL,0,0,NULL,0),(1626655,'PO #: 625293, EDI file submitted for transfer',1,'2017-01-09 08:39:31','X',1,0,1486504,0,NULL,0,0,NULL,0),(1626656,'EDI file transfered',1,'2017-01-09 08:39:32','X',1,0,1486472,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089021.xml',0,0,'',0),(1626657,'EDI file transfered',1,'2017-01-09 08:39:32','X',1,0,1486482,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089204.xml',0,0,'',0),(1626658,'EDI file transfered',1,'2017-01-09 08:39:32','X',1,0,1486490,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089564.xml',0,0,'',0),(1626659,'EDI file transfered',1,'2017-01-09 08:39:33','X',1,0,1486465,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3088771.xml',0,0,'',0),(1626660,'EDI file transfered',1,'2017-01-09 08:39:33','X',1,0,1486495,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089852.xml',0,0,'',0),(1626661,'EDI file transfered',1,'2017-01-09 08:39:33','X',1,0,1486446,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3087946.xml',0,0,'',0),(1626662,'EDI file transfered',1,'2017-01-09 08:39:33','X',1,0,1486477,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089091.xml',0,0,'',0),(1626663,'EDI file transfered',1,'2017-01-09 08:39:33','X',1,0,1486487,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3089517.xml',0,0,'',0),(1626664,'EDI file transfered',1,'2017-01-09 08:39:34','X',1,0,1486459,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088537.xml',0,0,'',0),(1626665,'EDI file transfered',1,'2017-01-09 08:39:34','X',1,0,1486501,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089922.xml',0,0,'',0),(1626666,'EDI file transfered',1,'2017-01-09 08:39:34','X',1,0,1486451,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088221.xml',0,0,'',0),(1626667,'EDI file transfered',1,'2017-01-09 08:39:34','X',1,0,1486481,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089170.xml',0,0,'',0),(1626668,'EDI file transfered',1,'2017-01-09 08:39:34','X',1,0,1486489,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3089556.xml',0,0,'',0),(1626669,'EDI file transfered',1,'2017-01-09 08:39:35','X',1,0,1486464,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088669.xml',0,0,'',0),(1626670,'EDI file transfered',1,'2017-01-09 08:39:35','X',1,0,1486471,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088976.xml',0,0,'',0),(1626671,'EDI file transfered',1,'2017-01-09 08:39:35','X',1,0,1486445,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3087932.xml',0,0,'',0),(1626672,'EDI file transfered',1,'2017-01-09 08:39:35','X',1,0,1486476,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089081.xml',0,0,'',0),(1626673,'EDI file transfered',1,'2017-01-09 08:39:36','X',1,0,1486486,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089516.xml',0,0,'',0),(1626674,'EDI file transfered',1,'2017-01-09 08:39:36','X',1,0,1486494,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089840.xml',0,0,'',0),(1626675,'EDI file transfered',1,'2017-01-09 08:39:36','X',1,0,1486458,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088493.xml',0,0,'',0),(1626676,'EDI file transfered',1,'2017-01-09 08:39:36','X',1,0,1486499,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089914.xml',0,0,'',0),(1626677,'EDI file transfered',1,'2017-01-09 08:39:36','X',1,0,1486500,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089919.xml',0,0,'',0),(1626678,'EDI file transfered',1,'2017-01-09 08:39:37','X',1,0,1486450,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088220.xml',0,0,'',0),(1626679,'EDI file transfered',1,'2017-01-09 08:39:37','X',1,0,1486455,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3088309.xml',0,0,'',0),(1626680,'EDI file transfered',1,'2017-01-09 08:39:37','X',1,0,1486480,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089108.xml',0,0,'',0),(1626681,'EDI file transfered',1,'2017-01-09 08:39:37','X',1,0,1486463,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3088666.xml',0,0,'',0),(1626682,'EDI file transfered',1,'2017-01-09 08:39:37','X',1,0,1486470,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_CustomBlastService_3088882.xml',0,0,'',0),(1626683,'EDI file transfered',1,'2017-01-09 08:39:38','X',1,0,1486475,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089067.xml',0,0,'',0),(1626684,'EDI file transfered',1,'2017-01-09 08:39:38','X',1,0,1486485,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089436.xml',0,0,'',0),(1626685,'EDI file transfered',1,'2017-01-09 08:39:38','X',1,0,1486493,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089721.xml',0,0,'',0),(1626686,'EDI file transfered',1,'2017-01-09 08:39:38','X',1,0,1486457,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088393.xml',0,0,'',0),(1626687,'EDI file transfered',1,'2017-01-09 08:39:38','X',1,0,1486498,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089913.xml',0,0,'',0),(1626688,'EDI file transfered',1,'2017-01-09 08:39:38','X',1,0,1486449,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088116.xml',0,0,'',0),(1626689,'EDI file transfered',1,'2017-01-09 08:39:39','X',1,0,1486454,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3088285.xml',0,0,'',0),(1626690,'EDI file transfered',1,'2017-01-09 08:39:39','X',1,0,1486479,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089107.xml',0,0,'',0),(1626691,'EDI file transfered',1,'2017-01-09 08:39:39','X',1,0,1486462,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3088665.xml',0,0,'',0),(1626692,'EDI file transfered',1,'2017-01-09 08:39:39','X',1,0,1486469,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3088881.xml',0,0,'',0),(1626693,'EDI file transfered',1,'2017-01-09 08:39:39','X',1,0,1486504,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3089975.xml',0,0,'',0),(1626694,'EDI file transfered',1,'2017-01-09 08:39:40','X',1,0,1486474,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089025.xml',0,0,'',0),(1626695,'EDI file transfered',1,'2017-01-09 08:39:40','X',1,0,1486484,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089415.xml',0,0,'',0),(1626696,'EDI file transfered',1,'2017-01-09 08:39:40','X',1,0,1486492,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089636.xml',0,0,'',0),(1626697,'EDI file transfered',1,'2017-01-09 08:39:40','X',1,0,1486456,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3088359.xml',0,0,'',0),(1626698,'EDI file transfered',1,'2017-01-09 08:39:40','X',1,0,1486497,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089906.xml',0,0,'',0),(1626699,'EDI file transfered',1,'2017-01-09 08:39:40','X',1,0,1486448,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3087999.xml',0,0,'',0),(1626700,'EDI file transfered',1,'2017-01-09 08:39:40','X',1,0,1486478,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089101.xml',0,0,'',0),(1626701,'EDI file transfered',1,'2017-01-09 08:39:41','X',1,0,1486461,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088647.xml',0,0,'',0),(1626702,'EDI file transfered',1,'2017-01-09 08:39:41','X',1,0,1486468,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3088778.xml',0,0,'',0),(1626703,'EDI file transfered',1,'2017-01-09 08:39:41','X',1,0,1486503,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3089974.xml',0,0,'',0),(1626704,'EDI file transfered',1,'2017-01-09 08:39:41','X',1,0,1486453,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3088275.xml',0,0,'',0),(1626705,'EDI file transfered',1,'2017-01-09 08:39:41','X',1,0,1486473,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089024.xml',0,0,'',0),(1626706,'EDI file transfered',1,'2017-01-09 08:39:41','X',1,0,1486483,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089386.xml',0,0,'',0),(1626707,'EDI file transfered',1,'2017-01-09 08:39:42','X',1,0,1486491,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089588.xml',0,0,'',0),(1626708,'EDI file transfered',1,'2017-01-09 08:39:42','X',1,0,1486466,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3088772.xml',0,0,'',0),(1626709,'EDI file transfered',1,'2017-01-09 08:39:42','X',1,0,1486488,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3089518.xml',0,0,'',0),(1626710,'EDI file transfered',1,'2017-01-09 08:39:42','X',1,0,1486496,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3089886.xml',0,0,'',0),(1626711,'EDI file transfered',1,'2017-01-09 08:39:42','X',1,0,1486447,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3087998.xml',0,0,'',0),(1626712,'EDI file transfered',1,'2017-01-09 08:39:42','X',1,0,1486460,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_3088566.xml',0,0,'',0),(1626713,'EDI file transfered',1,'2017-01-09 08:39:42','X',1,0,1486467,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3088777.xml',0,0,'',0),(1626714,'EDI file transfered',1,'2017-01-09 08:39:43','X',1,0,1486502,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_3089933.xml',0,0,'',0),(1626715,'EDI file transfered',1,'2017-01-09 08:39:43','X',1,0,1486452,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockIndustrialServices_3088274.xml',0,0,'',0),(1626716,'PO #: , EDI file submitted for transfer',1,'2017-01-09 08:54:03','X',1,0,1486505,0,NULL,0,0,NULL,0),(1626717,'PO #: , EDI file submitted for transfer',1,'2017-01-09 08:54:03','X',1,0,1486506,0,NULL,0,0,NULL,0),(1626718,'PO #: , EDI file submitted for transfer',1,'2017-01-09 08:54:04','X',1,0,1486507,0,NULL,0,0,NULL,0),(1626719,'PO #: , EDI file submitted for transfer',1,'2017-01-09 08:54:04','X',1,0,1486508,0,NULL,0,0,NULL,0),(1626720,'PO #: , EDI file submitted for transfer',1,'2017-01-09 08:54:04','X',1,0,1486509,0,NULL,0,0,NULL,0),(1626721,'EDI file transfered',1,'2017-01-09 08:54:08','X',1,0,1486505,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_9900188112.xml',0,0,'',0),(1626722,'EDI file transfered',1,'2017-01-09 08:54:09','X',1,0,1486506,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-West_9900190297.xml',0,0,'',0),(1626723,'EDI file transfered',1,'2017-01-09 08:54:09','X',1,0,1486507,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_9900196185.xml',0,0,'',0),(1626724,'EDI file transfered',1,'2017-01-09 08:54:09','X',1,0,1486508,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_9900196186.xml',0,0,'',0),(1626725,'EDI file transfered',1,'2017-01-09 08:54:09','X',1,0,1486509,0,'C:\\Comparatio EDI\\EDI Converter\\ftp\\brock\\Invoice_BrockServicesLLC-South_9900202785.xml',0,0,'',0),(1626726,'XML file processed',1,'2017-01-12 13:09:18','X',1,0,1486510,0,NULL,0,0,NULL,0),(1626727,'EDI file held, Reference # WO453, PO # 453',1,'2017-01-12 13:09:18','X',1,0,1486510,0,'',0,0,'',0),(1626728,'XML file processed',1,'2017-01-12 13:10:36','X',1,0,1486511,0,NULL,0,0,NULL,0),(1626729,'EDI file held, Reference # WO453, PO # 453',1,'2017-01-12 13:10:37','X',1,0,1486511,0,'',0,0,'',0),(1626730,'User logged in',1,'2017-01-12 13:42:41','X',0,0,0,1,NULL,0,0,NULL,0);
/*!40000 ALTER TABLE `lg1_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES ('m000000_000000_base',1527798677),('m170823_080921_users',1527798725);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `no1_numbers`
--

DROP TABLE IF EXISTS `no1_numbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `no1_numbers` (
  `NO1_TYPE` varchar(45) NOT NULL DEFAULT '',
  `NO1_NUMBER` varchar(45) DEFAULT NULL,
  `CU1_ID` int(11) NOT NULL DEFAULT '0',
  `VD1_ID` int(11) NOT NULL DEFAULT '0',
  `NO1_TEST_MODE` int(11) NOT NULL DEFAULT '0',
  `CO1_ID` bigint(20) DEFAULT '0',
  PRIMARY KEY (`NO1_TYPE`,`CU1_ID`,`VD1_ID`,`NO1_TEST_MODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `no1_numbers`
--

LOCK TABLES `no1_numbers` WRITE;
/*!40000 ALTER TABLE `no1_numbers` DISABLE KEYS */;
INSERT INTO `no1_numbers` (`NO1_TYPE`, `NO1_NUMBER`, `CU1_ID`, `VD1_ID`, `NO1_TEST_MODE`, `CO1_ID`) VALUES ('INVOICE','9900202785',1,0,1,0);
/*!40000 ALTER TABLE `no1_numbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `us1_user`
--

DROP TABLE IF EXISTS `us1_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `us1_user` (
  `US1_ID` int(11) NOT NULL AUTO_INCREMENT,
  `US1_TYPE` int(10) unsigned NOT NULL DEFAULT '0',
  `US1_LOGIN` varchar(100) DEFAULT NULL,
  `US1_PASS` varchar(200) DEFAULT NULL,
  `US1_NAME` varchar(200) DEFAULT NULL,
  `US1_EMAIL` varchar(200) DEFAULT NULL,
  `CU1_ID` int(11) DEFAULT NULL,
  `VD1_ID` int(11) DEFAULT NULL,
  `US1_CREATED_BY` varchar(10) NOT NULL DEFAULT '',
  `US1_CREATED_ON` date NOT NULL DEFAULT '0000-00-00',
  `US1_MODIFIED_BY` varchar(10) NOT NULL DEFAULT '',
  `US1_MODIFIED_ON` date NOT NULL DEFAULT '0000-00-00',
  `US1_SHOW_DEFAULT` varchar(1) NOT NULL DEFAULT 'X',
  `CO1_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`US1_ID`),
  KEY `US1_LOGIN` (`US1_LOGIN`),
  KEY `US1_PASS` (`US1_PASS`),
  KEY `US1_NAME` (`US1_NAME`),
  KEY `US1_EMAIL` (`US1_EMAIL`),
  KEY `CU1_ID` (`CU1_ID`),
  KEY `US1_SHOW_DEFAULT` (`US1_SHOW_DEFAULT`),
  KEY `US1_CREATED_BY` (`US1_CREATED_BY`),
  KEY `US1_CREATED_ON` (`US1_CREATED_ON`),
  KEY `US1_MODIFIED_BY` (`US1_MODIFIED_BY`),
  KEY `US1_MODIFIED_ON` (`US1_MODIFIED_ON`),
  KEY `US1_TYPE` (`US1_TYPE`),
  KEY `VD1_ID` (`VD1_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `us1_user`
--

LOCK TABLES `us1_user` WRITE;
/*!40000 ALTER TABLE `us1_user` DISABLE KEYS */;
INSERT INTO `us1_user` (`US1_ID`, `US1_TYPE`, `US1_LOGIN`, `US1_PASS`, `US1_NAME`, `US1_EMAIL`, `CU1_ID`, `VD1_ID`, `US1_CREATED_BY`, `US1_CREATED_ON`, `US1_MODIFIED_BY`, `US1_MODIFIED_ON`, `US1_SHOW_DEFAULT`, `CO1_ID`) VALUES (1,9,'admin','edi345','Administrator','Jan.Poehland@comparatio.com',NULL,NULL,'','0000-00-00','','0000-00-00','X',0);
/*!40000 ALTER TABLE `us1_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES (7,'admin','admin','SH-66pB3dsg_LbAMcl41H7gjGJ3OP5Ng','$2y$13$rk2jAbWx/kxpb3B62iuCEeZBuadRpY9I0cM0Ksf/dGlr32VX266ie',NULL,'test@test.ru',10,1527806637,1527806637);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vd1_vendor`
--

DROP TABLE IF EXISTS `vd1_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd1_vendor` (
  `VD1_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `VENDOR_ID` varchar(10) DEFAULT NULL,
  `VD1_NAME` varchar(100) NOT NULL,
  `VD1_CREATED_BY` int(11) NOT NULL DEFAULT '0',
  `VD1_CREATED_ON` datetime DEFAULT NULL,
  `VD1_MODIFIED_BY` int(11) NOT NULL DEFAULT '0',
  `VD1_MODIFIED_ON` datetime DEFAULT NULL,
  `VD1_SHOW_DEFAULT` varchar(1) NOT NULL DEFAULT 'X',
  `VD1_TEST_MODE` int(11) DEFAULT '0',
  `VD1_COMPANY_ID` varchar(20) DEFAULT '',
  `VD1_RECEIVE_EDI` int(11) NOT NULL DEFAULT '0',
  `VD1_SEND_EDI_PO` int(11) DEFAULT '0',
  `VD1_SEND_ACKNOWLEDGEMENT` int(11) DEFAULT '0',
  `VD1_PO_FORMAT` int(11) DEFAULT '0',
  `VD1_SEND_FTP` int(10) NOT NULL DEFAULT '0',
  `VD1_SEND_SFTP` int(11) DEFAULT '0',
  `VD1_POST_HTTP` int(11) NOT NULL DEFAULT '0',
  `VD1_RECEIVE_FTP` int(10) NOT NULL DEFAULT '0',
  `VD1_PICKUP_FTP` int(10) NOT NULL DEFAULT '0',
  `VD1_PICKUP_SFTP` int(11) DEFAULT '0',
  `VD1_RECEIVE_HTTP` int(11) NOT NULL DEFAULT '0',
  `VD1_REMOTE_FTP_SERVER` varchar(200) DEFAULT NULL,
  `VD1_REMOTE_FTP_USERNAME` varchar(200) DEFAULT NULL,
  `VD1_REMOTE_FTP_PASSWORD` varchar(200) DEFAULT NULL,
  `VD1_REMOTE_FTP_DIRECTORY_SEND` varchar(200) DEFAULT NULL,
  `VD1_REMOTE_FTP_DIRECTORY_PICKUP` varchar(200) DEFAULT NULL,
  `VD1_FTP_USER` varchar(200) DEFAULT NULL,
  `VD1_FTP_PASSWORD` varchar(200) DEFAULT NULL,
  `VD1_FTP_DIRECTORY` varchar(200) DEFAULT NULL,
  `VD1_REMOTE_HTTP_SERVER` varchar(200) DEFAULT NULL,
  `VD1_REMOTE_HTTP_USERNAME` varchar(100) DEFAULT '',
  `VD1_REMOTE_HTTP_PASSWORD` varchar(100) DEFAULT '',
  `VD1_SUPPLIER_CODE` varchar(45) DEFAULT NULL,
  `VD1_RECEIVER_QUALIFIER` varchar(2) DEFAULT NULL,
  `VD1_RECEIVER_ID` varchar(45) DEFAULT NULL,
  `VD1_FACILITY` varchar(45) DEFAULT NULL,
  `VD1_TRADING_PARTNER_QUALIFIER` varchar(2) DEFAULT NULL,
  `VD1_TRADING_PARTNER_ID` varchar(45) DEFAULT NULL,
  `VD1_TRADING_PARTNER_GS_ID` varchar(45) DEFAULT NULL,
  `VD1_FLAG` varchar(1) DEFAULT NULL,
  `VD1_X12_STANDARD` varchar(4) DEFAULT NULL,
  `VD1_EDI_VERSION` varchar(5) DEFAULT NULL,
  `VD1_DUNS` varchar(50) DEFAULT NULL,
  `VD1_SHARED_SECRET` varchar(100) DEFAULT NULL,
  `VD1_SEND_EDI_PO_CHANGE` int(11) DEFAULT '0',
  `VD1_SEND_ITEM_USAGE` int(11) DEFAULT '0',
  `VD1_ITEM_USAGE_FORMAT` int(11) DEFAULT '0',
  `VD1_ITEM_USAGE_SOURCE` varchar(255) DEFAULT NULL,
  `VD1_POST_AS2` int(11) DEFAULT '0',
  `VD1_RECEIVE_AS2` int(11) DEFAULT '0',
  `VD1_CXML_PAYLOAD_ID` varchar(255) DEFAULT NULL,
  `VD1_CHECK_P21_EDI_FLAG` int(11) DEFAULT '0',
  `VD1_SEND_EDI_PAYMENT_ADVICE` int(11) DEFAULT '0',
  `VD1_PAYMENT_ADVICE_FORMAT` int(11) DEFAULT '0',
  `VD1_BANK_ROUTING_NUMBER` varchar(50) DEFAULT NULL,
  `VD1_BANK_ACCOUNT_NUMBER` varchar(50) DEFAULT NULL,
  `VD1_AS2_CERTIFICATE_FILENAME` varchar(255) DEFAULT NULL,
  `VD1_AS2_RECEIVER_ID` varchar(255) DEFAULT NULL,
  `VD1_AS2_TRADING_PARTNER_ID` varchar(255) DEFAULT NULL,
  `VD1_PICKUP_DIRECTORY` varchar(255) DEFAULT NULL,
  `VD1_AS2_REQUEST_RECEIPT` int(11) DEFAULT '0',
  `VD1_AS2_SIGN_MESSAGES` int(11) DEFAULT '0',
  `VD1_AS2_KEY_LENGTH` varchar(10) DEFAULT '',
  `VD1_AS2_ENCRYPTION_ALGORITHM` varchar(15) DEFAULT '',
  `VD1_AS2_COMPATIBILITY_MODE` int(11) DEFAULT '0',
  `VD1_PAYMENT_QUALIFIER` varchar(40) DEFAULT '',
  `VD1_PAYMENT_ID` varchar(40) DEFAULT '',
  `VD1_MAP` varchar(50) DEFAULT NULL,
  `VD1_CHECK_ACKNOWLEDGEMENTS` int(11) DEFAULT '1',
  `VD1_FTP_DOWNLOAD_FILTER` varchar(255) DEFAULT '',
  `VD1_SFTP_PRIVATE_KEY_FILENAME` varchar(255) DEFAULT '',
  `VD1_SEND_EDI_RELEASE_SCHEDULE` int(11) DEFAULT '0',
  `VD1_RELEASE_SCHEDULE_FORMAT` int(11) DEFAULT '0',
  `CO1_ID` bigint(20) DEFAULT '0',
  PRIMARY KEY (`VD1_ID`),
  KEY `CU1_SHORT_CODE` (`VENDOR_ID`),
  KEY `CU1_NAME` (`VD1_NAME`),
  KEY `CU1_SHOW_DEFAULT` (`VD1_SHOW_DEFAULT`),
  KEY `CU1_CREATED_BY` (`VD1_CREATED_BY`),
  KEY `CU1_CREATED_ON` (`VD1_CREATED_ON`),
  KEY `CU1_MODIFIED_BY` (`VD1_MODIFIED_BY`),
  KEY `CU1_MODIFIED_ON` (`VD1_MODIFIED_ON`),
  KEY `CU1_INVOICE_TYPE_CXML` (`VD1_SEND_FTP`),
  KEY `CU1_INVOICE_TYPE_EXCEL` (`VD1_RECEIVE_FTP`),
  KEY `CU1_SEND_PDF_COPY` (`VD1_PICKUP_FTP`),
  KEY `CU1_ORDER_FTP_USER` (`VD1_FTP_USER`),
  KEY `VD1_PO_FORMAT` (`VD1_PO_FORMAT`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd1_vendor`
--

LOCK TABLES `vd1_vendor` WRITE;
/*!40000 ALTER TABLE `vd1_vendor` DISABLE KEYS */;
INSERT INTO `vd1_vendor` (`VD1_ID`, `VENDOR_ID`, `VD1_NAME`, `VD1_CREATED_BY`, `VD1_CREATED_ON`, `VD1_MODIFIED_BY`, `VD1_MODIFIED_ON`, `VD1_SHOW_DEFAULT`, `VD1_TEST_MODE`, `VD1_COMPANY_ID`, `VD1_RECEIVE_EDI`, `VD1_SEND_EDI_PO`, `VD1_SEND_ACKNOWLEDGEMENT`, `VD1_PO_FORMAT`, `VD1_SEND_FTP`, `VD1_SEND_SFTP`, `VD1_POST_HTTP`, `VD1_RECEIVE_FTP`, `VD1_PICKUP_FTP`, `VD1_PICKUP_SFTP`, `VD1_RECEIVE_HTTP`, `VD1_REMOTE_FTP_SERVER`, `VD1_REMOTE_FTP_USERNAME`, `VD1_REMOTE_FTP_PASSWORD`, `VD1_REMOTE_FTP_DIRECTORY_SEND`, `VD1_REMOTE_FTP_DIRECTORY_PICKUP`, `VD1_FTP_USER`, `VD1_FTP_PASSWORD`, `VD1_FTP_DIRECTORY`, `VD1_REMOTE_HTTP_SERVER`, `VD1_REMOTE_HTTP_USERNAME`, `VD1_REMOTE_HTTP_PASSWORD`, `VD1_SUPPLIER_CODE`, `VD1_RECEIVER_QUALIFIER`, `VD1_RECEIVER_ID`, `VD1_FACILITY`, `VD1_TRADING_PARTNER_QUALIFIER`, `VD1_TRADING_PARTNER_ID`, `VD1_TRADING_PARTNER_GS_ID`, `VD1_FLAG`, `VD1_X12_STANDARD`, `VD1_EDI_VERSION`, `VD1_DUNS`, `VD1_SHARED_SECRET`, `VD1_SEND_EDI_PO_CHANGE`, `VD1_SEND_ITEM_USAGE`, `VD1_ITEM_USAGE_FORMAT`, `VD1_ITEM_USAGE_SOURCE`, `VD1_POST_AS2`, `VD1_RECEIVE_AS2`, `VD1_CXML_PAYLOAD_ID`, `VD1_CHECK_P21_EDI_FLAG`, `VD1_SEND_EDI_PAYMENT_ADVICE`, `VD1_PAYMENT_ADVICE_FORMAT`, `VD1_BANK_ROUTING_NUMBER`, `VD1_BANK_ACCOUNT_NUMBER`, `VD1_AS2_CERTIFICATE_FILENAME`, `VD1_AS2_RECEIVER_ID`, `VD1_AS2_TRADING_PARTNER_ID`, `VD1_PICKUP_DIRECTORY`, `VD1_AS2_REQUEST_RECEIPT`, `VD1_AS2_SIGN_MESSAGES`, `VD1_AS2_KEY_LENGTH`, `VD1_AS2_ENCRYPTION_ALGORITHM`, `VD1_AS2_COMPATIBILITY_MODE`, `VD1_PAYMENT_QUALIFIER`, `VD1_PAYMENT_ID`, `VD1_MAP`, `VD1_CHECK_ACKNOWLEDGEMENTS`, `VD1_FTP_DOWNLOAD_FILTER`, `VD1_SFTP_PRIVATE_KEY_FILENAME`, `VD1_SEND_EDI_RELEASE_SCHEDULE`, `VD1_RELEASE_SCHEDULE_FORMAT`, `CO1_ID`) VALUES (1,'123456','Test',1,'2014-07-25 00:00:00',0,NULL,'X',1,'',1,1,1,0,0,0,0,0,0,0,0,'','','','out','in','test','','',NULL,'','','1111111','12','1111111',NULL,'01','111111111','1111111','T','4010','00200',NULL,NULL,0,0,0,'',0,0,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,0,0,'','',0,'','',NULL,1,'','',0,0,0);
/*!40000 ALTER TABLE `vd1_vendor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-05 14:38:33
