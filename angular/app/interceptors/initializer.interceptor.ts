import {AuthService} from "../auth/auth.service";
import {Injector} from "@angular/core";
import {LangService} from "../services/lang.service";

export function onAppInit(injector: Injector) {
    return () => new Promise((resolve, reject) => {

        if( injector.get(AuthService).isAuthenticated ) {
             injector.get(AuthService).loadPermissions();
             injector.get(AuthService).initExpDetection();
        }
        injector.get(LangService).initLanguages().then(function(arg) {
          resolve();
        });
    });
}

