import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable, Injector} from '@angular/core';
import {Observable, TimeoutError} from 'rxjs'
import {AuthService} from "../auth/auth.service";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private injector: Injector) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request)
        .catch((err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.error('An error occurred:', err.error.message);
          } else {
            var message = `Requested url: ${err.url}\n` +
                          `Returned status ${err.status}: ${err.statusText}\n` +
                          `Error: ${err.error.error}\n`+
                          `-----Returned content-----\n`+
                          `${err.error.text}`;
            console.log(message);
            if(err.status == 401){
              this.injector.get(AuthService).logout();
            }
          }

          // ...optionally return a default fallback value so app can continue (pick one)
          // which could be a default value
          return Observable.of(new HttpResponse({body: [
            {name: 'Default values returned by Interceptor', id: 88},
            {name: 'Default values returned by Interceptor(2)', id: 89}
          ]}));
        });
    }
}
