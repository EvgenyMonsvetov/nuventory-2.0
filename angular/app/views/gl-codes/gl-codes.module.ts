import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPermissionsModule } from 'ngx-permissions';
import { routes } from './gl-codes.routing';
import { GlCodesComponent } from './gl-codes.component';
import { GlCodeFormComponent } from './gl-code-form/gl-code-form.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
    declarations: [
        GlCodesComponent,
        GlCodeFormComponent
    ]
})
export class GlCodesModule { }
