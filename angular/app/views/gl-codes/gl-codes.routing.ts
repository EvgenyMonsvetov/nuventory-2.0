import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions }from './../../permissions';
import { GlCodesComponent } from './gl-codes.component';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { GlCodeFormComponent } from './gl-code-form/gl-code-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: GlCodesComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Gl Codes'),
        permissions: {
            only: [permissions.GL_CODES_READ]
        }
    }
},{
    path: 'add',
    component: GlCodeFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Add'),
        permissions: {
            only: [permissions.GL_CODES_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: GlCodeFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Edit#'),
        permissions: {
            only: [permissions.GL_CODES_EDIT]
        }
    }
},{
    path: 'copy/:id',
    component: GlCodeFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Clone'),
        permissions: {
            only: [permissions.GL_CODES_ADD]
        }
    }
}];