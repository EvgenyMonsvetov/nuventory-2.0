import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlCodeFormComponent } from './gl-code-form.component';

describe('GlCodeFormComponent', () => {
  let component: GlCodeFormComponent;
  let fixture: ComponentFixture<GlCodeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlCodeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlCodeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
