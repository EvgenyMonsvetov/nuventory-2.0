import { Component, OnInit } from '@angular/core';
import { GlCodesService } from '../../../services/glcodes.service';
import { ActivatedRoute } from '@angular/router';
import { permissions } from '../../../permissions';
import { CompanyFinderModalComponent } from '../../../components/company-finder-modal/company-finder-modal.component';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { _lang } from '../../../pipes/lang';
import { HeaderService } from '../../../services/header.service';
import { AuthService } from '../../../auth/auth.service';
import { GroupsService } from '../../../services/groups';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-form',
  templateUrl: './gl-code-form.component.html',
  styleUrls: ['./gl-code-form.component.scss'],
  providers: [
    GlCodesService
  ],
  preserveWhitespaces: true
})
export class GlCodeFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';
  private constants = Constants;
  private permissions = permissions;
  private record: any;
  private allowEditCompany: boolean = false;

  constructor(
      private route: ActivatedRoute,
      private glCodesService: GlCodesService,
      public dialog: MatDialog,
      private headerService: HeaderService,
      private auth: AuthService
  ) {
  }

  ngOnInit() {
    this.record = this.glCodesService.initialize();
    this.action = this.route.snapshot.url[0].toString();
    if (this.action === 'add') {
      this.record['CO1_ID'] = this.headerService.CO1_ID > 0 ? this.headerService.CO1_ID : 0;
      this.record['CO1_NAME'] = this.headerService.CO1_ID > 0 && this.headerService.CO1_NAME ? this.headerService.CO1_NAME : '';
    } else {
      this.glCodesService.getById(this.route.snapshot.params.id || 0).subscribe( data => {
        data['GL1_NO_STATEMENT'] = parseInt(data['GL1_NO_STATEMENT']);
        this.record = data;
      });
    }
    this.allowEditCompany = this.auth.accessLevel === GroupsService.GROUP_ADMIN;
  }

  cancelClick(){
    window.history.back();
  }

  openLanguageEditor() {
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('Gl Code Form'),
            componentRef: this,
            data: [],
            componentName: 'GlCodeFormComponent'
          },
      });
  }

  saveClick() {
    if (this.action === 'add' || this.action === 'copy') {
      this.glCodesService.create(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.glCodesService.update(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  selectCompany() {
    let dialogRef = this.dialog.open(CompanyFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Company'), CO1_ID: this.record.CO1_ID},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CO1_NAME = selected.CO1_NAME;
        this.record.CO1_ID   = selected.CO1_ID;
      }
    })
  }

  clearCompany() {
    this.record.CO1_NAME = '';
    this.record.CO1_ID   = '';
  }
}
