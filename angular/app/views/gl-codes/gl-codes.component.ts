import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { GlCodesService } from '../../services/glcodes.service';
import { TitleService } from 'app/services/title.service';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Subscription } from 'rxjs';
import { HeaderService } from '../../services/header.service';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-gl-codes',
  templateUrl: './gl-codes.component.html',
  styleUrls: ['./gl-codes.component.scss'],
  providers: [
    GlCodesService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class GlCodesComponent implements OnInit, OnDestroy {

    private permissions = permissions;

    @ViewChild('masterDataView')    masterDataView: ElementRef;
    @ViewChild('actionbar')         actionbar: ElementRef;
    @ViewChild('content')           content: ElementRef;
    @ViewChild('tableHeader')       tableHeader: ElementRef;
    @ViewChild('paginator')         paginator: ElementRef;

    cols: any[] = [];
    items: any[] = [];

    private loading = false;
    private page = new Page();
    private selected: any = null;
    private size: number = 50;
    private totalRecords: number = 0;
    private subscription: Subscription;

    constructor(
        private toastr: ToastrService,
        private glCodesService: GlCodesService,
        public  dialog: MatDialog,
        private router: Router,
        private confirmationService: ConfirmationService,
        private titleService: TitleService,
        private route: ActivatedRoute,
        private headerService: HeaderService
    ) {
        this.page.size = this.size;
        this.page.pageNumber = 0;

        this.subscription = this.headerService.subscribe({
          next: (v) => {
            this.page.pageNumber = 0;
            this.getItems();
          }
      });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngOnInit() {
        this.titleService.setNewTitle(this.route);

        this.cols = [
            { field: 'CO1_NAME',             header: _lang('CO1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 100 },
            { field: 'GL1_NAME',             header: _lang('GL1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 100 },
            { field: 'GL1_DESCRIPTION',      header: _lang('GL1_DESCRIPTION'),     visible: true, export: {visible: true, checked: true}, width: 100 },
            { field: 'GL1_NO_STATEMENT',     header: _lang('GL1_NO_STATEMENT'),    visible: true, export: {visible: true, checked: true}, width: 100 }
        ];

        this.getItems();
    }

    getItems() {
        this.glCodesService.getItems(this.page).subscribe(res => {
                this.items = res.data;
                this.totalRecords = res.count;

                this.loading = false;
            },
            error => {
                this.loading = false;
                this.toastr.error(_lang('Gl Codes') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                });
            });
    }

    onPage(event) {
        this.loading = true;
        this.page.size = event.rows;
        this.page.pageNumber = (event.first / event.rows);
        this.getItems();
    }

    openLanguageEditor() {
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: window.innerWidth-60+'px',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: _lang('Gl Code List'),
                componentRef: this,
                data: [],
                componentName: 'GlCodesComponent'
            },
        });
    }

    addClick(){
        this.router.navigate(['gl-codes/add']);
    }

    cloneRecord() {
        if (this.selected) {
            this.router.navigate(['gl-codes/copy/' + this.selected.GL1_ID]);
        }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.glCodesService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord(rowData?) {
      if (this.selected || rowData) {
          const GL1_ID = rowData && rowData.GL1_ID ? rowData.GL1_ID : this.selected[0].GL1_ID;
          this.router.navigate(['gl-codes/edit/' + GL1_ID]);
      }
    }

    deleteRecord() {
        if (this.selected.length && this.selected[0]['GL1_ID']) {
            this.confirmationService.confirm({
                message: _lang('Are you sure that you want to perform this action?'),
                accept: () => {
                    this.loading = true;
                    var GL1_IDs = this.selected.map( item => item['GL1_ID'] );
                    this.glCodesService.deleteMultiple(GL1_IDs).subscribe(result => {
                        this.getItems();
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('On delete GLCode error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
                }
            });
        }
    }
}
