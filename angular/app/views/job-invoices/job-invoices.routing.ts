import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { JobInvoicesComponent } from './job-invoices.component';
import { JobInvoiceFormComponent } from './job-invoice-form/job-invoice-form.component';

export const routes: Routes = [{
    path: '',
    component: JobInvoicesComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Job Invoices'),
        permissions: {
            only: [permissions.INVOICES_READ]
        }
    }
},{
    path: ':id',
    component: JobInvoiceFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.INVOICES_READ]
        }
    }
}];