import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobInvoiceFormComponent } from './job-invoice-form.component';

describe('JobInvoiceFormComponent', () => {
  let component: JobInvoiceFormComponent;
  let fixture: ComponentFixture<JobInvoiceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobInvoiceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobInvoiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
