import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { JobInvoiceService } from '../../../services/job-invoice.service';
import { JobInvoiceLineService } from '../../../services/job-invoice-line.service';
import { permissions } from '../../../permissions';
import { _lang } from '../../../pipes/lang';
import { Page } from '../../../model/page';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';

@Component({
  selector: 'app-job-invoice-form',
  templateUrl: './job-invoice-form.component.html',
  styleUrls: ['./job-invoice-form.component.scss'],
  providers: [
    JobInvoiceService,
    JobInvoiceLineService
  ],
  preserveWhitespaces: true
})
export class JobInvoiceFormComponent implements OnInit {
  @ViewChild('contentGrid')   contentGrid: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private record: any;

  private statusData: Array<any> = [
    {'label': _lang('#Open#'), 'IJ1_STATUS' : 0},
    {'label': _lang('#Received#'), 'IJ1_STATUS' : 1}
  ];

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private loading: boolean = true;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private jobInvoiceService: JobInvoiceService,
              private jobInvoiceLineService: JobInvoiceLineService,
              public  dialog: MatDialog,
              private titleService: TitleService,
              private toastr: ToastrService,
              private location: Location) {
    this.page.filterParams = {};
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'IJ2_LINE',                header: _lang('IJ2_LINE'),          visible: true, width: 90 },
      { field: 'IJ2_DESC1',               header: _lang('IJ2_DESC1'),         visible: true, width: 250 },
      { field: 'UM1_NAME',                header: _lang('MD1_UM'),            visible: true, width: 110 },
      { field: 'VD1_NAME',                header: _lang('VD1_NAME'),          visible: true, width: 110 },
      { field: 'GL1_NAME',                header: _lang('GL1_NAME'),          visible: true, width: 110 },
      { field: 'IJ2_UNIT_PRICE',          header: _lang('IJ2_UNIT_PRICE'),    visible: true, width: 100 },
      { field: 'MODIFIED_ON',             header: _lang('#ModifiedOn#'),      visible: true, width: 120 },
      { field: 'MODIFIED_BY',             header: _lang('#ModifiedBy#'),      visible: true, width: 160 },
      { field: 'CREATED_ON',              header: _lang('#CreatedOn#'),       visible: false, width: 120 },
      { field: 'CREATED_BY',              header: _lang('#CreatedBy#'),       visible: false, width: 160 }
    ];

    this.record = this.jobInvoiceService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.page.filterParams['IJ1_ID'] = this.route.snapshot.params.id;
      this.getItems();
      this.jobInvoiceService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
        if (result[0]) {
          let data = result[0];
          data['IJ1_ID'] = parseInt(data['IJ1_ID']);
          data['IJ1_PRINT_FLAG'] = parseInt(data['IJ1_PRINT_FLAG']);
          this.record = data;
        }
      });
    }
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.jobInvoiceLineService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Job Invoice lines') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Job Invoice Form'),
        componentRef: this,
        data: [],
        componentName: 'JobInvoiceFormComponent'
      },
    });
  }
}
