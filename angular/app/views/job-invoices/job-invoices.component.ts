import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { formatDate } from '@angular/common';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';
import { Subject, Subscription } from 'rxjs';
import { JobInvoiceService } from '../../services/job-invoice.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { HeaderService } from '../../services/header.service';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'app-job-invoices',
  templateUrl: './job-invoices.component.html',
  styleUrls: ['./job-invoices.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class JobInvoicesComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  formatDate = formatDate;

  @ViewChild('jobInvoicesView') jobInvoicesView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';

  /** control for the selected filters */
  public modifiedByCtrl: FormControl = new FormControl();
  public modifiedByFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyModifiedBy = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredModifiedByUsers: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  invStatuses: any[];
  statuses: any[];
  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;
  sub: Subscription;
  private subscription: Subscription;

  private users: Array<object> = [];
  private shops:   Array<object> = [];

  constructor(private jobInvoiceService: JobInvoiceService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = {};

    this.subscription = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.getItems();
      }
    });
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.statuses = [
      { label: _lang('#AllRecords#'), value: null},
      { label: _lang('Printed'), value: 1},
      { label: _lang('Not Printed'), value: 2}
    ];

    this.cols = [
      { field: 'IJ1_NUMBER',      header: _lang('IJ1_NUMBER'),      visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'JT1_NUMBER',      header: _lang('JT1_NUMBER'),      visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'IJ1_PRINT_FLAG',  header: _lang('IJ1_PRINT_FLAG'),  visible: true, export: {visible: true, checked: true}, width: 110 },
      { field: 'IJ1_TOTAL_VALUE', header: _lang('IJ1_TOTAL_VALUE'), visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'IJ1_TOTAL_SELL',  header: _lang('IJ1_TOTAL_SELL'),  visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'IJ1_COMMENT',     header: _lang('IJ1_COMMENT'),     visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MODIFIED_ON',     header: _lang('#ModifiedOn#'),    visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),    visible: true, export: {visible: true, checked: true}, width: 160 }
    ];

    this.jobInvoiceService.preloadData().subscribe( data => {
      this.prepareData(data);
      this.getItems();
    });

    this.modifiedByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyModifiedBy))
        .subscribe(() => {
          this.filterModifiedByUsers();
        });
  }

  private filterModifiedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.modifiedByFilterCtrl.value;
    if (!search) {
      this.filteredModifiedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Modified By Users
    this.filteredModifiedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.users = [{US1_ID: null, US1_NAME: _lang('#AllRecords#')}];
    let usersArr = data.users.map( user => {
      return {
        US1_ID:   user.US1_ID,
        US1_NAME: user.US1_NAME
      }
    }).filter( customer => {
      return customer.US1_NAME && customer.US1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.US1_NAME < b.US1_NAME)
        return -1;
      if (a.US1_NAME > b.US1_NAME)
        return 1;
      return 0;
    });
    this.users = this.users.concat(usersArr);
    this.filterModifiedByUsers();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);

  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscription)
      this.subscription.unsubscribe();
    this._onDestroyModifiedBy.next();
    this._onDestroyModifiedBy.complete();
  }

  getScrollHeight(): number {
    return (this.jobInvoicesView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.jobInvoiceService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Job Invoices') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Job Ticket Invoice List'),
        componentRef: this,
        data: [],
        componentName: 'JobInvoicesComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970') {
      this.page.filterParams[col] = '';
    } else {
      this.page.filterParams[col] = value;
    }
    this.page.pageNumber = 0;
    this.getItems();
  }

  onOpen() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['job-invoices/' + this.selected[0]['IJ1_ID']]);
  }

  editRecord(rowData) {
    this.router.navigate(['job-invoices/' + rowData['IJ1_ID']]);
  }

  onInvoice() {

  }

  onPdfPrint(option = null) {
    if (this.selected && this.selected.length) {
      if (option)
        this.page.filterParams['option'] = option;

      const IJ1_IDs: any[] = [];
      this.selected.forEach(function (item) {
        IJ1_IDs.push(item['IJ1_ID']);
      });
      this.page.filterParams['IJ1_IDs'] = IJ1_IDs;

      this.jobInvoiceService.printPDF(this.page).subscribe( result => {
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
      this.page.filterParams['option'] = null;
      this.page.filterParams['IJ1_IDs'] = null;
    }
  }

  onPdfPrintWithCost() {
    this.onPdfPrint(1);
  }

  onDelete() {
    if (this.selected.length && this.selected[0]['IJ1_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.jobInvoiceService.delete(this.selected[0]['IJ1_ID']).subscribe( result => {
                this.selected = null;
                this.getItems();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete invoice error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.jobInvoiceService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }
}
