import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './job-invoices.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { JobInvoicesComponent } from './job-invoices.component';
import { JobInvoiceFormComponent } from './job-invoice-form/job-invoice-form.component';
import { JobInvoiceService } from '../../services/job-invoice.service';
import { CurrencyModule } from '../../components/currency/currency.module';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        CurrencyModule
    ],
    providers: [
        JobInvoiceService
    ],
    declarations: [
        JobInvoicesComponent,
        JobInvoiceFormComponent
    ]
})
export class JobInvoicesModule { }