import { NgModule } from '@angular/core';
import {
  MatMenuModule,
  MatButtonModule,
  MatFormFieldModule,
  MatCardModule,
  MatDividerModule}
  from "@angular/material";
import {MatTabsModule} from '@angular/material/tabs';
import {MatMenuItem} from "@angular/material";
import {MatExpansionModule} from "@angular/material";
import {MatSelectModule} from "@angular/material";
import {MatOptionModule} from "@angular/material";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from "@angular/material";
import {MatNativeDateModule} from "@angular/material";
import {MatIconModule} from "@angular/material";
import {MatCheckboxModule} from "@angular/material";
import {MatListModule} from "@angular/material";
import {MatProgressSpinnerModule} from "@angular/material";
import {MatButtonToggleModule} from "@angular/material";


@NgModule({
  imports: [
    MatFormFieldModule,
    MatMenuModule,
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatSelectModule,
    MatOptionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatIconModule,
    MatCheckboxModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule
  ],
  exports: [
    MatFormFieldModule,
    MatMenuModule,
    MatButtonModule,
    MatTabsModule,
    MatCardModule,
    MatDividerModule,
    MatMenuItem,
    MatExpansionModule,
    MatSelectModule,
    MatOptionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatIconModule,
    MatCheckboxModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule
  ]
})
export class MaterialModule { }
