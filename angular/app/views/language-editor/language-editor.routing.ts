import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { LanguageEditorComponent } from './language-editor.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: LanguageEditorComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Language Editor'),
        permissions: {
            only: [permissions.LANGUAGES_EDIT]
        }
    }
}];