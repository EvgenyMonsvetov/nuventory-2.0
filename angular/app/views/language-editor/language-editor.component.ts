import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LangService } from '../../services/lang.service';
import { TitleService } from '../../services/title.service';
import { ActivatedRoute } from '@angular/router';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { MatDialog } from '@angular/material';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-language-editor',
  templateUrl: './language-editor.component.html',
  styleUrls: ['./language-editor.component.scss'],
  preserveWhitespaces: true
})
export class LanguageEditorComponent implements OnInit {
  private permissions = permissions;

  @ViewChild('languageEditor')  languageEditor: ElementRef;
  @ViewChild('actionbar')       actionbar:  ElementRef;
  @ViewChild('tableHeader')     tableHeader:  ElementRef;
  @ViewChild('tableFilter')       tableFilter: ElementRef;
  @ViewChild('paginator')       paginator:  ElementRef;

  private scrollHeight: string = '50px';

  selectedColumns: any[] = [];
  cols: any[] = [];
  items: any[] = [];

  //default columns
  key = { field: 'LA2_KEY', header: {label: 'Key', value: '0'}, visible: true, export: {visible: true, checked: true} };

  private pageLang = new Page();
  private sizeLang: number = 100;
  private pageLabel = new Page();
  private sizeLabel: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;
  private selected: any = null;
  private nameBeforeEdit: string;

  constructor(private langService: LangService,
              private toastr: ToastrService,
              public  dialog: MatDialog,
              private titleService: TitleService,
              private route: ActivatedRoute) {
    this.pageLang.size = this.sizeLang;
    this.pageLabel.size = this.sizeLabel;
    this.pageLabel.pageNumber = 0;
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.langService.getLanguages(this.pageLang).subscribe(result => {
      this.parseLanguages(result.data['currentVocabulary']);
      this.getLabels();
    },
    error => {
      this.toastr.error(_lang('Languages') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });

  }

  getScrollHeight(): number {
    return (this.languageEditor.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  //get all translation labels from DB
  getLabels() {
    let langs: any[] = [];
    this.selectedColumns.forEach( col => {
      if (col['visible']) {
        langs.push(col['header']['value']);
      }
    });
    this.pageLabel.filterParams['langs'] = langs.join(',');

    this.langService.getAllLabels(this.pageLabel)
        .then( res => {
              this.setLabels(res['currentVocabulary']);
              this.totalRecords = res['count'];
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Labels') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
  }

  //create dynamical columns from languages list
  parseLanguages(value) {
    this.cols = [this.key];

    for (var k = 0; k < value.length; k++) {
      let item = value[k];
      let obj = {field: item['LA1_ID'], visible: true, header: {label: item['LA1_NAME'], value: item['LA1_ID']}, export: {visible: true, checked: true}};
      this.cols.push(obj);
    }

    this.selectedColumns = this.cols.filter( item => {
      return item['field'] !== 'LA2_KEY';
    });
  }

  //create key objects with all translations
  setLabels(keys: any[]) {
    this.items = keys;

    this.items.forEach( item => {
      for (const key of Object.keys(item)) {
        item[key] = item[key] ? unescape(item[key]) : '';
      }
    });

    setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

    this.loading = false;
  }

  onEditInit(event) {
    this.nameBeforeEdit = event.data[event.field.field];
  }

  onPressEnter(event, rowData, data, id, colName) {
    if (event.keyCode === 13 || event.keyCode === 9) {
      this.onEditCell(rowData, data, id, colName);
    } else if (event.keyCode === 27) {
      rowData[colName['label']] = this.nameBeforeEdit;
    }
  }

  onEditCell(rowData, data, id, colName) {
    if (this.nameBeforeEdit !== data) {
      this.loading = true;

      this.langService.saveTranslation({'LA2_KEY': id, 'LA1_ID': colName.value, 'LA2_TEXT': data}).subscribe(result => {
        this.loading = false;
      },
      error => {
        this.loading = false;
        this.toastr.error(_lang('On label saving error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
    }
  }

  onPage(event) {
    this.loading = true;
    this.pageLabel.size = event.rows;
    this.pageLabel.pageNumber = (event.first / event.rows);
    this.getLabels();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    this.pageLabel.filterParams[col.header.value] = value;
    this.pageLabel.pageNumber = 0;
    this.getLabels();
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        const page: Page = new Page();
        page.filterParams['columns'] = columns;

        this.langService.downloadTranslationsXlsx(page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  columnsChanged(col, event) {
    this.filterChanged(col, '');
  }

}
