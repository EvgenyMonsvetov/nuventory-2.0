import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { ProjectedPoUsageReportComponent } from './projected-po-usage-report.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: ProjectedPoUsageReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Projected PO Usage Report'),
        permissions: {
            only: [permissions.PROJECTED_PO_USAGE_READ]
        }
    }
}];