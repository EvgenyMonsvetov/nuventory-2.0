import { OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Component } from '@angular/core';
import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import { MasterDataService } from '../../services/master.data.service';
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { TitleService } from '../../services/title.service';
import { Page } from '../../model/page';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from '../../services/header.service';
import { Subscription } from 'rxjs';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import {lang} from "moment";

@Component({
  selector: 'app-projected-po-usage-report',
  templateUrl: './projected-po-usage-report.component.html',
  styleUrls: ['./projected-po-usage-report.component.scss']
})
export class ProjectedPoUsageReportComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('projectedPOUsage', { read: ElementRef }) container: ElementRef;
  @ViewChild('projectedPOUsageView', { read: ElementRef }) chartView: ElementRef;
  private chart;

  private page = new Page();
  private sub: Subscription;
  private subscriptionHeader: Subscription;
  private headerSubscription: Subscription;

  private fontStyle = {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Roboto'
  };

  private budgetFontStyle = {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Roboto',
    color: 'red'
  };

  private slideShow: boolean = false;
  private graphHeight: string = '400px';

  constructor(private masterDataService: MasterDataService,
              private titleService: TitleService,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private router: Router,
              private headerService: HeaderService) {

    this.page.filterParams['LO1_ID'] = this.headerService.LO1_ID;
    this.page.filterParams['CO1_ID'] = this.headerService.CO1_ID;

    this.headerSubscription = this.headerService.subscribe({
      next: (v) => {
        this.page.filterParams['LO1_ID'] = this.headerService.LO1_ID;
        this.page.filterParams['CO1_ID'] = this.headerService.CO1_ID;
        this.getChartData();
      }
    });

    const eventUrl: string = this.router.url;
    this.slideShow = eventUrl.includes('/charts-slide-show/');
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    setTimeout(() => {
      if (this.slideShow) {
        this.graphHeight = this.getGraphHeight() + 'px';
      }
      setTimeout(() => {
        this.buildHighcharts([], 31, 31, 0, [], 0);
        this.getChartData();
      });
    });
  }

  getGraphHeight(): number {
    return (this.chartView.nativeElement.parentElement.parentElement.parentElement.parentElement.offsetHeight - 105);
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscriptionHeader)
      this.subscriptionHeader.unsubscribe();
    if (this.headerSubscription)
      this.headerSubscription.unsubscribe();
  }

  getChartData() {
    if (this.sub)
      this.sub.unsubscribe();

    this.chart.showLoading();

    this.sub = this.masterDataService.getProjectedPOUsage(this.page).subscribe(res => {
          this.chart.hideLoading();
          this.buildHighcharts(res['series'], res['today'], res['totalDays'], res['budget'], res['correlationData'], res['projectedAmount']);
        },
        error => {
          this.toastr.error(_lang('Projected PO Usage') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  buildHighcharts(series, today, totalDays, materialBudget, correlationData, projectedAmount = 0) {
    let titleText = _lang('Projected PO Usage MTD');
    const subTitleText = _lang('Projected Amount') + ': $' + projectedAmount.toFixed(2);

    Highcharts.setOptions({
      lang: {
        drillUpText: _lang('Previous')
      }
    });

    let me = this;
    this.chart = Highcharts.chart(this.container.nativeElement, {
      chart: {
        type: 'spline'
      },
      xAxis: {
        title: {
          text: 'Time/Day',
          style: this.fontStyle
        },
        type: 'category',
        labels: {
          style: this.fontStyle,
          step: 1
        },
        max: totalDays,
        plotLines: [
          {
            color: 'red',
            value: today,
            width: 1,
            zIndex: 4,
            dashStyle: 'LongDash'
          },
          {
            color: 'black',
            value: totalDays,
            width: 1,
            zIndex: 4,
            dashStyle: 'LongDash'
          }
        ]
      },
      yAxis: {
        title: {
          text: 'Usage ($)',
          style: this.fontStyle
        },
        labels: {
          style: this.fontStyle
        },
        minRange: materialBudget,
        min: 0,
        plotLines: [{
          color: 'red',
          value: materialBudget,
          width: 2,
          zIndex: 4,
          label: {
            text: _lang('Budget'),
            align: 'right',
            x: -30,
            style: this.budgetFontStyle
          }
        }]
      },
      title: {
        text: titleText
      },
      subtitle: me.slideShow ? {
        text: subTitleText
      } : null,
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          dataLabels: {
            enabled: false,
            format: '{point.y:.0f}',
            style: this.fontStyle
          }
        },
        column: {
          colorByPoint: true
        }
      },
      tooltip: {
        enabled: false,
        formatter: function() {
          return 'Usage: <b>$' + (this.y).toFixed(0) + '</b>';
        }
      },
      series: [{
        name: '',
        data: series,
        type: 'spline',
        marker: {
          enabled: false
        },
        enableMouseTracking: false
      },
      {
        type: 'line',
        name: 'Regression Line',
        data: correlationData,
        marker: {
          enabled: false
        },
        states: {
          hover: {
            lineWidth: 0
          }
        },
        enableMouseTracking: false
      }]
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Projected PO Usage'),
        componentRef: this,
        data: [],
        componentName: 'ProjectedPoUsageReportComponent'
      },
    });
  }

}
