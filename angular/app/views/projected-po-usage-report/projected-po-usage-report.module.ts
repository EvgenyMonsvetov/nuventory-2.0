import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './projected-po-usage-report.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MasterDataService } from '../../services/master.data.service';
import { ProjectedPoUsageReportComponent } from './projected-po-usage-report.component';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule
    ],
    providers: [
        MasterDataService
    ],
    declarations: [
        ProjectedPoUsageReportComponent
    ]
})
export class ProjectedPoUsageReportModule { }