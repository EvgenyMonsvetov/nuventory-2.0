import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from '../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { CountriesComponent } from './countries.component';
import { CountryFormComponent } from './country-form/country-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: CountriesComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Countries#'),
        permissions: {
            only: [permissions.COUNTRIES_READ]
        }
    }
},{
    path: 'add/:id',
    component: CountryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.COUNTRIES_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: CountryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.COUNTRIES_EDIT]
        }
    }
},{
    path: 'clone/:id',
    component: CountryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.COUNTRIES_ADD]
        }
    }
}];