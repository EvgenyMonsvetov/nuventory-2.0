import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RouterModule } from '@angular/router';
import { routes } from './countries.routing';
import { CountryService } from '../../services/country.service';
import { CountriesComponent } from './countries.component';
import { CountryFormComponent } from './country-form/country-form.component';
import { ConfirmationDialogComponent } from '../../components/confirmation-dialog/confirmation-dialog.component';
import { PaginationControlComponent } from '../../components/pagination-control/pagination-control.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  providers: [
      CountryService
  ],
  declarations: [
      CountriesComponent,
      CountryFormComponent,
      ConfirmationDialogComponent,
      PaginationControlComponent
  ],
  entryComponents : [ ConfirmationDialogComponent ],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA
  ]
})
export class CountriesModule { }
