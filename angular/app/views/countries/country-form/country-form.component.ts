import { Component, OnInit } from '@angular/core';
import { CountryService } from '../../../services/country.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from 'app/services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { _lang } from '../../../pipes/lang';
import { permissions } from '../../../permissions';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-country-add',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.scss'],
  preserveWhitespaces: true
})
export class CountryFormComponent implements OnInit {
  private permissions = permissions;
  private constants = Constants;

  private model:  any = {};
  private errors: any = {};
  private loading: boolean = false;
  private clone: boolean = false;
  private action: String = '';

  constructor(private router: Router,
              private route: ActivatedRoute,
              private toastr: ToastrService,
              public  dialog: MatDialog,
              private countryService: CountryService,
              private titleService: TitleService ) { }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    const countryId = this.route.snapshot.params.id;
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.queryParams.clone)
      this.clone = true;

    this.model.CY1_ID = 0;
    this.countryService.getById(countryId).subscribe( model => {
      this.model = model;
      if (this.clone)
        this.model.CY1_ID = 0;
    },
    error => {
      this.toastr.error(_lang('Country') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  cancelClick(): void {
    this.router.navigate(['/countries']);
  }

  saveClick(): void {
    this.loading = true;
    const isCreate = this.model.CY1_ID === 0;
    this.countryService.save(this.model, isCreate).subscribe( result => {
      this.loading = false;
      if (result.success) {
        this.cancelClick();
      } else {
        this.errors = result.errors;
      }
    },
    error => {
      this.toastr.error(_lang('On add country error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Country Form'),
        componentRef: this,
        data: [],
        componentName: 'CountryFormComponent'
      },
    });
  }
}
