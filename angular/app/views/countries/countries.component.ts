import { Component, OnInit } from '@angular/core';
import { CountryService } from '../../services/country.service';
import { permissions } from '../../permissions';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Page } from '../../model/page';
import { TitleService } from 'app/services/title.service';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { _lang } from '../../pipes/lang';
import { ConfirmationService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class CountriesComponent implements OnInit {
  public  permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  selectedCountry: any;
  totalRecords: number = 0;

  private loading: boolean = true;
  private page = new Page();

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  constructor(private countryService: CountryService,
              public dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'CY1_SHORT_CODE',  header: _lang('CY1_SHORT_CODE'),  visible: true, export: {visible: true, checked: true} },
      { field: 'CY1_NAME',        header: _lang('CY1_NAME'),        visible: true, export: {visible: true, checked: true} },
      { field: 'CREATED_ON',      header: _lang('#CreatedOn#'),     visible: true, export: {visible: true, checked: true} },
      { field: 'CREATED_BY',      header: _lang('#CreatedBy#'),     visible: true, export: {visible: true, checked: true} },
      { field: 'MODIFIED_ON',     header: _lang('#ModifiedOn#'),    visible: true, export: {visible: true, checked: true} },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),    visible: true, export: {visible: true, checked: true} }
    ];

    this.page.pageNumber = 0;
    this.page.size = 100;
    this.getCountries(null);
  }

  getCountries(params) {
    this.loading = true;
    this.selectedCountry = null;
    if (params) {
      this.page.pageNumber = params.currentPage;
      this.page.size = params.rowsPerPage;
    }

    this.countryService.getCountries(this.page).subscribe( countries => {
      this.items = countries['data'];
      this.totalRecords = countries && countries['count'] && countries['count'] ? countries['count'] : 0;
      this.loading = false;
    },
    error => {
      this.toastr.error(_lang('Countries') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Country List'),
        componentRef: this,
        data: [],
        componentName: 'CountriesComponent'
      },
    });
  }

  addNewCountry() {
    this.router.navigate(['countries/add/0']);
  }

  cloneCountry() {
    if (this.selectedCountry)
      this.router.navigate(['countries/clone/' + this.selectedCountry['CY1_ID']], {queryParams: {clone: true}});
  }

  editRecord() {
    if (this.selectedCountry)
      this.router.navigate(['countries/edit/' + this.selectedCountry['CY1_ID']]);
  }

  editCountry(rowData) {
    this.router.navigate(['countries/edit/' + rowData['CY1_ID']]);
  }

  onDeleteCountry(event) {
    if(this.selectedCountry && this.selectedCountry['CY1_ID']){
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.countryService.delete(this.selectedCountry['CY1_ID']).subscribe( result => {
              this.getCountries(null);
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On delete country error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.countryService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }
}
