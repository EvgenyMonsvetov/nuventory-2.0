import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { formatDate } from '@angular/common';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Subject, Subscription } from 'rxjs';
import { StatementService } from '../../services/statement.service';
import { HeaderService } from '../../services/header.service';
import { takeUntil } from 'rxjs/internal/operators';
import { FormControl } from '@angular/forms';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { MatMenuTrigger } from '@angular/material';
import { GenerateStatementModalComponent } from '../../components/generate-statement-modal/generate-statement-modal.component';

@Component({
  selector: 'app-statements',
  templateUrl: './statements.component.html',
  styleUrls: ['./statements.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class StatementsComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  formatDate = formatDate;

  @ViewChild('statementsView') statementsView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;
  @ViewChild('reportFilter')  reportFilter:  ElementRef;

  @ViewChild(MatMenuTrigger) contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  private scrollHeight: string = '50px';

  /** control for the selected filters */
  public locationFromCtrl: FormControl = new FormControl();
  public locationFromFilterCtrl: FormControl = new FormControl();
  public locationByCtrl: FormControl = new FormControl();
  public locationByFilterCtrl: FormControl = new FormControl();
  public modifiedByCtrl: FormControl = new FormControl();
  public modifiedByFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyLocationFrom = new Subject<void>();
  private _onDestroyLocationBy = new Subject<void>();
  private _onDestroyModifiedBy = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredShopsFrom: any[] = [];
  public filteredShopsBy: any[] = [];
  public filteredModifiedByUsers: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  statuses: any[];
  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;
  sub: Subscription;
  private subscription: Subscription;
  private subscriptionSidebarNav: Subscription;

  private users: Array<object> = [];
  private shops:   Array<object> = [];

  private confirmationHeader: string = _lang('#GenerateInvoice#');
  private acceptLabel: string = _lang('Yes');
  private rejectLabel: string = _lang('No');

  constructor(private statementService: StatementService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = {};

    this.page.filterParams['from'] = this.appSidebarNavService.fromDate;
    this.page.filterParams['to'] = this.appSidebarNavService.toDate;

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.filterParams['from'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['to'] = this.appSidebarNavService.toDate;
      }
    });

    this.subscription = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.getItems();
      }
    });

    this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.getItems();
      }
    });
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.statuses = [
      { label: _lang('#AllRecords#'), value: null},
      { label: _lang('#Open#'),       value: 'open'},
      { label: _lang('#Paid#'),       value: 'paid'}
    ];

    this.cols = [
      { field: 'IS1_COMMENT',     header: _lang('IS1_COMMENT'),             visible: true, export: {visible: true, checked: true}, width: 110 },
      { field: 'IS1_NUMBER',      header: _lang('IS1_NUMBER'),              visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'IS1_STATUS_TEXT', header: _lang('IS1_STATUS_TEXT'),         visible: true, export: {visible: true, checked: true}, width: 90 },
      { field: 'IS1_TOTAL_VALUE', header: _lang('IS1_TOTAL_VALUE'),         visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'FROM_NAME',       header: _lang('ORDER_FROM_LOCATION_NAME'),visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'TO_NAME',         header: _lang('ORDER_BY_LOCATION_NAME'),  visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MODIFIED_ON',     header: _lang('#ModifiedOn#'),            visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'CREATED_ON',      header: _lang('#CreatedOn#'),             visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),            visible: true, export: {visible: true, checked: true}, width: 160 }
    ];

    this.statementService.preloadData().subscribe( data => {
      this.prepareData(data);
      this.getItems();
    });

    this.locationFromFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocationFrom))
        .subscribe(() => {
          this.filterShopsFrom();
        });
    this.locationByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocationBy))
        .subscribe(() => {
          this.filterShopsBy();
        });
    this.modifiedByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyModifiedBy))
        .subscribe(() => {
          this.filterModifiedByUsers();
        });
  }

  private filterShopsFrom() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationFromFilterCtrl.value;
    if (!search) {
      this.filteredShopsFrom = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location from
    this.filteredShopsFrom = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterShopsBy() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationByFilterCtrl.value;
    if (!search) {
      this.filteredShopsBy = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location by
    this.filteredShopsBy = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterModifiedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.modifiedByFilterCtrl.value;
    if (!search) {
      this.filteredModifiedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Modified By Users
    this.filteredModifiedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.users = [{US1_ID: null, US1_NAME: _lang('#AllRecords#')}];
    let usersArr = data.users.map( user => {
      return {
        US1_ID:   user.US1_ID,
        US1_NAME: user.US1_NAME
      }
    }).filter( customer => {
      return customer.US1_NAME && customer.US1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.US1_NAME < b.US1_NAME)
        return -1;
      if (a.US1_NAME > b.US1_NAME)
        return 1;
      return 0;
    });
    this.users = this.users.concat(usersArr);
    this.filterModifiedByUsers();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);
    this.filterShopsFrom();
    this.filterShopsBy();
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    this.subscription.unsubscribe();
    this._onDestroyLocationFrom.next();
    this._onDestroyLocationFrom.complete();
    this._onDestroyLocationBy.next();
    this._onDestroyLocationBy.complete();
    this._onDestroyModifiedBy.next();
    this._onDestroyModifiedBy.complete();
  }

  getScrollHeight(): number {
    return (this.statementsView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.statementService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          if (this.statementsView.nativeElement.parentElement.parentElement) {
            setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});
          }

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Statements') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Statements List'),
        componentRef: this,
        data: [],
        componentName: 'StatementsComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970') {
      this.page.filterParams[col] = '';
    } else {
      this.page.filterParams[col] = value;
    }
    this.page.pageNumber = 0;
    this.getItems();
  }

  onOpen() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['statements/' + this.selected[0]['IS1_ID']]);
  }

  editRecord(rowData) {
    this.router.navigate(['statements/' + rowData['IS1_ID']]);
  }

  onPdfPrint() {
    if (this.selected && this.selected.length) {
      const IS1_IDs: any[] = [];
      this.selected.forEach(function (item) {
        IS1_IDs.push(item['IS1_ID']);
      });
      this.page.filterParams['IS1_IDs'] = IS1_IDs;

      this.statementService.printPDF(this.page).subscribe( result => {
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
      this.page.filterParams['IS1_IDs'] = null;
    }
  }

  onSendStatement() {
    if (this.selected && this.selected.length) {
      const IS1_IDs: any[] = [];
      this.selected.forEach(function (item) {
        IS1_IDs.push(item['IS1_ID']);
      });
      this.statementService.send(IS1_IDs).subscribe( result => {
        if(!result.success){
          this.toastr.error(_lang('Cannot send statements. Set up emails for: ') + '\n' + result.missing);
        }else{
          this.toastr.success(_lang('Operation was successfully executed!'));
        }
      });
    }
  }

  onDelete() {
    if (this.selected.length && this.selected[0]['IS1_ID']) {
      this.acceptLabel = _lang('Yes');
      this.rejectLabel = _lang('No');
      this.confirmationHeader = _lang('DELETE CONFIRMATION');
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        rejectVisible: true,
        accept: () => {
          this.loading = true;
          var IS1_IDs = this.selected.map( item => item['IS1_ID'] );
          this.statementService.deleteMultiple(IS1_IDs).subscribe( result => {
                if (result['data']['success']) {
                  this.selected = null;
                  this.getItems();
                } else {
                  this.loading = false;
                  this.toastr.error(_lang('On delete statement error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                  });
                }
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete statement error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;
        this.statementService.downloadXlsx(this.page).subscribe( result => {
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
      }
    })
  }

  generateStatement() {
    let dialogRef = this.dialog.open(GenerateStatementModalComponent, {
      width:  '500px',
      height: '250px',
      role: 'dialog',
      data: {title: _lang('Generate Statement'), componentRef: this, data: []},
    });

    dialogRef.beforeClose().subscribe( generate => {
      if (generate) {
        this.statementService.generateStatement(this.page).subscribe(res => {
          if(res['data'] && res['data']['success']) {
            this.getItems();
            this.printStatements(res['data']['IS1_IDs']);
          } else if (res['data'] && res['data']['errors']) {
            this.toastr.error(res['data']['errors'], _lang('Service Error'), {
              timeOut: 3000,
              positionClass: 'toast-center-center'
            });
          }
        },
        error => {
          this.toastr.error(_lang('Generate Statement') + ' ' + _lang('error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
      }
    });
  }

  printStatements(IS1_IDs){
    this.acceptLabel = _lang('Print');
    this.rejectLabel = _lang('Cancel');
    this.confirmationHeader = _lang('#GenerateInvoice#');
    this.confirmationService.confirm({
      message: _lang('Statement(s) generated successfully.'),
      rejectVisible: true,
      accept: () => {
        var page = new Page();
        page.filterParams['IS1_IDs'] = IS1_IDs;
        this.statementService.printPDF(page).subscribe( result => {
            },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
            timeOut: 3000,
            positionClass: 'toast-center-center',
          });
        });
      }
    });
  }
}
