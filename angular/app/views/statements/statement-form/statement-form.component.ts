import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { permissions } from '../../../permissions';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { TitleService } from '../../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../../pipes/lang';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { Page } from '../../../model/page';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';
import { StatementService } from '../../../services/statement.service';
import { StatementLineService } from '../../../services/statement-line.service';

@Component({
  selector: 'app-statement-form',
  templateUrl: './statement-form.component.html',
  styleUrls: ['./statement-form.component.scss'],
  providers: [
    StatementService,
    StatementLineService
  ],
  preserveWhitespaces: true
})
export class StatementFormComponent implements OnInit {
  @ViewChild('contentGrid')   contentGrid: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private record: any;

  private statusData: Array<any> = [
    {'label': _lang('#Open#'), 'IS1_STATUS' : 0},
    {'label': _lang('#Paid#'), 'IS1_STATUS' : 1}
  ];

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private loading: boolean = true;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private statementService: StatementService,
              private statementLineService: StatementLineService,
              public  dialog: MatDialog,
              private titleService: TitleService,
              private toastr: ToastrService,
              private location: Location) {
    this.page.filterParams = {};
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'IS2_LINE',    header: _lang('IN2_LINE'),     visible: true, width: 90 },
      { field: 'GL1_NAME',    header: _lang('GL1_NAME'),     visible: true, width: 110 },
      { field: 'MODIFIED_ON', header: _lang('#ModifiedOn#'), visible: true, width: 120 },
      { field: 'MODIFIED_BY', header: _lang('#ModifiedBy#'), visible: true, width: 160 },
      { field: 'CREATED_ON',  header: _lang('#CreatedOn#'), visible: true, width: 120 },
      { field: 'CREATED_BY',  header: _lang('#CreatedBy#'),  visible: true, width: 160 }
    ];

    this.record = this.statementService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.page.filterParams['IS1_ID'] = this.route.snapshot.params.id;
      this.getItems();
      this.statementService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
        if (result[0]) {
          let data = result[0];
          data['IS1_ID'] = parseInt(data['IS1_ID']);
          data['IS1_STATUS'] = parseInt(data['IS1_STATUS']);
          this.record = data;
        }
      });
    }
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.statementLineService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Statement lines') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Statement Form'),
        componentRef: this,
        data: [],
        componentName: 'StatementFormComponent'
      },
    });
  }
}
