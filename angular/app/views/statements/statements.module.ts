import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './statements.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { StatementsComponent } from './statements.component';
import { StatementService } from '../../services/statement.service';
import { StatementFormComponent } from './statement-form/statement-form.component';
import { StatementLineService } from '../../services/statement-line.service';
import { CurrencyModule } from '../../components/currency/currency.module';
import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        CurrencyModule,
        ReportingFilterModule
    ],
    providers: [
        StatementService,
        StatementLineService
    ],
    declarations: [
        StatementsComponent,
        StatementFormComponent
    ]
})
export class StatementsModule { }