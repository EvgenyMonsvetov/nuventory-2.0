import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { StatementsComponent } from './statements.component';
import { StatementFormComponent } from './statement-form/statement-form.component';

export const routes: Routes = [{
    path: '',
    component: StatementsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Statements'),
        permissions: {
            only: [permissions.STATEMENTS_READ]
        }
    }
},{
    path: ':id',
    component: StatementFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.STATEMENTS_READ]
        }
    }
}];