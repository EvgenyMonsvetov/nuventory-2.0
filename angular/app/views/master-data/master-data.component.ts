import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';
import { MatDialog } from '@angular/material';
import {ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router} from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { MasterDataService } from '../../services/master.data.service';
import { Subscription ,  Subject } from 'rxjs';
import { HeaderService } from '../../services/header.service';
import { PdfPrintSettingModalComponent } from '../../components/pdf-print-setting-modal/pdf-print-setting-modal.component';
import { PdfPrintCheckoutSheetSettingsModalComponent } from '../../components/pdf-print-checkout-sheet-settings-modal/pdf-print-checkout-sheet-settings-modal.component';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { ImportMasterDataModalComponent } from '../../components/import-master-data-modal/import-master-data-modal.component';
import * as _ from 'lodash';
import { ShopService } from '../../services/shop.service';
import { LinkCatalogModalComponent } from '../../components/link-catalog-modal/link-catalog-modal.component';
import { AddCatalogListModalComponent } from '../../components/add-catalog-list-modal/add-catalog-list-modal.component';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import {GroupsService} from "../../services/groups";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-master-data',
  templateUrl: './master-data.component.html',
  styleUrls: ['./master-data.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class MasterDataComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('masterDataView')    masterDataView: ElementRef;
  @ViewChild('actionbar')         actionbar:  ElementRef;
  @ViewChild('content')           content: ElementRef;
  @ViewChild('tableHeader')       tableHeader:  ElementRef;
  @ViewChild('tableFilter')         tableFilter:  ElementRef;
  @ViewChild('paginator')         paginator:  ElementRef;

  private scrollHeight: string = '50px';
  private dialogContentHeight: string = '150px';

  private confirmationHeader: string = '';
  private acceptLabel: string = _lang('Yes');
  private deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  /** control for the selected filters */
  public vendorCtrl: FormControl = new FormControl();
  public vendorFilterCtrl: FormControl = new FormControl();
  public unitCtrl: FormControl = new FormControl();
  public unitFilterCtrl: FormControl = new FormControl();
  public masterCategoriesCtrl: FormControl = new FormControl();
  public masterCategoriesFilterCtrl: FormControl = new FormControl();
  public categoriesCtrl: FormControl = new FormControl();
  public categoriesFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyVendor = new Subject<void>();
  private _onDestroyUnit = new Subject<void>();
  private _onDestroyMasterCategory = new Subject<void>();
  private _onDestroyCategory = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredVendors: any[] = [];
  public filteredUnits: any[] = [];
  public filteredMasterCategories: any[] = [];
  public filteredCategories: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  private flagsDP: any[] = [];
  private stockLevelsDP: any[] = [];
  private cols: any[] = [];
  private items: any[] = [];
  private selected: any;

  private vendors: Array<object> = [];
  private units: Array<object> = [];
  private masterCategories: Array<object> = [];
  private categories: Array<object> = [];

  // Properties for Barcode Collector
  private collectedBarcodes: Array<object> = [];
  private showCollectedBarcodes: boolean = false;
  private collectedBarcodesCols: any[] = [];
  private belowMinimumReportPage: boolean = false;
  private selectedBarcode: any;
  private barcodeTableHeight: string = '0px';
  @ViewChild('barcodesTableHeader') barcodesTableHeader:  ElementRef;

  private page = new Page();
  private size: number = 100;
  private totalRecords: number = 0;

  private loading: boolean = true;
  private subscription: Subscription;
  private sidebarNavsubscription: Subscription;

  constructor(private masterDataService: MasterDataService,
              private shopService: ShopService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService,
              private chngRef: ChangeDetectorRef,
              private auth: AuthService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    const filterParams = this.route.snapshot.queryParams;
    this.parseParams(filterParams);
    this.page.filterParams['MD2_ACTIVE'] = '1';

    if ('/below-minimum-report' == this.router.url.split('?')[0]) {
      this.belowMinimumReportPage = true;
    }

    /**
     * Left Panel -> Customer Data -> Click
     */
    var me = this;
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        me.router.routeReuseStrategy.shouldReuseRoute = function (future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot) {
          return future.routeConfig === curr.routeConfig;
        }
      }
    });

    this.subscription = this.headerService.subscribe({
        next: (v) => {
          this.page.pageNumber = 0;
          this.getItems();
        }
    });

    this.route.params.subscribe(params => {
      this.page.pageNumber = 0;
      this.parseParams(params);
      this.getItems();
    });

    this.sidebarNavsubscription = this.appSidebarNavService.subscribe({
      next: (v) => {
        if (this.appSidebarNavService.deatachChangeDetection) {
          me.chngRef.detach();
        } else {
          me.chngRef.reattach();
        }
      }
    });
  }

  parseParams(filterParams) {
    if (!_.isUndefined(filterParams)) {
      for (const key of Object.keys(filterParams)) {
        const value = filterParams[key];
        if (value && value !== '') {
          this.page.filterParams[key] = value;
        }
      }
    }
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
        { field: 'VD1_NAME',                header: _lang('VD1_NAME'),              visible: true, export: {visible: true, checked: true}, width: 120, sortable: true },
        { field: 'MD2_ACTIVE',              header: _lang('MD2_ACTIVE'),            visible: true, export: {visible: true, checked: true}, width: 50, sortable: true },
        { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),       visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'MD1_VENDOR_PART_NUMBER',  header: _lang('MD1_VENDOR_PART_NUMBER'),visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'MD1_OEM_NUMBER',          header: _lang('MD1_OEM_NUMBER'),        visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),             visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'MD1_UNIT_PRICE',          header: _lang('MD1_UNIT_PRICE'),        visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'MD2_STOCK_LEVEL',         header: _lang('MD2_STOCK_LEVEL'),       visible: true, export: {visible: true, checked: true}, width: 55, sortable: true },
        { field: 'MD2_ON_HAND_QTY',         header: _lang('MD2_ON_HAND_QTY'),       visible: true, export: {visible: true, checked: true}, width: 50, sortable: true },
        { field: 'MD2_AVAILABLE_QTY',       header: _lang('MD2_AVAILABLE_QTY'),     visible: true, export: {visible: true, checked: true}, width: 50, sortable: true },
        { field: 'MD2_MIN_QTY',             header: _lang('MD2_MIN_QTY'),           visible: true, export: {visible: true, checked: true}, width: 50, sortable: true },
        { field: 'MD2_RACK',                header: _lang('MD2_RACK'),              visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'MD1_UM1_PRICE_NAME',      header: _lang('UM1_PRICE_NAME'),        visible: true, export: {visible: true, checked: true}, width: 70, sortable: true },
        { field: 'C00_NAME',                header: _lang('Nuventory Category'),    visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'CA1_NAME',                header: _lang('Company Category'),      visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'GL1_NAME',                header: _lang('GL1_NAME'),              visible: true, export: {visible: true, checked: true}, width: 70, sortable: true },
        { field: 'MD1_UPC1',                header: _lang('MD1_UPC1'),              visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
        { field: 'MD1_BODY',                header: _lang('MD1_BODY'),              visible: true, export: {visible: true, checked: true}, width: 50, sortable: true },
        { field: 'MD1_DETAIL',              header: _lang('MD1_DETAIL'),            visible: true, export: {visible: true, checked: true}, width: 51, sortable: true },
        { field: 'MD1_PAINT',               header: _lang('MD1_PAINT'),             visible: true, export: {visible: true, checked: true}, width: 50, sortable: true },
    ];

    this.collectedBarcodesCols = [
      { field: 'BTN_DELETE',      visible: true, width: 32},
      { field: 'MD1_PART_NUMBER', visible: true}
    ];

    this.flagsDP = (this.auth.accessLevel === GroupsService.GROUP_STANDARD_USER )?[
        { label: _lang('Yes'),          value: '1'},
      ]:[
        { label: _lang('#AllRecords#'), value: '-1'},
        { label: _lang('Yes'),          value: '1'},
        { label: _lang('No'),           value: '0'}
      ];

    this.stockLevelsDP = [
      { label: _lang('#AllRecords#'), value: ''},
      { label: '0', value: '0'},
      { label: '1', value: '1'},
      { label: '2', value: '2'},
      { label: '3', value: '3'}
    ];

    this.masterDataService.preloadData().subscribe( data => {
      this.prepareData(data);
    });

    this.vendorFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyVendor))
        .subscribe(() => {
          this.filterVendors();
        });
    this.unitFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyUnit))
        .subscribe(() => {
          this.filterUnits();
        });
    this.masterCategoriesFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyMasterCategory))
        .subscribe(() => {
          this.filterMasterCategories();
        });
    this.categoriesFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCategory))
        .subscribe(() => {
          this.filterCategories();
        });
  }

  private filterVendors() {
    if (!this.vendors) {
      return;
    }
    // get the search keyword
    let search = this.vendorFilterCtrl.value;
    if (!search) {
      this.filteredVendors = this.vendors;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the vendors
    this.filteredVendors = this.vendors.filter(vendor => vendor['VD1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterUnits() {
    if (!this.units) {
      return;
    }
    // get the search keyword
    let search = this.unitFilterCtrl.value;
    if (!search) {
      this.filteredUnits = this.units;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the units
    this.filteredUnits = this.units.filter(unit => unit['UM1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterMasterCategories() {
    if (!this.masterCategories) {
      return;
    }
    // get the search keyword
    let search = this.masterCategoriesFilterCtrl.value;
    if (!search) {
      this.filteredMasterCategories = this.masterCategories;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Master Categories
    this.filteredMasterCategories = this.masterCategories.filter(unit => unit['C00_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterCategories() {
    if (!this.categories) {
      return;
    }
    // get the search keyword
    let search = this.categoriesFilterCtrl.value;
    if (!search) {
      this.filteredCategories = this.categories;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Master Categories
    this.filteredCategories = this.categories.filter(unit => unit['CA1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.vendors = [{VD1_ID: null, VD1_NAME: _lang('#AllRecords#')}];
    let vendorsArr = data.vendors.map( vendor => {
      return {
        VD1_ID:   vendor.VD1_ID,
        VD1_NAME: vendor.VD1_NAME
      }
    }).filter( v => {
      return v.VD1_NAME && v.VD1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.VD1_NAME < b.VD1_NAME)
        return -1;
      if (a.VD1_NAME > b.VD1_NAME)
        return 1;
      return 0;
    });
    this.vendors = this.vendors.concat(vendorsArr);
    this.filterVendors();

    this.units = [{UM1_ID: null, UM1_NAME: _lang('#AllRecords#')}];
    let unitsArr = data.units.map( vendor => {
      return {
        UM1_ID:   vendor.UM1_ID,
        UM1_NAME: vendor.UM1_NAME
      }
    }).filter( u => {
      return u.UM1_NAME && u.UM1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.UM1_NAME < b.UM1_NAME)
        return -1;
      if (a.UM1_NAME > b.UM1_NAME)
        return 1;
      return 0;
    });
    this.units = this.units.concat(unitsArr);
    this.filterUnits();

    this.masterCategories = [{C00_ID: null, C00_NAME: _lang('#AllRecords#')}];
    let masterCategoriesArr = data.masterCategories.map( masterCategory => {
      return {
        C00_ID:   masterCategory.C00_ID,
        C00_NAME: masterCategory.C00_NAME
      }
    }).filter( c => {
      return c.C00_NAME && c.C00_NAME.length > 0;
    }).sort((a, b) => {
      if (a.C00_NAME < b.C00_NAME)
        return -1;
      if (a.C00_NAME > b.C00_NAME)
        return 1;
      return 0;
    });
    this.masterCategories = this.masterCategories.concat(masterCategoriesArr);
    this.filterMasterCategories();

    this.categories = [{CA1_ID: null, CA1_NAME: _lang('#AllRecords#')}];
    let categoriesArr = data.categories.map( category => {
      return {
        CA1_ID:   category.CA1_ID,
        CA1_NAME: category.CA1_NAME
      }
    }).filter( c => {
      return c.CA1_NAME && c.CA1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.CA1_NAME < b.CA1_NAME)
        return -1;
      if (a.CA1_NAME > b.CA1_NAME)
        return 1;
      return 0;
    });
    this.categories = this.categories.concat(categoriesArr);
    this.filterCategories();
  }

  ngOnDestroy() {
    if (this.subscription){
      this.subscription.unsubscribe();
    }
    if (this.sidebarNavsubscription){
      this.sidebarNavsubscription.unsubscribe();
    }
    this._onDestroyVendor.next();
    this._onDestroyVendor.complete();
    this._onDestroyUnit.next();
    this._onDestroyUnit.complete();
    this._onDestroyMasterCategory.next();
    this._onDestroyMasterCategory.complete();
    this._onDestroyCategory.next();
    this._onDestroyCategory.complete();
  }

  getContentDialogHeight(): number{
    if(this.masterDataView.nativeElement.parentElement.parentElement) {
      return (this.masterDataView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
          (this.actionbar.nativeElement.offsetHeight + 48);
    }
    return 0;
  }

  getScrollHeight(): number{
    if(this.tableHeader){
      return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
          this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight + 8);
    }
    return 0;
  }

  getItems() {
    this.loading = true;
    this.selected = null;
    this.route.snapshot.data['selected'] = [];
    this.page.filterParams['MD2_ACTIVE'] = (this.page.filterParams['MD2_ACTIVE'] === undefined)
        ? 1
        : (this.page.filterParams['MD2_ACTIVE'] === '-1' ? '' : this.page.filterParams['MD2_ACTIVE']);
    this.page.filterParams['belowMinimumReport'] = this.belowMinimumReportPage ? 1 : 0;
    this.masterDataService.getAll(this.page).subscribe(res => {
      this.items = res.data.map( item => {
        item['MD1_UNIT_PRICE'] = parseFloat(item['MD1_UNIT_PRICE']).toFixed(2);
        return item;
      });
      this.totalRecords = res.count;

      setTimeout(() => {
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
      });

      this.loading = false;
    },
    error => {
      this.loading = false;
      this.toastr.error(_lang('Customer Data') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data List'),
        componentRef: this,
        data: [],
        componentName: 'MasterDataComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  masterSearch () {
    this.filterChanged('MASTER_SEARCH', this.page.filterParams['MASTER_SEARCH']);
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'ORDERED_ON' || col === 'COMPLETED_ON' || col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970' || value === '') {
      delete this.page.filterParams[col];
    } else if (col === 'MD2_ACTIVE') {
      this.page.filterParams[col] = value === null ? null : value.toString();
    } else {
      this.page.filterParams[col] = value;
    }

    this.router.navigate([], { relativeTo: this.route, queryParams: this.page.filterParams });

    this.page.pageNumber = 0;
    this.getItems();
  }

  onAddNew() {
    this.router.navigate(['master-data/add/0']);
  }

  onClone() {
    if (this.selected && this.selected[0])
      this.router.navigate(['master-data/clone/' + this.selected[0]['MD1_ID']], {queryParams: {clone: true}});
  }

  editRecord() {
    if (this.selected && this.selected[0])
      this.router.navigate(['master-data/edit/' + this.selected[0]['MD1_ID']]);
  }

  onEdit(rowData) {
    this.router.navigate(['master-data/edit/' + rowData['MD1_ID']]);
  }

  selectionChange() {
    this.route.snapshot.data['component'] = 'MasterDataComponent';
    this.route.snapshot.data['selected'] = this.selected;
  }

  onDelete(event) {
    if (this.selected && this.selected[0]['MD1_ID']) {
      this.confirmationHeader = _lang('DELETE CONFIRMATION');
      this.acceptLabel = _lang('Yes');
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        rejectVisible: true,
        accept: () => {
          this.loading = true;
          var MD1_IDs = this.selected.map( item => item['MD1_ID'] );
          this.masterDataService.deleteMultiple(MD1_IDs).subscribe( result => {
            this.getItems();
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On delete Customer Data error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.masterDataService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  printPdf() {
    this.printPdfDialog(this.selected);
  }

  printPdfDialog(items) {
    let dialogRef = this.dialog.open(PdfPrintSettingModalComponent, {
      width: '450px',
      height: '220px',
      role: 'dialog',
      data: {
        title: _lang('Print Label Settings')
      },
    });

    dialogRef.beforeClose().subscribe( params => {
      if (params) {
        this.printLabels(params, items);
      }
    })
  }

  printLabels(params, items) {
    if (items && items.length) {
      const MD1_IDs: any[] = [];
      items.forEach(function (item) {
        MD1_IDs.push(item['MD1_ID']);
      });
      let page = new Page();
      page.filterParams['MD1_IDs'] = MD1_IDs;
      page.filterParams['cols'] = params['cols'];
      page.filterParams['rows'] = params['rows'];
      page.filterParams['barcodeType'] = params['barcodeType'];
      this.masterDataService.printPDF(page).subscribe( result => {
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    }
  }

  printQtySheet() {
    this.masterDataService.printQtySheet(this.page).subscribe( result => {
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  printRack() {
    if (!this.headerService.LO1_ID || this.headerService.LO1_ID === 0) {
      this.showRackError(_lang('Please Select Shop'));
    } else {
      this.shopService.getById(this.headerService.LO1_ID).subscribe( result => {
        if (result && result['LO1_RACKS'] && parseInt(result['LO1_RACKS']) > 0) {
          if(this.belowMinimumReportPage) {
            this.router.navigate(['master-data/print-rack/'], {queryParams: {belowMinimumReport: this.belowMinimumReportPage}});
          }else {
            this.router.navigate(['master-data/print-rack/']);
          }
        } else {
          this.showRackError(_lang('Shop has no rack configured'));
        }
      });
    }
  }

  onPrintBelowMinimunPDF() {
    this.masterDataService.printBelowMinimunPDF(this.page).subscribe( result => {
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
  }

  showRackError(message) {
    this.confirmationHeader = _lang('Print Rack Error');
    this.acceptLabel = _lang('OK');
    this.confirmationService.confirm({
      message: message,
      rejectVisible: false
    });
  }

  printCheckoutSheet() {
    let dialogRef = this.dialog.open(PdfPrintCheckoutSheetSettingsModalComponent, {
      width: '420px',
      height: '200px',
      role: 'dialog',
      data: {
        title: _lang('Print Checkout Sheet')
      },
    });

    dialogRef.beforeClose().subscribe( params => {
      if (params && params['selected']) {
        this.printCheckout(params['selected']);
      }
    })
  }

  printCheckout(params) {
    switch (params) {
      case 'Body':
        this.page.filterParams['MD1_BODY'] = 1;
        break;
      case 'Detail':
        this.page.filterParams['MD1_DETAIL'] = 1;
        break;
      case 'Paint':
        this.page.filterParams['MD1_PAINT'] = 1;
        break;
      case 'Favorites':
        this.page.filterParams['MD1_FAVORITE'] = 1;
        break;
      case 'All':
        break;
    }
    this.page.filterParams['label'] = params;

    this.page.filterParams['CHECKOUT_SHEET'] = 1;

    this.page.filterParams['belowMinimumReport'] = this.belowMinimumReportPage ? 1 : 0;
    this.masterDataService.printCheckoutSheet(this.page).subscribe( result => {
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });

    this.page.filterParams['MD1_BODY'] = 0;
    this.page.filterParams['MD1_DETAIL'] = 0;
    this.page.filterParams['MD1_PAINT'] = 0;
    this.page.filterParams['MD1_FAVORITE'] = 0;
    this.page.filterParams['CHECKOUT_SHEET'] = 0;
    this.page.filterParams['label'] = '';
  }

  onImport() {
    let dialogRef = this.dialog.open(ImportMasterDataModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {title: _lang('Import Customer Data from Excel files'), componentRef: this, data: []},
    });

    dialogRef.beforeClose().subscribe( result => {
      if (result) {
        this.loading = true;
        this.getItems();
      }
    })
  }


  /** Begin Barcode Collector functionality */
  onCollectBarcode() {
    if (this.selected && this.selected.length) {
      let me = this;
      this.selected.forEach(function (item) {
        let exist = me.collectedBarcodes.filter( barcode => {
          return item['MD1_ID'] === barcode['MD1_ID'];
        });

        if (!exist || !exist.length) {
          me.collectedBarcodes.push(item);
        }
      });

      if (!this.showCollectedBarcodes) {
        this.showCollectedBarcodes = true;
        setTimeout(() => {
          this.barcodeTableHeight = this.getBarcodeScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
        });
      }
    }
  }

  getBarcodeScrollHeight(): number{
    return this.getContentDialogHeight() - (this.barcodesTableHeader.nativeElement.offsetHeight);
  }

  closeCollectedBarcodesPanel() {
    this.showCollectedBarcodes = false;
  }

  removeBarcodeClick(record) {
    this.collectedBarcodes = this.collectedBarcodes.filter( item => {
      return item['MD1_ID'] !== record.MD1_ID;
    });
    if (!this.collectedBarcodes.length) {
      this.closeCollectedBarcodesPanel();
    }
  }

  printCollectedBarcodes() {
    this.printPdfDialog(this.collectedBarcodes);
  }

  clearAllCollectedBarcodes() {
    this.collectedBarcodes = [];
    this.closeCollectedBarcodesPanel();
  }
  /** End Barcode Collector functionality */

  linkCatalog() {
    if (this.selected && this.selected.length) {
      let dialogRef = this.dialog.open(LinkCatalogModalComponent, {
        width: window.innerWidth-100+'px',
        height: window.innerHeight-80+'px',
        role: 'dialog',
        data: {title: _lang('Link Catalog'), masterRecord: true, records: this.selected}
      });
    } else {
      this.confirmationHeader = _lang('Link Catalog');
      this.acceptLabel = _lang('OK');
      this.confirmationService.confirm({
        message: _lang('Please select parts first to perform this action.'),
        rejectVisible: false
      });
    }
  }

  onAddCatalog() {
    this.dialog.open(AddCatalogListModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        componentRef: this,
        url: '/master-catalog/search-new-parts',
        filters: {
          LO1_TO_ID:     this.headerService.LO1_ID,
          LO1_FROM_ID:   null,
          usePagination: true
        },
        cols: [
          { field: 'BTN_DELETE',      visible: true, width: 32},
          { field: 'FI1_FILE_PATH',   visible: true, width: 32},
          { field: 'MD0_PART_NUMBER', visible: true},
          { field: 'MD0_UNIT_PRICE',  visible: true, width: 60}
        ],
        fields: {  id: 'MD0_ID',
          description: 'MD0_DESC1',
          partNumber: 'MD0_PART_NUMBER',
          unitPrice: 'MD0_UNIT_PRICE',
          orderQty: 'MD0_ORDER_QTY',
          masterVendor: 'VD0_NAME'
        },
        isVendorCatalog: true
      }
    });
  }

  onSort(col) {
    if (col.sortable) {
      if (this.page.filterParams.hasOwnProperty('SORT_COLUMN')) {
        if (this.page.filterParams['SORT_COLUMN'] === col.field) {
          if (this.page.filterParams['SORT_DIRECTION'] === 'ASC') {
            this.page.filterParams['SORT_DIRECTION'] = 'DESC';
          } else {
            this.page.filterParams['SORT_DIRECTION'] = 'ASC';
          }
        } else {
          this.page.filterParams['SORT_COLUMN'] = col.field;
          this.page.filterParams['SORT_DIRECTION'] = 'ASC';
        }
      } else {
        this.page.filterParams['SORT_COLUMN'] = col.field;
        this.page.filterParams['SORT_DIRECTION'] = 'ASC';
      }
      this.loading = true;
      this.page.pageNumber = 0;
      this.getItems();
    }
  }
}
