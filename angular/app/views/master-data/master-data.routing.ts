import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { MasterDataComponent } from './master-data.component';
import { MasterDataFormComponent } from './master-data-form/master-data-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: MasterDataComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Customer Data'),
        permissions: {
            only: [permissions.MASTER_DATA_READ]
        },
        selected: null
    }
},{
    path: 'add/:id',
    component: MasterDataFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_DATA_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: MasterDataFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_DATA_READ]
        }
    }
},{
    path: 'clone/:id',
    component: MasterDataFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_DATA_ADD]
        }
    }
}];