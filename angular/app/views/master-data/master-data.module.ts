import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './master-data.routing';
import { MasterDataService } from '../../services/master.data.service';
import { MasterDataComponent } from './master-data.component';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MatRadioModule } from '@angular/material/radio';
import { MasterDataFormComponent } from './master-data-form/master-data-form.component';
import { PartNumberLiveSearchModule } from '../../components/part-number-live-search/part-number-live-search.module';
import { MasterCatalogService } from '../../services/master-catalog.service';
import { FileUploadService } from '../../services/file-upload.service';
import { UmDropdownModule } from '../../components/um-dropdown/um-dropdown.module';
import { VendorDropdownModule } from '../../components/vendor-dropdown/vendor-dropdown.module';
import { RackDropdownModule } from '../../components/rack-dropdown/rack-dropdown.module';
import { DrawerDropdownModule } from '../../components/drawer-dropdown/drawer-dropdown.module';
import { BinDropdownModule } from '../../components/bin-dropdown/bin-dropdown.module';
import {CurrencyModule} from "../../components/currency/currency.module";

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      MatRadioModule,
      PartNumberLiveSearchModule,
      UmDropdownModule,
      VendorDropdownModule,
      RackDropdownModule,
      DrawerDropdownModule,
      BinDropdownModule,
      CurrencyModule
  ],
  providers: [
      MasterDataService,
      MasterCatalogService,
      FileUploadService
  ],
  declarations: [
      MasterDataComponent,
      MasterDataFormComponent
  ]
})
export class MasterDataModule { }
