import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {Page} from '../../model/page';
import {ConfirmationService} from 'primeng/api';
import {BudgetsService} from '../../services/budgets.service';
import { _lang } from '../../pipes/lang';
import {HeaderService} from '../../services/header.service';
import {ToastrService} from 'ngx-toastr';
import {permissions} from '../../permissions';

@Component({
  selector: 'app-budgets',
  templateUrl: './budgets.component.html',
  styleUrls: ['./budgets.component.scss'],
  providers: [
    ConfirmationService,
    BudgetsService
  ],
  preserveWhitespaces: true
})
export class BudgetsComponent implements OnInit {


  @ViewChild('BudgetsView')    budgetsView: ElementRef;
  @ViewChild('actionbar')         actionbar: ElementRef;
  @ViewChild('content')           content: ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('tableHeader')       tableHeader: ElementRef;
  @ViewChild('paginator')         paginator: ElementRef;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;
  private size = 50;
  private totalRecords = 0;
  private permissions = permissions;

  private deleteConfirmationHeader: string = _lang('DELETE CONFIRMATION');
  private deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  constructor(
      private router: Router,
      private budgetsService: BudgetsService,
      private headerService: HeaderService,
      private toastr: ToastrService,
      private confirmationService: ConfirmationService
  ) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    this.headerService.subscribe({
      next: (v) => {
        this.getItems();
      }
    });
  }

  ngOnInit() {
    this.cols = [
      { field: 'BU1_DATE',                header: _lang('Date'),                visible: true, export: {visible: true, checked: true}},
      { field: 'BU1_MONTHLY_GROSS_SALE',  header: _lang('Monthly Gross Sale'),  visible: true, export: {visible: true, checked: true}},
      { field: 'BU1_TARGET_PERCENTAGE',   header: _lang('Target Percentage'),   visible: true, export: {visible: true, checked: true}},
      { field: 'BU1_FINAL_RESULT',        header: _lang('Final Result'),        visible: true, export: {visible: true, checked: true}},
      { field: 'BU1_CREATED_ON',          header: _lang('Created On'),          visible: true, export: {visible: true, checked: true}},
      { field: 'BU1_MODIFIED_ON',         header: _lang('Modified On'),         visible: true, export: {visible: true, checked: true}},
    ];
    this.getItems();
  }

  addClick() {
    this.router.navigate(['budgets/add']);
  }

  editRecord() {
    if (this.selected && this.selected.length === 1) {
      this.router.navigate(['budgets/edit/' + this.selected[0]['BU1_ID']]);
    }
  }

  deleteRecords() {
    if (this.selected && this.selected.length > 0) {
      this.confirmationService.confirm({
        header: _lang('Budgets') + ' - ' + this.deleteConfirmationHeader,
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          const BU1_IDs = this.selected.map(budget => budget['BU1_ID']);
          this.budgetsService.deleteMultiple(BU1_IDs).subscribe(result => {
            if (!result.success) {
              this.toastr.error(_lang('Delete failed.'));
            }
            this.getItems();
          }, () => {
            this.toastr.error(_lang('Request failed.'));
            this.getItems();
          });
        }
      });
    }
  }

  onPage(event) {
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  getItems() {
    this.loading = true;
    this.budgetsService.getAll(this.page).subscribe(res => {
      this.items = res.data;
      this.totalRecords = res.count;
      this.loading = false;
    },
    error => {
      this.toastr.error(_lang('Budgets') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
      this.loading = false;
    });
  }

  getFixedPrice(price) {
    return parseFloat(price).toFixed(2);
  }

}