import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService as AuthGuard} from '../../auth-guard.service';
import {NgxPermissionsGuard} from 'ngx-permissions';
import {permissions} from '../../permissions';
import {BudgetsComponent} from './budgets.component';
import {BudgetsFormComponent} from './budgets-form/budgets-form.component';

const routes: Routes = [{
    path: '',
    component: BudgetsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: 'Budgets',
        permissions: {
            only: [permissions.BUDGETS_READ]
        }
    }
}, {
    path: 'add',
    component: BudgetsFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.BUDGETS_ADD]
        }
    }
}, {
    path: 'edit/:id',
    component: BudgetsFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.BUDGETS_EDIT]
        }
    }
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BudgetsRoutingModule { }
