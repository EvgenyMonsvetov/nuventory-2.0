import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BudgetsRoutingModule } from './budgets.routing';
import {BudgetsComponent} from './budgets.component';
import {SharedModule} from '../../modules/shared.module';
import { BudgetsFormComponent } from './budgets-form/budgets-form.component';
import { NgxCurrencyModule } from 'ngx-currency';
import {NgxPermissionsModule} from 'ngx-permissions';

@NgModule({
  declarations: [BudgetsComponent, BudgetsFormComponent],
    imports: [
        CommonModule,
        BudgetsRoutingModule,
        SharedModule,
        NgxCurrencyModule,
        NgxPermissionsModule
    ]
})
export class BudgetsModule { }
