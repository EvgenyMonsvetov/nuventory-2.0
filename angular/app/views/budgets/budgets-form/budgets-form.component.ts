import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { TitleService } from 'app/services/title.service';
import {MatDialog} from "@angular/material/dialog";
import {FormControl} from "@angular/forms";
import moment from "moment";
import {MatDatepicker} from "@angular/material/datepicker";
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {MY_FORMATS} from "../../materials-budget-report/materials-budget-report.component";
import {_lang} from "../../../pipes/lang";
import {LanguageEditorModalComponent} from "../../../components/language-editor-modal/language-editor-modal.component";
import {BudgetsService} from "../../../services/budgets.service";
import {DatePipe, Location} from '@angular/common';
import {ToastrService} from "ngx-toastr";
import {Page} from "../../../model/page";
import {HeaderService} from "../../../services/header.service";
import {Constants} from "../../../constants";

@Component({
  selector: 'app-budgets-form',
  templateUrl: './budgets-form.component.html',
  styleUrls: ['./budgets-form.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    BudgetsService
  ],
  preserveWhitespaces: true
})
export class BudgetsFormComponent implements OnInit {

  public loading: boolean = false;
  private action: String = '';
  private errors: any = [];
  private record: any = [];
  private date = new FormControl(moment());
  private minDate = moment('2008-01-01');
  private page = new Page();
  private constants = Constants;

  constructor(
      private router: Router,
      private budgetsService: BudgetsService,
      private headerService: HeaderService,
      public dialog: MatDialog,
      private titleService: TitleService,
      private location: Location,
      private toastr: ToastrService,
      private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.action = this.route.snapshot.url[0].toString();
    if(this.action == 'edit') {
      this.getBudget();
    }
  }

  getBudget() {
    if (this.route.snapshot.params.id) {
      this.loading = true;
      this.budgetsService.getById(this.route.snapshot.params.id).subscribe( data => {
        this.date.setValue(moment(data.data.BU1_DATE));
        this.record = data.data;
        this.loading = false;
      });
    }
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Material Budget Setup'),
        componentRef: this,
        data: [],
        componentName: 'BudgetsFormComponent'
      },
    });
  }

  saveClick() {
    this.loading = true;
    let date = this.date.value._d;
    date.setFullYear(date.getFullYear(), date.getMonth(), 1);
    let datePipe = new DatePipe('en-US');
    this.record['BU1_DATE'] = datePipe.transform(date, 'yyyy-MM-dd');
    if(this.action == 'edit') {
      this.budgetsService.update(this.record).subscribe(res => {
        if (res.success) {
          this.cancelClick();
        } else {
          this.errors = res.errors;
        }
        this.loading = false;
      },
      error => {
        this.toastr.error(_lang('On save Material Budget error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
        this.loading = false;
      });
    }else {
      this.budgetsService.create(this.record).subscribe(res => {
        if (res.success) {
          this.cancelClick();
        } else {
          this.errors = res.errors;
        }
      },
      error => {
        this.toastr.error(_lang('On save Material Budget error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
    }
  }

  public calculateFinal() {
    if (this.record['BU1_MONTHLY_GROSS_SALE'] < 0) {
      this.record['BU1_MONTHLY_GROSS_SALE'] = 0;
    }
    if (this.record['BU1_TARGET_PERCENTAGE'] < 0) {
      this.record['BU1_TARGET_PERCENTAGE'] = 0;
    } else if (this.record['BU1_TARGET_PERCENTAGE'] > 100) {
      this.record['BU1_TARGET_PERCENTAGE'] = 100;
    }
    this.record['BU1_FINAL_RESULT'] = this.record['BU1_MONTHLY_GROSS_SALE'] * this.record['BU1_TARGET_PERCENTAGE'] / 100;
  }

  chosenStartYearHandler(normalizedYear: moment.Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenStartMonthHandler(normalizedMonth: moment.Moment, datepicker: MatDatepicker<moment.Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    ctrlValue.year(normalizedMonth.year());
    this.date.setValue(ctrlValue);
    this.minDate = null;
    datepicker.close();
    setTimeout(() => {this.minDate = ctrlValue;}, 100);
  }
}
