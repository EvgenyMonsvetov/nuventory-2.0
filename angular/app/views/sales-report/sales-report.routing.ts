import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions  } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { SalesReportComponent } from './sales-report.component';

export const routes: Routes = [{
    path: '',
    component: SalesReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Sales'),
        permissions: {
            only: [permissions.SALES_READ]
        }
    }
}];