import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './sales-report.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { SalesReportComponent } from './sales-report.component';
import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';
import { CompaniesCheckboxDropdownModule } from '../../components/companies-checkbox-dropdown/companies-checkbox-dropdown.module';
import { ShopsCheckboxDropdownModule } from '../../components/shops-checkbox-dropdown/shops-checkbox-dropdown.module';
import { RawSalesDataStoreMonthService } from '../../services/raw-sales-data-store-month.service';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        ReportingFilterModule,
        CompaniesCheckboxDropdownModule,
        ShopsCheckboxDropdownModule
    ],
    providers: [
        RawSalesDataStoreMonthService
    ],
    declarations: [
        SalesReportComponent
    ]
})
export class SalesReportModule { }