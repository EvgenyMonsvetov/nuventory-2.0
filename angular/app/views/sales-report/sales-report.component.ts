import { OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Component } from '@angular/core';
import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { TitleService } from '../../services/title.service';
import { Page } from '../../model/page';
import { ActivatedRoute } from '@angular/router';
import { HeaderService } from '../../services/header.service';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { Subscription } from 'rxjs';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { RawSalesDataStoreMonthService } from '../../services/raw-sales-data-store-month.service';

@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.scss'],
  providers: [
    RawSalesDataStoreMonthService
  ],
  preserveWhitespaces: true
})
export class SalesReportComponent implements OnInit {
  private permissions = permissions;

  @ViewChild('sales', { read: ElementRef }) container: ElementRef;
  private chart;
  private showNoData: boolean = false;

  private page = new Page();
  private sub: Subscription;
  private subscriptionHeader: Subscription;
  private subscriptionSidebarNav: Subscription;
  private headerSubscription: Subscription;
  private selectedCompanies: Array<any> = [];
  private selectedShops: Array<any> = [];
  private disableRunButton: boolean = false;

  constructor(private rawSalesDataStoreMonthService: RawSalesDataStoreMonthService,
              private titleService: TitleService,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
    this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;

    this.selectedCompanies = [this.headerService.CO1_ID];
    this.selectedShops = [this.headerService.LO1_ID];
    this.page.filterParams['LO1_IDs'] = [this.headerService.LO1_ID];

    this.headerSubscription = this.headerService.subscribe({
      next: (v) => {
        const CO1_ID = parseInt(this.headerService.CO1_ID);
        const LO1_ID = parseInt(this.headerService.LO1_ID);
        this.selectedCompanies = CO1_ID > 0 ? [CO1_ID] : [];
        this.selectedShops = LO1_ID > 0 ? [LO1_ID] : [];
        this.page.filterParams['LO1_IDs'] = LO1_ID > 0 ? [LO1_ID] : [];
      }
    });

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
      }
    });
  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['startDate']);
    const toDate = new Date(this.page.filterParams['endDate']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableRunButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableRunButton = false;
    }
  }

  ngOnInit() {
    this.buildHighcharts([]);
    this.titleService.setNewTitle(this.route);
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscriptionHeader)
      this.subscriptionHeader.unsubscribe();
    if (this.subscriptionSidebarNav)
      this.subscriptionSidebarNav.unsubscribe();
  }

  getChartData() {
    if (this.sub)
      this.sub.unsubscribe();

    this.showNoData = false;
    this.chart.showLoading();

    this.sub = this.rawSalesDataStoreMonthService.getSalesReport(this.page).subscribe(res => {
          this.chart.hideLoading();
          if (res['series'] && res['series'].length) {
            this.showNoData = false;
            this.buildHighcharts(res);
          } else {
            this.showNoData = true;
          }
        },
        error => {
          this.toastr.error(_lang('Sales') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  buildHighcharts(data) {
  ﻿ let colors = this.getColors();

    if (data['series']) {
      data['series'].forEach(function(locationData) {
        const locationColor = colors.shift();
        locationData['color'] = locationColor;
        locationData['lineColor'] = locationColor;
        colors.push(locationColor);
      });
    }

    this.chart = Highcharts.chart(this.container.nativeElement, {
      chart: {
        type: 'area'
      },
      title: {
        text: _lang('Sales')
      },
      legend: {
      },
      xAxis: {
        min: .5,
        max: 5.5,
        categories: data.categories
      },
      yAxis: {
        title: {
          text: data.attributeType
        },
        labels: {
          enabled: false
        },
        gridLineColor: 'transparent'
      },
      tooltip: {
        shared: true,
        valueSuffix: '',
        valuePrefix: data.attributeTypeSymbol
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        areaspline: {
          fillOpacity: .65
        }
      },
      series: data.series
    });
  }

  getColors() {
    return [
      '#E75063',
      '#7DC566',
      '#6697D2',
      '#F2A55D',
      '#9A60AA',
      '#C56B59',
      '#D178B3',
      '#707070',
    ];
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Sales'),
        componentRef: this,
        data: [],
        componentName: 'SalesReportComponent'
      },
    });
  }

  selectedCompaniesChange(value) {
    this.selectedCompanies = value;
  }

  selectedShopsChange(value) {
    this.page.filterParams['LO1_IDs'] = value;
  }

}
