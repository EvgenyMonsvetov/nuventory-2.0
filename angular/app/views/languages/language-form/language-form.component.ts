import { Component, OnInit } from '@angular/core';
import { permissions } from '../../../permissions';
import { ActivatedRoute, Router } from '@angular/router';
import { LangService } from '../../../services/lang.service';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../../pipes/lang';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-language-add',
  templateUrl: './language-form.component.html',
  styleUrls: ['./language-form.component.scss'],
  providers: [
    LangService
  ],
  preserveWhitespaces: true
})
export class LanguageFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';
  private constants = Constants;
  private permissions = permissions;
  private record: any;

  constructor(
      private toastr: ToastrService,
      private router: Router,
      private route: ActivatedRoute,
      private langService: LangService,
      public  dialog: MatDialog,
      private titleService: TitleService
  ) {
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.record = this.langService.initializeLang();
    this.action = this.route.snapshot.url[0].toString();
    this.langService.getLanguageById(this.route.snapshot.params.id || 0).subscribe( result => {
      if (result[0]) {
        let data = result[0];
        data['LA1_ID'] = parseInt(data['LA1_ID']);
        this.record = data;
        if (this.action === 'clone')
          this.record['LA1_ID'] = 0;
      }
    },
    error => {
      this.toastr.error(_lang('Language') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  cancelClick() {
    this.router.navigate(['languages']);
  }

  openLanguageEditor(){
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Language Form'),
        componentRef: this,
        data: [],
        componentName: 'LanguageFormComponent'
      },
    });
  }

  saveClick() {
    if (this.action === 'add' || this.action === 'clone') {
      this.langService.createLanguage(this.record).subscribe(result => {
        if (result.data.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      },
      error => {
        this.toastr.error(_lang('On add language loading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
    } else if (this.action === 'edit') {
      this.langService.updateLanguage(this.record).subscribe(result => {
        if (result.data.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      },
      error => {
        this.toastr.error(_lang('On save language error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
    }
  }

}
