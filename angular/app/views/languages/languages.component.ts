import { Component, OnInit } from '@angular/core';
import { LangService } from '../../services/lang.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { TitleService } from '../../services/title.service';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss'],
  providers: [
    LangService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class LanguagesComponent implements OnInit {

  private permissions = permissions;

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[] = [];
  items: any[] = [];

  private loading = true;
  private page = new Page();
  private selected: any = null;
  private totalRecords: number = 0;
  private size: number = 50;

  constructor(
      private toastr: ToastrService,
      private langService: LangService,
      public  dialog: MatDialog,
      private router: Router,
      private confirmationService: ConfirmationService,
      private titleService: TitleService,
      private route: ActivatedRoute
  ) {
    this.page.size = this.size;
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'LA1_SHORT_CODE',  header: _lang('LA1_SHORT_CODE'),  visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'LA1_NAME',        header: _lang('LA1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 180 },
      { field: 'CREATED_ON',      header: _lang('#CreatedOn#'),     visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'CREATED_BY',      header: _lang('#CreatedBy#'),     visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MODIFIED_ON',     header: _lang('#ModifiedOn#'),    visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),    visible: true, export: {visible: true, checked: true}, width: 160 }
    ];
  }

  loadLanguagesLazy(event: LazyLoadEvent) {
    const offset = (event.first < this.size ? 1 : event.first / this.size) - 1;
    this.setPage({
      offset: offset
    });

    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

    console.log(event);
  }

  setPage(pageInfo) {
    this.loading = true;
    this.page.pageNumber = pageInfo.offset;

    this.langService.getLanguages(this.page).subscribe(result => {
          // this.page.totalElements = result.count;
          // this.page.totalPages    = result.count / this.page.size;
          this.items = result.data['currentVocabulary'];
          // this.totalRecords = result.count;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Languages') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Language List'),
        componentRef: this,
        data: [],
        componentName: 'LanguagesComponent'
      },
    });
  }

  addClick() {
    this.router.navigate(['languages/add/0']);
  }

  cloneRecord() {
    if(this.selected) {
      this.router.navigate(['languages/clone/' + this.selected.LA1_ID]);
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.langService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  editRecord(rowData?) {
    if(this.selected || rowData){
      const LA1_ID = (rowData && rowData.LA1_ID) ? rowData.LA1_ID : this.selected.LA1_ID;
      this.router.navigate(['languages/edit/' + LA1_ID]);
    }
  }

  deleteRecord() {
    if(this.selected) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.langService.deleteLanguage(this.selected.LA1_ID).subscribe(result => {
            this.items = this.items.filter(item => {
              return item.LA1_ID === this.selected.LA1_ID ? false : true;
            });
            this.loading = false;
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On delete language error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
        }
      });
    }
  }
}
