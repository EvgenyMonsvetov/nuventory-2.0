import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { LanguagesComponent } from './languages.component';
import { LanguageFormComponent } from './language-form/language-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: LanguagesComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Languages#'),
        permissions: {
            only: [permissions.LANGUAGES_READ]
        }
    }
},{
    path: 'add/:id',
    component: LanguageFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Language Add'),
        permissions: {
            only: [permissions.LANGUAGES_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: LanguageFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Language Edit'),
        permissions: {
            only: [permissions.LANGUAGES_EDIT]
        }
    }
},{
    path: 'clone/:id',
    component: LanguageFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Language Copy'),
        permissions: {
            only: [permissions.LANGUAGES_ADD]
        }
    }
}];