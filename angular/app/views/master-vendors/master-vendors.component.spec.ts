import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterVendorsComponent } from './master-vendors.component';

describe('MasterVendorsComponent', () => {
  let component: MasterVendorsComponent;
  let fixture: ComponentFixture<MasterVendorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterVendorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterVendorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
