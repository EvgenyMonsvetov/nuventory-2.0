import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { MasterVendorsComponent } from './master-vendors.component';
import { MasterVendorFormComponent } from './master-vendor-form/master-vendor-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: MasterVendorsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Master Vendors'),
        permissions: {
            only: [permissions.MASTER_VENDORS_READ]
        }
    }
},{
    path: 'add/:id',
    component: MasterVendorFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_VENDORS_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: MasterVendorFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_VENDORS_EDIT]
        }
    }
},{
    path: 'clone/:id',
    component: MasterVendorFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_VENDORS_ADD]
        }
    }
}];