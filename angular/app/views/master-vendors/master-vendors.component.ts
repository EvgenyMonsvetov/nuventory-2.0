import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';
import { MasterVendorService } from '../../services/master-vendor.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { UpdatePartsModalComponent } from '../../components/update-parts-modal/update-parts-modal.component';
import { LinkMcatalogModalComponent } from '../../components/link-mcatalog-modal/link-mcatalog-modal.component';

@Component({
  selector: 'app-master-vendors',
  templateUrl: './master-vendors.component.html',
  styleUrls: ['./master-vendors.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class MasterVendorsComponent implements OnInit {
  private permissions = permissions;

  @ViewChild('masterVendorsView')   masterVendorsView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';
  private dialogContentHeight: string = '150px';

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[] = [];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;

  constructor(private masterVendorService: MasterVendorService,
              public  dialog: MatDialog,
              private router: Router,
              private elementRef: ElementRef,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'VD0_NAME',        header: _lang('VD0_NAME'),            visible: true, export: {visible: true, checked: true}, width: 250 },
      { field: 'VD0_SHORT_CODE',  header: _lang('VD0_SHORT_CODE'),      visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'VD0_MANUFACTURER',header: _lang('VD0_MANUFACTURER'),    visible: true, export: {visible: true, checked: true}, width: 140 },
      { field: 'VD0_ADDRESS1',    header: _lang('#AddressStreet1#'),    visible: true, export: {visible: true, checked: true}, width: 250 },
      { field: 'VD0_ADDRESS2',    header: _lang('#AddressStreet2#'),    visible: true, export: {visible: true, checked: true}, width: 250 },
      { field: 'VD0_CITY',        header: _lang('#AddressCity#'),       visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'VD0_STATE',       header: _lang('#AddressState#'),      visible: true, export: {visible: true, checked: true}, width: 72 },
      { field: 'VD0_ZIP',         header: _lang('#AddressPostalCode#'), visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'VD0_PHONE',       header: _lang('#Phone#'),             visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'VD0_FAX',         header: _lang('#Fax#'),               visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'VD0_EMAIL',       header: _lang('#Email#'),             visible: true, export: {visible: true, checked: true}, width: 400 },
      { field: 'VD0_URL',         header: _lang('#URL#'),               visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'CY1_NAME',        header: _lang('#AddressPostalCode#'), visible: true, export: {visible: true, checked: true}, width: 155 },
      { field: 'CREATED_ON',      header: _lang('#CreatedOn#'),         visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'CREATED_BY',      header: _lang('#CreatedBy#'),         visible: true, export: {visible: true, checked: true}, width: 140 },
      { field: 'MODIFIED_ON',     header: _lang('#ModifiedOn#'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),        visible: true, export: {visible: true, checked: true}, width: 140 }
    ];

    this.getItems();
  }

  getContentDialogHeight(): number{
    return (this.masterVendorsView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48);
  }

  getScrollHeight(): number{
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight + 52);
  }

  getItems() {
    this.masterVendorService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Master Vendors') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Master Vendor List'),
        componentRef: this,
        data: [],
        componentName: 'MasterVendorsComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  onAddNew() {
    this.router.navigate(['master-vendors/add/0']);
  }

  onClone() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['master-vendors/clone/' + this.selected[0]['VD0_ID']], {queryParams: {clone: true}});
  }

  editRecord() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['master-vendors/edit/' + this.selected[0]['VD0_ID']]);
  }

  onEdit(rowData) {
    this.router.navigate(['master-vendors/edit/' + rowData['VD0_ID']]);
  }

  onDelete(event) {
    if (this.selected && this.selected[0] && this.selected[0]['VD0_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          var VD0_IDs = this.selected.map( item => item['VD0_ID'] );
          this.masterVendorService.deleteMultiple(VD0_IDs).subscribe( result => {
                this.getItems();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete master vendor error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.masterVendorService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  updateLinkedParts() {
    const masterRecord = true; //should be based on current selected vendor (VD1_MANUFACTURER == '1')
    let dialogRef = this.dialog.open(UpdatePartsModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Update Parts'), items: this.selected, masterRecord: masterRecord, componentRef: this.elementRef},
    });
  }

  linkMCatalog() {
    let dialogRef = this.dialog.open(LinkMcatalogModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Link Manufacturer Catalog'), items: this.selected, componentRef: this.elementRef},
    });
  }
}
