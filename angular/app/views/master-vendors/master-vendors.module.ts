import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './master-vendors.routing';
import { MasterVendorService } from '../../services/master-vendor.service';
import { MasterVendorsComponent } from './master-vendors.component';
import { MasterVendorFormComponent } from './master-vendor-form/master-vendor-form.component';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MatRadioModule } from '@angular/material/radio';
import { MasterCatalogService } from '../../services/master-catalog.service';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      MatRadioModule
  ],
  providers: [
      MasterVendorService,
      MasterCatalogService
  ],
  declarations: [
      MasterVendorsComponent,
      MasterVendorFormComponent
  ]
})
export class MasterVendorsModule { }
