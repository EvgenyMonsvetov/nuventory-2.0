import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterVendorFormComponent } from './master-vendor-form.component';

describe('MasterVendorFormComponent', () => {
  let component: MasterVendorFormComponent;
  let fixture: ComponentFixture<MasterVendorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterVendorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterVendorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
