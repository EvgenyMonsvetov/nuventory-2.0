import { Component, ElementRef, OnInit } from '@angular/core';
import { MasterVendorService } from '../../../services/master-vendor.service';
import { permissions } from '../../../permissions';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { _lang } from '../../../pipes/lang';
import { SelectCountriesComponent } from '../../../components/select-countries/select-countries.component';
import { Location } from '@angular/common';
import { ConfirmationService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../auth/auth.service';
import { GroupsService } from '../../../services/groups';
import { ImagesUploaderModalComponent } from '../../../components/images-uploader-modal/images-uploader-modal.component';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-master-vendor-add',
  templateUrl: './master-vendor-form.component.html',
  styleUrls: ['./master-vendor-form.component.scss'],
  providers: [
    MasterVendorService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class MasterVendorFormComponent implements OnInit {
  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private superAdmin: boolean = false;
  private record: any;
  private constants = Constants;

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  constructor(private route: ActivatedRoute,
              private router: Router,
              private masterVendorService: MasterVendorService,
              private confirmationService: ConfirmationService,
              private auth: AuthService,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private elementRef: ElementRef,
              private titleService: TitleService,
              private location: Location) { }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    if (this.auth.accessLevel === GroupsService.GROUP_ADMIN ) {
      this.superAdmin = true;
    }

    this.record = this.masterVendorService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.masterVendorService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
        if (result[0]) {
          let data = result[0];
          data['VD0_ID'] = parseInt(data['VD0_ID']);
          data['CY1_ID'] = parseInt(data['CY1_ID']);
          data['CO1_ID'] = parseInt(data['CO1_ID']);
          data['VD0_AS2_REQUEST_RECEIPT'] = parseInt(data['VD0_AS2_REQUEST_RECEIPT']);
          data['VD0_AS2_SIGN_MESSAGES'] = parseInt(data['VD0_AS2_SIGN_MESSAGES']);
          data['VD0_DISCOUNT'] = parseInt(data['VD0_DISCOUNT']);
          data['VD0_EDI_GL'] = parseInt(data['VD0_EDI_GL']);
          data['VD0_MANUFACTURER'] = parseInt(data['VD0_MANUFACTURER']);
          data['VD0_MINIMUM_ORDER'] = parseInt(data['VD0_MINIMUM_ORDER']);
          data['VD0_NO_INVENTORY'] = parseInt(data['VD0_NO_INVENTORY']);
          data['VD0_PICKUP_FTP'] = parseInt(data['VD0_PICKUP_FTP']);
          data['VD0_PICKUP_SFTP'] = parseInt(data['VD0_PICKUP_SFTP']);
          data['VD0_POST_AS2'] = parseInt(data['VD0_POST_AS2']);
          data['VD0_POST_HTTP'] = parseInt(data['VD0_POST_HTTP']);
          data['VD0_RECEIVE_AS2'] = parseInt(data['VD0_RECEIVE_AS2']);
          data['VD0_RECEIVE_EDI'] = parseInt(data['VD0_RECEIVE_EDI']);
          data['VD0_RECEIVE_FTP'] = parseInt(data['VD0_RECEIVE_FTP']);
          data['VD0_RECEIVE_HTTP'] = parseInt(data['VD0_RECEIVE_HTTP']);
          data['VD0_SEND_ACKNOWLEDGEMENT'] = parseInt(data['VD0_SEND_ACKNOWLEDGEMENT']);
          data['VD0_SEND_EDI_PO'] = parseInt(data['VD0_SEND_EDI_PO']);
          data['VD0_SEND_FTP'] = parseInt(data['VD0_SEND_FTP']);
          data['VD0_SEND_SFTP'] = parseInt(data['VD0_SEND_SFTP']);
          data['VD0_SUPPLIER'] = parseInt(data['VD0_SUPPLIER']);
          data['VD0_MINIMUM_ORDER_CHECK'] = data['VD0_MINIMUM_ORDER'] > 0 ? 1 : 0;

          this.record = data;
          if (this.action === 'clone')
            this.record['VD0_ID'] = 0;
        }
      });
    }
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Master Vendor Form'),
        componentRef: this,
        data: [],
        componentName: 'MasterVendorFormComponent'
      },
    });
  }

  saveClick() {
    if (this.action === 'add' || this.action === 'clone') {
      this.masterVendorService.create(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.masterVendorService.update(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  selectCountry() {
    let dialogRef = this.dialog.open(SelectCountriesComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Country'), CY1_ID: this.record.CY1_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CY1_ID = selected['CY1_ID'];
        this.record.CY1_SHORT_CODE = selected['CY1_SHORT_CODE'];
        this.record.CY1_NAME = selected['CY1_NAME'];
      }
    })
  }

  clearCountry() {
    this.record.CY1_ID = 0;
    this.record.CY1_SHORT_CODE = '';
    this.record.CY1_NAME = '';
  }

  linkedFieldChangeHandler(event) {
    switch(event.target.id) {
      case 'VD0_TRADING_PARTNER_ID':
      case 'VD0_SUPPLIER_CODE':
        if(!this.record.VD0_SUPPLIER_CODE || this.record.VD0_SUPPLIER_CODE.length === 0) {
          this.record.VD0_SUPPLIER_CODE = this.record.VD0_TRADING_PARTNER_ID;
        }
        break;
    }
  }

  showVendors() {
    this.router.navigate(['/vendors/' + this.record.VD0_ID]);
  }

  onDeleteParts() {
    this.deleteConfirmation = _lang('DELETE CONFIRMATION');
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.masterVendorService.deleteMasterVendorParts(this.record['VD0_ID']).subscribe( result => {
                if (result['data']['success']){
                  this.toastr.success(_lang('Operation was successfully executed!'), _lang('Service Success'), {
                    timeOut: 3000,
                  });
                } else {
                  this.toastr.error(_lang('On delete master vendor parts error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                  });
                }
              },
              error => {
                this.toastr.error(_lang('On delete master vendor parts error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
  }

  onDeleteAllImages() {
    this.deleteConfirmation = _lang('DELETE CONFIRMATION');
    this.confirmationService.confirm({
      message: this.deleteConfirmationMessage,
      accept: () => {
        this.masterVendorService.deleteAllImages(this.record['VD0_ID']).subscribe( result => {
              if (result['data']['success']) {
                this.toastr.success(_lang('Operation was successfully executed!'), _lang('Service Success'), {
                  timeOut: 3000,
                });
              } else {
                this.toastr.error(_lang('On delete master vendor images error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              }
            },
            error => {
              this.toastr.error(_lang('On delete master vendor images error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    });
  }

  onUploadImages() {
    let me = this;
    let dialogRef = this.dialog.open(ImagesUploaderModalComponent, {
      width: window.innerWidth - 100 + 'px',
      height: window.innerHeight - 100 + 'px',
      role: 'dialog',
      data: {title: _lang('File Upload'), componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( result => {
      if (result) {
        this.masterVendorService.uploadArchiveImages(this.record['VD0_ID'], result['fileToUpload']).subscribe( uploadResult => {
          if (uploadResult && uploadResult.data && uploadResult.data.success === true ) {
            me.toastr.success(_lang('Images uploading success'), _lang('Service Error'), {
              timeOut: 3000,
            });
          } else {
            me.toastr.error(_lang('Images uploading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          }
        });
      }
    });
  }

}
