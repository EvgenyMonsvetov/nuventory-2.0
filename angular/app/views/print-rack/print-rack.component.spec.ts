import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintRackComponent } from './print-rack.component';

describe('PrintRackComponent', () => {
  let component: PrintRackComponent;
  let fixture: ComponentFixture<PrintRackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintRackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintRackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
