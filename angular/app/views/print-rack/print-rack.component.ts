import { Component, OnInit } from '@angular/core';
import { MasterDataService } from '../../services/master.data.service';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-print-rack',
  templateUrl: './print-rack.component.html',
  providers: [ MasterDataService ],
  styleUrls: ['./print-rack.component.scss'],
  preserveWhitespaces: false
})
export class PrintRackComponent implements OnInit {
  data: any;
  gridHeight: any = '0px';
  belowMinimumReport: boolean = false;

  constructor(private masterDataService: MasterDataService,
              private sanitizer: DomSanitizer,
              private toastr: ToastrService,
              private location: Location,
              private router: ActivatedRoute) { }

  ngOnInit() {
      this.router.queryParams.subscribe(params => {
          this.belowMinimumReport = params['belowMinimumReport'];
      });
      var page = new Page();
      page.filterParams['belowMinimumReport'] = this.belowMinimumReport ? 1 : 0;
      this.masterDataService.printRack(page).subscribe( res => {
              this.data = this.sanitizer.bypassSecurityTrustHtml(res['html']);
              this.gridHeight = (res['pagesCount'] * (res['pageHeight'] + 1)).toString() + 'px';
          },
          error => {
              this.toastr.error(_lang('On generate Print Rack HTML error occured'), _lang('Service Error'), {
                  timeOut: 3000,
              });
          });
  }

  cancelClick() {
    this.location.back();
  }
}
