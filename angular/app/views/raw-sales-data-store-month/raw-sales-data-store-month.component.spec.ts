import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawSalesDataStoreMonthComponent } from './raw-sales-data-store-month.component';

describe('RawSalesDataStoreMonthComponent', () => {
  let component: RawSalesDataStoreMonthComponent;
  let fixture: ComponentFixture<RawSalesDataStoreMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawSalesDataStoreMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawSalesDataStoreMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
