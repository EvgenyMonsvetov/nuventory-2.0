import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { RawSalesDataStoreMonthComponent } from './raw-sales-data-store-month.component';

export const routes: Routes = [{
    path: '',
    component: RawSalesDataStoreMonthComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Sales Report Data'),
        permissions: {
            only: [permissions.IMPORT_REPORT_DATA_READ]
        }
    }
}];