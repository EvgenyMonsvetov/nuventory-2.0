import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { MatDialog} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Page } from '../../model/page';
import { Subscription } from 'rxjs';
import { HeaderService } from '../../services/header.service';
import { RawSalesDataStoreMonthService } from '../../services/raw-sales-data-store-month.service';
import {ConfirmationService} from "primeng/api";

@Component({
  selector: 'app-raw-sales-data-store-month',
  templateUrl: './raw-sales-data-store-month.component.html',
  styleUrls: ['./raw-sales-data-store-month.component.scss'],
  providers: [
      RawSalesDataStoreMonthService,
      ConfirmationService
  ]
})
export class RawSalesDataStoreMonthComponent implements OnInit, OnDestroy {

  private permissions = permissions;

  @ViewChild('view')              view: ElementRef;
  @ViewChild('actionbar')         actionbar:  ElementRef;
  @ViewChild('tableHeader')       tableHeader:  ElementRef;
  @ViewChild('paginator')         paginator:  ElementRef;

  private scrollHeight: string = '50px';

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  private cols: any[] = [];
  private items: any[] = [];

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private loading: boolean = true;
  private subscription: Subscription;
  private sub: Subscription;
  selected: any;

  constructor(private rawSalesDataStoreMonthService: RawSalesDataStoreMonthService,
              public  dialog: MatDialog,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    this.sub = this.headerService.subscribe({
        next: (v) => {
          this.page.pageNumber = 0;
          this.page.filterParams = {};
          this.loading = true;
          this.getData();
        }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'r03_co1_name',  header: _lang('Company Name'),    visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'r03_shop_name', header: _lang('Shop Name'),       visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'r03_month',     header: _lang('Month'),           visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'r03_year',      header: _lang('Year'),            visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'modified_on',   header: _lang('Updated Records'), visible: true, export: {visible: true, checked: true}, width: 120 }
    ];

    this.getData();
  }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
    this.sub.unsubscribe();
  }

  getScrollHeight(): number{
    return (this.view.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
         this.paginator.nativeElement.offsetHeight);
  }

  getData() {
    this.loading = true;

    if (this.subscription)
      this.subscription.unsubscribe();

    this.subscription = this.rawSalesDataStoreMonthService.getAll(this.page).subscribe(result => {
          this.items = result['data'];
          this.totalRecords = result['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px'});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Technicians') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Sales Data Store Month'),
        componentRef: this,
        data: [],
        componentName: 'RawSalesDataStoreMonthComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getData();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    this.page.filterParams[col] = value;
    this.page.pageNumber = 0;
    this.getData();
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.rawSalesDataStoreMonthService.downloadXlsx(this.page).subscribe( result => {
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
      }
    })
  }
}
