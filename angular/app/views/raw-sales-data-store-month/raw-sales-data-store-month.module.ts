import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './raw-sales-data-store-month.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RawSalesDataStoreMonthComponent } from './raw-sales-data-store-month.component';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule
    ],
    declarations: [
        RawSalesDataStoreMonthComponent
    ]
})
export class RawSalesDataStoreMonthModule { }