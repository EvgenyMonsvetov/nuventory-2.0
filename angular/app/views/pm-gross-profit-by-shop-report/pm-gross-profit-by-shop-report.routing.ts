import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions  } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { PmGrossProfitByShopReportComponent } from './pm-gross-profit-by-shop-report.component';

export const routes: Routes = [{
    path: '',
    component: PmGrossProfitByShopReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('P&M Gross Profit'),
        permissions: {
            only: [permissions.PM_GROSS_PROFIT_BY_SHOP]
        }
    }
}];