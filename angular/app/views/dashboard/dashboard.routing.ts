import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import {NgxPermissionsGuard } from 'ngx-permissions';
import {AuthGuardService as AuthGuard} from './../../auth-guard.service';
import {permissions } from './../../permissions';

export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
        only: [permissions.DASHBOARD_READ]
      }
    }
  }
];