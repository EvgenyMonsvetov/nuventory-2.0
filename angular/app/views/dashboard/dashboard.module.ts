import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { NgxPermissionsModule } from 'ngx-permissions';
import { routes } from './dashboard.routing';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../modules/shared.module';


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    NgxPermissionsModule.forChild(),
    SharedModule
  ],
  declarations: [
      DashboardComponent
  ]
})
export class DashboardModule { }
