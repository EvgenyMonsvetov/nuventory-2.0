import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Page } from '../../model/page';
import { Transaction } from '../../model/transaction';
import { PagedData } from '../../model/paged-data';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { permissions } from '../../permissions';

import { TransactionService } from '../../services/transaction.service';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../pipes/lang';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { Subscription } from 'rxjs/Rx';
import * as  Highcharts from 'highcharts';
import { HeaderService } from '../../services/header.service';
import { JobTicketService } from '../../services/job-ticket.service';
import { TechScanService } from '../../services/tech-scan.service';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [
    PurchaseOrderService,
    JobTicketService,
    TechScanService
  ],
  preserveWhitespaces: true
})
export class DashboardComponent implements OnInit, OnDestroy {
    @ViewChild('dashboardView', { read: ElementRef }) chartView: ElementRef;

    page = new Page();
    public loading = false;

    rows = new Array<Transaction>();
    checkedRows = new Array<Transaction>();
    private ret: PagedData<Transaction>;
    private transactions: number[];

    public permissions = permissions;

    @ViewChild('chart', { read: ElementRef }) container: ElementRef;
    private chart;

    private showNoData: boolean = false;
    private materialBudgetSubscription: Subscription;
    private chartDataSubscription: Subscription;
    private headerServiceSubscription: Subscription;
    private slideShow: boolean = false;
    private graphHeight: string = '400px';

    constructor(private transactionService: TransactionService,
                private purchaseOrderService: PurchaseOrderService,
                private toastr: ToastrService,
                private router: Router,
                private titleService: TitleService,
                private headerService: HeaderService,
                private jobTicketService: JobTicketService,
                private techScanService: TechScanService,
                private route: ActivatedRoute) {
        this.page.filterParams['DASHBOARD_REPORT'] = true;

        this.headerServiceSubscription = this.headerService.subscribe({
            next: (v) => {
                this.getChartData();
            }
        });

        const eventUrl: string = this.router.url;
        this.slideShow = eventUrl.includes('/charts-slide-show/');
    }

    ngOnInit() {
        this.titleService.setNewTitle(this.route);

        this.loading = true;
        setTimeout(() => {
            if (this.slideShow) {
                this.graphHeight = this.getGraphHeight() + 'px';
            }
            setTimeout(() => {
                this.buildHighcharts([], [], []);
                this.getChartData();
            });
        });
    }

    getGraphHeight(): number {
        return (this.chartView.nativeElement.parentElement.parentElement.parentElement.parentElement.parentElement.offsetHeight - 120);
    }

    ngOnDestroy() {
        if (this.materialBudgetSubscription) {
            this.materialBudgetSubscription.unsubscribe();
        }
        if (this.chartDataSubscription) {
            this.chartDataSubscription.unsubscribe();
        }
        if (this.headerServiceSubscription) {
            this.headerServiceSubscription.unsubscribe();
        }
    }

    getChartData() {
        if (this.chartDataSubscription) {
            this.chartDataSubscription.unsubscribe();
        }

        this.chartDataSubscription = this.purchaseOrderService.getDashboardCharts().subscribe(res => {
                this.chart.hideLoading();
                if (res['series'] && res['series'].length) {
                    this.showNoData = false;
                    this.buildHighcharts(res['series'], res['drilldown'], res['budgets']);
                } else {
                    this.showNoData = true;
                    this.buildHighcharts([], [], []);
                }
            },
            error => {
                this.toastr.error(_lang('Materials Budget') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                });
            });
    }

    getData() {
        this.loading = true;
        this.setPage({offset: this.page.pageNumber});
    }

    setPage(pageInfo) {
        this.page.pageNumber = pageInfo.offset;

        this.serverCall(this.page).subscribe(result => {
                const pagedData = this.getPagedData(this.page, result.count, result.data);
                this.page = pagedData.page;
                this.rows = pagedData.data;
                window.dispatchEvent(new Event('resize'));
                this.loading = false;
                console.log(this.rows);
            },
            error => {
                console.log(error);
                this.loading = false;
                window.dispatchEvent(new Event('resize'));
                this.toastr.error(_lang('Transaction') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                });
            })

    }

    private getPagedData(page: Page,  count: number, transactionData: Array<Transaction>): PagedData<Transaction> {
        const pagedData = new PagedData<Transaction>();
        pagedData.data = transactionData;
        page.totalElements = count;
        page.totalPages = page.totalElements / page.size;
        pagedData.page = page;
        return pagedData;
    }

    serverCall(page: Page): Observable<IResponse<Transaction>> {
        return this.transactionService.getTransactions(this.page);
    }

    resendTransaction(id): void {
        // this.transactionService.resendTransaction(id).subscribe(value => {
        //     console.log(value);
        //     this.getData();
        // });
    }

    onSort(event) {
        console.log('Sort Event', event);
        if (!_.isEmpty(event.sorts)) {
            for (const item of event.sorts) {
                this.page.sortParams[item.prop] = item.dir;
            }
        }
        this.ngOnInit();
    }

    techScanAdd() {
        var techScanResult = this.techScanService.isScanCanBeCreated(this.headerService);
        if (techScanResult['canBeCreated']) {
          this.router.navigate(['/tech-scans/add/0']);
        } else {
          this.toastr.error(techScanResult['error']);
        }
    }

    jobTicketAdd() {
        var jobTicketResult = this.jobTicketService.isCanBeCreated(this.headerService);
        if (jobTicketResult['canBeCreated']) {
          this.router.navigate(['/job-tickets/add/0']);
        } else {
          this.toastr.error(jobTicketResult['error']);
        }
    }

    purchaseOrderAdd() {
        var purchaseOrderResult = this.purchaseOrderService.isPOCanBeCreated(this.headerService);
        if (purchaseOrderResult['canBeCreated']) {
            this.router.navigate(['/purchase-orders/add/0']);
        } else {
            this.toastr.error(purchaseOrderResult['error']);
        }
    }

    checkAll(ev) {
        this.rows.forEach(x => x.checked = ev.target.checked)
    }

    isChecked() {
        return !this.rows.some(_ => _.checked);
    }

    isAllChecked() {
        return this.rows.every(_ => _.checked);
    }

    resendTransactions() {
        this.checkedRows = this.rows.filter(_ => _.checked);
    }

    buildHighcharts(series, drilldown, budgets) {
        let me = this;
        Highcharts.setOptions({
            lang: {
                drillUpText: _lang('Previous')
            }
        });

        this.chart = Highcharts.chart(this.container.nativeElement, {
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Monthly Goal ($)'
                },
                min: 0,
            },
            title: {
                text: 'Materials Budget'
            },
            subtitle: {
                text: this.showNoData ? _lang('There is no data') : ''
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        formatter: function() {
                            return this.series.type === 'column' ? ( this.point.hasOwnProperty('percentage') ? (this.point.y.toFixed(2) + ' (' + this.point.percentage.toFixed(0) + '%)') : this.point.y.toFixed(2)) : '';
                        }
                    }
                },
                column: {
                    colorByPoint: true,
                },
                line: {
                    // pointPlacement: -0.5
                }
            },
            tooltip: {
                formatter: function() {
                    return (this.series.name === 'Budget' ? 'Budget: ' : this.point.name + '<br/>') + '<b>$' + this.point.y.toFixed(2) + '</b>';
                }
            },
            series: [
                {
                    name: '',
                    data: series,
                    type: 'column'
                },
                {
                    name: 'Budget',
                    type: 'line',
                    marker: {
                        lineWidth: 2,
                        lineColor: 'red',
                        fillColor: 'white'
                        // enabled: false
                    },
                    step: 'center',
                    color: 'red',
                    data: budgets,
                    enableMouseTracking: true
                }],
            drilldown: {
                series: drilldown
            }
        });
    }

    onSlideShow() {
        this.router.navigate(['charts-slide-show/dashboard']);
    }
}
