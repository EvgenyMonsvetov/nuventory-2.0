import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { UserlogComponent } from './userlog.component';
import { _lang } from '../../pipes/lang';

const routes: Routes = [
  {
    path: '',
    component: UserlogComponent,
    data: {
      title: _lang('Userlog')
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserlogRoutingModule {}
