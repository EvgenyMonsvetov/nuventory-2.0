import { NgModule } from '@angular/core';
import { UserlogComponent } from './userlog.component';
import { UserlogRoutingModule } from './userlog-routing.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
      UserlogRoutingModule,
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  declarations: [
      UserlogComponent
  ]
})
export class UserlogModule { }
