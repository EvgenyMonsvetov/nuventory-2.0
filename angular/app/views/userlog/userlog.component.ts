import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Page } from '../../model/page';
import { PagedData } from '../../model/paged-data';
import { permissions } from '../../permissions';
import { ToastrService } from 'ngx-toastr';
import { UserlogService } from '../../services/userlog.service';
import { Userlog } from '../../model/userlog';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../pipes/lang';

@Component({
  templateUrl: 'userlog.component.html',
  providers: [
    UserlogService
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserlogComponent implements OnInit {
  public loading = false;
  page = new Page();
  rows = new Array<Userlog>();
  private ret: PagedData<Userlog>;

  loginFilter = '';
  logoutFilter = '';
  userFilter = '';
  ipFilter = '';
  userInfoFilter = '';

  public permissions = permissions;

  constructor(private userlogService: UserlogService,
              private toastr: ToastrService,
              private titleService: TitleService,
              private route: ActivatedRoute ) {
    this.page.pageNumber = 0;
    this.page.size = 50;
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.loading = true;
    this.setPage({offset: 0});
  }

  getData() {
    this.loading = true;
    this.setPage({offset: this.page.pageNumber});
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;

    this.serverCall(this.page).subscribe(result => {
          const pagedData = this.getPagedData(this.page, result.count, result.data);
          this.page = pagedData.page;
          this.rows = pagedData.data;
          window.dispatchEvent(new Event('resize'));
          this.loading = false;
          console.log(this.rows);
        },
        error => {
          console.log(error);
          this.loading = false;
          window.dispatchEvent(new Event('resize'));
          this.toastr.error(_lang('Userlogs') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        })

  }

  private getPagedData(page: Page,  count: number, userlogData: Array<Userlog>): PagedData<Userlog> {
    const pagedData = new PagedData<Userlog>();
    pagedData.data = userlogData;
    page.totalElements = count;
    page.totalPages = page.totalElements / page.size;
    pagedData.page = page;
    return pagedData;
  }

  serverCall(page: Page): Observable<IResponse<Userlog>> {
    return this.userlogService.getUserlogs(this.page);
  }

  onSort(event) {
    console.log('Sort Event', event);
    if (!_.isEmpty(event.sorts)) {
      for (const item of event.sorts) {
        this.page.sortParams[item.prop] = item.dir;
      }
    }
    this.ngOnInit();
  }

  onSubmit() {
    this.page.filterParams['ED1_LOGIN'] = this.loginFilter;
    this.page.filterParams['ED1_LOGOUT'] = this.logoutFilter;
    this.page.filterParams['ED1_USER'] = this.userFilter;
    this.page.filterParams['ED1_IP'] = this.ipFilter;
    this.page.filterParams['ED1_USER_INFO'] = this.userInfoFilter;

    this.getData();
  }
}