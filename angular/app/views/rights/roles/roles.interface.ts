import {IPermission} from "../permissions/permissions.interface";

export interface IRole{
    'type': number,
    'name': string,
    'description': string,
    'createdAt': Date,
    'updatedAt': Date,
    'permissions': Array<IPermission>
}