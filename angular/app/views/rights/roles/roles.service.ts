import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

import {GeneralService} from './../../../services/general.service';
import {IRole}      from './roles.interface';
import {StoreService} from "../../../services/store.service";

@Injectable()
export class RolesService extends GeneralService {

    private store: StoreService;

    constructor(protected http: HttpClient) {
        super(http);

        this.store = new StoreService('roles', 'name');
    }

    getStore(){
        return this.store;
    }

    getById(id: string): Observable<IRole> {

        const url = `${this.apiUrl}/roles/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .do(data => {
                this.store.add(data);
            })
            .catch(this.handleError);
    }

    save(role: IRole, isCreate: boolean): Observable<IRole> {
        if (role.name === '' || isCreate) {
            return this.create(role);
        }
        return this.update(role);
    }

    getAll( options: any): Observable<IResponse<any>> {
        let params = new HttpParams();

        if(options && options.page){
            const current_page = options.page.pageNumber + 1;
            params = params.append('perPage', options.page.size);
            params = params.append('page', current_page);
            if (!_.isUndefined(options.page.sortParams)) {
                for (const key of Object.keys(options.page.sortParams)) {
                    const value = options.page.sortParams[key];
                    params = params.append('sort', key + ',' + value );
                }
            }
            if (!_.isUndefined(options.page.filterParams)) {
                for (const key of Object.keys(options.page.filterParams)) {
                    const value = options.page.filterParams[key];
                    if (value && value !== '') {
                        params = params.append('filter', key + ',' + value );
                    }
                }
            }
        }

        return this.http.get<IResponse<any>>(this.apiUrl + '/roles', { params : params });
    }

    private create(role: IRole): Observable<IRole> {
        return this.http.post(this.apiUrl + '/roles', role)
            .do(data => console.log('createdRole: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private update(role: IRole): Observable<IRole> {
        const url = `${this.apiUrl + '/roles'}/${role.name}`;
        return this.http.put(url, role)
            .do(data => this.store.update(data))
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/roles/' + id);
    }

    initialize(): IRole {
        // Return an initialized object
        return {
            type: 1,
            name: '',
            description: '',
            createdAt: new Date(),
            updatedAt: new Date(),
            permissions: []
        };
    }
}
