import { Component, OnInit } from '@angular/core';
import { RolesService } from '../roles.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { IPermission } from '../../permissions/permissions.interface';
import { TitleService } from 'app/services/title.service';
import {Constants} from "../../../../constants";

@Component({
  selector: 'app-role-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  preserveWhitespaces: true
})
export class FormComponent implements OnInit {

  public form: FormGroup;
  public loading = false;

  public permissions: Array<IPermission>;
  private constants = Constants;

  constructor(
      private fb: FormBuilder,
      private router: Router,
      private route: ActivatedRoute,
      private rolesService: RolesService,
      private titleService: TitleService
  ) {
      this.form = this.fb.group({
        name: new FormControl({value: ''}, Validators.required),
        description: ['test', Validators.required],
        permissions: this.fb.array([])
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

      this.loading = true;

      const roleName = this.route.snapshot.params.name;
      this.rolesService.getById(roleName).subscribe( data => {
        this.loading = false;

        const fb = this.fb;
        let permissionsControl = this.form.get('permissions') as FormArray;
        data.permissions.forEach(function (permission) {
            permissionsControl.push(fb.group(permission));
        });

        this.form.get('name').setValue(data.name);

        if(this.route.snapshot.url.join('/') === 'roles/add') {
            this.form.get('name').enable();
        }else{
            this.form.get('name').disable();
        }
        this.form.get('description').setValue(data.description);
    });
  }

  save(form: NgForm) {

      if (form.dirty && form.valid) {
        // Copy the form values over the vendor object values
        const v = Object.assign({
            name: this.form.get('name').value
        }, form.value);

        const isCreate: boolean = this.route.snapshot.url.join('/') === 'roles/add' ? true : false;

        this.rolesService.save(v, isCreate)
            .subscribe(
                () => this.onSaveComplete()
            );
        } else if (!form.dirty) {
            this.onSaveComplete();
        }
  }

  onSaveComplete(){
      this.router.navigate(['rights/roles']);
  }

  onCancel(){
    this.router.navigate(['rights/roles']);
  }

}
