import { Component, OnInit } from '@angular/core';
import { RolesService } from './roles.service';
import { permissions } from '../../../permissions';
import { Router, ActivatedRoute } from '@angular/router';
import { TitleService } from 'app/services/title.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
  providers: [
    RolesService
  ],
})
export class RolesComponent implements OnInit {

    public  permissions = permissions;

    constructor(private router: Router,
        private titleService: TitleService,
        private route: ActivatedRoute ) {}

    ngOnInit() {
        this.titleService.setNewTitle(this.route);
    }

    addRole() {
        this.router.navigate(['rights/roles/add']);
    }
}
