import { Component, OnInit, Input } from '@angular/core';
import { RolesService } from '../roles.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { IRole } from '../roles.interface';
import { Page } from '../../../../model/page';
import { permissions } from '../../../../permissions';
import { PagedData } from '../../../../model/paged-data';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../../../pipes/lang';

@Component({
  selector: 'app-rights-roles-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    @Input() rows: Array<IRole>;
    @Input() autoload: boolean = true;

    public loading = false;
    page = new Page();

    public  permissions = permissions;

    constructor(
        private rolesService: RolesService,
        private toastr: ToastrService,
        private router: Router,
        private titleService: TitleService,
        private route: ActivatedRoute ) {
        this.page.pageNumber = 0;
        this.page.size = 10;
    }

    ngOnInit() {
        this.titleService.setNewTitle(this.route);
        if(this.autoload){
            this.loading = true;
            this.setPage({offset: 0});
        }
    }

    setPage(pageInfo) {
        this.page.pageNumber = pageInfo.offset;

        this.rolesService.getAll(null).subscribe(result => {
                const pagedData = this.getPagedData(this.page, result.count, result.data);
                this.page = pagedData.page;
                this.rows = pagedData.data;
                window.dispatchEvent(new Event('resize'));
                this.loading = false;
            },
            error => {
                console.log(error);
                this.loading = false;
                window.dispatchEvent(new Event('resize'));
                this.toastr.error(_lang('TimeOut occured'), _lang('Service Error'), {
                    timeOut: 3000,
                });
            })
    }

    private getPagedData(page: Page, count: number, rolesData: Array<IRole>): PagedData <IRole> {
        const pagedData = new PagedData<IRole>();
        pagedData.data = rolesData;
        page.totalElements = count;
        page.totalPages = page.totalElements / page.size;
        pagedData.page = page;
        return pagedData;
    }

    editRole(value){
        this.router.navigate(['rights/roles/edit/' + value]);
    }

    deleteRole(id){
      this.loading = true;
      this.rolesService.delete(id).subscribe(result => {
          this.setPage({offset: this.page});
      });
    }
}
