import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RolesService } from '../roles.service';
import { ToastrService } from 'ngx-toastr';
import { TitleService } from 'app/services/title.service';
import { ActivatedRoute } from '@angular/router';
import { _lang } from '../../../../pipes/lang';

@Component({
  selector: 'app-rights-roles-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  @Input() activeRole: number;
  @Output() notify: EventEmitter<number> = new EventEmitter<number>();

  private roles: Array<Object> = [];
  private loading: boolean = false;
  constructor(
    private rolesService: RolesService,
    private toastr: ToastrService,
    private titleService: TitleService,
    private route: ActivatedRoute ) { }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.loading = true;
    this.rolesService.getAll(null).subscribe(result => {
      this.roles = result.data;
      this.loading = false;
    },
    error => {
        this.loading = false;
        window.dispatchEvent(new Event('resize'));
        this.toastr.error(_lang('TimeOut occured'), _lang('Service Error'), {
            timeOut: 3000,
        });
    })
  }

  onChange(value){
    this.notify.emit(value);
  }
}
