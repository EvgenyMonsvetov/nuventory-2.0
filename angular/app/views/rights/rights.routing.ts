import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from '../../permissions';
import { RightsComponent } from './rights.component';
import { RolesComponent } from './roles/roles.component';
import { AssignmentComponent } from './assignment/assignment.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { AuthGuardService as AuthGuard} from './../../auth-guard.service';
import { FormComponent as RolesComponentForm } from './roles/form/form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
      path: '',
      component: RightsComponent,
      canActivate: [AuthGuard, NgxPermissionsGuard],
      data: {
          permissions: {
              only: [permissions.PERMISSION_READ]
          }
      }
  }, {
    path: 'assignment/user/:id',
    component: AssignmentComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
      title: _lang('Assignment Form'),
      only: [permissions.RIGHTS_READ]
    }
  }, {
    path: 'permissions',
    component: PermissionsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
          only: [permissions.PERMISSION_READ]
      }
    }
  }, {
    path: 'roles',
    component: RolesComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
          only: [permissions.ROLES_READ]
      }
    }
  },{
    path: 'roles/add',
    component: RolesComponentForm,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
          only: [permissions.ROLES_ADD]
      }
    }
  },
  {
    path: 'roles/edit/:name',
    component: RolesComponentForm,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
      permissions: {
          only: [permissions.ROLES_EDIT]
      }
    }
  }
];
