import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

import {GeneralService} from './../../../services/general.service';
import {IPermission} from "./permissions.interface";
import {StoreService} from "../../../services/store.service";

@Injectable()
export class PermissionsService extends GeneralService {

    private store: StoreService;

    constructor(protected http: HttpClient) {
        super(http);

        this.store = new StoreService('permissions', 'name');
    }

    getAll(): Observable<IResponse<IPermission>> {
        let params = new HttpParams();
        return this.http.get<IResponse<IPermission>>(this.apiUrl + '/permission', { params : params });
    }
}
