import { Component, OnInit } from '@angular/core';
import { IPermission } from './permissions.interface';
import { PermissionsService } from './permissions.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../../pipes/lang';


@Component({
  selector: 'app-rights-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent implements OnInit {

    public loading = false;
    rows = new Array<IPermission>();

    constructor(
        private permissionsService: PermissionsService,
        private toastr: ToastrService,
        private titleService: TitleService,
        private route: ActivatedRoute  ) {
    }

    ngOnInit() {
        this.titleService.setNewTitle(this.route);
        this.loading = true;
        this.permissionsService.getAll().subscribe(result => {
            this.rows = result.data;

            window.dispatchEvent(new Event('resize'));
            this.loading = false;
        },
        error => {
            console.log(error);
            this.loading = false;
            window.dispatchEvent(new Event('resize'));
            this.toastr.error(_lang('TimeOut occured'), _lang('Service Error'), {
                timeOut: 3000,
            });
        })
    }
}
