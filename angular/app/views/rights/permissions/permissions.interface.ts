export interface IPermission {
    'type': number,
    'name': string,
    'description': string,
    'createdAt': Date,
    'updatedAt': Date,
    'inheritsFromRole': boolean,
    'hasPermission': boolean
}