import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RouterModule } from '@angular/router';
import { RightsMenuComponent } from './rights-menu/rights-menu.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { RolesComponent } from './roles/roles.component';
import { OperationsComponent } from './operations/operations.component';
import { RightsComponent } from './rights.component';
import { AssignmentComponent } from './assignment/assignment.component';
import { routes } from './rights.routing';
import { FormComponent as RolesFormComponent } from './roles/form/form.component';
import { ListComponent } from './roles/list/list.component';
import { SelectComponent } from './roles/select/select.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  declarations: [
      RightsComponent,
      AssignmentComponent,
      RightsMenuComponent,
      PermissionsComponent,
      RolesComponent,
      OperationsComponent,
      RolesFormComponent,
      ListComponent,
      SelectComponent
  ],
   exports:[
     ListComponent,
     SelectComponent
   ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
    ]
})
export class RightsModule { }
