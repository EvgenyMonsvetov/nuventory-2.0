import {Component, ElementRef, OnInit} from '@angular/core';
import {permissions} from "../../../permissions";

@Component({
  selector: 'app-rights-menu',
  templateUrl: './rights-menu.component.html',
  styleUrls: ['./rights-menu.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})
export class RightsMenuComponent implements OnInit {
  show: boolean = false;
  public elementRef;

  public permissions = permissions;
  constructor(private myElement: ElementRef) {
    this.elementRef = myElement;
  }

  ngOnInit() {
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (inside) {
      this.show = !this.show;
    } else {
      this.show = false;
    }
  }
}
