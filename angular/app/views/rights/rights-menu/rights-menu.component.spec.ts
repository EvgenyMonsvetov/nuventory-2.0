import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightsMenuComponent } from './rights-menu.component';

describe('RightsMenuComponent', () => {
  let component: RightsMenuComponent;
  let fixture: ComponentFixture<RightsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
