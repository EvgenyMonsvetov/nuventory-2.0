import { Component, OnInit } from '@angular/core';
import { Page } from '../../../model/page';
import { PagedData } from '../../../model/paged-data';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { PermissionsService } from '../permissions/permissions.service';
import { IPermission } from '../permissions/permissions.interface';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../../pipes/lang';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.scss']
})
export class AssignmentComponent implements OnInit {
  page = new Page();
  rows = new Array<IPermission>();
  private loading = false;

  constructor(private permissionsService: PermissionsService,
              private toastr: ToastrService,
              private router: Router,
              private titleService: TitleService,
              private route: ActivatedRoute
              ) {
    this.page.pageNumber = 0;
    this.page.size = 10;
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.loading = true;
    this.setPage({offset: 0});
  }

  getData() {
    this.loading = true;
    this.setPage({offset: this.page.pageNumber});
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;

    this.serverCall(this.page).subscribe(result => {
          const pagedData = this.getPagedData(this.page, result.count, result.data);
          this.page = pagedData.page;
          this.rows = pagedData.data;
          window.dispatchEvent(new Event('resize'));
          this.loading = false;
          console.log(this.rows);
        },
        error => {
          console.log(error);
          this.loading = false;
          window.dispatchEvent(new Event('resize'));
          this.toastr.error(_lang('TimeOut occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        })
  }

  private getPagedData(page: Page,  count: number, rightData: Array<IPermission>): PagedData<IPermission> {
    const pagedData = new PagedData<IPermission>();
    pagedData.data = rightData;
    page.totalElements = count;
    page.totalPages = page.totalElements / page.size;
    pagedData.page = page;
    return pagedData;
  }

  serverCall(page: Page): Observable<IResponse<IPermission>> {
    return this.permissionsService.getAll();
  }

  editRight(id): void {
    this.router.navigate(['rights/assignment/user/' + id]);
  }
}
