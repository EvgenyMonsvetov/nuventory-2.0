import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { StoreOrdersComponent } from './store-orders.component';
import { StoreOrderViewComponent } from './store-order-view/store-order-view.component';
import { StoreOrderAddComponent } from './store-order-add/store-order-add.component';

export const routes: Routes = [{
    path: '',
    component: StoreOrdersComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Store Orders'),
        permissions: {
            only: [permissions.STORE_ORDERS_READ]
        }
    }
},{
    path: 'view/:id',
    component: StoreOrderViewComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Store Order'),
        permissions: {
            only: [permissions.STORE_ORDERS_READ]
        }
    }
},{
    path: 'receiving/:id',
    component: StoreOrderViewComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Store Order'),
        permissions: {
            only: [permissions.STORE_ORDERS_READ]
        }
    }
},{
    path: 'add/:id',
    component: StoreOrderAddComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Create Shop Orders'),
        permissions: {
            only: [permissions.STORE_ORDERS_ADD]
        }
    }
},{
    path: 'complete/:id',
    component: StoreOrderViewComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Store Order'),
        permissions: {
            only: [permissions.STORE_ORDERS_READ]
        }
    }
}];