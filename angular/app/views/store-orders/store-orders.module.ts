import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './store-orders.routing';
import { StoreOrderService } from '../../services/store-order.service';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MatRadioModule } from '@angular/material/radio';
import { StoreOrdersComponent } from './store-orders.component';
import { StoreOrderViewComponent } from './store-order-view/store-order-view.component';
import { StoreOrderAddComponent } from './store-order-add/store-order-add.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CurrencyModule } from '../../components/currency/currency.module';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      MatRadioModule,
      MatAutocompleteModule,
      CurrencyModule,
      ToastrModule.forRoot({
          positionClass: 'toast-middle-center',
          closeButton: false
      })
  ],
  providers: [
      StoreOrderService
  ],
  declarations: [
      StoreOrdersComponent,
      StoreOrderViewComponent,
      StoreOrderAddComponent
  ]
})
export class StoreOrdersModule { }
