import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreOrderAddComponent } from './store-order-add.component';

describe('StoreOrderAddComponent', () => {
  let component: StoreOrderAddComponent;
  let fixture: ComponentFixture<StoreOrderAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreOrderAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreOrderAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
