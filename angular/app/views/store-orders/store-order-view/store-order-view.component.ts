import { Component, ElementRef, Inject, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { DOCUMENT, Location } from '@angular/common';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { environment } from '../../../../environments/environment';
import { _lang } from '../../../pipes/lang';
import { permissions } from '../../../permissions';
import { Page } from '../../../model/page';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from 'primeng/api';
import { StoreOrderService } from '../../../services/store-order.service';
import { OrderLineService } from '../../../services/order-line.service';
import { OrderLineModalComponent } from '../../../components/order-line-modal/order-line-modal.component';
import { CellEditor } from 'primeng/table';

@Component({
  selector: 'app-store-order-view',
  templateUrl: './store-order-view.component.html',
  styleUrls: ['./store-order-view.component.scss'],
  providers: [
    StoreOrderService,
    OrderLineService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class StoreOrderViewComponent implements OnInit {
  @ViewChild('contentGrid')   contentGrid: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;
  @ViewChildren(CellEditor)   editors: QueryList<CellEditor>;

  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private StoreOrderService = StoreOrderService;
  private record: any;
  private apiUrl = environment.API_URL;

  public ORDER_STATUS_OPEN = 0;
  public ORDER_STATUS_RECEIVED = 1;
  public ORDER_STATUS_SENT = 2;
  public ORDER_STATUS_INVOICED = 3;
  public ORDER_STATUS_PICK_TICKET = 4;

  private KEYCODE_TAB   = 9;
  private KEYCODE_ENTER = 13;

  private statusData: Array<any> = [
    {'label': _lang('#Open#'),      'OR1_STATUS' : 0},
    {'label': _lang('#Received#'),  'OR1_STATUS' : 1}
  ];

  private newCustomer = '0';
  private panelOpenState = false;

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[] = [];
  orderLines: any[] = [];
  selected: any;

  private page = new Page();
  private loading: boolean = true;
  private processingUserAction = false;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private storeOrderService: StoreOrderService,
              private orderLineService: OrderLineService,
              private confirmationService: ConfirmationService,
              public  dialog: MatDialog,
              private elementRef: ElementRef,
              private titleService: TitleService,
              private toastr: ToastrService,
              @Inject(DOCUMENT) document,
              private router: Router,
              private location: Location) {
    this.page.filterParams = {};
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      // { field: 'OR1_NUMBER',              header: _lang('OR1_NUMBER'),        visible: true, width: 57 },
      { field: 'OR2_LINE',                header: _lang('Line #'),            visible: true, width: 35 },
      { field: 'OR2_STATUS_TEXT',         header: _lang('OR2_STATUS_TEXT'),   visible: true, width: 125 },
      { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),   visible: true, width: 100 },
      { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),         visible: true, width: 190 },
      { field: 'OR2_RECEIVING_QTY',       header: _lang('OR2_RECEIVING_QTY'), visible: true, width: 76 },
      { field: 'OR2_RECEIVED_QTY',        header: _lang('OR2_RECEIVED_QTY'),  visible: true, width: 75 },
      { field: 'QTY',                     header: _lang('QTY'),               visible: true, width: 79 },
      { field: 'UM1_RECEIVE_NAME',        header: _lang('UM1_RECEIVE_NAME'),  visible: true, width: 80 },
      { field: 'OR2_ORDER_QTY',           header: _lang('PO2_ORDER_QTY'),     visible: true, width: 71 },
      { field: 'UM1_NAME',                header: _lang('MD1_UM'),            visible: true, width: 80 },
      { field: 'MD1_UNIT_PRICE',          header: _lang('PO2_UNIT_PRICE'),    visible: true, width: 70 },
      { field: 'OR2_EXTENDED_VALUE',      header: _lang('OR2_EXTENDED_VALUE'),visible: true, width: 78 },
      { field: 'MODIFIED_ON',             header: _lang('#ModifiedOn#'),      visible: false, width: 120 },
      { field: 'MODIFIED_BY',             header: _lang('#ModifiedBy#'),      visible: false, width: 120 },
      { field: 'CREATED_ON',              header: _lang('#CreatedOn#'),       visible: false, width: 120 },
      { field: 'CREATED_BY',              header: _lang('#CreatedBy#'),       visible: false, width: 120 }
    ];

    this.record = this.storeOrderService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.page.filterParams['OR1_ID'] = this.route.snapshot.params.id;
      this.getItems();
      this.getOrder();
    }
  }

  getOrder() {
    this.storeOrderService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
      if (result[0]) {
        let data = result[0];
        data['OR1_ID'] = parseInt(data['OR1_ID']);
        data['OR1_STATUS'] = parseInt(data['OR1_STATUS']);
        this.newCustomer = parseInt(data['CU1_ID']) > 0 ? '1' : '0';
        this.record = data;
      }
    });
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.orderLineService.getAll(this.page).subscribe(res => {
          this.orderLines = res.data['currentVocabulary'];
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Order lines') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Order Form'),
        componentRef: this,
        data: [],
        componentName: 'StoreOrderViewComponent'
      },
    });
  }

  onOpen(rowData) {
    this.showStoreOrderForm(rowData['OR2_ID']);
  }

  openRecord() {
    if (this.selected) {
      this.showStoreOrderForm(this.selected['OR2_ID']);
    }
  }

  onEditInit( $event ) {
    var interval = setInterval(() => {
      const input = <HTMLInputElement>document.getElementById('input_' + $event.data.OR2_ID);
      if (input) {
        clearInterval(interval);
        interval = null;
        input.select();
      }
    }, 5);

  }

  unitPriceKeyDown(e, rowIndex) {
    if ( (e.keyCode === this.KEYCODE_TAB || e.keyCode === this.KEYCODE_ENTER) ) {
      e.stopPropagation();
      const i = rowIndex + 1;
      const elem = document.getElementById('td_unit_price_' + i);
      if (elem) {
        elem.click();
      } else {
        const currentTd = <HTMLTableCellElement>document.getElementById('td_unit_price_' + rowIndex);
        currentTd.nextSibling['click']();
      }
    }
  }

  unitPriceChanged(rowData, e) {
    rowData['OR2_EXTENDED_VALUE'] = (rowData['OR2_ORDER_QTY'] * e).toString();
    let total = 0;
    this.orderLines.forEach(function (item) {
      total += parseFloat(item['OR2_EXTENDED_VALUE']);
    });
    this.record['OR1_TOTAL_VALUE'] = total.toFixed(2);
  }

  qtyKeyDown(e, rowIndex) {
    if ( (e.keyCode === this.KEYCODE_TAB || e.keyCode === this.KEYCODE_ENTER) ) {
      e.stopPropagation();
      const i = rowIndex + 1;
      const elem = document.getElementById('td' + i);
      if (elem) {
        elem.click();
      } else {
        const currentTd = <HTMLTableCellElement>document.getElementById('td' + rowIndex);
        currentTd.nextSibling['click']();
      }
    }
  }

  orderedQtyKeyDown(e, rowIndex) {
    if ( (e.keyCode === this.KEYCODE_TAB || e.keyCode === this.KEYCODE_ENTER) ) {
      e.stopPropagation();
      const i = rowIndex + 1;
      const elem = document.getElementById('td_ordered_qty_' + i);
      if (elem) {
        elem.click();
      } else {
        const currentTd = <HTMLTableCellElement>document.getElementById('td_ordered_qty_' + rowIndex);
        currentTd.nextSibling['click']();
      }
    }
  }

  orderedQtyChanged(rowData, e) {
    rowData['OR2_EXTENDED_VALUE'] = (rowData['MD1_UNIT_PRICE'] * e).toString();
    rowData['QTY'] = this.parseToInt(rowData['OR2_ORDER_QTY']) * ( rowData['UM2_FACTOR'] ? this.parseToInt(rowData['UM2_FACTOR']) : 1);
    rowData['OR2_RECEIVING_QTY'] = this.parseToInt(rowData['OR2_ORDER_QTY'])
    let total = 0;
    this.orderLines.forEach(function (item) {
      total += parseFloat(item['OR2_EXTENDED_VALUE']);
    });
    this.record['OR1_TOTAL_VALUE'] = total.toFixed(2);
  }

  onReceive() {
    this.processingUserAction = true;
    var OR1_ID = this.route.snapshot.params.id;
    this.storeOrderService.receive(OR1_ID, this.orderLines).subscribe( result => {
      this.processingUserAction = false;
      if(result.success){
        this.toastr.success(
            _lang('Order(s) successfully marked as received.'),
            _lang('RECEIVING SUCCESS'), {
          timeOut: 3000,
        });
        this.router.navigate(['store-orders']);
      } else {
        this.toastr.error(
            _lang('There was an error while receiving. Please contact your administrator.'),
            _lang('RECEIVING ERROR'), {
          timeOut: 3000,
        });
      }
    }, () => {
      this.processingUserAction = false;
    });
  }

  showStoreOrderForm(poId) {
    let dialogRef = this.dialog.open(OrderLineModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Order Line'), OR2_ID: poId, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( value => {
      if (value) {
        this.getItems();
      }
    });
  }

  onDelete(event) {
    if (this.selected && this.selected['OR2_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.orderLineService.delete(this.selected['OR2_ID']).subscribe( result => {
                this.selected = null;
                this.getItems();
                this.getOrder();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete order line error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  onComplete() {
    this.processingUserAction = true;
    var OR1_ID = this.route.snapshot.params.id;
    this.storeOrderService.complete(OR1_ID, this.orderLines).subscribe( result => {
      if (result.success) {
        this.toastr.success(
            _lang('Order(s) successfully marked as received.'),
            _lang('COMPLETING SUCCESS'), {
              timeOut: 3000,
            });
        this.onPdfPrint();
      } else {
        this.processingUserAction = false;
        this.toastr.error(
            _lang('There was an error while completing. Please contact your administrator.'),
            _lang('MARK AS COMPLETED'), {
              timeOut: 3000,
            });
      }
    }, () => {
      this.processingUserAction = false;
    });
  }

  onPdfPrint() {
    this.page.filterParams['OR1_IDs'] = [this.record['OR1_ID']];

    this.storeOrderService.printPDF(this.page, true).subscribe( result => {
          this.router.navigate(['store-orders']);
          this.processingUserAction = false;
        },
        error => {
          this.processingUserAction = false;
          this.loading = false;
          this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  parseToInt(value) {
    if (typeof value === 'string') {
      return parseInt(value);
    } else {
      return value;
    }
  }

}
