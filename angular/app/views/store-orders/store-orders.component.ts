import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { _lang } from '../../pipes/lang';
import { StoreOrderService } from '../../services/store-order.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from '../../model/page';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Subject, Subscription } from 'rxjs';
import { formatDate } from '@angular/common';
import { HeaderService } from '../../services/header.service';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { InvoiceService } from '../../services/invoice.service';

@Component({
  selector: 'app-store-orders',
  templateUrl: './store-orders.component.html',
  styleUrls: ['./store-orders.component.scss'],
  providers: [
    ConfirmationService,
    InvoiceService
  ],
  preserveWhitespaces: true
})
export class StoreOrdersComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  formatDate = formatDate;

  @ViewChild('storeOrdersView')   storeOrdersView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';

  /** control for the selected filters */
  public orderedByCtrl: FormControl = new FormControl();
  public orderedByFilterCtrl: FormControl = new FormControl();
  public vendorCtrl: FormControl = new FormControl();
  public vendorFilterCtrl: FormControl = new FormControl();
  public locationFromCtrl: FormControl = new FormControl();
  public locationFromFilterCtrl: FormControl = new FormControl();
  public locationByCtrl: FormControl = new FormControl();
  public locationByFilterCtrl: FormControl = new FormControl();
  public modifiedByCtrl: FormControl = new FormControl();
  public modifiedByFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyOrderedBy = new Subject<void>();
  private _onDestroyVendor = new Subject<void>();
  private _onDestroyLocationFrom = new Subject<void>();
  private _onDestroyLocationBy = new Subject<void>();
  private _onDestroyModifiedBy = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredOrderedByUsers: any[] = [];
  public filteredVendors: any[] = [];
  public filteredShopsFrom: any[] = [];
  public filteredShopsBy: any[] = [];
  public filteredModifiedByUsers: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  invStatuses: any[] = [];
  statuses: any[] = [];
  cols: any[] = [];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;

  private users: Array<object> = [];
  private vendors:   Array<object> = [];
  private shops:   Array<object> = [];
  private subscription: Subscription;
  private currentLocation:any = null;

  private showReceiveButton = false;
  private showInvoiceButton = false;
  private showCompeteButton = false;

  constructor(private storeOrderService: StoreOrderService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private invoiceService: InvoiceService,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = {};

    this.invStatuses = [
      { label: _lang('#AllRecords#'), value: null},
      { label: _lang('#Open#'),       value: 'open'},
      { label: _lang('#Invoiced#'),   value: 'invoiced'},
      { label: _lang('Pick Ticket'),   value: 'pick_ticket'}
    ];

    this.statuses = [
      // { label: _lang('#AllRecords#'), value: null},
      { label: _lang('#Open#'),       value: 'open'},
      { label: _lang('#Received#'),   value: 'received'},
      { label: _lang('#Completed#'),  value: 'completed'}
    ];

    this.currentLocation = this.headerService.getCurrentLocation();
    this.subscription = this.headerService.subscribe({
          next: (v) => {
            this.currentLocation = v.selectedLocation;
            this.page.pageNumber = 0;
            this.page.filterParams = {};
            this.loading = true;
            this.initCols();
            this.initButtons();
            this.getItems();
          }
      });
    this.initButtons();
  }

  initCols() {
    var isCentral = this.currentLocation && parseInt(this.currentLocation['LO1_CENTER_FLAG']) ? true : false;
    this.cols = [
      { field: 'ORDER_BY_LOCATION_NAME',  header: _lang('ORDER_BY_LOCATION_NAME'),  visible: isCentral ? true : false, export: {visible: true, checked: isCentral ? true : false}, width: 160 },
      { field: 'ORDERED_BY',              header: _lang('#OrderedBy#'),             visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'ORDERED_ON',              header: _lang('OR1_ORDERED_ON'),          visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'OR1_NUMBER',              header: _lang('OR1_NUMBER'),              visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'OR1_INV_STATUS_TEXT',     header: _lang('OR1_INV_STATUS_TEXT'),     visible: true, export: {visible: true, checked: true}, width: 110 },
      { field: 'OR1_STATUS_TEXT',         header: _lang('OR1_STATUS_TEXT'),         visible: true, export: {visible: true, checked: true}, width: 110 },
      { field: 'COMPLETED_ON',            header: _lang('OR1_COMPLETED_ON'),        visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'OR1_TOTAL_VALUE',         header: _lang('OR1_TOTAL_VALUE'),         visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'VD1_NAME',                header: _lang('VD1_NAME'),                visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'ORDER_FROM_LOCATION_NAME',header: _lang('ORDER_FROM_LOCATION_NAME'),visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'ORDER_BY_LOCATION_NAME',  header: _lang('ORDER_BY_LOCATION_NAME'),  visible: isCentral ? false : true, export: {visible: true, checked: isCentral ? false : true}, width: 160 },
      { field: 'MODIFIED_ON',             header: _lang('#ModifiedOn#'),            visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MODIFIED_BY',             header: _lang('#ModifiedBy#'),            visible: true, export: {visible: true, checked: true}, width: 160 }
    ];
  }

  initButtons(){
    this.showReceiveButton = this.headerService.isCentralLocation( this.headerService.LO1_ID ) ? false : true;
    this.showInvoiceButton = !this.showReceiveButton;
    this.showCompeteButton = !this.showReceiveButton;
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);
    this.initCols();
    this.storeOrderService.preloadData().subscribe( data => {
      this.prepareData(data);
      this.getItems();
    });

    // listen for search field value changes
    this.orderedByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyOrderedBy))
        .subscribe(() => {
          this.filterOrderedByUsers();
        });
    this.vendorFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyVendor))
        .subscribe(() => {
          this.filterVendors();
        });
    this.locationFromFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocationFrom))
        .subscribe(() => {
          this.filterShopsFrom();
        });
    this.locationByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocationBy))
        .subscribe(() => {
          this.filterShopsBy();
        });
    this.modifiedByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyModifiedBy))
        .subscribe(() => {
          this.filterModifiedByUsers();
        });
  }

  private filterOrderedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.orderedByFilterCtrl.value;
    if (!search) {
      this.filteredOrderedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Ordered By Users
    this.filteredOrderedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterVendors() {
    if (!this.vendors) {
      return;
    }
    // get the search keyword
    let search = this.vendorFilterCtrl.value;
    if (!search) {
      this.filteredVendors = this.vendors;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the vendors
    this.filteredVendors = this.vendors.filter(vendor => vendor['VD1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterShopsFrom() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationFromFilterCtrl.value;
    if (!search) {
      this.filteredShopsFrom = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location from
    this.filteredShopsFrom = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterShopsBy() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationByFilterCtrl.value;
    if (!search) {
      this.filteredShopsBy = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location by
    this.filteredShopsBy = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterModifiedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.modifiedByFilterCtrl.value;
    if (!search) {
      this.filteredModifiedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Modified By Users
    this.filteredModifiedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.users = [{US1_ID: null, US1_NAME: _lang('#AllRecords#')}];
    let usersArr = data.users.map( user => {
      return {
        US1_ID:   user.US1_ID,
        US1_NAME: user.US1_NAME
      }
    }).filter( customer => {
      return customer.US1_NAME && customer.US1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.US1_NAME < b.US1_NAME)
        return -1;
      if (a.US1_NAME > b.US1_NAME)
        return 1;
      return 0;
    });
    this.users = this.users.concat(usersArr);
    this.filterOrderedByUsers();
    this.filterModifiedByUsers();

    this.vendors = [{VD1_ID: null, VD1_NAME: _lang('#AllRecords#')}];
    let vendorsArr = data.vendors.map( vendor => {
      return {
        VD1_ID:   vendor.VD1_ID,
        VD1_NAME: vendor.VD1_NAME
      }
    }).filter( customer => {
      return customer.VD1_NAME && customer.VD1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.VD1_NAME < b.VD1_NAME)
        return -1;
      if (a.VD1_NAME > b.VD1_NAME)
        return 1;
      return 0;
    });
    this.vendors = this.vendors.concat(vendorsArr);
    this.filterVendors();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);
    this.filterShopsFrom();
    this.filterShopsBy();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this._onDestroyOrderedBy.next();
    this._onDestroyOrderedBy.complete();
    this._onDestroyVendor.next();
    this._onDestroyVendor.complete();
    this._onDestroyLocationFrom.next();
    this._onDestroyLocationFrom.complete();
    this._onDestroyLocationBy.next();
    this._onDestroyLocationBy.complete();
    this._onDestroyModifiedBy.next();
    this._onDestroyModifiedBy.complete();
  }

  getScrollHeight(): number {
    return (this.storeOrdersView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getItems() {
    this.storeOrderService.getAll(this.page).subscribe(res => {
      this.items = res.data['currentVocabulary'];
      this.items.forEach(item => {
        item['OR1_INV_STATUS'] = parseInt(item['OR1_INV_STATUS']);
      });;
      this.totalRecords = res.data['count'];

      setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';}, 100);

      this.loading = false;
    },
    error => {
      this.loading = false;
      this.toastr.error(_lang('Store Orders') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Shop/Tech Order List'),
        componentRef: this,
        data: [],
        componentName: 'StoreOrdersComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'ORDERED_ON' || col === 'COMPLETED_ON' || col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970') {
      this.page.filterParams[col] = '';
    } else {
      this.page.filterParams[col] = value;
    }
    this.page.pageNumber = 0;
    this.getItems();
  }

  onOpen() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['store-orders/view/' + this.selected[0]['OR1_ID']]);
  }

  editRecord(rowData) {
    this.router.navigate(['store-orders/view/' + rowData['OR1_ID']]);
  }

  onReceive() {
    if (!this.selected || this.selected.length !== 1) {
      this.toastr.error(
          _lang('Please select exactly one record before performing this operation.'),
          _lang('SELECTION ERROR'), {
        timeOut: 3000,
      });
    }else {
      var record = this.selected[0];
      if (parseInt(record.OR1_STATUS) !== StoreOrderService.ORDER_STATUS_SENT ) {
        this.toastr.error(
          _lang('You can only receive orders that are marked as completed.'),
          _lang('#Receiving#'), {
          timeOut: 3000,
        });
      } else {
        this.router.navigate(['store-orders/receiving/' + record.OR1_ID ]);
      }
    }
  }

  onPickTicket() {
    if (!this.selected) {
      this.toastr.error(
          _lang('Please select at least one record.'),
          _lang('SELECTION ERROR'), {
            timeOut: 3000,
          });
    } else {
      var error = false;
      this.selected.forEach(item => {
        if (parseInt(item['OR1_INV_STATUS']) !== StoreOrderService.ORDER_STATUS_OPEN) {
          error = true;
          return;
        }
      });
      if (error) {
        this.toastr.error(
            _lang('You can only pick a ticket which has OPEN status.'),
            _lang('PICK TICKET'), {
              timeOut: 3000,
            });
      } else {
        var OR1_IDs = this.selected.map( item => item['OR1_ID'] );
        this.storeOrderService.pickTicket(OR1_IDs).subscribe( result => {
          if(result.success){
            this.getItems();
            this.confirmationService.confirm({
              header: _lang('PICK TICKET'),
              message: this.selected.length > 1 ? _lang('Orders successfully marked as pick ticket. Would you like to also print them?') :
                                                  _lang('Order successfully marked as pick ticket. Would you like to also print it?'),
              accept: () => {
                let page = new Page();
                page.filterParams['OR1_IDs'] = OR1_IDs;

                this.storeOrderService.printPDF(page, true).subscribe( result => {
                    },
                    error => {
                      this.loading = false;
                      this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
                        timeOut: 3000,
                      });
                    });
              }
            });
          } else {
            this.toastr.error(
                _lang('There was an error while pick ticket. Please contact your administrator.'),
                _lang('MARK AS PICK TICKET'), {
                  timeOut: 3000,
                });
          }
        });
      }
    }
  }

  onInvoice() {
    if (!this.selected) {
      this.toastr.error(
          _lang('Please select exactly one record before performing this operation.'),
          _lang('SELECTION ERROR'), {
        timeOut: 3000,
      });
    } else {
      var error = false;
      this.selected.forEach(item => {
          if (parseInt(item['OR1_INV_STATUS']) === StoreOrderService.ORDER_STATUS_INVOICED) {
              error = true;
              return;
          }
      });
      if (error) {
          this.toastr.error(
              _lang('You cannot invoice an invoiced order again.'),
              _lang('INVOICING'), {
                  timeOut: 3000,
              });
      } else {
        var OR1_IDs = this.selected.map( item => item['OR1_ID'] );
        this.invoiceService.createFromStoreOrder(OR1_IDs).subscribe( result => {
          if (result.success) {
            this.getItems();
            this.confirmationService.confirm({
              header: _lang('INVOICING'),
              message: _lang('Invoice(s) generated successfully. Would you like to also print it/them?'),
              accept: () => {
                let page = new Page();
                page.filterParams['OR1_IDs'] = OR1_IDs;

                this.invoiceService.printPDF(page).subscribe( result => {
                },
                error => {
                  this.loading = false;
                  this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                  });
                });
              }
            });
          } else {
            this.toastr.error(
                _lang('There was an error while receiving. Please contact your administrator.'),
                _lang('RECEIVING ERROR'), {
              timeOut: 3000,
            });
          }
        });
      }
    }
  }

  onComplete() {
    if (this.selected && this.selected.length === 1) {
      if (parseInt(this.selected[0]['OR1_INV_STATUS']) === StoreOrderService.ORDER_STATUS_PICK_TICKET) {
        this.router.navigate(['store-orders/complete/' + this.selected[0]['OR1_ID']]);
      } else {
        this.toastr.error(
            _lang('You can ONLY mark orders that are already picked but not yet completed. Please check your selection and try again.'),
            _lang('MARK AS COMPLETED'), {
              timeOut: 3000,
            });
      }
    }
  }

  onPdfPrint() {
    if (this.selected && this.selected.length) {
      const OR1_IDs: any[] = [];
      this.selected.forEach(function (item) {
        OR1_IDs.push(item['OR1_ID']);
      });
      this.page.filterParams['OR1_IDs'] = OR1_IDs;

      this.storeOrderService.printPDF(this.page).subscribe( result => {
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
      this.page.filterParams['OR1_IDs'] = null;
    }
  }

  onDelete(event) {
    if (this.selected.length && this.selected[0]['OR1_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          var OR1_IDs = this.selected.map( item => item['OR1_ID'] );
          this.storeOrderService.deleteMultiple(OR1_IDs).subscribe( result => {
              this.getItems();
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('On delete shop order error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.storeOrderService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

}
