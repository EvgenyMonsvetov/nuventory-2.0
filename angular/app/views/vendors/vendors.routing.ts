import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { VendorsComponent } from './vendors.component';
import { VendorFormComponent } from './vendor-form/vendor-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: VendorsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Vendors#'),
        permissions: {
            only: [permissions.VENDORS_READ]
        }
    }
}, {
    path: ':id',
    component: VendorsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Vendors#'),
        permissions: {
            only: [permissions.VENDORS_READ]
        }
    }
},{
    path: 'add/:id',
    component: VendorFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.VENDORS_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: VendorFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.VENDORS_EDIT]
        }
    }
},{
    path: 'clone/:id',
    component: VendorFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.VENDORS_ADD]
        }
    }
}];