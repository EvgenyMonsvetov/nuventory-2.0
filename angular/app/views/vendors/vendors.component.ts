import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { VendorService } from '../../services/vendor.service';
import { MatDialog} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../model/page';
import { _lang } from '../../pipes/lang';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { HeaderService } from '../../services/header.service';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class VendorsComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('vendorsView')   vendorsView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';
  private dialogContentHeight: string = '150px';

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[] = [];
  items: any[] = [];
  selectedVendor: any;
  masterVendors: boolean = false;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;
  private subscription: Subscription;

  constructor(private vendorService: VendorService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private location: Location,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    this.subscription = this.headerService.subscribe({
        next: (v) => {
          this.page.pageNumber = 0;
          this.getVendors();
        }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.page.filterParams = {'VD0_ID' : this.route.snapshot.params.id};
      this.masterVendors = true;
    } else {
      this.masterVendors = false;
    }

    this.cols = [
      { field: 'CO1_NAME',        header: _lang('CO1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'VD1_SHORT_CODE',  header: _lang('VD1_SHORT_CODE'),      visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'VD1_CUSTOMER_NO', header: _lang('VD1_CUSTOMER_NO'),     visible: true, export: {visible: true, checked: true}, width: 250 },
      { field: 'VD1_NAME',        header: _lang('VD1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'VD1_ADDRESS1',    header: _lang('#AddressStreet1#'),    visible: true, export: {visible: true, checked: true}, width: 250 },
      { field: 'VD1_ADDRESS2',    header: _lang('#AddressStreet2#'),    visible: true, export: {visible: true, checked: true}, width: 250 },
      { field: 'VD1_CITY',        header: _lang('#AddressCity#'),       visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'VD1_STATE',       header: _lang('#AddressState#'),      visible: true, export: {visible: true, checked: true}, width: 72 },
      { field: 'VD1_ZIP',         header: _lang('#AddressPostalCode#'), visible: true, export: {visible: true, checked: true}, width: 110 },
      { field: 'VD1_PHONE',       header: _lang('#Phone#'),             visible: true, export: {visible: true, checked: true}, width: 140 },
      { field: 'VD1_FAX',         header: _lang('#Fax#'),               visible: true, export: {visible: true, checked: true}, width: 140 },
      { field: 'VD1_EMAIL',       header: _lang('#Email#'),             visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'VD1_URL',         header: _lang('#URL#'),               visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'VD1_NO_INVENTORY',header: _lang('VD1_NO_INVENTORY'),    visible: true, export: {visible: true, checked: true}, width: 135 },
      { field: 'CREATED_ON',      header: _lang('#CreatedOn#'),         visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'CREATED_BY',      header: _lang('#CreatedBy#'),         visible: true, export: {visible: true, checked: true}, width: 140 },
      { field: 'MODIFIED_ON',     header: _lang('#ModifiedOn#'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),        visible: true, export: {visible: true, checked: true}, width: 140 }
    ];

    this.getVendors();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getContentDialogHeight(): number{
    return (this.vendorsView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48);
  }

  getScrollHeight(): number{
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight + 52);
  }

  getVendors() {
    this.loading = true;
    this.vendorService.getVendors(this.page).subscribe(res => {
          this.items = res.data['items'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Vendors') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Vendor List'),
        componentRef: this,
        data: [],
        componentName: 'VendorsComponent'
      },
    });
  }

  onPage(event) {
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getVendors();
  }

  onAddNewVendor() {
    this.router.navigate(['vendors/add/0']);
  }

  onCloneVendor() {
    if (this.selectedVendor)
      this.router.navigate(['vendors/clone/' + this.selectedVendor['VD1_ID']], {queryParams: {clone: true}});
  }

  editRecord() {
    if (this.selectedVendor)
      this.router.navigate(['vendors/edit/' + this.selectedVendor['VD1_ID']]);
  }

  onEditVendor(rowData) {
    this.router.navigate(['vendors/edit/' + rowData['VD1_ID']]);
  }

  onDeleteVendor(event) {
    if(this.selectedVendor && this.selectedVendor['VD1_ID']){
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.vendorService.delete(this.selectedVendor['VD1_ID']).subscribe( result => {
                this.getVendors();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete vendor error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.vendorService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  backClicked() {
    this.location.back();
  }
}
