import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './vendors.routing';
import { VendorsComponent } from './vendors.component';
import { VendorFormComponent } from './vendor-form/vendor-form.component';
import { VendorService } from '../../services/vendor.service';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  providers: [
      VendorService
  ],
  declarations: [
      VendorsComponent,
      VendorFormComponent
  ]
})
export class VendorsModule { }
