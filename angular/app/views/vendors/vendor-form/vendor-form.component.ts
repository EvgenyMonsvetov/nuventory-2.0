import { Component, ElementRef, OnInit } from '@angular/core';
import { VendorService } from '../../../services/vendor.service';
import { permissions } from '../../../permissions';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { _lang } from '../../../pipes/lang';
import { CompanyFinderModalComponent } from '../../../components/company-finder-modal/company-finder-modal.component';
import { SelectCountriesComponent } from '../../../components/select-countries/select-countries.component';
import { TitleService } from '../../../services/title.service';
import { MasterVendorFinderModalComponent } from '../../../components/master-vendor-finder-modal/master-vendor-finder-modal.component';
import { Location } from '@angular/common';
import { HeaderService } from '../../../services/header.service';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-vendor-add',
  templateUrl: './vendor-form.component.html',
  styleUrls: ['./vendor-form.component.scss'],
  providers: [
    VendorService
  ],
  preserveWhitespaces: true
})
export class VendorFormComponent implements OnInit {
  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private record: any;
  private constants = Constants;

  constructor(private route: ActivatedRoute,
              private vendorService: VendorService,
              public  dialog: MatDialog,
              private elementRef: ElementRef,
              private titleService: TitleService,
              private location: Location,
              private headerService: HeaderService) { }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.record = this.vendorService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id > 0) {
      this.vendorService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
        if (result[0]) {
          let data = result[0];
          data['VD1_ID'] = parseInt(data['VD1_ID']);
          data['VD1_NO_INVENTORY'] = parseInt(data['VD1_NO_INVENTORY']);
          data['VD1_EDI_GL'] = parseInt(data['VD1_EDI_GL']);
          data['VD1_MINIMUM_ORDER_CHECK'] = parseInt(data['VD1_MINIMUM_ORDER']) > 0;
          this.record = data;
          if (this.action === 'clone')
            this.record['VD1_ID'] = 0;
        }
      });
    } else {
      if (this.headerService.CO1_ID > 0) {
        this.record.CO1_ID = this.headerService.CO1_ID;
        this.record.CO1_NAME = this.headerService.CO1_NAME;
      }
    }
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Vendor Form'),
        componentRef: this,
        data: [],
        componentName: 'VendorFormComponent'
      },
    });
  }

  saveClick() {
    if (this.action === 'add' || this.action === 'clone') {
      this.vendorService.create(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.vendorService.update(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  selectCompany() {
    let dialogRef = this.dialog.open(CompanyFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Company'), CO1_ID: this.record.CO1_ID },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if(selected){
        this.record.CO1_NAME = selected.CO1_NAME;
        this.record.CO1_ID   = selected.CO1_ID;
      }
    })
  }

  selectCountry() {
    let dialogRef = this.dialog.open(SelectCountriesComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Country'), CY1_ID: this.record.CY1_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CY1_ID = selected['CY1_ID'];
        this.record.CY1_SHORT_CODE = selected['CY1_SHORT_CODE'];
        this.record.CY1_NAME = selected['CY1_NAME'];
      }
    })
  }

  selectMasterVendor() {
    let dialogRef = this.dialog.open(MasterVendorFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Master Vendor'), VD0_ID: this.record.VD0_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.VD0_ID = selected['VD0_ID'];
        this.record.VD0_SHORT_CODE = selected['VD0_SHORT_CODE'];
        this.record.VD0_NAME = selected['VD0_NAME'];
      }
    })
  }

  clearCompany(){
    this.record.CO1_NAME = '';
    this.record.CO1_ID   = '';
  }

  clearCountry() {
    this.record.CY1_ID = 0;
    this.record.CY1_SHORT_CODE = '';
    this.record.CY1_NAME = '';
  }

  clearMasterVendor() {
    this.record.VD0_ID = 0;
    this.record.VD0_SHORT_CODE = '';
    this.record.VD0_NAME = '';
  }

}
