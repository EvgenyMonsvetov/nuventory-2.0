import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './material-budget-setup.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MaterialBudgetSetupComponent } from './material-budget-setup.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      NgxCurrencyModule
  ],
  providers: [
  ],
  declarations: [
      MaterialBudgetSetupComponent
  ]
})
export class MaterialBudgetSetupModule { }
