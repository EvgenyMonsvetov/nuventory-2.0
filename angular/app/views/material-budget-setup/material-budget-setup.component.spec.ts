import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialBudgetSetupComponent } from './material-budget-setup.component';

describe('MaterialBudgetSetupComponent', () => {
  let component: MaterialBudgetSetupComponent;
  let fixture: ComponentFixture<MaterialBudgetSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialBudgetSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialBudgetSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
