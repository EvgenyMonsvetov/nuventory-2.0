import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { MaterialBudgetSetupComponent } from './material-budget-setup.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: MaterialBudgetSetupComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Material Budget Setup'),
        permissions: {
            only: [permissions.MATERIAL_BUDGET_SETUP_READ]
        }
    }
}];