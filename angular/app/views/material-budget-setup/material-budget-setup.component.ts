import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../services/title.service';
import { Location } from '@angular/common';
import { permissions } from '../../permissions';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs/Rx';
import { HeaderService } from '../../services/header.service';
import { ShopService } from '../../services/shop.service';
import {Constants} from "../../constants";

@Component({
  selector: 'app-material-budget-setup',
  templateUrl: './material-budget-setup.component.html',
  styleUrls: ['./material-budget-setup.component.scss'],
  providers: [ ShopService ],
  preserveWhitespaces: true
})
export class MaterialBudgetSetupComponent implements OnInit, OnDestroy {
  @ViewChild('materialBudgetForm')  materialBudgetForm: ElementRef;

  private errors: Array<any> = [];
  private record: any = [];
  private permissions = permissions;
  private loaderWidth: String = '0px';
  public loading: boolean = true;
  private materialBudgetSubscription: Subscription;
  private headerServiceSubscription: Subscription;
  private constants = Constants;

  constructor(private route: ActivatedRoute,
              private shopService: ShopService,
              private headerService: HeaderService,
              public dialog: MatDialog,
              private titleService: TitleService,
              private toastr: ToastrService,
              private location: Location) {
    this.headerServiceSubscription = this.headerService.subscribe({
      next: (v) => {
        this.getMaterialBudget();
      }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.loaderWidth = this.materialBudgetForm.nativeElement.parentElement.parentElement.offsetWidth + 'px';

    this.getMaterialBudget();
  }

  ngOnDestroy() {
    if (this.materialBudgetSubscription) {
      this.materialBudgetSubscription.unsubscribe();
    }
    if (this.headerServiceSubscription) {
      this.headerServiceSubscription.unsubscribe();
    }
  }

  getMaterialBudget() {
    if (this.materialBudgetSubscription) {
      this.materialBudgetSubscription.unsubscribe();
    }

    this.materialBudgetSubscription = this.shopService.getMaterialBudget().subscribe(res => {
          this.record['MOUNTHLY_GROSS_SALE'] = res['MOUNTHLY_GROSS_SALE'];
          this.record['TARGET_PERCENTAGE'] = res['TARGET_PERCENTAGE'];
          this.calculateFinal();
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On load Material Budget error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Material Budget Setup'),
        componentRef: this,
        data: [],
        componentName: 'MaterialBudgetSetupComponent'
      },
    });
  }

  saveClick() {
    this.loading = true;
    this.shopService.saveMaterialBudget(this.record).subscribe(res => {
          this.loading = false;
          if (res['success']) {
            this.toastr.success(_lang('Operation was successfully executed!'), _lang('Service Success'), {
              timeOut: 3000,
            });
          } else {
            this.toastr.error(_lang('On save Material Budget error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          }
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On save Material Budget error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  calculateFinal() {
    if (this.record['MOUNTHLY_GROSS_SALE'] < 0) {
      this.record['MOUNTHLY_GROSS_SALE'] = 0;
    }
    if (this.record['TARGET_PERCENTAGE'] < 0) {
      this.record['TARGET_PERCENTAGE'] = 0;
    } else if (this.record['TARGET_PERCENTAGE'] > 100) {
      this.record['TARGET_PERCENTAGE'] = 100;
    }
    this.record['FINAL_RESULT'] = this.record['MOUNTHLY_GROSS_SALE'] * this.record['TARGET_PERCENTAGE'] / 100;
  }
}
