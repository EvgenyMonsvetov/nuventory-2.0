import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions  } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { GrossProfitReportComponent } from './gross-profit-report.component';

export const routes: Routes = [{
    path: '',
    component: GrossProfitReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Gross Profit'),
        permissions: {
            only: [permissions.GROSS_PROFIT_READ]
        }
    }
}];