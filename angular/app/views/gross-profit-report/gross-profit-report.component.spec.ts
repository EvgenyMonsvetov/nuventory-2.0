import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrossProfitReportComponent } from './gross-profit-report.component';

describe('GrossProfitReportComponent', () => {
  let component: GrossProfitReportComponent;
  let fixture: ComponentFixture<GrossProfitReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrossProfitReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrossProfitReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
