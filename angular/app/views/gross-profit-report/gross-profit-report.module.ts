import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './gross-profit-report.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';
import { CompaniesCheckboxDropdownModule } from '../../components/companies-checkbox-dropdown/companies-checkbox-dropdown.module';
import { ShopsCheckboxDropdownModule } from '../../components/shops-checkbox-dropdown/shops-checkbox-dropdown.module';
import { GrossProfitReportComponent } from './gross-profit-report.component';
import { RawSalesDataStoreMonthService } from '../../services/raw-sales-data-store-month.service';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        ReportingFilterModule,
        CompaniesCheckboxDropdownModule,
        ShopsCheckboxDropdownModule
    ],
    providers: [
        RawSalesDataStoreMonthService
    ],
    declarations: [
        GrossProfitReportComponent
    ]
})
export class GrossProfitReportModule { }