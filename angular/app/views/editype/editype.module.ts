import { NgModule } from '@angular/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RouterModule } from '@angular/router';
import { routes } from './editype.routing';
import { EditypeComponent } from './editype.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    NgxPermissionsModule.forChild(),
    SharedModule
  ],
  declarations: [
    EditypeComponent
  ]
})
export class EdiTypeModule{ }
