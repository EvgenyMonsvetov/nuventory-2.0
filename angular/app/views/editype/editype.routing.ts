import {Routes } from '@angular/router';

import {permissions } from './../../permissions';
import {NgxPermissionsGuard } from 'ngx-permissions';
import {AuthGuardService as AuthGuard} from './../../auth-guard.service';
import {EditypeComponent } from "./editype.component";

export const routes: Routes = [{
  path: '',
  component: EditypeComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.EDI_TYPE_READ]
    }
  }
}];