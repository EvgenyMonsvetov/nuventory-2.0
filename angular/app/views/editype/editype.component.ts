import { Component, OnInit } from '@angular/core';
import { Page } from '../../model/page';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { permissions } from '../../permissions';
import { PagedData } from '../../model/paged-data';
import { Observable } from 'rxjs';
import { EdiTypeService } from '../../services/editype.service';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-editype',
  templateUrl: './editype.component.html',
  styleUrls: ['./editype.component.scss'],
  providers: [
    EdiTypeService
  ]
})
export class EditypeComponent implements OnInit {

    private loading =false;
    public  permissions = permissions;

    page = new Page();
    rows = new Array<object>();

    private filters: { [key: string]: string; } = {};

    constructor(
        private ediTypeService: EdiTypeService,
        private toastr: ToastrService,
        private titleService: TitleService,
        private route: ActivatedRoute ) {

      this.page.pageNumber = 0;
      this.page.size = 100;
    }

    ngOnInit() {
        this.titleService.setNewTitle(this.route);

        this.loading = true;
        this.setPage({offset: 0});
    }

    getData() {
        this.loading = true;
        this.setPage({offset: this.page.pageNumber});
    }

    setPage(pageInfo) {
        this.page.pageNumber = pageInfo.offset;

        this.serverCall(this.page).subscribe(result => {
            const pagedData = this.getPagedData(this.page, result.count, result.data);
            this.page = pagedData.page;
            this.rows = pagedData.data;
            window.dispatchEvent(new Event('resize'));
            this.loading = false;
        },
        error => {
            console.log(error);
            this.loading = false;
            window.dispatchEvent(new Event('resize'));
            this.toastr.error(_lang('TimeOut occured'), _lang('Service Error'), {
                timeOut: 3000,
            });
        })
    }

    private getPagedData(page: Page,  count: number, data: Array<object>): PagedData<object> {
        const pagedData = new PagedData<object>();
        pagedData.data = data;
        page.totalElements = count;
        page.totalPages = page.totalElements / page.size;
        pagedData.page = page;
        return pagedData;
    }

    serverCall(page: Page): Observable<IResponse<object>> {
        return this.ediTypeService.getItems(page);
    }

    synch(){
        this.loading = true;
        this.ediTypeService._synch().subscribe(result => {
            this.ngOnInit();
        })
    }
}
