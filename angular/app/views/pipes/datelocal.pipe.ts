import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'datelocal'
})

export class DatelocalPipe implements PipeTransform {
  constructor(private datePipe: DatePipe) {}

  transform(value: any, args?: any): any {
    if (!value || value === '')
      return '';

    const regex1 = /\d{2}.\d{2}.\d{4} \d{2}:\d{2}/;
    const regex2 = /\d{4}-\d{2}-\d2} \d{2}:\d{2}/;
    var formattedValue = value;

    if (value.match(regex1)) {
      formattedValue = moment(value, 'MM.DD.YYYY hh:mm', false).format('YYYY/MM/DD hh:mm A');
    } else if (value.match(regex2)) {
      formattedValue = moment(value, 'YYYY-MM-DD hh:mm', false).format('YYYY/MM/DD hh:mm A');
    }
    
    var m = moment.utc(formattedValue);
    return this.datePipe.transform(m.clone().local().format(), args);
  }
}