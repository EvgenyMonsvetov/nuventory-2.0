import { Pipe, PipeTransform } from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
    name: 'customDatePipe'
})

export class CustomDatePipe implements PipeTransform {

    constructor(private datePipe: DatePipe) {

    }

    transform(value: any, args?: any): any {
        return this.datePipe.transform(value.substr(0, 11), args);
    }
}
