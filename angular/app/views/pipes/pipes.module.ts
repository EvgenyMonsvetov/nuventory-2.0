import { NgModule } from '@angular/core';
import { DisplayFieldPipe } from './displayField.pipe';
import { BooleanPipe } from './boolean.pipe';
import { LangPipe } from './lang.pipe';
import { CustomDatePipe } from './customDatePipe';
import { DatePipe } from '@angular/common';
import { DatelocalPipe } from './datelocal.pipe';

@NgModule({
  declarations: [
    DisplayFieldPipe, BooleanPipe, LangPipe, CustomDatePipe, DatelocalPipe
  ],
  exports: [
    DisplayFieldPipe, BooleanPipe, LangPipe, CustomDatePipe, DatelocalPipe
  ],
  providers: [
     DatePipe
  ]
})
export class PipesModule { }
