import { Pipe, PipeTransform } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";

@Pipe({
  name: 'boolean',
  pure: true
})

export class BooleanPipe implements PipeTransform {

  constructor(private _sanitizer: DomSanitizer){}

  transform(value: number): any {
    var result = '';
    if(value){
      result += '<i class="fa fa-check" style="color:green" ></i>';
    }else{
      result += '<i class="fa fa-times" style="color:red"></i>';
    }
    return this._sanitizer.bypassSecurityTrustHtml(result);
  }
}
