import { Pipe, PipeTransform } from '@angular/core';
import { LangService } from '../../services/lang.service';

@Pipe({
  name: 'lang'
})

export class LangPipe implements PipeTransform {
  constructor(private langService: LangService){}

  transform(value: string, data:Array<any> = null): any {
    const currentVocabulary = this.langService.vocabulary;
    var result = '';
    if(currentVocabulary[value]){
      result = currentVocabulary[value];
    }else{
      const defaultVocabulary = this.langService.defaultVocabulary;
      if( defaultVocabulary[value] ){
        result = defaultVocabulary[value];
      }else{
        result = value;
      }
    }

    if(data && data.length){
      data.forEach( (arg,index) => {
        result = result.replace("%%"+(index+1)+"%%", arg);
      });
    }
    return result;
  }
}