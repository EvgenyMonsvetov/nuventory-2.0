import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'displayField'
})

export class DisplayFieldPipe implements PipeTransform {
  transform(value: any, cmpKey: string, dispKey: string, data: Array<object>, notFoundVal: string = ''): any {
    let elements = data.filter( el => {
      return el[cmpKey] == value;
    });
    return elements.length == 1 ? elements[0][dispKey] : notFoundVal;
  }
}
