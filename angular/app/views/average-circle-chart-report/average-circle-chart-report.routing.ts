import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { AverageCircleChartReportComponent } from './average-circle-chart-report.component';

export const routes: Routes = [{
    path: '',
    component: AverageCircleChartReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard]
}];