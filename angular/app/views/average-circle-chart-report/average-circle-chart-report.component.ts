import { OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Component } from '@angular/core';
import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { TitleService } from '../../services/title.service';
import { Page } from '../../model/page';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from '../../services/header.service';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { Subscription } from 'rxjs';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { RawSalesDataStoreMonthService } from '../../services/raw-sales-data-store-month.service';
import { AverageChartModalComponent } from '../../components/average-chart-modal/average-chart-modal.component';

@Component({
  selector: 'app-average-circle-chart-report',
  templateUrl: './average-circle-chart-report.component.html',
  styleUrls: ['./average-circle-chart-report.component.scss'],
  providers: [
    RawSalesDataStoreMonthService
  ],
})
export class AverageCircleChartReportComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('averageCircleChart', { read: ElementRef }) container: ElementRef;
  @ViewChild('averageCircleChartView', { read: ElementRef }) chartView: ElementRef;
  @ViewChild('filters', { read: ElementRef }) filters: ElementRef;
  private chart;
  private showNoData: boolean = false;

  private page = new Page();
  private sub: Subscription;
  private subscriptionSidebarNav: Subscription;
  private headerSubscription: Subscription;
  private selectedCompanies: Array<any> = [];
  private selectedShops: Array<any> = [];
  private disableRunButton: boolean = false;
  private chartLabel: string = '';
  private data: any;

  private graphHeight: string = '400px';
  private slideShow: boolean = false;
  private firstInit: boolean = true;

  constructor(private rawSalesDataStoreMonthService: RawSalesDataStoreMonthService,
              private titleService: TitleService,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private router: Router,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
    this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;

    this.selectedCompanies = [this.headerService.CO1_ID];
    this.selectedShops = [this.headerService.LO1_ID];
    this.page.filterParams['LO1_IDs'] = [this.headerService.LO1_ID];

    const eventUrl: string = this.router.url;
    this.slideShow = eventUrl.includes('/charts-slide-show/');

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
        if (this.firstInit) {
          this.firstInit = false;
          if (this.slideShow) {
            this.getChartData();
          }
        }
      }
    });

    this.headerSubscription = this.headerService.subscribe({
      next: (v) => {
        const CO1_ID = parseInt(this.headerService.CO1_ID);
        const LO1_ID = parseInt(this.headerService.LO1_ID);
        this.selectedCompanies = CO1_ID > 0 ? [CO1_ID] : [];
        this.selectedShops = LO1_ID > 0 ? [LO1_ID] : [];
        this.page.filterParams['LO1_IDs'] = LO1_ID > 0 ? [LO1_ID] : [];
      }
    });
  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['startDate']);
    const toDate = new Date(this.page.filterParams['endDate']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableRunButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableRunButton = false;
    }
  }

  ngOnInit() {
    switch (this.router.url.toString()) {
      case '/report/paint-labor-vs-sales-report':
      case '/charts-slide-show/paint-labor-vs-sales-report':
        this.page.filterParams['attribute_code'] = 'r03_c_paint_labor_vs_sales_p';
        break;
      case '/report/paint-materials-gp-report':
      case '/charts-slide-show/paint-materials-gp-report':
        this.page.filterParams['attribute_code'] = 'r03_c_gp_percent';
        break;
    }

    this.chartLabel = (this.route.snapshot.data.title).toString().replace(' Report', '');
    this.titleService.setNewTitle(this.route);

    setTimeout(() => {
      if (this.slideShow) {
        this.graphHeight = this.getGraphHeight() + 'px';
      }
      setTimeout(() => {
        this.buildHighcharts([]);
      });
    });
  }

  getGraphHeight(): number {
    return (this.chartView.nativeElement.parentElement.parentElement.parentElement.parentElement.offsetHeight - 105 - this.filters.nativeElement.offsetHeight);
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscriptionSidebarNav)
      this.subscriptionSidebarNav.unsubscribe();
    if (this.headerSubscription)
      this.headerSubscription.unsubscribe();
  }

  getChartData() {
    if (this.sub)
      this.sub.unsubscribe();

    this.showNoData = false;
    this.buildHighcharts([]);
    this.chart.showLoading();
    this.data = null;

    this.sub = this.rawSalesDataStoreMonthService.getAverageCircleChartReport(this.page).subscribe(res => {
          this.chart.hideLoading();
          if (res['series']) {
            this.showNoData = false;
            this.data = res['data'];
            this.buildHighcharts(res);
          } else {
            this.showNoData = true;
          }
        },
        error => {
          this.toastr.error(this.chartLabel + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  buildHighcharts(data) {
  ﻿ let colors = ['#e11b1b', '#efefef'];

    if (data.series) {
      data.series.forEach(function(locationData) {
        const locationColor = colors.shift();
        locationData['color'] = locationColor;
        locationData['lineColor'] = locationColor;
        locationData['innerSize'] = '5%';
        colors.push(locationColor);
      });
    }

    this.chart = Highcharts.chart(this.container.nativeElement, {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        height: null
      },
      title: {
        text: this.chartLabel + ' %'
      },
      subtitle: {
        text: data && data.series ? ((data && data.series ? data.series[0]['y'] : '0') + '%') : '',
        y: 25,
        align: 'center',
        verticalAlign: 'middle',
        style: { 'font-size': '16px'}
      },
      tooltip: {
        enabled: false
      },
      plotOptions: {
        pie: {
          allowPointSelect: false,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          }
        },
        series: { states: {
          hover: { enabled: false },
          inactive: { opacity: 1 }
        }}
      },
      // series: data.series
      series: [{
        type: 'pie',
        name: '',
        innerSize: '95%',
        shadow: false,
        data: data.series
      }]
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: this.chartLabel,
        componentRef: this,
        data: [],
        componentName: 'AverageCircleChartReportComponent'
      },
    });
  }

  selectedCompaniesChange(value) {
    this.selectedCompanies = value;
  }

  selectedShopsChange(value) {
    this.page.filterParams['LO1_IDs'] = value;
  }

  showDetails() {
    if (this.data) {
      let dialogRef = this.dialog.open(AverageChartModalComponent, {
        width: window.innerWidth-100+'px',
        height: window.innerHeight-80+'px',
        role: 'dialog',
        data: {title: _lang('Overall Pie Charts'), data: this.data },
      });
    }
  }
}
