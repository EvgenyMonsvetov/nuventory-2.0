import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './manual-inventory-edits-report.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ManualInventoryEditsReportComponent } from './manual-inventory-edits-report.component';
import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';
import { CompaniesCheckboxDropdownModule } from '../../components/companies-checkbox-dropdown/companies-checkbox-dropdown.module';
import { ShopsCheckboxDropdownModule } from '../../components/shops-checkbox-dropdown/shops-checkbox-dropdown.module';
import { InventoryService } from '../../services/inventory.service';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      ReportingFilterModule,
      CompaniesCheckboxDropdownModule,
      ShopsCheckboxDropdownModule
  ],
  providers: [
      InventoryService
  ],
  declarations: [
      ManualInventoryEditsReportComponent
  ]
})
export class ManualInventoryEditsReportModule { }
