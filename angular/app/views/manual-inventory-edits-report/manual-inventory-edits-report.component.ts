import { OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Component } from '@angular/core';
import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { TitleService } from '../../services/title.service';
import { Page } from '../../model/page';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from '../../services/header.service';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { Subscription } from 'rxjs';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { InventoryService } from '../../services/inventory.service';

@Component({
  selector: 'app-manual-inventory-edits-report',
  templateUrl: './manual-inventory-edits-report.component.html',
  styleUrls: ['./manual-inventory-edits-report.component.scss'],
  preserveWhitespaces: true
})
export class ManualInventoryEditsReportComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('manualInventoryEdits', { read: ElementRef }) container: ElementRef;
  @ViewChild('manualInventoryEditsView', { read: ElementRef }) chartView: ElementRef;
  private chart;

  private page = new Page();
  private sub: Subscription;
  private subscriptionSidebarNav: Subscription;
  private subscriptionHeader: Subscription;
  private headerSubscription: Subscription;
  private selectedCompanies: Array<any> = [];
  private selectedShops: Array<any> = [];
  private fontStyle = {
    fontSize: '14px',
    fontWeight: '400',
    fontFamily: 'Roboto'
  };
  private disableRunButton: boolean = false;
  private slideShow: boolean = false;
  private showNoData: boolean = false;

  constructor(private inventoryService: InventoryService,
              private titleService: TitleService,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private router: Router,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.selectedCompanies = [this.headerService.CO1_ID];
    this.selectedShops = [this.headerService.LO1_ID];
    this.page.filterParams['LO1_IDs'] = this.headerService.LO1_ID > 0 ? [this.headerService.LO1_ID] : [];
    this.page.filterParams['CO1_IDs'] = this.headerService.CO1_ID > 0 ? [this.headerService.CO1_ID] : [];
    this.page.filterParams['startDate'] = '';
    this.page.filterParams['endDate'] = '';
    this.page.filterParams['level'] = 1;
    this.page.filterParams['shopLevelId'] = 0;

    const eventUrl: string = this.router.url;
    this.slideShow = eventUrl.includes('/charts-slide-show/');

    this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
    this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;

    this.headerSubscription = this.headerService.subscribe({
      next: (v) => {
        const CO1_ID = parseInt(this.headerService.CO1_ID);
        const LO1_ID = parseInt(this.headerService.LO1_ID);
        this.selectedCompanies = CO1_ID > 0 ? [CO1_ID] : [];
        this.selectedShops = LO1_ID > 0 ? [LO1_ID] : [];
        this.page.filterParams['LO1_IDs'] = LO1_ID > 0 ? [LO1_ID] : [];
        this.page.filterParams['CO1_IDs'] = CO1_ID > 0 ? [CO1_ID] : [];
      }
    });

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
      }
    });
  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['startDate']);
    const toDate = new Date(this.page.filterParams['endDate']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableRunButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableRunButton = false;
    }
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    setTimeout(() => {
      this.buildHighcharts([], []);
    });
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscriptionHeader)
      this.subscriptionHeader.unsubscribe();
    if (this.headerSubscription)
      this.headerSubscription.unsubscribe();
    if (this.subscriptionSidebarNav)
      this.subscriptionSidebarNav.unsubscribe();
  }

  getChartData() {
    if (this.sub)
      this.sub.unsubscribe();

    this.chart.showLoading();
    this.page.filterParams['shopLevelId'] = 0;
    this.page.filterParams['level'] = 1;

    this.sub = this.inventoryService.getManualInventoryEdits(this.page).subscribe(res => {
          this.chart.hideLoading();
          if (res['series'] && res['series'].length) {
            this.showNoData = false;
            this.buildHighcharts(res['series'], res['drilldown']);
          } else {
            this.showNoData = true;
            this.buildHighcharts([], []);
          }
        },
        error => {
          this.toastr.error(_lang('Manual Inventory Edits') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  buildHighcharts(series, drilldown) {
    Highcharts.setOptions({
      lang: {
        drillUpText: _lang('Previous')
      }
    });

    let me = this;

    this.chart = Highcharts.chart(this.container.nativeElement, {
      chart: {
        type: 'column',
        events: {
          drilldown: function (event) {
            me.onDrillDownClick(event, 1);
          },
          drillup: function (event) {
            me.onDrillDownClick(event, -1);
          }
        }
      },
      xAxis: {
        type: 'category',
        labels: {
          style: this.fontStyle
        }
      },
      yAxis: {
        title: {
          text: 'Edits',
          style: this.fontStyle
        },
        labels: {
          style: this.fontStyle
        }
      },
      title: {
        text: _lang('Manual Inventory Edits')
      },
      subtitle: {
        text: this.showNoData ? _lang('There is no data') : ''
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            format: '{point.y:.0f}',
            style: this.fontStyle
          }
        },
        column: {
          colorByPoint: true
        }
      },
      tooltip: {
        formatter: function() {
          return 'Edits: <b>' + (this.y).toFixed(0) + '</b>' + ' Total Cost: ' + '<b>$' + (this.point['cost']).toFixed(2) + '</b>';
        }
      },
      series: [{
        name: '',
        data: series,
        type: 'column'
      }],
      drilldown: {
        series: drilldown
      }
    });
  }

  onDrillDownClick(event, value: number) {
    if (value === 1) {
      this.page.filterParams['shopLevelId'] = event.seriesOptions.id;
    } else {
      this.page.filterParams['shopLevelId'] = 0;
    }
    this.page.filterParams['level'] += value;
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Manual Inventory Edits'),
        componentRef: this,
        data: [],
        componentName: 'ManualInventoryEditsReportComponent'
      },
    });
  }

  selectedCompaniesChange(value) {
    this.selectedCompanies = value;
    this.page.filterParams['CO1_IDs'] = value;
  }

  selectedShopsChange(value) {
    this.page.filterParams['LO1_IDs'] = value;
  }

  exportClick() {
    this.chart.showLoading();
    this.inventoryService.downloadXlsx(this.page, 'manualInventoryEdits').subscribe( result => {
          this.chart.hideLoading();
      },
      error => {
        this.chart.hideLoading();
        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
  }
}
