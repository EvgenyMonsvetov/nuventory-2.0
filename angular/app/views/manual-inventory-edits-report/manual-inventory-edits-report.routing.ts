import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from '../../permissions';
import { AuthGuardService as AuthGuard } from '../../auth-guard.service';
import { ManualInventoryEditsReportComponent } from './manual-inventory-edits-report.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: ManualInventoryEditsReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Manual Inventory Edits'),
        permissions: {
            only: [permissions.MANUAL_INVENTORY_EDITS_READ]
        }
    }
}];