import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions  } from './../../permissions';
import { ShopsComponent } from './shops.component';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { ShopFormComponent } from './shop-form/shop-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: ShopsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Shops#'),
        permissions: {
            only: [permissions.SHOPS_READ]
        }
    }
},{
    path: 'add',
    component: ShopFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Add'),
        permissions: {
            only: [permissions.SHOPS_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: ShopFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Edit#'),
        permissions: {
            only: [permissions.SHOPS_EDIT]
        }
    }
},{
    path: 'copy/:id',
    component: ShopFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Clone'),
        permissions: {
            only: [permissions.SHOPS_ADD]
        }
    }
}];