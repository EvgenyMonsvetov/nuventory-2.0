import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { permissions } from '../../../permissions';
import { CompanyFinderModalComponent } from '../../../components/company-finder-modal/company-finder-modal.component';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { InventoryFinderModalComponent } from '../../../components/inventory-finder-modal/inventory-finder-modal.component';
import { ShopService } from '../../../services/shop.service';
import { _lang } from '../../../pipes/lang';
import { HeaderService } from '../../../services/header.service';
import { SelectCountriesComponent } from '../../../components/select-countries/select-countries.component';
import { TimezonesService } from '../../../services/timezones.service';
import * as _ from 'lodash';
import {Constants} from "../../../constants";


@Component({
  selector: 'app-form',
  templateUrl: './shop-form.component.html',
  styleUrls: ['./shop-form.component.scss'],
  providers: [
    ShopService
  ],
  preserveWhitespaces: true
})
export class ShopFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';
  private regiones: Array<any> = [];
  private timezones: Array<any> = [];
  private selectedRegion: any;
  private permissions = permissions;
  private record: any;
  private constants = Constants;

  constructor(
      private route: ActivatedRoute,
      private shopService: ShopService,
      public  dialog: MatDialog,
      private elementRef: ElementRef,
      private headerService: HeaderService,
      private timezonesService: TimezonesService
  ) {
  }

  ngOnInit() {
    this.record = this.shopService.initialize();
    this.action = this.route.snapshot.url[0].toString();
    this.regiones = this.timezonesService.getStoreTimezones();
    
    if (this.action === 'add') {
      this.shopService.generateShopShortCode().subscribe( data => {
        this.record['LO1_SHORT_CODE'] = data['LO1_SHORT_CODE'];
      });
      if (this.headerService.CO1_ID > 0) {
        this.record.CO1_ID = this.headerService.CO1_ID;
        this.record.CO1_NAME = this.headerService.CO1_NAME;
      }
    } else {
      this.shopService.getById(this.route.snapshot.params.id || 0).subscribe( data => {
        data['CA1_NO_INVENTORY'] = parseInt(data['CA1_NO_INVENTORY']);
        data['LO1_CENTER_FLAG']  = parseInt(data['LO1_CENTER_FLAG']);
        data['LO1_ORDER_DAYS_PER_MOUNTH']  = parseInt(data['LO1_ORDER_DAYS_PER_MOUNTH']);
        this.record = data;
        if (this.regiones) {
          this.setTimeZone(data.lO1_TIMEZONE);
        }
      });
    }
  }

  cancelClick() {
    window.history.back();
  }

  openLanguageEditor() {
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('Shop Form'),
            componentRef: this,
            data: [],
            componentName: 'ShopFormComponent'
          },
      });
  }

  saveClick() {
    var record = _.clone(this.record);
    record.LO1_CENTER_FLAG = record.LO1_CENTER_FLAG ? 1 : 0;
    if (this.action === 'add' || this.action === 'copy') {
      this.shopService.create(record).subscribe(result => {
        if (result.success) {
          this.headerService.next();
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.shopService.update(record).subscribe(result => {
        if (result.success) {
          this.headerService.next();
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  selectCompany() {
    let dialogRef = this.dialog.open(CompanyFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Company'), CO1_ID: this.record.CO1_ID },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if(selected){
        this.record.CO1_NAME = selected.CO1_NAME;
        this.record.CO1_ID   = selected.CO1_ID;
      }
    })
  }

  clearCompany() {
    this.record.CO1_NAME = '';
    this.record.CO1_ID   = '';
  }

  selectMasterCategory() {
    let dialogRef = this.dialog.open(InventoryFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-100+'px',
      role: 'dialog',
      data: {title: _lang('Inventory Nuventory Category List'), componentRef: this.elementRef, data: []},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.C00_NAME = selected.C00_NAME;
        this.record.C00_ID   = selected.C00_ID;
      }
    })
  }

  clearCategory() {
    this.record.C00_NAME = '';
    this.record.C00_ID   = '';
  }

  selectCountry() {
    let dialogRef = this.dialog.open(SelectCountriesComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Country'), CY1_ID: this.record.CY1_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CY1_ID = selected['CY1_ID'];
        this.record.CY1_SHORT_CODE = selected['CY1_SHORT_CODE'];
        this.record.CY1_NAME = selected['CY1_NAME'];
      }
    })
  }

  clearCountry() {
    this.record.CY1_ID = 0;
    this.record.CY1_SHORT_CODE = '';
    this.record.CY1_NAME = '';
  }

  setTimeZone(zoneId) {
    for (var i=0; i<this.regiones.length; i++) {
      let reg = this.regiones[i];
      let zone = reg.list.find(z => z.Time_zone_id === zoneId);
      if(zone) {
        this.selectedRegion = reg;
        break;
      }
    }

    this.onRegionChange();
  }

  onRegionChange() {
    this.timezones = this.selectedRegion ? this.selectedRegion.list : [];
  }
}
