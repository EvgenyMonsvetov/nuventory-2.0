import { NgModule  } from '@angular/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RouterModule  } from '@angular/router';
import { CompanyService } from '../../services/company.service';
import { ShopsComponent } from './shops.component';
import { routes } from './shops.routing';
import { ShopFormComponent } from './shop-form/shop-form.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  providers: [
      CompanyService
  ],
  declarations: [
      ShopsComponent,
      ShopFormComponent
  ]
})
export class ShopsModule { }
