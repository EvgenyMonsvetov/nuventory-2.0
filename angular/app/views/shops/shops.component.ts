import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ShopService } from '../../services/shop.service';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { HeaderService } from '../../services/header.service';
import { Subscription ,  Subject } from 'rxjs';
import { _lang } from '../../pipes/lang';
import { takeUntil } from 'rxjs/internal/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.scss'],
  providers: [
    ShopService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class ShopsComponent implements OnInit, OnDestroy {

  private permissions = permissions;

  @ViewChild('shopView')     shopView: ElementRef;
  @ViewChild('actionbar')    actionbar: ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('tableHeader')  tableHeader: ElementRef;
  @ViewChild('tableFilter')  tableFilter: ElementRef;
  @ViewChild('paginator')    paginator: ElementRef;

  private scrollHeight: string = '50px';

    /** control for the selected filters */
    public companyCtrl: FormControl = new FormControl();
    public companyFilterCtrl: FormControl = new FormControl();
    public locationByCtrl: FormControl = new FormControl();
    public locationByFilterCtrl: FormControl = new FormControl();
    public countryCtrl: FormControl = new FormControl();
    public countryFilterCtrl: FormControl = new FormControl();
    /** Subject that emits when the component has been destroyed. */
    private _onDestroyCompany = new Subject<void>();
    private _onDestroyLocationBy = new Subject<void>();
    private _onDestroyCountry = new Subject<void>();
    /** list of companies filtered by search keyword */
    public filteredCompanies: any[] = [];
    public filteredShopsBy: any[] = [];
    public filteredCountries: any[] = [];
    /** filter object with selected filters properties */
    private filter: any = {};

    private flagsDP: any[] = [];
    private cols: any[] = [];
    private items: any[] = [];
    private companies: Array<object> = [];
    private shops: Array<object> = [];
    private countries: any[] = [];

    private loading = false;
    private page = new Page();
    private selected: any = null;
    private size: number = 50;
    private totalRecords: number = 0;
    private headerSubscription: Subscription;
    private shopsSubscription: Subscription;

    constructor(private toastr: ToastrService,
                private shopService: ShopService,
                public  dialog: MatDialog,
                private router: Router,
                private confirmationService: ConfirmationService,
                private titleService: Title,
                private headerService: HeaderService) {
      this.page.size = this.size;
      this.page.pageNumber = 0;

      this.headerSubscription = this.headerService.subscribe({
          next: (v) => {
            this.page.pageNumber = 0;
            this.page.filterParams = {};
            this.loading = true;
            this.getItems();
          }
      });
    }

    ngOnDestroy() {
      if (this.headerSubscription)
        this.headerSubscription.unsubscribe();
      if (this.shopsSubscription)
        this.shopsSubscription.unsubscribe();
      this._onDestroyCompany.next();
      this._onDestroyCompany.complete();
      this._onDestroyLocationBy.next();
      this._onDestroyLocationBy.complete();
      this._onDestroyCountry.next();
      this._onDestroyCountry.complete();
    }

    ngOnInit() {
      this.titleService.setTitle('Shops');

      this.cols = [
        { field: 'CO1_ID',               header: _lang('CO1_SHORT_CODE'),      visible: false, export: {visible: true, checked: false}, width: 100 },
        { field: 'CO1_NAME',             header: _lang('CO1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 160 },
        { field: 'LO1_SHORT_CODE',       header: _lang('LO1_SHORT_CODE'),      visible: true, export: {visible: true, checked: true}, width: 80 },
        { field: 'LO1_NAME',             header: _lang('LO1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 160 },
        { field: 'LO1_CCCONE_NAME',      header: _lang('LO1_CCCONE_NAME'),     visible: true, export: {visible: true, checked: true}, width: 85 },
        { field: 'LO1_CCC_SHOP_ID',      header: _lang('LO1_CCC_SHOP_ID'),     visible: true, export: {visible: true, checked: true}, width: 100 },
        { field: 'LO1_CENTER_FLAG',      header: _lang('LO1_CENTER_FLAG'),     visible: true, export: {visible: true, checked: true}, width: 80},
        { field: 'LO1_ADDRESS1',         header: _lang('#AddressStreet1#'),    visible: true, export: {visible: true, checked: true}, width: 150 },
        { field: 'LO1_ADDRESS2',         header: _lang('#AddressStreet2#'),    visible: true, export: {visible: true, checked: true}, width: 120 },
        { field: 'LO1_CITY',             header: _lang('#AddressCity#'),       visible: true, export: {visible: true, checked: true}, width: 120 },
        { field: 'LO1_STATE',            header: _lang('#AddressState#'),      visible: true, export: {visible: true, checked: true}, width: 45 },
        { field: 'CY1_SHORT_CODE',       header: _lang('CY1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 65 },
        { field: 'LO1_ZIP',              header: _lang('#AddressPostalCode#'), visible: true, export: {visible: true, checked: true}, width: 80 },
        { field: 'LO1_PHONE',            header: _lang('#Phone#'),             visible: true, export: {visible: true, checked: true}, width: 110 },
        { field: 'LO1_FAX',              header: _lang('#Fax#'),               visible: true, export: {visible: true, checked: true}, width: 110 }
    ];

      this.flagsDP = [
          { label: _lang('#AllRecords#'),   value: ''},
          { label: _lang('Yes'),            value: '1'},
          { label: _lang('No'),             value: '0'}
      ];

      this.shopService.preloadData().subscribe( data => {
          this.prepareData(data);
          this.getItems();
      });

      this.companyFilterCtrl.valueChanges
          .pipe(takeUntil(this._onDestroyCompany))
          .subscribe(() => {
              this.filterCompanies();
          });
      this.locationByFilterCtrl.valueChanges
          .pipe(takeUntil(this._onDestroyLocationBy))
          .subscribe(() => {
              this.filterShopsBy();
          });
        this.countryFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroyCountry))
            .subscribe(() => {
                this.filterCountries();
            });
    }

    private filterCompanies() {
        if (!this.companies) {
            return;
        }
        // get the search keyword
        let search = this.companyFilterCtrl.value;
        if (!search) {
            this.filteredCompanies = this.companies;
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the companies
        this.filteredCompanies = this.companies.filter(company => company['CO1_NAME'].toLowerCase().indexOf(search) > -1);
    }

    private filterShopsBy() {
        if (!this.shops) {
            return;
        }
        // get the search keyword
        let search = this.locationByFilterCtrl.value;
        if (!search) {
            this.filteredShopsBy = this.shops;
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the location by
        this.filteredShopsBy = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
    }

    private filterCountries() {
        if (!this.countries) {
            return;
        }
        // get the search keyword
        let search = this.countryFilterCtrl.value;
        if (!search) {
            this.filteredCountries = this.countries;
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the countries
        this.filteredCountries = this.countries.filter(country => country['CY1_SHORT_CODE'].toLowerCase().indexOf(search) > -1);
    }

    getItems() {
        this.loading = true;

        if (this.shopsSubscription)
            this.shopsSubscription.unsubscribe();

        this.shopsSubscription = this.shopService.getItems(this.page).subscribe(res => {
            this.items = res.data;
            this.totalRecords = res.count;

            setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

            this.loading = false;
        },
        error => {
            this.loading = false;
            this.toastr.error(_lang('Shops') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                timeOut: 3000,
            });
        });
    }

    prepareData(data) {
        this.companies = [{CO1_ID: null, CO1_NAME: _lang('#AllRecords#')}];
        let companiesArr = data.companies.map( company => {
            return {
                CO1_ID:   company.CO1_ID,
                CO1_NAME: company.CO1_NAME
            }
        }).filter( company => {
            return company.CO1_NAME && company.CO1_NAME.length > 0;
        }).sort((a, b) => {
            if (a.CO1_NAME < b.CO1_NAME)
                return -1;
            if (a.CO1_NAME > b.CO1_NAME)
                return 1;
            return 0;
        });
        this.companies = this.companies.concat(companiesArr);
        this.filterCompanies();

        this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
        let shopsArr = data.shops.map( shop => {
            return {
                LO1_ID:   shop.LO1_ID,
                LO1_NAME: shop.LO1_NAME
            }
        }).filter( shop => {
            return shop.LO1_NAME && shop.LO1_NAME.length > 0;
        }).sort((a, b) => {
            if (a.LO1_NAME < b.LO1_NAME)
                return -1;
            if (a.LO1_NAME > b.LO1_NAME)
                return 1;
            return 0;
        });
        this.shops = this.shops.concat(shopsArr);
        this.filterShopsBy();

        this.countries = [{CY1_ID: null, CY1_SHORT_CODE: _lang('#AllRecords#')}];
        let countriesArr = data.countries.map( country => {
            return {
                CY1_ID:   country.CY1_ID,
                CY1_SHORT_CODE: country.CY1_SHORT_CODE
            }
        }).filter( country => {
            return country.CY1_SHORT_CODE && country.CY1_SHORT_CODE.length > 0;
        }).sort((a, b) => {
            if (a.CY1_SHORT_CODE < b.CY1_SHORT_CODE)
                return -1;
            if (a.CY1_SHORT_CODE > b.CY1_SHORT_CODE)
                return 1;
            return 0;
        });
        this.countries = this.countries.concat(countriesArr);
        this.filterCountries();
    }

    getScrollHeight(): number {
        return (this.shopView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
            (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
            this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
    }

    onPage(event) {
        this.loading = true;
        this.page.size = event.rows;
        this.page.pageNumber = (event.first / event.rows);
        this.getItems();
    }

    buttonPress(e, col, value) {
        if ( e.keyCode === 13 ) {
            this.filterChanged(col, value);
        }
    }

    filterChanged(col, value) {
        this.loading = true;
        this.page.filterParams[col] = value;
        this.page.pageNumber = 0;
        this.getItems();
    }

    openLanguageEditor() {
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: window.innerWidth-60+'px',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: _lang('Shop List'),
                componentRef: this,
                data: [],
                componentName: 'ShopsComponent'
            },
        });
    }

    addClick() {
        this.router.navigate(['shops/add']);
    }

    cloneRecord() {
      if (this.selected) {
          this.router.navigate(['shops/copy/' + this.selected.LO1_ID]);
      }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.shopService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord(rowData?) {
      if (this.selected || rowData) {
          const LO1_ID = (rowData && rowData.LO1_ID) ? rowData.LO1_ID : this.selected.LO1_ID;
          this.router.navigate(['shops/edit/' + LO1_ID]);
      }
    }

    deleteRecord() {
      if (this.selected) {
          this.confirmationService.confirm({
              message: _lang('Are you sure that you want to perform this action?'),
              accept: () => {
                  this.loading = true;
                  this.shopService.delete(this.selected.LO1_ID).subscribe(result => {
                      this.headerService.next();
                      this.items = this.items.filter(item => {
                          return item.LO1_ID == this.selected.LO1_ID ? false : true;
                      });
                      this.loading = false;
                  });
              }
          });
      }
    }
}
