import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { CategoriesComponent } from './categories.component';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { CountryFormComponent } from './country-form/country-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: CategoriesComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: 'Company Categories',
        permissions: {
            only: [permissions.CATEGORIES_READ]
        }
    }
},{
    path: 'add',
    component: CountryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: 'Add',
        permissions: {
            only: [permissions.CATEGORIES_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: CountryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: '#Edit#',
        permissions: {
            only: [permissions.CATEGORIES_EDIT]
        }
    }
},{
    path: 'copy/:id',
    component: CountryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Clone'),
        permissions: {
            only: [permissions.CATEGORIES_ADD]
        }
    }
}];