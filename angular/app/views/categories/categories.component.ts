import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { InventoryFinderModalComponent } from '../../components/inventory-finder-modal/inventory-finder-modal.component';
import { CategoryService } from '../../services/category.service';
import { TitleService } from 'app/services/title.service';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { HeaderService } from '../../services/header.service';
import { Subscription } from 'rxjs';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [
      CategoryService,
      ConfirmationService
  ],
  preserveWhitespaces: true
})
export class CategoriesComponent implements OnInit, OnDestroy {

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = true;
  private page = new Page();
  private selected: any = null;
  private totalRecords: number = 0;
  private size: number = 50;
  private subscription: Subscription;

  constructor(
    private toastr: ToastrService,
    private categoryService: CategoryService,
    public  dialog: MatDialog,
    private router: Router,
    private elementRef: ElementRef,
    private confirmationService: ConfirmationService,
    private titleService: TitleService,
    private route: ActivatedRoute,
    private headerService: HeaderService
  ) {
      this.page.size = this.size;
      this.page.pageNumber = 0;

      this.subscription = this.headerService.subscribe({
          next: (v) => {
            this.setPage({offset: 0});
          }
      });
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

  ngOnInit() {
      this.titleService.setNewTitle(this.route);

      this.cols = [
        { field: 'CO1_NAME',             header: _lang('CO1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 180 },
        { field: 'CA1_NAME',             header: _lang('CA1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 120 },
        { field: 'CA1_DESCRIPTION',      header: _lang('CA1_DESCRIPTION'),     visible: true, export: {visible: true, checked: true}, width: 180  },
        { field: 'CA1_NO_INVENTORY',     header: _lang('CA1_NO_INVENTORY'),    visible: true, export: {visible: true, checked: true}, width: 120  },
        { field: 'C00_NAME',             header: _lang('Nuventory Category'),  visible: true, export: {visible: true, checked: true}, width: 180 }
      ];
  }

  loadCarsLazy(event: LazyLoadEvent) {
      var offset = (event.first < this.size ? 1 : event.first / this.size) - 1;
      this.setPage({
          offset: offset
      });

        //in a real application, make a remote request to load data using state metadata from event
        //event.first = First row offset
        //event.rows = Number of rows per page
        //event.sortField = Field name to sort with
        //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
  }

  setPage(pageInfo) {
      this.loading = true;
      this.page.pageNumber = pageInfo.offset;

      this.categoryService.getItems(this.page).subscribe(result => {

          this.page.totalElements = result.count;
          this.page.totalPages    = result.count / this.page.size;
          this.items = result.data;
          this.totalRecords = result.count;
          this.loading = false;
      },
      error => {
          this.loading = false;
          this.toastr.error(_lang('Categories') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
          });
      });
    }

    openLanguageEditor() {
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: window.innerWidth-60+'px',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: _lang('Customer Data List'),
                componentRef: this,
                data: [],
                componentName: 'CategoriesComponent'
            },
        });
    }

    assignCategory() {
      let dialogRef = this.dialog.open(InventoryFinderModalComponent, {
            width: window.innerWidth-100+'px',
            height: window.innerHeight-100+'px',
            role: 'dialog',
            data: {title: _lang('Category List'), componentRef: this.elementRef, data: []},
        });
    }

    addClick() {
        this.router.navigate(['categories/add']);
    }

    cloneRecord() {
      if(this.selected){
          this.router.navigate(['categories/copy/' + this.selected.CA1_ID]);
      }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.categoryService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord(rowData?) {
      if(this.selected || rowData) {
          const CA1_ID = (rowData && rowData.CA1_ID) ? rowData.CA1_ID : this.selected[0].CA1_ID;
          this.router.navigate(['categories/edit/' + CA1_ID]);
      }
    }

    deleteRecord() {
      if (this.selected && this.selected[0] && this.selected[0]['CA1_ID']) {
          this.confirmationService.confirm({
              message: _lang('Are you sure that you want to perform this action?'),
              accept: () => {
                  this.loading = true;
                  var CA1_IDs = this.selected.map( item => item['CA1_ID'] );
                  this.categoryService.deleteMultiple(CA1_IDs).subscribe(result => {
                      this.setPage({
                          offset: this.page.pageNumber
                      });
                  },
                  error => {
                      this.loading = false;
                      this.toastr.error(_lang('On delete category error occured'), _lang('Service Error'), {
                          timeOut: 3000,
                      });
                  });
              }
          });
      }
    }
}
