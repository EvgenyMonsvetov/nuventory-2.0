import { NgModule} from '@angular/core';
import { NgxPermissionsModule} from 'ngx-permissions';
import { RouterModule } from '@angular/router';

import { CategoriesComponent } from './categories.component';
import { routes } from './categories.routing';
import { CountryFormComponent } from './country-form/country-form.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  providers: [ ],
  declarations: [ CategoriesComponent, CountryFormComponent ]
})
export class CategoriesModule { }
