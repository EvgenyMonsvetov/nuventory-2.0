import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { permissions } from '../../../permissions';
import { CompanyFinderModalComponent } from '../../../components/company-finder-modal/company-finder-modal.component';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { CategoryService } from '../../../services/category.service';
import { InventoryFinderModalComponent } from '../../../components/inventory-finder-modal/inventory-finder-modal.component';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../../pipes/lang';
import { HeaderService } from '../../../services/header.service';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.scss'],
  providers: [
    CategoryService
  ],
  preserveWhitespaces: true
})
export class CountryFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';
  private constants = Constants;
  private permissions = permissions;
  private record: any;
  constructor(
      private route: ActivatedRoute,
      private categoryService: CategoryService,
      public dialog: MatDialog,
      private titleService: TitleService,
      private headerService: HeaderService
  ) {
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.record = this.categoryService.initialize();
    this.record.CO1_ID = this.headerService.CO1_ID;
    this.record.CO1_NAME = this.headerService.CO1_NAME;

    this.action = this.route.snapshot.url[0].toString();
    if (this.route.snapshot.params.id) {
      this.categoryService.getById(this.route.snapshot.params.id).subscribe( data => {
        data['CA1_NO_INVENTORY'] = parseInt(data['CA1_NO_INVENTORY']);
        this.record = data;
      });
    }
  }

  cancelClick() {
    window.history.back();
  }

  openLanguageEditor() {
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('Gl Code Form'),
            componentRef: this,
            data: [],
            componentName: 'CountryFormComponent'
          },
      });
  }

  saveClick() {
    if (this.action == 'add' || this.action == 'copy') {
      this.categoryService.create(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.categoryService.update(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  selectCompany() {
    let dialogRef = this.dialog.open(CompanyFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Company'), CO1_ID: this.record.CO1_ID},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CO1_NAME = selected.CO1_NAME;
        this.record.CO1_ID   = selected.CO1_ID;
      }
    })
  }

  clearCompany() {
    this.record.CO1_NAME = '';
    this.record.CO1_ID   = '';
  }

  selectMasterCategory() {
    let dialogRef = this.dialog.open(InventoryFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-100+'px',
      role: 'dialog',
      data: {title: _lang('Inventory Nuventory Category List'), C00_ID: this.record.C00_ID},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.C00_NAME = selected.C00_NAME;
        this.record.C00_ID   = selected.C00_ID;
      }
    })
  }

  clearCategory() {
    this.record.C00_NAME = '';
    this.record.C00_ID   = '';
  }
}
