import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { permissions } from '../../../permissions';
import { CompanyFinderModalComponent } from '../../../components/company-finder-modal/company-finder-modal.component';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { CustomerService } from '../../../services/customer.service';
import { SelectCountriesComponent } from '../../../components/select-countries/select-countries.component';
import { _lang } from '../../../pipes/lang';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss'],
  providers: [
    CustomerService
  ],
  preserveWhitespaces: true
})
export class CustomerFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';
  private constants = Constants;
  private permissions = permissions;
  private record: any;
  constructor(
      private route: ActivatedRoute,
      private customerService: CustomerService,
      public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.record = this.customerService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    this.customerService.getById(this.route.snapshot.params.id || 0).subscribe( data => {
        this.record = data;
    });
  }

  cancelClick() {
    window.history.back();
  }

  openLanguageEditor() {
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('Customers Form'),
            componentRef: this,
            data: [],
            componentName: 'CustomerFormComponent'
          },
      });
  }

  saveClick() {
    if (this.action === 'add' || this.action === 'clone') {
      this.customerService.create(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.customerService.update(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  selectCompany() {

    let dialogRef = this.dialog.open(CompanyFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Company'), CO1_ID: this.record.CO1_ID },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CO1_NAME = selected.CO1_NAME;
        this.record.CO1_ID   = selected.CO1_ID;
      }
    })
  }

  clearCompany(){
    this.record.CO1_NAME = '';
    this.record.CO1_ID   = '';
  }

  selectCountry() {
    let dialogRef = this.dialog.open(SelectCountriesComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Country'), CY1_ID: this.record.CY1_ID },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CY1_ID = selected['CY1_ID'];
        this.record.CY1_SHORT_CODE = selected['CY1_SHORT_CODE'];
        this.record.CY1_NAME = selected['CY1_NAME'];
      }
    })
  }

  clearCountry() {
    this.record.CY1_ID = 0;
    this.record.CY1_SHORT_CODE = '';
    this.record.CY1_NAME = '';
  }
}
