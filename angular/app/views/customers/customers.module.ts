import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPermissionsModule } from 'ngx-permissions';

import { CustomersComponent } from './customers.component';
import { routes } from './customers.routing';
import { CustomerAccessComponent } from './customer-access/customer-access.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  declarations: [
    CustomersComponent,
    CustomerAccessComponent,
    CustomerFormComponent ]
})
export class CustomersModule { }
