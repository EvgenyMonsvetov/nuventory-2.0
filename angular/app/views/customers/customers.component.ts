import { Component, OnDestroy, OnInit } from '@angular/core';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { permissions } from '../../permissions';
import { CustomerService } from '../../services/customer.service';
import { Title } from '@angular/platform-browser';
import { ConfirmationService } from 'primeng/api';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { Subscription } from 'rxjs';
import { HeaderService } from '../../services/header.service';
import { _lang } from '../../pipes/lang';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';

@Component({
    templateUrl: 'customers.component.html',
    providers: [
        CustomerService,
        ConfirmationService
    ],
    preserveWhitespaces: true
})

export class CustomersComponent implements OnInit, OnDestroy {

    private loading = false;
    private page = new Page();
    cols: any[] = [];
    items: any[] = [];
    private selected: any = null;
    private permissions = permissions;
    private totalRecords: number = 0;
    private headerSubscription: Subscription;
    private getItemsSubscription: Subscription;

    constructor(
        private customerService: CustomerService,
        private toastr: ToastrService,
        private router: Router,
        private titleService: Title,
        public  dialog: MatDialog,
        private confirmationService: ConfirmationService,
        private headerService: HeaderService
    ) {
        this.page.pageNumber = 0;
        this.page.size = 50;

        this.headerSubscription = this.headerService.subscribe({
            next: (v) => {
                this.setPage({offset: 0});
            }
        });
    }

    ngOnDestroy() {
        if (this.headerSubscription)
            this.headerSubscription.unsubscribe();
        if (this.getItemsSubscription)
            this.getItemsSubscription.unsubscribe();
    }

    ngOnInit() {
        this.titleService.setTitle('CustomersListTitle');
        this.cols = [
            { field: 'CO1_NAME',       header: _lang('CO1_NAME'),           visible: true, export: {visible: true, checked: true},  width: 150 },
            { field: 'CU1_NAME',       header: _lang('CU1_NAME'),           visible: true, export: {visible: true, checked: true},  width: 150 },
            { field: 'CU1_ADDRESS1',   header: _lang('#AddressStreet1#'),   visible: true, export: {visible: true, checked: true},  width: 100 },
            { field: 'CU1_ADDRESS2',   header: _lang('#AddressStreet2#'),   visible: true, export: {visible: true, checked: true},  width: 100 },
            { field: 'CU1_CITY',       header: _lang('#AddressCity#'),      visible: true, export: {visible: true, checked: true},  width: 100 },
            { field: 'CU1_STATE',      header: _lang('#AddressState#'),     visible: true, export: {visible: true, checked: true},  width: 100},
            { field: 'CU1_ZIP',        header: _lang('#AddressPostalCode#'),visible: true, export: {visible: true, checked: true},  width: 150 },
            { field: 'CY1_NAME',       header: _lang('#Countries#'),        visible: true, export: {visible: true, checked: true},  width: 100 },
            { field: 'CU1_PHONE',      header: _lang('#Phone#'),            visible: true, export: {visible: true, checked: true},  width: 120 },
            { field: 'CU1_FAX',        header: _lang('#Fax#'),              visible: true, export: {visible: true, checked: true},  width: 100 },
            { field: 'CU1_EMAIL',      header: _lang('#EMAIL#'),            visible: true, export: {visible: true, checked: true},  width: 100 }
        ];

        this.setPage({offset: 0});
    }

    setPage(pageInfo) {
        if (this.getItemsSubscription)
            this.getItemsSubscription.unsubscribe();

        this.loading = true;
        this.page.pageNumber = pageInfo.offset;
        this.getItemsSubscription = this.customerService.getCustomers(this.page).subscribe(result => {
            this.page.totalElements = result.count;
            this.totalRecords = result.count;

            this.page.totalPages    = result.count / this.page.size;
            this.items = result.data;
            window.dispatchEvent(new Event('resize'));
            this.loading = false;
        },
        error => {
            this.loading = false;
            window.dispatchEvent(new Event('resize'));
            this.toastr.error(_lang('Customers') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                timeOut: 3000,
            });
        })
    }

    editCustomer(record): void {
        this.router.navigate(['customers/form/' + record.CU1_ID + '/' + record.CO1_ID]);
    }

    openLanguageEditor(){
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: window.innerWidth-60+'px',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: 'Shop List',
                componentRef: this,
                data: [],
                componentName: 'CustomersComponent'
            },
        });
    }

    addClick() {
        this.router.navigate(['customers/add']);
    }

    cloneRecord() {
      if (this.selected) {
          console.log(this.selected);
          this.router.navigate(['customers/clone/' + this.selected.CU1_ID]);
      }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.customerService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord(rowData?) {
      if (this.selected || rowData) {
          const CU1_ID = (rowData && rowData.CU1_ID) ? rowData.CU1_ID : this.selected.CU1_ID;
          this.router.navigate(['customers/edit/' + CU1_ID]);
      }
    }

    onPage(event) {
        this.loading = true;
        this.page.size = event.rows;
        this.page.pageNumber = (event.first / event.rows);
        this.setPage({offset: this.page.pageNumber});
    }

    deleteRecord() {
      if(this.selected) {
          this.confirmationService.confirm({
              message: _lang('Are you sure that you want to perform this action?'),
              accept: () => {
                  this.loading = true;
                  this.customerService.delete(this.selected.CU1_ID).subscribe(result => {
                      this.items = this.items.filter(item => {
                          return item.CU1_ID === this.selected.CU1_ID ? false : true;
                      });
                      this.loading = false;
                  });
              }
          });
      }
    }
}
