import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';

import { permissions } from './../../permissions';
import { CustomersComponent } from './customers.component';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: CustomersComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Customers'),
        permissions: {
            only: [permissions.CUSTOMERS_READ]
        }
    },
},{
    path: 'add',
    component: CustomerFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Add Customer'),
        permissions: {
            only: [permissions.CUSTOMERS_ADD]
        }
    }
},{
    path: 'clone/:id',
    component: CustomerFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Clone Customer'),
        permissions: {
            only: [permissions.CUSTOMERS_ADD]
        }
    }
}, {
    path: 'edit/:id',
    component: CustomerFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Edit Customer'),
        permissions: {
            only: [permissions.CUSTOMERS_EDIT]
        }
    }
}];