import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../../../services/customer.service';
import { Title } from '@angular/platform-browser';
import { _lang } from '../../../pipes/lang';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-customer-access',
  templateUrl: './customer-access.component.html',
  styleUrls: ['./customer-access.component.scss'],
  providers: [CustomerService]
})
export class CustomerAccessComponent implements OnInit {

  private items: Array<any> = [];
  private constants = Constants;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private customerService: CustomerService,
      private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle(_lang('CustomersAccessTitle'));

    const params = this.route.snapshot.params;
    /*
    this.customerService.getAccess(params['id'], params['CO1_ID']).subscribe( result => {
      this.items = result.data;
    });
    */
  }

  save(form){
    const params = this.route.snapshot.params;
    /*
    this.customerService.setAccess(params['id'], params['CO1_ID'], this.items).subscribe(result=>{
      this.router.navigate(['customers']);
    });
    */
  }

  onCancel(){
    this.router.navigate(['customers']);
  }
}
