import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { MatDialog} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Page } from '../../model/page';
import { Subscription ,  Subject } from 'rxjs';
import { HeaderService } from '../../services/header.service';
import {ImportReportDataService} from "../../services/import-report-data.service";
import {FileUploaderModalComponent} from "../../components/file-uploader-modal/file-uploader-modal.component";
import {FileUploadService} from "../../services/file-upload.service";
import {ShopService} from "../../services/shop.service";


@Component({
  selector: 'app-import-report-data',
  templateUrl: './import-report-data.component.html',
  styleUrls: ['./import-report-data.component.scss'],
  providers: [
      ImportReportDataService,
      ConfirmationService,
      FileUploadService
  ]
})
export class ImportReportDataComponent implements OnInit {

  private permissions = permissions;

  @ViewChild('view')              view: ElementRef;
  @ViewChild('actionbar')         actionbar:  ElementRef;
  @ViewChild('tableHeader')       tableHeader:  ElementRef;
  @ViewChild('paginator')         paginator:  ElementRef;

  private scrollHeight: string = '50px';

  /** filter object with selected filters properties */
  private filter: any = {};

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  private cols: any[] = [];
  private items: any[] = [];

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private loading: boolean = true;
  private subscription: Subscription;
  private sub: Subscription;
  selected: any;
  private locations: Array<any> = [];

  private jobCostSummaryFile: File = null;
  private salesJournalUploadFile: File = null;

  private fileTypes: Array<Object> = [];

  constructor(private importReportDataService: ImportReportDataService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private shopService: ShopService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private elementRef: ElementRef,
              private fileUploadService: FileUploadService,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    this.sub = this.headerService.subscribe({
        next: (v) => {
          this.page.pageNumber = 0;
          this.page.filterParams = {};
          this.loading = true;
          this.getImportData();
        }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'created_on',            header: _lang('#CreatedOn#'),       visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'im1_lo1_id',            header: _lang('LO1_NAME'),          visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'im1_file_name',         header: _lang('File Name'),         visible: true, export: {visible: true, checked: true}, width: 250 },
      { field: 'im1_type',              header: _lang('File Type'),         visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'im1_total_records',     header: _lang('Total Records'),     visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'im1_processed_records', header: _lang('Processed Records'), visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'im1_ignored_records',   header: _lang('Ignored Records'),   visible: true, export: {visible: true, checked: true}, width: 100 }
    ];

    this.fileTypes = this.importReportDataService.getFileTypes();

    var me = this;
    (new Promise((resolve, reject) => {
      var page = new Page();
      me.shopService.getItems(page).subscribe( res => {
        me.locations = res.data.map( (location:any) => {
            return {
                LO1_ID: parseInt(location.LO1_ID),
                text:   location.LO1_NAME
            }
        });
        resolve();
      })
    })).then(function (record) {
      return new Promise((resolve, reject) => {
        me.getImportData();
      });
    });
  }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
    this.sub.unsubscribe();
  }

  getScrollHeight(): number{
    return (this.view.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
         this.paginator.nativeElement.offsetHeight);
  }

  getImportData() {
    this.loading = true;

    if (this.subscription)
      this.subscription.unsubscribe();

    this.subscription = this.importReportDataService.getImportReportData(this.page).subscribe(result => {
          this.items = result['data'];
          this.totalRecords = result['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px'});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Technicians') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Import Data List'),
        componentRef: this,
        data: [],
        componentName: 'ImportReportDataComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getImportData();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    this.page.filterParams[col] = value;
    this.page.pageNumber = 0;
    this.getImportData();
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.importReportDataService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  public importJobCostSummary(){
    let dialogRef = this.dialog.open(FileUploaderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-100+'px',
      role: 'dialog',
      data: {
        title: _lang('Job Cost Summary Upload'),
        componentRef: this.elementRef,
        uploadTitle: 'Upload',
        fileTypes: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      }
    });

    var me = this;
    dialogRef.beforeClose().subscribe( result => {
      if(result){
        this.loading = true;
        this.importReportDataService.uploadFile(
            result['fileToUpload'], 'xls',
            result['fileToUpload']['name'], '', '', 'import-report-data/import-job-cost-summary').subscribe( result => {
            if (result && result.data && result.data.success === true ) {
              this.getImportData();
            } else {
              this.loading = false;
              me.toastr.error(
                  _lang('Import file uploading error occured. ' + "\n" + result.data.errors.join("\n")),
                  _lang('Service Error'), {
                timeOut: 10000,
              });
            }
        });
      }
    });
  }

  public importSalesJournal(){
    let dialogRef = this.dialog.open(FileUploaderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-100+'px',
      role: 'dialog',
      data: {
        title: _lang('Sales Journal Upload'),
        componentRef: this.elementRef,
        uploadTitle: 'Upload',
        fileTypes: 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      },
    });

    var me = this;
    dialogRef.beforeClose().subscribe( result => {
      if(result){
        this.loading = true;
        this.importReportDataService.uploadFile(
            result['fileToUpload'], 'xls',
            result['fileToUpload']['name'], '', '', 'import-report-data/import-sales-journal').subscribe( result => {
            if (result && result.data && result.data.success === true ) {
              this.getImportData();
            } else {
              this.loading = false;
              me.toastr.error(
                  _lang('Import file uploading error occured. ' + "\n" + result.data.errors.join("\n")),
                  _lang('Service Error'), {
                timeOut: 10000,
              });
            }
        });
      }
    });
  }
}
