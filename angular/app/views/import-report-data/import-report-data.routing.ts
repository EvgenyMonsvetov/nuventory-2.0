import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { ImportReportDataComponent } from './import-report-data.component';

export const routes: Routes = [{
    path: '',
    component: ImportReportDataComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Import Report Data'),
        permissions: {
            only: [permissions.IMPORT_REPORT_DATA_READ]
        }
    }
}];