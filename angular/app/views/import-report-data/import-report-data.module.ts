import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './import-report-data.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ImportReportDataComponent } from './import-report-data.component';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule
    ],
    declarations: [
        ImportReportDataComponent
    ]
})
export class ImportReportDataModule { }