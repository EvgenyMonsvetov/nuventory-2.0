import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportReportDataComponent } from './import-report-data.component';

describe('ImportReportDataComponent', () => {
  let component: ImportReportDataComponent;
  let fixture: ComponentFixture<ImportReportDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportReportDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportReportDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
