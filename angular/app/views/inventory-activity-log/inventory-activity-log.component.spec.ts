import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryActivityLogComponent } from './inventory-activity-log.component';

describe('InventoryActivityLogComponent', () => {
  let component: InventoryActivityLogComponent;
  let fixture: ComponentFixture<InventoryActivityLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryActivityLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryActivityLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
