import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './inventory-activity-log.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { CurrencyModule } from '../../components/currency/currency.module';
import { InventoryActivityLogComponent } from './inventory-activity-log.component';
import { InventoryService } from '../../services/inventory.service';
import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        CurrencyModule,
        ReportingFilterModule
    ],
    providers: [
        InventoryService
    ],
    declarations: [
        InventoryActivityLogComponent
    ]
})
export class InventoryActivityLogModule { }