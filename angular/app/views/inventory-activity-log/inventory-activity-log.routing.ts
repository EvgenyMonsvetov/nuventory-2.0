import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { InventoryActivityLogComponent } from './inventory-activity-log.component';

export const routes: Routes = [{
    path: '',
    component: InventoryActivityLogComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Inventory Activity Log'),
        permissions: {
            only: [permissions.DASHBOARD_READ]
        }
    }
}];