import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { TitleService } from '../../services/title.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HeaderService } from '../../services/header.service';
import { Subscription } from 'rxjs';
import { _lang } from '../../pipes/lang';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { Page } from '../../model/page';
import { MatDialog } from '@angular/material';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { InventoryService } from '../../services/inventory.service';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Rx';
import { takeUntil } from 'rxjs/internal/operators';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-inventory-activity-log',
  templateUrl: './inventory-activity-log.component.html',
  styleUrls: ['./inventory-activity-log.component.scss'],
  preserveWhitespaces: true
})
export class InventoryActivityLogComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  formatDate = formatDate;

  @ViewChild('inventoryActivityLogView') inventoryActivityLogView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableCaption')  tableCaption:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';

  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private disableRunButton: boolean = false;

  private loading: boolean = false;
  sub: Subscription;
  private subscriptionHeader: Subscription;
  private subscriptionSidebarNav: Subscription;

  private users: Array<object> = [];
  private shops:   Array<object> = [];
  private technicians:   Array<object> = [];

  /** control for the selected filters */
  public createdByCtrl: FormControl = new FormControl();
  public createdByFilterCtrl: FormControl = new FormControl();
  public locationCtrl: FormControl = new FormControl();
  public locationFilterCtrl: FormControl = new FormControl();
  public technicianCtrl: FormControl = new FormControl();
  public technicianFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyCreatedBy = new Subject<void>();
  private _onDestroyLocation = new Subject<void>();
  private _onDestroyTechnician = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredCreatedByUsers: any[] = [];
  public filteredShops: any[] = [];
  public filteredTechnicians: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  constructor(private inventoryService: InventoryService,
              public  dialog: MatDialog,
              private router: Router,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = {};
    this.page.filterParams['from'] = this.appSidebarNavService.fromDate;
    this.page.filterParams['to'] = this.appSidebarNavService.toDate;

    this.subscriptionHeader = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
      }
    });

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.page.filterParams['from'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['to'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
      }
    });
  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['from']);
    const toDate = new Date(this.page.filterParams['to']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableRunButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableRunButton = false;
    }
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'LG1_CREATED_ON',    header: _lang('LG1_CREATED_ON'),      visible: true, export: {visible: true, checked: true}, width: 120, sortable: true },
      { field: 'LO1_NAME',          header: _lang('LO1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 120, sortable: true },
      { field: 'CREATED_BY',        header: _lang('#By#'),                visible: true, export: {visible: true, checked: true}, width: 120, sortable: true },
      { field: 'TE1_NAME',          header: _lang('TE1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 120, sortable: true },
      { field: 'LG1_DESC',          header: _lang('LG1_DESC'),            visible: true, export: {visible: true, checked: true}, width: 160, sortable: true },
      { field: 'LTYPE',             header: _lang('#Type#'),              visible: true, export: {visible: true, checked: true}, width: 60, sortable: true },
      { field: 'NUMBER',            header: _lang('#TicketOrOrder#'),     visible: true, export: {visible: true, checked: true}, width: 100, },
      { field: 'LINE',              header: _lang('#Line#'),              visible: true, export: {visible: true, checked: true}, width: 50 },
      { field: 'MD1_PART_NUMBER',   header: _lang('MD1_PART_NUMBER'),     visible: true, export: {visible: true, checked: true}, width: 120, sortable: true },
      { field: 'MD1_DESC1',         header: _lang('MD1_DESC1'),           visible: true, export: {visible: true, checked: true}, width: 160, sortable: true },
      { field: 'LG1_UNIT_COST',     header: _lang('LG1_UNIT_COST'),       visible: true, export: {visible: true, checked: true}, width: 80, align: 'right' },
      { field: 'LG1_EXTENDED_COST', header: _lang('LG1_EXTENDED_COST'),   visible: true, export: {visible: true, checked: true}, width: 80, align: 'right' },
      { field: 'LG1_OLD_QTY',       header: _lang('LG1_OLD_QTY'),         visible: true, export: {visible: true, checked: true}, width: 80, align: 'right' },
      { field: 'LG1_NEW_QTY',       header: _lang('LG1_NEW_QTY'),         visible: true, export: {visible: true, checked: true}, width: 80, align: 'right' },
      { field: 'LG1_ADJ_QTY',       header: _lang('LG1_ADJ_QTY'),         visible: true, export: {visible: true, checked: true}, width: 70, align: 'right' }
    ];

    this.inventoryService.preloadData().subscribe( data => {
      this.prepareData(data);
    });

    // listen for search field value changes
    this.createdByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCreatedBy))
        .subscribe(() => {
          this.filterCreatedByUsers();
        });
    this.locationFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocation))
        .subscribe(() => {
          this.filterShops();
        });
  this.technicianFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyTechnician))
        .subscribe(() => {
          this.filterTechnicians();
        });
  }

  private filterCreatedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.createdByFilterCtrl.value;
    if (!search) {
      this.filteredCreatedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Created By Users
    this.filteredCreatedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterShops() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationFilterCtrl.value;
    if (!search) {
      this.filteredShops = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location
    this.filteredShops = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterTechnicians() {
    if (!this.technicians) {
      return;
    }
    // get the search keyword
    let search = this.technicianFilterCtrl.value;
    if (!search) {
      this.filteredTechnicians = this.technicians;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the technicians
    this.filteredTechnicians = this.technicians.filter(shop => shop['TE1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.users = [{US1_ID: null, US1_NAME: _lang('#AllRecords#')}];
    let usersArr = data.users.map( user => {
      return {
        US1_ID:   user.US1_ID,
        US1_NAME: user.US1_NAME
      }
    }).filter( customer => {
      return customer.US1_NAME && customer.US1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.US1_NAME < b.US1_NAME)
        return -1;
      if (a.US1_NAME > b.US1_NAME)
        return 1;
      return 0;
    });
    this.users = this.users.concat(usersArr);
    this.filterCreatedByUsers();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);
    this.filterShops();

    this.technicians = [{TE1_ID: null, TE1_NAME: _lang('#AllRecords#')}];
    let techniciansArr = data.technicians.map( technician => {
      return {
        TE1_ID:   technician.TE1_ID,
        TE1_NAME: technician.TE1_NAME
      }
    }).filter( technician => {
      return technician.TE1_NAME && technician.TE1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.TE1_NAME < b.TE1_NAME)
        return -1;
      if (a.TE1_NAME > b.TE1_NAME)
        return 1;
      return 0;
    });
    this.technicians = this.technicians.concat(techniciansArr);
    this.filterTechnicians();
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscriptionHeader)
      this.subscriptionHeader.unsubscribe();
    if (this.subscriptionSidebarNav)
      this.subscriptionSidebarNav.unsubscribe();
    this._onDestroyCreatedBy.next();
    this._onDestroyCreatedBy.complete();
    this._onDestroyLocation.next();
    this._onDestroyLocation.complete();
    this._onDestroyTechnician.next();
    this._onDestroyTechnician.complete();
  }

  getScrollHeight(): number {
    return (this.inventoryActivityLogView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight + this.tableFilter.nativeElement.offsetHeight +
        this.tableCaption.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getItems() {
    this.loading = true;

    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.inventoryService.getActivityLog(this.page).subscribe(res => {
          this.items = res['data']['currentVocabulary'];
          this.totalRecords = res['data']['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Inventory Activity Log') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Inventory Activity List'),
        componentRef: this,
        data: [],
        componentName: 'InventoryActivityLogComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  onPdfPrint() {
    this.inventoryService.printPDF(this.page).subscribe( result => {
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.inventoryService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  onSort(col) {
    if (col.sortable) {
      if (this.page.filterParams.hasOwnProperty('SORT_COLUMN')) {
        if (this.page.filterParams['SORT_COLUMN'] === col.field) {
          if (this.page.filterParams['SORT_DIRECTION'] === 'ASC') {
            this.page.filterParams['SORT_DIRECTION'] = 'DESC';
          } else {
            this.page.filterParams['SORT_DIRECTION'] = 'ASC';
          }
        } else {
          this.page.filterParams['SORT_COLUMN'] = col.field;
          this.page.filterParams['SORT_DIRECTION'] = 'ASC';
        }
      } else {
        this.page.filterParams['SORT_COLUMN'] = col.field;
        this.page.filterParams['SORT_DIRECTION'] = 'ASC';
      }
      this.loading = true;
      this.page.pageNumber = 0;
      this.getItems();
    }
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'LG1_CREATED_ON') && value === '01-01-1970') {
      this.page.filterParams[col] = '';
    } else {
      this.page.filterParams[col] = value;
    }
    this.page.pageNumber = 0;
    this.getItems();
  }
}
