import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';
import { Subject, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { formatDate } from '@angular/common';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { HeaderService } from '../../services/header.service';
import { takeUntil } from 'rxjs/internal/operators';
import { FormControl } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { GroupsService } from '../../services/groups';

@Component({
  selector: 'app-purchase-orders',
  templateUrl: './purchase-orders.component.html',
  styleUrls: ['./purchase-orders.component.scss'],
  providers: [
    PurchaseOrderService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class PurchaseOrdersComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  formatDate = formatDate;

  @ViewChild('purchaseOrdersView') purchaseOrdersView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';

  /** control for the selected filters */
  public orderedByCtrl: FormControl = new FormControl();
  public orderedByFilterCtrl: FormControl = new FormControl();
  public locationByCtrl: FormControl = new FormControl();
  public locationByFilterCtrl: FormControl = new FormControl();
  public companyCtrl: FormControl = new FormControl();
  public companyFilterCtrl: FormControl = new FormControl();
  public vendorCtrl: FormControl = new FormControl();
  public vendorFilterCtrl: FormControl = new FormControl();
  public modifiedByCtrl: FormControl = new FormControl();
  public modifiedByFilterCtrl: FormControl = new FormControl();
  public createdByCtrl: FormControl = new FormControl();
  public createdByFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyOrderedBy = new Subject<void>();
  private _onDestroyLocationBy = new Subject<void>();
  private _onDestroyCompany = new Subject<void>();
  private _onDestroyVendor = new Subject<void>();
  private _onDestroyModifiedBy = new Subject<void>();
  private _onDestroyCreatedBy = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredOrderedByUsers: any[] = [];
  public filteredShopsBy: any[] = [];
  public filteredCompanies: any[] = [];
  public filteredVendors: any[] = [];
  public filteredModifiedByUsers: any[] = [];
  public filteredCreatedByUsers: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  poStatuses: any[];
  uploadStatuses: any[];
  cols: any[] = [];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;
  private users: Array<object> = [];
  private companies:   Array<object> = [];
  private vendors:   Array<object> = [];
  private shops:   Array<object> = [];
  private headerSubscription: Subscription;
  private getItemsSubscription: Subscription;
  private selectedPOStatus;

  constructor(private purchaseOrderService: PurchaseOrderService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private auth: AuthService,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = {};
    this.headerSubscription = this.headerService.subscribe({
        next: (v) => {
          this.page.pageNumber = 0;
          this.loading = true;
          this.getItems();
        }
    });
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.poStatuses = [
      { label: _lang('#AllRecords#'), value: null},
      { label: _lang('#Open#'),       value: 'open'},
      { label: _lang('#Received#'),   value: 'received'}
    ];

    if (this.auth.accessLevel === GroupsService.GROUP_ADMIN || this.auth.accessLevel === GroupsService.GROUP_COMPANY_GROUP_MANAGER
        || this.auth.accessLevel === GroupsService.GROUP_COMPANY_MANAGER || this.auth.accessLevel === GroupsService.GROUP_MANUFACTURER_MANAGER) {
      this.poStatuses.push({ label: _lang('#Open#') + '/' + _lang('#Received#'), value: 'open_received'});
      this.poStatuses.push({ label: _lang('Deleted'),      value: 'deleted'});
      this.selectedPOStatus = 'open_received';
      this.page.filterParams['PO1_STATUS_TEXT'] = 'open_received';
    }

    this.uploadStatuses = [
      { label: _lang('#Sending#'),    value: 'sending'},
      { label: _lang('#InProgress#'), value: 'in_progress'},
      { label: _lang('#Completed#'),  value: 'completed'},
      { label: _lang('#None#'),       value: 'none'}
    ];

    this.cols = [
      { field: 'CREATED_ON',              header: _lang('#CreatedOn#'),             visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'ORDERED_BY',              header: _lang('#OrderedBy#'),             visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'PO1_NUMBER',              header: _lang('PO1_NUMBER'),              visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'VD1_NAME',                header: _lang('VD1_NAME'),                visible: true, export: {visible: true, checked: true}, width: 130 },
      { field: 'PO1_STATUS_TEXT',         header: _lang('PO1_STATUS_TEXT'),         visible: true, export: {visible: true, checked: true}, width: 130 },
      { field: 'PO1_UPLOAD_STATUS_TEXT',  header: _lang('PO1_UPLOAD_STATUS_TEXT'),  visible: true, export: {visible: true, checked: true}, width: 115 },
      { field: 'PO1_TOTAL_VALUE',         header: _lang('IN1_TOTAL_VALUE'),         visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'LO1_NAME',                header: _lang('ORDER_BY_LOCATION_NAME'),  visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'CO1_NAME',                header: _lang('ORDER_FROM_COMPANY_NAME'), visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'PO1_COMMENT',             header: _lang('PO1_COMMENT'),             visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MODIFIED_ON',             header: _lang('#ModifiedOn#'),            visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MODIFIED_BY',             header: _lang('#ModifiedBy#'),            visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'CREATED_BY',              header: _lang('#CreatedBy#'),             visible: true, export: {visible: true, checked: true}, width: 160 }
    ];

    this.purchaseOrderService.preloadData().subscribe( data => {
      this.prepareData(data);
      this.getItems();
    });

    // listen for search field value changes
    this.orderedByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyOrderedBy))
        .subscribe(() => {
          this.filterOrderedByUsers();
        });
    this.locationByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocationBy))
        .subscribe(() => {
          this.filterShopsBy();
        });
    this.companyFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCompany))
        .subscribe(() => {
          this.filterCompanies();
        });
    this.vendorFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyVendor))
        .subscribe(() => {
          this.filterVendors();
        });
    this.modifiedByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyModifiedBy))
        .subscribe(() => {
          this.filterModifiedByUsers();
        });
    this.createdByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCreatedBy))
        .subscribe(() => {
          this.filterCreatedByUsers();
        });
  }

  private filterOrderedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.orderedByFilterCtrl.value;
    if (!search) {
      this.filteredOrderedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Ordered By Users
    this.filteredOrderedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterShopsBy() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationByFilterCtrl.value;
    if (!search) {
      this.filteredShopsBy = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location by
    this.filteredShopsBy = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterCompanies() {
    if (!this.companies) {
      return;
    }
    // get the search keyword
    let search = this.companyFilterCtrl.value;
    if (!search) {
      this.filteredCompanies = this.companies;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the companies
    this.filteredCompanies = this.companies.filter(company => company['CO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterVendors() {
    if (!this.vendors) {
      return;
    }
    // get the search keyword
    let search = this.vendorFilterCtrl.value;
    if (!search) {
      this.filteredVendors = this.vendors;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the vendors
    this.filteredVendors = this.vendors.filter(company => company['VD1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterModifiedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.modifiedByFilterCtrl.value;
    if (!search) {
      this.filteredModifiedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Modified By Users
    this.filteredModifiedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterCreatedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.createdByFilterCtrl.value;
    if (!search) {
      this.filteredCreatedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Modified By Users
    this.filteredCreatedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.users = [{US1_ID: null, US1_NAME: _lang('#AllRecords#')}];
    let usersArr = data.users.map( user => {
      return {
        US1_ID:   user.US1_ID,
        US1_NAME: user.US1_NAME
      }
    }).filter( customer => {
      return customer.US1_NAME && customer.US1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.US1_NAME < b.US1_NAME)
        return -1;
      if (a.US1_NAME > b.US1_NAME)
        return 1;
      return 0;
    });
    this.users = this.users.concat(usersArr);
    this.filterOrderedByUsers();
    this.filterModifiedByUsers();
    this.filterCreatedByUsers();

    this.companies = [{CO1_ID: null, CO1_NAME: _lang('#AllRecords#')}];
    let companiesArr = data.companies.map( company => {
      return {
        CO1_ID:   company.CO1_ID,
        CO1_NAME: company.CO1_NAME
      }
    }).filter( company => {
      return company.CO1_NAME && company.CO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.CO1_NAME < b.CO1_NAME)
        return -1;
      if (a.CO1_NAME > b.CO1_NAME)
        return 1;
      return 0;
    });
    this.companies = this.companies.concat(companiesArr);
    this.filterCompanies();

    this.vendors = [{VD1_ID: null, VD1_NAME: _lang('#AllRecords#')}];
    let vendorsArr = data.vendors.map( vendor => {
      return {
        VD1_ID:   vendor.VD1_ID,
        VD1_NAME: vendor.VD1_NAME
      }
    }).filter( vendor => {
      return vendor.VD1_NAME && vendor.VD1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.VD1_NAME < b.VD1_NAME)
        return -1;
      if (a.VD1_NAME > b.VD1_NAME)
        return 1;
      return 0;
    });
    this.vendors = this.vendors.concat(vendorsArr);
    this.filterVendors();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);
    this.filterShopsBy();
  }

  ngOnDestroy() {
    if (this.headerSubscription)
      this.headerSubscription.unsubscribe();
    if (this.getItemsSubscription)
      this.getItemsSubscription.unsubscribe();
    this._onDestroyOrderedBy.next();
    this._onDestroyOrderedBy.complete();
    this._onDestroyCompany.next();
    this._onDestroyCompany.complete();
    this._onDestroyLocationBy.next();
    this._onDestroyLocationBy.complete();
    this._onDestroyModifiedBy.next();
    this._onDestroyModifiedBy.complete();
    this._onDestroyCreatedBy.next();
    this._onDestroyCreatedBy.complete();
  }

  getScrollHeight(): number {
    return (this.purchaseOrdersView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getItems() {
    if (this.getItemsSubscription)
      this.getItemsSubscription.unsubscribe();

    this.getItemsSubscription = this.purchaseOrderService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Purchase orders') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Purchase Orders List'),
        componentRef: this,
        data: [],
        componentName: 'PurchaseOrdersComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if((col === 'ORDERED_ON' || col === 'COMPLETED_ON' || col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970') {
      this.page.filterParams[col] = '';
    } else {
      this.page.filterParams[col] = value;
    }
    this.page.pageNumber = 0;
    this.getItems();
  }

  onAdd() {

  }

  onOpen() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['purchase-orders/view/' + this.selected[0]['PO1_ID']]);
  }

  editRecord(rowData) {
    this.router.navigate(['purchase-orders/view/' + rowData['PO1_ID']]);
  }

  onReceive() {
    if (!this.selected) {
      this.toastr.error(
        _lang("Please select exactly one record before performing this operation."),
        _lang( "SELECTION ERROR"), {
        timeOut: 3000,
      });
    }else if(this.selected.length>1){
      this.toastr.error(
        _lang("You can only receive one order at a time."),
        _lang( "#Receiving#"), {
        timeOut: 3000,
      });
    }else{
      this.router.navigate(['purchase-orders/receiving/' + this.selected[0]['PO1_ID'] ]);
    }
  }

  onUpload() {
    if (!this.selected) {
      this.toastr.error(
        _lang("Please select at least one record before performing this operation."),
        _lang( "NO RECORD SELECTED"), {
        timeOut: 3000,
      });
    }else {
      var error = false;
      this.selected.forEach(item => {
        if (item["PO1_UPLOAD_STATUS"] == 2 || item['PO1_UPLOAD_STATUS'] == 1) {
          error = true;
          return;
        }
      });
      if (error) {
        this.toastr.error(
          _lang("You cannot mark an order ready to be sent when it is already in the process."),
          _lang("MARK AS READY"), {
              timeOut: 3000,
          });
      } else {
        var PO1_IDs = this.selected.map( item => item['PO1_ID'] );
        this.purchaseOrderService.upload(PO1_IDs).subscribe( result => {
          this.getItems();
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On upload purchase order error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
      }
    }
  }

  onPdfPrint() {
    if (this.selected && this.selected.length) {
      const PO1_IDs: any[] = [];
      this.selected.forEach(function (item) {
        PO1_IDs.push(item['PO1_ID']);
      });
      this.page.filterParams['PO1_IDs'] = PO1_IDs;

      this.purchaseOrderService.printPDF(this.page).subscribe( result => {
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
      this.page.filterParams['PO1_IDs'] = null;
    }
  }

  onDelete(event) {
    if (this.selected.length && this.selected[0]['PO1_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          var PO1_IDs = this.selected.map( item => item['PO1_ID'] );
          this.purchaseOrderService.deleteMultiple(PO1_IDs).subscribe( result => {
                this.getItems();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete purchase order error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.purchaseOrderService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

}
