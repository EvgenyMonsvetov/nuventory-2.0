import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { Location } from '@angular/common';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { environment } from '../../../../environments/environment';
import { _lang } from '../../../pipes/lang';
import { permissions } from '../../../permissions';
import { Page } from '../../../model/page';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { PurchaseOrderService } from '../../../services/purchase-order.service';
import { PurchaseOrderLineModalComponent } from '../../../components/purchase-order-line-modal/purchase-order-line-modal.component';
import { PurchaseOrderLineService } from '../../../services/purchase-order-line.service';
import { ConfirmationService } from 'primeng/api';
import { ExportModalComponent } from '../../../components/export-modal/export-modal.component';

@Component({
  selector: 'app-purchase-order-view',
  templateUrl: './purchase-order-view.component.html',
  styleUrls: ['./purchase-order-view.component.scss'],
  providers: [
    PurchaseOrderService,
    PurchaseOrderLineService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class PurchaseOrderViewComponent implements OnInit {
  @ViewChild('contentGrid')   contentGrid: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private record: any;
  private apiUrl = environment.API_URL;

  private statusData: Array<any> = [
    {'label': _lang('#Open#'),      'PO1_STATUS' : 0},
    {'label': _lang('#Received#'),  'PO1_STATUS' : 1}
  ];

  private newCustomer = '0';

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[] = [];
  orderLines: any[] = [];
  selected: any;

  private page = new Page();

  private loading: boolean = true;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private purchaseOrderService: PurchaseOrderService,
              private purchaseOrderLineService: PurchaseOrderLineService,
              private confirmationService: ConfirmationService,
              public  dialog: MatDialog,
              private elementRef: ElementRef,
              private titleService: TitleService,
              private toastr: ToastrService,
              private location: Location) {
    this.page.pageNumber = 0;
    this.page.filterParams = {};
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'PO2_LINE',                header: _lang('Line #'),            visible: true, export: {visible: true, checked: true}, width: 55 },
      { field: 'PO1_STATUS_TEXT',         header: _lang('PO1_STATUS_TEXT'),   visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'PO2_PART_NUMBER',         header: _lang('PO2_PART_NUMBER'),   visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'PO2_DESC1',               header: _lang('PO2_DESC1'),         visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'PO2_RECEIVING_QTY',       header: _lang('PO2_RECEIVING_QTY'), visible: true, export: {visible: true, checked: true}, width: 76 },
      { field: 'PO2_RECEIVED_QTY',        header: _lang('PO2_RECEIVED_QTY'),  visible: true, export: {visible: true, checked: true}, width: 75 },
      { field: 'QTY',                     header: _lang('QTY'),               visible: true, export: {visible: true, checked: true}, width: 79 },
      { field: 'UM1_RECEIVE_NAME',        header: _lang('UM1_RECEIVE_NAME'),  visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'CA1_NAME',                header: _lang('CA1_NAME'),          visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'PO2_ORDER_QTY',           header: _lang('PO2_ORDER_QTY'),     visible: true, export: {visible: true, checked: true}, width: 71 },
      { field: 'UM1_NAME',                header: _lang('UM1_PURCHASE_NAME'), visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'PO2_UNIT_PRICE',          header: _lang('PO2_UNIT_PRICE'),    visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'CREATED_ON',              header: _lang('#CreatedOn#'),       visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'CREATED_BY',              header: _lang('#CreatedBy#'),       visible: true, export: {visible: true, checked: true}, width: 120 }
    ];

    this.record = this.purchaseOrderService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.page.filterParams['PO1_ID'] = this.route.snapshot.params.id;
      this.getItems();
      this.initPo();
    }
  }

  initPo(){
    this.purchaseOrderService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
      if (result[0]) {
        let data = result[0];
        data['PO1_ID'] = parseInt(data['PO1_ID']);
        data['PO1_STATUS'] = parseInt(data['PO1_STATUS']);
        data['VD1_ID'] = parseInt(data['VD1_ID']);
        data['CO1_ID'] = parseInt(data['CO1_ID']);
        data['LO1_ID'] = parseInt(data['LO1_ID']);
        data['PO2_RECEIVING_QTY'] = (parseInt(data['PO2_RECEIVING_QTY'], 2)).toFixed(2);
        data['PO2_RECEIVED_QTY'] = (parseInt(data['PO2_RECEIVED_QTY'], 2)).toFixed(2);
        data['QTY'] = (parseInt(data['QTY'], 2)).toFixed(2);
        data['PO2_ORDER_QTY'] = (parseInt(data['PO2_ORDER_QTY'], 2)).toFixed(2);

        this.newCustomer = parseInt(data['CU1_ID']) > 0 ? '1' : '0';
        this.record = data;
      }
    });
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.purchaseOrderLineService.getAll(this.page).subscribe(res => {
        this.orderLines = res.data['currentVocabulary'].map( item => {
          item['PO2_UNIT_PRICE'] = parseFloat(item['PO2_UNIT_PRICE']).toFixed(2);
          return item;
        });
        this.loading = false;
      },
      error => {
        this.loading = false;
        this.toastr.error(_lang('Purchase order lines') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('PO Form'),
        componentRef: this,
        data: [],
        componentName: 'PurchaseOrderViewComponent'
      },
    });
  }

  onOpen(rowData) {
    this.showPurchaseOrderForm(rowData['PO2_ID']);
  }

  openRecord() {
    if (this.selected) {
      this.showPurchaseOrderForm(this.selected['PO2_ID']);
    }
  }

  keyDown(e, rowIndex) {
    if ( (e.keyCode === 9 || e.keyCode === 13) ) {
      e.stopPropagation();
      const nextRowIndex = rowIndex + 1;
      this.setFocusToField(nextRowIndex, 'td');
    }
  }

  setFocusToField(rowIndex: number, rowName: string) {
    const row = rowName + rowIndex;
    const elem = document.getElementById(row);
    if (elem) {
      elem.click();
    } else {
      let currentTd = <HTMLTableCellElement>document.getElementById(rowName + (rowIndex-1));
      currentTd.nextSibling['click']();
    }
  }

  keyPress(e) {
    return e.charCode === 46 || (e.charCode >= 48 && e.charCode <= 57);
  }

  keyUp(e) {
    this.calculateTotals();
  }

  onEditInit( $event ) {
    var interval = setInterval(() => {
      const inputQty = <HTMLInputElement>document.getElementById('input_qty_' + $event.data.MD1_ID);
      if (inputQty) {
        clearInterval(interval);
        interval = null;
        inputQty.select();
      }

      const inputPrice = <HTMLInputElement>document.getElementById('input_price_' + $event.data.MD1_ID);
      if (inputPrice) {
        clearInterval(interval);
        interval = null;
        inputPrice.select();
      }
    }, 5);

  }

  onReceive() {
    var PO1_ID = this.route.snapshot.params.id;
    this.purchaseOrderService.receive(PO1_ID, this.orderLines).subscribe( result => {
      if(result.success){
        this.toastr.success(
            _lang('Order(s) successfully marked as received.'),
            _lang('RECEIVING SUCCESS'), {
          timeOut: 3000,
        });
        this.router.navigate(['purchase-orders']);
      }else{
        this.toastr.error(
            _lang("There was an error while receiving. \n" + result.errors.join('\n')),
            _lang( "RECEIVING ERROR"), {
          timeOut: 3000,
          positionClass: 'toast-center-center'
        });
      }
    });
  }

  showPurchaseOrderForm(poId) {
    let dialogRef = this.dialog.open(PurchaseOrderLineModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('PO Line'), PO2_ID: poId, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( value => {
      if (value) {
        this.getItems();
      }
    });
  }

  onDelete(event) {
    if (this.selected && this.selected['PO2_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.purchaseOrderLineService.delete(this.selected['PO2_ID']).subscribe( result => {
                this.selected = null;
                this.getItems();
                this.initPo();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete purchase order line error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        let cols = [];
        let me = this;
        columns.forEach(function (column) {
          me.cols.forEach(function (col) {
            if (column === col['field']) {
              cols.push(col);
            }
          });
        });

        this.page.filterParams['columnsLines'] = cols;
        this.page.filterParams['subtype'] = 'with-lines';
        this.page.filterParams['PO1_ID'] = this.route.snapshot.params.id;

        this.purchaseOrderService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  calculateTotals() {
    let me = this;
    this.record.PO1_TOTAL_VALUE = 0;
    this.orderLines.forEach(function (item) {
      item['PO2_TOTAL_VALUE'] = item['PO2_ORDER_QTY'] * item['PO2_UNIT_PRICE'];
      me.record.PO1_TOTAL_VALUE += item['PO2_TOTAL_VALUE'];
    });
  }
}
