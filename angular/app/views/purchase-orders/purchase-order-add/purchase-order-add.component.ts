import { Component, ElementRef, Inject, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ShopFinderModalComponent } from '../../../components/shop-finder-modal/shop-finder-modal.component';
import { Page } from '../../../model/page';
import { permissions } from '../../../permissions';
import { MasterDataService } from '../../../services/master.data.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { TitleService } from '../../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../../pipes/lang';
import { FormControl } from '@angular/forms';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { CellEditor } from 'primeng/table';
import { DOCUMENT } from '@angular/common';
import { AddCatalogListModalComponent } from '../../../components/add-catalog-list-modal/add-catalog-list-modal.component';
import { HeaderService } from '../../../services/header.service';
import { Subscription } from 'rxjs';
import { MasterDataModalComponent } from '../../../components/master-data-modal/master-data-modal.component';
import * as _ from 'lodash';
import { VendorFinderModalComponent } from '../../../components/vendor-finder-modal/vendor-finder-modal.component';
import { PurchaseOrderService } from '../../../services/purchase-order.service';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-purchase-order-add',
  templateUrl: './purchase-order-add.component.html',
  styleUrls: ['./purchase-order-add.component.scss'],
  providers: [
    PurchaseOrderService,
    MasterDataService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class PurchaseOrderAddComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  private constants = Constants;

  @ViewChild('storeAddView')      storeAddView: ElementRef;
  @ViewChild('contentGrid')       contentGrid:  ElementRef;
  @ViewChild('actionbar')         actionbar:  ElementRef;
  @ViewChild('content')           content: ElementRef;
  @ViewChild('tableHeader')       tableHeader:  ElementRef;
  @ViewChildren(CellEditor)       editors: QueryList<CellEditor>;
  @ViewChild('inputPartNumber')   inputPartNumber: ElementRef;

  cols: any[];
  items: any[] = [];
  parts: any[] = [];
  selected: any;
  private record: any;
  private errors: Array<any> = [];

  private page = new Page();
  private size: number = 50;
  private loading: boolean = false;
  private qtyIn = false;
  private barcode: string = '';

  searchValue: string = '';
  totalValue: number = 0;

  filteredParts: any[] = [];
  addedParts: any[] = [];
  shippingDP: any[] = [ 'None', 'UPS', 'FedEx', 'USPS'];
  partNumber: string = '';

  partsControl = new FormControl();
  private subscription: Subscription;
  private hiddenAutoOrderVendors: Array<any> = [];

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  constructor(private masterDataService: MasterDataService,
              private purchaseOrderService: PurchaseOrderService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              @Inject(DOCUMENT) document,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = {};
    this.page.filterParams['MD1_CENTRAL_ORDER_FLAG'] = 0;
    this.record = this.purchaseOrderService.initialize();

    this.subscription = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.initOrder();
      }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'MD1_ID',                  header: _lang('MD1_ID'),                visible: false, width: 100 },
      { field: 'VD1_ID',                  header: _lang('VD1_ID'),                visible: false, width: 100 },
      { field: 'VD1_SHORT_CODE',          header: _lang('VD1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'VD1_NAME',                header: _lang('VD1_NAME'),              visible: true, width: 100 },
      { field: 'CO1_ID',                  header: _lang('CO1_ID'),                visible: false, width: 100 },
      { field: 'CO1_SHORT_CODE',          header: _lang('CO1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'CO1_NAME',                header: _lang('CO1_NAME'),              visible: false, width: 100 },
      { field: 'LO1_ID',                  header: _lang('LO1_ID'),                visible: false, width: 100 },
      { field: 'LO1_SHORT_CODE',          header: _lang('LO1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'LO1_NAME',                header: _lang('LO1_NAME'),              visible: false, width: 100 },
      { field: 'FI1_ID',                  header: _lang('FI1_ID'),                visible: false, width: 100 },
      { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),       visible: true,  width: 100 },
      { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),             visible: true,  width: 100 },
      { field: 'MD1_ORIGINAL_UNIT_PRICE', header: _lang('MD1_UNIT_PRICE'),        visible: true,  width: 90  },
      { field: 'UM1_PRICE_NAME',          header: _lang('UM1_PRICE_NAME'),        visible: false, width: 100 },
      { field: 'MD1_ORDER_QTY',           header: _lang('MD1_ORDER_QTY'),         visible: true,  width: 50  },
      { field: 'UM1_PURCHASE_NAME',       header: _lang('UM1_PURCHASE_NAME'),     visible: true,  width: 70  },
      { field: 'RECEIPT_QTY',             header: _lang('QTY'),                   visible: true,  width: 90  },
      { field: 'UM1_RECEIPT_NAME',        header: _lang('UM1_RECEIPT_NAME'),      visible: true,  width: 100 },
      { field: 'MD2_AVAILABLE_QTY',       header: _lang('MD2_ON_HAND_QTY'),       visible: true,  width: 100 },
      { field: 'MD2_MIN_QTY',             header: _lang('MD2_MIN_QTY'),           visible: true,  width: 70  },
      { field: 'MD1_EXTENDED_VALUE',      header: _lang('MD1_EXTENDED_VALUE'),    visible: true,  width: 100 }
    ];

    this.initOrder();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  initOrder() {
    this.loading = true;

    const result = this.purchaseOrderService.isPOCanBeCreated( this.headerService );
    if (!result['canBeCreated']) {
      this.toastr.error(result['error']);
      this.router.navigate(['/store-orders']);
    } else {
      this.purchaseOrderService.initOrder(this.page).subscribe( res => {
        this.record['CO1_ID']         = res['company']['CO1_ID'];
        this.record['CO1_SHORT_CODE'] = res['company']['CO1_SHORT_CODE'];
        this.record['CO1_NAME']       = res['company']['CO1_NAME'];

        this.record['LO1_ID']         = res['fromLocation']['LO1_ID'];
        this.record['LO1_SHORT_CODE'] = res['fromLocation']['LO1_SHORT_CODE'];
        this.record['LO1_NAME']       = res['fromLocation']['LO1_NAME'];

        this.parts = res['parts'];
        this.record.PO1_NUMBER = res['PO1_NUMBER'];

        this.inputPartNumber.nativeElement.focus();
        this.loading = false;
      });
    }
  }

  filterParts(value: string) {
    const filterValue = value.toLowerCase();
    this.filteredParts = this.parts.filter(part => {
      return part['MD1_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1 ||
            (part['MD1_DESC1'] && part['MD1_DESC1'].toLowerCase().indexOf(filterValue) > -1) ||
            (part['MD1_UPC1'] && part['MD1_UPC1'].toLowerCase().indexOf(filterValue) > -1) ||
            (part['MD1_VENDOR_PART_NUMBER'] && part['MD1_VENDOR_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1)
    });
  }

  clearSaveBarCodeKeyUp(event, fieldName){
    this.purchaseOrderService.clearSaveBarcode( this.record, fieldName );
  }

  partNumberKeyUp(event) {
    if (event.keyCode === 13) {
      this.partNumber = this.partNumber.trim().toLowerCase();
      this.purchaseOrderService.clearSaveBarcode( this, 'partNumber' );
      this.filterParts(this.partNumber);
      if (this.partNumber && this.partNumber !== '') {
        if (this.filteredParts && this.filteredParts.length > 0) {
          this.onSelectPart(this.filteredParts[0]['MD1_PART_NUMBER']);
          this.filteredParts = [];
        } else {
          this.toastr.error(_lang('No one part number does not match'), _lang('Error'), {
            timeOut: 3000,
          });
        }
      }
    }
  }

  onSelectPart(value) {
    this.page.filterParams['LO1_FROM_ID'] = this.record.LO1_ID;
    this.page.filterParams['LO1_TO_ID']   = this.record.LO1_ID;
    this.page.filterParams['MD1_PART_NUMBER'] = value;

    this.masterDataService.getFullParts(this.page).subscribe(res => {
          this.addPartsToOrder(res['data'], true);
      },
      error => {
        this.toastr.error(_lang('Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
    setTimeout(() => {
      this.filteredParts = [];
      this.partNumber = '';
      this.barcode = '';
    }, 300);
  }

  selectShop() {
    let dialogRef = this.dialog.open(ShopFinderModalComponent, {
      width: '90%',
      height: window.innerHeight - 100 + 'px',
      role: 'dialog',
      data: {
        filters: {
          'CO1_ID': this.record.CO1_ID
        },
        HIGHLIGHT_LO1_ID: parseInt(this.record.LO1_ID)
      },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.LO1_NAME       = selected.LO1_NAME;
        this.record.LO1_SHORT_CODE = selected.LO1_SHORT_CODE;
        this.record.LO1_ID         = selected.LO1_ID;
      }
    })
  }

  clearShop() {
    this.record.LO1_NAME = '';
    this.record.LO1_SHORT_CODE = '';
    this.record.LO1_ID = '';
  }

  updateTotals() {
    this.totalValue = 0;
    this.addedParts = this.addedParts.map(record => {
      var receiptFactor:number = record["UM2_RECEIPT_FACTOR"] <= 0 ? 1 : record["UM2_RECEIPT_FACTOR"];

      record['RECEIPT_QTY']        = record['MD1_ORDER_QTY'] * receiptFactor;
      record['MD1_EXTENDED_VALUE'] = Math.ceil(record['MD1_ORIGINAL_UNIT_PRICE'] * record['MD1_ORDER_QTY'] *100)/100;
      this.totalValue             += record['MD1_EXTENDED_VALUE'];
      return record;
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Purchase Order Form'),
        componentRef: this,
        data: [],
        componentName: 'PurchaseOrderAddComponent'
      },
    });
  }

  partNumberKeyDown(e){
    if ( e.keyCode === 13 ) {
      var bcRegExp = /^\+?qty\.?([0-9]+)/i;
      if( bcRegExp.test(this.barcode) ){
        let match = bcRegExp.exec(this.barcode);
        this.addedParts = this.addedParts.map( (part, index) => {
          if(index == (this.addedParts.length - 1)){
            part['MD1_ORDER_QTY'] = parseInt(match[1]);
          }
          return part;
        });
        this.updateTotals();
        this.partNumber = '';
      }
      this.barcode = '';
    }else{
      if(e.key == '+'){
        this.barcode = '';
      }
      if(e.key != 'Shift'){
        this.barcode += e.key;
      }
    }
  }

  qtyKeyDown(e, rowData, field) {
    if ( e.keyCode === 13 ) {
      e.stopPropagation();
      let child = document.querySelectorAll(".ui-selectable-row:last-child");
      let rowIndex = child[0].getAttribute('ng-reflect-index');
      const nextRowIndex = parseInt(rowIndex) + 1;
      this.setFocusToField(nextRowIndex, 'td');
      this.inputPartNumber.nativeElement.focus();
    }
  }

  setFocusToField(rowIndex: number, rowName: string) {
    const row = rowName + rowIndex;
    const elem = document.getElementById(row);
    if (elem) {
      elem.click();
    } else {
      const currentTd = <HTMLTableCellElement>document.getElementById(rowName + (rowIndex - 1));
      currentTd.nextSibling['click']();
    }
  }

  qtyKeyPress(e){
    return e.charCode >= 48 && e.charCode <= 57;
  }

  qtyKeyUp(e) {
    this.updateTotals();
  }

  onEditInit( $event ) {
    var interval = setInterval(() => {
      const input = <HTMLInputElement>document.getElementById('input_' + $event.data.MD1_ID);
      if (input) {
        clearInterval(interval);
        interval = null;
        input.select();
      }
    }, 5);

  }

  onAddOrder() {
    let dialogRef = this.dialog.open(MasterDataModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data'),
        componentRef: this,
        url: '/master-data/full-parts',
        filters: {
          LO1_TO_ID:     this.record.LO1_ID,
          LO1_FROM_ID:   this.record.LO1_ID,
          nonCentralFilter: true,
          "sort[CO1_NAME]": 'ASC',
          "sort[VD1_NAME]": 'ASC',
          "sort[MD1_PART_NUMBER]": 'ASC',
          usePagination: true
        }
      }
    });

    dialogRef.beforeClose().subscribe( parts => {
      if (parts) {
        this.addPartsToOrder(parts, false, true);
      }
    })
  }

  onAddCatalog() {
    let dialogRef = this.dialog.open(AddCatalogListModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data'),
        componentRef: this,
        filters: {
          LO1_TO_ID:        this.record.LO1_ID,
          LO1_FROM_ID:      this.record.LO1_ID,
          nonCentralFilter: true,
          "sort[CO1_NAME]": 'ASC',
          "sort[VD1_NAME]": 'ASC',
          "sort[MD1_PART_NUMBER]": 'ASC',
          usePagination:    true
        }
      }
    });

    dialogRef.beforeClose().subscribe( parts => {
      if (parts) {
        this.addPartsToOrder(parts, false, true);
      }
    })
  }

  addPartsToOrder( partsToAdd: any[], useDefaultOrderQty: boolean = true, autoFocus: boolean = false ) {
    if (partsToAdd) {
        var parts: any[] = partsToAdd.map(part => {
            var item = _.clone(part);

            item['MD2_ON_HAND_QTY'] = part['MD2_ON_HAND_QTY_TO'];
            item['MD2_AVAILABLE_QTY'] = part['MD2_AVAILABLE_QTY_TO'];
            item['MD2_MIN_QTY'] = part['MD2_MIN_QTY_TO'];
            item['MD2_AVAILABLE_QTY_FROM'] = part['MD2_AVAILABLE_QTY'];
            item['MD2_ON_HAND_QTY_FROM'] = part['MD2_ON_HAND_QTY'];

            if(useDefaultOrderQty) {
              item['MD1_ORDER_QTY'] = part['DEFAULT_ORDER_QTY'];
            }else if(part['AUTO_ORDER_QTY']){
               item['MD1_ORDER_QTY'] = part['AUTO_ORDER_QTY'];
            }else if( part['UM1_PURCHASE_ID'] == part['UM1_RECEIPT_ID']){
              item['MD1_ORDER_QTY'] = parseInt(part['MD2_MIN_QTY_TO']) - parseInt(part['MD2_ON_HAND_QTY_TO']);
            }

            if (item["MD1_ORDER_QTY"] <= 0) {
                item["MD1_ORDER_QTY"] = 1;
            }

            item['MD1_EXTENDED_VALUE'] = part['MD1_UNIT_PRICE'] * item['MD1_ORDER_QTY'];
            if (item['VD1_ID']) {
                return item;
            }
        });

        parts.forEach(part => {
          if(part){
            if (!part['MD1_ORDER_QTY']) {
              part['MD1_ORDER_QTY'] = 1;
            }
            let filtered = this.addedParts.filter(item => {
              return item['MD1_ID'] === part.MD1_ID;
            });
            if (!filtered || filtered.length === 0) {
              this.addedParts.push(part);
            } else {
              filtered[0]['MD1_ORDER_QTY'] = parseInt(filtered[0]['MD1_ORDER_QTY']) + parseInt(part['MD1_ORDER_QTY']);
            }
          }
        });

        if (autoFocus) {
          setTimeout(() => {this.setFocusToPart(parts[parts.length - 1]);}, 300);
        }

        this.updateTotals();
    }
  }

  setFocusToPart(addedPart: any, rowName: string = 'td') {
    let partIndex: number = 0;
    for (var k = 0; k < this.addedParts.length; k++) {
      if (addedPart['MD1_PART_NUMBER'] === this.addedParts[k]['MD1_PART_NUMBER']) {
        break;
      }
      partIndex ++;
    }
    this.setFocusToField(partIndex, rowName);
  }

  onAutoOrder() {
    let dialogRef = this.dialog.open(VendorFinderModalComponent, {
      width: window.innerWidth - 100 + 'px',
      height: window.innerHeight - 80 + 'px',
      role: 'dialog',
      data: {
        title: _lang('#VendorAutoOrder#'),
        VD1_ID: this.record.VD1_ID,
        hideVendors: this.hiddenAutoOrderVendors
      },
    });

    var me = this;
    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.VD1_ID = selected['VD1_ID'];

        var page = new Page();
        selected.forEach(function (item, index) {
          page.filterParams['VD1_IDs['+index+']'] = item['VD1_ID'];
          me.hiddenAutoOrderVendors.push(item['VD1_ID']);
        });

        page.filterParams['nonCentralFilter'] = true;
        page.filterParams['AUTO_ORDER_TYPE']  = MasterDataService.AUTO_ORDER_PO;
        page.filterParams['LO1_TO_ID']        = this.record.LO1_ID;
        page.filterParams['LO1_FROM_ID']      = this.record.LO1_ID;

        this.masterDataService.geAutoOrderParts(page).subscribe(res => {
              if (res['parts'].length) {
                this.addPartsToOrder(res['parts'], false, true);
              } else {
                this.toastr.error(_lang('There is no parts for auto-order in the current shop'), _lang('Error'), {
                  timeOut: 5000,
                  positionClass: 'toast-middle-center'
                });
              }
              this.loading = false;
            },
            error => {
              this.toastr.error(_lang('Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
        this.page.filterParams['VD1_IDs'] = '';
      }
    });
  }

  onDelete() {
    const me = this;
    this.confirmationService.confirm({
      message: this.deleteConfirmationMessage,
      accept: () => {
        this.selected.forEach(function(record) {
          me.addedParts.forEach((item, index) => {
            if (item['MD1_ID'] === record['MD1_ID'])
              me.addedParts.splice(index, 1);
          });
        });
        this.selected = [];
        this.updateTotals();
      }
    });
  }

  onSave() {
    this.loading = true;
    var orders = [];
    var orderNumberIndex = 1;
    this.addedParts.forEach( part => {
      var orderExists = false;
      orders.forEach( order => {
        if(part['VD1_ID'] == order['VD1_ID']){
          orderExists = true;
          order.parts.push(part);
        }
      });

      if ( !orderExists ) {
        var newOrder = new Object();
        newOrder['VD1_ID'] = part['VD1_ID'];
        newOrder['LO1_ID'] = this.record.LO1_ID;
        newOrder['PO1_NUMBER'] = this.record.PO1_NUMBER + '-' + orderNumberIndex;
        newOrder['PO1_COMMENT'] = this.record.PO1_COMMENT;
        newOrder['PO1_SHIPPING_METHOD'] = this.record.PO1_SHIPPING_METHOD;
        newOrder["GL1_ID"] = part["GL1_ID"];
        newOrder['parts'] = [part];
        orders.push(newOrder);
        orderNumberIndex++;
      }
    });


    this.purchaseOrderService.create(orders).subscribe( result => {
      if (result.success) {
        this.toastr.success(_lang('Operation was successfully executed!'), _lang('Service Success'), {
          timeOut: 3000,
        });
        this.router.navigate(['/purchase-orders']);
      } else {
        this.errors = result.errors;
        this.toastr.error('Unable to create Purchase order.', 'Error', {
          timeOut: 3000,
          tapToDismiss: true
        });
        this.loading = false;
      }
    });
  }

  cancelClick() {
    window.history.back();
  }
}
