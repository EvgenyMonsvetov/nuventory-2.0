import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { PurchaseOrdersComponent } from './purchase-orders.component';
import { PurchaseOrderViewComponent } from './purchase-order-view/purchase-order-view.component';
import { PurchaseOrderAddComponent } from './purchase-order-add/purchase-order-add.component';

export const routes: Routes = [{
    path: '',
    component: PurchaseOrdersComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Purchase Orders'),
        permissions: {
            only: [permissions.PURCHASE_ORDERS_READ]
        }
    }
},{
    path: 'view/:id',
    component: PurchaseOrderViewComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.PURCHASE_ORDERS_EDIT]
        }
    }
},{
    path: 'receiving/:id',
    component: PurchaseOrderViewComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Purchase Order'),
        permissions: {
            only: [permissions.PURCHASE_ORDERS_EDIT]
        }
    }
},{
    path: 'add/:id',
    component: PurchaseOrderAddComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Create Purchase Orders'),
        permissions: {
            only: [permissions.PURCHASE_ORDERS_ADD]
        }
    }
}];