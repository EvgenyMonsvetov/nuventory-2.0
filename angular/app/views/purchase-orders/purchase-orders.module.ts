import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './purchase-orders.routing';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MatRadioModule } from '@angular/material/radio';
import { PurchaseOrdersComponent } from './purchase-orders.component';
import { PurchaseOrderViewComponent } from './purchase-order-view/purchase-order-view.component';
import { PurchaseOrderAddComponent } from './purchase-order-add/purchase-order-add.component';
import { MatAutocompleteModule } from '@angular/material';
import { CurrencyModule } from '../../components/currency/currency.module';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        MatRadioModule,
        MatAutocompleteModule,
        CurrencyModule,
        ToastrModule.forRoot({
            positionClass: 'toast-middle-center',
            closeButton: false
        })
    ],
    providers: [
        PurchaseOrderService
    ],
    declarations: [
        PurchaseOrdersComponent,
        PurchaseOrderViewComponent,
        PurchaseOrderAddComponent
    ]
})
export class PurchaseOrdersModule { }