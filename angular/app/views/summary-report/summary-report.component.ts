import { OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Component } from '@angular/core';
import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { TitleService } from '../../services/title.service';
import { Page } from '../../model/page';
import { ActivatedRoute } from '@angular/router';
import { HeaderService } from '../../services/header.service';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { Subscription } from 'rxjs';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { SummaryChartModalComponent } from '../../components/summary-chart-modal/summary-chart-modal.component';
import { RawSalesDataStoreMonthService } from '../../services/raw-sales-data-store-month.service';

@Component({
  selector: 'app-summary-report',
  templateUrl: './summary-report.component.html',
  styleUrls: ['./summary-report.component.scss'],
  providers: [
    RawSalesDataStoreMonthService
  ],
  preserveWhitespaces: true
})
export class SummaryReportComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('summaryView')   summaryView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;

  private page = new Page();
  private sub: Subscription;
  private subscriptionHeader: Subscription;
  private subscriptionSidebarNav: Subscription;
  private headerSubscription: Subscription;
  private selectedCompanies: Array<any> = [];
  private selectedShops: Array<any> = [];
  private cols: Array<any> = [];
  private items: Array<any> = [];
  private disableRunButton: boolean = false;

  private PRIMARY = 1;
  private SECONDARY = 2;
  private CALCULATED = 3;

  private scrollHeight: string = '50px';

  constructor(private rawSalesDataStoreMonthService: RawSalesDataStoreMonthService,
              private titleService: TitleService,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
    this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;
    this.page.filterParams['timeSpan'] = '3'; // By range default

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
      }
    });

    this.headerSubscription = this.headerService.subscribe({
      next: (v) => {
        const CO1_ID = parseInt(this.headerService.CO1_ID);
        const LO1_ID = parseInt(this.headerService.LO1_ID);
        this.selectedCompanies = CO1_ID > 0 ? [CO1_ID] : [];
        this.selectedShops = LO1_ID > 0 ? [LO1_ID] : [];
        this.page.filterParams['LO1_IDs'] = LO1_ID > 0 ? [LO1_ID] : [];
      }
    });
  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['startDate']);
    const toDate = new Date(this.page.filterParams['endDate']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableRunButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableRunButton = false;
    }
  }

  ngOnInit() {
    this.selectedCompanies = [this.headerService.CO1_ID];
    this.selectedShops = [this.headerService.LO1_ID];
    this.page.filterParams['LO1_IDs'] = [this.headerService.LO1_ID];
    this.titleService.setNewTitle(this.route);
  }

  getScrollHeight(): number {
    return (this.summaryView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + this.tableHeader.nativeElement.offsetHeight + 42);
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscriptionHeader)
      this.subscriptionHeader.unsubscribe();
    if (this.subscriptionSidebarNav)
      this.subscriptionSidebarNav.unsubscribe();
  }

  getChartData() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.rawSalesDataStoreMonthService.getSummaryReport(this.page).subscribe(res => {
        if (res) {
          this.cols = res['columns'];
          this.items = res['data']['allModels'];
          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';}, 500);
        } else {
          this.cols = [];
          this.items = [];
          this.scrollHeight = '50px';
        }
      },
      error => {
        this.toastr.error(_lang('Summary') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Summary'),
        componentRef: this,
        data: [],
        componentName: 'SummaryReportComponent'
      },
    });
  }

  selectedCompaniesChange(value) {
    this.selectedCompanies = value;
  }

  selectedShopsChange(value) {
    this.page.filterParams['LO1_IDs'] = value;
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols}
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.rawSalesDataStoreMonthService.downloadXlsx(this.page, 'summary').subscribe( result => {
            },
            error => {
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  aboveTargetsChange(value) {
    this.page.filterParams['aboveTargets'] = value;
    this.getChartData();
  }

  rangeChange(value) {
    this.page.filterParams['timeSpan'] = value;
    this.getChartData();
  }

  /**
   * Returns the color shading of a row based on it's categoryType
   * @param array $data The data of a row containing a 'categoryType'
   * @return string Contains a class found in CSS files defining shade color
   */
  getRowClass(col, rowData, value) {
    if (rowData['categoryType'] && rowData['categoryType'] !== 0 && (col.header === 'Attribute' || col.header === 'Total/Target') &&
        rowData['categoryType'] !== this.SECONDARY) {
      return 'light-grey-cell';
    } else if (rowData['categoryType'] && rowData['categoryType'] !== 0 && (col.header === 'Attribute' || col.header === 'Total/Target') &&
        rowData['categoryType'] === this.SECONDARY) {
      return 'dark-grey-cell';
    } else {
      return this.getCellColor(rowData, col, null);
    }
  }

  getCellColor(data, header, value = null) {
    if (value == null) {
      if (header && data.hasOwnProperty(header.value)) {
        value = data[header.value]['value'];
      }
    }

    let result = 'white-cell';
    if (value !== null) {
      if (data['target'] && !isNaN(Number(value)) && data['target'] !== 0) {
        const calculatedPercentage = (Number(value) / data['target']) * 100;
        if (calculatedPercentage < 100) {
          result = 'green-cell';
        } else {
          result = 'red-cell';
        }
      }

      // For certain attributes, higher is better, this condition will flip the colors for those
      if (data['attribute_code'] === 'r03_c_gp_percent' ||
          data['attribute_code'] === 'r03_c_average_ro' ||
          data['attribute_code'] === 'r03_c_paint_and_materials_sold_per_ro' ||
          data['attribute_code'] === 'r03_c_paint_sold_to_sales_p' ||
          data['attribute_code'] === 'r03_c_paint_and_materials_sold_p' ||
          data['attribute_code'] === 'r03_c_ref_labor_vs_sales_p' ||
          data['attribute_code'] === 'r03_c_paint_labor_vs_sales_p' ||
          data['attribute_code'] === 'r03_c_paint_labor_per_ro' ||
          data['attribute_code'] === 'r03_c_ref_and_paint_labor_vs_sales_p') {

        if (result === 'red-cell') {
          result = 'green-cell';
        } else if (result === 'green-cell') {
          result = 'red-cell';
        }
      }
    }
    return result;
  }

  showChart(rowData) {
    if (this.page.filterParams['timeSpan'] === '3') {
      this.page.filterParams['attribute_code'] = rowData['attribute_code'];
      this.dialog.open(SummaryChartModalComponent, {
        width: window.innerWidth - 60 + 'px',
        height: window.innerHeight - 40 + 'px',
        role: 'dialog',
        data: {title: _lang('Drill Down Chart'), componentRef: this, filterParams: this.page.filterParams},
      });
    }
  }

}
