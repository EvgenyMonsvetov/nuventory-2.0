import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions  } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { SummaryReportComponent } from './summary-report.component';

export const routes: Routes = [{
    path: '',
    component: SummaryReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Summary'),
        permissions: {
            only: [permissions.SUMMARY_READ]
        }
    }
}];