import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './master-catalogs.routing';
import { MasterCatalogsComponent} from './master-catalogs.component';
import { MasterCatalogFormComponent} from './master-catalog-form/master-catalog-form.component';
import { MasterCatalogService } from '../../services/master-catalog.service';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { PartNumberLiveSearchModule } from '../../components/part-number-live-search/part-number-live-search.module';
import { MasterVendorService } from '../../services/master-vendor.service';
import { CurrencyModule } from '../../components/currency/currency.module';
import { C00categoryLiveSearchModule } from '../../components/c00category-live-search/c00category-live-search.module';
import { MasterCategoryLiveSearchModule } from '../../components/master-category-live-search/master-category-live-search.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      PartNumberLiveSearchModule,
      CurrencyModule,
      C00categoryLiveSearchModule,
      MasterCategoryLiveSearchModule
  ],
  providers: [
      MasterCatalogService,
      MasterVendorService
  ],
  declarations: [
      MasterCatalogsComponent,
      MasterCatalogFormComponent
  ]
})
export class MasterCatalogsModule { }
