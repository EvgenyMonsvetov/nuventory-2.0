import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MasterCatalogService } from '../../../services/master-catalog.service';
import { permissions } from '../../../permissions';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { DefaultImageEditorModalComponent } from '../../../components/default-image-editor-modal/default-image-editor-modal.component';
import { environment } from '../../../../environments/environment';
import { _lang } from '../../../pipes/lang';
import { Location } from '@angular/common';
import { VendorCategoryFinderModalComponent } from '../../../components/vendor-category-finder-modal/vendor-category-finder-modal.component';
import { InventoryFinderModalComponent } from '../../../components/inventory-finder-modal/inventory-finder-modal.component';
import { MasterVendorFinderModalComponent } from '../../../components/master-vendor-finder-modal/master-vendor-finder-modal.component';
import { MeasurementUnitService } from '../../../services/measurement.unit.service';
import { Page } from '../../../model/page';
import { ToastrService } from 'ngx-toastr';
import { LinkedPartsModalComponent } from '../../../components/linked-parts-modal/linked-parts-modal.component';
import { Subscription } from 'rxjs/Rx';
import { FileUploaderModalComponent } from '../../../components/file-uploader-modal/file-uploader-modal.component';
import { FileUploadService } from '../../../services/file-upload.service';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-master-catalog-add',
  templateUrl: './master-catalog-form.component.html',
  styleUrls: ['./master-catalog-form.component.scss'],
  providers: [
    MasterCatalogService,
    MeasurementUnitService,
    FileUploadService
  ],
  preserveWhitespaces: true
})
export class MasterCatalogFormComponent implements OnInit, OnDestroy {
  @ViewChild('masterCatalogForm') masterCatalogForm: ElementRef;
  @ViewChild('actionbar')         actionbar:  ElementRef;
  @ViewChild('cardHeader')        cardHeader:  ElementRef;
  @ViewChild('gridAction')        gridAction:  ElementRef;
  @ViewChild('tableHeader')       tableHeader:  ElementRef;

  private errors: Array<any> = [];
  private action: String = '';
  private constants = Constants;
  private permissions = permissions;
  private record: any;
  private selectedPart: any;
  private apiUrl = environment.API_URL;

  private discontinued: Array<any> = [{label: _lang('#Select#'), value: '', disabled: true}, {label: _lang('Available'), value: 0, disabled: false}, {label: _lang('Discontinued'), value: 1, disabled: false}];
  private umData: Array<any> = [{'UM1_NAME': '- Select -', 'UM1_ID': '', 'UM1_SIZE': '1'}];
  private VOCUnitDP: Array<any> = [{label: 'G/L', value: 0}, {label: 'LBS', value: 1}];

  deleteConfirmation: string = '';

  private catalogPartSearchMode: boolean = false;

  public loading: boolean = true;
  private cols: Array<any> = [];
  private selected: any;
  private scrollHeight: string = '50px';
  private measurementUnitSubscription: Subscription;
  private masterCatalogSubscription: Subscription;

  private fileToUpload: File = null;

  constructor(private route: ActivatedRoute,
              private masterCatalogService: MasterCatalogService,
              private measurementUnitService: MeasurementUnitService,
              private fileUploadService: FileUploadService,
              public  dialog: MatDialog,
              private elementRef: ElementRef,
              private router: Router,
              private titleService: TitleService,
              private toastr: ToastrService,
              private location: Location) { }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.record = this.masterCatalogService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    var page = new Page();
    page.size = null;
    page.pageNumber = null;

    this.measurementUnitSubscription = this.measurementUnitService.getItems(page).subscribe( result => {
      if (result && result['data']) {
        this.umData = this.umData.concat(result['data']);
      }
      this.getCatalogData();
    });
  }

  getCatalogData() {
    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.masterCatalogSubscription = this.masterCatalogService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
        if (result[0]) {
          let data = result[0];
          data['MD0_ID'] = parseInt(data['MD0_ID']);
          data['MD0_DISCONTINUED'] = data['MD0_DISCONTINUED'] ? parseInt(data['MD0_DISCONTINUED']) : 0;
          data['MD0_BODY'] = parseInt(data['MD0_BODY']);
          data['MD0_DETAIL'] = parseInt(data['MD0_DETAIL']);
          data['MD0_PAINT'] = parseInt(data['MD0_PAINT']);
          data['MD0_VOC_FLAG'] = parseInt(data['MD0_VOC_FLAG']);
          data['MD0_VOC_UNIT'] = parseInt(data['MD0_VOC_UNIT']);
          this.record = data;
          this.defaultPricingChanged(this.record.UM1_DEFAULT_PRICING, true);
          this.defaultPurchaseChanged(this.record.UM1_DEFAULT_PRICING, true);
          this.defaultReceiveChanged(this.record.UM1_DEFAULT_PRICING, true);

          if (this.action === 'clone')
            this.record['MD0_ID'] = 0;
          if (this.record['MD0_PART_NUMBER'] && this.record['MD0_PART_NUMBER'].length) {
            let p = new Page();
            p.filterParams['MD0_PART_NUMBER1'] = this.record['MD0_PART_NUMBER'];
            this.masterCatalogService.getSearch(p).subscribe(res => {
                  if (res && res.data && res.data['currentVocabulary']) {
                    this.selectedPart = res.data['currentVocabulary'][0];
                  }
                },
                error => {
                  this.toastr.error(_lang('Vendor Catalogs Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                  });
                });
          }
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.measurementUnitSubscription) {
      this.measurementUnitSubscription.unsubscribe();
    }
    if (this.masterCatalogSubscription) {
      this.masterCatalogSubscription.unsubscribe();
    }
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data Form'),
        componentRef: this,
        data: [],
        componentName: 'MasterCatalogFormComponent'
      },
    });
  }

  selectVendorCategory() {
    let dialogRef = this.dialog.open(VendorCategoryFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Vendor Category'), CA0_ID: this.record.CA0_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CA0_ID = selected['CA0_ID'];
        this.record.CA0_NAME = selected['CA0_NAME'];
      }
    });
  }

  clearVendorCategory() {
    this.record.CA0_ID = 0;
    this.record.CA0_SHORT_CODE = '';
    this.record.CA0_NAME = '';
  }

  selectMasterCategory() {
    let dialogRef = this.dialog.open(InventoryFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-100+'px',
      role: 'dialog',
      data: {title: _lang('Select Nuventory Category'), C00_ID: this.record.C00_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.C00_ID = selected['C00_ID'];
        this.record.C00_NAME = selected['C00_NAME'];
      }
    });
  }

  clearMasterCategory() {
    this.record.C00_ID = 0;
    this.record.C00_SHORT_CODE = '';
    this.record.C00_NAME = '';
  }

  selectMasterVendor() {
    let dialogRef = this.dialog.open(MasterVendorFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-100+'px',
      role: 'dialog',
      data: {title: _lang('Select Mater Vendor'), VD0_ID: this.record.VD0_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.VD0_ID = selected['VD0_ID'];
        this.record.VD0_SHORT_CODE = selected['VD0_SHORT_CODE'];
        this.record.VD0_NAME = selected['VD0_NAME'];
      }
    });
  }

  clearMasterVendor() {
    this.record.VD0_ID = 0;
    this.record.VD0_SHORT_CODE = '';
    this.record.VD0_NAME = '';
  }

  defaultPricingChanged(value, initState:boolean = false) {
    const selectedPricing = this.umData.filter( item => {
      return item['UM1_ID'] === value;
    })[0];
    this.record.UM1_DEFAULT_PRICING_NAME = selectedPricing['UM1_NAME'];
    this.record.UM1_PRICING_SIZE = parseInt(selectedPricing['UM1_SIZE']);

    if (!initState) {
      this.checkSizes();
    }
  }

  defaultPurchaseChanged(value, initState:boolean = false) {
    const selectedPurchase = this.umData.filter( item => {
      return item['UM1_ID'] === value;
    })[0];
    this.record.UM1_DEFAULT_PURCHASE_NAME = selectedPurchase['UM1_NAME'];
    this.record.UM1_PURCHASE_SIZE = parseInt(selectedPurchase['UM1_SIZE']);

    if (!initState) {
      this.checkSizes();
    }
  }

  defaultReceiveChanged(value, initState:boolean = false) {
    const selectedReceive = this.umData.filter( item => {
      return item['UM1_ID'] === value;
    })[0];
    this.record.UM1_RECEIPT_SIZE = parseInt(selectedReceive['UM1_SIZE']);

    if (!initState) {
      this.checkSizes();
    }
  }

  checkSizes() {
    if (this.record.UM1_DEFAULT_PRICING !== '' && this.record.UM1_DEFAULT_PRICING === this.record.UM1_DEFAULT_PURCHASE) {
      this.record.UM1_DEFAULT_PURCHASE_FACTOR = 1;
    }

    if (this.record.UM1_DEFAULT_PURCHASE !== '' && this.record.UM1_DEFAULT_PURCHASE === this.record.UM1_DEFAULT_RECEIVE) {
      this.record.UM1_DEFAULT_RECEIVE_FACTOR = 1;
    }
  }

  MD0_URL_GO() {
    window.open(this.record.MD0_URL, '_blank');
  }

  MD0_SAFETY_URL_GO() {
    window.open(this.record.MD0_SAFETY_URL, '_blank');
  }

  marckupChange() {
    this.record.MD0_SELL_PRICE = (this.record.MD0_UNIT_PRICE * ( 1 + this.record.MD0_MARKUP * 0.01)).toFixed(2);
  }

  saveClick() {
    if (this.fileToUpload) {
      this.uploadFile();
    } else {
      this.doSave();
    }
  }

  private doSave()
  {
    if (this.action === 'add' || this.action === 'clone') {
      this.masterCatalogService.create(this.record).subscribe(result => {
        if (result.success) {

          if (result.masterdatawithimage != null) {
            this.setImageasDefault(result.data.MD0_ID, result.masterdatawithimage);
          } else {
            this.cancelClick();
          }

        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.masterCatalogService.update(this.record).subscribe(result => {
        if (result.success) {

          if (result.masterdatawithimage != null) {
            this.setImageasDefault(result.data.MD0_ID, result.masterdatawithimage);
          } else {
            this.cancelClick();
          }

        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  setImageasDefault(MD0_ID, masterdatawithimage) {
    let dialogRef = this.dialog.open(DefaultImageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        componentRef: this,
        MD0_ID: MD0_ID,
        items: masterdatawithimage,
        componentName: 'MasterCatalogFormComponent'
      },
    });

    dialogRef.beforeClose().subscribe(success => {
      if (success) {
        this.cancelClick();
      }
    });
  }

  triggerCatalogPartSearchMode() {
    this.catalogPartSearchMode = !this.catalogPartSearchMode;
  }

  catalogPartChange(part) {
    this.record.MD0_ID          = part.MD0_ID;
    this.record.MD0_PART_NUMBER = part.MD0_PART_NUMBER;
    this.record.MD0_DESC1       = part.MD0_DESC1;
    this.selectedPart           = part;

    this.triggerCatalogPartSearchMode();
  }

  linkedPartsClick() {
    let dialogRef = this.dialog.open(LinkedPartsModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {title: _lang('LinkedPartsTab'), componentRef: this, MD0_ID: this.record.MD0_ID},
    });
  }

  chooseImage() {
    let dialogRef = this.dialog.open(FileUploaderModalComponent, {
      width: window.innerWidth - 100 + 'px',
      height: window.innerHeight - 100 + 'px',
      role: 'dialog',
      data: {title: _lang('File Upload'), componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( result => {
      if (result) {
        this.fileToUpload = result['fileToUpload'];
        this.record.FI1_FILE_PATH = result['imageUrl'];
        this.record.FI1_FILE_PATH_EXIST = true;
      }
    });
  }

  uploadFile() {
    let me = this;
    this.fileUploadService.uploadFile(this.fileToUpload, 'image').subscribe( result => {
      if (result && result.data && result.data.success === true ) {
        me.record.FI1_ID = result.data.FI1_ID;
        me.record.FI1_FILE_PATH = result.data.imageUrl;
        me.record.FI1_FILE_PATH_EXIST = true;
        me.doSave();
      } else {
        me.toastr.error(_lang('Image uploading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      }
    });
  }

  removeImage() {
    this.record.FI1_ID = 0;
    this.record.FI1_FILE_PATH = '';
    this.record.FI1_FILE_PATH_EXIST = false;
    this.fileToUpload = null;
  }
}
