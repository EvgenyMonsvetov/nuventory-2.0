import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCatalogFormComponent } from './master-catalog-form.component';

describe('MasterCatalogFormComponent', () => {
  let component: MasterCatalogFormComponent;
  let fixture: ComponentFixture<MasterCatalogFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCatalogFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCatalogFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
