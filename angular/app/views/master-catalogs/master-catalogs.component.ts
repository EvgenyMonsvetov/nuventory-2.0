import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { _lang } from '../../pipes/lang';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../model/page';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { permissions } from '../../permissions';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { MasterCatalogService } from '../../services/master-catalog.service';
import { UpdatePartsModalComponent } from '../../components/update-parts-modal/update-parts-modal.component';
import { LinkMcatalogModalComponent } from '../../components/link-mcatalog-modal/link-mcatalog-modal.component';
import { Subject, Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import * as _ from 'lodash';
import { HeaderService } from '../../services/header.service';

@Component({
  selector: 'app-master-catalogs',
  templateUrl: './master-catalogs.component.html',
  styleUrls: ['./master-catalogs.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class MasterCatalogsComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('masterCatalogsView')  masterCatalogsView: ElementRef;
  @ViewChild('actionbar')           actionbar:  ElementRef;
  @ViewChild('content')             content: ElementRef;
  @ViewChild('tableHeader')         tableHeader:  ElementRef;
  @ViewChild('tableFilter')         tableFilter:  ElementRef;
  @ViewChild('paginator')           paginator:  ElementRef;

  private scrollHeight: string = '50px';

  /** control for the selected filters */
  public vendorCtrl: FormControl = new FormControl();
  public vendorFilterCtrl: FormControl = new FormControl();
  public unitCtrl: FormControl = new FormControl();
  public unitFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyVendor = new Subject<void>();
  private _onDestroyUnit = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredVendors: any[] = [];
  public filteredUnits: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  deleteConfirmation: string = _lang('DeleteConfirmation');
  deleteConfirmationMessage: string = _lang('DeleteConfirmationMessage');

  flagsDP: any[] = [];
  cols: any[] = [];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private loading: boolean = true;
  private subscription: Subscription;
  private routeSubscription: Subscription;
  private headerSubscription: Subscription;
  private vendors: Array<object> = [];
  private units: Array<object> = [];

  constructor(private masterCatalogService: MasterCatalogService,
              public  dialog: MatDialog,
              private router: Router,
              private elementRef: ElementRef,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private headerService: HeaderService,
              private route: ActivatedRoute) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    const filterParams = this.route.snapshot.queryParams;
    this.parseParams(filterParams);

    this.routeSubscription = this.route.params.subscribe(params => {
      this.page.pageNumber = 0;
      this.parseParams(params);
      this.getItems();
    });

    this.headerSubscription = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.getItems();
      }
    });
  }

  parseParams(filterParams) {
    if (!_.isUndefined(filterParams)) {
      for (const key of Object.keys(filterParams)) {
        const value = filterParams[key];
        if (value && value !== '') {
          this.page.filterParams[key] = value;
        }
      }
    }
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'FI1_FILE_PATH',   header: _lang('Has Image'),           visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'MD0_URL',         header: _lang('MD1_URL'),             visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'MD0_SAFETY_URL',  header: _lang('MD1_SAFETY_URL'),      visible: true, export: {visible: true, checked: true}, width: 82 },
      { field: 'VD0_NAME',        header: _lang('VD1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MD0_PART_NUMBER', header: _lang('MD1_PART_NUMBER'),     visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'MD0_DESC1',       header: _lang('MD1_DESC1'),           visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MD0_UNIT_PRICE',  header: _lang('MD1_UNIT_PRICE'),      visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'UM1_PRICE_NAME',  header: _lang('UM1_PRICE_NAME'),      visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'C00_NAME',        header: _lang('Nuventory Category'),  visible: true, export: {visible: true, checked: true}, width: 150 },
      { field: 'CA0_NAME',        header: _lang('Vendor Category'),     visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'MD0_UPC1',        header: _lang('MD1_UPC1'),            visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MD0_BODY',        header: _lang('MD1_BODY'),            visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'MD0_DETAIL',      header: _lang('MD1_DETAIL'),          visible: true, export: {visible: true, checked: true}, width: 68 },
      { field: 'MD0_PAINT',       header: _lang('MD1_PAINT'),           visible: true, export: {visible: true, checked: true}, width: 65 }
    ];

    this.flagsDP = [
      { label: _lang('#AllRecords#'), value: ''},
      { label: _lang('Yes'),       value: '1'},
      { label: _lang('No'),   value: '0'}
    ];

    this.masterCatalogService.preloadData().subscribe( data => {
      this.prepareData(data);
      this.getItems();
    });

    this.vendorFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyVendor))
        .subscribe(() => {
          this.filterVendors();
        });
    this.unitFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyUnit))
        .subscribe(() => {
          this.filterUnits();
        });
  }

  private filterVendors() {
    if (!this.vendors) {
      return;
    }
    // get the search keyword
    let search = this.vendorFilterCtrl.value;
    if (!search) {
      this.filteredVendors = this.vendors;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the vendors
    this.filteredVendors = this.vendors.filter(vendor => vendor['VD0_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterUnits() {
    if (!this.units) {
      return;
    }
    // get the search keyword
    let search = this.unitFilterCtrl.value;
    if (!search) {
      this.filteredUnits = this.units;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the units
    this.filteredUnits = this.units.filter(unit => unit['UM1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.vendors = [{VD0_ID: null, VD0_NAME: _lang('#AllRecords#')}];
    let vendorsArr = data.masterVendors.map( vendor => {
      return {
        VD0_ID:   vendor.VD0_ID,
        VD0_NAME: vendor.VD0_NAME
      }
    }).filter( v => {
      return v.VD0_NAME && v.VD0_NAME.length > 0;
    }).sort((a, b) => {
      if (a.VD0_NAME < b.VD0_NAME)
        return -1;
      if (a.VD0_NAME > b.VD0_NAME)
        return 1;
      return 0;
    });
    this.vendors = this.vendors.concat(vendorsArr);
    this.filterVendors();

    this.units = [{UM1_ID: null, UM1_NAME: _lang('#AllRecords#')}];
    let unitsArr = data.units.map( vendor => {
      return {
        UM1_ID:   vendor.UM1_ID,
        UM1_NAME: vendor.UM1_NAME
      }
    }).filter( u => {
      return u.UM1_NAME && u.UM1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.UM1_NAME < b.UM1_NAME)
        return -1;
      if (a.UM1_NAME > b.UM1_NAME)
        return 1;
      return 0;
    });
    this.units = this.units.concat(unitsArr);
    this.filterUnits();
  }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
    if (this.routeSubscription)
      this.routeSubscription.unsubscribe();
    if (this.headerSubscription)
      this.headerSubscription.unsubscribe();
    this._onDestroyVendor.next();
    this._onDestroyVendor.complete();
    this._onDestroyUnit.next();
    this._onDestroyUnit.complete();
  }

  getScrollHeight(): number {
    return (this.masterCatalogsView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getItems() {
    this.loading = true;
    
    if (this.subscription)
      this.subscription.unsubscribe();

    this.subscription = this.masterCatalogService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Vendor Catalogs') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data List'),
        componentRef: this,
        data: [],
        componentName: 'MasterCatalogsComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'ORDERED_ON' || col === 'COMPLETED_ON' || col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970' || value === '') {
      delete this.page.filterParams[col];
    } else {
      this.page.filterParams[col] = value;
    }

    this.router.navigate([], { relativeTo: this.route, queryParams: this.page.filterParams });

    this.page.pageNumber = 0;
    this.getItems();
  }

  onAddNew() {
    this.router.navigate(['master-catalogs/add/0']);
  }

  onClone() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['master-catalogs/clone/' + this.selected[0]['MD0_ID']], {queryParams: {clone: true}});
  }

  editRecord() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['master-catalogs/edit/' + this.selected[0]['MD0_ID']]);
  }

  onEdit(rowData) {
    this.router.navigate(['master-catalogs/edit/' + rowData['MD0_ID']]);
  }

  onDelete(event) {
    if (this.selected && this.selected[0] && this.selected[0]['MD0_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          var MD0_IDs = this.selected.map( item => item['MD0_ID'] );
          this.masterCatalogService.deleteMultiple(MD0_IDs).subscribe( result => {
                this.getItems();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete vendor catalog error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.masterCatalogService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  updateLinkedParts() {
    const masterRecord = true; //should be based on current selected vendor (VD1_MANUFACTURER == '1')
    let dialogRef = this.dialog.open(UpdatePartsModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Update Parts'), items: this.selected, masterRecord: masterRecord, componentRef: this.elementRef},
    });
  }

  linkMCatalog() {
    let dialogRef = this.dialog.open(LinkMcatalogModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Link Manufacturer Catalog'), items: this.selected, componentRef: this.elementRef},
    });
  }
}
