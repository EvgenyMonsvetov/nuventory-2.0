import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from '../../permissions';
import { AuthGuardService as AuthGuard } from '../../auth-guard.service';
import { MasterCatalogsComponent } from './master-catalogs.component';
import { MasterCatalogFormComponent } from './master-catalog-form/master-catalog-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: MasterCatalogsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Vendor Catalog'),
        permissions: {
            only: [permissions.MASTER_CATALOG_READ]
        }
    }
},{
    path: 'add/:id',
    component: MasterCatalogFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_CATALOG_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: MasterCatalogFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_CATALOG_EDIT]
        }
    }
},{
    path: 'clone/:id',
    component: MasterCatalogFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.MASTER_CATALOG_ADD]
        }
    }
}];