import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCatalogsComponent } from './master-catalogs.component';

describe('MasterCatalogsComponent', () => {
  let component: MasterCatalogsComponent;
  let fixture: ComponentFixture<MasterCatalogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCatalogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCatalogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
