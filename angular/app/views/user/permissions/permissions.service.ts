import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

import {GeneralService} from './../../../services/general.service';
import {IPermission} from "../../rights/permissions/permissions.interface";

@Injectable()
export class UserPermissionsService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getPermissions(userId: string): Observable<IResponse<IPermission>> {
        return this.http.get<IResponse<IPermission>>(this.apiUrl + '/user-permissions/'+userId);
    }

    public savePermissions(userId:string, permissions: Array<IPermission>): Observable<IResponse<IPermission>> {
        return this.http.put(this.apiUrl + '/user-permissions/',
            {userid : userId, permissions: permissions })
            .catch(this.handleError);
    }
}