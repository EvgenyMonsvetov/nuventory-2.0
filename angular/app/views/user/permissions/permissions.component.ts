import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPermission } from '../../rights/permissions/permissions.interface';
import { UserPermissionsService } from './permissions.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../../pipes/lang';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss'],
  providers: [UserPermissionsService],
  preserveWhitespaces: true
})
export class PermissionsComponent implements OnInit {

  public  loading = false;
  public  rows: Array<IPermission>;
  private originalRows: Array<IPermission>;

  constructor(
        private userPermissionsService: UserPermissionsService,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private titleService: TitleService) {
    }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.loading = true;
    this.userPermissionsService.getPermissions(this.route.snapshot.params.id).subscribe( result => {

        this.loading = false;
        this.originalRows = result.data.map(item=>{
          return Object.assign({}, item);
        });

        this.rows = result.data;

        window.dispatchEvent(new Event('resize'));
    }, error => {
        window.dispatchEvent(new Event('resize'));
        this.toastr.error(_lang('TimeOut occured'), _lang('Service Error'), {
            timeOut: 3000,
        });
      });
  }

  changeIndividualBehavior($event) {
    this.rows = this.rows.map(function (permission: IPermission) {
      if ($event.target.name === permission.name) {
        permission.hasPermission = $event.target.checked ? true : false;
      }
      return permission;
    });
  }

  onCancel() {
    this.router.navigate(['user/profile/' + this.route.snapshot.params.id]);
  }

  savePermissions(form: NgForm) {
    this.loading = true;
    this.userPermissionsService.savePermissions(this.route.snapshot.params.id, this.rows).subscribe(result => {
      this.loading = false;
      this.router.navigate(['user/profile/' + this.route.snapshot.params.id]);
    });
  }
}
