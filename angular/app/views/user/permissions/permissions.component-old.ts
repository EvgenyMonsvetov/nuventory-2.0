import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPermission } from '../../rights/permissions/permissions.interface';
import { UserPermissionsService } from './permissions.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { _lang } from '../../../pipes/lang';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss'],
  providers: [UserPermissionsService]
})
export class PermissionsComponent implements OnInit {

  public  loading = false;
  public  rows: Array<IPermission>;
  private originalRows: Array<IPermission>;

  constructor(
        private userPermissionsService: UserPermissionsService,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService) {
    }

  ngOnInit() {
    this.loading = true;
    this.userPermissionsService.getPermissions(this.route.snapshot.params.id).subscribe( result=> {

        this.loading = false;
        this.originalRows = result.data.map(item=>{
          return Object.assign({}, item);
        });

        this.rows = result.data.map(function (row) {
          row.inheritsFromRole = true;
          return row;
        });

        window.dispatchEvent(new Event('resize'));
    }, error => {
        window.dispatchEvent(new Event('resize'));
        this.toastr.error(_lang('TimeOut occured'), _lang('Service Error'), {
            timeOut: 3000,
        });
      });
  }

  changeInheritanceBehavior($event) {

    const originalRows = this.originalRows;
    this.rows = this.rows.map(function (permission: IPermission) {
      if($event.target.name == permission.name){
        var originalRow: IPermission = originalRows.filter(function (originalRow: IPermission) {
          if(originalRow.name == permission.name){
            return originalRow;
          }
        })[0];

        permission.hasPermission    = originalRow.hasPermission;
        permission.inheritsFromRole = $event.target.checked ? true : false;
      }
      return permission;
    });
  }

  changeIndividualBehavior($event) {
    this.rows = this.rows.map(function (permission: IPermission) {
      if($event.target.name == permission.name){
        permission.hasPermission = $event.target.checked ? true : false;
      }
      return permission;
    });
  }

  onCancel(){
    this.router.navigate(['user/profile']);
  }

  savePermissions(form: NgForm) {

    this.userPermissionsService.savePermissions(this.route.snapshot.params.id, this.rows).subscribe(result => {
        console.log(result);
    });

  }
}
