import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GeneralService } from './../../../services/general.service';
import { _lang } from '../../../pipes/lang';

@Injectable()
export class UserStatusService extends GeneralService {

   static STATUS_ACTIVE   = 0;
   static STATUS_DELETED  = 1;

    constructor(protected http: HttpClient) {
        super(http);
    }

    getStatuses(): Array<{status: number, name: string}> {
        return [
          {status: UserStatusService.STATUS_ACTIVE,   name: _lang('Active')},
          {status: UserStatusService.STATUS_DELETED,  name: _lang('Deleted')}
        ]
    }
}