import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserStatusService} from "./user-status.service";
import { TitleService } from 'app/services/title.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-status',
  templateUrl: './user-status.component.html',
  styleUrls: ['./user-status.component.scss']
})
export class UserStatusComponent implements OnInit {

  @Input() activeStatus: number;
  @Output() notify: EventEmitter<number> = new EventEmitter<number>();

  private statuses: Array<Object> = [];
  constructor( private userStatusService: UserStatusService,
               private route: ActivatedRoute,
               private titleService: TitleService) { }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.statuses = this.userStatusService.getStatuses();
  }

  onChange(value){
    this.notify.emit(value);
  }
}
