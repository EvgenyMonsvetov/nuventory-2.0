import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { IUser } from '../User';
import { TitleService } from 'app/services/title.service';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-user-changepassword',
  templateUrl: './user-changepassword.component.html',
  styleUrls: ['./user-changepassword.component.scss'],
  preserveWhitespaces: true
})
export class UserChangepasswordComponent implements OnInit {

  public loading: boolean = false;
  private constants = Constants;

  private user: IUser;
  private model: any = {};
  private errors: any = {};

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private userService: UserService,
                private titleService: TitleService) {
    this.user = this.userService.initialize();
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    const userId = this.route.snapshot.params.id;
    this.userService.getById(userId).subscribe( user => {
      this.user = user;
    });
  }

  savePassword() {
    this.loading = true;
    this.model.userid = this.route.snapshot.params.id;
    this.userService.changePassword( this.model).subscribe( result => {
      this.loading = false;
      if (result.errors) {
        this.errors = result.errors;
      } else {
        this.router.navigate(['user/profile/' + this.route.snapshot.params.id]);
      }
    });
  }

  onCancel(){
    this.router.navigate(['user/profile/' + this.route.snapshot.params.id]);
  }
}
