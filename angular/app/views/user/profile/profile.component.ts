import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../auth/auth.service";
import { ActivatedRoute, Router} from '@angular/router';
import { IUser } from "../User";
import {UserService} from "../../../services/user.service";
import {permissions} from "../../../permissions";
import {UserStatusService} from "../user-status/user-status.service";
import { TitleService } from 'app/services/title.service';

@Component({
  templateUrl: 'profile.component.html',
  providers: [
    UserStatusService
  ],
  preserveWhitespaces: true
})
export class ProfileComponent implements OnInit {

  properties;
  public permissions = permissions;

  private user: IUser;

  constructor(
      private authService: AuthService,
      private router: Router,
      private route: ActivatedRoute,
      private userService: UserService,
      private titleService: TitleService) {
    this.user = this.userService.initialize();
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    const userId = this.route.snapshot.params.id || this.authService.userId;
    this.userService.getById(userId).subscribe( user => {
       this.user = user;
    });
  }

  editCustomer() {
    this.router.navigate(['user/profile/edit/' + this.user.US1_ID]);
  }

  changePassword() {
    this.router.navigate(['user/changepassword/' + this.user.US1_ID]);
  }

  editPermissions(){
    this.router.navigate(['user/permissions/' + this.user.US1_ID]);
  }

  editRoles(){
    this.router.navigate(['user/roles/' + this.user.US1_ID]);
  }

  editAccess(){
    this.router.navigate(['user/access/' + this.user.US1_ID] );
  }

  logout() {
    this.authService.logout();
  }
}