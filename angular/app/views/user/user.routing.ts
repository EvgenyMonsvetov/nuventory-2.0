import {Routes } from '@angular/router';

import {UserComponent } from './user.component';
import {permissions } from './../../permissions';
import {NgxPermissionsGuard } from 'ngx-permissions';
import {AuthGuardService as AuthGuard} from './../../auth-guard.service';
import {UserChangepasswordComponent} from "./user-changepassword/user-changepassword.component";
import {PermissionsComponent} from "./permissions/permissions.component";
import {RolesComponent} from "./roles/roles.component";
import {UserFormComponent} from "./user-form/user-form.component";
import {AdminChangepasswordComponent} from "./admin-changepassword/admin-changepassword.component";
import {UserGuardService} from '../../user-guard.service';


export const routes: Routes = [{
  path: '',
  component: UserComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.USERS_READ]
    }
  }
},{
  path: 'add',
  component: UserFormComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.USERS_ADD]
    }
  }
},{
  path: 'changepassword/:id',
  component: UserChangepasswordComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard, UserGuardService]
},{
  path: 'adminchangepassword/:id',
  component: AdminChangepasswordComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.USERS_EDIT]
    }
  }
},{
  path: 'permissions/:id',
  component: PermissionsComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.USERS_EDIT]
    }
  }
},{
  path: 'roles/:id',
  component: RolesComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.USERS_EDIT]
    }
  }
},{
  path: 'profile',
  component: UserFormComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.PROFILE_READ]
    }
  }
},{
  path: 'profile/:id',
  component: UserFormComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard, UserGuardService]
},{
  path: 'profile/edit/:id',
  component: UserFormComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.USERS_EDIT]
    }
  }
},{
  path: 'clone/:id',
  component: UserFormComponent,
  canActivate: [AuthGuard, NgxPermissionsGuard],
  data: {
    permissions: {
      only: [permissions.USERS_EDIT]
    }
  }
}];
