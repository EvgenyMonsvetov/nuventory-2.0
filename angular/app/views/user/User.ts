export interface IUser {
  'US1_ID': number,
  'US1_ACTIVE': boolean,
  'US1_LOGIN': string,
  'US1_EMAIL': string,
  'password': string,
  'repeat_password': string,
  'VD0_ID': number,
  'CG1_ID': number,
  'US1_TYPE': number,
  'US1_NAME': string,
  'errors':  any,
  'DELETED': number
}
