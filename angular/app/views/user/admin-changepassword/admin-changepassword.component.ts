import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../services/user.service";
import {ToastrService} from "ngx-toastr";
import {IUser} from "../User";

@Component({
  selector: 'app-user-changepassword',
  templateUrl: './admin-changepassword.component.html',
  styleUrls: ['./admin-changepassword.component.scss'],
  preserveWhitespaces: true
})
export class AdminChangepasswordComponent implements OnInit {

  public loading: boolean = false;

  private user: IUser;
  private model: any = {};
  private errors: any = {};

  constructor(  private route: ActivatedRoute,
                private router: Router,
                private userService: UserService,
                private toastr: ToastrService) {
    this.user = this.userService.initialize();
  }

  ngOnInit() {
    const userId = this.route.snapshot.params.id;
    this.userService.getById(userId).subscribe( user => {
      this.user = user;
    });
  }

  changePassword() {
    let template = `Hello ${this.user.US1_NAME}.\nYour new password: ${this.model.repeat_password}`;

    this.model.subject = 'Your password has been changed';
    this.model.body = template;
  }

  savePassword() {
    this.loading = true;
    this.model.userid = this.route.snapshot.params.id;

    this.userService.changePasswordAdmin( this.model).subscribe( result => {
      this.loading = false;
      if(result.errors){
        this.errors = result.errors;
      }else{
        this.router.navigate(['user/profile/'+this.route.snapshot.params.id]);
      }
    });
  }

  onCancel(){
    this.router.navigate(['user/profile/'+this.route.snapshot.params.id]);
  }
}
