import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserProfileService } from '../../../services/user.profile.service';
import { TitleService } from 'app/services/title.service';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-user-access',
  templateUrl: './user-access.component.html',
  styleUrls: ['./user-access.component.scss'],
  providers: [UserProfileService],
  preserveWhitespaces: true
})
export class UserAccessComponent implements OnInit {

  @ViewChild('accessTable') table: any;

  private loading = false;
  private profile: any = {};
  private rows: Array<object>;
  private constants = Constants;

  private accessTypes: Array<object> = [
    { value: 2, text: 'Full'},
    { value: 1, text: 'Restricted'},
    { value: 0, text: 'No access'}
  ];

  constructor( private router: Router,
               private route: ActivatedRoute,
               private userProfileService: UserProfileService,
               private titleService: TitleService) { }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.loading = true;
    var userId = this.route.snapshot.params.id;
    this.userProfileService.getByUserId(userId).subscribe(data => {
      this.profile = data.profile;

      this.rows = data.access.map( row => {
        const disabled     = (row.access === 2 || row.access === 0) ? true : false;
        row.customers = row.customers.map( customer => {
          customer['disabled'] = disabled;
          return customer;
        });

        row.vendors = row.vendors.map( vendor => {
          vendor['disabled'] = disabled;
          return vendor;
        });

        return row;
      });
      this.loading = false;
    });
  }

  onCancel() {
    this.router.navigate(['user/profile/'+this.route.snapshot.params.id]);
  }

  save() {
    this.profile['UP1_RESTRICT_COMPANIES'] = this.profile['UP1_RESTRICT_COMPANIES'] ? 1 : 0;
    this.profile['UP1_INCOGNITO_MODE']     = this.profile['UP1_INCOGNITO_MODE'] ? 1 : 0;
    this.profile['access']                 = JSON.stringify(this.rows);

    this.userProfileService.save( this.profile, true).subscribe(data => {
      this.router.navigate(['user/profile/'+this.route.snapshot.params.id]);
    });
  }

  changeAccess(row) {
    if (row.access === 0) {
      if( row.extended ) {
        this.table.rowDetail.toggleExpandRow(row);
        row.extended = false;
      }
    } else if (row.access === 1) {
      if ( !row.extended ) {
        this.table.rowDetail.toggleExpandRow(row);
        row.extended = true;
      }
    } else {
      if ( row.extended ) {
        this.table.rowDetail.toggleExpandRow(row);
        row.extended = false;
      }
    }

    this.prepareData(row);
  }

  toggleExpandRow(row) {
    row.extended = !row.extended;
    this.prepareData(row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  prepareData(row) {
    var rowAccess = parseInt(row.access);
    var disabled  = (rowAccess === 2 || rowAccess === 0) ? true : false;

    row.customers.forEach( item => {
      item.selected = rowAccess === 2 ? true : (rowAccess === 1 ? item.hasAccess : false);
      item.disabled = disabled;
    });

    row.vendors.forEach( item => {
      item.selected = rowAccess === 2 ? true : (rowAccess === 1 ? item.hasAccess : false);
      item.disabled = disabled;
    });
  }

  onCustomerChanged(event, customer) {
    customer.hasAccess = event.selected;
    customer.selected  = event.selected;
  }

  onVendorChanged(event, vendor) {
    vendor.hasAccess = event.selected;
    vendor.selected  = event.selected;
  }

   getCellClass({ row, column, value }): any {
    return {
      'row-full-access': parseInt(row.access) === 2,
      'row-full-no-access': parseInt(row.access) === 0,
      'row-restricted-access': parseInt(row.access) === 1
    };
  }
}

