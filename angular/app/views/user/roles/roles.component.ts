import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IRole } from '../../rights/roles/roles.interface';
import { UserRolesService } from './roles.service';
import { RolesService } from '../../rights/roles/roles.service';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../../pipes/lang';

@Component({
  selector: 'app-user-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
  providers: [
    UserRolesService
  ],
})
export class RolesComponent implements OnInit {

  public loading = false;
  public rows: Array<IRole>;
  public roles: Array<IRole>;
  private originalRoles: Array<IRole>;

  constructor(
        private rolesService: RolesService,
        private userRolesService: UserRolesService,
        private router: Router,
        private toastr: ToastrService,
        private titleService: TitleService,
        private route: ActivatedRoute) {
    }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.loading = true;
    this.rolesService.getAll(null).subscribe( res => {
      this.originalRoles = res.data;

      this.userRolesService.getRoles(this.route.snapshot.params.id).subscribe(result => {
        this._updateData(result.data);
        this.loading = false;
        window.dispatchEvent(new Event('resize'));
      },
      error => {
        window.dispatchEvent(new Event('resize'));
        this.toastr.error(_lang('TimeOut occured'), _lang('Service Error'), {
            timeOut: 3000,
        });
      });
    })

  }

  private _updateData(assignedRoles) {
    this.rows = assignedRoles;
    let rolesName = assignedRoles.map(function (role: IRole) {
        return role.name;
    });

    this.roles = this.originalRoles.filter(function (role: IRole) {
       return rolesName.indexOf(role.name) === -1;
    });
  }

  assign(role) {
    this.userRolesService.assign(this.route.snapshot.params.id, role).subscribe(result => {
        this._updateData(result.data);
    });
  }

  removeAssignment(role) {
      this.userRolesService.remove(this.route.snapshot.params.id, role).subscribe(result => {
          this._updateData(result.data);
      });
  }

  onCancel() {
    this.router.navigate(['user/profile/' + this.route.snapshot.params.id]);
  }

  editRole(value) {
    this.router.navigate(['rights/roles/edit/' + value]);
  }
}
