import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

import {GeneralService} from './../../../services/general.service';
import {IRole} from "../../rights/roles/roles.interface";

@Injectable()
export class UserRolesService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getRoles(userId: string): Observable<IResponse<IRole>> {
        return this.http.get<IResponse<IRole>>(this.apiUrl + '/user-roles/'+userId);
    }

    public assign(userId:string, roleName: string): Observable<IResponse<IRole>> {
        return this.http.post(this.apiUrl + '/user-roles',
            {userid : userId, rolename: roleName })
            .catch(this.handleError);
    }

    public remove(userId:string, roleName: string): Observable<IResponse<IRole>> {
        const url = `${this.apiUrl + '/user-roles/'}`;
        return this.http.delete(url, { params: {userid: userId, rolename: roleName}} )
            .catch(this.handleError);
    }
}