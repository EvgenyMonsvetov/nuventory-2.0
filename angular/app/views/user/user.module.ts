import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RouterModule } from '@angular/router';
import { routes } from './user.routing';
import { UserComponent } from './user.component';
import { UserChangepasswordComponent } from './user-changepassword/user-changepassword.component';
import { AdminChangepasswordComponent} from "./admin-changepassword/admin-changepassword.component";
import { ProfileComponent } from './profile/profile.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { RolesComponent } from './roles/roles.component';
import { UserStatusComponent } from './user-status/user-status.component';
import { UserstatusPipe} from '../../pipes/userstatus.pipe';
import { UserAccessComponent } from './user-access/user-access.component';
import { UserProfileService } from '../../services/user.profile.service';
import { UserFormComponent } from './user-form/user-form.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    NgxPermissionsModule.forChild(),
    SharedModule
  ],
  declarations: [
    UserComponent,
    UserChangepasswordComponent,
    PermissionsComponent,
    RolesComponent,
    ProfileComponent,
    UserStatusComponent,
    UserstatusPipe,
    UserAccessComponent,
    UserFormComponent,
    AdminChangepasswordComponent
  ],
  providers: [
  	UserProfileService,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class UserModule { }
