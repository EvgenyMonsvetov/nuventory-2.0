import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { UserService } from '../../services/user.service';
import { IUser } from './User';
import { UserStatusService } from './user-status/user-status.service';
import { TitleService } from 'app/services/title.service';
import { ConfirmationService } from 'primeng/api';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Subscription ,  Subject } from 'rxjs';
import { HeaderService } from '../../services/header.service';
import { _lang } from '../../pipes/lang';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { NgxPermissionsService } from 'ngx-permissions';

@Component({
    templateUrl: 'user.component.html',
    styleUrls: ['./user.component.scss'],
    providers: [
        UserStatusService,
        ConfirmationService,
        UserService
    ],
preserveWhitespaces: true
})
export class UserComponent {

    private permissions = permissions;

    @ViewChild('usersView')    usersView: ElementRef;
    @ViewChild('actionbar')    actionbar: ElementRef;
    @ViewChild('tableHeader')  tableHeader: ElementRef;
    @ViewChild('tableFilter')  tableFilter: ElementRef;
    @ViewChild('paginator')    paginator :ElementRef;

    private scrollHeight: string = '50px';

    /** control for the selected filters */
    public companyCtrl: FormControl = new FormControl();
    public companyFilterCtrl: FormControl = new FormControl();
    public locationByCtrl: FormControl = new FormControl();
    public locationByFilterCtrl: FormControl = new FormControl();
    /** Subject that emits when the component has been destroyed. */
    private _onDestroyCompany = new Subject<void>();
    private _onDestroyLocationBy = new Subject<void>();
    /** list of companies filtered by search keyword */
    public filteredCompanies: any[] = [];
    public filteredShopsBy: any[] = [];
    /** filter object with selected filters properties */
    private filter: any = {};

    private cols: any[] = [];
    private items: any[] = [];
    private companies: Array<object> = [];
    private shops: Array<object> = [];
    private groups: Array<object> = [];

    private selected: any = null;
    private loading =false;
    private size: number = 50;
    private totalRecords: number = 0;
    private subscription: Subscription;

    page = new Page();
    rows = new Array<IUser>();
    private flagsActive: any[] = [];

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private titleService: TitleService,
        private route: ActivatedRoute,
        public  dialog: MatDialog,
        private confirmationService: ConfirmationService,
        private permissionsService: NgxPermissionsService,
        private userService: UserService,
        private headerService: HeaderService
    ) {
        this.page.size = this.size;
        this.page.pageNumber = 0;
        this.subscription = this.headerService.subscribe({
            next: (v) => {
                this.page.pageNumber = 0;
                this.page.filterParams = {};
                this.loading = true;
                this.getItems();
            }
        });
    }

    ngOnDestroy() {
        if (this.subscription)
            this.subscription.unsubscribe();
        this._onDestroyCompany.next();
        this._onDestroyCompany.complete();
        this._onDestroyLocationBy.next();
        this._onDestroyLocationBy.complete();
    }

    addUser(id): void {
        this.router.navigate(['user/add/' + id]);
    }

    ngOnInit() {
        this.titleService.setNewTitle(this.route);

        this.cols = [
            { field: 'CO1_NAME',       header: _lang('CO1_NAME'),      visible: true, export: {visible: true, checked: true}, width: 160 },
            { field: 'LO1_NAME',       header: _lang('LO1_NAME'),      visible: true, export: {visible: true, checked: true}, width: 160 },
            { field: 'US1_ID',         header: _lang('#UsersUserID#'), visible: true, export: {visible: true, checked: true}, width: 50 },
            { field: 'US1_ACTIVE',     header: _lang('Active'),        visible: true, export: {visible: true, checked: true}, width: 50 },
            { field: 'US1_LOGIN',      header: _lang('US1_LOGIN'),     visible: true, export: {visible: true, checked: true}, width: 120 },
            { field: 'US1_EMAIL',      header: _lang('US1_EMAIL'),     visible: true, export: {visible: true, checked: true}, width: 160 },
            { field: 'US1_NAME',       header: _lang('US1_NAME'),      visible: true, export: {visible: true, checked: true}, width: 160 },
            { field: 'GR1_NAME',       header: _lang('GR1_NAME'),      visible: true, export: {visible: true, checked: true}, width: 120 },
        ];

        this.flagsActive = [
          { label: _lang('#AllRecords#'), value: ''},
          { label: _lang('Yes'),          value: '1'},
          { label: _lang('No'),           value: '0'}
        ];

        this.userService.preloadData().subscribe( data => {
            this.prepareData(data);
            this.getItems();
        });

        this.companyFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroyCompany))
            .subscribe(() => {
                this.filterCompanies();
            });
        this.locationByFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroyLocationBy))
            .subscribe(() => {
                this.filterShopsBy();
            });
    }

    private filterCompanies() {
        if (!this.companies) {
            return;
        }
        // get the search keyword
        let search = this.companyFilterCtrl.value;
        if (!search) {
            this.filteredCompanies = this.companies;
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the companies
        this.filteredCompanies = this.companies.filter(company => company['CO1_NAME'].toLowerCase().indexOf(search) > -1);
    }

    private filterShopsBy() {
        if (!this.shops) {
            return;
        }
        // get the search keyword
        let search = this.locationByFilterCtrl.value;
        if (!search) {
            this.filteredShopsBy = this.shops;
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the location by
        this.filteredShopsBy = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
    }

    getItems() {
        this.loading = true;
        this.userService.getUsers(this.page).subscribe(res => {
                this.items = res.data;
                this.totalRecords = res.count;

                setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

                this.loading = false;
            },
            error => {
                this.loading = false;
                this.toastr.error(_lang('Users') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                });
            });
    }

    prepareData(data) {
        this.companies = [{CO1_ID: null, CO1_NAME: _lang('#AllRecords#')}];
        let companiesArr = data.companies.map( company => {
            return {
                CO1_ID:   company.CO1_ID,
                CO1_NAME: company.CO1_NAME
            }
        }).filter( company => {
            return company.CO1_NAME && company.CO1_NAME.length > 0;
        }).sort((a, b) => {
            if (a.CO1_NAME < b.CO1_NAME)
                return -1;
            if (a.CO1_NAME > b.CO1_NAME)
                return 1;
            return 0;
        });
        this.companies = this.companies.concat(companiesArr);
        this.filterCompanies();

        this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
        let shopsArr = data.shops.map( shop => {
            return {
                LO1_ID:   shop.LO1_ID,
                LO1_NAME: shop.LO1_NAME
            }
        }).filter( shop => {
            return shop.LO1_NAME && shop.LO1_NAME.length > 0;
        }).sort((a, b) => {
            if (a.LO1_NAME < b.LO1_NAME)
                return -1;
            if (a.LO1_NAME > b.LO1_NAME)
                return 1;
            return 0;
        });
        this.shops = this.shops.concat(shopsArr);
        this.filterShopsBy();

        this.groups = [{GR1_ID: null, GR1_NAME: _lang('#AllRecords#')}];
        this.groups = this.groups.concat(data.groups);
    }

    getScrollHeight(): number {
        return (this.usersView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
            (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
            this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
    }

    onPage(event) {
        this.page.size = event.rows;
        this.page.pageNumber = (event.first / event.rows);
        this.getItems();
    }

    buttonPress(e, col, value) {
        if ( e.keyCode === 13 ) {
            this.filterChanged(col, value);
        }
    }

    filterChanged(col, value) {
        this.loading = true;
        this.page.filterParams[col] = value;
        this.page.pageNumber = 0;
        this.getItems();
    }

    deleteUser(id){
      this.loading = true;
      this.userService.delete(id).subscribe(result => {
          this.getItems();
      });
    }

    onChangeUserStatusFilter(value:number) {
        console.log('user filter value' + value);
    }

    openLanguageEditor() {
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: '80%',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: _lang('Users List'),
                componentRef: this,
                data: [],
                componentName: 'UserComponent'
            },
        });
    }

    addClick() {
        this.router.navigate(['user/add']);
    }

    cloneRecord() {
      if (this.selected) {
          this.router.navigate(['user/clone/' + this.selected.US1_ID]);
      }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.userService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord(rowData?) {
      if (this.permissionsService.getPermission(this.permissions.USERS_EDIT)) {
          if (this.selected || rowData) {
              var US1_ID = (rowData && rowData.US1_ID) ? rowData.US1_ID : this.selected.US1_ID;
              this.router.navigate(['user/profile/' + US1_ID]);
          }
      }
    }

    deleteRecord() {
      if(this.selected) {
          this.confirmationService.confirm({
              message: _lang('Are you sure that you want to perform this action?'),
              accept: () => {
                  this.loading = true;
                  this.userService.delete(this.selected.US1_ID).subscribe(result => {
                      if (result['success']) {
                          this.getItems();
                      } else {
                          this.toastr.error(_lang('On delete user error occurred'), _lang('Service Error'), {
                              timeOut: 3000,
                          });
                      }
                      this.loading = false;
                  });
              }
          });
      }
    }
}
