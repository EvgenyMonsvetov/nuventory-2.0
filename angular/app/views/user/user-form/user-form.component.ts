import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { permissions } from '../../../permissions';
import { CompanyFinderModalComponent } from '../../../components/company-finder-modal/company-finder-modal.component';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { UserService } from '../../../services/user.service';
import { ShopFinderModalComponent } from '../../../components/shop-finder-modal/shop-finder-modal.component';
import { AuthService } from '../../../auth/auth.service';
import { GroupsService } from '../../../services/groups';
import { MasterVendorFinderModalComponent } from '../../../components/master-vendor-finder-modal/master-vendor-finder-modal.component';
import { _lang } from '../../../pipes/lang';
import { HeaderService } from '../../../services/header.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { Constants } from '../../../constants';
import { UserGuardService } from '../../../user-guard.service';

@Component({
  selector: 'app-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
  providers: [
    UserService,
    GroupsService,
    NgxPermissionsService,
    UserGuardService
  ],
  preserveWhitespaces: true
})
export class UserFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';
  private groups: Array<any> = [];

  private permissions = permissions;
  private hasEditRole: boolean = false;
  private canEditUserGroup: boolean = false;
  private record: any;
  private constants = Constants;
  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private userService: UserService,
      private groupsService: GroupsService,
      public dialog: MatDialog,
      private authService: AuthService,
      private headerService: HeaderService,
      private userGuardService: UserGuardService,
      protected ngxPermissionsService: NgxPermissionsService) {}

  ngOnInit() {
    this.record = this.userService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    let userId = this.route.snapshot.params.id ? parseInt(this.route.snapshot.params.id) :
                   ( this.action === 'profile' ? this.authService.userId : 0 );

    this.hasEditRole = this.ngxPermissionsService.getPermission(permissions.USERS_EDIT) ? true : false;
    this.canEditUserGroup = (userId === this.authService.userId) || !this.hasEditRole ? false : true;

    this.groupsService.getAll().subscribe( data => {
      this.groups = data['data'];
      if (userId > 0) {
        this.userService.getById(userId).subscribe( data => {
          data['US1_CORPORATE_REPORT'] = parseInt(data['US1_CORPORATE_REPORT']);
          data['US1_ALLOW_MOBILE_JOB_CREATE'] = parseInt(data['US1_ALLOW_MOBILE_JOB_CREATE']);
          data['US1_INVENTORY_REPORT'] = parseInt(data['US1_INVENTORY_REPORT']);
          data['US1_GATEKEEPER'] = parseInt(data['US1_GATEKEEPER']);
          data['US1_PO_NOTIFY'] = parseInt(data['US1_PO_NOTIFY']);
          data['US1_ACTIVE'] = parseInt(data['US1_ACTIVE']);
          this.record = data;
        });
      } else {
        if (this.headerService.CO1_ID > 0) {
          this.record.CO1_ID = this.headerService.CO1_ID;
          this.record.CO1_NAME = this.headerService.CO1_NAME;
        }
        if (this.headerService.LO1_ID > 0) {
          this.record.LO1_ID = this.headerService.LO1_ID;
          this.record.LO1_NAME = this.headerService.LO1_NAME;
        }
      }
    });
  }

  cancelClick() {
    this.router.navigate(['user']);
  }

  openLanguageEditor() {
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('User Form'),
            componentRef: this,
            data: [],
            componentName: 'UserFormComponent'
          },
      });
  }

  saveClick() {
    if (this.action === 'add' || this.action === 'copy') {
      this.userService.create(this.record).subscribe(result => {
        if (result.success) {
          this.router.navigate(['user']);
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit' || this.action === 'profile') {
      this.userService.update(this.record).subscribe(result => {
        if (result.success) {
          this.router.navigate(['user']);
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  selectCompany() {
    let dialogRef = this.dialog.open(CompanyFinderModalComponent, {
      width: '90%',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Company'), CO1_ID: this.record.CO1_ID },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CO1_NAME = selected.CO1_NAME;
        this.record.CO1_ID   = selected.CO1_ID;
      }
    })
  }

  clearCompany() {
    this.record.CO1_NAME = '';
    this.record.CO1_ID   = '';

    this.record.LO1_NAME = '';
    this.record.LO1_ID   = '';
  }

  selectShop() {
    let dialogRef = this.dialog.open(ShopFinderModalComponent, {
      width: '90%',
      height: window.innerHeight-100+'px',
      role: 'dialog',
      data: {
        sourceUrl: '/shop/all',
        filters: {
          CO1_ID: this.record.CO1_ID
        },
        HIGHLIGHT_LO1_ID: this.record.LO1_ID
      }
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.LO1_NAME = selected.LO1_NAME;
        this.record.LO1_ID   = selected.LO1_ID;
      }
    })
  }

  clearShop() {
    this.record.LO1_NAME = '';
    this.record.LO1_ID   = '';
  }

  selectMasterVendor() {
    let dialogRef = this.dialog.open(MasterVendorFinderModalComponent, {
      width: '90%',
      height: window.innerHeight-100+'px',
      role: 'dialog',
      data: {
        VD0_ID: this.record.VD0_ID
      },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.VD0_NAME = selected.VD0_NAME;
        this.record.VD0_ID   = selected.VD0_ID;
      }
    })
  }

  clearMasterVendor() {
    this.record.VD1_NAME = '';
    this.record.VD0_ID = 0;
  }

  editCustomer() {
    this.router.navigate(['user/profile/edit/' + this.record.US1_ID]);
  }

  changePassword() {
    if( this.ngxPermissionsService.getPermission(permissions.ADMIN_CHANGE_PASSWORD) && this.authService.userId != this.record.US1_ID ){
      this.router.navigate(['user/adminchangepassword/' + this.record.US1_ID]);
    }else{
      this.router.navigate(['user/changepassword/' + this.record.US1_ID]);
    }
  }
}
