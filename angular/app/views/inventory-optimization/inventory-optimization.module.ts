import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './inventory-optimization.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { InventoryOptimizationComponent } from './inventory-optimization.component';
import { CurrencyModule } from '../../components/currency/currency.module';
import { MasterDataService } from '../../services/master.data.service';
import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        CurrencyModule,
        ReportingFilterModule
    ],
    providers: [
        MasterDataService
    ],
    declarations: [
        InventoryOptimizationComponent
    ]
})
export class InventoryOptimizationModule { }