import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { InventoryOptimizationComponent } from './inventory-optimization.component';

export const routes: Routes = [{
    path: '',
    component: InventoryOptimizationComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Inventory Optimization'),
        permissions: {
            only: [permissions.DASHBOARD_READ]
        }
    }
}];