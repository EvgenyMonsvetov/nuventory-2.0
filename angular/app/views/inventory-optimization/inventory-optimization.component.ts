import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { TitleService } from '../../services/title.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { _lang } from '../../pipes/lang';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { Page } from '../../model/page';
import { MatDialog } from '@angular/material';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { MasterDataService } from '../../services/master.data.service';
import { takeUntil } from 'rxjs/internal/operators';
import { Subject } from 'rxjs/Rx';
import { FormControl } from '@angular/forms';
import {HeaderService} from "../../services/header.service";
import {AppSidebarNavService} from "../../services/app-sidebar-nav.service";
import * as _ from 'lodash';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-inventory-optimization',
  templateUrl: './inventory-optimization.component.html',
  styleUrls: ['./inventory-optimization.component.scss'],
  preserveWhitespaces: true
})
export class InventoryOptimizationComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('inventoryOptimizationView') inventoryOptimizationView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableCaption')  tableCaption:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';

  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private disableRunButton: boolean = false;

  private loading: boolean = false;
  sub: Subscription;
  private subscriptionHeader: Subscription;
  private subscriptionSidebarNav: Subscription;

  private totalValue: number = 0;
  private totalUsage: number = 0;

  private reportType: string = 'inventoryOptimization';

  private limitToMinQty: boolean = true;

  private shops: Array<object> = [];
  private vendors: Array<object> = [];
  private categories: Array<object> = [];
  private units: Array<object> = [];
  private statuses: any[] = [];

  /** control for the selected filters */
  public locationCtrl: FormControl = new FormControl();
  public locationFilterCtrl: FormControl = new FormControl();
  public vendorCtrl: FormControl = new FormControl();
  public vendorFilterCtrl: FormControl = new FormControl();
  public categoriesCtrl: FormControl = new FormControl();
  public categoriesFilterCtrl: FormControl = new FormControl();
  public receivedUnitCtrl: FormControl = new FormControl();
  public receivedUnitFilterCtrl: FormControl = new FormControl();
  public orderedUnitCtrl: FormControl = new FormControl();
  public orderedUnitFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyLocation = new Subject<void>();
  private _onDestroyVendor = new Subject<void>();
  private _onDestroyCategory = new Subject<void>();
  private _onDestroyReceivedUnit = new Subject<void>();
  private _onDestroyOrderedUnit = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredShops: any[] = [];
  public filteredVendors: any[] = [];
  public filteredCategories: any[] = [];
  public filteredReceivedUnits: any[] = [];
  public filteredOrderedUnits: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};
  public shopOrderDays: string = '';

  constructor(private masterDataService: MasterDataService,
              public  dialog: MatDialog,
              private router: Router,
              private title: Title,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.first = 0;
    this.page.filterParams = {};
    const filterParams = this.route.snapshot.queryParams;
    this.parseParams(filterParams);

    this.page.filterParams['limitToMinQty'] = this.limitToMinQty;

    this.statuses = [
      { label: _lang('Normal'), value: '0'},
      { label: _lang('Under'),  value: '1'},
      { label: _lang('Over'),   value: '2'}
    ];

    switch (this.router.url.toString().replace('/master-data', '')) {
      case '/physical-inventory':
        this.reportType = 'physicalInventory';
        this.title.setTitle(_lang('Physical Inventory'));
        this.page.filterParams['REPORT_TYPE'] = 'physicalInventory';
        break;
      case '/inventory-optimization':
        this.title.setTitle(_lang('Inventory Optimization'));
      default:
        this.reportType = 'inventoryOptimization';
        this.page.filterParams['from'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['to'] = this.appSidebarNavService.toDate;
        this.page.filterParams['REPORT_TYPE'] = 'inventoryOptimization';
        break;
    }

    this.subscriptionHeader = this.headerService.subscribe({
      next: (v) => {
        this.setOrderDays();
        this.filterShops();
      }
    });

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.page.filterParams['from'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['to'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
      }
    });

  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['from']);
    const toDate = new Date(this.page.filterParams['to']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableRunButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableRunButton = false;
    }
  }

  parseParams(filterParams) {
    if (!_.isUndefined(filterParams)) {
      for (const key of Object.keys(filterParams)) {
        const value = filterParams[key];
        if (value && value !== '') {
          this.page.filterParams[key] = value;
        }
      }
    }
  }

  ngOnInit() {

    this.cols = [
      { field: 'LO1_NAME',          header: _lang('LO1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'MD1_PART_NUMBER',   header: _lang('MD1_PART_NUMBER'),     visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
      { field: 'MD1_DESC1',         header: _lang('MD1_DESC1'),           visible: true, export: {visible: true, checked: true}, width: 160, sortable: true },
      { field: 'VD1_NAME',          header: _lang('VD1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 120, sortable: true },
      { field: 'CA1_NAME',          header: _lang('CA1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 100, sortable: true },
      { field: 'GL1_NAME',          header: _lang('GL1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 60, sortable: true },
      { field: 'MD2_ON_HAND_QTY',   header: _lang('MD2_ON_HAND_QTY'),     visible: true, export: {visible: true, checked: true}, width: 50, sortable: true,align: 'right' },
      { field: 'MD2_MIN_QTY',       header: _lang('MD2_MIN_QTY'),         visible: true, export: {visible: true, checked: true}, width: 50, align: 'right' },
      { field: 'AVERAGE_USAGE',     header: _lang('#AverageUsage#'),      visible: this.reportType === 'inventoryOptimization', export: {visible: this.reportType === 'inventoryOptimization', checked: true}, width: 50, align: 'right' },
      { field: 'AVERAGE_MIN_QTY',   header: _lang('Avg Min Qty'),     visible: true, export: {visible: true, checked: true}, width: 50, align: 'right' },
      //{ field: 'QTY_DIFF',          header: _lang('QTY_DIFF'),            visible: this.reportType === 'inventoryOptimization', export: {visible: this.reportType === 'inventoryOptimization', checked: true}, width: 50, align: 'right' },
      { field: 'PART_STATUS',       header: _lang('PART_STATUS'),         visible: this.reportType === 'inventoryOptimization', export: {visible: this.reportType === 'inventoryOptimization', checked: true}, width: 80, sortable: true },
      { field: 'UM1_NAME',          header: _lang('#ReceivUnitOfMeas#'),  visible: true, export: {visible: true, checked: true}, width: 70 },
      { field: 'UM1_PURCHASE_NAME', header: _lang('#OrderedUnitOfMeas#'), visible: true, export: {visible: true, checked: true}, width: 70 },
      { field: 'UM2_FACTOR',        header: _lang('UM2_FACTOR'),          visible: true, export: {visible: true, checked: true}, width: 50, align: 'right' },
      { field: 'MD2_UNIT_PRICE',    header: _lang('#Cost#'),              visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'EXTENDED_PRICE',    header: _lang('#TotalUsage#'),        visible: this.reportType === 'inventoryOptimization', export: {visible: this.reportType === 'inventoryOptimization', checked: true}, width: 80 },
      { field: 'EXTENDED_STOCK',    header: _lang('#InStockValue#'),      visible: true, export: {visible: true, checked: true}, width: 80 }
    ];

    if ( this.router.url === '/master-data/physical-inventory' ) {
      this.cols =  this.cols.filter(function(col) {
        return col.field !== 'AVERAGE_MIN_QTY';
      });
    }

    let me = this;
    this.masterDataService.preloadData().subscribe( data => {
      this.prepareData(data);
    });
    // listen for search field value changes
    this.locationFilterCtrl.valueChanges
        .pipe(takeUntil(me._onDestroyLocation))
        .subscribe(() => {
          me.filterShops();
        });
    this.vendorFilterCtrl.valueChanges
        .pipe(takeUntil(me._onDestroyVendor))
        .subscribe(() => {
          me.filterVendors();
        });
    this.categoriesFilterCtrl.valueChanges
        .pipe(takeUntil(me._onDestroyCategory))
        .subscribe(() => {
          me.filterCategories();
        });
    this.receivedUnitFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyReceivedUnit))
        .subscribe(() => {
          this.filterReceivedUnits();
        });
    this.orderedUnitFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyOrderedUnit))
        .subscribe(() => {
          this.filterOrderedUnits();
        });
    this.setOrderDays();

    if (this.page.filterParams.hasOwnProperty('reload')) {
      if (this.page.filterParams.hasOwnProperty('pageNumber')){
        var pageNumber = parseInt(this.page.filterParams.pageNumber) > 0 ? parseInt(this.page.filterParams.pageNumber) : 1;
        this.page.pageNumber = pageNumber;
        this.page.first = this.page.size * pageNumber;
      }
      this.getItems();
    }
  }

  private filterShops() {
    if (!this.shops || (this.headerService.LO1_ID != 0)) {
      this.filteredShops = [];
      return;
    }
    // get the search keyword
    let search = this.locationFilterCtrl.value;
    if (!search) {
      this.filteredShops = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the locations
    this.filteredShops = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterVendors() {
    if (!this.vendors) {
      return;
    }
    // get the search keyword
    let search = this.vendorFilterCtrl.value;
    if (!search) {
      this.filteredVendors = this.vendors;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the vendors
    this.filteredVendors = this.vendors.filter(vendor => vendor['VD1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterCategories() {
    if (!this.categories) {
      return;
    }
    // get the search keyword
    let search = this.categoriesFilterCtrl.value;
    if (!search) {
      this.filteredCategories = this.categories;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Categories
    this.filteredCategories = this.categories.filter(unit => unit['CA1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterReceivedUnits() {
    if (!this.units) {
      return;
    }
    // get the search keyword
    let search = this.receivedUnitFilterCtrl.value;
    if (!search) {
      this.filteredReceivedUnits = this.units;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the units
    this.filteredReceivedUnits = this.units.filter(unit => unit['UM1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterOrderedUnits() {
    if (!this.units) {
      return;
    }
    // get the search keyword
    let search = this.orderedUnitFilterCtrl.value;
    if (!search) {
      this.filteredOrderedUnits = this.units;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the units
    this.filteredOrderedUnits = this.units.filter(unit => unit['UM1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.vendors = [{VD1_ID: null, VD1_NAME: _lang('#AllRecords#')}];
    let vendorsArr = data.vendors.map( vendor => {
      return {
        VD1_ID:   vendor.VD1_ID,
        VD1_NAME: vendor.VD1_NAME
      }
    }).filter( v => {
      return v.VD1_NAME && v.VD1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.VD1_NAME < b.VD1_NAME)
        return -1;
      if (a.VD1_NAME > b.VD1_NAME)
        return 1;
      return 0;
    });
    this.vendors = this.vendors.concat(vendorsArr);
    this.filterVendors();

    this.units = [{UM1_ID: null, UM1_NAME: _lang('#AllRecords#')}];
    let unitsArr = data.units.map( vendor => {
      return {
        UM1_ID:   vendor.UM1_ID,
        UM1_NAME: vendor.UM1_NAME
      }
    }).filter( u => {
      return u.UM1_NAME && u.UM1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.UM1_NAME < b.UM1_NAME)
        return -1;
      if (a.UM1_NAME > b.UM1_NAME)
        return 1;
      return 0;
    });
    this.units = this.units.concat(unitsArr);
    this.filterReceivedUnits();
    this.filterOrderedUnits();

    this.categories = [{CA1_ID: null, CA1_NAME: _lang('#AllRecords#')}];
    let categoriesArr = data.categories.map( category => {
      return {
        CA1_ID:   category.CA1_ID,
        CA1_NAME: category.CA1_NAME
      }
    }).filter( c => {
      return c.CA1_NAME && c.CA1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.CA1_NAME < b.CA1_NAME)
        return -1;
      if (a.CA1_NAME > b.CA1_NAME)
        return 1;
      return 0;
    });
    this.categories = this.categories.concat(categoriesArr);
    this.filterCategories();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);
    this.filterShops();
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscriptionHeader)
      this.subscriptionHeader.unsubscribe();
    if (this.subscriptionSidebarNav)
      this.subscriptionSidebarNav.unsubscribe();
    this._onDestroyLocation.next();
    this._onDestroyLocation.complete();
    this._onDestroyVendor.next();
    this._onDestroyVendor.complete();
    this._onDestroyReceivedUnit.next();
    this._onDestroyReceivedUnit.complete();
    this._onDestroyOrderedUnit.next();
    this._onDestroyOrderedUnit.complete();
  }

  getScrollHeight(): number {
    return (this.inventoryOptimizationView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight + this.tableFilter.nativeElement.offsetHeight +
        this.tableCaption.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight + 32);
  }

  getItems() {
    this.loading = true;
    this.page.filterParams['reload'] = true;
    this.page.filterParams['pageNumber'] = this.page.pageNumber;
    this.router.navigate([], { relativeTo: this.route, queryParams: this.page.filterParams });

    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.masterDataService.getInventoryOptimization(this.page).subscribe(res => {
          this.items = res['data'].map(item => {
            item['MD2_ON_HAND_QTY'] = item['MD2_ON_HAND_QTY'] ? parseInt(item['MD2_ON_HAND_QTY']) : 0;
            item['MD2_MIN_QTY'] = item['MD2_MIN_QTY'] ? parseInt(item['MD2_MIN_QTY']) : 0;
            item['AVERAGE_USAGE'] = item['AVERAGE_USAGE'] ? parseFloat(item['AVERAGE_USAGE']).toFixed(2) : 0;
            item['QTY_DIFF'] = item['QTY_DIFF'] ? parseInt(item['QTY_DIFF']) : 0;
            item['MD2_UNIT_PRICE'] = item['MD2_UNIT_PRICE'] ? parseFloat(item['MD2_UNIT_PRICE']) : 0;
            item['EXTENDED_PRICE'] = item['EXTENDED_PRICE'] ? parseFloat(item['EXTENDED_PRICE']) : 0;
            item['EXTENDED_STOCK'] = item['EXTENDED_STOCK'] ? parseFloat(item['EXTENDED_STOCK']) : 0;
            return item;
          });

          this.totalRecords = res['count'];
          this.totalValue = res['totalValue'];
          this.totalUsage = res['totalUsage'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Inventory Optimization') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Inventory Optimization Report List'),
        componentRef: this,
        data: [],
        componentName: 'InventoryOptimizationComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  editRecord(rowData) {
    this.router.navigate(['master-data/edit/' + rowData['MD1_ID']]);
  }

  onOpen() {
    if (this.selected) {
      this.router.navigate(['master-data/edit/' + this.selected['MD1_ID']]);
    }
  }

  onPdfPrint() {
    this.masterDataService.printInventoryOptimization(this.page).subscribe( result => {
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.masterDataService.downloadXlsx(this.page, 'inventory-optimization').subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  limitToMinQtyChange(value) {
    this.limitToMinQty = value;
    this.page.pageNumber = 0;
    this.page.filterParams['limitToMinQty'] = value;
  }

  onSort(col) {
    if (col.sortable) {
      if (this.page.filterParams.hasOwnProperty('SORT_COLUMN')) {
        if (this.page.filterParams['SORT_COLUMN'] === col.field) {
          if (this.page.filterParams['SORT_DIRECTION'] === 'ASC') {
            this.page.filterParams['SORT_DIRECTION'] = 'DESC';
          } else {
            this.page.filterParams['SORT_DIRECTION'] = 'ASC';
          }
        } else {
          this.page.filterParams['SORT_COLUMN'] = col.field;
          this.page.filterParams['SORT_DIRECTION'] = 'ASC';
        }
      } else {
        this.page.filterParams['SORT_COLUMN'] = col.field;
        this.page.filterParams['SORT_DIRECTION'] = 'ASC';
      }
      this.loading = true;
      this.page.pageNumber = 0;
      this.getItems();
    }
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'ORDERED_ON' || col === 'COMPLETED_ON' || col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970') {
      this.page.filterParams[col] = '';
    } else {
      this.page.filterParams[col] = value;
    }
    this.page.pageNumber = 0;
    this.getItems();
  }

  setOrderDays(){
    this.shopOrderDays = this.headerService.getCurrentLocation()['LO1_ORDER_DAYS_PER_MONTH'];
  }

}
