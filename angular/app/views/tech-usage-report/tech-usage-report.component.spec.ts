import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechUsageReportComponent } from './tech-usage-report.component';

describe('TechUsageReportComponent', () => {
  let component: TechUsageReportComponent;
  let fixture: ComponentFixture<TechUsageReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechUsageReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechUsageReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
