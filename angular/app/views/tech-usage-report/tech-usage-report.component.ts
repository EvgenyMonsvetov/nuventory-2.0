import { OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Component } from '@angular/core';
import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import { MasterDataService } from '../../services/master.data.service';
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { TitleService } from '../../services/title.service';
import { Page } from '../../model/page';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from '../../services/header.service';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { Subscription } from 'rxjs';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { TechUsageTechnicianDetailListModalComponent } from '../../components/tech-usage-technician-detail-list-modal/tech-usage-technician-detail-list-modal.component';

@Component({
  selector: 'app-tech-usage-report',
  templateUrl: './tech-usage-report.component.html',
  styleUrls: ['./tech-usage-report.component.scss']
})
export class TechUsageReportComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('techUsage', { read: ElementRef }) container: ElementRef;
  @ViewChild('techUsageView', { read: ElementRef }) chartView: ElementRef;
  private chart;

  private isMTD: boolean = false;

  private page = new Page();
  private sub: Subscription;
  private subscriptionSidebarNav: Subscription;
  private subscriptionHeader: Subscription;
  private headerSubscription: Subscription;
  private selectedCompanies: Array<any> = [];
  private selectedShops: Array<any> = [];
  private fontStyle = {
                        fontSize: '14px',
                        fontWeight: '400',
                        fontFamily: 'Roboto'
                      };
  private disableRunButton: boolean = false;
  private slideShow: boolean = false;
  private graphHeight: string = '400px';

  constructor(private masterDataService: MasterDataService,
              private titleService: TitleService,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private router: Router,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.selectedCompanies = [this.headerService.CO1_ID];
    this.selectedShops = [this.headerService.LO1_ID];
    this.page.filterParams['startDate'] = '';
    this.page.filterParams['endDate'] = '';

    const eventUrl: string = this.router.url;
    this.slideShow = eventUrl.includes('/charts-slide-show/');

    switch (eventUrl.toString().replace('/master-data', '').replace('/charts-slide-show', '').replace('/charts-slide-show', '')) {
      case '/tech-usage-mtd-report':
        this.isMTD = true;
        this.page.filterParams['MTD'] = true;
        break;
      default:
        this.isMTD = false;
        this.page.filterParams['MTD'] = false;
        this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;
        break;
    }

    this.headerSubscription = this.headerService.subscribe({
      next: (v) => {
        const CO1_ID = parseInt(this.headerService.CO1_ID);
        const LO1_ID = parseInt(this.headerService.LO1_ID);
        this.selectedCompanies = CO1_ID > 0 ? [CO1_ID] : [];
        this.selectedShops = LO1_ID > 0 ? [LO1_ID] : [];
        this.page.filterParams['LO1_IDs'] = LO1_ID > 0 ? [LO1_ID] : [];
        if (this.isMTD) {
          this.getChartData();
        }
      }
    });

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.filterParams['startDate'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['endDate'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
      }
    });
  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['startDate']);
    const toDate = new Date(this.page.filterParams['endDate']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableRunButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableRunButton = false;
    }
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    setTimeout(() => {
      if (this.slideShow) {
        this.graphHeight = this.getGraphHeight() + 'px';
      }
      setTimeout(() => {
        this.buildHighcharts([], []);
        if (this.isMTD) {
          this.getChartData();
        }
      });
    });
  }

  getGraphHeight(): number {
    return (this.chartView.nativeElement.parentElement.parentElement.parentElement.parentElement.offsetHeight - 105);
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscriptionHeader)
      this.subscriptionHeader.unsubscribe();
    if (this.headerSubscription)
      this.headerSubscription.unsubscribe();
    if (this.subscriptionSidebarNav)
      this.subscriptionSidebarNav.unsubscribe();
  }

  getChartData() {
    if (this.sub)
      this.sub.unsubscribe();

    this.chart.showLoading();

    this.sub = this.masterDataService.getTechUsage(this.page).subscribe(res => {
          this.chart.hideLoading();
          if (this.isMTD && this.slideShow) {
            if (res['drilldown'] && res['drilldown'][0] && res['drilldown'][0]['data']) {
              this.buildHighcharts(res['drilldown'][0]['data'], []);
            } else {
              this.buildHighcharts([], []);
            }
          } else {
            this.buildHighcharts(res['series'], res['drilldown']);
          }
        },
        error => {
          this.toastr.error(_lang('Tech Usage') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  buildHighcharts(series, drilldown) {
    Highcharts.setOptions({
      lang: {
        drillUpText: _lang('Previous')
      }
    });

    let me = this;

    this.chart = Highcharts.chart(this.container.nativeElement, {
      chart: {
        type: 'column'
      },
      xAxis: {
        type: 'category',
        labels: {
          style: this.fontStyle
        }
      },
      yAxis: {
        title: {
          text: 'Tech Usage ($)',
          style: this.fontStyle
        },
        labels: {
          style: this.fontStyle
        }
      },
      title: {
        text: me.isMTD ? 'Tech Usage MTD' : 'Tech Usage'
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            format: '{point.y:.0f}/{point.perhour:.2f}',
            style: this.fontStyle
          },
          point: {
            events: {
              click: function () {
                if (this.options && this.options.hasOwnProperty('TE1_ID')) {
                  me.showTechnicianDetails(this.options);
                }
              }
            }
          }
        },
        column: {
          colorByPoint: true
        }
      },
      tooltip: {
        formatter: function() {
          return 'Tech Usage: <b>$' + (this.y).toFixed(0) + '</b>';
        }
      },
      series: [{
        name: '',
        data: series,
        type: 'column'
      }],
      drilldown: {
        series: drilldown
      }
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: this.isMTD ? _lang('Tech Usage MTD') : _lang('Tech Usage'),
        componentRef: this,
        data: [],
        componentName: 'TechUsageReportComponent'
      },
    });
  }

  selectedCompaniesChange(value) {
    this.selectedCompanies = value;
  }

  selectedShopsChange(value) {
    this.page.filterParams['LO1_IDs'] = value;
  }

  showTechnicianDetails(options) {
    this.dialog.open(TechUsageTechnicianDetailListModalComponent, {
      width: window.innerWidth - 100 + 'px',
      height: window.innerHeight - 80 + 'px',
      role: 'dialog',
      data: {params: options, startDate: this.page.filterParams['startDate'], endDate: this.page.filterParams['endDate'], isMTD: this.isMTD},
    });
  }
}
