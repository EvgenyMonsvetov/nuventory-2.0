import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './tech-usage-report.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { TechUsageReportComponent } from './tech-usage-report.component';
import { MasterDataService } from '../../services/master.data.service';
import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';
import { CompaniesCheckboxDropdownModule } from '../../components/companies-checkbox-dropdown/companies-checkbox-dropdown.module';
import { ShopsCheckboxDropdownModule } from '../../components/shops-checkbox-dropdown/shops-checkbox-dropdown.module';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        ReportingFilterModule,
        CompaniesCheckboxDropdownModule,
        ShopsCheckboxDropdownModule
    ],
    providers: [
        MasterDataService
    ],
    declarations: [
        TechUsageReportComponent
    ]
})
export class TechUsageReportModule { }