import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { TechUsageReportComponent } from './tech-usage-report.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: TechUsageReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Tech Usage Report'),
        permissions: {
            only: [permissions.DASHBOARD_READ]
        }
    }
}];