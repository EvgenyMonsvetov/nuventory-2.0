import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './voc-usage-report.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { VocUsageReportComponent } from './voc-usage-report.component';
import { MasterDataService } from '../../services/master.data.service';
import { TreeTableModule } from 'primeng/treetable';
import { TableModule } from 'primeng/table';

import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      TreeTableModule,
      TableModule,
      TreeTableModule,
      ReportingFilterModule
  ],
  providers: [
      MasterDataService
  ],
  declarations: [
      VocUsageReportComponent
  ]
})
export class VocUsageReportModule { }
