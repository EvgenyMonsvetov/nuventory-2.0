import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../model/page';
import { Subscription } from 'rxjs/Rx';
import { TitleService } from '../../services/title.service';
import { MatDialog } from '@angular/material';
import { permissions } from '../../permissions';
import { ActivatedRoute } from '@angular/router';
import { HeaderService } from '../../services/header.service';
import { MasterDataService } from '../../services/master.data.service';
import { formatDate } from '@angular/common';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';

@Component({
  selector: 'app-voc-usage-report',
  templateUrl: './voc-usage-report.component.html',
  styleUrls: ['./voc-usage-report.component.scss'],
  providers: [
    MasterDataService
  ],
  preserveWhitespaces: true
})
export class VocUsageReportComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  formatDate = formatDate;

  @ViewChild('vocUsageReportView') vocUsageReportView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableCaption')  tableCaption:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('footer')        footer:  ElementRef;

  private scrollHeight: string = '50px';

  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private totalRecords: number = 0;
  private disableRunButton: boolean = false;

  private loading: boolean = false;
  private currentTreeLevel: number = 0;

  private headerServiceSubscription: Subscription;
  private vocDataSubscription: Subscription;
  private subscriptionSidebarNav: Subscription;

  constructor(private masterDataService: MasterDataService,
              private titleService: TitleService,
              private headerService: HeaderService,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private appSidebarNavService: AppSidebarNavService) {
    this.page.pageNumber = 0;
    this.page.filterParams = {};
    this.page.filterParams['START_DATE'] = this.appSidebarNavService.fromDate;
    this.page.filterParams['END_DATE'] = this.appSidebarNavService.toDate;
    this.page.filterParams['TREE_ENABLED'] = 1;
    this.page.filterParams['isVOC'] = 1;
    this.page.filterParams['isCentral'] = this.headerService.getCurrentLocation() && this.headerService.getCurrentLocation()['LO1_CENTER_FLAG'] ? parseInt(this.headerService.getCurrentLocation()['LO1_CENTER_FLAG']) : 0;

    this.headerServiceSubscription = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.page.filterParams['isCentral'] = this.headerService.getCurrentLocation() && this.headerService.getCurrentLocation()['LO1_CENTER_FLAG'] ? parseInt(this.headerService.getCurrentLocation()['LO1_CENTER_FLAG']) : 0;
      }
    });

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.page.filterParams['START_DATE'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['END_DATE'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
      }
    });
  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['START_DATE']);
    const toDate = new Date(this.page.filterParams['END_DATE']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableRunButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableRunButton = false;
    }
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'TREE_HEADER',       header: _lang('#Group#'),             visible: true, export: {visible: true, checked: true}, width: 200 },
      { field: 'TE1_NAME',          header: _lang('TE1_NAME'),            visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MD1_DESC1',         header: _lang('#CoatingCategory#'),   visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MD1_PART_NUMBER',   header: _lang('#CoatingID#'),         visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'MD1_VOC_CATA',      header: _lang('MD1_VOC_CATA'),        visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'MD1_VOC_MFG',       header: _lang('MD1_VOC_MFG'),         visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'COATING',           header: _lang('#Coating#'),           visible: true, export: {visible: true, checked: true}, width: 60, align: 'right' },
      { field: 'BY_WEIGHT',         header: _lang('BY_WEIGHT'),           visible: true, export: {visible: true, checked: true}, width: 60, align: 'right' },
      { field: 'MD1_VOC_VALUE',     header: _lang('MD1_VOC_VALUE'),       visible: true, export: {visible: true, checked: true}, width: 60, align: 'right' },
      { field: 'MD1_VOC_UNIT_NAME', header: _lang('MD1_VOC_UNIT_NAME'),   visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'MD1_TOTAL_QTY',     header: _lang('MD1_TOTAL_QTY'),       visible: true, export: {visible: true, checked: true}, width: 60, align: 'right' },
      { field: 'MD1_TOTAL_HITS',    header: _lang('MD1_TOTAL_HITS'),      visible: true, export: {visible: true, checked: true}, width: 60, align: 'right' }
    ];
  }

  ngOnDestroy() {
    if (this.vocDataSubscription) {
      this.vocDataSubscription.unsubscribe();
    }
    if (this.headerServiceSubscription) {
      this.headerServiceSubscription.unsubscribe();
    }
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('VOC Usage Report'),
        componentRef: this,
        data: [],
        componentName: 'VocUsageReportComponent'
      },
    });
  }

  getScrollHeight(): number {
    return (this.vocUsageReportView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableCaption.nativeElement.offsetHeight + this.footer.nativeElement.offsetHeight);
  }

  getVocData(data: any = null) {
    this.loading = true;
    let level = 0;
    this.cleanFilters();

    if (data) {
      level = parseInt(data.TREE_LEVEL) + 1;

      switch (level) {
        case 3:
          this.page.filterParams['MD1_ID'] = data.MD1_ID;
        case 2:
          this.page.filterParams['LO1_ID'] = data.LO1_ID;
        case 1:
          this.page.filterParams['CO1_ID'] = data.CO1_ID;
          break;
      }
    }

    this.page.filterParams['TREE_LEVEL'] = level;
    this.currentTreeLevel = level;

    if (this.vocDataSubscription) {
      this.vocDataSubscription.unsubscribe();
    }

    this.vocDataSubscription = this.masterDataService.getVOCUsageReport(this.page).subscribe(res => {
          this.setItems(res['data']);
          if (level === 0) {
            this.totalRecords = res['data'].length;
          }
          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('VOC Usage Report') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  setItems(items) {
    let records: any = items.map(record => {
      let item:object = {};
      item['data'] = record;
      item['expanded'] = false;
      item['leaf'] = this.currentTreeLevel < 3 ? false : true;
      return item;
    });

    if (this.currentTreeLevel === 0) {
      this.items = records;
    } else {
      this.selected['children'] = records;
    }

    this.items = [... this.items];
  }

  onPdfPrint() {
    this.cleanFilters();
    this.page.filterParams['systemMethod'] = 'getpdf';

    this.masterDataService.printUsage(this.page).subscribe( result => {
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  cleanFilters() {
    this.page.filterParams['TREE_LEVEL'] = 1;
    delete this.page.filterParams['systemMethod'];
    delete this.page.filterParams['CO1_ID'];
    delete this.page.filterParams['LO1_ID'];
    delete this.page.filterParams['MD1_ID'];
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe(columns => {
      if (columns) {
        this.cleanFilters();
        this.page.filterParams['columns'] = columns;
        this.page.filterParams['systemMethod'] = 'getexcel';

        this.masterDataService.downloadXlsx(this.page, 'voc_usage').subscribe(result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  onNodeExpand(e) {
    if (e.node.expanded && (!e.node.children || e.node.children.length === 0)) {
      this.selected = e.node;
      this.getVocData(e.node.data);
    }
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  searchButtonPressed () {
    this.filterChanged('SEARCH', this.page.filterParams['SEARCH']);
  }

  filterChanged(col, value) {
    this.page.filterParams[col] = value;
    this.getVocData();
  }
}
