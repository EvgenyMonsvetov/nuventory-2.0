import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VocUsageReportComponent } from './voc-usage-report.component';

describe('VocUsageReportComponent', () => {
  let component: VocUsageReportComponent;
  let fixture: ComponentFixture<VocUsageReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VocUsageReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VocUsageReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
