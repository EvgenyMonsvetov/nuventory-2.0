import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { VocUsageReportComponent } from './voc-usage-report.component';

export const routes: Routes = [{
    path: '',
    component: VocUsageReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('VOC Usage'),
        permissions: {
            only: [permissions.VOC_USAGE_READ]
        }
    }
}];