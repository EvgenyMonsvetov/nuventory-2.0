import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { MatDialog} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Page } from '../../model/page';
import { TechnicianService } from '../../services/technician.service';
import { Subscription ,  Subject } from 'rxjs';
import { HeaderService } from '../../services/header.service';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'app-technicians',
  templateUrl: './technicians.component.html',
  styleUrls: ['./technicians.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class TechniciansComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('techniciansView')   techniciansView: ElementRef;
  @ViewChild('actionbar')         actionbar:  ElementRef;
  @ViewChild('tableHeader')       tableHeader:  ElementRef;
  @ViewChild('tableFilter')       tableFilter: ElementRef;
  @ViewChild('paginator')         paginator:  ElementRef;

  private scrollHeight: string = '50px';

  /** control for the selected filters */
  public companyCtrl: FormControl = new FormControl();
  public companyFilterCtrl: FormControl = new FormControl();
  public locationByCtrl: FormControl = new FormControl();
  public locationByFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyCompany = new Subject<void>();
  private _onDestroyLocationBy = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredCompanies: any[] = [];
  public filteredShopsBy: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  private cols: any[] = [];
  private items: any[] = [];
  private companies: Array<object> = [];
  private shops: Array<object> = [];

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private loading: boolean = true;
  private subscription: Subscription;
  private sub: Subscription;
  selected: any;
  private flagsActive: any[] = [];

  constructor(private technicianService: TechnicianService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    this.sub = this.headerService.subscribe({
        next: (v) => {
          this.page.pageNumber = 0;
          this.page.filterParams = {};
          this.loading = true;
          this.getTechnicians();
        }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.page.filterParams['TE1_ACTIVE'] = '1';


    this.cols = [
      { field: 'CO1_NAME',        header: _lang('CO1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'LO1_NAME',        header: _lang('LO1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'TE1_SHORT_CODE',  header: _lang('TE1_SHORT_CODE'),  visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'TE1_ACTIVE',      header: _lang('Active'),          visible: true, export: {visible: true, checked: true}, width: 50 },
      { field: 'TE1_NAME',        header: _lang('TE1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'US1_NAME',        header: _lang('US1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'TE1_BARCODE',     header: _lang('Barcode'),         visible: true, export: {visible: false, checked: true}, width: 120 }
      // { field: 'CO1_SHORT_CODE',  header: _lang('CO1_SHORT_CODE'),  visible: true },
      // { field: 'LO1_SHORT_CODE',  header: _lang('LO1_SHORT_CODE'),  visible: true },
    ];

    this.flagsActive = [
      { label: _lang('#AllRecords#'), value: ''},
      { label: _lang('Yes'),          value: '1'},
      { label: _lang('No'),           value: '0'}
    ];

    this.technicianService.preloadData().subscribe( data => {
      this.prepareData(data);
      this.getTechnicians();
    });

    this.companyFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCompany))
        .subscribe(() => {
          this.filterCompanies();
        });
    this.locationByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocationBy))
        .subscribe(() => {
          this.filterShopsBy();
        });
  }

  private filterCompanies() {
    if (!this.companies) {
      return;
    }
    // get the search keyword
    let search = this.companyFilterCtrl.value;
    if (!search) {
      this.filteredCompanies = this.companies;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the companies
    this.filteredCompanies = this.companies.filter(company => company['CO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterShopsBy() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationByFilterCtrl.value;
    if (!search) {
      this.filteredShopsBy = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location by
    this.filteredShopsBy = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
    this.sub.unsubscribe();
    this._onDestroyCompany.next();
    this._onDestroyCompany.complete();
    this._onDestroyLocationBy.next();
    this._onDestroyLocationBy.complete();
  }

  getScrollHeight(): number{
    return (this.techniciansView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getTechnicians() {
    this.loading = true;

    if (this.subscription)
      this.subscription.unsubscribe();

    this.subscription = this.technicianService.getTechnicians(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px'});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Technicians') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  prepareData(data) {
    this.companies = [{CO1_ID: null, CO1_NAME: _lang('#AllRecords#')}];
    let companiesArr = data.companies.map( company => {
      return {
        CO1_ID:   company.CO1_ID,
        CO1_NAME: company.CO1_NAME
      }
    }).filter( company => {
      return company.CO1_NAME && company.CO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.CO1_NAME < b.CO1_NAME)
        return -1;
      if (a.CO1_NAME > b.CO1_NAME)
        return 1;
      return 0;
    });
    this.companies = this.companies.concat(companiesArr);
    this.filterCompanies();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);
    this.filterShopsBy();
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Technician List'),
        componentRef: this,
        data: [],
        componentName: 'TechniciansComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getTechnicians();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    this.page.filterParams[col] = value;
    this.page.pageNumber = 0;
    this.getTechnicians();
  }

  onAddNewTechnician() {
    this.router.navigate(['technicians/add/0']);
  }

  onCloneTechnician() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['technicians/clone/' + this.selected[0]['TE1_ID']], {queryParams: {clone: true}});
  }

  editRecord() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['technicians/edit/' + this.selected[0]['TE1_ID']]);
  }

  onEditTechnician(rowData) {
    this.router.navigate(['technicians/edit/' + rowData['TE1_ID']]);
  }

  onDeleteTechnician(event) {
      if (this.selected.length && this.selected[0]['TE1_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.technicianService.delete(this.selected[0]['TE1_ID']).subscribe( result => {
                this.getTechnicians();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete technician error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.technicianService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  onPdfPrint() {
    if (this.selected && this.selected.length) {
      const TE1_IDs: any[] = [];
      this.selected.forEach(function (item) {
        TE1_IDs.push(item['TE1_ID']);
      });
      this.page.filterParams['TE1_IDs'] = TE1_IDs;

      this.technicianService.printPDF(this.page).subscribe( result => {
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
      this.page.filterParams['TE1_IDs'] = null;
    }
  }

}
