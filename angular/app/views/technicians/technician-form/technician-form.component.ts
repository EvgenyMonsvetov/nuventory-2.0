import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { TechnicianService } from '../../../services/technician.service';
import { permissions } from '../../../permissions';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { CompanyFinderModalComponent } from '../../../components/company-finder-modal/company-finder-modal.component';
import { ShopFinderModalComponent } from '../../../components/shop-finder-modal/shop-finder-modal.component';
import { _lang } from '../../../pipes/lang';
import { HourService } from '../../../services/hour.service';
import { Page } from '../../../model/page';
import { Subscription } from 'rxjs/Rx';
import { ToastrService } from 'ngx-toastr';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import moment from 'moment';
import * as _ from 'lodash';
import { Location } from '@angular/common';
import { HeaderService } from '../../../services/header.service';
import {ConfirmationService} from "primeng/api";
import {Constants} from "../../../constants";

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-technician-add',
  templateUrl: './technician-form.component.html',
  styleUrls: ['./technician-form.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    TechnicianService,
    HourService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})

export class TechnicianFormComponent implements OnInit, OnDestroy {
  private errors: Array<any> = [];
  private hourErrors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private record: any;
  private user: any;
  private hour: any;
  private cols: any[] = [];
  private hours: any[] = [];
  private page = new Page();
  private technicianSubscription: Subscription;
  private hourSubscription: Subscription;
  private loading: boolean = true;
  private selectedHour: any;
  private selectedTab: number = 0;
  private acceptLabel: string = _lang('Yes');
  private confirmationHeader: string = _lang('DELETE CONFIRMATION');
  private constants = Constants;

  private hourDate = new FormControl(moment());

  constructor(private route: ActivatedRoute,
              private router: Router,
              private technicianService: TechnicianService,
              private confirmationService: ConfirmationService,
              private hourService: HourService,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private elementRef: ElementRef,
              private titleService: TitleService,
              private location: Location,
              private headerService: HeaderService) {
    this.page.filterParams = {};
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.record = this.technicianService.initialize();
    this.action = this.route.snapshot.url[0].toString();
    this.page.filterParams['TE1_ID'] = this.route.snapshot.params.id || 0;
    this.hour = this.hourService.initialize();

    if (this.route.snapshot.queryParams.openHoursTab) {
      setTimeout(() => { this.selectedTab = 1; }, 100);
    }

    this.cols = [
      { field: 'BTN_DELETE',      header: '',                     visible: true, width: 32},
      { field: 'HO1_HOURS',       header: _lang('Hours'),         visible: true},
      { field: 'HO1_DATE',        header: _lang('#Date#'),        visible: true},
      { field: 'HO1_CREATED_ON',  header: _lang('#CreatedOn#'),   visible: true},
      { field: 'CREATED_BY',      header: _lang('#CreatedBy#'),   visible: true},
      { field: 'HO1_MODIFIED_ON', header: _lang('#ModifiedOn#'),  visible: true},
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),  visible: true},
    ];

    if (this.route.snapshot.params.id > 0) {
      this.technicianSubscription = this.technicianService.getById(this.route.snapshot.params.id).subscribe( result => {
        if (result[0]) {
          let data = result[0];
          data['TE1_ID'] = parseInt(data['TE1_ID']);
          data['US1_ALLOW_MOBILE_JOB_CREATE'] = parseInt(data['US1_ALLOW_MOBILE_JOB_CREATE']);
          data['TE1_BODY'] = parseInt(data['TE1_BODY']);
          data['TE1_DETAIL'] = parseInt(data['TE1_DETAIL']);
          data['TE1_PAINT'] = parseInt(data['TE1_PAINT']);
          data['TE1_ACTIVE'] = parseInt(data['TE1_ACTIVE']);
          this.record = data;
          if (this.action === 'clone')
            this.record['TE1_ID'] = 0;
        }
      });
    } else {
      if (this.headerService.CO1_ID > 0) {
        this.record.CO1_ID = this.headerService.CO1_ID;
        this.record.CO1_NAME = this.headerService.CO1_NAME;
      }
      if (this.headerService.LO1_ID > 0) {
        this.record.LO1_ID = this.headerService.LO1_ID;
        this.record.LO1_NAME = this.headerService.LO1_NAME;
      }
      this.technicianSubscription = this.technicianService.getLastShortCode().subscribe( data => {
        if (data && data.hasOwnProperty('TE1_SHORT_CODE') && parseInt(data['TE1_SHORT_CODE']) > 0) {
          this.record.TE1_SHORT_CODE = parseInt(data['TE1_SHORT_CODE']) + 1;
        }
      });
    }

    this.getHours();
  }

  ngOnDestroy() {
    if (this.technicianSubscription) {
      this.technicianSubscription.unsubscribe();
    }
    this.hourSubscription.unsubscribe();
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Technician Form'),
        componentRef: this,
        data: [],
        componentName: 'TechnicianFormComponent'
      },
    });
  }

  saveClick() {
    this.record['user'] = {};
    this.record['user']['US1_LOGIN'] = this.dbEncode(this.record['US1_LOGIN']);
    this.record['user']['US1_PASS']  = this.dbEncode(this.record['US1_PASS']);
    this.record['user']['US1_ALLOW_MOBILE_JOB_CREATE'] = this.record['US1_ALLOW_MOBILE_JOB_CREATE'] ? '1' : '0';
    this.record['user']['US1_NAME']  = this.dbEncode(this.record['TE1_NAME']);
    this.record['user']['CO1_ID']    = this.record['CO1_ID'];
    this.record['user']['LO1_ID']    = this.record['LO1_ID'];
    this.record['user']['DELETED']   = 0;
    this.record['user']['US1_LOGIN'] = this.record['US1_LOGIN'];
    this.record['TE1_BODY'] = this.record['TE1_BODY'] ? 1 : 0;
    this.record['TE1_DETAIL'] = this.record['TE1_DETAIL'] ? 1 : 0;
    this.record['TE1_PAINT'] = this.record['TE1_PAINT'] ? 1 : 0;
    this.record['TE1_ACTIVE'] = this.record['TE1_ACTIVE'] ? 1 : 0;
    if (this.action === 'add' || this.action === 'clone') {
      this.technicianService.create(this.record).subscribe(result => {
        if (result.success) {
          this.record['TE1_ID'] = result['TE1_ID'];
          this.saveHours();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.technicianService.update(this.record).subscribe(result => {
        if (result.success) {
          this.record['TE1_ID'] = result['TE1_ID'];
          this.saveHours();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

  saveHours() {
    const hoursToSave = this.hours.filter( item => {
      return item['EDITED'] === true;
    }).map( item => {
      item['TE1_ID'] = this.record['TE1_ID'];
      item['CO1_ID'] = this.record['CO1_ID'];
      item['LO1_ID'] = this.record['LO1_ID'];
      item['HO1_DATE'] = item['HO1_DATE_YMD'];
      return item;
    });

    this.hourService.save(hoursToSave).subscribe(result => {
      if (result.success) {
        this.cancelClick();
      } else {
        this.hourErrors = result.errors;
      }
    });
  }

  selectCompany() {
    let dialogRef = this.dialog.open(CompanyFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Company'), CO1_ID: this.record.CO1_ID },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if(selected){
        this.record.CO1_NAME = selected.CO1_NAME;
        this.record.CO1_ID   = selected.CO1_ID;
      }
    })
  }

  selectShop() {
    let dialogRef = this.dialog.open(ShopFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {
        title: _lang('Select Shop'),
        filters: {
          CO1_ID: this.record.CO1_ID
        },
        HIGHLIGHT_LO1_ID: this.record.LO1_ID,
        componentRef: this.elementRef
      },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.LO1_ID = selected['LO1_ID'];
        this.record.LO1_SHORT_CODE = selected['LO1_SHORT_CODE'];
        this.record.LO1_NAME = selected['LO1_NAME'];
      }
    })
  }

  clearCompany() {
    this.record.CO1_NAME = '';
    this.record.CO1_ID   = '';
  }

  clearShop() {
    this.record.LO1_ID = 0;
    this.record.LO1_SHORT_CODE = '';
    this.record.LO1_NAME = '';
  }

  dbEncode(str) {
    if (str == null)
      return "";
    var rtv:String = str;
    rtv = rtv.replace(/^[ \t]+|[ \t]+$/g, '');
    rtv = rtv.replace(/\r/gm, "\n");
    rtv = rtv.replace(/[']/g, "\\'");
    rtv = rtv.replace(/[=]/g, "\\=");
    rtv = rtv.replace(/[;]/g, "\|");
    return rtv;
  }

  chosenYearHandler(normalizedYear: moment.Moment) {
    const ctrlValue = this.hourDate.value;
    ctrlValue.year(normalizedYear.year());
    this.hourDate.setValue(ctrlValue);
    this.formatHourDate();
  }

  chosenMonthHandler(normalizedMonth: moment.Moment, datepicker: MatDatepicker<moment.Moment>) {
    const ctrlValue = this.hourDate.value;
    ctrlValue.month(normalizedMonth.month());
    ctrlValue.year(normalizedMonth.year());
    this.hourDate.setValue(ctrlValue);
    datepicker.close();
    this.formatHourDate();
  }

  dateChangeHandler(e) {
    this.formatHourDate();
  }

  formatHourDate() {
    const datePipe = new DatePipe('en-US');
    let date = this.hourDate.value._d;
    date.setFullYear(date.getFullYear(), date.getMonth() + 1, 0);
    this.hour['HO1_DATE'] = datePipe.transform(date, 'MM.dd.yyyy');
    this.hour['HO1_DATE_YMD'] = datePipe.transform(date, 'yyyy-MM');
  }

  onEditInit( $event ) {
    var interval = setInterval(() => {
      const input = <HTMLInputElement>document.getElementById('input_hours_' + $event.data.HO1_ID);
      if (input) {
        clearInterval(interval);
        interval = null;
        input.select();
      }
    }, 5);
  }

  hoursChanged(rowData, e) {
    rowData['EDITED'] = true;
  }

  addHourClick() {
    this.hour['EDITED'] = true;
    if (!this.hour['HO1_DATE']) {
      this.formatHourDate();
    }

    if (this.validateHour()) {
      this.hourErrors = [];
      this.hours.push(_.clone(this.hour));
      this.hour = this.hourService.initialize();
      this.hourDate = new FormControl(moment());

      this.hours.sort((a, b) => {
        const aDate = new Date(a.HO1_DATE);
        const bDate = new Date(b.HO1_DATE);
        if ( aDate > bDate) {
          return -1;
        }
        if (aDate < bDate) {
          return 1;
        }
        return 0;
      });
    } else {
      this.hourErrors = [_lang('Record with choosen date already exist')];
    }
  }

  validateHour() {
    const filteredHours = this.hours.filter( item => {

      return new Date(item['HO1_DATE']).getFullYear() === new Date(this.hour['HO1_DATE']).getFullYear() &&
          new Date(item['HO1_DATE']).getMonth() === new Date(this.hour['HO1_DATE']).getMonth();
    });

    return !filteredHours.length;
  }

  dateFormat(date){
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let explodeDate = date.split(".");
    return explodeDate[2]+'-'+months[ parseInt(explodeDate[0])-1];
  }

  deleteClick(event) {
    if (this.selectedHour['HO1_ID']) {
      this.confirmationService.confirm({
        message: _lang('Are you sure that you want to perform this action?'),
        rejectVisible: true,
        accept: () => {
          this.loading = true;
          this.hourSubscription = this.hourService.delete(this.selectedHour['HO1_ID']).subscribe( result => {
            this.selectedHour = 0;
            this.getHours();
          },
          error => {
            this.toastr.error(_lang('On delete an hour error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
        }
      });
    }else{
      var me = this;
      this.hours = this.hours.filter( (hour)=>{
          return hour['HO1_HOURS'] == me.selectedHour['HO1_HOURS'] &&
                 hour['HO1_DATE']  == me.selectedHour['HO1_DATE'] ? false : true;
      });
    }
  }

  getHours() {
    this.hourSubscription = this.hourService.getHours(this.page).subscribe(res => {
      this.hours = res['data'].map( record => {
        record['HO1_ID'] = parseInt(record['HO1_ID']);
        record['HO1_HOURS'] = parseFloat(record['HO1_HOURS']);
        return record;
      });

      this.loading = false;
    },
    error => {
      this.loading = false;
      this.toastr.error(_lang('Hours') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }
}
