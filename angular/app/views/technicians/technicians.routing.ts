import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { TechniciansComponent } from './technicians.component';
import { TechnicianFormComponent } from './technician-form/technician-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: TechniciansComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Technicians'),
        permissions: {
            only: [permissions.TECHNICIANS_READ]
        }
    }
},{
    path: 'add/:id',
    component: TechnicianFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.TECHNICIANS_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: TechnicianFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.TECHNICIANS_EDIT]
        }
    }
},{
    path: 'clone/:id',
    component: TechnicianFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.TECHNICIANS_ADD]
        }
    }
}];