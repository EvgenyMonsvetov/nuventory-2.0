import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RouterModule } from '@angular/router';
import { routes } from './technicians.routing';
import { TechnicianService } from '../../services/technician.service';
import { TechniciansComponent } from './technicians.component';
import { TechnicianFormComponent } from './technician-form/technician-form.component';
import { SharedModule } from '../../modules/shared.module';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      MomentDateModule,
      MatDatepickerModule
  ],
  providers: [
      TechnicianService
  ],
  declarations: [
      TechniciansComponent,
      TechnicianFormComponent
  ],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA,
      NO_ERRORS_SCHEMA
  ]
})
export class TechniciansModule { }
