import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { HighBarChartReportComponent } from './high-bar-chart-report.component';

export const routes: Routes = [{
    path: '',
    component: HighBarChartReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard]
}];