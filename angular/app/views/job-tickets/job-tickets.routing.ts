import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { JobTicketsComponent } from './job-tickets.component'
import { JobTicketViewComponent } from './job-ticket-view/job-ticket-view.component'
import { JobTicketAddComponent } from './job-ticket-add/job-ticket-add.component';


export const routes: Routes = [{
    path: '',
    component: JobTicketsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Job Tickets'),
        permissions: {
            only: [permissions.JOB_TICKETS_READ]
        }
    }
},{
    path: 'view/:id',
    component: JobTicketViewComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.JOB_TICKETS_EDIT]
        }
    }
},{
    path: 'add/:id',
    component: JobTicketAddComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Create Job Tickets'),
        permissions: {
            only: [permissions.JOB_TICKETS_ADD]
        }
    }
}];