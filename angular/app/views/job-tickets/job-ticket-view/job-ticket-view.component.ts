import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { JobTicketService } from '../../../services/job-ticket.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { Location } from '@angular/common';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { environment } from '../../../../environments/environment';
import { _lang } from '../../../pipes/lang';
import { permissions } from '../../../permissions';
import { SelectCountriesComponent } from '../../../components/select-countries/select-countries.component';
import { CustomerFinderModalComponent } from '../../../components/customer-finder-modal/customer-finder-modal.component';
import { Page } from '../../../model/page';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { JobLineModalComponent } from '../../../components/job-line-modal/job-line-modal.component';
import { JobLineService } from '../../../services/job-line.service';
import { ConfirmationService } from 'primeng/api';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-job-ticket-view',
  templateUrl: './job-ticket-view.component.html',
  styleUrls: ['./job-ticket-view.component.scss'],
  providers: [
    JobTicketService,
    JobLineService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class JobTicketViewComponent implements OnInit {
  @ViewChild('contentGrid') contentGrid: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private errors: Array<any> = [];
  private action: String = '';
  private constants = Constants;
  private permissions = permissions;
  private record: any;
  private apiUrl = environment.API_URL;

  private statusData: Array<any> = [
    {'label': _lang('#Open#'), 'JT1_STATUS' : 0},
    {'label': _lang('#Received#'), 'JT1_STATUS' : 1}
  ];

  private newCustomer = '0';
  private status;

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[] = [];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private loading: boolean = true;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private jobTicketService: JobTicketService,
              private jobLineService: JobLineService,
              private confirmationService: ConfirmationService,
              public  dialog: MatDialog,
              private elementRef: ElementRef,
              private titleService: TitleService,
              private toastr: ToastrService,
              private location: Location) {
    this.page.filterParams = {};
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      // { field: 'JT1_NUMBER',              header: _lang('JT1_NUMBER'),        visible: true, width: 55 },
      { field: 'JT2_LINE',                header: _lang('Line #'),            visible: true, width: 30 },
      { field: 'JT2_STATUS_TEXT',         header: _lang('JT1_STATUS_TEXT'),   visible: true, width: 70 },
      { field: 'TE1_NAME',                header: _lang('TE1_NAME'),          visible: true, width: 100 },
      { field: 'JT2_PART_NUMBER',         header: _lang('IJ2_PART_NUMBER'),   visible: true, width: 70 },
      { field: 'GL1_NAME',                header: _lang('GL1_ID'),            visible: true, width: 70 },
      { field: 'JT2_DESC1',               header: _lang('GL1_DESCRIPTION'),   visible: true, width: 140 },
      { field: 'UM1_RECEIVE_NAME',        header: _lang('UM1_RECEIVE_NAME'),  visible: true, width: 70 },
      { field: 'JT2_ORDER_QTY',           header: _lang('JT2_ORDER_QTY'),     visible: true, width: 50 },
      { field: 'UM1_NAME',                header: _lang('MD1_UM'),            visible: true, width: 80 },
      { field: 'JT2_UNIT_PRICE',          header: _lang('IN2_UNIT_PRICE'),    visible: true, width: 70 },
      { field: 'JT2_TOTAL_VALUE',         header: _lang('JT1_TOTAL_VALUE'),   visible: true, width: 78 },
      // { field: 'MODIFIED_ON',             header: _lang('#ModifiedOn#'),      visible: true, width: 120 },
      // { field: 'MODIFIED_BY',             header: _lang('#ModifiedBy#'),      visible: true, width: 120 },
      { field: 'CREATED_ON',              header: _lang('#CreatedOn#'),       visible: true, width: 120 },
      { field: 'CREATED_BY',              header: _lang('#CreatedBy#'),       visible: true, width: 100 }
    ];

    this.record = this.jobTicketService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.page.filterParams['JT1_ID'] = this.route.snapshot.params.id;
      this.getItems();
      this.getOrder();
    }
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.jobLineService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Job lines') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  getOrder() {
    this.jobTicketService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
      if (result[0]) {
        let data = result[0];
        data['JT1_ID'] = parseInt(data['JT1_ID']);
        data['JT1_STATUS'] = parseInt(data['JT1_STATUS']);
        data['VD1_ID'] = parseInt(data['VD1_ID']);
        data['CO1_ID'] = parseInt(data['CO1_ID']);
        data['LO1_ID'] = parseInt(data['LO1_ID']);
        this.newCustomer = parseInt(data['CU1_ID']) > 0 ? '1' : '0';
        this.record = data;
      }
    });
  }

  selectCustomer() {
    let dialogRef = this.dialog.open(CustomerFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Customer'), CU1_ID: this.record.CU1_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CU1_ID = selected['CU1_ID'];
        this.record.CU1_SHORT_CODE = selected['CU1_SHORT_CODE'];
        this.record.CU1_NAME = selected['CU1_NAME'];
        this.record.CU1_ADDRESS1 = selected['CU1_ADDRESS1'];
        this.record.CU1_ADDRESS2 = selected['CU1_ADDRESS2'];
        this.record.CU1_CITY = selected['CU1_CITY'];
        this.record.CU1_STATE = selected['CU1_STATE'];
        this.record.CU1_ZIP = selected['CU1_ZIP'];
        this.record.CU1_PHONE = selected['CU1_PHONE'];
        this.record.CU1_FAX = selected['CU1_FAX'];
        this.record.CY1_ID = selected['CY1_ID'];
        this.record.CY1_NAME = selected['CY1_NAME'];
      }
    })
  }

  customerChangeRB(value) {
    if (value === '0') {
      this.clearCustomer();
    }
  }

  clearCustomer() {
    this.record.CU1_ID = 0;
    this.record.CU1_SHORT_CODE = '';
    this.record.CU1_NAME = '';
    this.record.CU1_ADDRESS1 = '';
    this.record.CU1_ADDRESS2 = '';
    this.record.CU1_CITY = '';
    this.record.CU1_STATE = '';
    this.record.CU1_ZIP = '';
    this.record.CU1_PHONE = '';
    this.record.CU1_FAX = '';
  }

  customerNameChanged() {
    this.record.CU1_ID = 0;
    this.record.CU1_SHORT_CODE = '';
  }

  selectCountry() {
    let dialogRef = this.dialog.open(SelectCountriesComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Country'), CY1_ID: this.record.CY1_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CY1_ID = selected['CY1_ID'];
        this.record.CY1_SHORT_CODE = selected['CY1_SHORT_CODE'];
        this.record.CY1_NAME = selected['CY1_NAME'];
      }
    })
  }

  clearCountry() {
    this.record.CY1_ID = 0;
    this.record.CY1_SHORT_CODE = '';
    this.record.CY1_NAME = '';
  }

  cancelClick() {
    this.location.back();
  }


  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Job Ticket Form'),
        componentRef: this,
        data: [],
        componentName: 'JobTicketViewComponent'
      },
    });
  }

  saveClick() {
    if (this.action === 'add') {
      this.jobTicketService.create(this.record).subscribe(result => {
        if (result.success) {
          this.toastr.success(_lang('Job Ticket was successfully created!'), _lang('Service Success'), {
            timeOut: 3000,
          });
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'view') {
      this.loading = true;
      this.jobTicketService.update(this.record).subscribe(result => {
        if (result.success) {
          this.toastr.success(_lang('Job Ticket was successfully updated!'), _lang('Service Success'), {
            timeOut: 3000,
          });
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
        this.loading = false;
      });
    }
  }

  onOpen(rowData) {
    this.showJobTicketForm(rowData['JT2_ID']);
  }

  openRecord() {
    if (this.selected) {
      this.showJobTicketForm(this.selected['JT2_ID']);
    }
  }

  showJobTicketForm(lineId) {
    let dialogRef = this.dialog.open(JobLineModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Job Line'), JT2_ID: lineId, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( value => {
      if (value) {
        this.getItems();
      }
    });
  }

  onDelete(event) {
    if (this.selected && this.selected['JT2_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.jobLineService.delete(this.selected['JT2_ID']).subscribe( result => {
                this.selected = null;
                this.getItems();
                this.getOrder();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete job ticket line error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

}
