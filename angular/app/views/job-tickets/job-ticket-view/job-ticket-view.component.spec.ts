import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTicketViewComponent } from './job-ticket-view.component';

describe('JobTicketViewComponent', () => {
  let component: JobTicketViewComponent;
  let fixture: ComponentFixture<JobTicketViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTicketViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTicketViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
