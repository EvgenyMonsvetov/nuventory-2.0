import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './job-tickets.routing';
import { JobTicketService } from '../../services/job-ticket.service';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MatRadioModule } from '@angular/material/radio';
import { MatAutocompleteModule } from '@angular/material';
import { JobTicketsComponent } from './job-tickets.component';
import { JobTicketViewComponent } from './job-ticket-view/job-ticket-view.component';
import { JobTicketAddComponent } from './job-ticket-add/job-ticket-add.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { CurrencyModule } from '../../components/currency/currency.module';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        MatRadioModule,
        MatAutocompleteModule,
        NgxMatSelectSearchModule,
        CurrencyModule
    ],
    providers: [
        JobTicketService
    ],
    declarations: [
        JobTicketsComponent,
        JobTicketViewComponent,
        JobTicketAddComponent
    ]
})
export class JobTicketsModule { }