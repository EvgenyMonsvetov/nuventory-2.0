import { Component, ElementRef, Inject, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { TechnicianFinderModalComponent } from '../../../components/technician-finder-modal/technician-finder-modal.component';
import { Page } from '../../../model/page';
import { permissions } from '../../../permissions';
import { MasterDataService } from '../../../services/master.data.service';
import { MatDialog, MatSelect } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { TitleService } from '../../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { JobTicketService } from '../../../services/job-ticket.service';
import { _lang } from '../../../pipes/lang';
import { FormControl } from '@angular/forms';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { CellEditor } from 'primeng/table';
import { DOCUMENT } from '@angular/common';
import { AddCatalogListModalComponent } from '../../../components/add-catalog-list-modal/add-catalog-list-modal.component';
import { HeaderService } from '../../../services/header.service';
import { Subscription ,  ReplaySubject, Subject } from 'rxjs';
import { MasterDataModalComponent } from '../../../components/master-data-modal/master-data-modal.component';
import { SelectCountriesComponent } from '../../../components/select-countries/select-countries.component';
import { CustomerFinderModalComponent } from '../../../components/customer-finder-modal/customer-finder-modal.component';
import { take, takeUntil } from 'rxjs/operators';
import * as _ from 'lodash';
import {CustomerService} from "../../../services/customer.service";
import {Constants} from "../../../constants";

@Component({
  selector: 'app-job-ticket-add',
  templateUrl: './job-ticket-add.component.html',
  styleUrls: ['./job-ticket-add.component.scss'],
  providers: [
    JobTicketService,
    MasterDataService,
    ConfirmationService,
    CustomerService
  ],
  preserveWhitespaces: true
})
export class JobTicketAddComponent implements OnInit {
  private permissions = permissions;
  private constants = Constants;

  @ViewChild('jobTicketAddView')  jobTicketAddView: ElementRef;
  @ViewChild('contentGrid')       contentGrid:  ElementRef;
  @ViewChild('actionbar')         actionbar:  ElementRef;
  @ViewChild('content')           content: ElementRef;
  @ViewChild('tableHeader')       tableHeader:  ElementRef;
  @ViewChildren(CellEditor)       editors: QueryList<CellEditor>;
  @ViewChild('inputPartNumber')   inputPartNumber: ElementRef;

  cols: any[];
  items: any[] = [];
  parts: any[] = [];
  technicians: any[] = [];
  selected: any;
  private record: any;
  private data: any;
  private errors: Array<any> = [];

  private page = new Page();
  private size: number = 50;

  private loading: boolean = false;
  private processingUserAction = false;
  private mitchellState = false;
  private newCustomer = '0';
  private barcode: string = '';

  searchValue: string = '';
  totalValue: number = 0;

  filteredParts: any[] = [];
  addedParts: any[] = [];
  partNumber: string = '';
  TE1_ID = '';
  TE1_NAME = '';
  existingID = '';

  partsControl = new FormControl();
  private subscription: Subscription;

  selectedJobCCC: any;
  public jobCCCCtrl: FormControl = new FormControl();
  public jobCCCFilterCtrl: FormControl = new FormControl();
  @ViewChild('jobCCC') jobCCC: MatSelect;
  private jobDP: Array<any> = [];

  /** list of jobCCC filtered by search keyword */
  public filteredJobCCC: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  private _onDestroyJobCCC = new Subject<void>();

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  constructor(private masterDataService: MasterDataService,
              private jobTicketService: JobTicketService,
              private confirmationService: ConfirmationService,
              public  dialog: MatDialog,
              private router: Router,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private elementRef: ElementRef,
              private customerService: CustomerService,
              @Inject(DOCUMENT) document,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.record = this.jobTicketService.initialize();

    this.subscription = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.initTicket();
      }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'MD1_ID',                  header: _lang('MD1_ID'),                visible: false, width: 100 },
      { field: 'VD1_ID',                  header: _lang('VD1_ID'),                visible: false, width: 100 },
      { field: 'VD1_SHORT_CODE',          header: _lang('VD1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'VD1_NAME',                header: _lang('VD1_NAME'),              visible: false, width: 100 },
      { field: 'CO1_ID',                  header: _lang('CO1_ID'),                visible: false, width: 100 },
      { field: 'CO1_SHORT_CODE',          header: _lang('CO1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'CO1_NAME',                header: _lang('CO1_NAME'),              visible: false, width: 100 },
      { field: 'LO1_ID',                  header: _lang('LO1_ID'),                visible: false, width: 100 },
      { field: 'LO1_SHORT_CODE',          header: _lang('LO1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'LO1_NAME',                header: _lang('LO1_NAME'),              visible: false, width: 100 },
      { field: 'FI1_ID',                  header: _lang('FI1_ID'),                visible: false, width: 100 },
      { field: 'FI1_NAME',                header: _lang('FI1_NAME'),              visible: false, width: 100 },
      { field: 'TE1_SHORT_CODE',          header: _lang('TE1_SHORT_CODE'),        visible: false,  width: 100 },
      { field: 'TE1_ID',                  header: _lang('TE1_NAME'),              visible: true,  width: 160 },
      { field: 'TE1_NAME',                header: _lang('TE1_NAME'),              visible: false,  width: 100 },
      { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),       visible: true,  width: 100 },
      { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),             visible: true,  width: 100 },
      { field: 'MD1_UNIT_PRICE',          header: _lang('MD1_UNIT_PRICE'),        visible: true,  width: 90  },
      { field: 'UM1_PRICE_NAME',          header: _lang('UM1_PRICE_NAME'),        visible: false, width: 100 },
      { field: 'MD1_ORDER_QTY',           header: _lang('MD1_ORDER_QTY'),         visible: true,  width: 50  },
      { field: 'UM1_PURCHASE_NAME',       header: _lang('UM1_PURCHASE_NAME'),     visible: true,  width: 70  },
      { field: 'RECEIPT_QTY',             header: _lang('QTY'),                   visible: false,  width: 90  },
      { field: 'UM1_RECEIPT_NAME',        header: _lang('UM1_RECEIPT_NAME'),      visible: false,  width: 100 },
      { field: 'MD2_ON_HAND_QTY',         header: _lang('MD2_ON_HAND_QTY'),       visible: true,  width: 70  },
      { field: 'MD2_AVAILABLE_QTY_FROM',  header: _lang('MD2_AVAILABLE_QTY_FROM'),visible: true,  width: 70  },
      { field: 'MD2_MIN_QTY',             header: _lang('MD2_MIN_QTY'),           visible: true,  width: 70  },
      { field: 'MD1_EXTENDED_VALUE',      header: _lang('MD1_EXTENDED_VALUE'),    visible: true,  width: 100 }
    ];

    this.filteredJobCCC
        .pipe(take(1), takeUntil(this._onDestroyJobCCC))
        .subscribe(() => {
          if(this.selectedJobCCC){
            this.selectedJobCCC.compareWith = (a: any, b: any) => a && b && a.CCC_ID === b.CCC_ID;
          }
        });


    this.jobCCCFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyJobCCC))
        .subscribe(() => {
          this.filterJobCCC();
        });

    this.initTicket();
  }

  private filterJobCCC() {
    if (!this.jobDP) {
      return;
    }
    // get the search keyword
    let search = this.jobCCCFilterCtrl.value;
    if (!search) {
      this.filteredJobCCC.next(this.jobDP.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the jobDP
    this.filteredJobCCC.next(
        this.jobDP.filter(job => job.CCC_RO_NUMBER.toLowerCase().indexOf(search) > -1 || job.CCC_YEAR.toLowerCase().indexOf(search) > -1 || job.CCC_MAKE.toLowerCase().indexOf(search) > -1)
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this._onDestroyJobCCC.next();
    this._onDestroyJobCCC.complete();
  }

  initTicket() {
    this.loading = true;
    const result = this.jobTicketService.isCanBeCreated( this.headerService );
    if (!result['canBeCreated']) {
      this.toastr.error(result['error']);
      this.router.navigate(['/job-tickets']);
    } else {
      this.jobTicketService.initTicket(this.page).subscribe( res => {

        // if (!this.TE1_ID || this.TE1_ID === '') {
        //   this.selectTechnician();
        // }

        const CO1_MITCHELL_FLAG = res['company']['CO1_MITCHELL_FLAG'];
        const CO1_CCC_FLAG = res['company']['CO1_CCC_FLAG'];

        if (CO1_MITCHELL_FLAG === 1 || CO1_CCC_FLAG === 1) {
          this.mitchellState = true;
          this.record['JT1_NUMBER'] = '';
        }

        if (this.data && this.data['JT1_ID']) {
          this.existingID = this.data['JT1_ID'];
          this.record['JT1_NUMBER'] = this.data['JT1_NUMBER'];
        } else {
          this.existingID = '';
        }

        if (this.existingID !== '') {
          this.record['CO1_ID'] = this.data['CO1_ID'];
          this.record['LO1_ID'] = this.data['LO1_ID'];
          this.record['CO1_SHORT_CODE'] = this.data['CO1_SHORT_CODE'];
          this.record['CO1_NAME'] = this.data['CO1_NAME'];
          this.record['LO1_SHORT_CODE'] = this.data['LO1_SHORT_CODE'];
          this.record['LO1_NAME'] = this.data['LO1_NAME'];

          this.selectTechnician();
        } else if (res['fromLocation']['LO1_ID'] && res['fromLocation']['LO1_ID'] !== '') {
          this.record['CO1_ID']         = res['company']['CO1_ID'];
          this.record['CO1_SHORT_CODE'] = res['company']['CO1_SHORT_CODE'];
          this.record['CO1_NAME']       = res['company']['CO1_NAME'];

          this.record['LO1_ID']         = res['fromLocation']['LO1_ID'];
          this.record['LO1_SHORT_CODE'] = res['fromLocation']['LO1_SHORT_CODE'];
          this.record['LO1_NAME']       = res['fromLocation']['LO1_NAME'];

          this.inputPartNumber.nativeElement.focus();
        }

        if (res['parts']){
          this.parts = res['parts'];
        }

        this.technicians = [{TE1_NAME: _lang('- Select -'), TE1_ID: 0}];
        if (res['technicians'])
          this.technicians = this.technicians.concat(res['technicians']);

        if (this.mitchellState) {
          this.jobDP = res['repair'];
          this.filterJobCCC();
        }

        this.loading = false;
      });
    }
  }

  filterParts(value: string) {
    const filterValue = value.toLowerCase();
    this.filteredParts = this.parts.filter(part => {
      return part['MD1_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1 ||
          (part['MD1_DESC1'] && (part['MD1_DESC1'].toLowerCase().indexOf(filterValue) > -1)) ||
          (part['MD1_UPC1'] && part['MD1_UPC1'].toLowerCase().indexOf(filterValue) > -1) ||
          (part['MD1_VENDOR_PART_NUMBER'] && part['MD1_VENDOR_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1)
    });
  }

  onKeyUp(event, fieldName){
    this.jobTicketService.clearSaveBarcode( this.record, fieldName );
  }

  partNumberKeyUp(event) {
    if (event.keyCode === 13) {
      this.partNumber = this.partNumber.trim().toLowerCase();
      this.jobTicketService.clearSaveBarcode( this, 'partNumber' );
      this.filterParts(this.partNumber);
      if (this.partNumber && this.partNumber !== '') {
        if (this.filteredParts && this.filteredParts.length > 0) {
          this.onSelectPart(this.filteredParts[0]['MD1_PART_NUMBER']);
          this.filteredParts = [];
        } else {
          this.toastr.error(_lang('No one part number does not match'), _lang('Error'), {
            timeOut: 3000,
          });
        }
      }
    }
  }

  partNumberKeyDown(e){
    if ( e.keyCode === 13 ) {
      var bcRegExp = /^\+?qty(\.?[0-9]+)/i;
      if( bcRegExp.test(this.barcode) ){
        let match = bcRegExp.exec(this.barcode);
        this.addedParts = this.addedParts.map( (part, index) => {
          if(index == (this.addedParts.length - 1)){
            part['MD1_ORDER_QTY'] = (/^\./.test(match[1]) ? '0' : '') + match[1];
          }
          return part;
        });
        this.updateTotals();
        this.partNumber = '';
      }
      this.barcode = '';
    }else{
      if(e.key == '+'){
        this.barcode = '';
      }
      if(e.key != 'Shift'){
        this.barcode += e.key;
      }
    }
  }

  onSelectPart(value) {
    this.partNumber = '';
    this.page.filterParams['LO1_TO_ID']       = this.record.LO1_ID;
    this.page.filterParams['LO1_FROM_ID']     = this.record.LO1_ID;
    this.page.filterParams['MD1_PART_NUMBER'] = value;

    var MD1_IDs = [];
    this.addedParts.forEach(function (part) {
      if (part['MD1_ID']){
        MD1_IDs.push(part['MD1_ID']);
      }
    });

    this.masterDataService.getFullParts(this.page).subscribe(res => {
      this.addPartsToOrder(res['data']);
    },
    error => {
      this.toastr.error(_lang('Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });

    setTimeout(() => {
      this.filteredParts = [];
      this.partNumber = '';
      this.barcode = '';
    }, 300);
  }

  selectTechnician() {
    let dialogRef = this.dialog.open(TechnicianFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Technician'), TE1_ID: this.record.TE1_ID},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.TE1_ID = selected['TE1_ID'];
        this.record.TE1_SHORT_CODE = selected['TE1_SHORT_CODE'];
        this.record.TE1_NAME = selected['TE1_NAME'];
      }
      this.inputPartNumber.nativeElement.focus();
    })
  }

  updateTotals() {
    this.totalValue = 0;
    this.addedParts = this.addedParts.map(record => {
      if( /^\.[0-9]*/.test(record['MD1_ORDER_QTY'])) {
        record['MD1_ORDER_QTY'] = '0' + record['MD1_ORDER_QTY'];
      }

      const unitPrice: number = Number(record['MD1_UNIT_PRICE'].toString().replace(/[^.0-9]/g, ''));
      const orderQty: number  = Number(record['MD1_ORDER_QTY']);
      record['MD1_EXTENDED_VALUE'] = unitPrice * orderQty;
      record['RECEIPT_QTY']        = orderQty;
      this.totalValue += record['MD1_EXTENDED_VALUE'];
      return record;
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Shop Order Form'),
        componentRef: this,
        data: [],
        componentName: 'JobTicketAddComponent'
      },
    });
  }

  qtyKeyDown(e, rowData, field) {
    if ( e.keyCode === 13 ) {
      e.stopPropagation();
      let child = document.querySelectorAll(".ui-selectable-row:last-child");
      let rowIndex = child[0].getAttribute('ng-reflect-index');
      const nextRowIndex = parseInt(rowIndex) + 1;
      this.setFocusToField(nextRowIndex, 'td');
      this.inputPartNumber.nativeElement.focus();
    }
  }

  setFocusToField(rowIndex: number, rowName: string) {
    const row = rowName + rowIndex;
    const elem = document.getElementById(row);
    if (elem) {
      elem.click();
    } else {
      const currentTd = <HTMLTableCellElement>document.getElementById(rowName + (rowIndex - 1));
      currentTd.nextSibling['click']();
    }
  }

  setFocusToPart(addedPart: any, rowName: string = 'td') {
    let partIndex: number = 0;
    for (var k = 0; k < this.addedParts.length; k++) {
      if (addedPart['MD1_PART_NUMBER'] === this.addedParts[k]['MD1_PART_NUMBER']) {
        break;
      }
      partIndex ++;
    }
    this.setFocusToField(partIndex, rowName);
  }

  qtyKeyPress(e){
    return (e.charCode >= 48 && e.charCode <= 57) || e.charCode == 46;
  }

  qtyKeyUp(e) {
    this.updateTotals();
  }

  onEditInit( $event ) {
    var interval = setInterval(() => {
      const input = <HTMLInputElement>document.getElementById('input_' + $event.data.MD1_ID);
      if (input){
        clearInterval(interval);
        interval = null;
        input.select();
      }
    }, 5);
  }

  onAddOrder() {
    let dialogRef = this.dialog.open(MasterDataModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data - Stock at ') + this.record.LO1_NAME,
        componentRef: this,
        url: '/master-data/full-parts',
        filters: {
          LO1_TO_ID:     this.record.LO1_ID,
          LO1_FROM_ID:   this.record.LO1_ID,
          "sort[CO1_NAME]": 'ASC',
          "sort[VD1_NAME]": 'ASC',
          "sort[MD1_PART_NUMBER]": 'ASC',
          usePagination: true
        }
      }
    });

    dialogRef.beforeClose().subscribe( parts => {
      this.addPartsToOrder(parts);
    })
  }

  onAddCatalog() {
    let dialogRef = this.dialog.open(AddCatalogListModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data - Stock at ') + this.record.LO1_NAME,
        componentRef: this,
        filters: {
          LO1_TO_ID:     this.record.LO1_ID,
          LO1_FROM_ID:   this.record.LO1_ID,
          "sort[CO1_NAME]": 'ASC',
          "sort[VD1_NAME]": 'ASC',
          "sort[MD1_PART_NUMBER]": 'ASC',
          usePagination: true
        }
      },
    });

    dialogRef.beforeClose().subscribe( parts => {
      this.addPartsToOrder(parts);
    })
  }

  addPartsToOrder( parts, autoFocus: boolean = false ) {

    if (parts) {

      var parts = parts.map( part => {
        var item = _.clone(part);

        item['TE1_ID']     = this.record.TE1_ID;
        item['TE1_NAME']   = this.record.TE1_NAME;
        item["MD1_UNIT_PRICE"] = part["MD1_SELL_PRICE"];
        // Internal order.
        // The purchase UM needs to be set to the receipt UM since its the same shop.
        // That means factor must be 1.
		item["UM1_PURCHASE_ID"] = part["UM1_RECEIPT_ID"];
        item["UM1_PURCHASE_NAME"] = part["UM1_RECEIPT_NAME"];
        item["UM2_RECEIPT_FACTOR"] = "1";

        item["MD2_ON_HAND_QTY"] = part["MD2_ON_HAND_QTY_TO"];
        item["MD2_AVAILABLE_QTY"] = part["MD2_AVAILABLE_QTY_TO"];
        item["MD2_MIN_QTY"] = part["MD2_MIN_QTY_TO"];
        item["MD2_AVAILABLE_QTY_FROM"] = part["MD2_AVAILABLE_QTY"];
        item["MD2_ON_HAND_QTY_FROM"] = part["MD2_ON_HAND_QTY"];
        item["MD1_ORDER_QTY"] = part['MD1_ORDER_QTY'] ? parseInt(part['MD1_ORDER_QTY']) : 1;
        item["MD1_EXTENDED_VALUE"] = part["MD1_UNIT_PRICE"];

        return item;
      });

      var me = this;
      parts.forEach( part => {
        if (!part['MD1_ORDER_QTY']) {
          part['MD1_ORDER_QTY'] = 1;
        }
        let filtered = me.addedParts.filter( item => {
          return item['MD1_ID'] === part.MD1_ID;
        });
        if (!filtered || filtered.length === 0) {
          this.addedParts.push(part);
        } else {
          filtered[0]['MD1_ORDER_QTY'] = parseInt(filtered[0]['MD1_ORDER_QTY']) + parseInt(part['MD1_ORDER_QTY']);
        }
      });

      if (autoFocus) {
        setTimeout(() => {this.setFocusToPart(parts[parts.length - 1]);}, 300);
      }

      this.updateTotals();
    }
  }

  onDelete() {
    const me = this;
    this.confirmationService.confirm({
      message: this.deleteConfirmationMessage,
      accept: () => {
        this.selected.forEach(function(record) {
          me.addedParts.forEach((item, index) => {
            if (item['MD1_ID'] === record['MD1_ID'])
              me.addedParts.splice(index, 1);
          });
        });
        this.selected = [];
        this.updateTotals();
      }
    });
  }

  onSave() {
    var errors = [];
    var zeros  = [];

    if(!this.addedParts.length){
      errors.push(_lang('You have to add parts to the order!'));
    }else if(!this.record.JT1_NUMBER){
      errors.push(_lang('RO # is required to send order/s!'));
    }else{
      var overStockErrors = [];
      this.addedParts.forEach( part => {
        if (part['VD1_NO_INVENTORY'] !== '1') {
          if ( !part['MD2_AVAILABLE_QTY'] || parseInt(part['MD1_ORDER_QTY']) > parseInt(part['MD2_AVAILABLE_QTY']) && parseInt(part['NO_INVENTORY']) == 0 && (!part['NO_JOB_INVENTORY'] || parseInt(part['NO_JOB_INVENTORY']) == 0)) {
            overStockErrors.push(part['MD1_PART_NUMBER'] + ' - Ordering: ' + part['MD1_ORDER_QTY'] + ', Available: <b>' + (part['MD2_AVAILABLE_QTY'] ? part['MD2_AVAILABLE_QTY'] : 0) + '</b>');
          }
        }
        if (parseFloat(part['MD1_ORDER_QTY']) <= 0) {
          zeros.push(part['MD1_PART_NUMBER'] + ': ' + part['MD1_DESC1']);
        }
      });
      if(overStockErrors.length){
        var msg  = _lang(':ErrorOverStock:') + "<br/>";
            msg += _lang(':ErrorReduceQty:') + "<br/>";
            msg +=  overStockErrors.join("<br/>");
        errors.push(msg);
      }
    }

    if(errors.length){
      this.toastr.error(errors.join('<br/>'), _lang('Error!'),{
        timeOut: 10000,
        positionClass: 'toast-center-center',
        enableHtml: true
      });
    }else if(zeros.length){
      var msg  = _lang(':ErrorRemoveInvalidQty:') + "\n";
          msg +=  errors.join("\n");
      this.toastr.error(msg, _lang('#QtyInvalid#'), {
        timeOut: 10000,
        positionClass: 'toast-center-center',
        enableHtml: true
      });
    }else{
      if(!this.record.CU1_ID){
        this.processingUserAction = true;
        this.customerService.generateShortCode().subscribe( result => {
          this.record.CU1_SHORT_CODE = result.CU1_SHORT_CODE.toString();
          this.customerService.create(this.record).subscribe( data => {
              if(!data.success){
                this.processingUserAction = false;
                this.toastr.error(_lang(data.errors.join('<br/>')), _lang('Error!'), {
                  timeOut: 3000,
                  positionClass: 'toast-center-center',
                  enableHtml: true
                });
              }else{
                this.record.CU1_ID = data.data.CU1_ID;
                this._createJobTicket();
              }
          }, () => {
            this.processingUserAction = false;
          });
        }, () => {
          this.processingUserAction = false;
        })
      }else{
        this._createJobTicket();
      }
    }
  }

  private _createJobTicket() {
    this.processingUserAction = true;
    var orders = [];
    var orderNumberIndex = 0;
    this.addedParts.forEach( part => {
      var jobTicketExists = false;

      orders.forEach( order => {
        if(part['VD1_ID'] == order['VD1_ID']){
          jobTicketExists = true;
          order.parts.push(part);
        }
      });

      if ( !jobTicketExists ) {
        var newOrder = new Object();
        newOrder['VD1_ID']      = part['VD1_ID'];
        newOrder['JT1_NUMBER']  = this.record.JT1_NUMBER;
        newOrder['LO1_ID']      = this.record.LO1_ID;
        newOrder['CA1_ID']      = part['CA1_ID'];
        newOrder['CU1_ID']      = this.record.CU1_ID;
        newOrder['JT1_COMMENT'] = this.record.JT1_COMMENT;
        newOrder['parts']       = [part];
        orders.push(newOrder);
        orderNumberIndex ++;
      }
    });

    this.jobTicketService.create(orders).subscribe( result => {
      if(result.success){
        this.toastr.success(_lang('Operation was successfully executed!'), _lang('Service Success'), {
          timeOut: 3000,
        });
        this.router.navigate(['/job-tickets']);
        this.processingUserAction = false;
      }else{
        this.processingUserAction = false;
        this.toastr.error(_lang(result.errors.join('<br/>')), _lang('Error!'), {
          timeOut: 3000,
          positionClass: 'toast-center-center',
          enableHtml: true
        });
      }
    }, () => {
      this.processingUserAction = false;
    });
  }

  selectCountry() {
    let dialogRef = this.dialog.open(SelectCountriesComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Country'), CY1_ID: this.record.CY1_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CY1_ID = selected['CY1_ID'];
        this.record.CY1_SHORT_CODE = selected['CY1_SHORT_CODE'];
        this.record.CY1_NAME = selected['CY1_NAME'];
      }
    })
  }

  selectCustomer() {
    let dialogRef = this.dialog.open(CustomerFinderModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Select Customer'), CU1_ID: this.record.CU1_ID, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.CU1_ID = selected['CU1_ID'];
        this.record.CU1_SHORT_CODE = selected['CU1_SHORT_CODE'];
        this.record.CU1_NAME = selected['CU1_NAME'];
        this.record.CU1_ADDRESS1 = selected['CU1_ADDRESS1'];
        this.record.CU1_ADDRESS2 = selected['CU1_ADDRESS2'];
        this.record.CU1_CITY = selected['CU1_CITY'];
        this.record.CU1_STATE = selected['CU1_STATE'];
        this.record.CU1_ZIP = selected['CU1_ZIP'];
        this.record.CU1_PHONE = selected['CU1_PHONE'];
        this.record.CU1_FAX = selected['CU1_FAX'];
        this.record.CY1_ID = selected['CY1_ID'];
        this.record.CY1_NAME = selected['CY1_NAME'];
      }
    })
  }

  customerChangeRB(value) {
    if (value === '0') {
      this.clearCustomer();
    }
  }

  clearCustomer() {
    this.record.CU1_ID = 0;
    this.record.CU1_SHORT_CODE = '';
    this.record.CU1_NAME = '';
    this.record.CU1_ADDRESS1 = '';
    this.record.CU1_ADDRESS2 = '';
    this.record.CU1_CITY = '';
    this.record.CU1_STATE = '';
    this.record.CU1_ZIP = '';
    this.record.CU1_PHONE = '';
    this.record.CU1_FAX = '';
  }

  customerNameChanged() {
    this.record.CU1_ID = 0;
    this.record.CU1_SHORT_CODE = '';
  }

  clearCountry() {
    this.record.CY1_ID = 0;
    this.record.CY1_SHORT_CODE = '';
    this.record.CY1_NAME = '';
  }

  jobCCCChanged(e) {
    this.record['JT1_COMMENT'] = 'CCC: ' + e.value['CCC_YEAR'] + ' ' +  e.value['CCC_MAKE'] + ' ' +  e.value['CCC_MODEL'] + ' ' +  e.value['CCC_VIN'];
    this.record['JT1_NUMBER'] = e.value['CCC_RO_NUMBER'] + ' [' + e.value['CCC_YEAR'] + ' ' +  e.value['CCC_MAKE'] + ']';
  }

  cancelClick() {
    window.history.back();
  }

}
