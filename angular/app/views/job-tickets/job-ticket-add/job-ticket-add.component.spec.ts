import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobTicketAddComponent } from './job-ticket-add.component';

describe('JobTicketAddComponent', () => {
  let component: JobTicketAddComponent;
  let fixture: ComponentFixture<JobTicketAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobTicketAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobTicketAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
