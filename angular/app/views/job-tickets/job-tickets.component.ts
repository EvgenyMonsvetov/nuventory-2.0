import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';
import { Subject, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { formatDate } from '@angular/common';
import { JobTicketService } from '../../services/job-ticket.service';
import { HeaderService } from '../../services/header.service';
import { takeUntil } from 'rxjs/internal/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-job-tickets',
  templateUrl: './job-tickets.component.html',
  styleUrls: ['./job-tickets.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class JobTicketsComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  formatDate = formatDate;

  @ViewChild('jobTicketsView') jobTicketsView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';

  /** control for the selected filters */
  public vendorCtrl: FormControl = new FormControl();
  public vendorFilterCtrl: FormControl = new FormControl();
  public companyCtrl: FormControl = new FormControl();
  public companyFilterCtrl: FormControl = new FormControl();
  public locationCtrl: FormControl = new FormControl();
  public locationFilterCtrl: FormControl = new FormControl();
  public modifiedByCtrl: FormControl = new FormControl();
  public modifiedByFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyVendor = new Subject<void>();
  private _onDestroyCompany = new Subject<void>();
  private _onDestroyLocation = new Subject<void>();
  private _onDestroyModifiedBy = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredVendors: any[] = [];
  public filteredCompanies: any[] = [];
  public filteredShops: any[] = [];
  public filteredModifiedByUsers: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  private flagsDP: any[] = [];

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  invStatuses: any[] = [];
  statuses: any[] = [];
  cols: any[] = [];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;
  sub: Subscription;

  private users: Array<object> = [];
  private vendors: Array<object> = [];
  private companies: Array<object> = [];
  private shops: Array<object> = [];
  private subscription: Subscription;

  constructor(private jobTicketService: JobTicketService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = {};
    this.subscription = this.headerService.subscribe({
        next: (v) => {
          this.loading = true;
          this.page.pageNumber = 0;
          this.getItems();
        }
    });
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.invStatuses = [
      { label: _lang('#AllRecords#'), value: null},
      { label: _lang('Open'),         value: 'open'},
      { label: _lang('Invoiced'),     value: 'invoiced'}
    ];

    this.statuses = [
      { label: _lang('#AllRecords#'), value: null},
      { label: _lang('Open'),         value: 'open'},
      { label: _lang('Received'),     value: 'received'},
      { label: _lang('Completed'),    value: 'completed'}
    ];

    this.cols = [
      { field: 'JT1_PRINTED',     header: _lang('JT1_PRINTED'),  visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'ORDERED_ON',      header: _lang('JT1_ORDERED_ON'),  visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'JT1_NUMBER',      header: _lang('JT1_INV_NUMBER'),  visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'VD1_NAME',        header: _lang('VD1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'JT1_TOTAL_VALUE', header: _lang('JT1_TOTAL_VALUE'), visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'CO1_NAME',        header: _lang('CO1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'LO1_NAME',        header: _lang('LO1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'MODIFIED_ON',     header: _lang('#ModifiedOn#'),    visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),    visible: true, export: {visible: true, checked: true}, width: 160 }
    ];

    this.flagsDP = [
      { label: _lang('#AllRecords#'), value: ''},
      { label: _lang('Yes'),          value: '1'},
      { label: _lang('No'),           value: '0'}
    ];

    this.jobTicketService.preloadData().subscribe( data => {
      this.prepareData(data);
      this.getItems();
    });

    // listen for search field value changes
    this.vendorFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyVendor))
        .subscribe(() => {
          this.filterVendors();
        });
    this.companyFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCompany))
        .subscribe(() => {
          this.filterCompanies();
        });
    this.locationFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocation))
        .subscribe(() => {
          this.filterShops();
        });
    this.modifiedByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyModifiedBy))
        .subscribe(() => {
          this.filterModifiedByUsers();
        });
  }

  private filterVendors() {
    if (!this.vendors) {
      return;
    }
    // get the search keyword
    let search = this.vendorFilterCtrl.value;
    if (!search) {
      this.filteredVendors = this.vendors;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the vendors
    this.filteredVendors = this.vendors.filter(vendor => vendor['VD1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterCompanies() {
    if (!this.companies) {
      return;
    }
    // get the search keyword
    let search = this.companyFilterCtrl.value;
    if (!search) {
      this.filteredCompanies = this.companies;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the companies
    this.filteredCompanies = this.companies.filter(company => company['CO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterShops() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationFilterCtrl.value;
    if (!search) {
      this.filteredShops = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location
    this.filteredShops = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterModifiedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.modifiedByFilterCtrl.value;
    if (!search) {
      this.filteredModifiedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Modified By Users
    this.filteredModifiedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.users = [{US1_ID: null, US1_NAME: _lang('#AllRecords#')}];
    let usersArr = data.users.map( user => {
      return {
        US1_ID:   user.US1_ID,
        US1_NAME: user.US1_NAME
      }
    }).filter( customer => {
      return customer.US1_NAME && customer.US1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.US1_NAME < b.US1_NAME)
        return -1;
      if (a.US1_NAME > b.US1_NAME)
        return 1;
      return 0;
    });
    this.users = this.users.concat(usersArr);
    this.filterModifiedByUsers();

    this.vendors = [{VD1_ID: null, VD1_NAME: _lang('#AllRecords#')}];
    let vendorsArr = data.vendors.map( vendor => {
      return {
        VD1_ID:   vendor.VD1_ID,
        VD1_NAME: vendor.VD1_NAME
      }
    }).filter( customer => {
      return customer.VD1_NAME && customer.VD1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.VD1_NAME < b.VD1_NAME)
        return -1;
      if (a.VD1_NAME > b.VD1_NAME)
        return 1;
      return 0;
    });
    this.vendors = this.vendors.concat(vendorsArr);
    this.filterVendors();

    this.companies = [{CO1_ID: null, CO1_NAME: _lang('#AllRecords#')}];
    let companiesArr = data.companies.map( company => {
      return {
        CO1_ID:   company.CO1_ID,
        CO1_NAME: company.CO1_NAME
      }
    }).filter( company => {
      return company.CO1_NAME && company.CO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.CO1_NAME < b.CO1_NAME)
        return -1;
      if (a.CO1_NAME > b.CO1_NAME)
        return 1;
      return 0;
    });
    this.companies = this.companies.concat(companiesArr);
    this.filterCompanies();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);
    this.filterShops();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this._onDestroyVendor.next();
    this._onDestroyVendor.complete();
    this._onDestroyCompany.next();
    this._onDestroyCompany.complete();
    this._onDestroyLocation.next();
    this._onDestroyLocation.complete();
    this._onDestroyModifiedBy.next();
    this._onDestroyModifiedBy.complete();
  }

  getScrollHeight(): number {
    return (this.jobTicketsView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getItems() {
    this.jobTicketService.getAll(this.page).subscribe(res => {
        this.items = res.data['currentVocabulary'];
        this.totalRecords = res.data['count'];

        setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

        this.loading = false;
      },
      error => {
        this.loading = false;
        this.toastr.error(_lang('Job tickets') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Job Tickets List'),
        componentRef: this,
        data: [],
        componentName: 'JobTicketsComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if((col === 'ORDERED_ON' || col === 'COMPLETED_ON' || col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970') {
      this.page.filterParams[col] = '';
    } else {
      this.page.filterParams[col] = value;
    }
    this.page.pageNumber = 0;
    this.getItems();
  }

  onOpen() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['job-tickets/view/' + this.selected[0]['JT1_ID']]);
  }

  editRecord(rowData) {
    this.router.navigate(['job-tickets/view/' + rowData['JT1_ID']]);
  }

  onPdfPrint() {
    if (this.selected && this.selected.length) {
      const JT1_IDs: any[] = [];
      this.selected.forEach(function (item) {
        JT1_IDs.push(item['JT1_ID']);
      });
      this.page.filterParams['JT1_IDs'] = JT1_IDs;
      this.jobTicketService.printPDF(this.page).subscribe( result => {
        this.getItems();
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
      this.page.filterParams['JT1_IDs'] = null;
    }
  }

  onDelete(event) {
    if (this.selected.length && this.selected[0]['JT1_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          var JT1_IDs = this.selected.map( item => item['JT1_ID'] );
          this.jobTicketService.deleteMultiple(JT1_IDs).subscribe( result => {
                this.getItems();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete job ticket error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.jobTicketService.downloadXlsx(this.page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

}
