import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './invoices.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { InvoicesComponent } from './invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceService } from '../../services/invoice.service';
import { InvoiceLineService } from '../../services/invoice-line.service';
import { CurrencyModule } from '../../components/currency/currency.module';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        CurrencyModule
    ],
    providers: [
        InvoiceService,
        InvoiceLineService
    ],
    declarations: [
        InvoicesComponent,
        InvoiceFormComponent
    ]
})
export class InvoicesModule { }