import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { permissions } from '../../../permissions';
import { InvoiceService } from '../../../services/invoice.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { TitleService } from '../../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../../pipes/lang';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { Page } from '../../../model/page';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';
import { InvoiceLineService } from '../../../services/invoice-line.service';

@Component({
  selector: 'app-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.scss'],
  providers: [
    InvoiceService,
    InvoiceLineService
  ],
  preserveWhitespaces: true
})
export class InvoiceFormComponent implements OnInit {
  @ViewChild('contentGrid')   contentGrid: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private record: any;

  private statusData: Array<any> = [
    {'label': _lang('#Open#'),      'IN1_STATUS' : 0},
    {'label': _lang('#Received#'),  'IN1_STATUS' : 1}
  ];

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private loading: boolean = true;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private invoiceService: InvoiceService,
              private invoiceLineService: InvoiceLineService,
              public  dialog: MatDialog,
              private titleService: TitleService,
              private toastr: ToastrService,
              private location: Location) {
    this.page.filterParams = {};
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'IN2_LINE',                header: _lang('IN2_LINE'),          visible: true, width: 55 },
      { field: 'IN2_LINK_FROM',           header: _lang('IN2_LINK_FROM'),     visible: true, width: 110 },
      { field: 'IN2_PART_NUMBER',         header: _lang('IN2_PART_NUMBER'),   visible: true, width: 85 },
      { field: 'IN2_DESC1',               header: _lang('IN2_DESC1'),         visible: true, width: 200 },
      { field: 'UM1_NAME',                header: _lang('UM1_NAME'),          visible: true, width: 80 },
      { field: 'GL1_NAME',                header: _lang('GL1_NAME'),          visible: true, width: 70 },
      { field: 'IN2_UNIT_PRICE',          header: _lang('IN2_UNIT_PRICE'),    visible: true, width: 100 },
      { field: 'IN2_EXTENDED_VALUE',      header: _lang('IN2_EXTENDED_VALUE'),visible: true, width: 120 },
      { field: 'MODIFIED_ON',             header: _lang('#ModifiedOn#'),      visible: true, width: 120 },
      { field: 'MODIFIED_BY',             header: _lang('#ModifiedBy#'),      visible: true, width: 120 },
      { field: 'CREATED_ON',              header: _lang('#CreatedOn#'),       visible: false, width: 120 },
      { field: 'CREATED_BY',              header: _lang('#CreatedBy#'),       visible: false, width: 120 }
    ];

    this.record = this.invoiceService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.page.filterParams['IN1_ID'] = this.route.snapshot.params.id;
      this.invoiceService.getById(this.route.snapshot.params.id || 0).subscribe( result => {
        if (result[0]) {
          let data = result[0];
          data['IN1_ID'] = parseInt(data['IN1_ID']);
          data['IN1_STATUS'] = parseInt(data['IN1_STATUS']);
          this.record = data;
          this.getItems();
        }
      });
    }
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.sub = this.invoiceLineService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'].map( item => {
            if (this.record.IN1_TOTAL_VALUE < 0) {
              item['IN2_EXTENDED_VALUE'] = -parseFloat(item['IN2_EXTENDED_VALUE']);
            }
            return item;
          });;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Invoice lines') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Invoice Form'),
        componentRef: this,
        data: [],
        componentName: 'InvoiceFormComponent'
      },
    });
  }

}
