import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { InvoiceService } from '../../services/invoice.service';
import { permissions } from '../../permissions';
import { formatDate } from '@angular/common';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { Subject, Subscription } from 'rxjs';
import { HeaderService } from '../../services/header.service';
import { takeUntil } from 'rxjs/internal/operators';
import { FormControl } from '@angular/forms';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  providers: [
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class InvoicesComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  formatDate = formatDate;

  @ViewChild('invoicesView') invoicesView: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private scrollHeight: string = '50px';

  /** control for the selected filters */
  public locationFromCtrl: FormControl = new FormControl();
  public locationFromFilterCtrl: FormControl = new FormControl();
  public locationByCtrl: FormControl = new FormControl();
  public locationByFilterCtrl: FormControl = new FormControl();
  public modifiedByCtrl: FormControl = new FormControl();
  public modifiedByFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyLocationFrom = new Subject<void>();
  private _onDestroyLocationBy = new Subject<void>();
  private _onDestroyModifiedBy = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredShopsFrom: any[] = [];
  public filteredShopsBy: any[] = [];
  public filteredModifiedByUsers: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private loading: boolean = true;
  sub: Subscription;
  private subscription: Subscription;

  private users: Array<object> = [];
  private shops:   Array<object> = [];

  constructor(private invoiceService: InvoiceService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = {};

    this.subscription = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.getItems();
      }
    });

    this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.getItems();
      }
    });
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'IN1_NUMBER',      header: _lang('IN1_NUMBER'),              visible: true, export: {visible: true, checked: true}, width: 110 },
      { field: 'IN1_TOTAL_VALUE', header: _lang('IN1_TOTAL_VALUE'),         visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'IN1_COMMENT',     header: _lang('IN1_COMMENT'),             visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'LO1_FROM_NAME',   header: _lang('ORDER_FROM_LOCATION_NAME'),visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'LO1_TO_NAME',     header: _lang('ORDER_BY_LOCATION_NAME'),  visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'COMPLETED_ON',    header: _lang('IN1_COMPLETED_ON'),        visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),            visible: true, export: {visible: true, checked: true}, width: 160 }
    ];

    this.invoiceService.preloadData().subscribe( data => {
      this.prepareData(data);
      this.getItems();
    });

    // listen for search field value changes
    this.locationFromFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocationFrom))
        .subscribe(() => {
          this.filterShopsFrom();
        });
    this.locationByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyLocationBy))
        .subscribe(() => {
          this.filterShopsBy();
        });
    this.modifiedByFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyModifiedBy))
        .subscribe(() => {
          this.filterModifiedByUsers();
        });
  }

  private filterShopsFrom() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationFromFilterCtrl.value;
    if (!search) {
      this.filteredShopsFrom = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location from
    this.filteredShopsFrom = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterShopsBy() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.locationByFilterCtrl.value;
    if (!search) {
      this.filteredShopsBy = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the location by
    this.filteredShopsBy = this.shops.filter(shop => shop['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterModifiedByUsers() {
    if (!this.users) {
      return;
    }
    // get the search keyword
    let search = this.modifiedByFilterCtrl.value;
    if (!search) {
      this.filteredModifiedByUsers = this.users;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Modified By Users
    this.filteredModifiedByUsers = this.users.filter(user => user['US1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.users = [{US1_ID: null, US1_NAME: _lang('#AllRecords#')}];
    let usersArr = data.users.map( user => {
      return {
        US1_ID:   user.US1_ID,
        US1_NAME: user.US1_NAME
      }
    }).filter( customer => {
      return customer.US1_NAME && customer.US1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.US1_NAME < b.US1_NAME)
        return -1;
      if (a.US1_NAME > b.US1_NAME)
        return 1;
      return 0;
    });
    this.users = this.users.concat(usersArr);
    this.filterModifiedByUsers();

    this.shops = [{LO1_ID: null, LO1_NAME: _lang('#AllRecords#')}];
    let shopsArr = data.shops.map( shop => {
      return {
        LO1_ID:   shop.LO1_ID,
        LO1_NAME: shop.LO1_NAME
      }
    }).filter( shop => {
      return shop.LO1_NAME && shop.LO1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.LO1_NAME < b.LO1_NAME)
        return -1;
      if (a.LO1_NAME > b.LO1_NAME)
        return 1;
      return 0;
    });
    this.shops = this.shops.concat(shopsArr);
    this.filterShopsFrom();
    this.filterShopsBy();
  }

  ngOnDestroy() {
    if (this.sub)
      this.sub.unsubscribe();
    if (this.subscription)
      this.subscription.unsubscribe();
    this._onDestroyLocationFrom.next();
    this._onDestroyLocationFrom.complete();
    this._onDestroyLocationBy.next();
    this._onDestroyLocationBy.complete();
    this._onDestroyModifiedBy.next();
    this._onDestroyModifiedBy.complete();
  }

  getScrollHeight(): number {
    return (this.invoicesView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
        (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
  }

  getItems() {
    if (this.sub)
      this.sub.unsubscribe();

    this.loading = true;
    this.sub = this.invoiceService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Invoices') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Invoices List'),
        componentRef: this,
        data: [],
        componentName: 'InvoicesComponent'
      },
    });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'COMPLETED_ON' || col === 'CREATED_ON') && value === '01-01-1970') {
      this.page.filterParams[col] = '';
    } else {
      this.page.filterParams[col] = value;
    }
    this.page.pageNumber = 0;
    this.getItems();
  }

  onOpen() {
    if (this.selected && this.selected.length === 1)
      this.router.navigate(['invoices/' + this.selected[0]['IN1_ID']]);
  }

  editRecord(rowData) {
    this.router.navigate(['invoices/' + rowData['IN1_ID']]);
  }

  onInvoice() {

  }

  onPdfPrint() {
    if (this.selected && this.selected.length) {
      const IN1_IDs: any[] = [];
      const OR1_IDs: any[] = [];
      this.selected.forEach(function (item) {
        if (item['IN1_ID'] && item['IN1_ID'] !== '')
          IN1_IDs.push(item['IN1_ID']);
        else if (item['OR1_ID'] && item['OR1_ID'] !== '')
          OR1_IDs.push(item['OR1_ID']);
      });
      let page = new Page();
      if (IN1_IDs.length) {
        page.filterParams['IN1_IDs'] = IN1_IDs;
      }
      if (OR1_IDs.length) {
        page.filterParams['OR1_IDs'] = OR1_IDs;
      }
      this.invoiceService.printPDF(page).subscribe( result => {
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    }
  }

  onDelete() {
    if (this.selected.length && this.selected[0]['IN1_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          var IN1_IDs = this.selected.map( item => item['IN1_ID'] );
          this.invoiceService.deleteMultiple(IN1_IDs).subscribe( result => {
                this.selected = null;
                this.getItems();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete invoice error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;

        this.invoiceService.downloadXlsx(this.page, 'invoices').subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }
}
