import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { InvoicesComponent } from './invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';

export const routes: Routes = [{
    path: '',
    component: InvoicesComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Invoices'),
        permissions: {
            only: [permissions.INVOICES_READ]
        }
    }
},{
    path: ':id',
    component: InvoiceFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        permissions: {
            only: [permissions.INVOICES_READ]
        }
    }
}];