import { Component, OnDestroy, OnInit } from '@angular/core';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { Observable, Subscription } from 'rxjs/Rx';
import { TitleService } from '../../services/title.service';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { HeaderService } from '../../services/header.service';
import { ActivatedRoute } from '@angular/router';
import { _lang } from '../../pipes/lang';
import { UserService } from '../../services/user.service';
import { MailService } from '../../services/mail.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-spam',
  templateUrl: './spam.component.html',
  styleUrls: ['./spam.component.scss'],
  providers: [
      UserService,
      MailService
  ],
  preserveWhitespaces: true
})

export class SpamComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  private page = new Page();
  private usersSubscription: Subscription;
  private selectedCompanies: Array<any> = [];
  private selectedShops: Array<any> = [];
  private selectedGroups: Array<any> = [];
  private record: any;
  private errors: Array<any> = [];
  private currentState = 'filters' || 'customers';
  private cols: any[] = [];
  private users: Array<any> = [];
  private selectedUsers: Array<any> = [];
  private usersLoading: boolean = true;
  private selectedAll: boolean = true;
  private sendingProgress: number = 0; // progress bar percentage
  private sentMails: number = 0;
  private disableButtons: boolean = false;

  constructor(private userService: UserService,
              private mailService: MailService,
              private titleService: TitleService,
              private http: HttpClient,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private headerService: HeaderService) {
    this.selectedCompanies = [this.headerService.CO1_ID];
    this.selectedShops = [this.headerService.LO1_ID];
    this.page.filterParams = {};
    this.page.filterParams['LO1_IDs'] = this.selectedShops;
    this.page.filterParams['CO1_IDs'] = this.selectedCompanies;
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.record = {'SUBJECT' : '', 'MESSAGE' : ''};

    this.cols = [
      { field: 'SELECTED',    header: '',                 visible: true, width: 40 },
      { field: 'US1_EMAIL',   header: _lang('US1_EMAIL'), visible: true, width: 400 },
      { field: 'STATUS',      header: _lang('#Status#'),  visible: true, width: 160 },
    ];
  }

  ngOnDestroy() {
    if (this.usersSubscription) {
      this.usersSubscription.unsubscribe();
    }
  }

  getUsers() {
    this.currentState = 'customers';
    this.usersLoading = true;

    if (this.usersSubscription) {
      this.usersSubscription.unsubscribe();
    }

    this.usersSubscription = this.userService.getUsers(this.page).subscribe(res => {
          this.users = this.selectedUsers = res.data.map(record => {
            record['SELECTED'] = true;
            record['STATUS'] = '';
            return record;
          });
          this.selectedAll = true;
          this.usersLoading = false;
        },
        error => {
          this.usersLoading = false;
          this.toastr.error(_lang('Users') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  selectedCompaniesChange(value) {
    this.page.filterParams['CO1_IDs'] = value;
    this.selectedCompanies = value;
  }

  selectedShopsChange(value) {
    if (!value.length) {
      delete this.page.filterParams['LO1_IDs'];
    } else {
      this.page.filterParams['LO1_IDs'] = value;
    }
    this.selectedShops = value;
  }

  selectedGroupsChange(value) {
    if (!value.length) {
      delete this.page.filterParams['GR1_IDs'];
    } else {
      this.page.filterParams['GR1_IDs'] = value;
    }
    this.selectedGroups = value;
  }

  goBackToFilters() {
    this.currentState = 'filters';
    this.selectedUsers = [];
    this.sendingProgress = 0;
  }

  selectAllChange(event) {
    this.users = this.users.map(record => {
      record['SELECTED'] = event.checked ? 1 : 0;
      record['STATUS'] = '';
      return record;
    });
    this.selectCBChange();
  }

  selectCBChange() {
    this.selectedUsers = this.users.filter( item => {
      return item['SELECTED'];
    });
    this.selectedAll = this.selectedUsers.length === this.users.length;
  }

  sendEmails() {
    let me = this;
    this.sentMails = 0;
    this.sendingProgress = 0;
    this.disableButtons = true;
    this.selectedUsers.forEach(user => {user['STATUS'] = ''});

    Observable.of(...this.selectedUsers)
      .concatMap((user) => {
          user['STATUS'] = _lang('Sending');
          me.record.EMAIL = user['US1_EMAIL'];
          return me.mailService.sendMail(me.record);
      })
      .subscribe((result) => {
          if (!result['success']) {
            me.selectedUsers[me.sentMails]['STATUS'] = _lang('Failed');
          } else {
            me.selectedUsers[me.sentMails]['STATUS'] = _lang('Sent');
          }
          me.sentMails ++;
          me.sendingProgress = parseInt((100 * me.sentMails / me.selectedUsers.length).toFixed(0), 0);
          if (me.selectedUsers.length === me.sentMails) {
            this.disableButtons = false;
          }
      });
  }
}
