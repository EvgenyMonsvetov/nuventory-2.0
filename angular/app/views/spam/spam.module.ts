import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './spam.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { SpamComponent } from './spam.component';
import { CompaniesCheckboxDropdownModule } from '../../components/companies-checkbox-dropdown/companies-checkbox-dropdown.module';
import { ShopsCheckboxDropdownModule } from '../../components/shops-checkbox-dropdown/shops-checkbox-dropdown.module';
import { GroupsCheckboxDropdownModule } from '../../components/groups-checkbox-dropdown/groups-checkbox-dropdown.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MailService } from '../../services/mail.service';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        CompaniesCheckboxDropdownModule,
        ShopsCheckboxDropdownModule,
        GroupsCheckboxDropdownModule,
        MatProgressBarModule
    ],
    providers: [
        MailService
    ],
    declarations: [
        SpamComponent
    ]
})
export class SpamModule {}