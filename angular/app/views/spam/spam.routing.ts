import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { SpamComponent } from './spam.component';

export const routes: Routes = [{
    path: '',
    component: SpamComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Spam'),
        permissions: {
            only: [permissions.SPAM_ACCESS]
        }
    }
}];