import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions  } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { MaterialsBudgetReportComponent } from './materials-budget-report.component';

export const routes: Routes = [{
    path: '',
    component: MaterialsBudgetReportComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Materials Budget'),
        permissions: {
            only: [permissions.MATERIALS_BUDGET_READ]
        }
    }
}];