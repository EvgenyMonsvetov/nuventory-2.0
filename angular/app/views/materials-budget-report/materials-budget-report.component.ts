import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { FormControl } from '@angular/forms';
import moment from 'moment';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { Page } from '../../model/page';
import { Subscription } from 'rxjs/Rx';
import { TitleService } from '../../services/title.service';
import { MatDialog } from '@angular/material';
import { permissions } from '../../permissions';
import * as  Highcharts from 'highcharts';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { HeaderService } from '../../services/header.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-materials-budget-report',
  templateUrl: './materials-budget-report.component.html',
  styleUrls: ['./materials-budget-report.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    PurchaseOrderService
  ],
  preserveWhitespaces: true
})
export class MaterialsBudgetReportComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('materialBudget', { read: ElementRef }) container: ElementRef;
  private chart;
  private showNoData: boolean = false;

  private startDate = new FormControl(moment());
  private endDate = new FormControl(moment());
  private minStartDate = moment('2008-01-01');
  private minEndDate = moment();
  private maxStartDate = moment();
  private maxEndDate = moment();

  private chartDataSubscription: Subscription;
  private headerServiceSubscription: Subscription;
  private page = new Page();
  private materialBudget: number = 0;

  constructor(private purchaseOrderService: PurchaseOrderService,
              private titleService: TitleService,
              private headerService: HeaderService,
              public  dialog: MatDialog,
              private toastr: ToastrService,
              private route: ActivatedRoute) {
    let startD = new Date();
    startD.setFullYear(new Date().getFullYear(), new Date().getMonth(), 1);
    let endD = new Date();
    endD.setFullYear(new Date().getFullYear(), new Date().getMonth()+1, 0);
    let datePipe = new DatePipe('en-US');

    this.page.filterParams['START_DATE'] = datePipe.transform(startD, 'yyyy-MM-dd');
    this.page.filterParams['END_DATE'] = datePipe.transform(endD, 'yyyy-MM-dd');
    this.page.filterParams['MATERIAL_BUDGET_REPORT'] = true;

    this.headerServiceSubscription = this.headerService.subscribe({
      next: (v) => {
        this.getChartData();
      }
    });
  }

  ngOnInit() {
    this.buildHighcharts([], [], []);
    this.titleService.setNewTitle(this.route);
  }

  ngOnDestroy() {
    if (this.chartDataSubscription) {
      this.chartDataSubscription.unsubscribe();
    }
    if (this.headerServiceSubscription) {
      this.headerServiceSubscription.unsubscribe();
    }
  }

  chosenStartYearHandler(normalizedYear: moment.Moment) {
    const ctrlValue = this.startDate.value;
    ctrlValue.year(normalizedYear.year());
    this.startDate.setValue(ctrlValue);
  }

  chosenStartMonthHandler(normalizedMonth: moment.Moment, datepicker: MatDatepicker<moment.Moment>) {
    const ctrlValue = this.startDate.value;
    ctrlValue.month(normalizedMonth.month());
    ctrlValue.year(normalizedMonth.year());
    this.startDate.setValue(ctrlValue);
    this.minEndDate = null;
    setTimeout(() => {this.minEndDate = ctrlValue;}, 100);
    datepicker.close();
  }

  chosenEndYearHandler(normalizedYear: moment.Moment) {
    const ctrlValue = this.endDate.value;
    ctrlValue.year(normalizedYear.year());
    this.endDate.setValue(ctrlValue);
  }

  chosenEndMonthHandler(normalizedMonth: moment.Moment, datepicker: MatDatepicker<moment.Moment>) {
    const ctrlValue = this.endDate.value;
    ctrlValue.month(normalizedMonth.month());
    ctrlValue.year(normalizedMonth.year());
    this.endDate.setValue(ctrlValue);
    this.maxStartDate = null;
    setTimeout(() => {this.maxStartDate = ctrlValue;}, 100);
    datepicker.close();
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Materials Budget Report'),
        componentRef: this,
        data: [],
        componentName: 'MaterialsBudgetReportComponent'
      },
    });
  }

  getChartData() {
    if (this.chartDataSubscription) {
      this.chartDataSubscription.unsubscribe();
    }

    this.showNoData = false;
    this.chart.showLoading();

    let startD = this.startDate.value._d;
    startD.setFullYear(startD.getFullYear(), startD.getMonth(), 1);
    let endD = this.endDate.value._d;
    endD.setFullYear(endD.getFullYear(), endD.getMonth() + 1, 0);
    let datePipe = new DatePipe('en-US');

    this.page.filterParams['START_DATE'] = datePipe.transform(startD, 'yyyy-MM-01');
    this.page.filterParams['END_DATE'] = datePipe.transform(endD, 'yyyy-MM-dd');
    this.page.filterParams['GROUP_BY_MONTH'] = startD.getFullYear() !== endD.getFullYear() || startD.getMonth() !== endD.getMonth();

    this.chartDataSubscription = this.purchaseOrderService.getDashboardCharts(this.page).subscribe(res => {
          this.chart.hideLoading();
          this.materialBudget =  res['budgets'] && res['budgets'].length > 0 ? res['budgets'][0]['y'] : 0;
          if (res['series'] && res['series'].length) {
            this.showNoData = false;
            this.buildHighcharts(res['series'], res['drilldown'], res['budgets']);
          } else {
            this.showNoData = true;
            this.buildHighcharts([], [], []);
          }
      },
      error => {
        this.toastr.error(_lang('Materials Budget') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
  }

  buildHighcharts(series, drilldown, budgets) {
    Highcharts.setOptions({
      lang: {
        drillUpText: _lang('Back to Month')
      }
    });

    this.chart = Highcharts.chart(this.container.nativeElement, {
      chart: {
        type: 'column'
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text: 'Materials Budget ($)'
        },
        minRange: this.page.filterParams['GROUP_BY_MONTH'] === true ? null : this.materialBudget,
        min: 0,
        plotLines: this.page.filterParams['GROUP_BY_MONTH'] === true ? [] : [{
          color: 'red',
          value: this.materialBudget,
          width: 2,
          zIndex: 4
        }]
      },
      title: {
        text: 'Materials Budget'
      },
      subtitle: {
        text: this.showNoData ? _lang('There is no data') : ''
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            formatter: function() {
              return this.series.type === 'column' ? ( this.point.hasOwnProperty('percentage') ? (this.point.y.toFixed(2) + ' (' + this.point.percentage.toFixed(0) + '%)') : this.point.y.toFixed(2)) : '';            }
          }
        },
        column: {
          colorByPoint: true,
        }
      },
      tooltip: {
        formatter: function() {
          return (this.series.name === 'Budget' ? 'Budget: ' : _lang('Total MTD of') + ' ' + this.point.name + '<br/>') + '<b>$' + this.point.y.toFixed(2) + '</b>';
        }
      },
      series: [
        {
          name: 'Materials Budget',
          data: series,
          type: 'column'
        },
        {
          name: 'Budget',
          type: 'line',
          marker: {
            lineWidth: 2,
            lineColor: 'red',
            fillColor: 'white',
            // enabled: false
          },
          step: 'center',
          color: 'red',
          data: this.page.filterParams['GROUP_BY_MONTH'] === true ? budgets : [],
          enableMouseTracking: true
        }
      ],
      drilldown: {
        series: drilldown
      }
    });
  }

}
