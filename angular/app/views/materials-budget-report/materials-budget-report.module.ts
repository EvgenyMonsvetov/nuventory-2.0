import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './materials-budget-report.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ReportingFilterModule } from '../../components/reporting-filter/reporting-filter.module';
import { MaterialsBudgetReportComponent } from './materials-budget-report.component';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material';
import { RawSalesDataStoreMonthService } from '../../services/raw-sales-data-store-month.service';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        ReportingFilterModule,
        MomentDateModule,
        MatDatepickerModule
    ],
    providers: [
        RawSalesDataStoreMonthService
    ],
    declarations: [
        MaterialsBudgetReportComponent
    ]
})
export class MaterialsBudgetReportModule { }