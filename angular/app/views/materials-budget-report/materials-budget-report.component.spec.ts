import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialsBudgetReportComponent } from './materials-budget-report.component';

describe('MaterialsBudgetReportComponent', () => {
  let component: MaterialsBudgetReportComponent;
  let fixture: ComponentFixture<MaterialsBudgetReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialsBudgetReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialsBudgetReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
