import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechScanAddComponent } from './tech-scan-add.component';

describe('TechScanAddComponent', () => {
  let component: TechScanAddComponent;
  let fixture: ComponentFixture<TechScanAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechScanAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechScanAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
