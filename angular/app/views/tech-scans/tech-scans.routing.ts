import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { TechScansComponent } from './tech-scans.component';
import { TechScanViewComponent } from './tech-scan-view/tech-scan-view.component';
import { TechScanAddComponent } from './tech-scan-add/tech-scan-add.component';

export const routes: Routes = [{
    path: '',
    component: TechScansComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Tech Scans'),
        permissions: {
            only: [permissions.TECH_SCANS_READ]
        }
    }
},{
    path: 'view/:id',
    component: TechScanViewComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Tech Scan'),
        permissions: {
            only: [permissions.TECH_SCANS_EDIT]
        }
    }
},{
    path: 'add/:id',
    component: TechScanAddComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Create Tech Scans'),
        permissions: {
            only: [permissions.TECH_SCANS_ADD]
        }
    }
},{
    path: 'add/:id/:id',
    component: TechScanAddComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Create Tech Scans'),
        permissions: {
            only: [permissions.TECH_SCANS_ADD]
        }
    }
}];