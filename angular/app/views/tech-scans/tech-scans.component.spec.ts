import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechScansComponent } from './tech-scans.component';

describe('TechScansComponent', () => {
  let component: TechScansComponent;
  let fixture: ComponentFixture<TechScansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechScansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechScansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
