import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './tech-scans.routing';
import { TechScanService } from '../../services/tech-scan.service';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MatRadioModule } from '@angular/material/radio';
import { TechScansComponent } from './tech-scans.component';
import { TechScanViewComponent } from './tech-scan-view/tech-scan-view.component';
import { TechScanAddComponent } from './tech-scan-add/tech-scan-add.component';
import { MatAutocompleteModule } from '@angular/material';
import { CurrencyModule } from '../../components/currency/currency.module';
import { TechScanAddFormModule } from '../../components/tech-scan-add-form/tech-scan-add-form.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule,
      MatRadioModule,
      MatAutocompleteModule,
      CurrencyModule,
      TechScanAddFormModule
  ],
  providers: [
      TechScanService
  ],
  declarations: [
      TechScansComponent,
      TechScanViewComponent,
      TechScanAddComponent
  ]
})
export class TechScansModule { }