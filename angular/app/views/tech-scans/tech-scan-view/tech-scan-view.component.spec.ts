import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechScanViewComponent } from './tech-scan-view.component';

describe('TechScanViewComponent', () => {
  let component: TechScanViewComponent;
  let fixture: ComponentFixture<TechScanViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechScanViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechScanViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
