import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { TitleService } from '../../../services/title.service';
import { Location } from '@angular/common';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { environment } from '../../../../environments/environment';
import { _lang } from '../../../pipes/lang';
import { permissions } from '../../../permissions';
import { Page } from '../../../model/page';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationService } from 'primeng/api';
import { TechScanService } from '../../../services/tech-scan.service';
import { OrderLineService } from '../../../services/order-line.service';
import { OrderLineModalComponent } from '../../../components/order-line-modal/order-line-modal.component';

@Component({
  selector: 'app-tech-scan-view',
  templateUrl: './tech-scan-view.component.html',
  styleUrls: ['./tech-scan-view.component.scss'],
  providers: [
    TechScanService,
    OrderLineService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class TechScanViewComponent implements OnInit {
  @ViewChild('contentGrid')   contentGrid: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;

  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private record: any;
  private apiUrl = environment.API_URL;

  private statusData: Array<any> = [
    {'label': _lang('#Open#'),      'OR1_STATUS' : 0},
    {'label': _lang('#Received#'),  'OR1_STATUS' : 1}
  ];

  private newCustomer = '0';
  private panelOpenState = false;

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  cols: any[] = [];
  items: any[] = [];
  selected: any;

  private page = new Page();
  private loading: boolean = true;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private techScanService: TechScanService,
              private orderLineService: OrderLineService,
              private confirmationService: ConfirmationService,
              public  dialog: MatDialog,
              private elementRef: ElementRef,
              private titleService: TitleService,
              private toastr: ToastrService,
              private location: Location) {
    this.page.filterParams = {};
  }

  ngOnInit() {
    this.loading = true;
    this.titleService.setNewTitle(this.route);

    this.cols = [
      // { field: 'OR1_NUMBER',              header: _lang('OR1_NUMBER'),        visible: true, width: 57 },
      { field: 'OR2_LINE',                header: _lang('Line #'),            visible: true, width: 57 },
      { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),   visible: true, width: 110 },
      { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),         visible: true, width: 200 },
      { field: 'OR2_RECEIVING_QTY',       header: _lang('OR2_RECEIVING_QTY'), visible: true, width: 76 },
      { field: 'OR2_RECEIVED_QTY',        header: _lang('OR2_RECEIVED_QTY'),  visible: true, width: 75 },
      { field: 'UM1_RECEIVE_NAME',        header: _lang('UM1_RECEIVE_NAME'),  visible: true, width: 80 },
      { field: 'OR2_ORDER_QTY',           header: _lang('PO2_ORDER_QTY'),     visible: true, width: 71 },
      { field: 'UM1_NAME',                header: _lang('MD1_UM'),            visible: true, width: 80 },
      { field: 'MD1_UNIT_PRICE',          header: _lang('PO2_UNIT_PRICE'),    visible: true, width: 70 },
      { field: 'OR2_EXTENDED_VALUE',      header: _lang('OR2_EXTENDED_VALUE'),visible: true, width: 78 },
      // { field: 'MODIFIED_ON',             header: _lang('#ModifiedOn#'),      visible: true, width: 120 },
      // { field: 'MODIFIED_BY',             header: _lang('#ModifiedBy#'),      visible: true, width: 120 },
      // { field: 'CREATED_ON',              header: _lang('#CreatedOn#'),       visible: true, width: 120 },
      // { field: 'CREATED_BY',              header: _lang('#CreatedBy#'),       visible: true, width: 120 }
    ];

    this.record = this.techScanService.initialize();
    this.action = this.route.snapshot.url[0].toString();

    if (this.route.snapshot.params.id && this.route.snapshot.params.id > 0) {
      this.page.filterParams['OR1_ID'] = this.route.snapshot.params.id;
      this.getItems();
      this.getTechScan();
    }
  }

  getTechScan() {
    this.techScanService.getById(this.route.snapshot.params.id || 0).subscribe( data => {
      data['OR1_ID'] = parseInt(data['OR1_ID']);
      data['OR1_STATUS'] = parseInt(data['OR1_STATUS']);
      this.newCustomer = parseInt(data['CU1_ID']) > 0 ? '1' : '0';
      this.record = data;
    });
  }

  getItems() {
    if (this.sub){
      this.sub.unsubscribe();
    }

    this.sub = this.orderLineService.getAll(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Order lines') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  cancelClick() {
    this.location.back();
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Order Form'),
        componentRef: this,
        data: [],
        componentName: 'TechScanViewComponent'
      },
    });
  }

  onOpen(rowData) {
    this.showStoreOrderForm(rowData['OR2_ID']);
  }

  openRecord() {
    if (this.selected) {
      this.showStoreOrderForm(this.selected['OR2_ID']);
    }
  }

  showStoreOrderForm(poId) {
    let dialogRef = this.dialog.open(OrderLineModalComponent, {
      width: window.innerWidth-100+'px',
      height: window.innerHeight-80+'px',
      role: 'dialog',
      data: {title: _lang('Order Line'), OR2_ID: poId, componentRef: this.elementRef},
    });

    dialogRef.beforeClose().subscribe( value => {
      if (value) {
        this.getItems();
      }
    });
  }

  onDelete(event) {
    if (this.selected && this.selected['OR2_ID']) {
      this.confirmationService.confirm({
        message: this.deleteConfirmationMessage,
        accept: () => {
          this.loading = true;
          this.orderLineService.delete(this.selected['OR2_ID']).subscribe( result => {
                this.selected = null;
                this.getItems();
                this.getTechScan();
              },
              error => {
                this.loading = false;
                this.toastr.error(_lang('On delete order line error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
        }
      });
    }
  }

}
