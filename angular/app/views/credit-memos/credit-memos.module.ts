import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './credit-memos.routing';
import { SharedModule } from '../../modules/shared.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { CreditMemosComponent } from './credit-memos.component';
import { InvoiceService } from '../../services/invoice.service';
import { CreditMemoAddComponent } from './credit-memo-add/credit-memo-add.component';
import { MatAutocompleteModule } from '@angular/material';
import { CurrencyModule } from '../../components/currency/currency.module';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgxPermissionsModule.forChild(),
        SharedModule,
        MatAutocompleteModule,
        CurrencyModule
    ],
    providers: [
        InvoiceService
    ],
    declarations: [
        CreditMemosComponent,
        CreditMemoAddComponent
    ]
})
export class CreditMemosModule { }