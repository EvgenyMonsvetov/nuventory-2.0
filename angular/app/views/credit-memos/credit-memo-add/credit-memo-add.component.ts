import { Component, ElementRef, Inject, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ShopFinderModalComponent } from '../../../components/shop-finder-modal/shop-finder-modal.component';
import { Page } from '../../../model/page';
import { permissions } from '../../../permissions';
import { MasterDataService } from '../../../services/master.data.service';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { TitleService } from '../../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../../pipes/lang';
import { FormControl } from '@angular/forms';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { CellEditor } from 'primeng/table';
import { DOCUMENT } from '@angular/common';
import { AddCatalogListModalComponent } from '../../../components/add-catalog-list-modal/add-catalog-list-modal.component';
import { HeaderService } from '../../../services/header.service';
import { Subscription } from 'rxjs';
import { MasterDataModalComponent } from '../../../components/master-data-modal/master-data-modal.component';
import { InvoiceService } from '../../../services/invoice.service';
import { InvoiceLineService } from '../../../services/invoice-line.service';
import * as _ from 'lodash';
import { Location } from '@angular/common';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-credit-memo-add',
  templateUrl: './credit-memo-add.component.html',
  styleUrls: ['./credit-memo-add.component.scss'],
  providers: [
    MasterDataService,
    InvoiceService,
    InvoiceLineService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class CreditMemoAddComponent implements OnInit {
  private permissions = permissions;
  private constants = Constants;

  @ViewChild('creditMemoAddView')  creditMemoAddView: ElementRef;
  @ViewChild('contentGrid')   contentGrid:  ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChildren(CellEditor)   editors: QueryList<CellEditor>;

  cols: any[];
  items: any[] = [];
  parts: any[] = [];
  selected: any;
  private record: any;
  private errors: Array<any> = [];

  private page = new Page();
  private size: number = 50;
  private loading: boolean = false;

  searchValue: string = '';
  totalValue: number = 0;

  filteredParts: any[] = [];
  addedParts: any[] = [];
  partNumber: string = '';

  partsControl = new FormControl();
  private subscription: Subscription;

  headerList: any[] = [];
  lineList: any[] = [];
  selectedHeader: any = {IN1_NUMBER: _lang('- Select Invoice -'), IN1_ID: 0};
  selectedInvoiceLine: any = {IN2_DESC1: _lang('- Select Line -'), IN2_ID: 0};

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  IN1_ID = 0;
  IN1_NUMBER = 0;
  IN1_COMMENT = '';
  IN2_DESC1 = '';
  CO1_ID = 0;
  CO1_NAME = '';
  CO1_SHORT_CODE = '';
  LO1_TO_ID = 0;
  LO1_TO_NAME = '';
  LO1_FROM_ID = 0;
  LO1_FROM_NAME = '';

  constructor(private masterDataService: MasterDataService,
              private invoiceService: InvoiceService,
              private invoiceLineService: InvoiceLineService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              @Inject(DOCUMENT) document,
              private headerService: HeaderService,
              private location: Location) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    this.CO1_ID = this.headerService.CO1_ID;
    this.LO1_FROM_ID = this.headerService.LO1_ID;
    this.page.filterParams['CO1_ID'] = this.CO1_ID;
    this.page.filterParams['LO1_ID'] = this.LO1_FROM_ID;

    this.subscription = this.headerService.subscribe({
      next: (v) => {
        this.CO1_ID = this.headerService.CO1_ID;
        this.LO1_FROM_ID = this.headerService.LO1_ID;
        this.page.filterParams['CO1_ID'] = this.CO1_ID;
        this.page.filterParams['LO1_FROM_ID'] = this.LO1_FROM_ID;
        this.page.filterParams['LO1_TO_ID'] = this.LO1_TO_ID;
        this.page.pageNumber = 0;

        this.initOrder();
      }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

    this.cols = [
      { field: 'IN2_LINK',                header: _lang('IN2_LINK'),              visible: false, width: 100 },
      { field: 'IN2_LINK_NAME',           header: _lang('IN2_LINK_NAME'),         visible: true,  width: 80 },
      { field: 'MD1_ID',                  header: _lang('MD1_ID'),                visible: false, width: 100 },
      { field: 'VD1_ID',                  header: _lang('VD1_ID'),                visible: false, width: 100 },
      { field: 'VD1_SHORT_CODE',          header: _lang('VD1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'VD1_NAME',                header: _lang('VD1_NAME'),              visible: true,  width: 120 },
      { field: 'CO1_ID',                  header: _lang('CO1_ID'),                visible: false, width: 100 },
      { field: 'CO1_SHORT_CODE',          header: _lang('CO1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'CO1_NAME',                header: _lang('CO1_NAME'),              visible: false, width: 100 },
      { field: 'LO1_ID',                  header: _lang('LO1_ID'),                visible: false, width: 100 },
      { field: 'LO1_SHORT_CODE',          header: _lang('LO1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'LO1_NAME',                header: _lang('LO1_NAME'),              visible: false, width: 100 },
      { field: 'FI1_ID',                  header: _lang('FI1_ID'),                visible: false, width: 100 },
      { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),       visible: true,  width: 100 },
      { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),             visible: true,  width: 160 },
      { field: 'MD1_UNIT_PRICE',          header: _lang('MD1_UNIT_PRICE'),        visible: true,  width: 90  },
      { field: 'UM1_PRICE_NAME',          header: _lang('UM1_PRICE_NAME'),        visible: false, width: 100 },
      { field: 'MD1_ORDER_QTY',           header: _lang('MD1_ORDER_QTY'),         visible: true,  width: 50  },
      { field: 'UM1_PURCHASE_NAME',       header: _lang('UM1_PURCHASE_NAME'),     visible: false, width: 70  },
      { field: 'RECEIPT_QTY',             header: _lang('QTY'),                   visible: false, width: 90  },
      { field: 'UM1_RECEIPT_NAME',        header: _lang('UM1_RECEIPT_NAME'),      visible: true,  width: 100 },
      { field: 'MD2_ON_HAND_QTY',         header: _lang('MD2_ON_HAND_QTY'),       visible: true,  width: 70  },
      { field: 'MD2_MIN_QTY',             header: _lang('MD2_MIN_QTY'),           visible: false, width: 70  },
      { field: 'MD1_EXTENDED_VALUE',      header: _lang('MD1_EXTENDED_VALUE'),    visible: true,  width: 100 }
    ];

    this.initOrder();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  initOrder() {
    if(this.loading)
      return;

    this.loading = true;

    var result = this.invoiceService.isInvoiceCanBeCreated( this.headerService );

    if (!result['canBeCreated']) {
      this.toastr.error(result['error']);
      this.router.navigate(['/credit-memos']);
    } else {
      this.page.filterParams['MD1_CENTRAL_ORDER_FLAG'] = 1;
      this.invoiceService.initInvoice(this.page).subscribe( res => {
        this.CO1_ID = res['company']['CO1_ID'];
        this.CO1_NAME = res['company']['CO1_NAME'];
        this.CO1_SHORT_CODE = res['company']['CO1_SHORT_CODE'];

        this.LO1_FROM_ID   = res['fromLocation'] ? res['fromLocation']['LO1_ID'] : 0;
        this.LO1_FROM_NAME = res['fromLocation'] ? res['fromLocation']['LO1_NAME'] : '';

        this.IN1_NUMBER = parseInt(res['number']['OR1_NUMBER'], 0);

        this.page.filterParams['CO1_ID'] = this.CO1_ID;
        this.page.filterParams['LO1_FROM_ID'] = this.LO1_FROM_ID;
        this.page.filterParams['LO1_TO_ID'] = this.LO1_TO_ID;

        if (this.LO1_TO_ID && this.LO1_TO_ID !== 0 && this.LO1_FROM_ID && this.LO1_FROM_ID !== 0) {
          this.getLinkHeaders();
        } else {
          this.loading = false;
        }

        this.parts = res['parts'];
      });
    }
  }

  getLinkHeaders() {
    this.lineList = [];
    this.IN1_ID = 0;
    this.IN2_DESC1 = '';
    this.page.filterParams['LO1_FROM_ID'] = this.LO1_FROM_ID;
    this.page.filterParams['LO1_TO_ID'] = this.LO1_TO_ID;

    this.invoiceService.getLinkHeaders(this.page).subscribe(res => {
          this.headerList = [{IN1_NUMBER: _lang('- Select Invoice -'), IN1_ID: 0}];
          this.headerList = this.headerList.concat(res['data']['currentVocabulary']);
          this.loading = false;
        },
        error => {
          this.toastr.error(_lang('Link Headers') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
          this.loading = false;
        });
  }

  getInvoiceLines() {
    this.IN1_ID = 0;
    this.IN2_DESC1 = '';
    this.selectedInvoiceLine = {IN2_DESC1: _lang('- Select Line -'), IN2_ID: 0};

    if (this.selectedHeader && this.selectedHeader['IN1_ID'] !== 0) {
      this.IN1_ID = this.selectedHeader['IN1_ID'];
      const p = new Page();
      p.filterParams['IN1_ID'] = this.selectedHeader['IN1_ID'];
      this.invoiceLineService.getInvoiceLines(p).subscribe(res => {
            this.lineList = [{IN2_DESC1: _lang('- Select Line -'), IN2_ID: 0}];
            this.lineList = this.lineList.concat(res['data']['currentVocabulary']);
          },
          error => {
            this.toastr.error(_lang('Invoice Lines') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    }
  }

  filterParts(value: string) {
    const filterValue = value.toLowerCase();
    this.filteredParts = this.parts.filter(option => { return option['MD1_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1 ||
        option['MD1_DESC1'].toLowerCase().indexOf(filterValue) > -1 } );
  }

  partNumberKeyUp(event) {
    if (event.keyCode === 13) {
      this.filteredParts = this.parts.filter(option => { return option['MD1_PART_NUMBER'].toLowerCase() === this.partNumber} );
      if (this.partNumber && this.partNumber !== '') {
        if (this.filteredParts && this.filteredParts.length > 0) {
          this.onSelectPart(this.partNumber);
          this.filteredParts = [];
        } else {
          this.toastr.error(_lang('No one part number does not match'), _lang('Error'), {
            timeOut: 3000,
          });
        }
      }
    }
  }

  onSelectPart(value) {
    this.partNumber = '';
    this.page.filterParams['LO1_TO_ID'] = this.LO1_TO_ID;
    this.page.filterParams['MD1_PART_NUMBER'] = value;
    this.page.filterParams['LINKED_PART'] = 0;

    this.masterDataService.getCreditMemoFullPart(this.page).subscribe(res => {
          this.addPartsToOrder(res['data']);
        },
        error => {
          this.toastr.error(_lang('Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  selectToShop() {
    let dialogRef = this.dialog.open(ShopFinderModalComponent, {
      width: '90%',
      height: window.innerHeight - 100 + 'px',
      role: 'dialog',
      data: {
        filters: {
          'CO1_ID': this.CO1_ID
        },
        HIGHLIGHT_LO1_ID: this.LO1_TO_ID
      },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.LO1_TO_NAME = selected.LO1_NAME;
        this.LO1_TO_ID = selected.LO1_ID;
        this.getLinkHeaders();
      }
    })
  }

  clearToShop() {
    this.LO1_TO_NAME = '';
    this.LO1_TO_ID = 0;
  }

  updateTotals() {
    this.totalValue = 0;
    this.addedParts = this.addedParts.map(record => {
      const unitPrice: number = Number(record['MD1_UNIT_PRICE'].toString().replace(/[^.0-9]/g, ''));
      const orderQty: number  = Number(record['MD1_ORDER_QTY'].toString().replace(/[^.0-9]/g, ''));
      record['MD1_EXTENDED_VALUE'] = (unitPrice * orderQty);
      this.totalValue += -record['MD1_EXTENDED_VALUE'];
      if (!record['IN2_LINK'] || record['IN2_LINK'] === '')
        record['MD1_EXTENDED_VALUE'] *= -1;

      return record;
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Shop Order Form'),
        componentRef: this,
        data: [],
        componentName: 'CreditMemoAddComponent'
      },
    });
  }

  qtyKeyDown(e, rowIndex) {
    if ( (e.keyCode === 9 || e.keyCode === 13) ) {
      e.stopPropagation();
      const i = rowIndex + 1;
      let elem = document.getElementById('td' + i);
      if (elem) {
        elem.click();
      } else {
        let currentTd = <HTMLTableCellElement>document.getElementById('td' + rowIndex);
        currentTd.nextSibling['click']();
      }
    }
  }

  qtyKeyUp(e) {
    this.updateTotals();
  }

  onEditInit( $event ) {
    var interval = setInterval(() => {
      const input = <HTMLInputElement>document.getElementById('input_' + $event.data.MD1_ID);
      if (input) {
        clearInterval(interval);
        interval = null;
        input.select();
      }
    }, 5);

  }

  onAddMasterData() {
    let dialogRef = this.dialog.open(MasterDataModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data - Stock at ') + this.LO1_FROM_NAME,
        componentRef: this,
        url: '/master-data/full-parts',
        filters: {
          LO1_TO_ID:     this.LO1_FROM_ID,
          LO1_FROM_ID:   this.LO1_FROM_ID,
          'sort[CO1_NAME]': 'ASC',
          'sort[VD1_NAME]': 'ASC',
          'sort[MD1_PART_NUMBER]': 'ASC',
          usePagination: true
        }
      }
    });

    dialogRef.beforeClose().subscribe( parts => {
      this.addPartsToOrder(parts, this.IN1_NUMBER);
    })
  }

  onAddCatalog() {
    let dialogRef = this.dialog.open(AddCatalogListModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data - Stock at ') + this.LO1_FROM_NAME,
        componentRef: this,
        filters: {
          LO1_TO_ID:     this.LO1_FROM_ID,
          LO1_FROM_ID:   this.LO1_FROM_ID,
          'sort[CO1_NAME]': 'ASC',
          'sort[VD1_NAME]': 'ASC',
          'sort[MD1_PART_NUMBER]': 'ASC',
          usePagination: true
        }
      },
    });

    dialogRef.beforeClose().subscribe( parts => {
      this.addPartsToOrder(parts, this.IN1_NUMBER);
    })
  }

  addPartsToOrder( parts, IN1_NUMBER: number = 0) {
    if (parts) {
      var parts = parts.map( part => {
        var item = _.clone(part);

        item['MD2_ON_HAND_QTY'] = part['MD2_ON_HAND_QTY_TO'];
        item['MD2_AVAILABLE_QTY'] = part['MD2_AVAILABLE_QTY_TO'];
        item['MD2_MIN_QTY'] = part['MD2_MIN_QTY_TO'];
        item['MD2_AVAILABLE_QTY_FROM'] = part['MD2_AVAILABLE_QTY'];
        item['MD2_ON_HAND_QTY_FROM'] = part['MD2_ON_HAND_QTY'];


        if (! item['MD1_UNIT_PRICE'])
          item['MD1_UNIT_PRICE'] = part['MD1_UNIT_PRICE'];

        if (!item['MD1_ORDER_QTY'])
          item['MD1_ORDER_QTY'] = part['DEFAULT_ORDER_QTY'];
        if (Number(item['MD1_ORDER_QTY']) <= 0) {
          item['MD1_ORDER_QTY'] = 1;
        }

        if (!item['MD1_EXTENDED_VALUE'])
          item['MD1_EXTENDED_VALUE'] = (Number(item['MD1_ORDER_QTY']) * (Number(part['MD1_UNIT_PRICE']))).toString();

        if (IN1_NUMBER)
          item['IN1_NUMBER'] = IN1_NUMBER;

        return item;
      });

      parts.forEach( part => {
        if (!part['MD1_ORDER_QTY']) {
          part['MD1_ORDER_QTY'] = 1;
        }
        let filtered = this.addedParts.filter( item => {
          return item['MD1_ID'] === part.MD1_ID;
        });

        if (!filtered || filtered.length === 0) {
          this.addedParts.push(part);
        } else {
          filtered[0]['MD1_ORDER_QTY'] += part['MD1_ORDER_QTY'];
        }
      });

      this.updateTotals();
    }
  }

  onDelete() {
    const me = this;
    this.confirmationService.confirm({
      message: this.deleteConfirmationMessage,
      accept: () => {
        this.selected.forEach(function(record) {
          me.addedParts.forEach((item, index) => {
            if (item['MD1_PART_NUMBER'] === record['MD1_PART_NUMBER'])
              me.addedParts.splice(index, 1);
          });
        });
        this.selected = [];
        this.updateTotals();
      }
    });
  }

  onSave() {
    var orders = [];
    this.addedParts.forEach( line => {
      var orderExists = false;

      orders.forEach( header => {
        if (line['IN1_NUMBER'] === header['IN1_NUMBER']) {
          header['TOTAL'] += Number(line['MD1_EXTENDED_VALUE']);
          header['parts'].push(line);
          orderExists = true;
        }
      });

      if ( !orderExists ) {
        var newHeader = new Object();
        newHeader['LO1_TO_ID']   = this.LO1_TO_ID;
        newHeader['LO1_FROM_ID'] = this.LO1_FROM_ID;
        newHeader['LO1_ID']      = line['LO1_ID'];
        newHeader['CO1_ID']      = line['CO1_ID'];
        newHeader['VD1_ID']      = line['VD1_ID'];
        newHeader['CA1_ID']      = line['CA1_ID'];
        newHeader['GL1_ID']      = line['GL1_ID'];
        newHeader['IN1_COMMENT'] = this.IN1_COMMENT;
        newHeader['IN1_NUMBER']  = line['IN1_NUMBER'];
        newHeader['TOTAL']       = Number(line['MD1_EXTENDED_VALUE']);
        newHeader['parts']       = [line];
        orders.push(newHeader);
      }
    });

    this.invoiceService.create(orders, this.IN1_NUMBER).subscribe( result => {
      if (result.success) {
        this.toastr.success(_lang('Operation was successfully executed!'), _lang('Service Success'), {
          timeOut: 3000,
        });
        this.router.navigate(['/invoice/credit-memos']);
      } else {
        this.toastr.error(_lang(result.errors.join('<br/>')), _lang('Error!'), {
          timeOut: 3000,
        });
      }
    });
  }

  onLinkAdd() {
    this.page.filterParams['LINKED_PART'] = 1;
    this.page.filterParams['MD1_ID'] = this.selectedInvoiceLine['MD1_ID'];
    this.page.filterParams['MD1_PART_NUMBER'] = null;

    if (this.addedParts && this.addedParts.length > 0) {
      let ids = [];
      this.addedParts.forEach( part => {
        ids.push(part['MD1_ID']);
      });
      this.page.filterParams['MD1_IDs'] = ids.join(',');
    }

    this.masterDataService.getCreditMemoFullPart(this.page).subscribe(res => {
        if (res['data'] && res['data'][0]) {
          let part = res['data'][0];
          part['IN2_LINK'] = this.selectedInvoiceLine['IN2_ID'];
          part['OR2_ID'] = this.selectedInvoiceLine['OR2_ID'];
          part['IN2_LINK_NAME'] = this.selectedHeader['IN1_NUMBER'] + ' - ' + this.selectedInvoiceLine['IN2_LINE'];
          part['MD1_UNIT_PRICE'] = this.selectedInvoiceLine['IN2_UNIT_PRICE'];
          part['MD1_EXTENDED_VALUE'] = this.selectedInvoiceLine['IN2_TOTAL_VALUE'];
          part['MD1_ORDER_QTY'] = this.selectedInvoiceLine['IN2_INVOICE_QTY'];
          this.addPartsToOrder([part]);
        }
      },
      error => {
        this.toastr.error(_lang('Linked Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
  }

  cancelClick() {
    this.location.back();
  }
}
