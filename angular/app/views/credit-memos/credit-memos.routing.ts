import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';
import { CreditMemosComponent } from './credit-memos.component';
import { CreditMemoAddComponent } from './credit-memo-add/credit-memo-add.component';

export const routes: Routes = [{
    path: '',
    component: CreditMemosComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Credit Memos'),
        permissions: {
            only: [permissions.INVOICES_READ]
        }
    }
},{
    path: 'add/:id',
    component: CreditMemoAddComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Create Credit Memo'),
        permissions: {
            only: [permissions.STORE_ORDERS_ADD]
        }
    }
}]