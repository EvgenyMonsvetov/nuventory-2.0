import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCategoriesComponent } from './master-categories.component';

describe('MasterCategoriesComponent', () => {
  let component: MasterCategoriesComponent;
  let fixture: ComponentFixture<MasterCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
