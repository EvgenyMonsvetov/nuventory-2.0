import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ConfirmationService } from 'primeng/api';
import { Page } from '../../model/page';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MasterCategoriesService } from '../../services/master-categories.service';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { UpdatePartsModalComponent } from '../../components/update-parts-modal/update-parts-modal.component';
import { _lang } from '../../pipes/lang';
import { LinkMcatalogModalComponent } from '../../components/link-mcatalog-modal/link-mcatalog-modal.component';

@Component({
  selector: 'app-master-categories',
  templateUrl: './master-categories.component.html',
  styleUrls: ['./master-categories.component.scss'],
  providers: [
    MasterCategoriesService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class MasterCategoriesComponent implements OnInit {

  private permissions = permissions;

  @ViewChild('masterDataView')    masterDataView: ElementRef;
  @ViewChild('actionbar')         actionbar: ElementRef;
  @ViewChild('content')           content: ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;
  @ViewChild('tableHeader')       tableHeader: ElementRef;
  @ViewChild('paginator')         paginator: ElementRef;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;
  private size: number = 50;
  private totalRecords: number = 0;

  private scrollHeight: string = '50px';

  constructor(private toastr: ToastrService,
              private masterCategoriesService: MasterCategoriesService,
              public  dialog: MatDialog,
              private router: Router,
              private elementRef: ElementRef,
              private confirmationService: ConfirmationService,
              private titleService: Title
  ) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
  }

  ngOnInit() {
      this.titleService.setTitle('Vendor Category');

      this.cols = [
        { field: 'CA0_NAME',             header: _lang('CA1_NAME'),        visible: true, export: {visible: true, checked: true}, width: 120 },
        { field: 'CA0_DESCRIPTION',      header: _lang('CA1_DESCRIPTION'), visible: true, export: {visible: true, checked: true}, width: 160 },
        { field: 'CA0_CREATED_ON',       header: _lang('#CreatedOn#'),     visible: true, export: {visible: true, checked: true}, width: 120 },
        { field: 'CA0_CREATED_BY_NAME',  header: _lang('#CreatedBy#'),     visible: true, export: {visible: true, checked: true}, width: 160 },
        { field: 'CA0_MODIFIED_ON',      header: _lang('#ModifiedOn#'),    visible: true, export: {visible: true, checked: true}, width: 120 },
        { field: 'CA0_MODIFIED_BY_NAME', header: _lang('#ModifiedBy#'),    visible: true, export: {visible: true, checked: true}, width: 160 }
      ];

      this.getItems();
  }

    getItems() {
        this.masterCategoriesService.getItems(this.page).subscribe(res => {
                this.items = res.data;
                this.totalRecords = res.count;
                setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});
                this.loading = false;
            },
            error => {
                this.loading = false;
                this.toastr.error(_lang('Маster Category') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                });
            });
    }

    getScrollHeight(): number {
        return (this.masterDataView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
            (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
            this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
    }

    onPage(event) {
        this.loading = true;
        this.page.size = event.rows;
        this.page.pageNumber = (event.first / event.rows);
        this.getItems();
    }

    buttonPress(e, col, value) {
        if ( e.keyCode === 13 ) {
            this.filterChanged(col, value);
        }
    }

    filterChanged(col, value) {
        this.loading = true;
        this.page.filterParams[col] = value;
        this.page.pageNumber = 0;
        this.getItems();
    }

    openLanguageEditor() {
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: window.innerWidth-60+'px',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: _lang('Master Category List'),
                componentRef: this,
                data: [],
                componentName: 'MasterCategoriesComponent'
            },
        });
    }

    addClick() {
        this.router.navigate(['master-categories/add']);
    }

    cloneRecord() {
      if (this.selected && this.selected.length === 1) {
          this.router.navigate(['master-categories/copy/' + this.selected[0].CA0_ID]);
      }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.masterCategoriesService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord() {
        if (this.selected && this.selected.length === 1)
            this.router.navigate(['master-categories/edit/' + this.selected[0]['CA0_ID']]);
    }

    onEdit(rowData) {
        this.router.navigate(['master-categories/edit/' + rowData['CA0_ID']]);
    }

    deleteRecord() {
      if (this.selected && this.selected[0] && this.selected[0].CA0_ID) {
          this.confirmationService.confirm({
              message: _lang('Are you sure that you want to perform this action?'),
              accept: () => {
                  this.loading = true;
                  var CA0_IDs = this.selected.map( item => item['CA0_ID'] );
                  this.masterCategoriesService.deleteMultiple(CA0_IDs).subscribe(result => {
                      this.getItems();
                  });
              }
          });
      }
    }

    updateLinkedParts() {
        const masterRecord = true; //should be based on current selected vendor (VD1_MANUFACTURER == '1')
        let dialogRef = this.dialog.open(UpdatePartsModalComponent, {
            width: window.innerWidth-100+'px',
            height: window.innerHeight-80+'px',
            role: 'dialog',
            data: {title: _lang('Update Parts'), items: this.selected, masterRecord: masterRecord, componentRef: this.elementRef},
        });
    }

    linkMCatalog() {
        let dialogRef = this.dialog.open(LinkMcatalogModalComponent, {
            width: window.innerWidth-100+'px',
            height: window.innerHeight-80+'px',
            role: 'dialog',
            data: {title: _lang('Link Manufacturer Catalog'), items: this.selected, componentRef: this.elementRef},
        });
    }
}
