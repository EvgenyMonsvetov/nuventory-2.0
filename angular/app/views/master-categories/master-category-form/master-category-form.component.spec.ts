import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCategoryFormComponent } from './master-category-form.component';

describe('MasterCategoryFormComponent', () => {
  let component: MasterCategoryFormComponent;
  let fixture: ComponentFixture<MasterCategoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCategoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
