import { Component, OnInit } from '@angular/core';
import { MasterCategoriesService } from '../../../services/master-categories.service';
import { ActivatedRoute } from '@angular/router';
import { permissions } from '../../../permissions';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { _lang } from '../../../pipes/lang';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-form',
  templateUrl: './master-category-form.component.html',
  styleUrls: ['./master-category-form.component.scss'],
    providers: [
    MasterCategoriesService
  ],
  preserveWhitespaces: true
})
export class MasterCategoryFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';
  private constants = Constants;
  private permissions = permissions;
  private record: any;
  constructor(
      private route: ActivatedRoute,
      private masterCategoriesService: MasterCategoriesService,
      public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.record = this.masterCategoriesService.initialize();
    this.action = this.route.snapshot.url[0].toString();
    this.masterCategoriesService.getById(this.route.snapshot.params.id || 0).subscribe( data => {
        this.record = data;
    });
  }

  cancelClick() {
    window.history.back();
  }

  openLanguageEditor() {
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('Master Category Form'),
            componentRef: this,
            data: [],
            componentName: 'MasterCategoryFormComponent'
          },
      });
  }

  saveClick() {
    if (this.action === 'add' || this.action === 'copy') {
      this.masterCategoriesService.create(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.masterCategoriesService.update(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

}
