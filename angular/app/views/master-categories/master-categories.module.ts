import { NgModule  } from '@angular/core';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RouterModule  } from '@angular/router';
import { CompanyService } from '../../services/company.service';
import { MasterCategoriesComponent } from './master-categories.component';
import { routes } from './master-categories.routing';
import { MasterCategoryFormComponent } from './master-category-form/master-category-form.component';
import { SharedModule } from '../../modules/shared.module';
import { MasterCatalogService } from '../../services/master-catalog.service';
import { MasterVendorService } from '../../services/master-vendor.service';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  providers: [
      CompanyService,
      MasterCatalogService,
      MasterVendorService
  ],
  declarations: [
      MasterCategoriesComponent,
      MasterCategoryFormComponent
  ]
})
export class MasterCategoriesModule { }
