import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions  } from './../../permissions';
import { MasterCategoriesComponent } from './master-categories.component';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { MasterCategoryFormComponent } from './master-category-form/master-category-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: MasterCategoriesComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Vendor Category'),
        permissions: {
            only: [permissions.MASTER_CATEGORIES_READ]
        }
    }
},{
    path: 'add',
    component: MasterCategoryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Add'),
        permissions: {
            only: [permissions.MASTER_CATEGORIES_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: MasterCategoryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Edit#'),
        permissions: {
            only: [permissions.MASTER_CATEGORIES_EDIT]
        }
    }
},{
    path: 'copy/:id',
    component: MasterCategoryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Clone'),
        permissions: {
            only: [permissions.MASTER_CATEGORIES_ADD]
        }
    }
}];