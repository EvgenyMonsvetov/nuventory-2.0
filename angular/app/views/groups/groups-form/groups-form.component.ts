import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { permissions } from '../../../permissions';
import { GroupsService } from '../../../services/groups';
import { MatDialog } from '@angular/material';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { _lang } from '../../../pipes/lang';
import { RolesService } from '../../rights/roles/roles.service';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-groups-form',
  templateUrl: './groups-form.component.html',
  styleUrls: ['./groups-form.component.scss'],
  providers: [
    GroupsService,
    RolesService
  ],
  preserveWhitespaces: true
})
export class GroupsFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';
  private accessLevelList: Array<any> = [];
  private roles: Array<any> = [];
  private constants = Constants;
  private permissions = permissions;
  private record: any;
  constructor(
      private route: ActivatedRoute,
      private groupsService: GroupsService,
      private rolesService: RolesService,
      public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.accessLevelList = this.groupsService.getAccessLevelList();

    this.rolesService.getAll(null).subscribe( res => {
      this.roles = res.data;
    });

    this.record = this.groupsService.initialize();
    this.action = this.route.snapshot.url[0].toString();
    this.groupsService.getById(this.route.snapshot.params.id || 0).subscribe( data => {
        this.record = data;
    });
  }

  cancelClick() {
    window.history.back();
  }

  openLanguageEditor() {
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('Group Form'),
            componentRef: this,
            data: [],
            componentName: 'GroupsFormComponent'
          },
      });
  }

  saveClick() {
    if (!this.record['ROLE_NAME']) {
      this.errors = [_lang('Role is required')];
    } else {
      if (this.action === 'add' || this.action === 'copy') {
        this.groupsService.create(this.record).subscribe(result => {
          if (result.success) {
            this.cancelClick();
          } else {
            this.errors = result.errors;
          }
        });
      } else if (this.action === 'edit') {
        this.groupsService.update(this.record).subscribe(result => {
          if (result.success) {
            this.cancelClick();
          } else {
            this.errors = result.errors;
          }
        });
      }
    }
  }
}
