import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPermissionsModule } from 'ngx-permissions';
import { GroupsComponent} from "./groups.component";
import { GroupsFormComponent } from "./groups-form/groups-form.component";
import { SharedModule } from '../../modules/shared.module';
import { routes } from "./groups.routing";

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
    declarations: [
        GroupsComponent,
        GroupsFormComponent
    ]
})
export class GroupsModule { }