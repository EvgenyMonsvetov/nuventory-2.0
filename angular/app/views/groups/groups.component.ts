import { Component, OnInit } from '@angular/core';
import { GroupsService } from '../../services/groups';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { permissions } from '../../permissions';
import { TitleService } from '../../services/title.service';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss'],
  providers: [
    GroupsService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class GroupsComponent implements OnInit {

    private permissions = permissions;
    private accessLevelList: Array<any> = [];

    cols: any[] = [];
    items: any[] = [];

    private loading = false;
    private page = new Page();
    private selected: any = null;

    constructor(
        private toastr: ToastrService,
        private groupsService: GroupsService,
        public dialog: MatDialog,
        private router: Router,
        private confirmationService: ConfirmationService,
        private titleService: TitleService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.titleService.setNewTitle(this.route);

        this.accessLevelList = this.groupsService.getAccessLevelList();

        this.cols = [
            { field: 'GR1_NAME',         header: _lang('GR1_NAME'),         visible: true, export: {visible: true, checked: true} },
            { field: 'GR1_DESC',         header: _lang('GR1_DESC'),         visible: true, export: {visible: true, checked: true} },
            { field: 'GR1_ACCESS_LEVEL', header: _lang('GR1_ACCESS_LEVEL'), visible: true, export: {visible: true, checked: true} },
            { field: 'ROLE_NAME',        header: _lang('Role'),             visible: true, export: {visible: true, checked: true} }
        ];

        this.setPage({offset: 0});
    }

    setPage(pageInfo) {
        this.loading = true;
        this.page.pageNumber = pageInfo.offset;

        this.groupsService.getItems(this.page).subscribe(result => {

            this.page.totalElements = result.count;
            this.page.totalPages    = result.count / this.page.size;
            this.items = result.data.map( item => {
                var accessLevel = this.accessLevelList.filter( item2 => {
                    return parseInt(item2['GR1_ACCESS_LEVEL']) === parseInt(item['GR1_ACCESS_LEVEL']);
                });
                item['GR1_ACCESS_LEVEL'] = accessLevel[0]['GR1_ACCESS_LEVEL_DESC'];
                return item;
            });
            this.loading = false;
        },
        error => {
            this.loading = false;
            this.toastr.error(_lang('Groups') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                timeOut: 3000,
            });
        });
    }

    openLanguageEditor() {
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: window.innerWidth-60+'px',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: _lang('Groups List'),
                componentRef: this,
                data: this.accessLevelList.map(item=>{
                    return item['GR1_ACCESS_LEVEL_LANG'];
                }),
                componentName: 'GroupsComponent'
            },
        });
    }

    addClick() {
        this.router.navigate(['groups/add']);
    }

    cloneRecord() {
        if (this.selected) {
            this.router.navigate(['groups/copy/' + this.selected.GR1_ID]);
        }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.groupsService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord(rowData?) {
      if (this.selected || rowData) {
          const GR1_ID = rowData && rowData.GR1_ID ? rowData.GR1_ID : this.selected.GR1_ID;
          this.router.navigate(['groups/edit/' + GR1_ID]);
      }
    }

    deleteRecord() {
        if (this.selected) {
            this.confirmationService.confirm({
                message: _lang('Are you sure that you want to perform this action?'),
                accept: () => {
                    this.loading = true;
                    this.groupsService.delete(this.selected.GR1_ID).subscribe(result => {
                        if (result['success']) {
                            this.items = this.items.filter(item => {
                                return item.GR1_ID === this.selected.GR1_ID ? false : true;
                            });
                        } else {
                            this.toastr.error(_lang('This group contains') + ' ' + result['usersCount'] + ' ' + _lang('users') + '. ' +
                                _lang('Please unlink users from the group before deleting a group.'), _lang('Service Error'), {
                                timeOut: 10000,
                            });
                        }
                        this.loading = false;
                    });
                }
            });
        }
    }
}
