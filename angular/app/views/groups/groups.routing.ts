import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions  } from './../../permissions';
import { GroupsComponent } from './groups.component';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { GroupsFormComponent } from './groups-form/groups-form.component';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: GroupsComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Group List'),
        permissions: {
            only: [permissions.DASHBOARD_READ]
        }
    }
},{
    path: 'add',
    component: GroupsFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Add'),
        permissions: {
            only: [permissions.DASHBOARD_READ]
        }
    }
},{
    path: 'edit/:id',
    component: GroupsFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Edit#'),
        permissions: {
            only: [permissions.DASHBOARD_READ]
        }
    }
},{
    path: 'copy/:id',
    component: GroupsFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Clone'),
        permissions: {
            only: [permissions.DASHBOARD_READ]
        }
    }
}];