import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { C00categoryComponent} from './c00category.component';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { C00categoryFormComponent } from './c00category-form/c00category-form.component';

export const routes: Routes = [{
    path: '',
    component: C00categoryComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: 'Categories',
        permissions: {
            only: [permissions.NUVENTORY_CATEGORIES_READ]
        }
    },
},{
    path: 'add',
    component: C00categoryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: 'Add category',
        permissions: {
            only: [permissions.NUVENTORY_CATEGORIES_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: C00categoryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: 'Edit category',
        permissions: {
            only: [permissions.NUVENTORY_CATEGORIES_EDIT]
        }
    }
},{
    path: 'copy/:id',
    component: C00categoryFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: 'Clone category',
        permissions: {
            only: [permissions.NUVENTORY_CATEGORIES_ADD]
        }
    }
}];