import { Component, OnInit } from '@angular/core';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { c00CategoryService } from '../../services/c00category.service';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { InventoryFinderModalComponent } from '../../components/inventory-finder-modal/inventory-finder-modal.component';
import { TitleService } from 'app/services/title.service';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-c00category',
  templateUrl: './c00category.component.html',
  styleUrls: ['./c00category.component.scss'],
  providers: [
      c00CategoryService,
      ConfirmationService
  ],
  preserveWhitespaces: true
})
export class C00categoryComponent implements OnInit {

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;

  private menuItems: MenuItem[] = [
        {label: 'Edit', icon: 'pi pi-fw pi-plus', routerLink:['/c00category/edit/10']},
        {label: 'Open', icon: 'pi pi-fw pi-download'},
        {label: 'Undo', icon: 'pi pi-fw pi-refresh'}
  ];

  constructor(
    private toastr: ToastrService,
    private c00CategoryService: c00CategoryService,
    public  dialog: MatDialog,
    private router: Router,
    private confirmationService: ConfirmationService,
    private titleService: TitleService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);

      this.cols = [
        { field: 'C00_NAME',             header: _lang('Nuventory Category'),   visible: true, export: {visible: true, checked: true} },
        { field: 'C00_DESCRIPTION',      header: _lang('CA1_DESCRIPTION'),      visible: true, export: {visible: true, checked: true} },
        { field: 'C00_CREATED_ON',       header: _lang('#CreatedOn#'),          visible: true, export: {visible: true, checked: true}, width: 180 },
        { field: 'C00_CREATED_BY_NAME',  header: _lang('#CreatedBy#'),          visible: true, export: {visible: true, checked: true}  },
        { field: 'C00_MODIFIED_ON',      header: _lang('#ModifiedOn#'),         visible: true, export: {visible: true, checked: true}, width: 180 },
        { field: 'C00_MODIFIED_BY_NAME', header: _lang('#ModifiedBy#'),         visible: true, export: {visible: true, checked: true} }
    ];

    this.setPage({offset: 0});
  }

  setPage(pageInfo) {
      this.loading = true;
      this.page.pageNumber = pageInfo.offset;

      this.c00CategoryService.getItems(this.page).subscribe(result => {

          this.page.totalElements = result.count;
          this.page.totalPages    = result.count / this.page.size;
          this.items = result.data;
          this.loading = false;
      },
      error => {
          this.loading = false;
          this.toastr.error(_lang('Customers') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
          });
      });
    }

    openLanguageEditor() {
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: window.innerWidth-60+'px',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: _lang('Customer Data List'),
                componentRef: this,
                data: [],
                componentName: 'C00categoryComponent'
            },
        });
    }

    assignCategory() {
      let dialogRef = this.dialog.open(InventoryFinderModalComponent, {
            width: window.innerWidth-100+'px',
            height: window.innerHeight-100+'px',
            role: 'dialog',
            data: {title: _lang('Inventory Category List')},
        });
    }

    addClick() {
        this.router.navigate(['c00category/add']);
    }

    cloneRecord() {
      if(this.selected){
          this.router.navigate(['c00category/copy/' + this.selected.C00_ID]);
      }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.c00CategoryService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord(rowData?) {
      if (this.selected || rowData){
          const C00_ID = (rowData && rowData.C00_ID) ? rowData.C00_ID : this.selected[0].C00_ID;
          this.router.navigate(['c00category/edit/' + C00_ID]);
      }
    }

    deleteRecord() {
      if (this.selected && this.selected[0] && this.selected[0]['C00_ID']) {
          this.confirmationService.confirm({
              message: _lang('Are you sure that you want to perform this action?'),
              accept: () => {
                  this.loading = true;
                  var C00_IDs = this.selected.map( item => item['C00_ID'] );
                  this.c00CategoryService.deleteMultiple(C00_IDs).subscribe(result => {
                      this.setPage({offset: 0});
                  });
              }
          });
      }
    }
}
