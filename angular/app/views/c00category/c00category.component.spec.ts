import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { C00categoryComponent } from './c00category.component';

describe('C00categoryComponent', () => {
  let component: C00categoryComponent;
  let fixture: ComponentFixture<C00categoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ C00categoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(C00categoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
