import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPermissionsModule } from 'ngx-permissions';
import { routes } from './c00category.routing';
import { C00categoryComponent } from './c00category.component';
import { C00categoryFormComponent } from './c00category-form/c00category-form.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
    providers: [  ],
    declarations: [ C00categoryComponent, C00categoryFormComponent ]
})
export class C00categoryModule { }
