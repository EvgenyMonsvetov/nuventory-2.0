import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { c00CategoryService } from '../../../services/c00category.service';
import { permissions } from '../../../permissions';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../../pipes/lang';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-form',
  templateUrl: './c00category-form.component.html',
  styleUrls: ['./c00category-form.component.scss'],
  preserveWhitespaces: true,
  providers: [
    c00CategoryService
  ],
})
export class C00categoryFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private constants = Constants;
  private record: any;
  constructor(
      private route: ActivatedRoute,
      private c00CategoryService: c00CategoryService,
      public dialog: MatDialog,
      private titleService: TitleService
  ) {

  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    
    this.record = this.c00CategoryService.initialize();
    this.action = this.route.snapshot.url[0].toString();
    this.c00CategoryService.getById(this.route.snapshot.params.id || 0).subscribe( data => {
        this.record = data;
    });
  }

  cancelClick(){
    window.history.back();
  }

  openLanguageEditor(){
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('Inventory Category Form'),
            componentRef: this,
            data: [],
            componentName: 'C00categoryFormComponent'
          },
      });
  }

  saveClick(){
    if (this.action === 'add' || this.action === 'copy') {
      this.c00CategoryService.create(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.c00CategoryService.update(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }
}
