import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { C00categoryFormComponent } from './c00category-form.component';

describe('C00categoryFormComponent', () => {
  let component: C00categoryFormComponent;
  let fixture: ComponentFixture<C00categoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ C00categoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(C00categoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
