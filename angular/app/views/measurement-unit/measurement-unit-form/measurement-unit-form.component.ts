import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { permissions } from '../../../permissions';
import { MeasurementUnitService } from '../../../services/measurement.unit.service';
import { LanguageEditorModalComponent } from '../../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { TitleService } from 'app/services/title.service';
import { _lang } from '../../../pipes/lang';
import {Constants} from "../../../constants";

@Component({
  selector: 'app-form',
  templateUrl: './measurement-unit-form.component.html',
  styleUrls: ['./measurement-unit-form.component.scss'],
  providers: [
    MeasurementUnitService
  ],
  preserveWhitespaces: true
})
export class MeasurementUnitFormComponent implements OnInit {

  private errors: Array<any> = [];
  private action: String = '';

  private permissions = permissions;
  private record: any;
  private constants = Constants;

  constructor(
      private route: ActivatedRoute,
      private measurementUnitService: MeasurementUnitService,
      public  dialog: MatDialog,
      private titleService: TitleService
  ) {

  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.record = this.measurementUnitService.initialize();
    this.action = this.route.snapshot.url[0].toString();
    this.measurementUnitService.getById(this.route.snapshot.params.id || 0).subscribe( data => {
        this.record = data;
    });
  }

  cancelClick() {
    window.history.back();
  }

  openLanguageEditor() {
      let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
            title: _lang('Unit of Measure Form'),
            componentRef: this,
            data: [],
            componentName: 'MeasurementUnitFormComponent'
          },
      });
  }

  saveClick() {
    if (this.action === 'add' || this.action === 'copy') {
      this.measurementUnitService.create(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    } else if (this.action === 'edit') {
      this.measurementUnitService.update(this.record).subscribe(result => {
        if (result.success) {
          this.cancelClick();
        } else {
          this.errors = result.errors;
        }
      });
    }
  }

}
