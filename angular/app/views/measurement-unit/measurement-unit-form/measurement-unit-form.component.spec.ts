import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementUnitFormComponent } from './measurement-unit-form.component';

describe('MeasurementUnitFormComponent', () => {
  let component: MeasurementUnitFormComponent;
  let fixture: ComponentFixture<MeasurementUnitFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasurementUnitFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasurementUnitFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
