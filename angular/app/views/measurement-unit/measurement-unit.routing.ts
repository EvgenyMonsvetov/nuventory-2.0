import { Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { permissions } from './../../permissions';
import { MeasurementUnitComponent } from './measurement-unit.component';
import { MeasurementUnitFormComponent } from './measurement-unit-form/measurement-unit-form.component';
import { AuthGuardService as AuthGuard } from './../../auth-guard.service';
import { _lang } from '../../pipes/lang';

export const routes: Routes = [{
    path: '',
    component: MeasurementUnitComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Units of measure'),
        permissions: {
            only: [permissions.UNITS_MEASURE_READ]
        }
    }
},{
    path: 'add',
    component: MeasurementUnitFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Add'),
        permissions: {
            only: [permissions.UNITS_MEASURE_ADD]
        }
    }
},{
    path: 'edit/:id',
    component: MeasurementUnitFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('#Edit#'),
        permissions: {
            only: [permissions.UNITS_MEASURE_EDIT]
        }
    }
},{
    path: 'copy/:id',
    component: MeasurementUnitFormComponent,
    canActivate: [AuthGuard, NgxPermissionsGuard],
    data: {
        title: _lang('Clone'),
        permissions: {
            only: [permissions.UNITS_MEASURE_ADD]
        }
    }
}];