import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MeasurementUnitService } from '../../services/measurement.unit.service';
import { ConfirmationService } from 'primeng/primeng';
import { TitleService } from 'app/services/title.service';
import { ExportModalComponent } from '../../components/export-modal/export-modal.component';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-measurement-unit',
  templateUrl: './measurement-unit.component.html',
  styleUrls: ['./measurement-unit.component.scss'],
  providers: [
    MeasurementUnitService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class MeasurementUnitComponent implements OnInit {

  private permissions = permissions;

  @ViewChild('measurementUnitView') measurementUnitView: ElementRef;
  @ViewChild('actionbar')         actionbar: ElementRef;
  @ViewChild('content')           content: ElementRef;
  @ViewChild('tableHeader')       tableHeader: ElementRef;
  @ViewChild('tableFilter')       tableFilter: ElementRef;
  @ViewChild('paginator')         paginator: ElementRef;

  private scrollHeight: string = '50px';

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;
  private size: number = 50;
  private totalRecords: number = 0;

  constructor(
    private toastr: ToastrService,
    private measurementUnitService: MeasurementUnitService,
    public  dialog: MatDialog,
    private router: Router,
    private confirmationService: ConfirmationService,
    private titleService: TitleService,
    private route: ActivatedRoute) {
      this.page.size = this.size;
      this.page.pageNumber = 0;
  }

  ngOnInit() {
      this.titleService.setNewTitle(this.route);

      this.cols = [
          { field: 'UM1_SHORT_CODE',       header: _lang('UM1_SHORT_CODE'),  visible: true, export: {visible: true, checked: true} },
          { field: 'UM1_NAME',             header: _lang('UM1_NAME'),        visible: true, export: {visible: true, checked: true} },
          { field: 'UM1_SIZE',             header: _lang('UM1_SIZE'),        visible: true, export: {visible: true, checked: true} },
          { field: 'UM1_CREATED_ON',       header: _lang('#CreatedOn#'),     visible: true, export: {visible: true, checked: true}, width: 180  },
          { field: 'UM1_CREATED_BY_NAME',  header: _lang('#CreatedBy#'),     visible: true, export: {visible: true, checked: true}  },
          { field: 'UM1_MODIFIED_ON',      header: _lang('#ModifiedOn#'),    visible: true, export: {visible: true, checked: true}, width: 180 },
          { field: 'UM1_MODIFIED_BY_NAME', header: _lang('#ModifiedBy#'),    visible: true, export: {visible: true, checked: true} }
      ];

      this.getItems();
  }

    getItems() {
        this.measurementUnitService.getItems(this.page).subscribe(res => {
                this.items = res.data;
                this.totalRecords = res.count;

                setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';});

                this.loading = false;
            },
            error => {
                this.loading = false;
                this.toastr.error(_lang('Units of measure') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                    timeOut: 3000,
                });
            });
    }

    getScrollHeight(): number {
        return (this.measurementUnitView.nativeElement.parentElement.parentElement.parentElement.offsetHeight) -
            (this.actionbar.nativeElement.offsetHeight + 48) - (this.tableHeader.nativeElement.offsetHeight +
            this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight);
    }

    onPage(event) {
        this.loading = true;
        this.page.size = event.rows;
        this.page.pageNumber = (event.first / event.rows);
        this.getItems();
    }

    buttonPress(e, col, value) {
        if ( e.keyCode === 13 ) {
            this.filterChanged(col, value);
        }
    }

    filterChanged(col, value) {
        this.loading = true;
        this.page.pageNumber = 0;
        this.getItems();
    }

    openLanguageEditor() {
        let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
            width: window.innerWidth-60+'px',
            height: window.innerHeight-40+'px',
            role: 'dialog',
            data: {
                title: _lang('Unit of Measure List'),
                componentRef: this,
                data: [],
                componentName: 'MeasurementUnitComponent'
            },
        });
    }

    addClick() {
        this.router.navigate(['measurement-unit/add']);
    }

    cloneRecord() {
      if (this.selected) {
          this.router.navigate(['measurement-unit/copy/' + this.selected.UM1_ID]);
      }
    }

    exportClick() {
        let dialogRef = this.dialog.open(ExportModalComponent, {
            width: 400 + 'px',
            height: window.innerHeight - 40 + 'px',
            role: 'dialog',
            data: {cols: this.cols},
        });

        dialogRef.beforeClose().subscribe( columns => {
            if (columns) {
                this.page.filterParams['columns'] = columns;

                this.measurementUnitService.downloadXlsx(this.page).subscribe( result => {
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                            timeOut: 3000,
                        });
                    });
            }
        })
    }

    editRecord(rowData?) {
      if (this.selected || rowData) {
          var UM1_ID = (rowData && rowData.UM1_ID) ? rowData.UM1_ID : this.selected[0].UM1_ID;
          this.router.navigate(['measurement-unit/edit/' + UM1_ID]);
      }
    }

    deleteRecord() {
        if (this.selected.length && this.selected[0]['UM1_ID']) {
          this.confirmationService.confirm({
              message: _lang('Are you sure that you want to perform this action?'),
              accept: () => {
                  this.loading = true;
                  var UM1_IDs = this.selected.map( item => item['UM1_ID'] );
                  this.measurementUnitService.deleteMultiple(UM1_IDs).subscribe(result => {
                          this.getItems();
                      },
                      error => {
                          this.loading = false;
                          this.toastr.error(_lang('On delete measurement unit error occured'), _lang('Service Error'), {
                              timeOut: 3000,
                          });
                      });
              }
          });
      }
    }
}
