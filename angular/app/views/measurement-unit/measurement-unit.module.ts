import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPermissionsModule } from 'ngx-permissions';
import { routes } from './measurement-unit.routing';
import { MeasurementUnitComponent } from './measurement-unit.component';
import { MeasurementUnitFormComponent } from './measurement-unit-form/measurement-unit-form.component';
import { SharedModule} from '../../modules/shared.module';

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      NgxPermissionsModule.forChild(),
      SharedModule
  ],
  declarations: [
      MeasurementUnitComponent,
      MeasurementUnitFormComponent
  ]
})
export class MeasurementUnitModule { }
