import { Routes } from '@angular/router';
import { SignupComponent } from './auth/signup/signup.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AuthGuardService as AuthGuard } from './auth-guard.service';
import { _lang } from './pipes/lang';
import { PrintRackComponent } from './views/print-rack/print-rack.component';
import { permissions } from './permissions';

export const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        data: {
            title: _lang('Home')
        },
        children: [{
            path: 'userlog',
            loadChildren: './views/userlog/userlog.module#UserlogModule',
            canActivate: [NgxPermissionsGuard],
            data: {
                permissions: {
                    only: []
                }
            }
        }]
    },
    {
        path: 'master-data',
        loadChildren: './views/master-data/master-data.module#MasterDataModule'
    },
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'c00category',
        loadChildren: './views/c00category/c00category.module#C00categoryModule'
    },
    {
        path: 'categories',
        loadChildren: './views/categories/categories.module#CategoriesModule'
    },
    {
        path: 'companies',
        loadChildren: './views/companies/companies.module#CompaniesModule'
    },
    {
        path: 'countries',
        loadChildren: './views/countries/countries.module#CountriesModule'
    },
    {
        path: 'customers',
        loadChildren: './views/customers/customers.module#CustomersModule'
    },
    {
        path: 'setup',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'reporting',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'edi-type',
        loadChildren: './views/editype/editype.module#EdiTypeModule'
    },
    {
        path: 'gl-codes',
        loadChildren: './views/gl-codes/gl-codes.module#GlCodesModule'
    },
    {
        path: 'language-editor',
        loadChildren: './views/language-editor/language-editor.module#LanguageEditorModule'
    },
    {
        path: 'languages',
        loadChildren: './views/languages/languages.module#LanguagesModule'
    },
    {
        path: 'master-categories',
        loadChildren: './views/master-categories/master-categories.module#MasterCategoriesModule'
    },
    {
        path: 'master-vendors',
        loadChildren: './views/master-vendors/master-vendors.module#MasterVendorsModule'
    },
    {
        path: 'master-catalogs',
        loadChildren: './views/master-catalogs/master-catalogs.module#MasterCatalogsModule'
    },
    {
        path: 'measurement-unit',
        loadChildren: './views/measurement-unit/measurement-unit.module#MeasurementUnitModule'
    },
    {
        path: 'rights',
        loadChildren: './views/rights/rights.module#RightsModule'
    },
    {
        path: 'shops',
        loadChildren: './views/shops/shops.module#ShopsModule'
    },
    {
        path: 'user',
        loadChildren: './views/user/user.module#UserModule'
    },
    {
        path: 'vendors',
        loadChildren: './views/vendors/vendors.module#VendorsModule'
    },
    {
        path: 'technicians',
        loadChildren: './views/technicians/technicians.module#TechniciansModule'
    },
    {
        path: 'hours',
        loadChildren: './views/hours/hours.module#HoursModule'
    },
    {
        path: 'groups',
        loadChildren: './views/groups/groups.module#GroupsModule'
    },
    {
        path: 'store-orders',
        loadChildren: './views/store-orders/store-orders.module#StoreOrdersModule'
    },
    {
        path: 'tech-scans',
        loadChildren: './views/tech-scans/tech-scans.module#TechScansModule'
    },
    {
        path: 'job-tickets',
        loadChildren: './views/job-tickets/job-tickets.module#JobTicketsModule'
    },
    {
        path: 'purchase-orders',
        loadChildren: './views/purchase-orders/purchase-orders.module#PurchaseOrdersModule'
    },
    {
        path: 'invoices',
        loadChildren: './views/invoices/invoices.module#InvoicesModule'
    },
    {
        path: 'job-invoices',
        loadChildren: './views/job-invoices/job-invoices.module#JobInvoicesModule'
    },
    {
        path: 'invoice/credit-memos',
        loadChildren: './views/credit-memos/credit-memos.module#CreditMemosModule'
    },
    {
        path: 'statements',
        loadChildren: './views/statements/statements.module#StatementsModule'
    },
    {
        path: 'master-data/inventory-optimization',
        loadChildren: './views/inventory-optimization/inventory-optimization.module#InventoryOptimizationModule'
    },
    {
        path: 'master-data/physical-inventory',
        loadChildren: './views/inventory-optimization/inventory-optimization.module#InventoryOptimizationModule'
    },
    {
        path: 'inventory/inventory-activity-log',
        loadChildren: './views/inventory-activity-log/inventory-activity-log.module#InventoryActivityLogModule'
    },
    {
        path: 'master-data/tech-usage-report',
        loadChildren: './views/tech-usage-report/tech-usage-report.module#TechUsageReportModule'
    },
    {
        path: 'master-data/tech-usage-mtd-report',
        loadChildren: './views/tech-usage-report/tech-usage-report.module#TechUsageReportModule'
    },
    {
        path: 'report/manual-inventory-edits-report',
        loadChildren: './views/manual-inventory-edits-report/manual-inventory-edits-report.module#ManualInventoryEditsReportModule'
    },
    {
        path: 'below-minimum-report',
        loadChildren: './views/master-data/master-data.module#MasterDataModule'
    },
    {
        path: 'report/sales-report',
        loadChildren: './views/sales-report/sales-report.module#SalesReportModule'
    },
    {
        path: 'report/gross-profit-report',
        loadChildren: './views/gross-profit-report/gross-profit-report.module#GrossProfitReportModule'
    },
    {
        path: 'report/summary-report',
        loadChildren: './views/summary-report/summary-report.module#SummaryReportModule'
    },
    {
        path: 'import-report-data',
        loadChildren: './views/import-report-data/import-report-data.module#ImportReportDataModule'
    },
    {
        path: 'raw-sales-data-store-month',
        loadChildren: './views/raw-sales-data-store-month/raw-sales-data-store-month.module#RawSalesDataStoreMonthModule'
    },
    {
        path: 'material-budget-setup',
        loadChildren: './views/material-budget-setup/material-budget-setup.module#MaterialBudgetSetupModule'
    },
    {
        path: 'budgets',
        loadChildren: './views/budgets/budgets.module#BudgetsModule'
    },
    {
        path: 'purchase-orders/materials-budget-report',
        loadChildren: './views/materials-budget-report/materials-budget-report.module#MaterialsBudgetReportModule'
    },
    {
        path: 'master-data/print-rack',
        component: PrintRackComponent
    },
    {
        path: 'spam',
        loadChildren: './views/spam/spam.module#SpamModule'
    },
    {
        path: 'voc-usage-report',
        loadChildren: './views/voc-usage-report/voc-usage-report.module#VocUsageReportModule'
    },
    {
        path: 'report/pm-gross-profit-by-shop-report',
        loadChildren: './views/pm-gross-profit-by-shop-report/pm-gross-profit-by-shop-report.module#PmGrossProfitByShopReportModule'
    },
    {
        path: 'report/paint-labor-vs-sales-report',
        loadChildren: './views/average-circle-chart-report/average-circle-chart-report.module#AverageCircleChartReportModule',
        data: {
            title: _lang('Paint Labor vs Sales Report'),
            permissions: {
                only: [permissions.PAINT_LABOR_VS_SALES_READ]
            }
        }
    },
    {
        path: 'report/paint-materials-gp-report',
        loadChildren: './views/average-circle-chart-report/average-circle-chart-report.module#AverageCircleChartReportModule',
        data: {
            title: _lang('Paint & Materials GP Report'),
            permissions: {
                only: [permissions.PAINT_MATERIALS_GP_READ]
            }
        }
    }
    ,
    {
        path: 'report/paint-materials-usage-total-report',
        loadChildren: './views/high-bar-chart-report/high-bar-chart-report.module#HighBarChartReportModule',
        data: {
            title: _lang('Paint Materials Usage Total Report'),
            permissions: {
                only: [permissions.PAINT_MATERIALS_USAGE_TOTAL]
            }
        }
    },
    {
        path: 'report/projected-po-usage-report',
        loadChildren: './views/projected-po-usage-report/projected-po-usage-report.module#ProjectedPoUsageReportModule',
        data: {
            title: _lang('Projected PO Usage Report'),
            permissions: {
                only: [permissions.PROJECTED_PO_USAGE_READ]
            }
        }
    },
    {
        path: 'charts-slide-show/dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'charts-slide-show/projected-po-usage-report',
        loadChildren: './views/projected-po-usage-report/projected-po-usage-report.module#ProjectedPoUsageReportModule',
        data: {
            title: _lang('Projected PO Usage Report'),
            permissions: {
                only: [permissions.PROJECTED_PO_USAGE_READ]
            }
        }
    },
    {
        path: 'charts-slide-show/tech-usage-mtd-report',
        loadChildren: './views/tech-usage-report/tech-usage-report.module#TechUsageReportModule'
    },
    {
        path: 'charts-slide-show/paint-labor-vs-sales-report',
        loadChildren: './views/average-circle-chart-report/average-circle-chart-report.module#AverageCircleChartReportModule',
        data: {
            title: _lang('Paint Labor vs Sales Report'),
            permissions: {
                only: [permissions.PAINT_LABOR_VS_SALES_READ]
            }
        }
    },
    {
        path: 'charts-slide-show/paint-materials-gp-report',
        loadChildren: './views/average-circle-chart-report/average-circle-chart-report.module#AverageCircleChartReportModule',
        data: {
            title: _lang('Paint & Materials GP Report'),
            permissions: {
                only: [permissions.PAINT_MATERIALS_GP_READ]
            }
        }
    }
];
