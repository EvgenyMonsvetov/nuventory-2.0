import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { NgxPermissionsService } from 'ngx-permissions';
import { permissions } from './permissions';

@Injectable()
export class UserGuardService implements CanActivate {

    constructor(public auth: AuthService,
                public router: Router,
                public route: ActivatedRoute,
                private ngxPermissionsService: NgxPermissionsService) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        const userId = this.auth.userId;
        const id = parseInt(next.params['id']);
        const url = state.url.replace(next.params['id'], '');
        const profilePermission = this.ngxPermissionsService.getPermission(permissions.PROFILE_READ);

        return userId === id
            || (url === '/user/profile/' || url === '/user/changepassword/') && profilePermission !== undefined;
    }
}
