import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../views/pipes/pipes.module';
import { MaterialModule } from '../views/material/material.module';
import { TableModule } from 'primeng/table';
import { MenuModule } from 'primeng/menu';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule, PaginatorModule } from 'primeng/primeng';
import { TitleService } from '../services/title.service';
import { LangPipe } from '../views/pipes/lang.pipe';
import { TranslationJsonService } from '../services/translation-json.service';
import { CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
    exports: [
        RouterModule,
        CommonModule,
        FormsModule,
        TableModule,
        ReactiveFormsModule,
        ConfirmDialogModule,
        PipesModule,
        MaterialModule,
        NgxLoadingModule,
        NgxPaginationModule,
        NgxDatatableModule,
        NgMultiSelectDropDownModule,
        NgxMatSelectSearchModule
    ],
    imports: [
        NgbModule,
        MenuModule,
        DialogModule,
        PaginatorModule
    ],
    providers: [
        TitleService,
        LangPipe,
        TranslationJsonService
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                TitleService,
                LangPipe,
                TranslationJsonService
            ]
        };
    }
}