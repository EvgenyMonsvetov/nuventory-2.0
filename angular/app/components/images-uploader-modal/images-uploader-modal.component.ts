import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { UploadEvent, FileSystemFileEntry } from 'ngx-file-drop';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LangPipe } from '../../views/pipes/lang.pipe';
import { ToastrService } from 'ngx-toastr';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-images-uploader-modal',
  templateUrl: './images-uploader-modal.component.html',
  styleUrls: ['./images-uploader-modal.component.scss'],
  preserveWhitespaces: true
})
export class ImagesUploaderModalComponent implements OnInit {
  private permissions = permissions;
  private environment = environment;

  private fileToUpload: File = null;
  private localUrl: any[] = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('uploadModal') uploadModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('actionbar')    actionbar:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;

  constructor(public dialogRef: MatDialogRef<ImagesUploaderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private lang: LangPipe,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit() {
    setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';});
  }

  getContentDialogHeight(): number{
    return (this.uploadModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight + 12);
  }

  getScrollHeight(): number{
    return this.getContentDialogHeight() - (this.actionbar.nativeElement.offsetHeight + 22 );
  }

  public dropped(event: UploadEvent) {
    if (event.files && event.files[0]) {
      const droppedFile = event.files[0];
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          if (file.size <= environment.maxUploadZipFile) {
            this.fileToUpload = file;
          } else {
            this.fileToUpload = null;
          }
        });
      }
    }
  }

  handleFileInput(files: FileList) {
    let me = this;
    if (files && files.item(0)) {
      const file = files.item(0);
      if (file.size <= environment.maxUploadZipFile) {
        this.fileToUpload = file;
        var reader = new FileReader();
        reader.onload = (event: any) => {
          me.localUrl = event.target.result;
        };
        reader.readAsDataURL(file);
      } else {
        this.fileToUpload = null;
        this.localUrl = null;
      }
    }
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Images Upload Form'),
        componentRef: this,
        data: [],
        componentName: 'ImagesUploaderModalComponent'
      },
    });
  }

  saveFile() {
    this.dialogRef.close({fileToUpload: this.fileToUpload, imageUrl: this.localUrl});
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

}
