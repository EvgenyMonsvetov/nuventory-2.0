import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { VendorDropdownComponent } from './vendor-dropdown.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ VendorDropdownComponent ],
    exports: [ VendorDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class VendorDropdownModule { }
