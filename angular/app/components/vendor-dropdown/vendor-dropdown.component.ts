import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-vendor-dropdown',
  templateUrl: './vendor-dropdown.component.html'
})
export class VendorDropdownComponent {
  private vendors: Array<any> = [];
  private disabledField:boolean = false;

  @Output() vendorChange = new EventEmitter();
  @Input() field: string = '';
  @Input()  materialDP: boolean = true;

  onChange(event) {
    this.vendorChange.emit(this.field);
  }

  @Input()
  get dataProvider() {
    return this.vendors;
  }

  set dataProvider(val) {
    this.vendors = val;
  }

  @Input()
  get disabled(){
    return this.disabledField;
  }

  set disabled(val) {
    this.disabledField = val;
  }

  constructor() { }

}
