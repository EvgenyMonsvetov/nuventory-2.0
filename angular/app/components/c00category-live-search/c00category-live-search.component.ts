import { Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { c00CategoryService } from '../../services/c00category.service';
import { Page } from '../../model/page';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { _lang } from '../../pipes/lang';
import { MatAutocompleteTrigger } from '@angular/material';

@Component({
  selector: 'app-c00category-live-search',
  templateUrl: './c00category-live-search.component.html',
  styleUrls: ['./c00category-live-search.component.scss'],
  providers: [ c00CategoryService ]
})
export class C00categoryLiveSearchComponent implements OnDestroy {
  public categoryValue: string = '';

  @ViewChild('c00category') c00c: ElementRef;
  @ViewChild('c00category', { read: MatAutocompleteTrigger })
  autoComplete: MatAutocompleteTrigger;

  @Input()
  get category() {
    return this.categoryValue;
  }

  @Output() categoryChange = new EventEmitter();
  @Output() inputChange = new EventEmitter();

  filteredArray: any[] = [];
  isLoading = false;
  private page = new Page();
  sub: Subscription;
  myControl = new FormControl();

  constructor(private toastr: ToastrService,
              private c00CategoryService: c00CategoryService) {}

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  set category(val) {
    this.categoryValue = val;
    this.c00c.nativeElement.value = val;
    this.inputChange.emit(this.categoryValue);
  }

  search(value) {
    this.category = value;
    this.page.filterParams['C00_NAME'] = value;
    if (value.length > 2) {
      this.getItems();
    } else {
      this.filteredArray = [];
    }
  }

  keyDown(keyCode) {
    if (keyCode === 13) {
      this.categoryChange.emit(this.categoryValue);
      this.autoComplete.closePanel();
    }
  }

  getItems() {
    this.isLoading = true;

    if (this.sub) {
      this.sub.unsubscribe();
    }

    this.sub = this.c00CategoryService.getItems(this.page).subscribe(res => {
          if (res && res.data) {
            this.filteredArray = res.data;
          } else {
            this.filteredArray = [];
          }

          this.isLoading = false;
        },
        error => {
          this.isLoading = false;
          this.toastr.error(_lang('Master Category') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

}
