import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { C00categoryLiveSearchComponent } from './c00category-live-search.component';

describe('C00categoryLiveSearchComponent', () => {
  let component: C00categoryLiveSearchComponent;
  let fixture: ComponentFixture<C00categoryLiveSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ C00categoryLiveSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(C00categoryLiveSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
