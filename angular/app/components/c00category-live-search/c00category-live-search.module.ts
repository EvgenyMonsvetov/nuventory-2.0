import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatAutocompleteModule, MatButtonModule, MatFormFieldModule, MatInputModule,
    MatProgressSpinnerModule
} from '@angular/material';
import { SharedModule } from '../../modules/shared.module';
import { C00categoryLiveSearchComponent } from './c00category-live-search.component';

@NgModule({
    imports: [
        CommonModule,
        MatInputModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        SharedModule
    ],
    declarations: [ C00categoryLiveSearchComponent ],
    exports: [ C00categoryLiveSearchComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class C00categoryLiveSearchModule { }
