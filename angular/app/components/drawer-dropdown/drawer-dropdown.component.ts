import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Constants } from '../../constants';

@Component({
  selector: 'app-drawer-dropdown',
  templateUrl: './drawer-dropdown.component.html'
})
export class DrawerDropdownComponent {
  private drawerDP: Array<any> = Constants.drawerDP;
  private disabledField: boolean = false;

  @Input()  drawerID: number;
  @Output() drawerIDChange = new EventEmitter();
  @Input()  materialDP: boolean = true;

  selectionChanged(value) {
    this.drawerIDChange.emit(value);
  }

  @Input()
  get disabled(){
    return this.disabledField;
  }

  set disabled(val) {
    this.disabledField = val;
  }

  constructor() { }

}
