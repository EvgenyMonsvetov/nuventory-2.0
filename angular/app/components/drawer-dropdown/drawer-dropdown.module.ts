import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { DrawerDropdownComponent } from './drawer-dropdown.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ DrawerDropdownComponent ],
    exports: [ DrawerDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class DrawerDropdownModule { }
