import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'app-vendor-search',
  templateUrl: './vendor-search.component.html'
})
export class VendorSearchComponent implements OnInit, OnDestroy {
  private _vendors: Array<object> = [];

  /** control for the selected filters */
  public vendorCtrl: FormControl = new FormControl();
  public vendorFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyVendor = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredVendors: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  @Input()  vendorId: number;
  @Input()  vendorName: string;
  @Input()  disabled: boolean = false;
  @Output() vendorIdChange = new EventEmitter();
  @Output() vendorNameChange = new EventEmitter();

  @Input()
  get vendors() {
    return this._vendors;
  }

  set vendors(val) {
    this._vendors = val;
    this.filterVendors();
  }

  constructor() { }

  ngOnInit() {
    this.vendorFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyVendor))
        .subscribe(() => {
          this.filterVendors();
        });
  }

  private filterVendors() {
    if (!this._vendors) {
      return;
    }
    // get the search keyword
    let search = this.vendorFilterCtrl.value;
    if (!search) {
      this.filteredVendors = this._vendors;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the vendors
    this.filteredVendors = this._vendors.filter(vendor => vendor['VD1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  ngOnDestroy() {
    this._onDestroyVendor.next();
    this._onDestroyVendor.complete();
  }

  selectionChanged(value) {
    this.vendorIdChange.emit(value);
    const _filteredVendors = this._vendors.filter(vendor => vendor['VD1_ID'] === value);
    if (_filteredVendors.length) {
      this.vendorNameChange.emit(_filteredVendors[0]['VD1_NAME']);

    }
  }

}
