import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { VendorSearchComponent } from './vendor-search.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ VendorSearchComponent ],
    exports: [ VendorSearchComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class VendorSearchModule { }
