import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { UploadEvent, FileSystemFileEntry } from 'ngx-file-drop';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LangPipe } from '../../views/pipes/lang.pipe';
import { ToastrService } from 'ngx-toastr';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-file-uploader-modal',
  templateUrl: './file-uploader-modal.component.html',
  styleUrls: ['./file-uploader-modal.component.scss'],
  preserveWhitespaces: true
})
export class FileUploaderModalComponent implements OnInit {
  private permissions = permissions;

  private fileToUpload: File = null;
  private localUrl: any[] = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';
  fileTypes: string = 'image/png, image/jpeg, image/gif';

  @ViewChild('uploadModal') uploadModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('actionbar')    actionbar:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;

  constructor(public dialogRef: MatDialogRef<FileUploaderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private lang: LangPipe,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit() {
    if (this.data['fileTypes']) {
      this.fileTypes = this.data['fileTypes'];
    }
    setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';});
  }

  getContentDialogHeight(): number{
    return (this.uploadModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight + 12);
  }

  getScrollHeight(): number{
    return this.getContentDialogHeight() - (this.actionbar.nativeElement.offsetHeight + 22 );
  }

  public dropped(event: UploadEvent) {
    if (event.files && event.files[0]) {
      const droppedFile = event.files[0];
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          if (file.size <= 8 * 1024 * 1024) {
            this.fileToUpload = file;
          } else {
            this.fileToUpload = null;
          }
        });
      }
    }
  }

  handleFileInput(files: FileList) {
    let me = this;
    if (files && files.item(0)) {
      const file = files.item(0);
      if (file.size <= 8 * 1024 * 1024) {
        this.fileToUpload = file;
        var reader = new FileReader();
        reader.onload = (event: any) => {
          me.localUrl = event.target.result;
        };
        reader.readAsDataURL(file);
      } else {
        this.fileToUpload = null;
        this.localUrl = null;
      }
    }
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('File Upload Form'),
        componentRef: this,
        data: [],
        componentName: 'FileUploaderModalComponent'
      },
    });
  }

  saveFile() {
    this.dialogRef.close({fileToUpload: this.fileToUpload, imageUrl: this.localUrl});
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }
}
