import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-export-modal',
  templateUrl: './export-modal.component.html',
  styleUrls: ['./export-modal.component.scss'],
  preserveWhitespaces: true
})
export class ExportModalComponent implements OnInit, AfterViewInit {
  cols: any[];
  items: any[];

  private dialogContentHeight: string = '0px';

  @ViewChild('exportModal')   exportModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('formContainer') formContainer:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('footer')        footer: ElementRef;

  form: FormGroup;
  columns: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<ExportModalComponent>,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.cols = this.data['cols'].filter(col => col['export']['visible']);
    this.form = this.formBuilder.group({
      checkboxes: this.buildColumns()
    });
  }

  ngAfterViewInit() {
    setTimeout(() => this.dialogContentHeight = this.getContentDialogHeight() + 'px', 0);
  }

  get checkboxes() {
    return this.form.get('checkboxes');
  };

  buildColumns() {
    const arr = this.cols.map(col => {
      return this.formBuilder.control(col['export']['checked']);
    });

    return this.formBuilder.array(arr);
  }

  getContentDialogHeight(): number{
    return (this.exportModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight + this.footer.nativeElement.offsetHeight + 2);
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  submit(form) {
    const formValue = Object.assign({}, {
      columns: form.checkboxes.map((selected, i) => {
        return {
          id: this.cols[i].field, selected: selected
        }
      }).filter(v => v.selected === true)
    });

    const cols: any[] = [];
    formValue.columns.forEach( col => {
        cols.push(col['id']);
    });
    this.closeDialog(cols.join(','));
  }

}
