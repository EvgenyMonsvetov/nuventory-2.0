import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { ReportingFilterComponent } from './reporting-filter.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ ReportingFilterComponent ],
    exports: [ ReportingFilterComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class ReportingFilterModule { }
