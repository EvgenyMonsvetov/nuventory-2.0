import { Component, Input, OnInit } from '@angular/core';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-reporting-filter',
  templateUrl: './reporting-filter.component.html',
  styleUrls: ['./reporting-filter.component.scss']
})
export class ReportingFilterComponent implements OnInit {
  @Input() range: number = 0;

  @Input() fromYear: number = 0;
  @Input() fromMonth: number = 0;
  @Input() toYear: number = 0;
  @Input() toMonth: number = 0;

  private years: any[] = [];
  private months: any[] = [
    {label: 'Jan', data: 0},
    {label: 'Feb', data: 1},
    {label: 'Mar', data: 2},
    {label: 'Apr', data: 3},
    {label: 'May', data: 4},
    {label: 'Jun', data: 5},
    {label: 'Jul', data: 6},
    {label: 'Aug', data: 7},
    {label: 'Sep', data: 8},
    {label: 'Oct', data: 9},
    {label: 'Nov', data: 10},
    {label: 'Dec', data: 11}];

  private selectedFromYear: number;
  private selectedFromMonth: number;
  private selectedToYear: number;
  private selectedToMonth: number;

  constructor(private appSidebarNavService: AppSidebarNavService) {
    this.years = this.buildYearsArray();
  }

  ngOnInit() {
    let today = new Date();
    today.setMonth(today.getMonth() - 1);

    if (this.fromYear) {
      this.selectedFromYear = this.fromYear;
      this.selectedFromMonth = this.fromMonth;
    } else {
      this.selectedFromYear = today.getFullYear();
      if (this.range > 0) {
        this.selectedFromYear -= this.range;
      }
      this.selectedFromMonth = today.getMonth();
    }

    if (this.toYear) {
      this.selectedToYear = this.toYear;
      this.selectedToMonth = this.toMonth;
    } else {
      this.selectedToYear = today.getFullYear();
      this.selectedToMonth = today.getMonth();
    }

    this.appSidebarNavService.setFromDate(this.createFromDateFilter());
    this.appSidebarNavService.setToDate(this.createToDateFilter());
    this.appSidebarNavService.next();
  }

  buildYearsArray() {
    let years: any[] = [];
    let year: number = 2008;
    let currYear: number = (new Date()).getFullYear();

    while (year <= currYear) {
      years.push({'label': year, 'data': year});
      year ++;
    }

    return years;
  }

  fromYearChanged(value) {
    this.selectedFromYear = value;
    this.appSidebarNavService.setFromDate(this.createFromDateFilter());
    this.appSidebarNavService.next();
  }

  fromMonthChanged(value) {
    this.selectedFromMonth = value;
    this.appSidebarNavService.setFromDate(this.createFromDateFilter());
    this.appSidebarNavService.next();
  }

  toYearChanged(value) {
    this.selectedToYear = value;
    this.appSidebarNavService.setToDate(this.createToDateFilter());
    this.appSidebarNavService.next();
  }

  toMonthChanged(value) {
    this.selectedToMonth = value;
    this.appSidebarNavService.setToDate(this.createToDateFilter());
    this.appSidebarNavService.next();
  }

  createFromDateFilter() {
    let fromYear = this.selectedFromYear;
    let fromMonth = this.selectedFromMonth;

    let fromDate = new Date();
    fromDate.setFullYear(fromYear, fromMonth, 1);

    let datePipe = new DatePipe('en-US');
    return datePipe.transform(fromDate, 'yyyy-MM-dd');
  }

  createToDateFilter() {
    let toYear = this.selectedToYear;
    let toMonth = this.selectedToMonth+1;

    let toDate = new Date();
    toDate.setFullYear(toYear, toMonth, 0);

    let datePipe = new DatePipe('en-US');
    return datePipe.transform(toDate, 'yyyy-MM-dd');
  }

}
