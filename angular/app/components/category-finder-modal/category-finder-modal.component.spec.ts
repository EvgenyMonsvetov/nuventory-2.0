import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryFinderModalComponent } from './category-finder-modal.component';

describe('CategoryFinderModalComponent', () => {
  let component: CategoryFinderModalComponent;
  let fixture: ComponentFixture<CategoryFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
