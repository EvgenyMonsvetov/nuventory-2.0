import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-average-chart-modal',
  templateUrl: './average-chart-modal.component.html',
  styleUrls: ['./average-chart-modal.component.scss'],
  preserveWhitespaces: true
})
export class AverageChartModalComponent implements OnInit {
  cols: any[] = [];
  items: any[] = [];

  private scrollHeight: string = '0px';
  private listHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('averageChartModal') averageChartModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('ordersList')    ordersList:  ElementRef;
  @ViewChild('footer')        footer:  ElementRef;

  constructor(public dialogRef: MatDialogRef<AverageChartModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.items = this.data['data'];
    setTimeout(() => { this.scrollHeight = this.getScrollHeight() + 'px';
      this.listHeight = this.getContentDialogHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px'; }, 300);
  }

  getContentDialogHeight(): number {
    return (this.averageChartModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight + 15);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - 2;
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
