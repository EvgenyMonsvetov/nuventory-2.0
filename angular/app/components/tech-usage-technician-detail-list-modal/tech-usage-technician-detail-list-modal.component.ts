import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { MasterDataService } from '../../services/master.data.service';
import { TreeNode } from 'primeng/api';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import {ExportModalComponent} from "../export-modal/export-modal.component";

@Component({
  selector: 'app-tech-usage-technician-detail-list-modal',
  templateUrl: './tech-usage-technician-detail-list-modal.component.html',
  styleUrls: ['./tech-usage-technician-detail-list-modal.component.scss'],
  providers: [
    MasterDataService
  ],
  preserveWhitespaces: true
})
export class TechUsageTechnicianDetailListModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  private title: string = '';

  @ViewChild('techUsageDetailModal') techUsageDetailModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<TechUsageTechnicianDetailListModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private masterDataService: MasterDataService,
              public  dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.title = _lang('Tech Usage Report Details for Technician') + ' - ' + this.data.params['TE1_NAME'];
    this.page.filterParams['startDate'] = this.data.startDate;
    this.page.filterParams['endDate'] = this.data.endDate;
    this.page.filterParams['TE1_ID'] = this.data.params['TE1_ID'];
    this.page.filterParams['TECH_USAGE_BY_MONTHS'] = true;
    this.page.filterParams['MTD'] = this.data.isMTD;

    this.cols = [
      { field: 'CA1_NAME',        header: _lang('CA1_NAME'),        visible: true, export: {visible: true, checked: true} },
      { field: 'TE1_NAME',        header: _lang('TE1_NAME'),        visible: false, export: {visible: true, checked: true} },
      { field: 'MD1_PART_NUMBER', header: _lang('MD1_PART_NUMBER'), visible: true, export: {visible: true, checked: true}, align: 'right' },
      { field: 'MD1_DESC1',       header: _lang('MD1_DESC1'),       visible: true, export: {visible: true, checked: true} },
      { field: 'TOTAL_PRICE',     header: _lang('Total Price'),     visible: true, export: {visible: true, checked: true}, align: 'right' },
      { field: 'TOTAL_QTY',       header: _lang('Total QTY'),       visible: true, export: {visible: true, checked: true},  align: 'right' },
      { field: 'CREATED_BY_NAME', header: _lang('#CreatedBy#'),     visible: true, export: {visible: true, checked: true} },
      { field: 'OR_CREATED_ON',   header: _lang('#CreatedOn#'),     visible: true, export: {visible: true, checked: true} }
    ];

    this.setPage({offset: 0});
  }

  setPage(pageInfo) {
    this.loading = true;
    this.page.pageNumber = pageInfo.offset;

    this.masterDataService.getTechUsageByMonths(this.page).subscribe(result => {
          this.items = <TreeNode[]>result.data;
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Technicians') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  getContentDialogHeight(): number {
    return (this.techUsageDetailModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight) - 10;
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
  }

  closeDialog() {
    this.dialogRef.close();
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Tech Usage Report Details'),
        componentRef: this,
        data: [],
        componentName: 'TechUsageTechnicianDetailListModalComponent'
      },
    });
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        this.page.filterParams['columns'] = columns;
        this.page.filterParams['TE1_NAME'] = this.data.params['TE1_NAME'];

        this.masterDataService.downloadXlsx(this.page, 'tech-usage-technician-details').subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

}
