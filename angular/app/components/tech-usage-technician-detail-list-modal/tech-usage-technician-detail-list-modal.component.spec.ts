import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechUsageTechnicianDetailListModalComponent } from './tech-usage-technician-detail-list-modal.component';

describe('TechUsageTechnicianDetailListModalComponent', () => {
  let component: TechUsageTechnicianDetailListModalComponent;
  let fixture: ComponentFixture<TechUsageTechnicianDetailListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechUsageTechnicianDetailListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechUsageTechnicianDetailListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
