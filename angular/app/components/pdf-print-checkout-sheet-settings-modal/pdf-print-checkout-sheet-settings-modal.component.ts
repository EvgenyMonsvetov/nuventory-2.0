import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { permissions } from '../../permissions';

@Component({
  selector: 'app-pdf-print-checkout-sheet-settings-modal',
  templateUrl: './pdf-print-checkout-sheet-settings-modal.component.html',
  preserveWhitespaces: true
})
export class PdfPrintCheckoutSheetSettingsModalComponent {
  private permissions = permissions;
  private selected = 'Body';

  private columnDP: any[] = [
    {label: 'Body'},
    {label: 'Detail'},
    {label: 'Paint'},
    {label: 'Favorites'},
    {label: 'All'}
  ];

  constructor(public dialogRef: MatDialogRef<PdfPrintCheckoutSheetSettingsModalComponent>,
              public  dialog: MatDialog) { }

  openLanguageEditor() {
    this.closeDialog();
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('User Setting Form'),
        componentRef: this,
        data: [],
        componentName: 'PdfPrintCheckoutSheetSettingsModalComponent'
      },
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onPrint() {
    this.dialogRef.close({selected: this.selected});
  }

}
