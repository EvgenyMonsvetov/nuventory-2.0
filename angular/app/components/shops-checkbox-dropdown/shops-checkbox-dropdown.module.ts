import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { ShopsCheckboxDropdownComponent } from './shops-checkbox-dropdown.component';
import { ShopService } from '../../services/shop.service';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ ShopsCheckboxDropdownComponent ],
    exports: [ ShopsCheckboxDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
    providers: [
        ShopService
    ]

})
export class ShopsCheckboxDropdownModule { }
