import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { ShopService } from '../../services/shop.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';

@Component({
  selector: 'app-shops-checkbox-dropdown',
  templateUrl: './shops-checkbox-dropdown.component.html',
  styleUrls: ['./shops-checkbox-dropdown.component.scss']
})
export class ShopsCheckboxDropdownComponent implements OnInit, OnDestroy {
  private shops: Array<any> = [];

  private  _selectedCompanies: Array<any> = [];

  @Input()  selectedShops: Array<any> = [];
  @Output() selectedShopsChange = new EventEmitter();

  @Input()
  get selectedCompanies() {
    return this._selectedCompanies;
  }

  set selectedCompanies(val) {
    this._selectedCompanies = val;
    this.getShops();
  }

  selectionChanged(value) {
    this.selectedShopsChange.emit(value);
  }

  private subscription: Subscription;

  constructor(private shopService: ShopService,
              private toastr: ToastrService) { }

  ngOnInit() {

    this.selectedShops = this.selectedShops.map( (LO1_ID) => {
        return parseInt(LO1_ID);
    });

    this.getShops();
  }

  getShops() {
    if (this.subscription)
      this.subscription.unsubscribe();

    let page = new Page();
    page.filterParams['CO1_IDs'] = this.selectedCompanies;

    this.subscription = this.shopService.getByCountries(page).subscribe(res => {
      this.shops = res.data.map( (shop)=> {
          shop['LO1_ID'] = parseInt(shop['LO1_ID']);
          return shop;
      });
    },
    error => {
      this.toastr.error(_lang('Shops') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  selectAllClickHandler() {
    let selectedAll = [];
    this.shops.forEach(function(item) {
      selectedAll.push(item['LO1_ID']);
    });
    this.selectedShops = selectedAll;
    this.selectedShopsChange.emit(selectedAll);
  }

  deselectAllClickHandler() {
    this.selectedShops = [];
    this.selectedShopsChange.emit([]);
  }
}
