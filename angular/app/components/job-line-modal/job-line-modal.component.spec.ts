import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobLineModalComponent } from './job-line-modal.component';

describe('JobLineModalComponent', () => {
  let component: JobLineModalComponent;
  let fixture: ComponentFixture<JobLineModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobLineModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobLineModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
