import { AfterViewInit, Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { TechnicianFinderModalComponent } from '../technician-finder-modal/technician-finder-modal.component';
import { JobLineService } from '../../services/job-line.service';

@Component({
  selector: 'app-job-line-modal',
  templateUrl: './job-line-modal.component.html',
  styleUrls: ['./job-line-modal.component.scss'],
  providers: [
    JobLineService,
  ],
  preserveWhitespaces: true
})
export class JobLineModalComponent implements OnInit, AfterViewInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[];
  items: any[];

  private loading = true;
  private record: any = null;
  private errors: Array<any> = [];
  private dialogContentHeight: string = '0px';

  @ViewChild('jobLineModal')  jobLineModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('actionbar')     actionbar: ElementRef;

  constructor(public dialogRef: MatDialogRef<JobLineModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public  dialog: MatDialog,
              private jobLineService: JobLineService,
              private toastr: ToastrService) { }

  ngOnInit() {
      this.record = this.jobLineService.initialize();
      this.getData();
  }

  getData() {
      this.jobLineService.getById(this.data.JT2_ID).subscribe( result => {
          if (result[0]) {
              let data = result[0];
              this.record = data;
          }
      },
      error => {
          this.loading = false;
          this.toastr.error(_lang('Job Line') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
          });
      });
  }

  ngAfterViewInit() {
      setTimeout(() => this.dialogContentHeight = this.getContentDialogHeight() + 'px', 0);
  }

  getContentDialogHeight(): number {
    return (this.jobLineModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight + this.actionbar.nativeElement.offsetHeight + 25);
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  openLanguageEditor() {
      this.dialog.open(LanguageEditorModalComponent, {
          width: window.innerWidth-60+'px',
          height: window.innerHeight-40+'px',
          role: 'dialog',
          data: {
              title: _lang('Job Ticket Line Form'),
              componentRef: this,
              data: [],
              componentName: 'JobLineModalComponent'
          },
      });
  }

  selectTechnician() {
    let dialogRef = this.dialog.open(TechnicianFinderModalComponent, {
        width: window.innerWidth-100+'px',
        height: window.innerHeight-80+'px',
        role: 'dialog',
        data: {title: _lang('Select Technician'), TE1_ID: this.record.TE1_ID},
    });

    dialogRef.beforeClose().subscribe( selected => {
        if (selected) {
            this.record.TE1_ID = selected['TE1_ID'];
            this.record.TE1_SHORT_CODE = selected['TE1_SHORT_CODE'];
            this.record.TE1_NAME = selected['TE1_NAME'];
        }
    })
  }

  clearTechnician() {
    this.record.TE1_ID = 0;
    this.record.TE1_SHORT_CODE = '';
    this.record.TE1_NAME = '';
  }

  saveClick() {
    this.jobLineService.update(this.record).subscribe(result => {
        if (result.success) {
            this.closeDialog(true);
        } else {
            this.errors = result.errors;
        }
    });
  }
}
