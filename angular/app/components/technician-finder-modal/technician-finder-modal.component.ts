import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { TechnicianService } from '../../services/technician.service';

@Component({
  selector: 'app-technician-finder-modal',
  templateUrl: './technician-finder-modal.component.html',
  styleUrls: ['./technician-finder-modal.component.scss'],
  providers: [
    TechnicianService
  ],
  preserveWhitespaces: true
})
export class TechnicianFinderModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('tecnicianModal') tecnicianModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<TechnicianFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private technicianService: TechnicianService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.cols = [
      { field: 'TE1_SHORT_CODE',  header: _lang('TE1_SHORT_CODE'),  visible: true },
      { field: 'TE1_NAME',        header: _lang('TE1_NAME'),        visible: true },
      { field: 'CO1_NAME',        header: _lang('CO1_NAME'),        visible: true },
      { field: 'LO1_NAME',        header: _lang('LO1_NAME'),        visible: true },
      { field: 'US1_NAME',        header: _lang('US1_NAME'),        visible: true }
    ];

    this.setPage({offset: 0});
  }

  setPage(pageInfo) {
    this.loading = true;
    this.page.pageNumber = pageInfo.offset;
    this.page.filterParams['TE1_ACTIVE'] = '1';

    this.technicianService.getTechnicians(this.page).subscribe(result => {
          this.page.totalElements = result.data['count'];
          this.page.totalPages    = result.data['count'] / this.page.size;
          this.items = result.data['currentVocabulary'];
          if (this.data.TE1_ID) {
            this.selected = result.data['currentVocabulary'].filter( item => {
              return item['TE1_ID'] === this.data.TE1_ID;
            })[0];
          }
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Technicians') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  getContentDialogHeight(): number {
    return (this.tecnicianModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
  }

  closeDialog() {
    this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
    this.selected = record;
    this.closeDialog();
  }

}
