import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicianFinderModalComponent } from './technician-finder-modal.component';

describe('TechnicianFinderModalComponent', () => {
  let component: TechnicianFinderModalComponent;
  let fixture: ComponentFixture<TechnicianFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicianFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicianFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
