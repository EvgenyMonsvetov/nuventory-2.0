import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { RackDropdownComponent } from './rack-dropdown.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ RackDropdownComponent ],
    exports: [ RackDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class RackDropdownModule { }
