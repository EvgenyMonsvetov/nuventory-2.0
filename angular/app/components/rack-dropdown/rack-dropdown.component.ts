import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-rack-dropdown',
  templateUrl: './rack-dropdown.component.html'
})
export class RackDropdownComponent {
  private lo1_racks: string = '';
  private disabledField: boolean = false;
  private rackDP: Array<any> = [];

  @Input()  rackID: number;
  @Output() rackIDChange = new EventEmitter();
  @Input()  materialDP: boolean = true;

  selectionChanged(value) {
    this.rackIDChange.emit(value);
  }

  @Input()
  get LO1_RACKS() {
    return this.lo1_racks;
  }

  set LO1_RACKS(val) {
    this.lo1_racks = val;
    this.setRacks(val);
  }

  @Input()
  get disabled(){
    return this.disabledField;
  }

  set disabled(val) {
    this.disabledField = val;
  }

  constructor() { }

  setRacks(value) {
    this.rackDP = [{label: 'N/A', data: '0'}];
    for (let i = 1; i <= value; i++) {
      this.rackDP.push({label: i.toString(), data: i.toString()});
    }
  }
}
