import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateStatementModalComponent } from './generate-statement-modal.component';

describe('GenerateStatementModalComponent', () => {
  let component: GenerateStatementModalComponent;
  let fixture: ComponentFixture<GenerateStatementModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateStatementModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateStatementModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
