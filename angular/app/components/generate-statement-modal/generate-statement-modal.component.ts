import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { HeaderService } from '../../services/header.service';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { Page } from '../../model/page';
import { ConfirmationService } from 'primeng/api';
import { StatementService } from '../../services/statement.service';
import { ToastrService } from 'ngx-toastr';
import { permissions } from '../../permissions';
import { Subscription } from 'rxjs/Rx';
import * as moment from 'moment-timezone';
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-generate-statement-modal',
  templateUrl: './generate-statement-modal.component.html',
  styleUrls: ['./generate-statement-modal.component.scss'],
  providers: [
    StatementService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class GenerateStatementModalComponent implements OnInit {
  private permissions = permissions;

  private fromYear: number;
  private fromMonth: number;
  private toYear: number;
  private toMonth: number;
  private disableGenerateButton: boolean = false;

  private dialogContentHeight = '80px';

  private confirmationHeader: string = _lang('#GenerateInvoice#');
  private acceptLabel: string = _lang('Yes');

  private page = new Page();
  private subscriptionSidebarNav: Subscription;

  constructor(public dialogRef: MatDialogRef<GenerateStatementModalComponent>,
              public  dialog: MatDialog,
              private headerService: HeaderService,
              private appSidebarNavService: AppSidebarNavService,
              private confirmationService: ConfirmationService,
              private statementService: StatementService,
              private toastr: ToastrService) {
    this.page.filterParams = {};

    var browserTime = moment(new Date());
    browserTime.subtract(1, 'months').format();

    this.fromYear = browserTime.year();
    this.fromMonth = browserTime.month();

    this.toYear = browserTime.year();
    this.toMonth = browserTime.month();

    this.page.filterParams['from'] = this.createDate(this.fromYear, this.fromMonth);
    this.page.filterParams['to']   = this.createDate(this.toYear, this.toMonth);

    this.subscriptionSidebarNav = this.appSidebarNavService.subscribe({
      next: (v) => {
        this.page.filterParams['from'] = this.appSidebarNavService.fromDate;
        this.page.filterParams['to'] = this.appSidebarNavService.toDate;
        this.checkFilterDate();
      }
    });
  }

  checkFilterDate() {
    const fromDate = new Date(this.page.filterParams['from']);
    const toDate = new Date(this.page.filterParams['to']);
    if (fromDate.getTime() > toDate.getTime() || toDate.getTime() < fromDate.getTime()) {
      this.disableGenerateButton = true;
      this.toastr.error(_lang('Incorrect Date Range'), _lang('Date Error'), {
        timeOut: 3000,
      });
    } else {
      this.disableGenerateButton = false;
    }
  }

  createDate( year, month ){
    let date = new Date();
    date.setFullYear(year, month, 1);

    let datePipe = new DatePipe('en-US');
    return datePipe.transform(date, 'yyyy-MM-dd');
  }

  ngOnInit() {
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Generate Statement'),
        componentRef: this,
        data: [],
        componentName: 'GenerateStatementModalComponent'
      },
    });
  }

  generateStatementClick() {
    this.acceptLabel = _lang('Yes');
    this.confirmationHeader = _lang('#GenerateInvoice#');
    this.confirmationService.confirm({
      message: _lang('CO1_NAME') + ': ' + this.headerService.CO1_NAME + '<br/>' +
               _lang('LO1_NAME') + ': ' + (this.headerService.LO1_ID > 0 ? this.headerService.LO1_NAME : _lang('#SelectShop#')) + '<br/>' +
               _lang('From') + ': ' + this.page.filterParams['from'] + ' ' + _lang('To') + ': ' + this.page.filterParams['to'] + '<br/>' + _lang('#Continue?#'),
      rejectVisible: true,
      accept: () => {
        this.closeDialog(true);
      }
    });
  }
}
