import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultImageEditorModalComponent } from './default-image-editor-modal.component';

describe('DefaultImageEditorModalComponent', () => {
  let component: DefaultImageEditorModalComponent;
  let fixture: ComponentFixture<DefaultImageEditorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultImageEditorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultImageEditorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
