import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {_lang} from '../../pipes/lang';
import {ToastrService} from 'ngx-toastr';
import {MasterCatalogService} from '../../services/master-catalog.service';

@Component({
    selector: 'app-default-image-editor-modal',
    templateUrl: './default-image-editor-modal.component.html',
    styleUrls: ['./default-image-editor-modal.component.scss'],
    preserveWhitespaces: true
})
export class DefaultImageEditorModalComponent implements OnInit {
    selected: any[] = [];
    items: any[] = [];
    success: boolean = false;

    @ViewChild('header') header: ElementRef;
    @ViewChild('content') content: ElementRef;
    @ViewChild('actionbar') actionbar: ElementRef;
    @ViewChild('tableHeader') tableHeader: ElementRef;

    constructor(public dialogRef: MatDialogRef<DefaultImageEditorModalComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private toastr: ToastrService,
                public dialog: MatDialog,
                private masterCatalogService: MasterCatalogService
    ) {
    }

    ngOnInit() {
        this.items = this.data.items;

        this.items.map(item => {
            this.selected.push(item.MD1_ID);
        });
    }

    closeDialog() {
        this.dialogRef.close(this.success);
    }

    changeSelected(event, MD1_ID) {
        if (event.target.checked) {
            this.selected.push(MD1_ID);
        } else {
            let s = [];
            this.selected.map(val => {
                if (val != MD1_ID) {
                    s.push(val);
                }
            });
            this.selected = s;
        }
    }

    setDefaultImages() {
        this.masterCatalogService.setDefaultImages(this.data.MD0_ID, this.selected).subscribe(result => {
                this.success = result.success;
                if (this.success) {
                    this.toastr.success(_lang('Replaced successful'), _lang('Create a default Vendor Image For Customer Part Image'), {
                        timeOut: 3000,
                    });

                    this.dialogRef.close(this.success);
                } else {
                    this.toastr.error(_lang('Failed replace images'), _lang('Service Error'), {
                        timeOut: 3000,
                    });
                }
            }
        );
    }
}
