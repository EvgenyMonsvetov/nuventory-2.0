import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { СomboboxSearchComponent } from './combobox-search.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ СomboboxSearchComponent ],
    exports: [ СomboboxSearchComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]
})
export class СomboboxSearchModule { }
