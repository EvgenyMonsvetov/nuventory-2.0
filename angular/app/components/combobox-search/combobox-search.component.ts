import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'app-combobox-search',
  templateUrl: './combobox-search.component.html'
})
export class СomboboxSearchComponent implements OnInit, OnDestroy {
  private _items: Array<object> = [];

  /** control for the selected filters */
  public searchCtrl: FormControl = new FormControl();
  public searchFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyItem = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredItems: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  @Input()  FIELD_ID: string;
  @Input()  FIELD_NAME: string;
  @Input()  itemId: number;
  @Input()  itemName: number;
  @Input()  disabled: boolean = false;
  @Output() itemIdChange = new EventEmitter();
  @Output() itemNameChange = new EventEmitter();

  @Input()
  get data() {
    return this._items;
  }

  set data(val) {
    this._items = val;
    this.filterItems();
  }

  constructor() { }

  ngOnInit() {
    this.searchFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyItem))
        .subscribe(() => {
          this.filterItems();
        });
  }

  private filterItems() {
    if (!this._items) {
      return;
    }
    // get the search keyword
    let search = this.searchFilterCtrl.value;
    if (!search) {
      this.filteredItems = this._items;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the items
    this.filteredItems = this._items.filter(item => item[this.FIELD_NAME].toLowerCase().indexOf(search) > -1);
  }

  ngOnDestroy() {
    this._onDestroyItem.next();
    this._onDestroyItem.complete();
  }

  selectionChanged(value) {
    this.itemIdChange.emit(value);
    const _filteredItem = this._items.filter(item => item[this.FIELD_ID] === value);
    if (_filteredItem.length) {
      this.itemNameChange.emit(_filteredItem[0][this.FIELD_NAME]);
    }
  }

}
