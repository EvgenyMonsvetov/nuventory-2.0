import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { permissions } from '../../permissions';
import { CompanyFinderModalComponent } from '../company-finder-modal/company-finder-modal.component';
import { ShopFinderModalComponent } from '../shop-finder-modal/shop-finder-modal.component';
import { FormControl } from '@angular/forms';
import { HeaderService } from '../../services/header.service';
import { Subscription } from 'rxjs/Rx';
import { VendorService } from '../../services/vendor.service';
import { ConfirmationService} from 'primeng/api';
import { MasterDataModalComponent } from '../master-data-modal/master-data-modal.component';
import { MasterDataService } from '../../services/master.data.service';
import { MasterDataLocationService } from '../../services/master-data-location.service';
import { AuthService } from '../../auth/auth.service';
import { GroupsService } from '../../services/groups';
import { CategoryService} from '../../services/category.service';
import { c00CategoryService} from '../../services/c00category.service';
import {Constants} from "../../constants";

@Component({
  selector: 'app-edit-inventory-modal',
  templateUrl: './edit-inventory-modal.component.html',
  styleUrls: ['./edit-inventory-modal.component.scss'],
  providers: [
    MasterDataService,
    CategoryService,
    c00CategoryService,
    VendorService,
    ConfirmationService,
    MasterDataLocationService
  ],
  preserveWhitespaces: true
})
export class EditInventoryModalComponent implements OnInit {
  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];
  vendorData: any[] = [];
  c00categoriesData: any[] = [];
  categoriesData: any[] = [];
  selected: any;
  search: string;
  currentSearchIndex = -1;
  private subscription: Subscription;
  private constants = Constants;

  CO1_ID = 0;
  CO1_NAME = '';
  LO1_ID = 0;
  LO1_NAME = '';

  isAdmin: boolean = true;
  isLite: boolean = false;
  scannerMode: boolean = false;

  private loading = false;
  private page = new Page();
  private size: number = 1000;
  private currentPageIndex: number = 0;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';
  private standardUser: boolean = false;

  parts: any[] = [];
  filteredParts: any[] = [];
  partNumber: string = '';
  partsControl = new FormControl();

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  @ViewChild('editInventoryModal')    editInventoryModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('searchbar')     searchbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('footer')        footer:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('searchInput')   searchInput:  ElementRef;
  @ViewChild('errorsContainer') errorsContainer:  ElementRef;
  @ViewChild('inputPartNumber')   inputPartNumber: ElementRef;

  masterRecord = false;
  private errors: Array<any> = [];

  constructor(public dialogRef: MatDialogRef<EditInventoryModalComponent>,
              private masterDataService: MasterDataService,
              private categoryService: CategoryService,
              private c00CategoryService: c00CategoryService,
              private masterDataLocationService: MasterDataLocationService,
              private vendorService: VendorService,
              private confirmationService: ConfirmationService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public  dialog: MatDialog,
              private elementRef: ElementRef,
              private toastr: ToastrService,
              private headerService: HeaderService,
              private auth: AuthService ) {
    this.page.size = this.size;
    this.page.pageNumber = 0;

    if (auth.accessLevel === GroupsService.GROUP_STANDARD_USER) {
      this.standardUser = true;
    }
  }

  ngOnInit() {
    this.loading = true;
    this.cols = [
      // { field: 'MD1_ID',            header: _lang('MD1_ID'),            visible: false, width: 80 },
      // { field: 'VD1_SHORT_CODE',    header: _lang('VD1_SHORT_CODE'),    visible: false, width: 120 },
      { field: 'VD1_ID',            header: _lang('VD1_NAME'),          visible: true,  width: 120 },
      { field: 'C00_NAME',          header: _lang('Nuventory Category'),visible: this.auth.accessLevel === GroupsService.GROUP_ADMIN,  width: 120 },
      { field: 'CA1_NAME',          header: _lang('Company Category'),  visible: true,  width: 120 },
      { field: 'MD2_ACTIVE',        header: _lang('MD2_ACTIVE'),        visible: true,  width: 40 },
      // { field: 'MD2_PARTIAL',       header: _lang('MD2_PARTIAL'),       visible: false, width: 78 },
      // { field: 'CO1_ID',            header: _lang('CO1_ID'),            visible: false, width: 80 },
      // { field: 'CO1_SHORT_CODE',    header: _lang('CO1_SHORT_CODE'),    visible: false, width: 80 },
      // { field: 'CO1_NAME',          header: _lang('CO1_NAME'),          visible: false, width: 120 },
      // { field: 'LO1_ID',            header: _lang('LO1_ID'),            visible: false, width: 80 },
      // { field: 'LO1_SHORT_CODE',    header: _lang('LO1_SHORT_CODE'),    visible: false, width: 80 },
      // { field: 'LO1_NAME',          header: _lang('LO1_NAME'),          visible: false, width: 120 },
      // { field: 'FI1_ID',            header: _lang('FI1_ID'),            visible: false, width: 100 },
      { field: 'MD1_PART_NUMBER',   header: _lang('MD1_PART_NUMBER'),   visible: true,  width: 100 },
      { field: 'MD1_UPC1',          header: _lang('MD1_UPC1'),          visible: true,  width: 100 },
      // { field: 'MD1_UPC2',          header: _lang('MD1_UPC2'),          visible: false, width: 100 },
      // { field: 'MD1_UPC3',          header: _lang('MD1_UPC3'),          visible: false, width: 100 },
      { field: 'MD1_DESC1',         header: _lang('MD1_DESC1'),         visible: true,  width: 160 },
      // { field: 'MD1_DESC2',         header: _lang('MD1_DESC2'),         visible: false, width: 120 },
      { field: 'UM1_RECEIPT_NAME',  header: _lang('UM1_RECEIPT_NAME'),  visible: true,  width: 80 },
      // { field: 'MD1_ORDER_QTY',     header: _lang('MD1_ORDER_QTY'),     visible: false, width: 50 },
      { field: 'MD2_ON_HAND_QTY',   header: _lang('MD2_ON_HAND_QTY'),   visible: !this.isLite,  width: 50 },
      // { field: 'MD2_AVAILABLE_QTY', header: _lang('MD2_AVAILABLE_QTY'), visible: false, width: 50 },
      { field: 'MD2_MIN_QTY',       header: _lang('MD2_MIN_QTY'),       visible: !this.isLite,  width: 50 },
      // { field: 'MD2_MAX_QTY',       header: _lang('MD2_MAX_QTY'),       visible: false, width: 50 },
      { field: 'MD2_RACK',          header: _lang('MD2_RACK'),          visible: true,  width: 50 },
      { field: 'MD2_DRAWER',        header: _lang('MD2_DRAWER'),        visible: true,  width: 50 },
      { field: 'MD2_BIN',           header: _lang('MD2_BIN'),           visible: true,  width: 50 },
      { field: 'MD1_UNIT_PRICE',     header: _lang('MD1_UNIT_PRICE'),    visible: true,  width: 60 },
      { field: 'MD1_UM1_PRICE_NAME', header: _lang('UM1_PRICE_NAME'),    visible: true,  width: 80 }
      // { field: 'MD1_EXTENDED_VALUE',header: _lang('MD1_EXTENDED_VALUE'),visible: false, width: 80 }
    ];

    this.CO1_ID = this.headerService.CO1_ID;
    this.CO1_NAME = this.headerService.CO1_NAME;
    this.LO1_ID = this.headerService.LO1_ID;
    this.LO1_NAME = this.headerService.LO1_NAME;

    this.masterDataService.getQuickAddParts(this.page).subscribe(res => {
      this.parts = res['parts'];
    },
    error => {
      this.toastr.error(_lang('Customer Data parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });

    var me = this;
    (new Promise((resolve, reject) => {
      var page = new Page();
      page.filterParams['CO1_ID'] = me.headerService.CO1_ID;
      me.vendorService.getVendors(page).subscribe((data: any) => {
        me.vendorData = data.data.items.map( item => {
          return item;
        });
        resolve();
      });
    })).then(function (resolve) {
      return new Promise((resolve, reject) => {
          me.categoryService.getItems(me.page).subscribe(res => {
                me.categoriesData = res.data.map( item => {
                  return item;
                });
                resolve();
              },
              error => {
                me.toastr.error(
                    _lang('Categories') + ' ' + _lang('loading error occured'),
                    _lang('Service Error'), {
                      timeOut: 3000,
                    });
                resolve();
              });
      });
    }).then(function (resolve) {
      return new Promise((resolve, reject) => {
        me.c00CategoryService.getItems(me.page).subscribe(res => {
              me.c00categoriesData = res.data.map( item => {
                return item;
              });
              resolve();
            },
            error => {
              me.toastr.error(
                  _lang('Nuventory Categories') + ' ' + _lang('loading error occured'),
                  _lang('Service Error'), {
                    timeOut: 3000,
                  });
              resolve();
            });
      });
    }).then(function (resolve) {
        return new Promise((resolve, reject) => {
          if (me.data.records) {
          var partIds = me.data.records.map( item => {
            return item['MD1_ID'];
          });
          var partMd2Ids = me.data.records.map( item => {
            return item['MD2_ID'];
          });

          if (partIds.length > 0) {
            me.page.filterParams['CO1_ID'] = me.headerService.CO1_ID;
            me.page.filterParams['LO1_ID'] = me.headerService.LO1_ID;
            me.page.filterParams['MD1_IDs'] = partIds.join(',');
            me.page.filterParams['MD2_IDs'] = partMd2Ids.join(',');
            me.page.filterParams['editEnventory'] = 1;
            me.masterDataService.getAll(me.page).subscribe(res => {
                setTimeout(() => {
                  me.addPartsToOrder(res['data']);
                  me.scrollHeight = me.getScrollHeight() + 'px';
                  me.dialogContentHeight = me.getContentDialogHeight() + 'px';
                  resolve();
                });
            },
            error => {
                me.toastr.error(
                _lang('Parts') + ' ' + _lang('loading error occured'),
                _lang('Service Error'), {
                    timeOut: 3000,
                });
                resolve();
            });
          } else {
            setTimeout(() => {
              me.scrollHeight = me.getScrollHeight() + 'px';
              me.dialogContentHeight = me.getContentDialogHeight() + 'px';
              resolve();
            });
          }
        }
      });
    }).then( function (resolve) {
      me.inputPartNumber.nativeElement.focus();
      me.loading = false;
    })
  }

  getContentDialogHeight(): number{
    return (this.editInventoryModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight + 41 + (this.errorsContainer && this.errorsContainer.nativeElement ? this.errorsContainer.nativeElement.offsetHeight : 0) );
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
        this.searchbar.nativeElement.offsetHeight + 12);
  }

  filterParts(value: string) {
    const filterValue = value.toLowerCase();
    this.filteredParts = this.parts.filter(part => {
      return part['MD1_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1 ||
             (part['MD1_DESC1'] && part['MD1_DESC1'].toLowerCase().indexOf(filterValue) > -1) ||
             (part['MD1_UPC1'] && part['MD1_UPC1'].toLowerCase().indexOf(filterValue) > -1) ||
             (part['MD1_VENDOR_PART_NUMBER'] && part['MD1_VENDOR_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1)
    });
  }

  partNumberKeyUp(event) {
    if (event.keyCode === 13) {
      this.partNumber = this.partNumber.trim().toLowerCase();
      if (this.partNumber.toLowerCase().indexOf('qty') === 0) {
        this.addLastPartQty(this.partNumber.toLowerCase().replace('qty', ''));
        this.filteredParts = [];
        this.partNumber = '';
      } else {
        this.masterDataService.clearSaveBarcode( this, 'partNumber' );
        this.filterParts(this.partNumber);
        if (this.partNumber && this.partNumber !== '') {
          if (this.filteredParts && this.filteredParts.length > 0) {
            this.onSelectPart(this.filteredParts[0]['MD1_PART_NUMBER']);
            this.filteredParts = [];
          } else {
            this.toastr.error(_lang('No one part number does not match'), _lang('Error'), {
              timeOut: 3000,
            });
          }
        }
      }
    }
  }

  onSelectPart(value) {
    this.page.filterParams['LO1_TO_ID'] = this.LO1_ID;
    this.page.filterParams['MD1_PART_NUMBER'] = value;
    delete this.page.filterParams['MD1_IDs'];
    this.masterDataService.getFullParts(this.page).subscribe(res => {
      this.addPartsToOrder(res['data'], false);
    },
    error => {
      this.toastr.error(_lang('Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });

    setTimeout(() => {
      this.filteredParts = [];
      this.partNumber = '';
    }, 300);
  }

  onInput(value) {
    this.search = value;
    this.currentSearchIndex = -1;
    this.onNext();
  }

  onNext() {
    let index = 0;
    if (this.currentSearchIndex >= 0)
      index = this.currentSearchIndex  + 1;

    for (index; index < this.items.length; index ++) {
      if (this.items[index]) {
        if ((this.items[index].MD1_PART_NUMBER && this.items[index].MD1_PART_NUMBER.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.items[index].MD1_DESC1 && this.items[index].MD1_DESC1.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.items[index].MD1_DESC2 && this.items[index].MD1_DESC2.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.items[index].MD1_UPC1 && this.items[index].MD1_UPC1.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.items[index].MD1_UPC2 && this.items[index].MD1_UPC2.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.items[index].MD1_UPC3 && this.items[index].MD1_UPC3.toString().toLowerCase().search(this.search.toLowerCase()) !== -1)) {
          this.currentSearchIndex = index;
          this.setCurrentPage(index);
          const row = 'row' + this.currentSearchIndex;

          setTimeout(() => {this.selected = this.items[index];
            const elem = document.getElementById(row);
            if (elem) {
              elem.scrollIntoView();
            }
          });
          break;
        }
      }
    }
  }

  activateSelected() {
    if (this.selected) {
      this.selected.forEach(function (item) {
        item['MD2_ACTIVE'] = '1';
      });
    }

    this.selected = [];
  }

  deactivateSelected() {
    if (this.selected) {
      this.selected.forEach(function (item) {
        item['MD2_ACTIVE'] = '0';
      });
    }
    this.selected = [];
  }

  onPage(event) {
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.currentSearchIndex = 0;
    this.currentPageIndex = event.first;
  }

  setCurrentPage(index: number) {
    const n = index === 0 ? 0 : Math.ceil(index / this.page.size) - 1;
    this.currentPageIndex = this.page.size * n;
  }

  openLanguageEditor() {
    this.closeDialog();
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Inventory Form'),
        componentRef: this,
        data: [],
        componentName: 'EditInventoryModalComponent'
      },
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onSave() {
    this.errors = [];
    setTimeout(() => {
      this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';
    });
    this.masterDataLocationService.updateInventory( this.items ).subscribe( result => {
      if (result.data.success) {
       this.dialogRef.close();
       this.headerService.next();
      } else if (result.data.errors) {
        this.errors = result.data.errors;
        setTimeout(() => {
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
        });
      } else {
        this.toastr.error('', _lang('Service Error'), {
          timeOut: 3000,
        });
      }
    });
  }

  selectCompany() {
    let dialogRef = this.dialog.open(CompanyFinderModalComponent, {
      width: window.innerWidth - 100 + 'px',
      height: window.innerHeight - 80 + 'px',
      role: 'dialog',
      data: {title: _lang('Select Company'), CO1_ID: this.CO1_ID },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.CO1_NAME = selected.CO1_NAME;
        this.CO1_ID   = selected.CO1_ID;
      }
    })
  }

  selectShop() {
    let dialogRef = this.dialog.open(ShopFinderModalComponent, {
      width: window.innerWidth - 100 + 'px',
      height: window.innerHeight - 80 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Select Shop'),
        filters: {
          CO1_ID: this.CO1_ID
        },
        HIGHLIGHT_LO1_ID: this.LO1_ID,
        componentRef: this.elementRef
      },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.LO1_ID = selected['LO1_ID'];
        this.LO1_NAME = selected['LO1_NAME'];
      }
    })
  }

  clearCompany() {
    this.CO1_NAME = '';
    this.CO1_ID = 0;
  }

  clearShop() {
    this.LO1_ID = 0;
    this.LO1_NAME = '';
  }

  onChange(event) {
    console.log(event);
  }

  onDelete() {
    const me = this;
    this.confirmationService.confirm({
      message: this.deleteConfirmationMessage,
      accept: () => {
        this.selected.forEach(function(record) {
          me.items.forEach((item, index) => {
            if (item['MD1_PART_NUMBER'] === record['MD1_PART_NUMBER'])
              me.items.splice(index, 1);
          });
        });
        this.selected = [];
      }
    });
  }

  onAddOrder() {
    const dialogRef = this.dialog.open(MasterDataModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data - Stock at ') + this.LO1_NAME,
        componentRef: this,
        url: '/master-data/full-parts',
        filters: {
          LO1_TO_ID:     this.LO1_ID,
          LO1_FROM_ID:   null,
          "sort[CO1_NAME]": 'ASC',
          "sort[VD1_NAME]": 'ASC',
          "sort[MD1_PART_NUMBER]": 'ASC',
          usePagination: false
        }
      }
    });

    dialogRef.beforeClose().subscribe( parts => {
      this.addPartsToOrder(parts, true);
    })
  }

  addPartsToOrder( parts, autoFocus: boolean = false ) {
    if (parts) {
      parts.forEach( part => {
        if (!part['MD1_ORDER_QTY']) {
          part['MD1_ORDER_QTY'] = 1;
        }
        let filtered = this.items.filter( item => {
          return item['MD1_ID'] === part.MD1_ID;
        });

        if (!filtered || filtered.length === 0) {
          this.items.push(part);
        } else {
          filtered[0]['MD2_ON_HAND_QTY'] = parseInt(filtered[0]['MD2_ON_HAND_QTY']) + 1;
        }
      });

      if (autoFocus) {
        setTimeout(() => {this.setFocusToField(this.items.length - 1, 'qtyOnHandRow');}, 100);
      }
    }
  }

  //on press tab button in count fields and navigate to next input
  buttonPress(e, rowIndex: number, rowName: string) {
    if ( (e.keyCode === 9 || e.keyCode === 13) && rowIndex !== this.items.length - 1) {
      e.stopPropagation();
      const nextRowIndex = rowIndex + 1;
      this.setFocusToField(nextRowIndex, rowName);
    }
  }

  setFocusToField(rowIndex: number, rowName: string) {
    const row = rowName + rowIndex;
    var elem: HTMLElement = document.getElementById(row);
    if (elem) {
      elem.click();
    } else {
      let currentTd = <HTMLTableCellElement>document.getElementById(row);
      currentTd.nextSibling['click']();
    }
  }

  qtyKeyPress(e){
    return e.charCode >= 48 && e.charCode <= 57;
  }

  addLastPartQty(qty) {
    if (this.items.length > 0) {
      this.items[this.items.length - 1]['MD2_ON_HAND_QTY'] = parseInt(qty);
    }
  }
}
