import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInventoryModalComponent } from './edit-inventory-modal.component';

describe('EditInventoryModalComponent', () => {
  let component: EditInventoryModalComponent;
  let fixture: ComponentFixture<EditInventoryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInventoryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInventoryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
