import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartNumberLiveSearchComponent } from './part-number-live-search.component';

describe('PartNumberLiveSearchComponent', () => {
  let component: PartNumberLiveSearchComponent;
  let fixture: ComponentFixture<PartNumberLiveSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartNumberLiveSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartNumberLiveSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
