import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatAutocompleteModule, MatButtonModule, MatFormFieldModule, MatInputModule,
    MatProgressSpinnerModule
} from '@angular/material';
import { PartNumberLiveSearchComponent } from './part-number-live-search.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
    imports: [
        CommonModule,
        MatInputModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        SharedModule
    ],
    declarations: [ PartNumberLiveSearchComponent ],
    exports: [ PartNumberLiveSearchComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class PartNumberLiveSearchModule { }
