import {
    AfterContentInit,
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { MasterCatalogService } from '../../services/master-catalog.service';
import { Page } from '../../model/page';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import {MatInput} from "@angular/material";

@Component({
  selector: 'app-part-number-live-search',
  templateUrl: './part-number-live-search.component.html',
  styleUrls: ['./part-number-live-search.component.scss']
})
export class PartNumberLiveSearchComponent implements OnDestroy, AfterContentInit {

  @ViewChild('inputPartNumber') inputPartNumber: ElementRef;

  @Input() partNumber: string = '';
  @Input() partId: string    = '';

  @Output() partChange = new EventEmitter();

  filteredArray: any[] = [];
  isLoading = false;
  private page = new Page();
  sub: Subscription;
  private searchTimeout:any = null;

  constructor(private toastr: ToastrService,
              private masterCatalogService: MasterCatalogService) {}

  ngAfterContentInit(){
    var me = this;
    setTimeout(_ => {
      me.inputPartNumber.nativeElement.focus();
    }, 10);
  }

  ngOnDestroy() {
    if (this.sub){
      this.sub.unsubscribe();
    }
  }

  onSelected(event) {
    this.partId = event.option.value;
    var part = this.filteredArray.filter( part => {
      return part.MD0_ID == event.option.value;
    })[0];
    this.partNumber = part.MD0_PART_NUMBER;
    this.partChange.emit(part);
  }

  search(value) {
    this.page.filterParams['MD0_PART_NUMBER'] = value;
    clearTimeout(this.searchTimeout);
    this.searchTimeout = null;
    var me = this;
    this.searchTimeout = setTimeout( _ => {
      if (value.length > 2) {
        me.getItems();
      }
    }, 500);
  }

  getItems() {
    this.isLoading = true;
    this.sub = this.masterCatalogService.getSearch(this.page).subscribe(res => {
      if (res && res.data && res.data['currentVocabulary']) {
        this.filteredArray = res.data['currentVocabulary'];
      } else {
        this.filteredArray = [];
      }

      this.isLoading = false;
    },
    error => {
      this.isLoading = false;
      this.toastr.error(_lang('Vendor Catalogs Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

}