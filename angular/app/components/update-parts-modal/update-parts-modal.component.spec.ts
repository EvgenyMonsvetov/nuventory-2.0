import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePartsModalComponent } from './update-parts-modal.component';

describe('UpdatePartsModalComponent', () => {
  let component: UpdatePartsModalComponent;
  let fixture: ComponentFixture<UpdatePartsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePartsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePartsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
