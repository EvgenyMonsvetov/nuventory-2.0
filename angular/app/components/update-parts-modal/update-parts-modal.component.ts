import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { permissions } from '../../permissions';
import { MasterCatalogService } from '../../services/master-catalog.service';

@Component({
  selector: 'app-update-parts-modal',
  templateUrl: './update-parts-modal.component.html',
  styleUrls: ['./update-parts-modal.component.scss'],
  preserveWhitespaces: true
})
export class UpdatePartsModalComponent implements OnInit {
  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];
  selected: any;
  search: string;
  currentSearchIndex = -1;

  private loading = false;
  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private currentPageIndex: number = 0;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('partsModal')    partsModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('searchbar')     searchbar:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('footer')        footer:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableHeaderCb') tableHeaderCb:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;
  @ViewChild('searchInput')   searchInput:  ElementRef;

  masterRecord = false;

  constructor(public dialogRef: MatDialogRef<UpdatePartsModalComponent>,
              private masterCatalogService: MasterCatalogService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialog: MatDialog,
              private toastr: ToastrService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
  }

  ngOnInit() {
    this.loading = true;
    this.masterRecord = this.data.masterRecord;
    this.page.filterParams['MASTER_DATA'] = this.data.masterRecord ? 1 : 0;

    if (this.data.items) {
      const partIds = [];

      this.data.items.forEach(function (item) {
        if (item['MD0_ID'])
          partIds.push(item['MD0_ID']);
      });

      if (partIds && partIds.length > 0)
        this.page.filterParams['MD0_IDs'] = partIds.join(',');
    }

    this.cols = [
      { field: 'CO1_NAME',        header: _lang('CO1_NAME'),        visible: true, cb_visible: false, cb_selected: false, width: 200 },
      { field: 'MD0_PART_NUMBER', header: _lang('MD1_PART_NUMBER'), visible: true, cb_visible: true, cb_selected: true,   width: 120 },
      { field: 'MD0_DESC1',       header: _lang('MD1_DESC1'),       visible: true, cb_visible: true, cb_selected: true,   width: 250 },
      { field: 'MD0_BODY',        header: _lang('MD1_BODY'),        visible: true, cb_visible: true, cb_selected: true,   width: 70 },
      { field: 'MD0_DETAIL',      header: _lang('MD1_DETAIL'),      visible: true, cb_visible: true, cb_selected: true,   width: 78 },
      { field: 'MD0_PAINT',       header: _lang('MD1_PAINT'),       visible: true, cb_visible: true, cb_selected: true,   width: 75 },
      { field: 'MD0_UNIT_PRICE',  header: _lang('MD1_UNIT_PRICE'),  visible: true, cb_visible: true, cb_selected: true,   width: 100 },
      { field: 'UM1_DEFAULT_PRICING_NAME', header: _lang('UM1_PRICE_NAME'),  visible: true, cb_visible: true, cb_selected: true,   width: 100 },
      { field: 'MD0_MARKUP',      header: _lang('MD1_MARKUP'),      visible: true, cb_visible: true, cb_selected: true,   width: 100 },
      { field: 'FI0_ID',          header: _lang('Image'),           visible: true, cb_visible: true, cb_selected: true,   width: 100 }
    ];

    this.page.pageNumber = 0;
    this.getParts();
  }

  getContentDialogHeight(): number{
    return (this.partsModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight + 51);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + this.tableHeaderCb.nativeElement.offsetHeight +
        this.searchbar.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight + 12);
  }

  getParts() {
    this.masterCatalogService.getAllParts(this.page).subscribe(res => {
          this.items = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Vendor Catalogs') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  onInput(value) {
    this.search = value;
    this.currentSearchIndex = -1;
    this.onSearch();
  }

  onSearch() {
    let index = 0;
    if (this.currentSearchIndex >= 0)
      index = this.currentSearchIndex  + 1;

    for (index; index < this.items.length; index ++) {
      if (this.items[index]) {
        if ((this.items[index].MD0_PART_NUMBER && this.items[index].MD0_PART_NUMBER.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.items[index].MD0_DESC1 && this.items[index].MD0_DESC1.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.items[index].MD0_UPC1 && this.items[index].MD0_UPC1.toString().toLowerCase().search(this.search.toLowerCase()) !== -1)) {
          this.currentSearchIndex = index;
          this.setCurrentPage(index);
          const row = 'row' + this.currentSearchIndex;

          setTimeout(() => {this.selected = this.items[index];
            const elem = document.getElementById(row);
            if (elem) {
              elem.scrollIntoView();
            }
          });
          break;
        }
      }
    }
  }

  onPage(event) {
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.currentSearchIndex = 0;
    this.currentPageIndex = event.first;
  }

  setCurrentPage(index: number) {
    const n = index === 0 ? 0 : Math.ceil(index / this.page.size) - 1;
    this.currentPageIndex = this.page.size * n;
  }

  openLanguageEditor() {
    this.closeDialog()
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Assign Form'),
        componentRef: this,
        data: [],
        componentName: 'UpdatePartsModalComponent'
      },
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onSave() {
    const parts: any = [];

    if (this.items) {
      this.loading = true;
      const cols = this.cols;
      const m = this.masterRecord;
      this.items.forEach(function(item) {
        const part: any = {};
        part['MD0_ID'] = item['MD0_ID'];
        part['MD1_ID'] = item['MD1_ID'];
        part['MASTER_DATA'] = m ? 1 : 0;
        cols.forEach(function(col) {
          switch (col.field) {
            case 'MD0_PART_NUMBER':
              part['MD0_PART_NUMBER'] = col['cb_selected'] ? item['MD0_PART_NUMBER'] : '';
              part['MD1_PART_NUMBER'] = col['cb_selected'] ? item['MD1_PART_NUMBER'] : '';
              break;
            case 'MD0_DESC1':
              part['MD0_DESC1'] = col['cb_selected'] ? item['MD0_DESC1'] : '';
              part['MD1_DESC1'] = col['cb_selected'] ? item['MD1_DESC1'] : '';
              break;
            case 'MD0_BODY':
              part['MD0_BODY'] = col['cb_selected'] ? item['MD0_BODY'] : '';
              part['MD1_BODY'] = col['cb_selected'] ? item['MD1_BODY'] : '';
              break;
            case 'MD0_DETAIL':
              part['MD0_DETAIL'] = col['cb_selected'] ? item['MD0_DETAIL'] : '';
              part['MD1_DETAIL'] = col['cb_selected'] ? item['MD1_DETAIL'] : '';
              break;
            case 'MD0_PAINT':
              part['MD0_PAINT'] = col['cb_selected'] ? item['MD0_PAINT'] : '';
              part['MD1_PAINT'] = col['cb_selected'] ? item['MD1_PAINT'] : '';
              break;
            case 'MD0_UNIT_PRICE':
              part['MD0_UNIT_PRICE'] = col['cb_selected'] ? item['MD0_UNIT_PRICE'] : '';
              part['MD1_UNIT_PRICE'] = col['cb_selected'] ? item['MD1_UNIT_PRICE'] : '';
              break;
            case 'UM1_DEFAULT_PRICING_NAME':
              part['UM1_DEFAULT_PRICING'] = col['cb_selected'] ? item['UM1_DEFAULT_PRICING'] : '';
              part['UM1_PRICING_ID'] = col['cb_selected'] ? item['UM1_PRICING_ID'] : '';
              break;
            case 'MD0_MARKUP':
              part['MD0_MARKUP'] = col['cb_selected'] ? item['MD0_MARKUP'] : '';
              part['MD1_MARKUP'] = col['cb_selected'] ? item['MD1_MARKUP'] : '';
              break;
            case 'FI0_ID':
              part['FI0_ID'] = col['cb_selected'] ? item['FI0_ID'] : '';
              part['FI1_ID'] = col['cb_selected'] ? item['FI1_ID'] : '';
              break;
            default:
              break;
          }
        });

        parts.push(part);
      });

      this.masterCatalogService.updateParts(parts).subscribe(result => {
        if (result.success) {
          this.getParts();
        } else {
          this.loading = false;
          this.toastr.error(_lang('On save parts error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        }
      });
    }
  }
}
