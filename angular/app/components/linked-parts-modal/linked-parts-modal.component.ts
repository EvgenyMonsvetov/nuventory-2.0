import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {permissions} from "../../permissions";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {LangPipe} from "../../views/pipes/lang.pipe";
import {ToastrService} from "ngx-toastr";
import {LanguageEditorModalComponent} from "../language-editor-modal/language-editor-modal.component";
import {_lang} from "../../pipes/lang";
import {MasterDataService} from "../../services/master.data.service";
import {Page} from "../../model/page";
import {Router} from "@angular/router";

@Component({
  selector: 'app-linked-parts-modal',
  templateUrl: './linked-parts-modal.component.html',
  styleUrls: ['./linked-parts-modal.component.scss'],
  providers: [
    MasterDataService
  ],
  preserveWhitespaces: true
})
export class LinkedPartsModalComponent implements OnInit {
  public loading: boolean = true;

  @ViewChild('LinkedPartsModal')   LinkedPartsModal: ElementRef;
  @ViewChild('header')                header:  ElementRef;
  @ViewChild('actionbar')             actionbar:  ElementRef;
  @ViewChild('tableHeader')           tableHeader:  ElementRef;
  @ViewChild('footer')                footer:  ElementRef;

  private permissions = permissions;
  private cols: Array<any> = [];
  private linkedParts: Array<any> = [];
  private scrollHeight: string = '50px';
  private dialogContentHeight: string = '0px';
  private selected: any;

  constructor(public dialogRef: MatDialogRef<LinkedPartsModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private masterDataService: MasterDataService,
              private lang: LangPipe,
              private router: Router,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.loading = true;

    this.cols = [
      { field: 'VD1_NAME',                header: _lang('VD1_NAME'),                visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MD2_ACTIVE',              header: _lang('MD2_ACTIVE'),              visible: true, export: {visible: true, checked: true}, width: 53 },
      { field: 'MD2_PRINT_FLAG',          header: _lang('MD2_PRINT_FLAG'),          visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),         visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'MD1_VENDOR_PART_NUMBER',  header: _lang('MD1_VENDOR_PART_NUMBER'),  visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MD1_OEM_NUMBER',          header: _lang('MD1_OEM_NUMBER'),          visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),               visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'MD1_UNIT_PRICE',          header: _lang('MD1_UNIT_PRICE'),          visible: true, export: {visible: true, checked: true}, width: 77 },
      { field: 'MD2_STOCK_LEVEL',         header: _lang('MD2_STOCK_LEVEL'),         visible: true, export: {visible: true, checked: true}, width: 90 },
      { field: 'MD2_ON_HAND_QTY',         header: _lang('MD2_ON_HAND_QTY'),         visible: true, export: {visible: true, checked: true}, width: 70 },
      { field: 'MD2_AVAILABLE_QTY',       header: _lang('MD2_AVAILABLE_QTY'),       visible: true, export: {visible: true, checked: true}, width: 75 },
      { field: 'MD2_MIN_QTY',             header: _lang('MD2_MIN_QTY'),             visible: true, export: {visible: true, checked: true}, width: 65 },
      { field: 'MD2_RACK',                header: _lang('MD2_RACK'),                visible: true, export: {visible: true, checked: true}, width: 45 },
      { field: 'UM1_PRICE_NAME',          header: _lang('UM1_PRICE_NAME'),          visible: true, export: {visible: true, checked: true}, width: 90 },
      { field: 'C00_NAME',                header: _lang('Nuventory Category'),      visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'CA1_NAME',                header: _lang('CA1_NAME'),                visible: true, export: {visible: true, checked: true}, width: 120 },
      { field: 'GL1_NAME',                header: _lang('GL1_NAME'),                visible: true, export: {visible: true, checked: true}, width: 80 },
      { field: 'MD1_UPC1',                header: _lang('MD1_UPC1'),                visible: true, export: {visible: true, checked: true}, width: 60 },
      { field: 'MD1_BODY',                header: _lang('MD1_BODY'),                visible: true, export: {visible: true, checked: true}, width: 50 },
      { field: 'MD1_DETAIL',              header: _lang('MD1_DETAIL'),              visible: true, export: {visible: true, checked: true}, width: 52 },
      { field: 'MD1_PAINT',               header: _lang('MD1_PAINT'),               visible: true, export: {visible: true, checked: true}, width: 50 },
    ];

    let page = new Page();
    page.filterParams['MD0_ID'] = this.data.MD0_ID;

    this.masterDataService.getFullParts(page).subscribe(res => {
          this.linkedParts = res['data'];
          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          }, 100);
          this.loading = false;
        },
        error => {
          this.toastr.error(_lang('Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  getContentDialogHeight(): number {
    return this.LinkedPartsModal.nativeElement.offsetHeight - (this.header.nativeElement.offsetHeight +
        this.actionbar.nativeElement.offsetHeight + this.footer.nativeElement.offsetHeight) - 8;
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - this.tableHeader.nativeElement.offsetHeight - 2;
  }


  openLanguageEditor() {
    this.closeDialog()
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Linked Parts'),
        componentRef: this,
        data: [],
        componentName: 'LinkedPartsModalComponent'
      },
    });
  }

  openLine() {
    if (this.selected && this.selected[0]) {
      this.onOpenLine(this.selected[0]);
    }
  }

  onOpenLine(rowData) {
    this.router.navigate(['master-data/edit/' + rowData['MD1_ID']]);
    this.dialogRef.close();
  }

  closeDialog() {
    this.dialogRef.close();
  }

}
