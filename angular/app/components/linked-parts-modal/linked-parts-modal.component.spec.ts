import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedPartsModalComponent } from './linked-parts-modal.component';

describe('LinkedPartsModalComponent', () => {
  let component: LinkedPartsModalComponent;
  let fixture: ComponentFixture<LinkedPartsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkedPartsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedPartsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
