import { Component, ElementRef, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { permissions } from '../../permissions';
import { LangService } from '../../services/lang.service';

@Component({
  selector: 'app-settings-menu',
  templateUrl: './settings-menu.component.html',
  styleUrls: ['./settings-menu.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})
export class SettingsMenuComponent implements OnInit {

  show: boolean = false;
  public elementRef;
  public permissions = permissions;
  private languages: any;
  private locale: any;
  userID = 0;
  userName = '';

  constructor(
    private oauthService: AuthService,
    private myElement: ElementRef,
    private languageService: LangService) {
    this.elementRef = myElement;
  }

  ngOnInit() {
    this.languages = this.languageService.languages;
    this.locale    = this.languageService.locale;
    this.userID = this.oauthService.userId;
    this.userName = this.oauthService.userName;
  }

  logout() {
    this.oauthService.logout();
  }

  changeLang(lang){
    this.languageService.reloadVocabulay(lang.LA1_LOCALE);
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if(inside){
      this.show = !this.show;
    }else{
      this.show = false;
    }
  }
}
