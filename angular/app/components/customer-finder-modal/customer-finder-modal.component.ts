import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-customer-finder-modal',
  templateUrl: './customer-finder-modal.component.html',
  styleUrls: ['./customer-finder-modal.component.scss'],
  providers: [
    CustomerService
  ],
  preserveWhitespaces: true
})
export class CustomerFinderModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = true;
  private page = new Page();
  private totalRecords: number = 0;
  private size: number = 50;
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('customerModal') customerModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('table')         table:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('footer')        footer:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<CustomerFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private customerService: CustomerService,
              private toastr: ToastrService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
  }

  ngOnInit() {
    this.cols = [
      { field: 'CO1_NAME',       header: _lang('CO1_NAME'),             width: 150 },
      { field: 'CU1_NAME',       header: _lang('CU1_NAME'),             width: 150 },
      { field: 'CU1_ADDRESS1',   header: _lang('#AddressStreet1#'),     width: 100 },
      { field: 'CU1_ADDRESS2',   header: _lang('#AddressStreet2#'),     width: 100 },
      { field: 'CU1_CITY',       header: _lang('#AddressCity#'),        width: 100 },
      { field: 'CU1_STATE',      header: _lang('#AddressState#'),       width: 100},
      { field: 'CU1_ZIP',        header: _lang('#AddressPostalCode#'),  width: 150 },
      { field: 'CY1_NAME',       header: _lang('#Countries#'),          width: 100 },
      { field: 'CU1_PHONE',      header: _lang('#Phone#'),              width: 120 },
      { field: 'CU1_FAX',        header: _lang('#Fax#'),                width: 100 }
    ];

    this.setPage();
  }

  setPage() {
    this.loading = true;

    this.customerService.getCustomers(this.page).subscribe(result => {
          this.page.totalElements = result.count;
          this.page.totalPages    = result.count / this.page.size;
          this.items = result.data;

          if(this.data.CU1_ID){
            this.selected = result.data.filter( item => {
              return item['CU1_ID'] === this.data.CU1_ID;
            })[0];
          }

          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.totalRecords = result.count;
          this.loading = false;
        },
        error => {
          this.loading = false;
          window.dispatchEvent(new Event('resize'));
          this.toastr.error(_lang('#Customers#') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.setPage();
  }

  getContentDialogHeight(): number {
    return (this.customerModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
        this.paginator.nativeElement.parentElement.parentElement.offsetHeight + 2);
  }

  closeDialog() {
    this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
    this.selected = record;
    this.closeDialog();
  }

}
