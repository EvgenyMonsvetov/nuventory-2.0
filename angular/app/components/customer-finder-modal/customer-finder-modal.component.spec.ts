import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerFinderModalComponent } from './customer-finder-modal.component';

describe('CustomerFinderModalComponent', () => {
  let component: CustomerFinderModalComponent;
  let fixture: ComponentFixture<CustomerFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
