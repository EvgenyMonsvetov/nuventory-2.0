import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageEditorModalComponent } from './language-editor-modal.component';

describe('LanguageEditorModalComponent', () => {
  let component: LanguageEditorModalComponent;
  let fixture: ComponentFixture<LanguageEditorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanguageEditorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageEditorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
