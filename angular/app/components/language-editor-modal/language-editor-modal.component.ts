import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LangService } from '../../services/lang.service';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { ExportModalComponent } from '../export-modal/export-modal.component';
import { TranslationJsonService } from '../../services/translation-json.service';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-language-editor-modal',
  templateUrl: './language-editor-modal.component.html',
  styleUrls: ['./language-editor-modal.component.scss'],
  preserveWhitespaces: true
})
export class LanguageEditorModalComponent implements OnInit {
  selectedColumns: any[] = [];
  cols: any[] = [];
  items: any[] = [];

  //default columns
  key = { field: 'LA2_KEY', header: {label: _lang('Key'), value: '0'}, visible: true, export: {visible: true, checked: true} };

  @ViewChild('languageModal') languageModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  title = _lang('Language Editor');

  private pageLang = new Page();
  private sizeLang: number = 100;
  private pageLabel = new Page();
  private sizeLabel: number = 100;

  private loading: boolean = false;
  private selected: any = null;
  private nameBeforeEdit: string;
  private className: string;

  constructor(public dialogRef: MatDialogRef<LanguageEditorModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private langService: LangService,
              private translationJsonService: TranslationJsonService
  ) {
    let component = this.data.componentRef;
    this.className = this.data.componentName ? this.data.componentName : component.constructor.name;
    this.pageLang.size = this.sizeLang;
    this.pageLabel.size = this.sizeLabel;
  }

  ngOnInit() {
    this.loading = true;

    this.title = _lang('Language Editor');
    this.title += this.data['title'] && this.data['title'].length > 0 ? ' - ' + this.data['title'] : '';

    this.langService.getLanguages(this.pageLang).subscribe(result => {
          this.parseLanguages(result.data['currentVocabulary']);
          this.getChildrenKeys();
        },
        error => {
          this.toastr.error(_lang('Languages') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  //create dynamical columns from languages list
  parseLanguages(value) {
    this.cols = [this.key];

    for (var k = 0; k < value.length; k++) {
      let item = value[k];
      let obj = {field: item['LA1_ID'], visible: true, header: {label: item['LA1_NAME'], value: item['LA1_ID']},  export: {visible: true, checked: true}};

      this.cols.push(obj);
    }

    this.selectedColumns = this.cols.filter( item => {
      return item['field'] !== 'LA2_KEY';
    });
  }

  //get all translation keys for current view
  getChildrenKeys() {
    let keys = this.translationJsonService.getKeysByClassName(this.className);
    let dataKeys = this.data.data;

    if (dataKeys) {
      for (var j = 0; j < dataKeys.length; j++) {
        let isFound = keys.filter( item => {
          return item === dataKeys[j];
        });

        if (!isFound.length)
          keys.push(dataKeys[j]);
      }
    }

    if (keys && keys.length > 0) {
      this.getLabelsByKeys(keys);
    } else {
      this.setLabels([], []);
    }
  }

  //get translation labels for all keys for current view
  getLabelsByKeys(value) {
    let keys = value.toString();

    let langs: any[] = [];
    this.selectedColumns.forEach( col => {
      if (col['visible']) {
        langs.push(col['header']['value']);
      }
    });
    this.pageLabel.filterParams['langs'] = langs.join(',');

    this.langService.getLabelsByKeys(keys, this.pageLabel)
        .then( res => {
          this.setLabels(value, res['currentVocabulary']);
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Labels') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  //create key objects with all translations
  setLabels(keys: any, lang: any[]) {
    let vocabulary: any[] = [];
    for (var i = 0; i < keys.length; i++) {
      let key = keys[i];
      let item = {};
      item['LA2_KEY'] = key;

      let langs = lang.filter( v => {
        return v['LA2_KEY'] === key;
      });

      if (langs && langs.length > 0) {
        item = langs[0];

        for (const key of Object.keys(item)) {
            item[key] = item[key] ? unescape(item[key]) : '';
        }
      }

      vocabulary.push(item);
    }

    this.items = vocabulary;

    setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';}, 1000);

    this.loading = false;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onEditInit(event) {
    this.nameBeforeEdit = event.data[event.field.field];
  }

  onPressEnter(event, rowData, data, id, colName) {
    if (event.keyCode === 13 || event.keyCode === 9) {
      this.onEditCell(rowData, data, id, colName);
    } else if (event.keyCode === 27) {
      rowData[colName['label']] = this.nameBeforeEdit;
    }
  }

  onEditCell(rowData, data, id, colName) {
    if (this.nameBeforeEdit !== data) {
      this.loading = true;

      this.langService.saveTranslation({'LA2_KEY': id, 'LA1_ID': colName.value, 'LA2_TEXT': data}).subscribe(result => {
        this.loading = false;
      },
      error => {
        this.loading = false;
        this.toastr.error(_lang('On label saving error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      });
    }
  }

  getContentDialogHeight(): number{
    return (this.languageModal.nativeElement.parentElement.parentElement.offsetHeight) -
        (this.header.nativeElement.offsetHeight +
        this.actionbar.nativeElement.offsetHeight + this.header.nativeElement.offsetTop + 10);
  }

  getScrollHeight(): number{
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        console.log(columns);
      }
    });
  }
}
