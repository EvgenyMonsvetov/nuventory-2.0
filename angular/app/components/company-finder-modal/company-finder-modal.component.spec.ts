import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyFinderModalComponent } from './company-finder-modal.component';

describe('CompanyFinderModalComponent', () => {
  let component: CompanyFinderModalComponent;
  let fixture: ComponentFixture<CompanyFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
