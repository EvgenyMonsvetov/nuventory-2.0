import {
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CompanyService } from '../../services/company.service';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import {_lang} from '../../pipes/lang';

@Component({
  selector: 'app-company-finder-modal',
  templateUrl: './company-finder-modal.component.html',
  styleUrls: ['./company-finder-modal.component.scss'],
  providers: [
    CompanyService,
  ],
  preserveWhitespaces: true
})
export class CompanyFinderModalComponent implements OnInit {

  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('companyModal') companyModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('table')        table:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<CompanyFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private companyService: CompanyService,
              private toastr: ToastrService,
              private element: ElementRef) {
  }

  ngOnInit() {
    this.cols = [
        { field: 'CO1_SHORT_CODE',    header: _lang('CO1_SHORT_CODE'), width: 120 },
        // { field: 'CY1_ID',            header: _lang('CY1_NAME'), width: 140 },
        { field: 'CO1_NAME',          header: _lang('CO1_NAME'), width: 140},
        { field: 'CO1_ADDRESS1',      header: _lang('#AddressStreet1#'), width: 140 },
        // { field: 'CO1_ADDRESS2',      header: _lang('#AddressStreet2#'), width: 140},
        { field: 'CO1_CITY',          header: _lang('#AddressCity#'), width: 140},
        { field: 'CO1_STATE',         header: _lang('#AddressState#'), width: 60},
        { field: 'CY1_SHORT_CODE',    header: _lang('CY1_SHORT_CODE'), width: 100},
        { field: 'CO1_PHONE',         header: _lang('#Phone#'), width: 140},
        { field: 'CO1_MITCHELL_FLAG', header: _lang('CO1_MITCHELL_FLAG'), width: 80, align: 'center'},
        { field: 'CO1_CCC_FLAG',      header: _lang('CO1_CCC_FLAG'), width: 80, align: 'center'},
        { field: 'CO1_LITE',          header: _lang('CO1_LITE'), width: 80, align: 'center'}
    ];

    this.setPage({offset: 0});
  }

   setPage(pageInfo) {
      this.loading = true;
      this.page.pageNumber = pageInfo.offset;

      this.companyService.getCompanies(this.page).subscribe(result => {

          this.page.totalElements = result.count;
          this.page.totalPages    = result.count / this.page.size;
          this.items = result.data.map( item => {
            item['CO1_MITCHELL_FLAG'] = item['CO1_MITCHELL_FLAG'] == 1 ? 'x' : '';
            item['CO1_CCC_FLAG']      = item['CO1_CCC_FLAG'] == 1 ? 'x' : '';
            item['CO1_LITE']          = item['CO1_LITE'] == 1 ? 'x' : '';
            return item;
          });
          if(this.data.CO1_ID){
              this.selected = result.data.filter( item => {
                  return item['CO1_ID'] == this.data.CO1_ID;
              })[0];
          }

          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.loading = false;
      },
      error => {
          this.loading = false;
          this.toastr.error(_lang('#Customers#') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
          });
      });
    }

    getContentDialogHeight(): number {
      return (this.companyModal.nativeElement.offsetHeight ) -
                (this.header.nativeElement.offsetHeight +
                 this.footer.nativeElement.offsetHeight);
    }

    getScrollHeight(): number {
      return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
    }

  closeDialog() {
      this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
      this.selected = record;
      this.closeDialog();
  }
}
