import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'app-company-location-dropdown',
  templateUrl: './company-location-dropdown.component.html'
})
export class CompanyLocationDropdownComponent implements OnInit, OnDestroy {
  /** control for the selected filters */
  public companyLocationCtrl: FormControl = new FormControl();
  public companyLocationFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyCompanyLocation = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredCompanyLocations: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  private companyLocationDP: Array<any> = [];
  private disabledField:boolean = false;

  @Output() dataChange = new EventEmitter();
  @Input() field: any;

  selectionChanged(value) {
    this.dataChange.emit(value);
  }

  @Input()
  get dataProvider() {
    return this.companyLocationDP;
  }

  set dataProvider(val) {
    this.companyLocationDP = val;
    this.filterCompanyLocations();
  }

  @Input()
  get disabled() {
    return this.disabledField;
  }

  set disabled(val) {
    this.disabledField = val;
  }

  constructor() { }

  ngOnInit() {
    this.companyLocationFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCompanyLocation))
        .subscribe(() => {
          this.filterCompanyLocations();
        });
  }

  private filterCompanyLocations() {
    if (!this.companyLocationDP) {
      return;
    }
    // get the search keyword
    let search = this.companyLocationFilterCtrl.value;
    if (!search) {
      this.filteredCompanyLocations = this.companyLocationDP;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the companyLocations
    this.filteredCompanyLocations = this.companyLocationDP.filter(company => company['CO1_NAME'] && company['CO1_NAME'].toLowerCase().indexOf(search) > -1
    || company['LO1_NAME'] && company['LO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  ngOnDestroy() {
    this._onDestroyCompanyLocation.next();
    this._onDestroyCompanyLocation.complete();
  }

}
