import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { CompanyLocationDropdownComponent } from './company-location-dropdown.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ CompanyLocationDropdownComponent ],
    exports: [ CompanyLocationDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class CompanyLocationDropdownModule { }
