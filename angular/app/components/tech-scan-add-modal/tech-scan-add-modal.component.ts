import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { BaseModalComponent } from '../base-modal/base-modal.component';
import { TechScanAddFormComponent } from '../tech-scan-add-form/tech-scan-add-form.component';

@Component({
  selector: 'app-tech-scan-add-modal',
  templateUrl: './tech-scan-add-modal.component.html',
  preserveWhitespaces: true
})
export class TechScanAddModalComponent extends BaseModalComponent {
  @ViewChild('techScanAddForm')  techScanAddForm: TechScanAddFormComponent;

  technicianShortCode: string = '';

  constructor(public dialogRef: MatDialogRef<TechScanAddModalComponent>,
              public  dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    super();
    this.tagSave = 'TECH_SAVE';
    this.technicianShortCode = this.data['technicianShortCode'] && this.data['technicianShortCode'].length > 0 ?
        this.data['technicianShortCode'] : '';
  }

  save() {
    this.techScanAddForm.onSave();
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }
}