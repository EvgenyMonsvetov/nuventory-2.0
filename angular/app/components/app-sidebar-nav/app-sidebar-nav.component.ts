import { Component, Inject, ViewChild } from '@angular/core';
import { permissions } from './../../permissions';
import {ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, NavigationStart, Router} from '@angular/router';
import { MatDialog, MatExpansionPanel } from '@angular/material';
import { navigation } from './../../_nav';
import { HeaderService } from '../../services/header.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { StoreOrderService } from '../../services/store-order.service';
import { TechScanService } from '../../services/tech-scan.service';
import { JobTicketService } from '../../services/job-ticket.service';
import { PurchaseOrderService } from '../../services/purchase-order.service';
import { DOCUMENT } from '@angular/common';
import { EditInventoryModalComponent } from '../edit-inventory-modal/edit-inventory-modal.component';
import { _lang } from '../../pipes/lang';
import { SetupWizardModalComponent } from '../setup-wizard-modal/setup-wizard-modal.component';
import { InvoiceService } from '../../services/invoice.service';
import { InventoryService } from '../../services/inventory.service';
import { StatementService } from '../../services/statement.service';
import { ConfirmationService } from 'primeng/api';
import { AppSidebarNavService } from '../../services/app-sidebar-nav.service';
import { Page } from '../../model/page';

@Component({
  selector: 'app-sidebar-nav',
  templateUrl: './app-sidebar-nav.component.html',
  styleUrls: ['./app-sidebar-nav.component.scss'],
  providers: [
    StoreOrderService,
    TechScanService,
    JobTicketService,
    PurchaseOrderService,
    InvoiceService,
    StatementService,
    ConfirmationService,
    InventoryService,
  ],
  preserveWhitespaces: true
})
export class AppSidebarNavComponent {

  @ViewChild('inventory') inventory:  MatExpansionPanel;
  @ViewChild('setup')     setup:      MatExpansionPanel;
  @ViewChild('invoicing') invoicing:  MatExpansionPanel;
  @ViewChild('reporting') reporting:  MatExpansionPanel;

  public navigation = navigation;
  public permissions = permissions;
  private subscription: Subscription;

  private storeOrderCanBeAdded: boolean = true;
  private storeOrderCanBeAddedError: any;

  private techScanCanBeAdded: boolean = true;
  private techScanCanBeAddedError: any;

  private jobTicketCanBeAdded: boolean = true;
  private jobTicketCanBeAddedError: any;

  private purchaseOrderCanBeAdded: boolean = true;
  private purchaseOrderCanBeAddedError: any;

  private creditMemoCanBeAdded: boolean = true;
  private creditMemoCanBeAddedError: any;

  private editInventoryCanBeAdded: boolean = true;
  private editInventoryCanBeAddedError: any;

  private currentUrl: string = '';

  private confirmationHeader: string = _lang('#GenerateInvoice#');
  private acceptLabel: string = _lang('Yes');

  public isDivider(item) {
    return item.divider ? true : false
  }

  public isTitle(item) {
    return item.title ? true : false
  }

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private headerService: HeaderService,
      private appSidebarNavService: AppSidebarNavService,
      private toastr: ToastrService,
      private storeOrderService: StoreOrderService,
      private techScanService: TechScanService,
      private jobTicketService: JobTicketService,
      private invoiceService: InvoiceService,
      private statementService: StatementService,
      private confirmationService: ConfirmationService,
      private inventoryService: InventoryService,
      @Inject(DOCUMENT) document,
      public  dialog: MatDialog,
      private purchaseOrderService: PurchaseOrderService
  ) {
    var me = this;

    this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd && me.currentUrl !== evt.url ) {
            if (me.checkUrlInSection('inventory', evt.url, me.currentUrl)) {
              if (me.inventory) me.inventory.open();
              if (me.setup) me.setup.close();
              if (me.invoicing) me.invoicing.close();
              if (me.reporting) me.reporting.close();
            } else if (me.checkUrlInSection('invoicing', evt.url, me.currentUrl)) {
              if (me.inventory) me.inventory.close();
              if (me.setup) me.setup.close();
              if (me.invoicing) me.invoicing.open();
              if (me.reporting) me.reporting.close();
            } else if (me.checkUrlInSection('reporting', evt.url, me.currentUrl)) {
              if (me.inventory) me.inventory.close();
              if (me.setup) me.setup.close();
              if (me.invoicing) me.invoicing.close();
              if (me.reporting) me.reporting.open();
            } else if (me.checkUrlInSection('setup', evt.url, me.currentUrl)) {
              if (me.inventory) me.inventory.close();
              if (me.setup) me.setup.open();
              if (me.invoicing) me.invoicing.close();
              if (me.reporting) me.reporting.close();
            } else {
              if (me.inventory) me.inventory.close();
              if (me.setup) me.setup.close();
              if (me.invoicing) me.invoicing.close();
              if (me.reporting) me.reporting.close();
            }
            me.currentUrl = evt.url;
            return;
        }
    });

    this.subscription = this.headerService.subscribe({
        next: function (v) {
          var storeOrderResult = me.storeOrderService.isOrderCanBeCreated(me.headerService);
          me.storeOrderCanBeAdded = storeOrderResult['canBeCreated'];
          me.storeOrderCanBeAddedError = storeOrderResult['error'];

          var techScanResult = me.techScanService.isScanCanBeCreated(me.headerService);
          me.techScanCanBeAdded = techScanResult['canBeCreated'];
          me.techScanCanBeAddedError = techScanResult['error'];

          var jobTicketResult = me.jobTicketService.isCanBeCreated(me.headerService);
          me.jobTicketCanBeAdded = jobTicketResult['canBeCreated'];
          me.jobTicketCanBeAddedError = jobTicketResult['error'];

          var purchaseOrderResult = me.purchaseOrderService.isPOCanBeCreated(me.headerService);
          me.purchaseOrderCanBeAdded = purchaseOrderResult['canBeCreated'];
          me.purchaseOrderCanBeAddedError = purchaseOrderResult['error'];

          var creditMemoResult = me.invoiceService.isCreditMemoCanBeCreated(me.headerService);
          me.creditMemoCanBeAdded = creditMemoResult['canBeCreated'];
          me.creditMemoCanBeAddedError = creditMemoResult['error'];

          var editInventoryResult = me.inventoryService.isEditInventoryCanBeCreated(me.headerService);
          me.editInventoryCanBeAdded = editInventoryResult['canBeCreated'];
          me.editInventoryCanBeAddedError = editInventoryResult['error'];
        }
    });
  }

  getActiveParam(url) {
    let index = url.indexOf(';active=');
    let parsedUrl = url;
    if (index >= 0) {
      parsedUrl = parsedUrl.substr(index + 8, url.length - 1);
    } else {
      return true;
    }

    index = parsedUrl.indexOf(';');
    while (index >= 0) {
      parsedUrl = parsedUrl.substr(0, index);
      index = parsedUrl.indexOf(';');
    }

    if (parsedUrl && parsedUrl.length > 0) {
      return parsedUrl === 'false' ? false : true;
    }

    return true;
  }

  checkUrlInSection(panelCls: string, url: string, prevUrl: string) {
    const route = this.parseUrl(url);
    const panelEl = document.getElementsByClassName(panelCls);
    if(panelCls == route){
      return true;
    }else if (panelEl && panelEl[0]) {
      const routerLinkEl = panelEl[0].getElementsByClassName(route);
      if (routerLinkEl && routerLinkEl[0]) {
        if (prevUrl === '')
          setTimeout(() => {routerLinkEl[0].scrollIntoView();}, 1000);
        return true;
      }
    }
    return false;
  }

  parseUrl(value) {
    let parsedUrl = value;

    // special url cases
    if (value.indexOf('materials-budget-report') >= 0) {
      return 'materials-budget-report';
    }
    if (value.indexOf('/invoice/credit-memos') >= 0) {
      return 'credit-memos';
    }
    if (value.indexOf('/invoice/') >= 0) {
      return value.substr(9, value.length);
    }
    if (value.indexOf('/statement/') >= 0) {
      return value.substr(11, value.length);
    }
    if (value.indexOf('/inventory/') >= 0) {
      return value.substr(11, value.length);
    }
    if (value.indexOf('/report/') >= 0) {
      return value.substr(8, value.length);
    }
    if (value.indexOf('tech-usage-report') >= 0) {
      return 'tech-usage-report';
    }
    if (value.indexOf('tech-usage-mtd-report') >= 0) {
      return 'tech-usage-mtd-report';
    }
    if (value.indexOf('inventory-optimization') >= 0) {
      return 'inventory-optimization';
    }
    if (value.indexOf('physical-inventory') >= 0) {
      return 'physical-inventory';
    }
    if (value.indexOf('import-report-data') >= 0) {
      return 'import-report-data';
    }
    if (value.indexOf('manual-inventory-edits-report') >= 0) {
      return 'manual-inventory-edits-report';
    }

    if (value.indexOf(';') > 0) {
      parsedUrl = value.substr(0, value.indexOf(';'));
    }

    if (parsedUrl.indexOf('/') === 0) {
      parsedUrl = value.substr(1, parsedUrl.length - 1);
    }

    let index = parsedUrl.indexOf('/');
    if (index > 0) {
      parsedUrl = parsedUrl.substr(0, index);
    }
    index = parsedUrl.indexOf('?');
    if (index > 0) {
      parsedUrl = parsedUrl.substr(0, index);
    }
    return parsedUrl;
  }

  editInventory() {
    if (this.editInventoryCanBeAdded) {
      const route = this.getRoute();
      let selectedItems = [];

      if (route.snapshot.data.component && route.snapshot.data.component === 'MasterDataComponent' && route.snapshot.data['selected']) {
        selectedItems = route.snapshot.data['selected'];
      }
      this.appSidebarNavService.setDeatachChangeDetection(true);
      this.appSidebarNavService.next();
      let dialogRef = this.dialog.open(EditInventoryModalComponent, {
        width: window.innerWidth-100+'px',
        height: window.innerHeight-80+'px',
        role: 'dialog',
        data: {title: _lang('Set Inventory'), records: selectedItems},
      });

      dialogRef.beforeClose().subscribe( result => {
        this.appSidebarNavService.setDeatachChangeDetection(false);
        this.appSidebarNavService.next();
      })
    } else {
      this.toastr.error(this.editInventoryCanBeAddedError);
    }
  }

  getRoute() {
    let route = this.route.firstChild;
    let child = route;
    while (child) {
      if (child.firstChild) {
        child = child.firstChild;
        route = child;
      } else {
        child = null;
      }
    }

    return route;
  }

  storeOrderAdd() {
    if (this.storeOrderCanBeAdded) {
      this.router.navigate(['/store-orders/add/0']);
    } else {
      this.toastr.error(this.storeOrderCanBeAddedError);
    }
  }

  techScanAdd() {
    if (this.techScanCanBeAdded) {
      this.router.navigate(['/tech-scans/add/0']);
    } else {
      this.toastr.error(this.techScanCanBeAddedError);
    }
  }

  jobTicketAdd() {
    if (this.jobTicketCanBeAdded) {
      this.router.navigate(['/job-tickets/add/0']);
    } else {
      this.toastr.error(this.jobTicketCanBeAddedError);
    }
  }

  purchaseOrderAdd() {
    if (this.purchaseOrderCanBeAdded) {
      this.router.navigate(['/purchase-orders/add/0']);
    } else {
      this.toastr.error(this.purchaseOrderCanBeAddedError);
    }
  }

  creditMemoAdd() {
    if (this.creditMemoCanBeAdded) {
      this.router.navigate(['/invoice/credit-memos/add/0']);
    } else {
      this.toastr.error(this.creditMemoCanBeAddedError);
    }
  }

  public dashboardClick() {
     this.inventory.close();
     this.setup.close();
     this.invoicing.close();
     this.reporting.close();
  }

  public customerClick(){
    const route = this.getRoute();
    var url = route.snapshot['_routerState'].url;

    if (url.indexOf('/master-data') > -1) {
      this.router.routeReuseStrategy.shouldReuseRoute = function (future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot) {
        return false;
      }
    }
  }

  onMmaterialBudgetSetupClick() {
    if (this.headerService.LO1_ID < 1) {
      this.acceptLabel = _lang('OKButton');
      this.confirmationHeader = _lang('Material Budget Setup');

      this.confirmationService.confirm({
        message: _lang('The Material Budget can only be applied for a shop'),
        rejectVisible: false
      });
    } else {
      this.router.navigate(['/material-budget-setup']);
    }
  }

  openSetupWizard() {
    this.dialog.open(SetupWizardModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {title: _lang('Setup Wizard'), componentRef: this, data: []},
    });
  }

  generateAllInvoicesClick() {
    if (this.headerService.CO1_ID.toString().length > 0) {

      if (this.headerService.LO1_ID > 0) {
        if (this.headerService.isCentralLocation(this.headerService.LO1_ID)) {
          this.acceptLabel = _lang('OKButton');
          this.confirmationHeader = _lang('#GenerateInvoice#');

          this.confirmationService.confirm({
            message: _lang(':ErrorInvoiceCentral:') + ' ' + _lang('#AllShops?#').replace('%%1%%', _lang('#SelectShop#')),
            rejectVisible: false
          });
        } else {
          this.generateAllInvoicesMessage();
        }
      } else {
        this.generateAllInvoicesMessage();
      }
    }
  }

  generateAllInvoicesMessage() {
    this.acceptLabel = _lang('Yes');
    this.confirmationHeader = _lang('#GenerateInvoice#');

    this.confirmationService.confirm({
      message: _lang('#GenStatements#') + '<br/>' + _lang('CO1_NAME') + ': ' + this.headerService.CO1_NAME + '<br/>' + _lang('#And#') + '<br/>' +
      _lang('LO1_NAME') + ': ' + (this.headerService.LO1_ID > 0 ? this.headerService.LO1_NAME : _lang('#SelectShop#')) + '<br/>' + _lang('#Continue?#'),
      rejectVisible: true,
      accept: () => {
        this.generateAllInvoices();
      }
    });
  }

  generateAllInvoices() {
    this.statementService.generateAllInvoices().subscribe(res => {
          if(res['data'] && res['data']['success']) {
            this.generateAllInvoicesFinished(res['data']['IN1_IDs']);
          } else if (res['data'] && res['data']['errors']) {
            this.toastr.error(res['data']['errors'], _lang('Service Error'), {
              timeOut: 3000,
            });
          } else if (res['data'] && !res['data']['success']) {
            this.acceptLabel = _lang('OKButton');
            this.confirmationHeader = _lang('INVOICING ERROR');
            this.confirmationService.confirm({
              message: _lang('There are no orders to be invoiced.'),
              rejectVisible: false
            });
          }
        },
        error => {
          this.toastr.error(_lang('Generate All Invoices') + ' ' + _lang('error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  generateAllInvoicesFinished(IN1_IDs) {
    if (this.currentUrl === '/invoices') {
      this.appSidebarNavService.next();
    }

    this.acceptLabel = _lang('Yes');
    this.confirmationHeader = _lang('INVOICING');
    this.confirmationService.confirm({
      message: _lang('Invoice(s) generated successfully.') + '<br/>' + _lang('Would you like to also print it/them?'),
      rejectVisible: true,
      accept: () => {
        this.printGeneratedAllInvoices(IN1_IDs);
      }
    });
  }

  printGeneratedAllInvoices(IN1_IDs) {
    if (IN1_IDs && IN1_IDs.length) {
      let page = new Page();
      page.filterParams['IN1_IDs'] = IN1_IDs;
      this.invoiceService.printPDF(page).subscribe( result => {
          },
          error => {
            this.toastr.error(_lang('On print PDF error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    }
  }

}

export const APP_SIDEBAR_NAV = [
  AppSidebarNavComponent
];
