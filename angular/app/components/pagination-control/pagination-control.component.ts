import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination-control',
  templateUrl: './pagination-control.component.html',
  styleUrls: ['./pagination-control.component.scss'],
  preserveWhitespaces: true
})
export class PaginationControlComponent implements OnInit, OnDestroy {
  @Input() name: string = '';
  @Input() rowsPerPage: number = 100;
  @Input() totalRecords: number = 0;
  @Output() pagerChange = new EventEmitter();
  currentPage: number = 0;

  constructor() { }

  ngOnInit() {
  }

  paginate(event) {
    this.currentPage = event.page;
    this.rowsPerPage = event.rows;
    this.pagerChange.emit({rowPerPage: this.rowsPerPage, page: this.currentPage});
  }

  onRowPerPageChange(value) {
    this.rowsPerPage = value;
    this.pagerChange.emit({rowPerPage: this.rowsPerPage, page: this.currentPage});
  }

  onRefresh() {
    this.pagerChange.emit({rowPerPage: this.rowsPerPage, page: this.currentPage});
  }

  ngOnDestroy() {
    this.pagerChange.unsubscribe();
  }
}
