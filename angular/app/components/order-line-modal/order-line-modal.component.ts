import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { OrderLineService } from '../../services/order-line.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-order-line-modal',
  templateUrl: './order-line-modal.component.html',
  styleUrls: ['./order-line-modal.component.scss'],
  providers: [
    OrderLineService,
  ],
  preserveWhitespaces: true
})
export class OrderLineModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[];
  items: any[];

  private loading = true;
  private record: any = null;
  private dialogContentHeight: string = '0px';

  @ViewChild('orderLineModal')  orderLineModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('actionbar')     actionbar: ElementRef;

  constructor(public dialogRef: MatDialogRef<OrderLineModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public  dialog: MatDialog,
              private orderLineService: OrderLineService,
              private router: Router,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.record = this.orderLineService.initialize();
    this.getData();
  }

  getData() {
    this.orderLineService.getById(this.data.OR2_ID).subscribe( result => {
          if (result[0]) {
            let data = result[0];
            this.record = data;
          }
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Order Line') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  ngAfterViewInit() {
    setTimeout(() => this.dialogContentHeight = this.getContentDialogHeight() + 'px', 0);
  }

  getContentDialogHeight(): number {
    return (this.orderLineModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight + this.actionbar.nativeElement.offsetHeight + 25);
  }

  saveClick() {
    this.orderLineService.update(this.record).subscribe(result => {
      this.closeDialog(true);
    });
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Order Line Form'),
        componentRef: this,
        data: [],
        componentName: 'OrderLineModalComponent'
      },
    });
  }

}
