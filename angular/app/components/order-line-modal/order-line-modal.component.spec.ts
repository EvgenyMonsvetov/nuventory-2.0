import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreOrderLineModalComponent } from './order-line-modal.component';

describe('StoreOrderLineModalComponent', () => {
  let component: StoreOrderLineModalComponent;
  let fixture: ComponentFixture<StoreOrderLineModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreOrderLineModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreOrderLineModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
