import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLaborHoursModalComponent } from './edit-labor-hours-modal.component';

describe('EditInventoryModalComponent', () => {
  let component: EditLaborHoursModalComponent;
  let fixture: ComponentFixture<EditLaborHoursModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLaborHoursModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLaborHoursModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
