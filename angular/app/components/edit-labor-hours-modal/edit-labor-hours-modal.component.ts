import {Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild} from '@angular/core';
import {permissions} from '../../permissions';
import {Page} from '../../model/page';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {_lang} from '../../pipes/lang';
import {HourService} from "../../services/hour.service";

@Component({
    selector: 'app-edit-labor-hours-modal',
    templateUrl: './edit-labor-hours-modal.component.html',
    styleUrls: ['./edit-labor-hours-modal.component.scss'],
    providers: [
        HourService
    ],
    preserveWhitespaces: true
})
export class EditLaborHoursModalComponent implements OnInit {
    @Output() select = new EventEmitter();

    private permissions = permissions;

    cols: any[] = [];
    items: any[] = [];

    private loading = true;
    private page = new Page();
    private totalRecords: number = 0;
    private size: number = 50;
    private selected: any = null;

    private scrollHeight: string = '0px';
    private dialogContentHeight: string = '0px';

    @ViewChild('editLaborHoursModal') editLaborHoursModal: ElementRef;
    @ViewChild('header') header: ElementRef;
    @ViewChild('table') table: ElementRef;
    @ViewChild('content') content: ElementRef;
    @ViewChild('footer') footer: ElementRef;
    @ViewChild('paginator') paginator: ElementRef;
    @ViewChild('tableHeader') tableHeader: ElementRef;

    constructor(public dialogRef: MatDialogRef<EditLaborHoursModalComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private hourService: HourService,
                private toastr: ToastrService) {
        this.page.size = this.size;
        this.page.pageNumber = 0;
    }

    ngOnInit() {

        this.cols = [
           { field: 'TE1_NAME', header: _lang('TECHNICAN'), visible: true, width: 150 }
        ];

        let date = new Date();
        let monthList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        for (let i = 1; i < 13; i++) {
            let month = date.getMonth();
            let year = date.getFullYear();
            let monthStr = ('0' + (month + 1).toString()).slice(-2);
            this.cols.push({
                field: year + '-' + monthStr,
                header: monthList[month] + ' ' + year,
                visible: true,
                width: 80
            });
            date.setMonth(month - 1);
        }
        this.setPage();
    }

    getHour(data: any): string {
        return data != null ? data.HO1_HOURS : '0';
    }

    setPage() {
        this.loading = true;
        this.page.filterParams['group_by_technican'] = true;

        var me = this;
        this.hourService.getTechHours(this.page).subscribe(result => {

            let items = result.data.map(function (item) {
                if(item['HO1_DATE']){
                    let date = item['HO1_DATE'].split('.');
                    let fieldName = date[2] + '-' + date[0].toString(2);
                    item['fieldName'] = fieldName;
                    me.cols.forEach( (col) => {
                        if(col.field == fieldName){
                            item[fieldName] = item['HO1_HOURS'];
                            return false;
                        }
                    });
                }
                return item;
            });

            let rows = [];
            items.forEach(function (item) {
                if(!rows[item['TE1_ID']]){
                    rows[item['TE1_ID']] = {
                        TE1_ID: item['TE1_ID'],
                        TE1_NAME: item['TE1_NAME'],
                        original: []
                    }
                }
                if(item['HO1_DATE']) {
                    rows[item['TE1_ID']][item['fieldName']] = item[item['fieldName']];
                }
            });

            me.cols.forEach( (col) => {
                rows.forEach(function (row) {
                    if (!row[col.field]) {
                        row[col.field] = 0.00;
                    }
                    row['original'][col.field] = row[col.field];
                });
            });


            let results = [];
            rows.forEach((row)=>{ results.push(row) });
            this.items = results;

            this.scrollHeight = this.getScrollHeight() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';

            this.totalRecords = result.count;
            this.loading = false;
        },
        error => {
            this.loading = false;
            window.dispatchEvent(new Event('resize'));
            this.toastr.error(_lang('#Customers#') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                timeOut: 3000,
            });
        });
    }

    getContentDialogHeight(): number {
        return (this.editLaborHoursModal.nativeElement.offsetHeight ) -
            (this.header.nativeElement.offsetHeight +
            this.footer.nativeElement.offsetHeight);
    }

    getScrollHeight(): number {
        return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
    }

    onSave(){
        var values = [];
        var me = this;
        this.items.forEach( (item) => {
            me.cols.forEach( (col) => {
                if(item[col.field] != item['original'][col.field]){
                    values.push({
                        TE1_ID: item.TE1_ID,
                        HO1_DATE: col.field,
                        value:  item[col.field]
                    });
                }
            });
        });
        if(values.length){
            this.loading = true;
            this.hourService.updateMultiple(values).subscribe(result => {
                this.loading = false;
                this.closeDialog();
            });
        }else{
            this.closeDialog();
        }
    }

    closeDialog() {
        this.dialogRef.close(this.selected);
    }
}
