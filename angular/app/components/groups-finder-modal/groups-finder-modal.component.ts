import {
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { RolesService } from '../../views/rights/roles/roles.service';
import { _lang } from '../../pipes/lang';


@Component({
  selector: 'app-groups-finder-modal',
  templateUrl: './groups-finder-modal.component.html',
  styleUrls: ['./groups-finder-modal.component.scss'],
  providers: [
    RolesService,
  ],
  preserveWhitespaces: true
})
export class GroupsFinderModalComponent implements OnInit {

  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('modal')        modal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('table')        table:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<GroupsFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private rolesService: RolesService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.cols = [
        { field: 'name',         header: _lang('Name'),        width: 120 },
        { field: 'description',  header: _lang('Description'), width: 140}
    ];

    this.setPage({offset: 0});
  }

   setPage(pageInfo) {
      this.loading = true;
      this.page.pageNumber = pageInfo.offset;

      this.rolesService.getAll(this.page).subscribe(result => {

          this.page.totalElements = result.count;
          this.page.totalPages    = result.count / this.page.size;
          this.items = result.data;

          if(this.data.name){
              this.selected = result.data.filter( item => {
                  return item['name'] === this.data.name;
              })[0];
          }

          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.loading = false;
      },
      error => {
          this.loading = false;
          this.toastr.error(_lang('Roles') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
          });
      });
    }

    getContentDialogHeight(): number {
      return (this.modal.nativeElement.offsetHeight ) -
                (this.header.nativeElement.offsetHeight +
                 this.footer.nativeElement.offsetHeight);
    }

    getScrollHeight(): number {
      return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
    }

  closeDialog() {
      this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
      this.selected = record;
      this.closeDialog();
  }

}
