import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupsFinderModalComponent } from './groups-finder-modal.component';

describe('GroupsFinderModalComponent', () => {
  let component: GroupsFinderModalComponent;
  let fixture: ComponentFixture<GroupsFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupsFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
