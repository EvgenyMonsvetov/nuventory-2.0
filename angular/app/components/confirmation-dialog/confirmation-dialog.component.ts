import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
  preserveWhitespaces: true
})
export class ConfirmationDialogComponent implements OnInit {

  @Input() display: boolean;
  @Output() displayChange = new EventEmitter();
  @Output() confirmChange = new EventEmitter();

  @Input() header = '';
  @Input() message = '';

  constructor() { }

  ngOnInit() {
  }

  onConfirm() {
    this.display = false;
    this.confirmChange.emit(true);
  }

  onClose() {
    this.display = false;
    this.displayChange.emit(false);
  }

  ngOnDestroy() {
    this.displayChange.unsubscribe();
    this.confirmChange.unsubscribe();
  }
}
