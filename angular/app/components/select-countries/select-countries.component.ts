import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import { CountryService } from '../../services/country.service';
import { LangPipe } from '../../views/pipes/lang.pipe';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { Page } from '../../model/page'
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-select-countries',
  templateUrl: './select-countries.component.html',
  styleUrls: ['./select-countries.component.scss'],
  preserveWhitespaces: true
})
export class SelectCountriesComponent implements OnInit {
  cols: any[] = [];
  items: any[] = [];
  selectedCountry: any;
  search: string;
  selectedField: any;

  private loading = false;
  private page = new Page();

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('countryModal') countryModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('searchbar')    searchbar:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<SelectCountriesComponent>,
              private countryService: CountryService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.loading = true;

    this.cols = [
      { field: 'CY1_SHORT_CODE',  header: _lang('CY1_SHORT_CODE') },
      { field: 'CY1_NAME',        header: _lang('CY1_NAME') },
      { field: 'CY1_CREATED_ON',  header: _lang('#CreatedOn#') },
      { field: 'CREATED_BY',      header: _lang('#CreatedBy#') },
      { field: 'CY1_MODIFIED_ON', header: _lang('#ModifiedOn#') },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#') }
    ];

    this.page.pageNumber = 0;
    this.getCountries();
  }

  getContentDialogHeight(): number {
    return (this.countryModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + this.searchbar.nativeElement.offsetHeight + 2);
  }

  getCountries() {
    this.selectedCountry = null;
    this.countryService.getCountries(this.page).subscribe( countries => {
      this.items = countries['data'];
      if(this.data.CY1_ID){
          this.selectedCountry = countries['data'].filter( item => {
              return item['CY1_ID'] === this.data.CY1_ID;
          })[0];
      }

      this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';
      this.loading = false;
    },
      error => {
        this.loading = false;
        this.toastr.error(_lang('#Countries#') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
          timeOut: 3000,
      });
    });
  }

  onFieldChange(value) {
    this.selectedField = value;
  }

  onSearch() {
    this.page.sortParams = {};
    if (this.search)
      this.page.filterParams[this.selectedField] = this.search;
    else
      this.page.filterParams = {};

    this.getCountries();
  }

  onSelectCountry() {
    this.dialogRef.close( this.selectedCountry );
  }

  openLanguageEditor() {
    this.closeDialog()
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Country List'),
        componentRef: this,
        data: [],
        componentName: 'SelectCountriesComponent'
      },
    });
  }

  selectRecord(record) {
    this.selectedCountry = record;
    this.closeDialog();
  }

  closeDialog() {
    this.dialogRef.close( this.selectedCountry );
  }

}
