import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { permissions } from '../../permissions';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-link-catalog-parts-modal',
  templateUrl: './link-catalog-parts-modal.component.html',
  styleUrls: ['./link-catalog-parts-modal.component.scss'],
  providers: [
    CategoryService,
  ],
  preserveWhitespaces: true
})
export class LinkCatalogPartsModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[];
  items: any[];
  private loading = false;
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('parentContainer') parentContainer: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('table')         table:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('footer')        footer:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<LinkCatalogPartsModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.cols = [
      { field: 'VD0_NAME',        header: _lang('Vendor'),       visible: true, width: 150 },
      { field: 'MD0_PART_NUMBER', header: _lang('Part Number'),  visible: true, width: 100 },
      { field: 'MD0_DESC1',       header: _lang('Description'),  visible: true, width: 300 },
    ];

    this.setPage();
  }

  setPage() {
    this.items = this.data.items;
    var me = this;
    setTimeout( ()=> {
      me.scrollHeight        = me.getScrollHeight() + 'px';
      me.dialogContentHeight = me.getContentDialogHeight() + 'px';
    }, 100);
  }

  getContentDialogHeight(): number {
    return (this.parentContainer.nativeElement.offsetHeight ) -
           (this.header.nativeElement.offsetHeight +
            this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
  }

  closeDialog() {
    this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
    this.selected = record;
    this.closeDialog();
  }
}

