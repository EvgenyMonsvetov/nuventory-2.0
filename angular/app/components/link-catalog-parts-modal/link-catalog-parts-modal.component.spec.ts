import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkCatalogPartsModalComponent } from './link-catalog-parts-modal.component';

describe('LinkCatalogPartsModalComponent', () => {
  let component: LinkCatalogPartsModalComponent;
  let fixture: ComponentFixture<LinkCatalogPartsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkCatalogPartsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkCatalogPartsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
