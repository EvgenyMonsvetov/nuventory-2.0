import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Constants } from '../../constants';

@Component({
  selector: 'app-bin-dropdown',
  templateUrl: './bin-dropdown.component.html'
})
export class BinDropdownComponent {
  private binDP: Array<any> = Constants.binDP;
  private disabledField: boolean = false;

  @Input()  binID: number;
  @Output() binIDChange = new EventEmitter();
  @Input()  materialDP: boolean = true;

  selectionChanged(value) {
    this.binIDChange.emit(value);
  }

  @Input()
  get disabled() {
    return this.disabledField;
  }

  set disabled(val) {
    this.disabledField = val;
  }

  constructor() { }

}
