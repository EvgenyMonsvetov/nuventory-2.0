import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { BinDropdownComponent } from './bin-dropdown.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ BinDropdownComponent ],
    exports: [ BinDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class BinDropdownModule { }
