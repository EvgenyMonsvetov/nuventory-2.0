import { OnInit, ViewChild, ElementRef, OnDestroy, Inject } from '@angular/core';
import { Component } from '@angular/core';
import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import { _lang } from '../../pipes/lang';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../model/page';
import { Subscription } from 'rxjs';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { RawSalesDataStoreMonthService } from '../../services/raw-sales-data-store-month.service';

@Component({
  selector: 'app-summary-chart-modal',
  templateUrl: './summary-chart-modal.component.html',
  styleUrls: ['./summary-chart-modal.component.scss'],
  providers: [
    RawSalesDataStoreMonthService
  ],
  preserveWhitespaces: true
})
export class SummaryChartModalComponent implements OnInit, OnDestroy {
  private permissions = permissions;

  @ViewChild('summaryChartModal')   summaryChartModal: ElementRef;
  @ViewChild('header')              header:  ElementRef;
  @ViewChild('actionbar')           actionbar: ElementRef;
  @ViewChild('footer')              footer: ElementRef;
  @ViewChild('summary', { read: ElementRef }) container: ElementRef;
  private chart;

  private page = new Page();
  private sub: Subscription;
  private dialogContentHeight: string = '50px';

  constructor(public dialogRef: MatDialogRef<SummaryChartModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private rawSalesDataStoreMonthService: RawSalesDataStoreMonthService,
              public  dialog: MatDialog,
              private toastr: ToastrService) {
    this.page.filterParams = this.data.filterParams;

    this.sub = this.rawSalesDataStoreMonthService.getSummaryChartReport(this.page).subscribe(res => {
          this.chart.hideLoading();
          this.buildHighcharts(res);
        },
        error => {
          this.toastr.error(_lang('Summary chart') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  ngOnInit() {
    setTimeout(() => {this.dialogContentHeight = this.getContentDialogHeight() + 'px';
      this.buildHighcharts([]);}, 100);
  }

  buildHighcharts(data) {
    this.chart = Highcharts.chart(this.container.nativeElement, {
      chart: {
        type: 'column',
        height: this.dialogContentHeight
      },
      title: {
        text: ''
      },
      legend: {
      },
      xAxis: {
        categories: data.categories,
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: data.attributeType
        },
        plotLines: [{
          value: data.target,
          color: 'lightgrey',
          width: 1,
          zIndex: 4,
        }],
        gridLineColor: 'transparent'
      },
      tooltip: {
        shared: true,
        valueSuffix: this.getValueSuffix(data.attributeTypeSymbol),
        valuePrefix: this.getValuePrefix(data.attributeTypeSymbol)
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        column: {
          pointPadding: 0.05,
          borderWidth: 0,
          dataLabels: {
            format: this.getChartPointFormat(data.attributeTypeSymbol)
          }
        }
      },
      series: data.series
    });
  }

  getValueSuffix(attributeTypeSymbol) {
    if (attributeTypeSymbol === '%') {
      return '%';
    }
    return '';
  }

  getValuePrefix(attributeTypeSymbol) {
    if (attributeTypeSymbol === '$') {
      return '$';
    }
    return '';
  }

  getChartPointFormat(attributeTypeSymbol) {
    if (attributeTypeSymbol === '$') {
      return '${point.y:,.2f}';
    } else if (attributeTypeSymbol === '%') {
      return '{point.y:,.2f}%';
    } else {
      return '{point.y:,.2f}';
    }
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  getContentDialogHeight(): number {
    return this.summaryChartModal.nativeElement.offsetHeight - (this.header.nativeElement.offsetHeight +
        this.actionbar.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight + 2) + 20;
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Drill Down Chart'),
        componentRef: this,
        data: [],
        componentName: 'SummaryChartModalComponent'
      },
    });
  }
}
