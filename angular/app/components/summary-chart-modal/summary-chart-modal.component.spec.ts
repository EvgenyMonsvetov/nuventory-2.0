import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryChartModalComponent } from './summary-chart-modal.component';

describe('SummaryChartModalComponent', () => {
  let component: SummaryChartModalComponent;
  let fixture: ComponentFixture<SummaryChartModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryChartModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryChartModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
