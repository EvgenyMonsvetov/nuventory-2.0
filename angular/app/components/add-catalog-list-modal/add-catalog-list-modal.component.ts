import { Component, ElementRef, Inject, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { MasterDataService } from '../../services/master.data.service';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { permissions } from '../../permissions';
import * as _ from 'lodash';

@Component({
  selector: 'app-add-catalog-list-modal',
  templateUrl: './add-catalog-list-modal.component.html',
  styleUrls: ['./add-catalog-list-modal.component.scss'],
  providers: [
    MasterDataService
  ],
  preserveWhitespaces: true
})

export class AddCatalogListModalComponent implements OnInit {
  private permissions = permissions;

  @ViewChild('catalogListModal') catalogListModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('actionbar')     actionbar:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;
  @ViewChild('ordersList')    ordersList:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('ordersTable')   ordersTable:  ElementRef;
  @ViewChild('tableFooter')   tableFooter:  ElementRef;

  private scrollHeight: string = '0px';
  private listHeight: string = '0px';
  private tableHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  private title: string = null;
  private page = new Page();
  private size: number = 100;
  private totalRecords: number = 0;
  private searchValue: string = '';
  private isLoadingItems: boolean = false;
  private isLoading: boolean = true;

  private items: any[] = [];
  private filteredItems: any[] = [];
  private filteredItemsPaginated: any[] = [];
  private recordsPerPage: number = 100;
  private orderedItems: any[] = [];

  private totalCart: number = 0;
  private selected: any;
  private selectedItems: any[] = [];
  private lastSelectedIndex: number = -1;

  @ViewChild('templateLeft') templateLeft: TemplateRef<any>;

  private cols = [
    { field: 'BTN_DELETE',      visible: true, width: 32},
    { field: 'FI1_FILE_PATH',   visible: true, width: 32},
    { field: 'MD1_PART_NUMBER', visible: true},
    { field: 'MD1_UNIT_PRICE',  visible: true, width: 60},
    { field: 'MD1_ORDER_QTY',   visible: true, width: 61}
  ];

  private fields = {  id: 'MD1_ID',
                      description: 'MD1_DESC1',
                      partNumber: 'MD1_PART_NUMBER',
                      unitPrice: 'MD1_UNIT_PRICE',
                      onHandQty: 'MD2_ON_HAND_QTY',
                      orderQty: 'MD1_ORDER_QTY'
                    };
  private addOrderButtonLabel = _lang('Add Order(s)');
  private isVendorCatalog: boolean = false;

  constructor(public dialogRef: MatDialogRef<AddCatalogListModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private masterDataService: MasterDataService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = this.data.filters;
    if (this.data.url) {
      this.page.url = this.data.url;
    } else {
      this.page.url = '/master-data/full-parts';
    }
    if (!_.isUndefined(this.data.cols)) {
      this.cols = this.data.cols;
    }
    if (!_.isUndefined(this.data.fields)) {
      this.fields = this.data.fields;
    }
    if (!_.isUndefined(this.data.isVendorCatalog)) {
      this.isVendorCatalog = this.data.isVendorCatalog;
     if (this.isVendorCatalog) {
       this.addOrderButtonLabel = _lang('Create Parts')
     }
    }
  }

  ngOnInit() {
    setTimeout(() => {
      this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';
      this.listHeight = this.getListHeight() + 'px';
      this.tableHeight = this.getContentTableHeight() + 'px';
      this.getItems();
    }, 1000);
  }

  getItems() {
    this.masterDataService.getAll(this.page).subscribe(res => {
          this.items = res['data'];
          this.filteredItems = res['data'];
          this.totalRecords = res['count'];

          setTimeout(() => {
            this.scrollHeight = this.getScrollHeight() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';
            this.listHeight = this.getListHeight() + 'px';
            this.tableHeight = this.getContentTableHeight() + 'px';
            this.isLoadingItems = false;
            this.isLoading = false;
          }, 1000);
        },
        error => {
          this.isLoadingItems = false;
          this.isLoading = false;
          this.toastr.error(_lang('Parts') + ' ' + _lang('loading error occured'),  _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  getContentDialogHeight(): number {
    return (this.catalogListModal.nativeElement.parentElement.parentElement.offsetHeight) -
        (this.header.nativeElement.offsetHeight + this.actionbar.nativeElement.offsetHeight + 50);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - 2;
  }

  getContentTableHeight(): number {
    return this.getContentDialogHeight() - 37*2;
  }

  getListHeight(): number {
    return this.getContentDialogHeight() - this.paginator.nativeElement.offsetHeight - 2;
  }

  closeDialog() {
    this.dialogRef.close(false);
  }

  //add all selected items from Customer Data list on click 'Add to Cart
  addSelectedItemsToCartClick() {
    if (this.selectedItems) {
      this.selectedItems.forEach(record => {
        record.selected = false;
        let filtered = this.orderedItems.filter( item => {
          return item[this.fields.id] === record[this.fields.id];
        });
        if (!filtered || filtered.length === 0) {
          record[this.fields.orderQty] = 1;
          this.orderedItems.push(record);
        } else {
          filtered[0][this.fields.orderQty] ++;
        }
      });
      if (!this.isVendorCatalog) {
        this.updateTotals();
      }
      this.selectedItems = [];
    }
  }

  //on click add icon from Customer Data list item
  addClickItem(record) {
    let filtered = this.orderedItems.filter( item => {
      return item[this.fields.id] === record[this.fields.id];
    });
    if (!filtered || filtered.length === 0) {
      record[this.fields.orderQty] = 1;
      this.orderedItems.push(record);
    } else {
      filtered[0][this.fields.orderQty] ++;
    }
    this.itemSelect(record, null);
    if (!this.isVendorCatalog) {
      this.updateTotals();
    }
  }

  //remove item from cart list
  removeClick(record) {
    this.orderedItems = this.orderedItems.filter( item => {
      return item[this.fields.id] !== record[this.fields.id];
    });
    this.updateTotals();
  }

  //on press tab button in count fields and navigate to next input
  buttonPress(e, rowIndex) {
    if ( (e.keyCode === 9 || e.keyCode === 13) && rowIndex !== this.orderedItems.length-1) {
      e.stopPropagation();
      const i = rowIndex + 1;
      const row = 'row' + i;
      const elem = document.getElementById(row);
      if (elem) {
        elem.focus();
      }
    }
    this.updateTotals();
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data Form'),
        componentRef: this,
        data: [],
        componentName: 'AddCatalogListModalComponent'
      }
    });
  }

  updateTotals() {
    this.totalCart = 0;
    this.orderedItems.forEach(record => {
      const unitPrice: number = Number(record[this.fields.unitPrice].toString().replace(/[^.0-9]/g, ''));
      const orderQty: number = Number(record[this.fields.orderQty].toString().replace(/[^.0-9]/g, ''));

      this.totalCart += unitPrice * orderQty;
    });
  }

  //change selected/unselected item style in Customer Data list
  itemSelect(record, event) {
    // shift key pressed + click
    if (event && event.shiftKey && this.lastSelectedIndex > -1) {
      const selectedIndex = this.filteredItemsPaginated.indexOf(record);

      if (selectedIndex > this.lastSelectedIndex) {
        for (let i = this.lastSelectedIndex; i <= selectedIndex; i++) {
          this.filteredItemsPaginated[i].selected = true;
          this.selectedItems.push(this.filteredItemsPaginated[i]);
        }
      } else if (selectedIndex < this.lastSelectedIndex) {
        for (let i = selectedIndex; i <= this.lastSelectedIndex; i++) {
          this.filteredItemsPaginated[i].selected = true;
          this.selectedItems.push(this.filteredItemsPaginated[i]);
        }
      }
    } // control/command key pressed + click
    else if (event && (event.metaKey || event.ctrlKey)) {
      if (record.selected) {
        this.selectedItems = this.selectedItems.filter( item => {
          return item[this.fields.id] !== record[this.fields.id];
        });
      } else {
        this.selectedItems.push(record);
      }
      record.selected = !record.selected;
    } // default selection on click
    else {
      this.selectedItems.forEach(item => {
        item.selected = false;
      });
      this.selectedItems = [];
      record.selected = true;
      this.selectedItems.push(record);
    }

    this.lastSelectedIndex = this.filteredItemsPaginated.indexOf(record);
  }

  //add items from cart to parent view
  addOrder() {
    if (this.isVendorCatalog) {
      this.isLoading = true;
      const MD0_IDs = this.orderedItems.map(({ MD0_ID }) => MD0_ID);
      this.masterDataService.createMultiple(MD0_IDs.join()).subscribe( result => {
        if (result.success) {
          this.isLoading = false;
          this.toastr.success('Parts created successfully.', 'Success', {
            timeOut: 3000,
            tapToDismiss: true
          });
          this.dialogRef.close(false);
        } else {
          this.isLoading = false;
          this.toastr.error('Unable to save Part info in database.' + result.errors, 'Error', {
            timeOut: 5000,
            tapToDismiss: true
          });
        }

      });
    } else {
      this.dialogRef.close(this.orderedItems);
    }
  }

  searchKeyUp(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }

  search() {
    this.isLoadingItems = true;
    this.lastSelectedIndex = -1;
    this.selectedItems = [];
    this.page.filterParams[this.fields.partNumber] = this.searchValue.toLowerCase();
    this.page.filterParams[this.fields.description] = this.searchValue.toLowerCase();
    this.page.filterParams['search'] = true;
    this.isLoadingItems = true;
    this.getItems();
  }

  paginate(event) {
    let page = 0;
    if (event) {
      this.recordsPerPage = event.rows;
      page = event.page;
    }

    this.page.pageNumber = page;
    this.page.size = this.recordsPerPage;
    this.isLoadingItems = true;
    this.getItems();
  }

}
