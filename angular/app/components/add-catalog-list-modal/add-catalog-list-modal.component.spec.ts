import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCatalogListModalComponent } from './add-catalog-list-modal.component';

describe('AddCatalogListModalComponent', () => {
  let component: AddCatalogListModalComponent;
  let fixture: ComponentFixture<AddCatalogListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCatalogListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCatalogListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
