import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { CompanyDropdownComponent } from './company-dropdown.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ CompanyDropdownComponent ],
    exports: [ CompanyDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class CompanyDropdownModule { }
