import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'app-company-dropdown',
  templateUrl: './company-dropdown.component.html'
})

export class CompanyDropdownComponent implements OnInit, OnDestroy {
  /** control for the selected filters */
  public companyCtrl: FormControl = new FormControl();
  public companyFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyCompany = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredCompanies: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  private companiesDP: Array<any> = [];
  private disabledField:boolean = false;

  @Output() fieldChange = new EventEmitter();
  @Input() field: any;

  selectionChanged(value) {
    this.fieldChange.emit(value);
  }

  @Input()
  get dataProvider() {
    return this.companiesDP;
  }

  set dataProvider(val) {
    this.companiesDP = val;
    this.filterCompanies();
  }

  @Input()
  get disabled() {
    return this.disabledField;
  }

  set disabled(val) {
    this.disabledField = val;
  }

  constructor() { }

  ngOnInit() {
    this.companyFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCompany))
        .subscribe(() => {
          this.filterCompanies();
        });
  }

  private filterCompanies() {
    if (!this.companiesDP) {
      return;
    }
    // get the search keyword
    let search = this.companyFilterCtrl.value;
    if (!search) {
      this.filteredCompanies = this.companiesDP;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the companies
    this.filteredCompanies = this.companiesDP.filter(company => company['CO1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  ngOnDestroy() {
    this._onDestroyCompany.next();
    this._onDestroyCompany.complete();
  }

}
