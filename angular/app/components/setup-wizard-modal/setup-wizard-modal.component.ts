import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { _lang } from '../../pipes/lang';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { CompanyService } from '../../services/company.service';
import { ShopService } from '../../services/shop.service';
import { UserService } from '../../services/user.service';
import { ConfirmationService } from 'primeng/api';
import { GroupsService } from '../../services/groups';
import { MatStepper } from '@angular/material/stepper';
import { Page } from '../../model/page';
import { permissions } from '../../permissions';

@Component({
  selector: 'app-setup-wizard-modal',
  templateUrl: './setup-wizard-modal.component.html',
  styleUrls: ['./setup-wizard-modal.component.scss'],
  providers: [
    CompanyService,
    ShopService,
    UserService,
    GroupsService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class SetupWizardModalComponent implements OnInit {
  private permissions = permissions;

  @ViewChild('setupWizardModal') setupWizardModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('stepper')       stepper:  MatStepper;
  @ViewChild('footer')        footer:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('actionbarCompany')   actionbarCompany:  ElementRef;
  @ViewChild('actionbarShop')   actionbarShop:  ElementRef;
  @ViewChild('actionbarUser')   actionbarUser:  ElementRef;

  private dialogContentHeight: string = '0px';
  private contentHeight: string = '0px';
  private scrollHeightCompany: string = '50px';
  private scrollHeightShop: string = '50px';
  private scrollHeightUser: string = '50px';
  private scrollHeightUploadList: string = '50px';

  private loading = false;

  private colsCompany: any[] = [];
  private colsShop: any[] = [];
  private colsUser: any[] = [];
  private companiesDP: any[] = [];
  private companyLocationDP: any[] = [];
  private shopsDP: any[] = [];
  private usersDP: any[] = [];
  private groupDP: any[] = [];
  private newCompaniesDP: any[] = [];
  private newShopsDP: any[] = [];
  private newUsersDP: any[] = [];

  private selectedCompany: any;
  private selectedShop: any;
  private selectedUser: any;

  private CO1_SHORT_CODE: number = 0;
  private LO1_SHORT_CODE: number = 0;
  private USER_TEMPORARY_ID: number = 0;

  private title = _lang('Setup Wizard');

  private companiesLoaded: boolean = false;
  private locationsLoaded: boolean = false;
  private companyLocationsLoaded: boolean = false;
  private groupsLoaded: boolean = false;
  private usersLoaded: boolean = false;
  private isEditable: boolean = true;
  private isCompanyAdded: boolean = false;
  private isShopAdded: boolean = false;
  private isUserAdded: boolean = false;

  constructor(public dialogRef: MatDialogRef<SetupWizardModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private toastr: ToastrService,
              public dialog: MatDialog,
              private confirmationService: ConfirmationService,
              private companyService: CompanyService,
              private shopService: ShopService,
              private userService: UserService,
              private groupsService: GroupsService) {

  }

  ngOnInit() {
    this.colsCompany = [
      { field: 'CO1_SHORT_CODE',    header: _lang('Short Code'),          visible: true, width: '8%',   minWidth: 60 },
      { field: 'CO1_NAME',          header: _lang('CO1_NAME'),            visible: true, width: '16%',  minWidth: 120 },
      { field: 'CO1_ADDRESS1',      header: _lang('#AddressStreet1#'),    visible: true, width: '16%',  minWidth: 120 },
      { field: 'CO1_CITY',          header: _lang('#AddressCity#'),       visible: true, width: '16%',  minWidth: 120},
      { field: 'CO1_STATE',         header: _lang('#AddressState#'),      visible: true, width: '11%',  minWidth: 80},
      { field: 'CO1_ZIP',           header: _lang('#AddressPostalCode#'), visible: true, width: '8%',   minWidth: 60},
      { field: 'CO1_PHONE',         header: _lang('#Phone#'),             visible: true, width: '11%',  minWidth: 80},
      { field: 'CO1_MITCHELL_FLAG', header: _lang('Mitchell'),            visible: true, width: '7%',   minWidth: 50, align: 'center'},
      { field: 'CO1_CCC_FLAG',      header: _lang('CCC'),                 visible: true, width: '7%',   minWidth: 50, align: 'center'}
    ];

    this.colsShop = [
      { field: 'CO1_ID',               header: _lang('CO1_NAME'),             visible: true, width: '7.3%',  minWidth: 100 },
      { field: 'LO1_SHORT_CODE',       header: _lang('LO1_SHORT_CODE'),       visible: true, width: '5.8%',  minWidth: 80 },
      { field: 'LO1_MITCHELL_SHOP_ID', header: _lang('LO1_MITCHELL_SHOP_ID'), visible: true, width: '6.2%',  minWidth: 85 },
      { field: 'LO1_CCC_SHOP_ID',      header: _lang('LO1_CCC_SHOP_ID'),      visible: true, width: '7.3%',  minWidth: 100 },
      { field: 'LO1_NAME',             header: _lang('LO1_NAME'),             visible: true, width: '11.5%', minWidth: 160 },
      { field: 'LO1_RACKS',            header: _lang('LO1_RACKS'),            visible: true, width: '11.5%', minWidth: 160 },
      { field: 'LO1_CENTER_FLAG',      header: _lang('LO1_CENTER_FLAG'),      visible: true, width: '5.8%',  minWidth: 80, align: 'center'},
      { field: 'LO1_ADDRESS1',         header: _lang('#AddressStreet1#'),     visible: true, width: '10.8%', minWidth: 150 },
      { field: 'LO1_CITY',             header: _lang('#AddressCity#'),        visible: true, width: '8.7%',  minWidth: 120 },
      { field: 'LO1_STATE',            header: _lang('#AddressState#'),       visible: true, width: '3.3%',  minWidth: 45 },
      { field: 'LO1_ZIP',              header: _lang('#AddressPostalCode#'),  visible: true, width: '5.8%',  minWidth: 80 },
      { field: 'LO1_PHONE',            header: _lang('#Phone#'),              visible: true, width: '8%',    minWidth: 110 },
      { field: 'LO1_FAX',              header: _lang('#Fax#'),                visible: true, width: '8%',    minWidth: 110 }
    ];

    this.colsUser = [
      { field: 'US1_LOGIN', header: _lang('Login Name'),        visible: true, width: '21%',  minWidth: 120 },
      { field: 'US1_NAME',  header: _lang('Full Name'),         visible: true, width: '21%',  minWidth: 120 },
      { field: 'ID',        header: _lang('Affiliation'),       visible: true, width: '29%',  minWidth: 160 },
      { field: 'GR1_ID',    header: _lang('GR1_ACCESS_LEVEL'),  visible: true, width: '29%',  minWidth: 160 }
    ];

    setTimeout(() => {this.dialogContentHeight = this.getContentDialogHeight() + 'px';
      this.contentHeight = this.getContentHeight() + 'px';}, 100);
  }

  getCompany() {
    if( !this.companiesLoaded ) {
      this.companyService.getCompanies(new Page()).subscribe(res => {
            this.companyHandler(res.data)
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('Companies') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    } else {
      this.loading = false;
    }
  }

  companyHandler(data: any[]) {
    this.companiesDP = [];
    let me = this;
    data.forEach( record => {
      let newItem = {};
      newItem['CO1_ID'] = record['CO1_ID'];
      newItem['CO1_SHORT_CODE'] = record['CO1_SHORT_CODE'];
      newItem['CO1_NAME'] = record['CO1_NAME'];
      me.companiesDP.push(newItem);
    });
    this.loading = false;
    this.companiesLoaded = true;
  }

  getLocation() {
    if (!this.locationsLoaded) {
      this.shopService.getAll().subscribe(res => {
            this.locationHandler(res.data)
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('Locations') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    } else {
      this.loading = false;
    }
  }

  locationHandler(data: any[]) {
    this.shopsDP = [];
    let me = this;

    data.forEach( record => {
      let newItem = {};
      newItem['CO1_ID'] = record['CO1_ID'];
      newItem['LO1_ID'] = record['LO1_ID'];
      newItem['LO1_SHORT_CODE'] = record['LO1_SHORT_CODE'];
      newItem['LO1_NAME'] = record['LO1_NAME'];
      newItem['MD1_ACTIVE'] = 0;
      me.shopsDP.push(newItem);
    });

    this.loading = false;
    this.locationsLoaded = true;

    this.getCompany();
    if (!this.isUserAdded) {
      this.addItem();
      this.isUserAdded = true;
    }
  }

  getCompanyLocation() {
    if (!this.companyLocationsLoaded) {
      this.companyService.getCompanyLocation().subscribe(res => {
            this.companyLocationHandler(res.data)
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('Locations') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    } else {
      this.getGroup();
    }
  }

  companyLocationHandler(data: any[]) {
    this.companyLocationDP = [];
    let me = this;
    data.forEach( record => {
      let newItem = {};
      newItem['LO1_ID'] = record['LO1_ID'];
      newItem['LO1_SHORT_CODE'] = record['LO1_SHORT_CODE'];
      newItem['LO1_NAME'] = record['LO1_NAME'];
      newItem['CO1_ID'] = record['CO1_ID'];
      newItem['CO1_SHORT_CODE'] = record['CO1_SHORT_CODE'];
      newItem['CO1_NAME'] = record['CO1_NAME'];

      newItem['label'] = record['CO1_NAME'] + ' - ' + record['LO1_NAME'];
      newItem['ID'] = record['CO1_ID'] + '@' + record['LO1_ID'];

      me.companyLocationDP.push(newItem);
    });
    this.companyLocationsLoaded = true;
    this.getGroup();
  }

  getGroup() {
    if (!this.groupsLoaded) {
      this.groupsService.getAll().subscribe(res => {
            this.groupHandler(res.data)
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('Locations') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    } else {
      this.getUser();
    }
  }

  groupHandler(data: any[]) {
    this.groupDP = [];
    let me = this;

    data.forEach( record => {
      let newItem = {};
      newItem['GR1_ID'] = record['GR1_ID'];
      newItem['GR1_NAME'] = record['GR1_NAME'];
      me.groupDP.push(newItem);
    });

    this.groupsLoaded = true;
    this.getUser();
  }

  getUser() {
    if (!this.usersLoaded) {
      this.userService.getUsers(new Page()).subscribe(res => {
            this.userHandler(res.data)
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('Users') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    } else {
      this.getLocation();
    }
  }

  userHandler(data: any[]) {
    this.usersDP = [];;
    let me = this;

    data.forEach( record => {
      let newItem = {};
      newItem['CO1_ID'] = record['CO1_ID'];
      newItem['US1_ID'] = record['US1_ID'];
      newItem['US1_NAME'] = record['US1_NAME'];
      newItem['US1_LOGIN'] = record['US1_LOGIN'];
      me.usersDP.push(newItem);
    });

    this.usersLoaded = true;
    this.getLocation();
  }

  addItem() {
    switch (this.stepper.selectedIndex) {
      case 1:
        this.addCompany();
        break;
      case 2:
        this.addShop();
        break;
      case 3:
        this.addUser();
        break;
    }
  }

  addCompany() {
    this.searchID(this.newCompaniesDP, 'CO1_SHORT_CODE');
    this.newCompaniesDP.push({CO1_SHORT_CODE: this.CO1_SHORT_CODE.toString(), CO1_NAME: 'New Company'});
    this.selectedCompany = null;
  }

  addShop() {
    this.searchID(this.newShopsDP, 'LO1_SHORT_CODE');
    this.newShopsDP.push({LO1_SHORT_CODE: this.LO1_SHORT_CODE.toString(), LO1_NAME: 'New Shop', CO1_ID: null});
    this.selectedShop = null;
  }

  addUser() {
    this.USER_TEMPORARY_ID ++;
    this.newUsersDP.push({US1_LOGIN: 'newuser', US1_NAME: 'New User', ID: null, GR1_ID: null, USER_TEMPORARY_ID: this.USER_TEMPORARY_ID, DELETED: 0, password: 'default', repeat_password: 'default' });
    this.selectedUser = null;
  }

  deleteCompany() {
    this.newCompaniesDP = this.newCompaniesDP.filter(c => c !== this.selectedCompany);
    this.selectedCompany = null;
  }

  deleteShop() {
    this.newShopsDP = this.newShopsDP.filter(s => s !== this.selectedShop);
    this.selectedShop = null;
  }

  deleteUser() {
    this.newUsersDP = this.newUsersDP.filter(u => u !== this.selectedUser);
    this.selectedUser = null;
  }

  searchID(dataProvider: any[], fieldName: string) {
    if (dataProvider && fieldName.length > 0) {
        dataProvider.forEach( item => {
          if (! Number.isNaN(parseInt(item[fieldName])) )
            this[fieldName] = Math.max(this[fieldName], parseInt(item[fieldName]) + 1);
        });
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }

  getContentDialogHeight(): number {
    return (this.setupWizardModal.nativeElement.parentElement.parentElement.offsetHeight) -
        (this.header.nativeElement.offsetHeight + this.footer.nativeElement.offsetHeight + 52);
  }

  getContentHeight(): number {
    return this.getContentDialogHeight() - 70;
  }

  getScrollHeightCompany(): number {
    return this.getContentHeight() - this.tableHeader.nativeElement.offsetHeight - this.actionbarCompany.nativeElement.offsetHeight;
  }

  getScrollHeightShop(): number {
    return this.getContentHeight() - this.tableHeader.nativeElement.offsetHeight - this.actionbarShop.nativeElement.offsetHeight - 24;
  }

  getScrollHeightUser(): number {
    return this.getContentHeight() - this.tableHeader.nativeElement.offsetHeight - this.actionbarUser.nativeElement.offsetHeight;
  }

  getScrollHeightUploadList(): number {
    return this.getContentHeight();
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Setup Wizard'),
        componentRef: this,
        data: [],
        componentName: 'SetupWizardModalComponent'
      },
    });
  }

  onBackClick(event = null) {
    if (!event) {
      this.stepper.previous();
    }

    let index = event ? event.selectedIndex : this.stepper.selectedIndex;

    switch (index) {
      case 0:
        this.isCompanyAdded = true;
        break;
      case 1:
        this.isShopAdded = true;
        break;
      case 2:
        this.isUserAdded = true;
        break;
      case 4:
        break;
    }
  }

  onSkipClick(stepper: MatStepper) {
    this.stepper.next();
  }

  onNextClick(event = null) {
    switch (this.stepper.selectedIndex) {
      case 0:
        this.stepper.next();
        break;
      case 1:
        if (this.validateCompanies()) {
          this.saveCompanies();
        }
        break;
      case 2:
        if (this.validateLocations()) {
          this.saveLocations();
        }
        break;
      case 3:
        if (this.validateUsers()) {
          this.saveUsers();
        }
        break;
      case 4:
        this.stepper.next();
        break;
    }
  }

  stepperSelectionChange(event) {
    this.nextStepHandler(event);
  }

  nextStepHandler(event = null) {
    if (event && event.selectedIndex < event.previouslySelectedIndex) {
        this.onBackClick(event);
        return;
    }

    switch (event.selectedIndex) {
      case 1:
        this.loading = true;
        this.getCompanyLastShortCode();
        setTimeout(() => {this.scrollHeightCompany = this.getScrollHeightCompany() + 'px';}, 100);
        break;
      case 2:
        this.loading = true;
        this.getLocationLastShortCode();
        this.companiesDP = this.newCompaniesDP.concat(this.companiesDP);
        setTimeout(() => {this.scrollHeightShop = this.getScrollHeightShop() + 'px';}, 100);
        break;
      case 3:
        this.loading = true;
        this.getCompanyLocation();
        setTimeout(() => {this.scrollHeightUser = this.getScrollHeightUser() + 'px';}, 100);
        break;
      case 4:
        setTimeout(() => {this.scrollHeightUploadList = this.getScrollHeightUploadList() + 'px';}, 100);
        break;
    }
  }

  validateCompanies() {
    let valid = true;

    this.newCompaniesDP.forEach( item => {
      if (!valid) {
        return;
      }

      if (item['CO1_NAME'].toString().length === 0) {
        this.displayErrorDialog(_lang('Company name required for all records. Please enter a company name') + ' ' + _lang('and try again.'));
        valid = false;
        return;
      }
      if (item['CO1_SHORT_CODE'].toString().length === 0) {
        this.displayErrorDialog(_lang('Company short code required for all records. Please enter a company short code') + ' ' + _lang('and try again.'));
        valid = false;
        return;
      }

      this.newCompaniesDP.forEach( item2 => {
        if (item !== item2 && item['CO1_NAME'] === item2['CO1_NAME']) {
          this.displayErrorDialog(_lang('Duplicate company name') + ' "' + item['CO1_NAME'] + '". ' + _lang('Please change company name') + ' ' + _lang('and try again.'));
          valid = false;
          return;
        }
        if (item !== item2 && item['CO1_SHORT_CODE'] === item2['CO1_SHORT_CODE']) {
          this.displayErrorDialog(_lang('Duplicate company code') + ' "' + item['CO1_SHORT_CODE'] + '". ' + _lang('Please change company code') + ' ' + _lang('and try again.'));
          valid = false;
          return;
        }
      });
    });

    return valid;
  }

  validateLocations() {
    let valid = true;

    this.newShopsDP.forEach( item => {
      if (!valid) {
        return;
      }

      if (item['LO1_NAME'].toString().length === 0) {
        this.displayErrorDialog(_lang('Shop name required for all records. Please enter a shop name') + ' ' + _lang('and try again.'));
        valid = false;
        return;
      }
      if (item['LO1_SHORT_CODE'].toString().length === 0) {
        this.displayErrorDialog(_lang('Shop short code required for all records. Please enter a shop short code') + ' ' + _lang('and try again.'));
        valid = false;
        return;
      }
      if (!item['CO1_ID'] || item['CO1_ID'].toString().length === 0) {
        this.displayErrorDialog(_lang('Company field required for all records. Please select a company for') + ' "' + item['LO1_NAME'] + '" ' + _lang('and try again.'));
        valid = false;
        return;
      }

      this.newShopsDP.forEach( item2 => {
        if (item !== item2 && item['LO1_NAME'] === item2['LO1_NAME'] && item['CO1_ID'] === item2['CO1_ID']) {
          this.displayErrorDialog(_lang('Duplicate shop name') + ' "' + item['LO1_NAME'] + '". ' + _lang('Please change shop name') + ' ' + _lang('and try again.'));
          valid = false;
          return;
        }
        if (item !== item2 && item['LO1_SHORT_CODE'] === item2['LO1_SHORT_CODE']) {
          this.displayErrorDialog(_lang('Duplicate shop code') + ' "' + item['LO1_SHORT_CODE'] + '". ' + _lang('Please change shop code') + ' ' + _lang('and try again.'));
          valid = false;
          return;
        }
      });
    });

    return valid;
  }

  validateUsers() {
    let valid = true;

    this.newUsersDP.forEach( item => {
      if (!valid) {
        return;
      }

      if (item['US1_LOGIN'].toString().length < 3) {
        this.displayErrorDialog(_lang('User name with at least 3 characters required for all records. Please try again.'));
        valid = false;
        return;
      }
      if (item['US1_NAME'].toString().length === 0) {
        this.displayErrorDialog(_lang('User full name required for all records. Please enter a user name') + ' ' + _lang('and try again.'));
        valid = false;
        return;
      }
      if (!item['CO1_ID'] || !item['LO1_ID'] || item['CO1_ID'].toString().length === 0 || item['LO1_ID'].toString().length === 0) {
        this.displayErrorDialog(_lang('Affiliation required for all records. Please select an affiliation for') + ' "' + item['US1_NAME'] + '" ' + _lang('and try again.'));
        valid = false;
        return;
      }
      if (!item['GR1_ID'] || item['GR1_ID'].toString().length === 0) {
        this.displayErrorDialog(_lang('Access level required for all records. Please select an access level for') + ' "' + item['US1_NAME'] + '" ' + _lang('and try again.'));
        valid = false;
        return;
      }

      this.newUsersDP.forEach( item2 => {
        if (item !== item2 && item['US1_NAME'] === item2['US1_NAME'] && item['CO1_ID'] === item2['CO1_ID']) {
          this.displayErrorDialog(_lang('Duplicate user name') + ' "' + item['US1_NAME'] + '". ' + _lang('Please change user name') + ' ' + _lang('and try again.'));
          valid = false;
          return;
        }
        if (item !== item2 && item['US1_LOGIN'] === item2['US1_LOGIN']) {
          this.displayErrorDialog(_lang('Duplicate user login') + ' "' + item['US1_LOGIN'] + '". ' + _lang('Please change user login') + ' ' + _lang('and try again.'));
          valid = false;
          return;
        }
      });
    });

    return valid;
  }

  saveCompanies() {
    if (this.newCompaniesDP.length > 0) {
      this.loading = true;
      this.companiesLoaded = false;
      this.companyService.createCompanies(this.newCompaniesDP).subscribe(event => {
            this.saveCompanyHandler(event);
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On save companies') + ' ' + _lang('error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    } else {
      this.stepper.next();
    }
  }

  saveCompanyHandler(event) {
    if (event && event.success === true ) {
      this.stepper.next();
    } else {
      if (event.data) {
        this.newCompaniesDP = event.data.map(company => {
          if (company['errors']) {
            company['errors'] = this.parseErrors(company['errors']);
          }
          return company;
        });
      }
      this.toastr.error(_lang('On save companies') + ' ' + _lang('error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    }
    this.loading = false;
  }

  saveLocations() {
    if (this.newCompaniesDP.length > 0) {
      this.loading = true;
      this.locationsLoaded = false;
      this.companyLocationsLoaded = false;
      this.shopService.createShops(this.newShopsDP).subscribe(event => {
            this.saveLocationsHandler(event);
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On save locations') + ' ' + _lang('error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    } else {
      this.stepper.next();
    }
  }

  saveLocationsHandler(event) {
    if (event && event.success === true ) {
      this.stepper.next();
    } else {
      if (event.data) {
        this.newShopsDP = event.data.map(shop => {
          if (shop['errors']) {
            shop['errors'] = this.parseErrors(shop['errors']);
          }
          return shop;
        });
      }
      this.toastr.error(_lang('On save locations') + ' ' + _lang('error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    }
    this.loading = false;
  }

  saveUsers() {
    if (this.newUsersDP.length > 0) {
      this.loading = true;
      this.userService.createUsers(this.newUsersDP).subscribe(event => {
            this.saveUsersHandler(event);
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('On save users') + ' ' + _lang('error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
    } else {
      this.stepper.next();
    }
  }

  saveUsersHandler(event) {
    if (event && event.success === true ) {
      this.stepper.next();
    } else {
      if (event.data) {
        this.newUsersDP = event.data.map(user => {
          if (user['errors']) {
            user['errors'] = this.parseErrors(user['errors']);
          }
          return user;
        });
      }
      this.toastr.error(_lang('On save users') + ' ' + _lang('error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    }
    this.loading = false;
  }

  parseErrors(error) {
    let errors = [];
    for (const key of Object.keys(error)) {
      const value = error[key];
      errors = errors.concat(value);
    }
    return errors;
  }

  getCompanyLastShortCode() {
    this.companyService.getCompanyLastShortCode().subscribe(res => {
          this.companyLastShortCodeHandler(res['CO1_SHORT_CODE'])
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Company Short Code') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  companyLastShortCodeHandler(data: any) {
    this.CO1_SHORT_CODE = 0;
    if (! Number.isNaN(parseInt(data)) )
      this.CO1_SHORT_CODE = parseInt(data) + 1;
    if (!this.isCompanyAdded) {
      this.addItem();
      this.isCompanyAdded = true;
    }
    this.loading = false;
  }

  getLocationLastShortCode() {
    this.shopService.getLocationLastShortCode().subscribe(res => {
          this.locationLastShortCodeHandler(res['LO1_SHORT_CODE'])
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Location Short Code') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  locationLastShortCodeHandler(data: any) {
    this.LO1_SHORT_CODE = 0;
    if (! Number.isNaN(parseInt(data)) )
      this.LO1_SHORT_CODE = parseInt(data) + 1;
    if (!this.isShopAdded) {
      this.addItem();
      this.isShopAdded = true;
    }
    this.getCompany();
  }

  displayErrorDialog(message: string) {
    this.confirmationService.confirm({
      message: message,
      rejectVisible: false
    });
  }

  onFinishClick() {
    this.closeDialog();
  }

  affiliationChanged(item: any, value: any) {
    item['ID'] = value;
    const arr = value.split('@');
    if (arr.length === 2) {
      item['CO1_ID'] = arr[0];
      item['LO1_ID'] = arr[1];
    } else {
      item['CO1_ID'] = null;
      item['LO1_ID'] = null;
    }
  }

}
