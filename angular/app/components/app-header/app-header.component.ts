import { Component, ElementRef, EventEmitter, NgModule, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { ShopService } from '../../services/shop.service';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { MatSelect } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../../auth/auth.service';
import { HeaderService } from '../../services/header.service';
import { Subscription } from 'rxjs/Rx';

interface ICompany {
  CO1_ID: string;
  CO1_SHORT_CODE: string;
  CO1_NAME: string;
}

interface IShop {
  LO1_ID: string;
  LO1_SHORT_CODE: string;
  LO1_NAME: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
  providers: [
    ShopService,
    CompanyService,
    AuthService
  ]
})

@NgModule({
  imports: [
  ]
})

export class AppHeaderComponent implements OnInit, OnDestroy {
  private page = new Page();
  @ViewChild('hr') header:  ElementRef;
  @Output() onshow = new EventEmitter();

  private allCompany = {
    'CO1_ID': null,
    'CO1_SHORT_CODE': '',
    'CO1_NAME': _lang('#SelectCompany#')
  };

  private allShop  = {
    'LO1_ID': 0,
    'LO1_SHORT_CODE': '',
    'LO1_NAME': _lang('#SelectShop#')
  };

  private companies:   Array<any> = [this.allCompany];

  private shops:   Array<any> = [this.allShop];

  companyControl = new FormControl();
  shopControl = new FormControl();

  selectedCompany: any = {
    'CO1_ID': null,
    'CO1_SHORT_CODE': '',
    'CO1_NAME': _lang('#SelectCompany#'),
  };
  selectedShop: any = {
    'LO1_ID': 0,
    'LO1_SHORT_CODE': '',
    'LO1_NAME': _lang('#SelectShop#'),
  };

  CO1_ID:number = null;
  LO1_ID:number = 0;

  private headerSubscription: Subscription;
  private companiesSubscription: Subscription;

  constructor(private companyService: CompanyService,
              private shopService: ShopService,
              private authService: AuthService,
              private toastr: ToastrService,
              private headerService: HeaderService) {
    let me = this;
    this.headerSubscription = this.headerService.subscribe({
      next: (v) => {
        if (!v.hasOwnProperty('loadHeader')) {
          me.companiesSubscription = this.companyService.getCompanies(new Page()).subscribe(res => {
            me.companies = [this.allCompany].concat(res.data);
            me.authService.companies = res.data;
            me.filterCompanies();
            me.getCompanies();
          },
          error => {
            me.toastr.error(_lang('Companies') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
        }
      }
    });
  }

  /** control for the selected company */
  public companyCtrl: FormControl = new FormControl();

  /** control for the selected shop */
  public shopCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public companyFilterCtrl: FormControl = new FormControl();

  /** control for the MatSelect filter keyword */
  public shopFilterCtrl: FormControl = new FormControl();

  /** list of companies filtered by search keyword */
  public filteredCompanies: any[] = [];

  /** list of shops filtered by search keyword */
  public filteredShops: any[] = [];

  @ViewChild('companySelect') companySelect: MatSelect;
  @ViewChild('shopSelect') shopSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  private _onDestroyCompany = new Subject<void>();
  private _onDestroyShop = new Subject<void>();

  ngOnInit() {
    this.onshow.next();
    this.CO1_ID = this.authService.companyId;
    this.LO1_ID = this.authService.locationId;
    this.headerService.setCentralLocation(this.authService.centralLocation);

    this.getCompanies();

    // listen for search field value changes
    this.companyFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCompany))
        .subscribe(() => {
          this.filterCompanies();
        });
    this.shopFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyShop))
        .subscribe(() => {
          this.filterShops();
        });
  }

  ngOnDestroy() {
    this._onDestroyCompany.next();
    this._onDestroyCompany.complete();
    this._onDestroyShop.next();
    this._onDestroyShop.complete();
    this.headerSubscription.unsubscribe();
    if (this.companiesSubscription)
      this.companiesSubscription.unsubscribe();
  }

  private filterCompanies() {
    if (!this.companies) {
      return;
    }
    // get the search keyword
    let search = this.companyFilterCtrl.value;
    if (!search) {
      this.filteredCompanies = this.companies;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the companies
    this.filteredCompanies = this.companies.filter(company => company.CO1_SHORT_CODE && company.CO1_SHORT_CODE.toLowerCase().indexOf(search) > -1
          || company.CO1_NAME && company.CO1_NAME.toLowerCase().indexOf(search) > -1);
  }

  private filterShops() {
    if (!this.shops) {
      return;
    }
    // get the search keyword
    let search = this.shopFilterCtrl.value;
    if (!search) {
      this.filteredShops = this.shops;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the companies
    this.filteredShops = this.shops.filter(shop => shop.LO1_SHORT_CODE && shop.LO1_SHORT_CODE.toLowerCase().indexOf(search) > -1
          || shop.LO1_NAME && shop.LO1_NAME.toLowerCase().indexOf(search) > -1);
  }

  companyChanged(e) {
    this.CO1_ID = parseInt(e.value['CO1_ID']);
    this.clearShops();
    this.headerService.setCentralLocation(null);
    this.authService.setCompanyId(this.CO1_ID, true).subscribe( result => {
      this.headerService.setCompany(this.CO1_ID);
      this.headerService.setCompanyName(e.value['CO1_NAME']);
      this.getShops();
    });
  }

  shopChanged(e) {
    this.LO1_ID = parseInt(e.value['LO1_ID']);
    this.authService.setLocationId(this.LO1_ID, true).subscribe( result => {
      this.headerService.setLocation(this.LO1_ID);
      this.headerService.setLocationName(e.value['LO1_NAME']);
      this.headerService.next({'loadHeader': true});
    });
  }

  clearShops() {
    this.authService.setLocationId(0);
    this.LO1_ID = this.authService.locationId;
    this.shops = [{
      'LO1_ID': this.LO1_ID,
      'LO1_SHORT_CODE': '',
      'LO1_NAME': _lang('#SelectShop#'),
    }];
  }

  getCompanies() {
    this.companies = this.companies.concat( this.authService.companies.filter(v => v['CO1_NAME'] && v['CO1_NAME'] !== ''));
    this.headerService.setCompanies(this.companies);
    let me = this;
    if (this.CO1_ID) {
      this.selectedCompany = this.companies.filter( item => {
        return parseInt(item['CO1_ID']) === me.CO1_ID;
      })[0];
      if (this.selectedCompany) {
        this.headerService.setCompany(this.selectedCompany['CO1_ID']);
        this.headerService.setCompanyName(this.selectedCompany['CO1_NAME']);
      }
    }
    this.shops = [{
      'LO1_ID': 0,
      'LO1_SHORT_CODE': '',
      'LO1_NAME': _lang('#SelectShop#'),
    }];
    this.getShops();
    this.filterCompanies();
  }

  getShops() {
    if (this.CO1_ID) {
      this.page.filterParams['CO1_ID'] = this.CO1_ID;
    }
    let me = this;
    this.shopService.getItems(this.page).subscribe(result => {
      me.shops = me.shops.concat(result.data.filter(v => v['LO1_NAME'] && v['LO1_NAME'] !== ''));
      me.headerService.setLocations(me.shops);
      var shops = me.shops.filter( item => {
        return parseInt(item['LO1_ID']) === me.LO1_ID;
      });
      me.selectedShop = shops[0];
      if (me.selectedShop) {
        me.headerService.setLocation(me.selectedShop['LO1_ID']);
        me.headerService.setLocationName(me.selectedShop['LO1_NAME']);
      }
      me.headerService.next({'loadHeader': true});
      me.filterShops();
    },
    error => {
      me.toastr.error(_lang('#Shops#') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  getHeaderHeight(): number {
    return this.header && this.header.nativeElement ? this.header.nativeElement.offsetHeight : 0;
  }
}
