import {
  Component, ElementRef, EventEmitter, HostListener, Inject, Input, OnDestroy, OnInit, Output, QueryList, ViewChild,
  ViewChildren
} from '@angular/core';
import { TechnicianFinderModalComponent } from '../../components/technician-finder-modal/technician-finder-modal.component';
import { ShopFinderModalComponent } from '../../components/shop-finder-modal/shop-finder-modal.component';
import { Page } from '../../model/page';
import { permissions } from '../../permissions';
import { MasterDataService } from '../../services/master.data.service';
import { MatAutocompleteTrigger, MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { TitleService } from '../../services/title.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { FormControl } from '@angular/forms';
import { LanguageEditorModalComponent } from '../../components/language-editor-modal/language-editor-modal.component';
import { CellEditor } from 'primeng/table';
import { DOCUMENT } from '@angular/common';
import { AddCatalogListModalComponent } from '../../components/add-catalog-list-modal/add-catalog-list-modal.component';
import { HeaderService } from '../../services/header.service';
import { Subscription } from 'rxjs';
import { MasterDataModalComponent } from '../../components/master-data-modal/master-data-modal.component';
import * as _ from 'lodash';
import { StoreOrderService } from '../../services/store-order.service';
import { TechnicianService } from '../../services/technician.service';
import { TechScanService } from '../../services/tech-scan.service';
import {Constants} from "../../constants";

@Component({
  selector: 'app-tech-scan-add-form',
  templateUrl: './tech-scan-add-form.component.html',
  styleUrls: ['./tech-scan-add-form.component.scss'],
  providers: [
    StoreOrderService,
    MasterDataService,
    TechnicianService,
    ConfirmationService,
    TechScanService
  ],
  preserveWhitespaces: true
})
export class TechScanAddFormComponent implements OnInit, OnDestroy {
  private permissions = permissions;
  private constants = Constants;

  @Input() isModal: boolean = false; // form or modal
  @Input() technicianShortCode: string = '';
  @Output() close = new EventEmitter();

  @ViewChild('techScansAddForm')  techScansAddForm: ElementRef;
  @ViewChild('contentGrid')       contentGrid:  ElementRef;
  @ViewChild('actionbar')         actionbar:  ElementRef;
  @ViewChild('cardHeader')        cardHeader:  ElementRef;
  @ViewChild('addForm')           addForm:  ElementRef;
  @ViewChild('content')           content: ElementRef;
  @ViewChild('tableHeader')       tableHeader:  ElementRef;
  @ViewChildren(CellEditor)       editors: QueryList<CellEditor>;
  @ViewChild('inputPartNumber')   inputPartNumber: ElementRef;
  @ViewChild(MatAutocompleteTrigger) autoParts: MatAutocompleteTrigger;

  cols: any[];
  items: any[] = [];
  parts: any[] = [];
  selected: any;
  private record: any;
  private errors: Array<any> = [];

  private page = new Page();
  private size: number = 50;
  private loading: boolean = false;
  private loaderWidth: String = '0px';
  private barcode: string = '';

  searchValue: string = '';
  totalValue: number = 0;

  filteredParts: any[] = [];
  addedParts: any[] = [];
  partNumber: string = '';
  private qtyIn = false;

  partsControl = new FormControl();
  private subscription: Subscription;

  deleteConfirmation: string = _lang('DELETE CONFIRMATION');
  deleteConfirmationMessage: string = _lang('Are you sure that you want to perform this action?');

  private formHeight: string = '0px';
  private dialogContentHeight: string = '0px';
  private scrollHeight: string = '0px';

  constructor(private masterDataService: MasterDataService,
              private storeOrderService: StoreOrderService,
              private technicianService: TechnicianService,
              private techScanService: TechScanService,
              public  dialog: MatDialog,
              private router: Router,
              private confirmationService: ConfirmationService,
              private titleService: TitleService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              @Inject(DOCUMENT) document,
              private headerService: HeaderService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.record = this.techScanService.initialize();

    this.subscription = this.headerService.subscribe({
      next: (v) => {
        this.page.pageNumber = 0;
        this.initOrder();
      }
    });
  }

  ngOnInit() {
    this.titleService.setNewTitle(this.route);
    this.loaderWidth = this.techScansAddForm.nativeElement.parentElement.parentElement.offsetWidth + 'px';

    this.cols = [
      { field: 'MD1_ID',                  header: _lang('MD1_ID'),                visible: false, width: 100 },
      { field: 'VD1_ID',                  header: _lang('VD1_ID'),                visible: false, width: 100 },
      { field: 'VD1_SHORT_CODE',          header: _lang('VD1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'VD1_NAME',                header: _lang('VD1_NAME'),              visible: false, width: 100 },
      { field: 'CO1_ID',                  header: _lang('CO1_ID'),                visible: false, width: 100 },
      { field: 'CO1_SHORT_CODE',          header: _lang('CO1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'CO1_NAME',                header: _lang('CO1_NAME'),              visible: false, width: 100 },
      { field: 'LO1_ID',                  header: _lang('LO1_ID'),                visible: false, width: 100 },
      { field: 'LO1_SHORT_CODE',          header: _lang('LO1_SHORT_CODE'),        visible: false, width: 100 },
      { field: 'LO1_NAME',                header: _lang('LO1_NAME'),              visible: false, width: 100 },
      { field: 'FI1_ID',                  header: _lang('FI1_ID'),                visible: false, width: 100 },
      { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),       visible: true,  width: 100 },
      { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),             visible: true,  width: 100 },
      { field: 'MD1_UNIT_PRICE',          header: _lang('MD1_UNIT_PRICE'),        visible: true,  width: 90  },
      { field: 'UM1_PRICE_NAME',          header: _lang('UM1_PRICE_NAME'),        visible: false, width: 100 },
      { field: 'MD1_ORDER_QTY',           header: _lang('MD1_ORDER_QTY'),         visible: true,  width: 50  },
      { field: 'UM1_PURCHASE_NAME',       header: _lang('UM1_PURCHASE_NAME'),     visible: true,  width: 70  },
      { field: 'RECEIPT_QTY',             header: _lang('QTY'),                   visible: true,  width: 90  },
      { field: 'UM1_RECEIPT_NAME',        header: _lang('UM1_RECEIPT_NAME'),      visible: true,  width: 100 },
      { field: 'MD2_ON_HAND_QTY',         header: _lang('MD2_ON_HAND_QTY'),       visible: true,  width: 70  },
      { field: 'MD2_MIN_QTY',             header: _lang('MD2_MIN_QTY'),           visible: true,  width: 70  },
      { field: 'MD1_EXTENDED_VALUE',      header: _lang('MD1_EXTENDED_VALUE'),    visible: true,  width: 100 }
    ];

    this.initOrder();

    setTimeout(() => {this.formHeight = this.getFormHeight() + 'px';
      this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';}, 300);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  initOrder() {
    this.loading = true;
    var result = this.techScanService.isScanCanBeCreated( this.headerService );
    if (!result['canBeCreated']) {
      this.toastr.error(result['error']);
      this.router.navigate(['/store-orders']);
    } else {
      this.techScanService.initOrder(this.page).subscribe( res => {
        this.record['CO1_ID']        = res['company']['CO1_ID'];
        this.record['CO1_NAME']      = res['company']['CO1_NAME'];

        this.record['LO1_FROM_ID']   = res['fromLocation']['LO1_ID'];
        this.record['LO1_FROM_NAME'] = res['fromLocation']['LO1_NAME'];

        this.record['LO1_TO_ID']   = res['toLocation']['LO1_ID'];
        this.record['LO1_TO_NAME'] = res['toLocation']['LO1_NAME'];

        this.parts = res['parts'];
        this.record.OR1_NUMBER = res['OR1_NUMBER'];
        this.record.OR1_ID     = res['OR1_ID'];

        this.addedParts = [];
        if (this.technicianShortCode) {
          let TE1_SHORT_CODE = this.technicianShortCode;
          let page = new Page();
          page.filterParams['SHORT_CODE'] = TE1_SHORT_CODE;
          this.technicianService.getTechnicians(page).subscribe(result => {
                if (result.data['currentVocabulary'] && result.data['currentVocabulary'].length) {
                  let tech = result.data['currentVocabulary'][0];
                  this.record.TE1_ID = tech['TE1_ID'];
                  this.record.TE1_SHORT_CODE = tech['TE1_SHORT_CODE'];
                  this.record.TE1_NAME = tech['TE1_NAME'];
                }
              },
              error => {
                this.toastr.error(_lang('Technicians') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
                  timeOut: 3000,
                });
              });
          this.inputPartNumber.nativeElement.focus();
        } else {
          this.selectTechnician();
        }

        this.loading = false;
      });
    }
  }

  filterParts(value: string) {
    const filterValue = value.toLowerCase();
    if (filterValue.length === 0) {
      this.autoParts.closePanel();
    }
    this.filteredParts = this.parts.filter(part => {
      return part['MD1_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1 ||
            (part['MD1_DESC1'] && part['MD1_DESC1'].toLowerCase().indexOf(filterValue) > -1) ||
            (part['MD1_UPC1'] && part['MD1_UPC1'].toLowerCase().indexOf(filterValue) > -1) ||
            (part['MD1_VENDOR_PART_NUMBER'] && part['MD1_VENDOR_PART_NUMBER'].toLowerCase().indexOf(filterValue) > -1)
      });
  }

  clearSaveBarCodeKeyUp(event, fieldName){
    this.techScanService.clearSaveBarcode( this.record, fieldName );
  }

  partNumberKeyUp(event) {
    if (event.keyCode === 13) {
      this.partNumber = this.partNumber.trim().toLowerCase();
      this.techScanService.clearSaveBarcode( this, 'partNumber' );
      this.filterParts(this.partNumber);
      if (this.partNumber && this.partNumber !== '') {
        if (this.filteredParts && this.filteredParts.length > 0) {
          this.onSelectPart(this.filteredParts[0]['MD1_PART_NUMBER']);
          this.filteredParts = [];
        } else {
          this.toastr.error(_lang('No one part number does not match'), _lang('Error'), {
            timeOut: 3000,
          });
        }
      }
    }
  }

  partNumberKeyDown(e){
    if ( e.keyCode === 13 ) {
      var bcRegExp = /^\+?qty\.?([0-9]+)/i;
      if( bcRegExp.test(this.barcode) ){
        let match = bcRegExp.exec(this.barcode);
        this.addedParts = this.addedParts.map( (part, index) => {
          if(index == (this.addedParts.length - 1)){
            part['MD1_ORDER_QTY'] = parseInt(match[1]);
          }
          return part;
        });
        this.updateTotals();
        this.partNumber = '';
      }
      this.barcode = '';
    }else{
      if(e.key == '+'){
        this.barcode = '';
      }
      if(e.key != 'Shift'){
        this.barcode += e.key;
      }
    }
  }

  onSelectPart(value) {
    this.partNumber = '';
    this.page.filterParams['LO1_TO_ID']       = this.record.LO1_TO_ID;
    this.page.filterParams['LO1_FROM_ID']     = this.record.LO1_FROM_ID;
    this.page.filterParams['MD1_PART_NUMBER'] = value;

    this.masterDataService.getFullParts(this.page).subscribe(res => {
          this.addPartsToOrder(res['data'], false);
        },
        error => {
          this.toastr.error(_lang('Parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });

    setTimeout(() => {
      this.filteredParts = [];
      this.partNumber = '';
      this.barcode = '';
    }, 300);
  }

  selectTechnician() {
    let dialogRef = this.dialog.open(TechnicianFinderModalComponent, {
      width: window.innerWidth - 100 + 'px',
      height: window.innerHeight - 80 + 'px',
      role: 'dialog',
      data: {title: _lang('Select Technician'), TE1_ID: this.record.TE1_ID},
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.TE1_ID = selected['TE1_ID'];
        this.record.TE1_SHORT_CODE = selected['TE1_SHORT_CODE'];
        this.record.TE1_NAME = selected['TE1_NAME'];
      }
      this.inputPartNumber.nativeElement.focus();
    })
  }

  clearTechnician() {
    this.record.TE1_ID = 0;
    this.record.TE1_SHORT_CODE = '';
    this.record.TE1_NAME = '';
  }

  selectToShop() {
    let dialogRef = this.dialog.open(ShopFinderModalComponent, {
      width: '90%',
      height: window.innerHeight - 100 + 'px',
      role: 'dialog',
      data: {
        filters: {
          'CO1_ID': this.record.CO1_ID
        },
        HIGHLIGHT_LO1_ID: this.record.LO1_TO_ID
      },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.LO1_TO_NAME = selected.LO1_NAME;
        this.record.LO1_TO_ID = selected.LO1_ID;
      }
    })
  }

  clearToShop() {
    this.record.ORDER_BY_LOCATION_NAME = '';
    this.record.LO1_TO_ID = '';
  }

  selectFromShop() {
    let dialogRef = this.dialog.open(ShopFinderModalComponent, {
      width: '90%',
      height: window.innerHeight - 100 + 'px',
      role: 'dialog',
      data: {
        sourceUrl: '/shops/central-locations',
        filters: {
          'CO1_ID': this.record.CO1_ID,
          'LO1_ID': this.record.LO1_TO_ID
        },
        HIGHLIGHT_LO1_ID: this.record.LO1_FROM_ID
      }
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.record.LO1_FROM_NAME = selected.LO1_NAME;
        this.record.LO1_FROM_ID = selected.LO1_ID;
      }
    })
  }

  clearFromShop() {
    this.record.ORDER_FROM_LOCATION_NAME = '';
    this.record.LO1_FROM_ID = '';
  }

  updateTotals() {
    this.totalValue = 0;
    this.addedParts = this.addedParts.map(record => {
      const unitPrice: number = Number(record['MD1_UNIT_PRICE'].toString().replace(/[^.0-9]/g, ''));
      const orderQty: number  = Number(record['MD1_ORDER_QTY'].toString().replace(/[^.0-9]/g, ''));
      record['MD1_EXTENDED_VALUE'] = unitPrice * orderQty;
      this.totalValue += record['MD1_EXTENDED_VALUE'];
      return record;
    });
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Shop Order Form'),
        componentRef: this,
        data: [],
        componentName: 'TechScanAddComponent'
      },
    });
  }

  qtyKeyDown(e, rowData, field) {
    if ( e.keyCode === 13 ) {
      e.stopPropagation();
      let child = document.querySelectorAll(".ui-selectable-row:last-child");
      let rowIndex = child[0].getAttribute('ng-reflect-index');
      const nextRowIndex = parseInt(rowIndex) + 1;
      this.setFocusToField(nextRowIndex, 'td');
      this.inputPartNumber.nativeElement.focus();
    }
  }

  setFocusToField(rowIndex: number, rowName: string) {
    const row = rowName + rowIndex;
    const elem = document.getElementById(row);
    if (elem) {
      elem.click();
    } else {
      const currentTd = <HTMLTableCellElement>document.getElementById(rowName + (rowIndex - 1));
      currentTd.nextSibling['click']();
    }
  }

  qtyKeyPress(e){
    return e.charCode >= 48 && e.charCode <= 57;
  }

  qtyKeyUp(e) {
    this.updateTotals();
  }

  onEditInit( $event ) {
    var interval = setInterval(() => {
      const input = <HTMLInputElement>document.getElementById('input_' + $event.data.MD1_ID);
      if (input) {
        clearInterval(interval);
        interval = null;
        input.select();
      }
    }, 5);
  }

  onAddMasterData() {
    let dialogRef = this.dialog.open(MasterDataModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data - Stock at ') + this.record.LO1_FROM_NAME,
        componentRef: this,
        url: '/master-data/full-parts',
        filters: {
          LO1_TO_ID:     this.record.LO1_TO_ID,
          LO1_FROM_ID:   this.record.LO1_FROM_ID,
          "sort[CO1_NAME]": 'ASC',
          "sort[VD1_NAME]": 'ASC',
          "sort[MD1_PART_NUMBER]": 'ASC',
          usePagination: true
        }
      }
    });

    dialogRef.beforeClose().subscribe( parts => {
      this.addPartsToOrder(parts);
    })
  }

  onAddCatalog() {
    let dialogRef = this.dialog.open(AddCatalogListModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Customer Data - Stock at ') + this.record.LO1_FROM_NAME,
        componentRef: this,
        filters: {
          LO1_TO_ID:     this.record.LO1_TO_ID,
          LO1_FROM_ID:   this.record.LO1_FROM_ID,
          "sort[CO1_NAME]": 'ASC',
          "sort[VD1_NAME]": 'ASC',
          "sort[MD1_PART_NUMBER]": 'ASC',
          usePagination: true
        }
      },
    });

    dialogRef.beforeClose().subscribe( parts => {
      this.addPartsToOrder(parts, true);
    })
  }

  addPartsToOrder( parts, fromCatalog: boolean = false, autoFocus: boolean = false ) {
    if (parts) {

      var parts = parts.map( part => {
        var item = _.clone(part);
        if (this.record.LO1_FROM_ID != this.record.LO1_TO_ID) {
          // Not an internal order.
          item["UM1_PURCHASE_ID"]   =  part["UM1_PURCHASE_ID"];
          item["UM1_PURCHASE_NAME"]  = part["UM1_PURCHASE_NAME"];
          item["UM2_RECEIPT_FACTOR"] = part["UM2_RECEIPT_FACTOR"];
        } else {
          // Internal order. The purchase UM needs to be set to the receipt UM since its the same shop.
          // That means factor must be 1.
          item["UM1_PURCHASE_ID"]    = part["UM1_RECEIPT_ID"];
          item["UM1_PURCHASE_NAME"]  = part["UM1_RECEIPT_NAME"];
          item["UM2_RECEIPT_FACTOR"] = "1";
        }
        item["MD2_ON_HAND_QTY"] = part["MD2_ON_HAND_QTY_TO"];
        item["MD2_AVAILABLE_QTY"] = part["MD2_AVAILABLE_QTY_TO"];
        item["MD2_MIN_QTY"] = part["MD2_MIN_QTY_TO"];
        item["MD2_AVAILABLE_QTY_FROM"] = part["MD2_AVAILABLE_QTY"];
        item["MD2_ON_HAND_QTY_FROM"] = part["MD2_ON_HAND_QTY"];

        if (part['MD1_MARKUP'] > 0 && this.record.LO1_FROM_ID == this.record.LO1_TO_ID){
          if(fromCatalog){
            item["MD1_UNIT_PRICE"] = part["MD1_UNIT_PRICE"]/(1+part['MD1_MARKUP']/100);
          }else{
            item["MD1_UNIT_PRICE"] = part["MD1_UNIT_PRICE"]*(1+part['MD1_MARKUP']/100);
          }
        }

        if (this.record.LO1_FROM_ID == this.record.LO1_TO_ID) {
            item["MD1_ORDER_QTY"] = 1;
        } else {
            item["MD1_ORDER_QTY"] = part["DEFAULT_ORDER_QTY"] ? part["DEFAULT_ORDER_QTY"] : 1;
        }
        item["MD1_EXTENDED_VALUE"] = part["MD1_UNIT_PRICE"];
        item["MD1_UNIT_COST"] = part["MD1_UNIT_PRICE"];
        return item;
      });

      parts.forEach( part => {
        if (!part['MD1_ORDER_QTY']) {
          part['MD1_ORDER_QTY'] = 1;
        }
        let filtered = this.addedParts.filter( item => {
          return item['MD1_ID'] === part.MD1_ID;
        });
        if (!filtered || filtered.length === 0) {
          this.addedParts.push(part);
        } else {
          filtered[0]['MD1_ORDER_QTY'] = parseInt(filtered[0]['MD1_ORDER_QTY']) + parseInt(part['MD1_ORDER_QTY']);
        }
      });

      if (autoFocus) {
        setTimeout(() => {this.setFocusToPart(parts[parts.length - 1]);}, 300);
      }

      this.updateTotals();
    }
  }

  setFocusToPart(addedPart: any, rowName: string = 'td') {
    let partIndex: number = 0;
    for (var k = 0; k < this.addedParts.length; k++) {
      if (addedPart['MD1_PART_NUMBER'] === this.addedParts[k]['MD1_PART_NUMBER']) {
        break;
      }
      partIndex ++;
    }
    this.setFocusToField(partIndex, rowName);
  }

  onDelete() {
    const me = this;
    this.confirmationService.confirm({
      message: this.deleteConfirmationMessage,
      accept: () => {
        this.selected.forEach(function(record) {
          me.addedParts.forEach((item, index) => {
            if (item['MD1_ID'] === record['MD1_ID'])
              me.addedParts.splice(index, 1);
          });
        });
        this.selected = [];
        this.updateTotals();
      }
    });
  }

  onSave() {
    if (this.headerService.isCentralLocation(this.record.LO1_TO_ID) && !this.record.TE1_ID) {
      this.toastr.error(
          _lang(':ErrorTechReqd:'),
          _lang('#ReqdField#'),{
        timeOut: 10000,
        positionClass: 'toast-center-center',
        enableHtml: true
      });
    } else {
      var errors = [];
      var zeros  = [];

      this.addedParts.forEach( part => {
        var ON_HAND_QTY   = parseInt(this.record.LO1_FROM_ID == this.record.LO1_TO_ID ? part['MD2_ON_HAND_QTY']   : part['MD2_ON_HAND_QTY_FROM']);

        if ( part['MD1_ORDER_QTY'] > ON_HAND_QTY) {
          errors.push(part['MD1_PART_NUMBER'] + ' - Ordering: ' + part['MD1_ORDER_QTY'] + ', On Hand: <b>' + ON_HAND_QTY + '</b>');
        }
        if (part['MD1_ORDER_QTY'] <= 0 || ON_HAND_QTY <= 0) {
          zeros.push(part['MD1_PART_NUMBER'] + ': ' + part['MD1_DESC1']);
        }
      });

      if (errors.length) {
        var msg  = _lang(':ErrorOverStock:') + "<br/>";
            msg += _lang(':ErrorReduceQty:') + "<br/>";
            msg +=  errors.join("<br/>");
        this.toastr.error(msg, _lang('#QtyExceeded#'),{
          timeOut: 10000,
          positionClass: 'toast-center-center',
          enableHtml: true
        });
      } else if (zeros.length) {
        var msg = _lang(':ErrorRemoveInvalidQty:') + "\n";
            msg +=  errors.join("\n");
        this.toastr.error(msg, _lang('#QtyInvalid#'), {
          timeOut: 10000,
          positionClass: 'toast-center-center',
          enableHtml: true
        });
      } else {
        var orders = [];
        var orderNumberIndex = 0;
        this.addedParts.forEach( part => {
            var orderExists = false;

            orders.forEach( order => {
                if (part['VD1_ID'] === order['VD1_ID']) {
                  orderExists = true;
                  order.parts.push(part);
                } else if (this.record.LO1_FROM_ID !== this.record.LO1_TO_ID) {
                  orderExists = true;
                  orders[0].parts.push(part);
                }
            });

            if ( !orderExists ) {
                var newOrder = new Object();
                newOrder['VD1_ID']      = part['VD1_ID'];
                newOrder['TE1_ID']      = this.record.TE1_ID;
                newOrder['LO1_FROM_ID'] = this.record.LO1_FROM_ID;
                newOrder['LO1_TO_ID']   = this.record.LO1_TO_ID;
                newOrder['OR1_NUMBER']  = this.record.OR1_NUMBER + orderNumberIndex;
                newOrder['OR1_COMMENT'] = this.record.OR1_COMMENT;
                newOrder['parts']       = [part];
                orders.push(newOrder);
                orderNumberIndex ++;
            }
        });

        this.loading = true;
        this.storeOrderService.create(orders).subscribe( result => {
          this.loading = false;
          if (result.success) {
            this.toastr.success(_lang('Tech Scan was successfully created!'), _lang('Service Success'), {
              timeOut: 3000,
            });

            if (this.isModal) {
              this.close.emit(true);
            } else {
              this.router.navigate(['/tech-scans']);
            }
          } else {
            this.toastr.error(_lang(result.errors.join('<br/>')), _lang('Error!'), {
              timeOut: 3000
            });
          }
        });
      }
    }
  }

  cancelClick() {
    if (this.isModal) {
      this.close.emit(true);
    } else {
        window.history.back();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    setTimeout(() => {this.formHeight = this.getFormHeight() + 'px';
      this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';}, 300);
  }

  getFormHeight(): number {
    return this.techScansAddForm.nativeElement.parentElement.parentElement.parentElement.parentElement.offsetHeight - 50;
  }

  getContentDialogHeight(): number {
    return this.getFormHeight() -
        (this.actionbar.nativeElement.offsetHeight + this.cardHeader.nativeElement.offsetHeight +
        this.addForm.nativeElement.offsetHeight + 60);
  }

  getScrollHeight(): number{
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
  }
}
