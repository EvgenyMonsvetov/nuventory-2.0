import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { TechScanAddFormComponent } from './tech-scan-add-form.component';
import { MatAutocompleteModule, MatRadioModule } from '@angular/material';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
    imports: [
        NgxPermissionsModule,
        CommonModule,
        SharedModule,
        MatRadioModule,
        MatAutocompleteModule
    ],
    declarations: [ TechScanAddFormComponent ],
    exports: [ TechScanAddFormComponent ],
    schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ]
})
export class TechScanAddFormModule { }
