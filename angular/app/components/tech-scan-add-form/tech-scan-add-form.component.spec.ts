import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechScanAddFormComponent } from './tech-scan-add-form.component';

describe('TechScanAddFormComponent', () => {
  let component: TechScanAddFormComponent;
  let fixture: ComponentFixture<TechScanAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechScanAddFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechScanAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
