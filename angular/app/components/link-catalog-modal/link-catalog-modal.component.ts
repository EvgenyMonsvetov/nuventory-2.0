import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LangPipe } from '../../views/pipes/lang.pipe';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { permissions } from '../../permissions';
import { MasterDataService } from '../../services/master.data.service';
import { VendorService } from '../../services/vendor.service';
import { ShopFinderModalComponent } from '../shop-finder-modal/shop-finder-modal.component';

@Component({
  selector: 'app-link-catalog-modal',
  templateUrl: './link-catalog-modal.component.html',
  providers: [
    VendorService,
    MasterDataService
  ],
  preserveWhitespaces: true
})
export class LinkCatalogModalComponent implements OnInit {
  private permissions = permissions;

  cols1: any[] = [];
  cols2: any[] = [];
  itemData: any[] = [];
  catalogData: any[] = [];
  vendorData: any[] = [{'VD0_ID': 0,
    'VD0_SHORT_CODE': '',
    'VD0_NAME': _lang(' All -')
  }];
  selectedLocalPart: any;
  selectedCatalogPart: any;
  selectedVendor: any = 0;
  search: string;
  currentSearchIndex = -1;

  private loading1 = false;
  private loading = false;
  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private currentPageIndex: number = 0;

  private scrollHeight1: string = '0px';
  private scrollHeight2: string = '0px';
  private emptyHeight1: string = '0px';
  private emptyHeight2: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('linkCatalogModal')    linkCatalogModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('fields')        fields:  ElementRef;
  @ViewChild('buttons')       buttons:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('footer')        footer:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;
  @ViewChild('searchInput')   searchInput:  ElementRef;

  masterRecord = false;

  private CO1_ID: number = 0;
  private CO1_NAME: string = '';
  private CO1_SHORT_CODE: string = '';
  private LO1_ID: number = 0;
  private LO1_NAME: string = '';

  constructor(public dialogRef: MatDialogRef<LinkCatalogModalComponent>,
              private vendorService: VendorService,
              private masterDataService: MasterDataService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private lang: LangPipe,
              public dialog: MatDialog,
              private toastr: ToastrService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
  }

  ngOnInit() {
    this.loading = true;
    this.masterRecord = this.data.masterRecord;
    this.page.filterParams['MASTER_DATA'] = this.data.masterRecord ? 1 : 0;
    this.itemData = this.data.records;

    this.cols1 = [
      { field: 'MD1_ID',          header: _lang('MD1_ID'),          visible: false, width: 200 },
      { field: 'MD0_ID',          header: _lang('MD0_ID'),          visible: false, width: 200 },
      { field: 'ASSIGNED_TO',     header: _lang('Assigned to..'),   visible: true,  width: 120 },
      { field: 'MD1_PART_NUMBER', header: _lang('MD1_PART_NUMBER'), visible: true,  width: 70 },
      { field: 'MD1_DESC1',       header: _lang('MD1_DESC1'),       visible: true,  width: 200 },
      { field: 'MD1_UPC1',        header: _lang('MD1_UPC1'),        visible: true,  width: 80 },
      { field: 'MD1_VENDOR_PART_NUMBER', header: _lang('MD1_VENDOR_PART_NUMBER'), visible: true,  width: 80 },
      { field: 'UM1_PRICING_NAME',header: _lang('UM1_RECEIPT_NAME'),visible: true, width: 95 },
      { field: 'UM1_PRICE_NAME',  header: _lang('UM1_PRICE_NAME'),  visible: true, width: 85 }
    ];

    this.cols2 = [
      { field: 'MD0_ID',          header: _lang('MD0_ID'),          visible: false, width: 200 },
      { field: 'VD0_SHORT_CODE',  header: _lang('VD1_SHORT_CODE'),  visible: true,  width: 100 },
      { field: 'VD0_NAME',        header: _lang('VD1_NAME'),        visible: true,  width: 160 },
      { field: 'MD0_PART_NUMBER', header: _lang('MD1_PART_NUMBER'), visible: true,  width: 70 },
      { field: 'MD0_DESC1',       header: _lang('MD1_DESC1'),       visible: true,  width: 200 },
      { field: 'MD0_UPC1',        header: _lang('MD1_UPC1'),        visible: true,  width: 80 },
      { field: 'UM1_NAME',        header: _lang('UM1_DEFAULT_PRICING'), visible: true, width: 120 }
    ];

    this.page.pageNumber = 0;
    this.fillform();
  }

  getContentDialogHeight(): number {
    return (this.linkCatalogModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight + 51);
  }

  getScrollHeight1(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
        this.fields.nativeElement.offsetHeight + this.buttons.nativeElement.offsetHeight + 25);
  }

  getScrollHeight2(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
        this.fields.nativeElement.offsetHeight + this.buttons.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight + 15);
  }

  getEmptyHeight1(): number {
    return this.getScrollHeight1() - 1;
  }

  getEmptyHeight2(): number {
    return this.getScrollHeight2();
  }

  fillform() {
    if (this.data.items && this.data.items.length > 0) {
      this.itemData = this.data.items;
    }

    this.getVendors();
  }

  getVendors() {
    this.vendorService.getAllMasterVendors(this.page).subscribe(res => {
          if (res && res.data) {
            this.vendorData = this.vendorData.concat(res.data['currentVocabulary'].filter(v => v['VD0_NAME'] && v['VD0_NAME'] !== ''));
            this.CO1_ID = res.data['company']['CO1_ID'];
            this.CO1_NAME = res.data['company']['CO1_NAME'];
            this.CO1_SHORT_CODE = res.data['company']['CO1_SHORT_CODE'];
            this.LO1_ID = res.data['fromLocation']['LO1_ID'];
            this.LO1_NAME = res.data['fromLocation']['LO1_NAME'];
            this.page.filterParams['VD0_ID'] = this.CO1_ID;
            this.getParts();
          }
        },
        error => {
          this.toastr.error(_lang('Vendors') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
          this.getParts();
        });
  }

  getParts() {
    this.page.filterParams['VD0_ID'] = this.selectedVendor === 0 ? null : this.selectedVendor;
    this.selectedCatalogPart = null;

    this.vendorService.getAllParts(this.page).subscribe(res => {
          this.catalogData = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight1 = this.getScrollHeight1() + 'px';
            this.scrollHeight2 = this.getScrollHeight2() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';
            this.emptyHeight1 = this.getEmptyHeight1() + 'px';
            this.emptyHeight2 = this.getEmptyHeight2() + 'px'; });

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Vendor catalog parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  onInput(value) {
    this.search = value;
    this.currentSearchIndex = -1;
    this.onSearch();
  }

  onSearch() {
    let index = 0;
    if (this.currentSearchIndex >= 0)
      index = this.currentSearchIndex  + 1;

    for (index; index < this.catalogData.length; index ++) {
      if (this.catalogData[index]) {
        if ((this.catalogData[index].MD0_PART_NUMBER && this.catalogData[index].MD0_PART_NUMBER.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.catalogData[index].MD0_DESC1 && this.catalogData[index].MD0_DESC1.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.catalogData[index].MD0_UPC1 && this.catalogData[index].MD0_UPC1.toString().toLowerCase().search(this.search.toLowerCase()) !== -1)) {
          this.currentSearchIndex = index;
          this.setCurrentPage(index);
          const row = 'row' + this.currentSearchIndex;

          setTimeout(() => {this.selectedCatalogPart = this.catalogData[index];
            const elem = document.getElementById(row);
            if (elem) {
              elem.scrollIntoView();
            }
          });
          break;
        }
      }
    }
  }

  onPage2(event) {
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.currentSearchIndex = 0;
    this.currentPageIndex = event.first;
    this.selectedCatalogPart = null;
  }

  setCurrentPage(index: number) {
    const n = index === 0 ? 0 : Math.ceil(index / this.page.size) - 1;
    this.currentPageIndex = this.page.size * n;
  }

  openLanguageEditor() {
    this.closeDialog()
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Assign Form'),
        componentRef: this,
        data: [],
        componentName: 'LinkCatalogModalComponent'
      },
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onSave() {
    const parts: any = [];

    if (this.itemData) {
      this.loading1 = true;
      const cols = this.cols1;
      const m = this.masterRecord;
      this.itemData.forEach(function(item) {
          const part: any = {};
          part['MD0_ID'] = item['MD0_ID'];
          part['MD1_ID'] = item['MD1_ID'];
          parts.push(part);
      });

      this.masterDataService.updateParts(parts).subscribe(result => {
        if (result.success) {
          this.loading1 = false;
        } else {
          this.loading1 = false;
          this.toastr.error(_lang('On save parts error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        }
      });
    }
  }

  onAutoMatch() {
    this.loading1 = true;
    const catalogParts = this.catalogData;

    this.itemData.forEach(function(item) {
      catalogParts.forEach(function(item2) {
        if ((item.MD1_VENDOR_PART_NUMBER === item2.MD0_PART_NUMBER && item2.MD0_PART_NUMBER !== '') ||
            (item.MD1_UPC1 === item2.MD0_UPC1 && item2.MD0_UPC1 !== '')) {
          item['MD0_ID'] = item2['MD0_ID'];
          item['ASSIGNED_TO'] = item2['VD0_SHORT_CODE'] + '-' + item2['MD0_PART_NUMBER'];
        }
      });
    });
    this.loading1 = false;
  }

  onClearSelected() {
    this.selectedLocalPart.forEach(function(item) {
      item['MD0_ID'] = '';
      item['ASSIGNED_TO'] = '';
    });
  }

  onAssign() {
    const selectedCPart = this.selectedCatalogPart;
    this.selectedLocalPart.forEach(function(item) {
      item['MD0_ID'] = selectedCPart['MD0_ID'];
      item['ASSIGNED_TO'] = selectedCPart['VD0_SHORT_CODE'] + '-' + selectedCPart['MD0_PART_NUMBER'];
    });
  }

  selectShop() {
    let dialogRef = this.dialog.open(ShopFinderModalComponent, {
      width: '90%',
      height: window.innerHeight - 100 + 'px',
      role: 'dialog',
      data: {
        filters: {
          'CO1_ID': this.CO1_ID
        },
        HIGHLIGHT_LO1_ID: this.LO1_ID
      },
    });

    dialogRef.beforeClose().subscribe( selected => {
      if (selected) {
        this.LO1_NAME = selected.LO1_NAME;
        this.LO1_ID = selected.LO1_ID;
      }
    })
  }

  clearShop() {
    this.LO1_NAME = '';
    this.LO1_ID = 0;
  }

}
