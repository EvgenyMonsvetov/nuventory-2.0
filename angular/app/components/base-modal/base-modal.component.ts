import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-base-modal',
  templateUrl: './base-modal.component.html',
})
export class BaseModalComponent {
  private barcode: string = '';
  public tagSave: string = '_SAVE';

  constructor() { }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event) {
    if (event.key !== 'Enter') {
      this.barcode += event.key;
    } else {
      let scannedText = this.barcode;
      this.barcode = '';
      let indexSave = scannedText.indexOf(this.tagSave);

      if (indexSave >= 0) {
        event.stopImmediatePropagation();
        this.save();
      }
    }
  }

  save() {
    // method for override
  }
}
