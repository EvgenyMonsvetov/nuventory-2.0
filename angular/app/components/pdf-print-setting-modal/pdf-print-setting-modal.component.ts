import { Component } from '@angular/core';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { MatDialog, MatDialogRef } from '@angular/material';
import { permissions } from '../../permissions';

@Component({
  selector: 'app-pdf-print-setting-modal',
  templateUrl: './pdf-print-setting-modal.component.html',
  preserveWhitespaces: true
})
export class PdfPrintSettingModalComponent {
  private permissions = permissions;

  private selectedRow = 0;
  private selectedColumn = 1;
  private selectedBarcodeType = 'C39';

  private columnDP: any[] = [
  {label: '1(Default)', data: 1},
  {label: '2', data: 2},
  {label: '3', data: 3}];

  private rowDP: any[] = [
  {label: '1(Default)', data: 0},
  {label: '2', data: 1},
  {label: '3', data: 2},
  {label: '4', data: 3},
  {label: '5', data: 4},
  {label: '6', data: 5},
  {label: '7', data: 6},
  {label: '8', data: 7},
  {label: '9', data: 8},
  {label: '10', data: 9}];

  private barcodeTypesDP: any[] = [
    {label: 'Code 39 (Default)', data: 'C39'},
    {label: 'Code 128', data: 'C128'}];

  constructor(public dialogRef: MatDialogRef<PdfPrintSettingModalComponent>,
              public  dialog: MatDialog) { }

  openLanguageEditor() {
    this.closeDialog();
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('User Setting Form'),
        componentRef: this,
        data: [],
        componentName: 'PdfPrintSettingModalComponent'
      },
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onPrint() {
    this.dialogRef.close({rows: this.selectedRow, cols: this.selectedColumn, barcodeType: this.selectedBarcodeType});
  }
}
