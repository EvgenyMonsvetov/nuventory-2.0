import {
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { c00CategoryService } from '../../services/c00category.service';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-inventory-finder-modal',
  templateUrl: './inventory-finder-modal.component.html',
  styleUrls: ['./inventory-finder-modal.component.scss'],
  providers: [
    c00CategoryService
  ],
  preserveWhitespaces: true
})
export class InventoryFinderModalComponent implements OnInit {

  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('companyModal') companyModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<InventoryFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private c00CategoryService: c00CategoryService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.cols = [
        { field: 'C00_NAME',             header: _lang('Nuventory Category') },
        { field: 'C00_DESCRIPTION',      header: _lang('CA1_DESCRIPTION') },
        { field: 'C00_CREATED_ON',       header: _lang('#CreatedOn#'), width: 180  },
        { field: 'C00_CREATED_BY_NAME',  header: _lang('#CreatedBy#')  },
        { field: 'C00_MODIFIED_ON',      header: _lang('#ModifiedOn#'), width: 180 },
        { field: 'C00_MODIFIED_BY_NAME', header: _lang('#ModifiedBy#') }
    ];

    this.setPage({offset: 0});
  }

   setPage(pageInfo) {
      this.loading = true;
      this.page.pageNumber = pageInfo.offset;

      this.c00CategoryService.getItems(this.page).subscribe(result => {

          this.page.totalElements = result.count;
          this.page.totalPages    = result.count / this.page.size;
          this.items = result.data;
          if (this.data.C00_ID) {
              this.selected = result.data.filter( item => {
                  return item['C00_ID'] === this.data.C00_ID;
              })[0];
          }
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.loading = false;
      },
      error => {
          this.loading = false;
          this.toastr.error(_lang('Inventory categories') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
          });
      });
    }

    getContentDialogHeight(): number {
      return (this.companyModal.nativeElement.offsetHeight ) -
                (this.header.nativeElement.offsetHeight +
                 this.footer.nativeElement.offsetHeight);
    }

    getScrollHeight(): number {
      return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
    }

  closeDialog() {
      this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
      this.selected = record;
      this.closeDialog();
  }
}