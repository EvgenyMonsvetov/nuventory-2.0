import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryFinderModalComponent } from './inventory-finder-modal.component';

describe('InventoryFinderModalComponent', () => {
  let component: InventoryFinderModalComponent;
  let fixture: ComponentFixture<InventoryFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
