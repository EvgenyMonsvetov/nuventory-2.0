import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { Page } from '../../model/page';

@Component({
  selector: 'app-companies-checkbox-dropdown',
  templateUrl: './companies-checkbox-dropdown.component.html',
  styleUrls: ['./companies-checkbox-dropdown.component.scss']
})
export class CompaniesCheckboxDropdownComponent implements OnInit, OnDestroy {
  private companies: Array<any> = [];

  @Input()  selectedCompanies: Array<any> = [];
  @Output() selectedCompaniesChange = new EventEmitter();

  selectionChanged(value) {
    this.selectedCompaniesChange.emit(value);
  }

  private subscription: Subscription;

  constructor(private companyService: CompanyService,
              private toastr: ToastrService) { }

  ngOnInit() {
    if (this.subscription)
      this.subscription.unsubscribe();

    this.selectedCompanies = this.selectedCompanies.map( (CO1_ID) => {
        return parseInt(CO1_ID);
    });

    this.subscription = this.companyService.getCompanies(new Page()).subscribe(res => {
      this.companies = res.data.map( (company)=> {
          company['CO1_ID'] = parseInt(company['CO1_ID']);
          return company;
      });
    },
    error => {
      this.toastr.error(_lang('Companies') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  ngOnDestroy() {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

  selectAllClickHandler() {
      let selectedAll = [];
      this.companies.forEach(function(item) {
        selectedAll.push(item['CO1_ID']);
      });
      this.selectedCompanies = selectedAll;
      this.selectedCompaniesChange.emit(selectedAll);
  }

  deselectAllClickHandler() {
    this.selectedCompanies = [];
    this.selectedCompaniesChange.emit([]);
  }
}
