import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { CompaniesCheckboxDropdownComponent } from './companies-checkbox-dropdown.component';
import { CompanyService } from '../../services/company.service';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ CompaniesCheckboxDropdownComponent ],
    exports: [ CompaniesCheckboxDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
    providers: [
        CompanyService
    ]
})
export class CompaniesCheckboxDropdownModule { }
