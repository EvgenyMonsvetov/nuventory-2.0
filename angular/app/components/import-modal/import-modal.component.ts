import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { UploadEvent, FileSystemFileEntry } from 'ngx-file-drop';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LangPipe } from '../../views/pipes/lang.pipe';
import { ToastrService } from 'ngx-toastr';
import { permissions } from '../../permissions';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { FileUploadService } from '../../services/file-upload.service';
import {ConfirmationService} from "primeng/api";

@Component({
  selector: 'app-import-modal',
  templateUrl: './import-modal.component.html',
  styleUrls: ['./import-modal.component.scss'],
  providers: [
    FileUploadService,
    ConfirmationService
  ],
  preserveWhitespaces: true
})
export class ImportModalComponent implements OnInit {
  private permissions = permissions;

  fileToUpload: File = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('importModal') importModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('actionbar')    actionbar:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;

  constructor(public dialogRef: MatDialogRef<ImportModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private lang: LangPipe,
              public dialog: MatDialog,
              private confirmationService: ConfirmationService,
              private fileUploadService: FileUploadService,
              private toastr: ToastrService) { }

  ngOnInit() {

    setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';});
  }

  getContentDialogHeight(): number{
    return (this.importModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight + 12);
  }

  getScrollHeight(): number{
    return this.getContentDialogHeight() - (this.actionbar.nativeElement.offsetHeight + 22 );
  }

  public dropped(event: UploadEvent) {
    if (event.files && event.files[0]) {
      const droppedFile = event.files[0];
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          if (this.isFileAllowed(file.name)) {
            this.fileToUpload = file;
          } else {
            this.fileToUpload = null;
          }
        });
      }
    }
  }

  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    const allowedFiles = ['.xls', 'xlsx', '.csv'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    return isFileAllowed;
  }

  handleFileInput(files: FileList) {
    if (files && files.item(0)) {
      const file = files.item(0);
      if (this.isFileAllowed(file.name)) {
        this.fileToUpload = file;
      } else {
        this.fileToUpload = null;
        this.displayErrorDialog(_lang('Unsupported format'));
      }
    }
  }

  uploadFile() {
    this.fileUploadService.uploadFile(this.fileToUpload, 'xls', this.fileToUpload['name'], this.data['CO1_ID'], this.data['LO1_IDs'] ).subscribe( result => {
      if (result && result.data && result.data.success === true ) {
        this.closeDialog(this.fileToUpload['name']);
      } else {
        this.displayErrorDialog(result.data.errors);
        this.toastr.error(_lang('File uploading error occured'), _lang('Service Error'), {
          timeOut: 3000,
        });
      }
    });
  }

  openLanguageEditor() {
    let dialogRef = this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('File Import Form'),
        componentRef: this,
        data: [],
        componentName: 'ImportModalComponent'
      },
    });
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  displayErrorDialog(message: string) {
    this.confirmationService.confirm({
      message: message,
      rejectVisible: false
    });
  }
}
