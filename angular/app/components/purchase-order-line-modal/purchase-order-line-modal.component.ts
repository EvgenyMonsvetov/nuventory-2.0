import { AfterViewInit, Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { PurchaseOrderLineService } from '../../services/purchase-order-line.service';

@Component({
  selector: 'app-purchase-order-line-modal',
  templateUrl: './purchase-order-line-modal.component.html',
  styleUrls: ['./purchase-order-line-modal.component.scss'],
  providers: [
    PurchaseOrderLineService,
  ],
  preserveWhitespaces: true
})
export class PurchaseOrderLineModalComponent implements OnInit, AfterViewInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[];
  items: any[];

  private loading = true;
  private record: any = null;
  private dialogContentHeight: string = '0px';

  @ViewChild('purchaseOrderLineModal')  purchaseOrderLineModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('actionbar')     actionbar: ElementRef;

  constructor(public dialogRef: MatDialogRef<PurchaseOrderLineModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public  dialog: MatDialog,
              private purchaseOrderLineService: PurchaseOrderLineService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.record = this.purchaseOrderLineService.initialize();
    this.getData();
  }

  getData() {
    this.purchaseOrderLineService.getById(this.data.PO2_ID).subscribe( result => {
          if (result[0]) {
            let data = result[0];
            this.record = data;
          }
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('PO Line') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  ngAfterViewInit() {
    setTimeout(() => this.dialogContentHeight = this.getContentDialogHeight() + 'px', 0);
  }

  getContentDialogHeight(): number {
    return (this.purchaseOrderLineModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight + this.actionbar.nativeElement.offsetHeight + 25);
  }

  closeDialog(value) {
    this.dialogRef.close(value);
  }

  openLanguageEditor() {
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Purchase Order Line Form'),
        componentRef: this,
        data: [],
        componentName: 'PurchaseOrderLineModalComponent'
      },
    });
  }

}
