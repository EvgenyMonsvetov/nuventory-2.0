import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderLineModalComponent } from './purchase-order-line-modal.component';

describe('PurchaseOrderLineModalComponent', () => {
  let component: PurchaseOrderLineModalComponent;
  let fixture: ComponentFixture<PurchaseOrderLineModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderLineModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderLineModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
