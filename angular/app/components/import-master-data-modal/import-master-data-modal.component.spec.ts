import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportMasterDataModalComponent } from './import-master-data-modal.component';

describe('ImportMasterDataModalComponent', () => {
  let component: ImportMasterDataModalComponent;
  let fixture: ComponentFixture<ImportMasterDataModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportMasterDataModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportMasterDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
