import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {_lang} from "../../pipes/lang";
import {CompanyService} from "../../services/company.service";
import {ToastrService} from "ngx-toastr";
import {Page} from "../../model/page";
import {ShopService} from "../../services/shop.service";
import {MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-import-master-data-modal',
  templateUrl: './import-master-data-modal.component.html',
  styleUrls: ['./import-master-data-modal.component.scss'],
  providers: [
    CompanyService,
    ShopService
  ],
  preserveWhitespaces: true
})
export class ImportMasterDataModalComponent implements OnInit {

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('importMasterDataModal') importMasterDataModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;

  private loading = false;
  private companiesDP: any[] = [];
  private shopsDP: any[] = [];
  private imported: boolean = false;

  constructor(private companyService: CompanyService,
              private shopService: ShopService,
              private toastr: ToastrService,
              public dialogRef: MatDialogRef<ImportMasterDataModalComponent>) { }

  ngOnInit() {
    this.getCompany();
    setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
      this.dialogContentHeight = this.getContentDialogHeight() + 'px';});
  }

  getContentDialogHeight(): number {
    return (this.importMasterDataModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight();
  }

  closeDialog() {
    this.dialogRef.close(this.imported);
  }

  getCompany() {
      this.companyService.getCompanies(new Page()).subscribe(res => {
            this.companyHandler(res.data);
            this.getLocation();
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('Companies') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
  }

  companyHandler(data: any[]) {
    this.companiesDP = [];
    let me = this;
    data.forEach( record => {
      let newItem = {};
      newItem['CO1_ID'] = record['CO1_ID'];
      newItem['CO1_SHORT_CODE'] = record['CO1_SHORT_CODE'];
      newItem['CO1_NAME'] = record['CO1_NAME'];
      me.companiesDP.push(newItem);
    });
  }

  getLocation() {
      this.shopService.getAll().subscribe(res => {
            this.locationHandler(res.data)
          },
          error => {
            this.loading = false;
            this.toastr.error(_lang('Locations') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
            });
          });
  }

  locationHandler(data: any[]) {
    this.shopsDP = [];
    let me = this;

    data.forEach( record => {
      let newItem = {};
      newItem['CO1_ID'] = record['CO1_ID'];
      newItem['LO1_ID'] = record['LO1_ID'];
      newItem['LO1_SHORT_CODE'] = record['LO1_SHORT_CODE'];
      newItem['LO1_NAME'] = record['LO1_NAME'];
      newItem['MD1_ACTIVE'] = 0;
      me.shopsDP.push(newItem);
    });

    this.loading = false;
  }

  importChange() {
    this.imported = true;
  }

}
