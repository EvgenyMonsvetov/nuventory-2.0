import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatAutocompleteModule, MatButtonModule, MatFormFieldModule, MatInputModule,
    MatProgressSpinnerModule
} from '@angular/material';
import { MasterCategoryLiveSearchComponent } from './master-category-live-search.component';
import { SharedModule } from '../../modules/shared.module';

@NgModule({
    imports: [
        CommonModule,
        MatInputModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        SharedModule
    ],
    declarations: [ MasterCategoryLiveSearchComponent ],
    exports: [ MasterCategoryLiveSearchComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class MasterCategoryLiveSearchModule { }
