import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterCategoryLiveSearchComponent } from './master-category-live-search.component';

describe('MasterCategoryLiveSearchComponent', () => {
  let component: MasterCategoryLiveSearchComponent;
  let fixture: ComponentFixture<MasterCategoryLiveSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterCategoryLiveSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterCategoryLiveSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
