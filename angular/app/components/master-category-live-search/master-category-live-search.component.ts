import { Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MasterCategoriesService } from '../../services/master-categories.service';
import { Page } from '../../model/page';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { _lang } from '../../pipes/lang';
import { MatAutocompleteTrigger } from '@angular/material';

@Component({
  selector: 'app-master-category-live-search',
  templateUrl: './master-category-live-search.component.html',
  styleUrls: ['./master-category-live-search.component.scss'],
  providers: [ MasterCategoriesService ]
})
export class MasterCategoryLiveSearchComponent implements OnDestroy {
  public categoryValue: string = '';

  @ViewChild('mcategory') mc: ElementRef;
  @ViewChild('mcategory', { read: MatAutocompleteTrigger })
  autoComplete: MatAutocompleteTrigger;

  @Input()
  get category() {
    return this.categoryValue;
  }

  @Output() categoryChange = new EventEmitter();
  @Output() inputChange = new EventEmitter();

  filteredArray: any[] = [];
  isLoading = false;
  private page = new Page();
  sub: Subscription;
  myControl = new FormControl();

  constructor(private toastr: ToastrService,
              private masterCategoriesService: MasterCategoriesService) {}

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  set category(val) {
    this.categoryValue = val;
    this.mc.nativeElement.value = val;
    this.inputChange.emit(this.categoryValue);
  }


  search(value) {
    this.category = value;
    this.page.filterParams['CA0_NAME'] = value;
    if (value.length > 0) {
      this.getItems();
    } else {
      this.filteredArray = [];
    }
  }

  keyDown(keyCode) {
    if (keyCode === 13) {
      this.categoryChange.emit(this.categoryValue);
      this.autoComplete.closePanel();
    }
  }

  getItems() {
    this.isLoading = true;

    if (this.sub) {
      this.sub.unsubscribe();
    }

    this.sub = this.masterCategoriesService.getItems(this.page).subscribe(res => {
          if (res && res.data) {
            this.filteredArray = res.data;
          } else {
            this.filteredArray = [];
          }

          this.isLoading = false;
        },
        error => {
          this.isLoading = false;
          this.toastr.error(_lang('Category') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

}
