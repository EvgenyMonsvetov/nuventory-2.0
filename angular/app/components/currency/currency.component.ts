import { Component, Input } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'currency',
  templateUrl: './currency.component.html',
  providers: [CurrencyPipe]
})
export class CurrencyComponent {


  _value: string = '';
  get value(): string {
    return this._value;
  }

  @Input('value')
  set value(v: string) {
    this._value = v;
    this.updateText();
  }

  @Input() precision:  number = 2;

  private txt: string = '';
  private align: string = 'right';

  constructor(
      private currencyPipe: CurrencyPipe
  ) { }

  updateText() {
    this.txt = this.currencyPipe.transform(this.value, 'USD', 'symbol', '1.' + this.precision + '-' + this.precision);
  }

}
