import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyComponent } from './currency.component';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [ CurrencyComponent ],
    exports: [ CurrencyComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class CurrencyModule { }
