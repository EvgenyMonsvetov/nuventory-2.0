import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimezonesDropdownComponent } from './timezones-dropdown.component';

describe('TimezonesDropdownComponent', () => {
  let component: TimezonesDropdownComponent;
  let fixture: ComponentFixture<TimezonesDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimezonesDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimezonesDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
