import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TimezonesService } from '../../services/timezones.service';

@Component({
  selector: 'app-timezones-dropdown',
  templateUrl: './timezones-dropdown.component.html',
  styleUrls: ['./timezones-dropdown.component.scss']
})
export class TimezonesDropdownComponent implements OnInit {
  @Input() activeTimezone: number;
  @Output() notify: EventEmitter<number> = new EventEmitter<number>();

  private timezones: any[] = [];

  constructor(private timezonesService: TimezonesService) { }

  ngOnInit() {
    this.timezones = this.timezonesService.getStoreTimezones();
  }

  onChange(value){
    this.notify.emit(value);
  }
}
