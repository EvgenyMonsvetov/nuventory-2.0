import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ImportModalComponent } from '../import-modal/import-modal.component';
import { _lang } from '../../pipes/lang';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-import-master-data',
  templateUrl: './import-master-data.component.html',
  styleUrls: ['./import-master-data.component.scss']
})
export class ImportMasterDataComponent implements OnInit {
  private scrollHeightUploadLocations: string = '50px';
  private scrollHeightUploadList: string = '50px';

  @ViewChild('importForm')   importForm:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('actionbarCompany')   actionbarCompany:  ElementRef;
  @ViewChild('actionbarShop')   actionbarShop:  ElementRef;
  @ViewChild('actionbarUser')   actionbarUser:  ElementRef;
  @ViewChild('actionbarUpload')   actionbarUpload:  ElementRef;
  @ViewChild('textUpload')   textUpload:  ElementRef;

  private loading = false;
  private selectedCompanyUpload: any = null;
  private colsUpload: any[] = [];
  private newUploadsDP: any[] = [];
  private uploadLocationDP: any[] = [];

  private companiesDP: any[] = [];
  private shopsDP: any[] = [];
  @Input() isModal: boolean = false;
  @Output() importChange = new EventEmitter();

  @Input()
  get companiesProvider() {
    return this.companiesDP;
  }

  set companiesProvider(val) {
    this.companiesDP = val;
  }

  @Input()
  get shopsProvider() {
    return this.shopsDP;
  }

  set shopsProvider(val) {
    this.shopsDP = val;
    this.buildColumns();
  }

  private form: FormGroup;

  constructor(public dialog: MatDialog,
              private elementRef: ElementRef,
              private formBuilder: FormBuilder,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.colsUpload = [
      { field: 'FILENAME', header: _lang('#Filename#'), visible: true, width: 160 },
      { field: 'CO1_NAME', header: _lang('CO1_NAME'),   visible: true, width: 160 },
      { field: 'LO1_NAME', header: _lang('Shop(s)'),    visible: true },
    ];

    this.form = this.formBuilder.group({
      checkboxes: this.buildColumns()
    });
  }

  getContentDialogHeight(): number {
    return (this.importForm.nativeElement.parentElement.parentElement.offsetHeight);
  }

  getContentHeight(): number {
    return this.getContentDialogHeight() - 10;
  }

  getScrollHeightUploadList(): number {
    return this.getContentHeight() - this.tableHeader.nativeElement.offsetHeight - this.textUpload.nativeElement.offsetHeight -
        this.actionbarUpload.nativeElement.offsetHeight - 19;
  }

  getScrollHeightUploadLocations(): number {
    return this.getContentHeight() - this.textUpload.nativeElement.offsetHeight -
        this.actionbarUpload.nativeElement.offsetHeight - 18;
  }

  chooseFile() {
    const formValue = Object.assign({}, {
      columns:  this.form.value.checkboxes.map((selected, i) => {
        return {
          id: this.uploadLocationDP[i]['LO1_ID'], name: this.uploadLocationDP[i]['LO1_NAME'], selected: selected
        }
      }).filter(v => v.selected === true)
    });

    const colsID: any[] = [];
    const colsName: any[] = [];

    formValue.columns.forEach( col => {
      colsID.push(col['id']);
      colsName.push(col['name']);
    });

    const selectedShopIDs: string = colsID.join(',');
    const selectedShops: string = colsName.join(',');

    let dialogRef = this.dialog.open(ImportModalComponent, {
      width: window.innerWidth - 100 + 'px',
      height: window.innerHeight - 100 + 'px',
      role: 'dialog',
      data: {title: _lang('File Upload'), componentRef: this.elementRef, CO1_ID: this.selectedCompanyUpload, LO1_IDs: selectedShopIDs},
    });

    dialogRef.beforeClose().subscribe( result => {
      if (result) {
        const company = this.companiesDP.filter(c => c.CO1_ID === this.selectedCompanyUpload);
        const CO1_NAME = company[0] && company[0]['CO1_NAME'] ? company[0]['CO1_NAME'] : '';
        this.newUploadsDP.push({FILENAME: result, CO1_NAME: CO1_NAME, LO1_NAME: selectedShops});
        this.toastr.success(
            _lang('Parts successfully imported.'),
            _lang('IMPORT SUCCESS'), {
              timeOut: 3000,
            });
        this.importChange.emit(true);
      }
    });
  }

  companyComboChangeHandler(value) {
    this.selectedCompanyUpload = value;
    this.uploadLocationDP = [];
    let me = this;
    if (value) {
      this.shopsDP.forEach(function(item) {
        if (item['CO1_ID'] === value) {
          me.uploadLocationDP.push(item);
        }
      });
    }

    this.form = this.formBuilder.group({
      checkboxes: this.buildColumns()
    });

    setTimeout(() => {this.scrollHeightUploadList = this.getScrollHeightUploadList() + 'px';
      this.scrollHeightUploadLocations = this.getScrollHeightUploadLocations() + 'px';}, 100);
  }

  get checkboxes() {
    return this.form.get('checkboxes');
  };

  buildColumns() {
    const arr = this.uploadLocationDP.map(location => {
      return this.formBuilder.control(location['MD1_ACTIVE']);
    });

    return this.formBuilder.array(arr);
  }

}
