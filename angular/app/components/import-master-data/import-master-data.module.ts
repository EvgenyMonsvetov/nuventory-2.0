import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { ImportMasterDataComponent } from './import-master-data.component';
import { CompanyDropdownModule } from '../company-dropdown/company-dropdown.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        CompanyDropdownModule
    ],
    declarations: [ ImportMasterDataComponent ],
    exports: [ ImportMasterDataComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class ImportMasterDataModule { }
