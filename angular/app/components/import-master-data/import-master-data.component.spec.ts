import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportMasterDataComponent } from './import-master-data.component';

describe('ImportMasterDataComponent', () => {
  let component: ImportMasterDataComponent;
  let fixture: ComponentFixture<ImportMasterDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportMasterDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportMasterDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
