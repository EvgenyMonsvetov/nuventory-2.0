import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { MasterDataService } from '../../services/master.data.service';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { Subject } from 'rxjs/Rx';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { isNull } from 'util';
import { AuthService } from '../../auth/auth.service';
import { GroupsService } from '../../services/groups';

@Component({
  selector: 'app-master-data-modal',
  templateUrl: './master-data-modal.component.html',
  styleUrls: ['./master-data-modal.component.scss'],
  providers: [
    MasterDataService
  ],
  preserveWhitespaces: true
})
export class MasterDataModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;
  private cols:any = [
    { field: 'VD1_NAME',                header: _lang('VD1_NAME'),              visible: true, width: 120 },
    { field: 'MD2_ACTIVE',              header: _lang('MD2_ACTIVE'),            visible: true, width: 40 },
    { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),       visible: true, width: 100 },
    { field: 'MD1_VENDOR_PART_NUMBER',  header: _lang('MD1_VENDOR_PART_NUMBER'),visible: true, width: 100 },
    { field: 'MD1_OEM_NUMBER',          header: _lang('MD1_OEM_NUMBER'),        visible: true, width: 100 },
    { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),             visible: true, width: 100 },
    { field: 'MD1_UNIT_PRICE',          header: _lang('MD1_UNIT_PRICE'),        visible: true, width: 100 },
    { field: 'MD2_STOCK_LEVEL',         header: _lang('MD2_STOCK_LEVEL'),       visible: true, width: 50 },
    { field: 'MD2_ON_HAND_QTY',         header: _lang('MD2_ON_HAND_QTY'),       visible: true, width: 50 },
    { field: 'MD2_AVAILABLE_QTY',       header: _lang('MD2_AVAILABLE_QTY'),     visible: true, width: 50 },
    { field: 'MD2_MIN_QTY',             header: _lang('MD2_MIN_QTY'),           visible: true, width: 50 },
    { field: 'MD2_RACK',                header: _lang('MD2_RACK'),              visible: true, width: 100 },
    { field: 'UM1_PRICE_NAME',          header: _lang('UM1_PRICE_NAME'),        visible: true, width: 100 },
    { field: 'C00_NAME',                header: _lang('Nuventory Category'),    visible: true, width: 100 },
    { field: 'CA1_NAME',                header: _lang('Company Category'),      visible: true, width: 100 },
    { field: 'GL1_NAME',                header: _lang('GL1_NAME'),              visible: true, width: 100 },
    { field: 'MD1_UPC1',                header: _lang('MD1_UPC1'),              visible: true, width: 100 },
    { field: 'MD1_BODY',                header: _lang('MD1_BODY'),              visible: true, width: 50 },
    { field: 'MD1_DETAIL',              header: _lang('MD1_DETAIL'),            visible: true, width: 50 },
    { field: 'MD1_PAINT',               header: _lang('MD1_PAINT'),             visible: true, width: 50 }
  ];

  items: any[] = [];

  private loading = false;
  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;

  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('masterDataModal') masterDataModal: ElementRef;
  @ViewChild('header')          header:  ElementRef;
  @ViewChild('content')         content: ElementRef;
  @ViewChild('footer')          footer:  ElementRef;
  @ViewChild('tableHeader')     tableHeader:  ElementRef;
  @ViewChild('tableFilter')     tableFilter:  ElementRef;
  @ViewChild('paginator')       paginator:  ElementRef;

  /** control for the selected filters */
  public vendorCtrl: FormControl = new FormControl();
  public vendorFilterCtrl: FormControl = new FormControl();
  public unitCtrl: FormControl = new FormControl();
  public unitFilterCtrl: FormControl = new FormControl();
  public masterCategoriesCtrl: FormControl = new FormControl();
  public masterCategoriesFilterCtrl: FormControl = new FormControl();
  public categoriesCtrl: FormControl = new FormControl();
  public categoriesFilterCtrl: FormControl = new FormControl();
  /** Subject that emits when the component has been destroyed. */
  private _onDestroyVendor = new Subject<void>();
  private _onDestroyUnit = new Subject<void>();
  private _onDestroyMasterCategory = new Subject<void>();
  private _onDestroyCategory = new Subject<void>();
  /** list of companies filtered by search keyword */
  public filteredVendors: any[] = [];
  public filteredUnits: any[] = [];
  public filteredMasterCategories: any[] = [];
  public filteredCategories: any[] = [];
  /** filter object with selected filters properties */
  private filter: any = {};

  private flagsDP: any[] = [];
  private stockLevelsDP: any[] = [];

  private vendors: Array<object> = [];
  private units: Array<object> = [];
  private masterCategories: Array<object> = [];
  private categories: Array<object> = [];

  constructor(public dialogRef: MatDialogRef<MasterDataModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private masterDataService: MasterDataService,
              private toastr: ToastrService,
              private auth: AuthService) {

    this.page.size = this.size;
    this.page.pageNumber = 0;
    this.page.filterParams = this.data.filters;
    if(this.data.url){
      this.page.url = this.data.url;
    }
  }

  ngOnInit() {
    this.flagsDP = (this.auth.accessLevel === GroupsService.GROUP_STANDARD_USER ) ? [
      { label: _lang('Yes'),          value: '1'},
    ] : [
      { label: _lang('#AllRecords#'), value: ''},
      { label: _lang('Yes'),          value: '1'},
      { label: _lang('No'),           value: '0'}
    ];

    this.stockLevelsDP = [
      { label: _lang('#AllRecords#'), value: ''},
      { label: '0', value: '0'},
      { label: '1', value: '1'},
      { label: '2', value: '2'},
      { label: '3', value: '3'}
    ];

    this.vendorFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyVendor))
        .subscribe(() => {
          this.filterVendors();
        });
    this.unitFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyUnit))
        .subscribe(() => {
          this.filterUnits();
        });
    this.masterCategoriesFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyMasterCategory))
        .subscribe(() => {
          this.filterMasterCategories();
        });
    this.categoriesFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroyCategory))
        .subscribe(() => {
          this.filterCategories();
        });

    this.masterDataService.preloadData().subscribe( data => {
      this.prepareData(data);
    });
  }

  private filterVendors() {
    if (!this.vendors) {
      return;
    }
    // get the search keyword
    let search = this.vendorFilterCtrl.value;
    if (!search) {
      this.filteredVendors = this.vendors;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the vendors
    this.filteredVendors = this.vendors.filter(vendor => vendor['VD1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterUnits() {
    if (!this.units) {
      return;
    }
    // get the search keyword
    let search = this.unitFilterCtrl.value;
    if (!search) {
      this.filteredUnits = this.units;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the units
    this.filteredUnits = this.units.filter(unit => unit['UM1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterMasterCategories() {
    if (!this.masterCategories) {
      return;
    }
    // get the search keyword
    let search = this.masterCategoriesFilterCtrl.value;
    if (!search) {
      this.filteredMasterCategories = this.masterCategories;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Master Categories
    this.filteredMasterCategories = this.masterCategories.filter(unit => unit['C00_NAME'].toLowerCase().indexOf(search) > -1);
  }

  private filterCategories() {
    if (!this.categories) {
      return;
    }
    // get the search keyword
    let search = this.categoriesFilterCtrl.value;
    if (!search) {
      this.filteredCategories = this.categories;
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Master Categories
    this.filteredCategories = this.categories.filter(unit => unit['CA1_NAME'].toLowerCase().indexOf(search) > -1);
  }

  prepareData(data) {
    this.vendors = [{VD1_ID: null, VD1_NAME: _lang('#AllRecords#')}];
    let vendorsArr = data.vendors.map( vendor => {
      return {
        VD1_ID:   vendor.VD1_ID,
        VD1_NAME: vendor.VD1_NAME
      }
    }).filter( v => {
      return v.VD1_NAME && v.VD1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.VD1_NAME < b.VD1_NAME)
        return -1;
      if (a.VD1_NAME > b.VD1_NAME)
        return 1;
      return 0;
    });
    this.vendors = this.vendors.concat(vendorsArr);
    this.filterVendors();

    this.units = [{UM1_ID: null, UM1_NAME: _lang('#AllRecords#')}];
    let unitsArr = data.units.map( vendor => {
      return {
        UM1_ID:   vendor.UM1_ID,
        UM1_NAME: vendor.UM1_NAME
      }
    }).filter( u => {
      return u.UM1_NAME && u.UM1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.UM1_NAME < b.UM1_NAME)
        return -1;
      if (a.UM1_NAME > b.UM1_NAME)
        return 1;
      return 0;
    });
    this.units = this.units.concat(unitsArr);
    this.filterUnits();

    this.masterCategories = [{C00_ID: null, C00_NAME: _lang('#AllRecords#')}];
    let masterCategoriesArr = data.masterCategories.map( masterCategory => {
      return {
        C00_ID:   masterCategory.C00_ID,
        C00_NAME: masterCategory.C00_NAME
      }
    }).filter( c => {
      return c.C00_NAME && c.C00_NAME.length > 0;
    }).sort((a, b) => {
      if (a.C00_NAME < b.C00_NAME)
        return -1;
      if (a.C00_NAME > b.C00_NAME)
        return 1;
      return 0;
    });
    this.masterCategories = this.masterCategories.concat(masterCategoriesArr);
    this.filterMasterCategories();

    this.categories = [{CA1_ID: null, CA1_NAME: _lang('#AllRecords#')}];
    let categoriesArr = data.categories.map( category => {
      return {
        CA1_ID:   category.CA1_ID,
        CA1_NAME: category.CA1_NAME
      }
    }).filter( c => {
      return c.CA1_NAME && c.CA1_NAME.length > 0;
    }).sort((a, b) => {
      if (a.CA1_NAME < b.CA1_NAME)
        return -1;
      if (a.CA1_NAME > b.CA1_NAME)
        return 1;
      return 0;
    });
    this.categories = this.categories.concat(categoriesArr);
    this.filterCategories();

    this.getItems();
  }

  ngOnDestroy() {
    this._onDestroyVendor.next();
    this._onDestroyVendor.complete();
    this._onDestroyUnit.next();
    this._onDestroyUnit.complete();
    this._onDestroyMasterCategory.next();
    this._onDestroyMasterCategory.complete();
    this._onDestroyCategory.next();
    this._onDestroyCategory.complete();
  }

  getItems() {
    this.loading = true;

    this.masterDataService.getAll(this.page).subscribe(res => {
      this.items = res.data;
      this.totalRecords = res.count;

      setTimeout(() => {
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
      });

      this.loading = false;
    },
    error => {
      this.loading = false;
      this.toastr.error(_lang('Customer Data') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
        timeOut: 3000,
      });
    });
  }

  onPage(event) {
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.getItems();
  }

  getContentDialogHeight(): number {
    return (this.masterDataModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
        + this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight + 10);
  }

  closeDialog() {
    this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
    this.selected = record;
    this.closeDialog();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    if ((col === 'ORDERED_ON' || col === 'COMPLETED_ON' || col === 'MODIFIED_ON' || col === 'CREATED_ON') && value === '01-01-1970' || value === '') {
      delete this.page.filterParams[col];
    } else if (col === 'MD2_ACTIVE') {
      this.page.filterParams[col] = isNull(value ) ? null : value.toString();
    } else {
      this.page.filterParams[col] = value;
    }

    this.page.pageNumber = 0;
    this.getItems();
  }

}