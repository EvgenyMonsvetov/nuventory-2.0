import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceChangeLogModalComponent } from './price-change-log-modal.component';

describe('PriceChangeLogModalComponent', () => {
  let component: PriceChangeLogModalComponent;
  let fixture: ComponentFixture<PriceChangeLogModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceChangeLogModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceChangeLogModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
