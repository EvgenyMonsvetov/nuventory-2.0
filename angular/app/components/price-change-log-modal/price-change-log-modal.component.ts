import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LangPipe } from '../../views/pipes/lang.pipe';
import { ToastrService } from 'ngx-toastr';
import { PriceService } from '../../services/price.service';
import { Page } from '../../model/page';
import { permissions } from '../../permissions';
import { ExportModalComponent } from '../export-modal/export-modal.component';

@Component({
  selector: 'app-price-change-log-modal',
  templateUrl: './price-change-log-modal.component.html',
  styleUrls: ['./price-change-log-modal.component.scss'],
  providers: [
    PriceService
  ],
  preserveWhitespaces: true
})
export class PriceChangeLogModalComponent implements OnInit {
  public loading: boolean = true;

  @ViewChild('priceChangeLogModal')   priceChangeLogModal: ElementRef;
  @ViewChild('header')                header:  ElementRef;
  @ViewChild('actionbar')             actionbar:  ElementRef;
  @ViewChild('tableHeader')           tableHeader:  ElementRef;
  @ViewChild('footer')                footer:  ElementRef;

  private permissions = permissions;
  private cols: Array<any> = [];
  private prices: Array<any> = [];
  private scrollHeight: string = '50px';
  private dialogContentHeight: string = '0px';

  constructor(public dialogRef: MatDialogRef<PriceChangeLogModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private priceService: PriceService,
              private lang: LangPipe,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.loading = true;

    this.cols = [
      { field: 'MD1_PART_NUMBER',   header: _lang('MD1_PART_NUMBER'), visible: true, export: {visible: true, checked: true}, width: 100 },
      { field: 'MD1_DESC1',         header: _lang('MD1_DESC1'),       visible: true, export: {visible: true, checked: true}, width: 160 },
      { field: 'LG2_OLD_PRICE',     header: _lang('#OldPrice#'),      visible: true, export: {visible: true, checked: true}, width: 80, align: 'right' },
      { field: 'LG2_NEW_PRICE',     header: _lang('#NewPrice#'),      visible: true, export: {visible: true, checked: true}, width: 80, align: 'right' },
      { field: 'LG2_CREATED_ON',    header: _lang('LG1_CREATED_ON'),  visible: true, export: {visible: true, checked: true}, width: 100 },
    ];

    let page = new Page();
    page.filterParams['MD1_ID'] = this.data.MD1_ID;

    this.priceService.getItems(page).subscribe(res => {
          this.prices = res.data;
          setTimeout(() => {this.scrollHeight = this.getScrollHeight() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          }, 100);
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Prices') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  getContentDialogHeight(): number {
    return this.priceChangeLogModal.nativeElement.offsetHeight - (this.header.nativeElement.offsetHeight +
        this.actionbar.nativeElement.offsetHeight + this.footer.nativeElement.offsetHeight) + 12;
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - this.tableHeader.nativeElement.offsetHeight - 2;
  }

  exportClick() {
    let dialogRef = this.dialog.open(ExportModalComponent, {
      width: 400 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {cols: this.cols},
    });

    dialogRef.beforeClose().subscribe( columns => {
      if (columns) {
        let page = new Page();
        page.filterParams['MD1_ID'] = this.data.MD1_ID;
        page.filterParams['columns'] = columns;

        this.priceService.downloadXlsx(page).subscribe( result => {
              this.loading = false;
            },
            error => {
              this.loading = false;
              this.toastr.error(_lang('Generate xml error occured'), _lang('Service Error'), {
                timeOut: 3000,
              });
            });
      }
    })
  }

  openLanguageEditor() {
    this.closeDialog()
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth - 60 + 'px',
      height: window.innerHeight - 40 + 'px',
      role: 'dialog',
      data: {
        title: _lang('Price Change Log'),
        componentRef: this,
        data: [],
        componentName: 'PriceChangeLogModalComponent'
      },
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
