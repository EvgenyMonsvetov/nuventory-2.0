import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-um-dropdown',
  templateUrl: './um-dropdown.component.html'
})
export class UmDropdownComponent{
  private umData: Array<any> = [];
  private disabledField: boolean = false;

  @Output() change = new EventEmitter();
  @Input()  field: string = '';
  @Input()  name: string = '';
  @Input()  materialDP: boolean = true;

  onChange(event) {
    const filteredItems = this.umData.filter(c => c['UM1_ID'] === this.field);
    const selectedItem = filteredItems && filteredItems.length ? filteredItems[0] : null;
    this.change.emit(selectedItem);
  }

  @Input()
  get dataProvider() {
    return this.umData;
  }

  set dataProvider(val) {
    this.umData = val;
  }

  @Input()
  get disabled(){
    return this.disabledField;
  }

  set disabled(val) {
    this.disabledField = val;
  }

  constructor() { }

}
