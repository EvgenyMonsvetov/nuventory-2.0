import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { UmDropdownComponent } from './um-dropdown.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ UmDropdownComponent ],
    exports: [ UmDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]

})
export class UmDropdownModule { }
