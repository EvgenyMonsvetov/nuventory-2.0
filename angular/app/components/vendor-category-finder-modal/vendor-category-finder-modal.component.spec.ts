import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorCategoryFinderModalComponent } from './vendor-category-finder-modal.component';

describe('VendorCategoryFinderModalComponent', () => {
  let component: VendorCategoryFinderModalComponent;
  let fixture: ComponentFixture<VendorCategoryFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorCategoryFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorCategoryFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
