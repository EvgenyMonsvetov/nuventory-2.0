import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { MasterCategoriesService } from '../../services/master-categories.service';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-vendor-category-finder-modal',
  templateUrl: './vendor-category-finder-modal.component.html',
  styleUrls: ['./vendor-category-finder-modal.component.scss'],
  providers: [
    MasterCategoriesService,
  ],
  preserveWhitespaces: true
})
export class VendorCategoryFinderModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = true;
  private page = new Page();
  private totalRecords: number = 0;
  private size: number = 50;
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('vendorCategoryModal') vendorCategoryModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('table')         table:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('footer')        footer:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('tableFilter')   tableFilter:  ElementRef;


  constructor(public dialogRef: MatDialogRef<VendorCategoryFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private masterCategoriesService: MasterCategoriesService,
              private toastr: ToastrService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
  }

  ngOnInit() {
    this.cols = [
      { field: 'CA0_NAME',             header: _lang('CA0_NAME'),            visible: true, width: 120 },
      { field: 'CA0_DESCRIPTION',      header: _lang('CA0_DESCRIPTION'),     visible: true, width: 180 },
      { field: 'CA0_CREATED_ON',       header: _lang('#CreatedOn#'),         visible: true, width: 120 },
      { field: 'CA0_CREATED_BY_NAME',  header: _lang('#CreatedBy#'),         visible: true, width: 160 },
      { field: 'CA0_MODIFIED_ON',      header: _lang('#ModifiedOn#'),        visible: true, width: 120 },
      { field: 'CA0_MODIFIED_BY_NAME', header: _lang('#ModifiedBy#'),        visible: true, width: 160 }
    ];

    this.setPage();
  }

  setPage() {
    this.loading = true;

    this.masterCategoriesService.getItems(this.page).subscribe(result => {

          this.page.totalElements = result.count;
          this.page.totalPages    = result.count / this.page.size;
          this.items = result.data;

          if (this.data.CA0_ID) {
            this.selected = result.data.filter( item => {
              return item['CA0_ID'] === this.data.CA0_ID;
            })[0];
          }

          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.totalRecords = result.count;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Vendor Categories') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  onPage(event) {
    this.loading = true;
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.setPage();
  }

  buttonPress(e, col, value) {
    if ( e.keyCode === 13 ) {
      this.filterChanged(col, value);
    }
  }

  filterChanged(col, value) {
    this.loading = true;
    this.page.filterParams[col] = value;
    this.page.pageNumber = 0;
    this.setPage();
  }

  getContentDialogHeight(): number {
    return (this.vendorCategoryModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
        this.tableFilter.nativeElement.offsetHeight + this.paginator.nativeElement.parentElement.parentElement.offsetHeight + 2);
  }

  closeDialog() {
    this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
    this.selected = record;
    this.closeDialog();
  }

}
