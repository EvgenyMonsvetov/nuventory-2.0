import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { GroupsService } from '../../services/groups';

@Component({
  selector: 'app-groups-checkbox-dropdown',
  templateUrl: './groups-checkbox-dropdown.component.html',
  styleUrls: ['./groups-checkbox-dropdown.component.scss']
})
export class GroupsCheckboxDropdownComponent implements OnInit, OnDestroy {

  private groups: Array<any> = [];

  @Input()  selectedGroups: Array<any> = [];
  @Output() selectedGroupsChange = new EventEmitter();

  selectionChanged(value) {
    this.selectedGroupsChange.emit(value);
  }

  private subscription: Subscription;

  constructor(private groupsService: GroupsService,
              private toastr: ToastrService) { }

  ngOnInit() {
    if (this.subscription)
      this.subscription.unsubscribe();

    this.subscription = this.groupsService.getAll().subscribe(res => {
          this.groups = res.data;
        },
        error => {
          this.toastr.error(_lang('Groups') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
