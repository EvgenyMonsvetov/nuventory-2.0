import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared.module';
import { GroupsCheckboxDropdownComponent } from './groups-checkbox-dropdown.component';
import { GroupsService } from '../../services/groups';

@NgModule({
    imports: [
        CommonModule,
        SharedModule
    ],
    declarations: [ GroupsCheckboxDropdownComponent ],
    exports: [ GroupsCheckboxDropdownComponent ],
    schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
    providers: [
        GroupsService
    ]
})
export class GroupsCheckboxDropdownModule { }
