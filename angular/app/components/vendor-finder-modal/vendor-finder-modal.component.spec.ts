import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorFinderModalComponent } from './vendor-finder-modal.component';

describe('VendorFinderModalComponent', () => {
  let component: VendorFinderModalComponent;
  let fixture: ComponentFixture<VendorFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
