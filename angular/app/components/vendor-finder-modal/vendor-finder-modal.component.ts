import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';
import { VendorService } from '../../services/vendor.service';

@Component({
  selector: 'app-vendor-finder-modal',
  templateUrl: './vendor-finder-modal.component.html',
  styleUrls: ['./vendor-finder-modal.component.scss'],
  providers: [
    VendorService
  ],
  preserveWhitespaces: true
})
export class VendorFinderModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];
  title: string = _lang('Assign Vendor');

  private loading = false;
  private page = new Page();
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('vendorModal')  vendorModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<VendorFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private vendorService: VendorService,
              private toastr: ToastrService) { }

  ngOnInit() {
    if (this.data.title && this.data.title !== '')
      this.title = this.data.title;

    this.cols = [
      { field: 'CO1_NAME',        header: _lang('CO1_NAME'),            visible: true, width: 200 },
      { field: 'VD1_SHORT_CODE',  header: _lang('VD1_SHORT_CODE'),      visible: true, width: 120 },
      { field: 'VD1_CUSTOMER_NO', header: _lang('VD1_CUSTOMER_NO'),     visible: true, width: 250 },
      { field: 'VD1_NAME',        header: _lang('VD1_NAME'),            visible: true, width: 200 },
      { field: 'VD1_ADDRESS1',    header: _lang('#AddressStreet1#'),    visible: true, width: 250 },
      { field: 'VD1_ADDRESS2',    header: _lang('#AddressStreet2#'),    visible: true, width: 250 },
      { field: 'VD1_CITY',        header: _lang('#AddressCity#'),       visible: true, width: 200 },
      { field: 'VD1_STATE',       header: _lang('#AddressState#'),      visible: true, width: 72 },
      { field: 'VD1_ZIP',         header: _lang('#AddressPostalCode#'), visible: true, width: 110 },
      { field: 'VD1_PHONE',       header: _lang('#Phone#'),             visible: true, width: 140 },
      { field: 'VD1_FAX',         header: _lang('#Fax#'),               visible: true, width: 140 },
      { field: 'VD1_EMAIL',       header: _lang('#Email#'),             visible: true, width: 200 },
      { field: 'VD1_URL',         header: _lang('#URL#'),               visible: true, width: 200 },
      { field: 'VD1_NO_INVENTORY',header: _lang('VD1_NO_INVENTORY'),    visible: true, width: 135 },
      { field: 'VD1_CREATED_ON',  header: _lang('#CreatedOn#'),         visible: true, width: 160 },
      { field: 'CREATED_BY',      header: _lang('#CreatedBy#'),         visible: true, width: 140 },
      { field: 'VD1_MODIFIED_ON', header: _lang('#ModifiedOn#'),        visible: true, width: 160 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),        visible: true, width: 140 }
    ];

    this.setPage({offset: 0});
  }

  setPage(pageInfo) {
    this.loading = true;
    this.page.pageNumber = pageInfo.offset;

    this.vendorService.getVendors(this.page).subscribe(result => {
          this.page.totalElements = result.data['count'];
          this.page.totalPages    = result.data['count'] / this.page.size;
          var items = result.data['items'];
          if(this.data.hideVendors){
            items = items.filter( item => {
              return this.data.hideVendors.indexOf(item['VD1_ID']) > -1 ? false : true;
            });
          }
          this.items = items;
          if(this.data.VD1_ID){
            this.selected = result.data['items'].filter( item => {
              return item['VD1_ID'] === this.data.VD1_ID;
            })[0];
          }
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('#Vendors#') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  getContentDialogHeight(): number {
    return (this.vendorModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
  }

  closeDialog() {
    this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
    this.selected = [record];
    this.closeDialog();
  }

}
