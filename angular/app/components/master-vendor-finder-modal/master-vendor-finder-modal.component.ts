import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MasterVendorService } from '../../services/master-vendor.service';
import { ToastrService } from 'ngx-toastr';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-master-vendor-finder-modal',
  templateUrl: './master-vendor-finder-modal.component.html',
  styleUrls: ['./master-vendor-finder-modal.component.scss'],
  providers: [
    MasterVendorService
  ],
  preserveWhitespaces: true
})
export class MasterVendorFinderModalComponent implements OnInit {
  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  private loading = false;
  private page = new Page();
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('masterVendorModal') masterVendorModal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<MasterVendorFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private masterVendorService: MasterVendorService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.cols = [
      { field: 'VD0_NAME',        header: _lang('VD0_NAME'),            visible: true, width: 250 },
      { field: 'VD0_SHORT_CODE',  header: _lang('VD0_SHORT_CODE'),      visible: true, width: 200 },
      { field: 'VD0_MANUFACTURER',header: _lang('VD0_MANUFACTURER'),    visible: true, width: 140 },
      { field: 'VD0_ADDRESS1',    header: _lang('#AddressStreet1#'),    visible: true, width: 250 },
      { field: 'VD0_ADDRESS2',    header: _lang('#AddressStreet2#'),    visible: true, width: 250 },
      { field: 'VD0_CITY',        header: _lang('#AddressCity#'),       visible: true, width: 200 },
      { field: 'VD0_STATE',       header: _lang('#AddressState#'),      visible: true, width: 72 },
      { field: 'VD0_ZIP',         header: _lang('#AddressPostalCode#'), visible: true, width: 160 },
      { field: 'VD0_PHONE',       header: _lang('#Phone#'),             visible: true, width: 160 },
      { field: 'VD0_FAX',         header: _lang('#Fax#'),               visible: true, width: 160 },
      { field: 'VD0_EMAIL',       header: _lang('#Email#'),             visible: true, width: 400 },
      { field: 'VD0_URL',         header: _lang('#URL#'),               visible: true, width: 200 },
      { field: 'CY1_NAME',        header: _lang('#AddressPostalCode#'), visible: true, width: 155 },
      { field: 'VD0_CREATED_ON',  header: _lang('#CreatedOn#'),         visible: true, width: 160 },
      { field: 'CREATED_BY',      header: _lang('#CreatedBy#'),         visible: true, width: 140 },
      { field: 'VD0_MODIFIED_ON', header: _lang('#ModifiedOn#'),        visible: true, width: 160 },
      { field: 'MODIFIED_BY',     header: _lang('#ModifiedBy#'),        visible: true, width: 140 }
    ];

    this.setPage({offset: 0});
  }

  setPage(pageInfo) {
    this.loading = true;
    this.page.pageNumber = pageInfo.offset;

    this.masterVendorService.getAll(this.page).subscribe(result => {
          this.page.totalElements = result.data['count'];
          this.page.totalPages    = result.data['count'] / this.page.size;
          this.items = result.data['currentVocabulary'];
          if(this.data.VD0_ID){
            this.selected = result.data['currentVocabulary'].filter( item => {
              return item['VD0_ID'] == this.data.VD0_ID;
            })[0];
          }
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Master vensors') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  getContentDialogHeight(): number {
    return (this.masterVendorModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight +
        this.footer.nativeElement.offsetHeight);
  }

  getScrollHeight(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
  }

  closeDialog() {
    this.dialogRef.close( this.selected );
  }

  selectRecord(record) {
    this.selected = record;
    this.closeDialog();
  }

}
