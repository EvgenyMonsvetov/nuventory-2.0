import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterVendorFinderModalComponent } from './master-vendor-finder-modal.component';

describe('MasterVendorFinderModalComponent', () => {
  let component: MasterVendorFinderModalComponent;
  let fixture: ComponentFixture<MasterVendorFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterVendorFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterVendorFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
