import {
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { permissions } from '../../permissions';
import { Page } from '../../model/page';
import { ToastrService } from 'ngx-toastr';
import { ShopService } from '../../services/shop.service';
import { _lang } from '../../pipes/lang';

@Component({
  selector: 'app-shop-finder-modal',
  templateUrl: './shop-finder-modal.component.html',
  styleUrls: ['./shop-finder-modal.component.scss'],
  providers: [
    ShopService,
  ],
  preserveWhitespaces: true
})
export class ShopFinderModalComponent implements OnInit {

  @Output() select = new EventEmitter();

  private permissions = permissions;

  cols: any[] = [];
  items: any[] = [];

  sourceUrl: '';
  filters: { [key: string]: any; } = {};
  HIGHLIGHT_LO1_ID: number = null;

  private loading = false;
  private page = new Page();
  private selected: any = null;

  private scrollHeight: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('modal')        modal: ElementRef;
  @ViewChild('header')       header:  ElementRef;
  @ViewChild('content')      content: ElementRef;
  @ViewChild('footer')       footer:  ElementRef;
  @ViewChild('tableHeader')  tableHeader:  ElementRef;

  constructor(public dialogRef: MatDialogRef<ShopFinderModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private shopService: ShopService,
              private toastr: ToastrService) {
        this.sourceUrl        = this.data.sourceUrl || null;
        this.filters          = this.data.filters;
        this.HIGHLIGHT_LO1_ID = this.data.HIGHLIGHT_LO1_ID || null;
  }

  ngOnInit() {
    this.cols = [
        { field: 'CO1_NAME',             header: _lang('CO1_NAME'), width: 170 },
        { field: 'LO1_SHORT_CODE',       header: _lang('LO1_SHORT_CODE'), width: 120 },
        { field: 'LO1_NAME',             header: _lang('LO1_NAME'), width: 170 },
        { field: 'LO1_ADDRESS1',         header: _lang('#AddressStreet1#'), width: 150 },
        { field: 'LO1_CITY',             header: _lang('#AddressCity#'), width: 120 },
        { field: 'LO1_STATE',            header: _lang('#AddressState#'), width: 100 },
        { field: 'CY1_SHORT_CODE',       header: _lang('CY1_NAME'), width: 100 },
        { field: 'LO1_ZIP',              header: _lang('#AddressPostalCode#'), width: 80 },
        { field: 'LO1_PHONE',            header: _lang('#Phone#'), width: 140 },
    ];

    this.setPage({offset: 0});
   }

   setPage(pageInfo) {
      this.loading = true;
      this.page.pageNumber = pageInfo.offset;

      if(this.filters){
          this.page.filterParams = this.filters;
      }

      this.shopService.getItems(this.page, this.sourceUrl).subscribe(result => {

          this.page.totalElements = result.count;
          this.page.totalPages    = result.count / this.page.size;
          this.items = result.data;
          if(this.HIGHLIGHT_LO1_ID){
              this.selected = result.data.filter( item => {
                  return item['LO1_ID'] === this.HIGHLIGHT_LO1_ID;
              })[0];
          }
          this.scrollHeight = this.getScrollHeight() + 'px';
          this.dialogContentHeight = this.getContentDialogHeight() + 'px';
          this.loading = false;
      },
      error => {
          this.loading = false;
          this.toastr.error(_lang('Shops') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
              timeOut: 3000,
          });
      });
    }

    getContentDialogHeight(): number {
      return (this.modal.nativeElement.offsetHeight ) -
                (this.header.nativeElement.offsetHeight +
                 this.footer.nativeElement.offsetHeight);
    }

    getScrollHeight(): number{
      return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight + 2);
    }

    closeDialog() {
      this.dialogRef.close( this.selected );
    }

    selectRecord(record) {
      this.selected = record;
      this.closeDialog();
    }
}
