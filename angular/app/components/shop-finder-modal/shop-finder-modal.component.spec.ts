import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopFinderModalComponent } from './shop-finder-modal.component';

describe('ShopFinderModalComponent', () => {
  let component: ShopFinderModalComponent;
  let fixture: ComponentFixture<ShopFinderModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopFinderModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopFinderModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
