import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../model/page';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { LangPipe } from '../../views/pipes/lang.pipe';
import { ToastrService } from 'ngx-toastr';
import { LanguageEditorModalComponent } from '../language-editor-modal/language-editor-modal.component';
import { _lang } from '../../pipes/lang';
import { permissions } from '../../permissions';
import { MasterCatalogService } from '../../services/master-catalog.service';
import { MasterVendorService } from '../../services/master-vendor.service';

@Component({
  selector: 'app-link-mcatalog-modal',
  templateUrl: './link-mcatalog-modal.component.html',
  preserveWhitespaces: true
})
export class LinkMcatalogModalComponent implements OnInit {
  private permissions = permissions;

  cols1: any[] = [];
  cols2: any[] = [];
  itemData: any[] = [];
  catalogData: any[] = [];
  vendorData: any[] = [{'VD0_ID': 0,
    'VD0_SHORT_CODE': '',
    'VD0_NAME': _lang('- All -')
  }];
  selectedVendorPart: any;
  selectedCatalogPart: any;
  selectedVendor: any = 0;
  search: string;
  currentSearchIndex = -1;

  private loading1 = false;
  private loading = false;
  private page = new Page();
  private size: number = 50;
  private totalRecords: number = 0;
  private currentPageIndex: number = 0;

  private scrollHeight1: string = '0px';
  private scrollHeight2: string = '0px';
  private emptyHeight1: string = '0px';
  private emptyHeight2: string = '0px';
  private dialogContentHeight: string = '0px';

  @ViewChild('partsModal')    partsModal: ElementRef;
  @ViewChild('header')        header:  ElementRef;
  @ViewChild('buttons')       buttons:  ElementRef;
  @ViewChild('content')       content: ElementRef;
  @ViewChild('footer')        footer:  ElementRef;
  @ViewChild('tableHeader')   tableHeader:  ElementRef;
  @ViewChild('paginator')     paginator:  ElementRef;
  @ViewChild('searchInput')   searchInput:  ElementRef;

  masterRecord = false;

  constructor(public dialogRef: MatDialogRef<LinkMcatalogModalComponent>,
              private masterVendorService: MasterVendorService,
              private masterCatalogService: MasterCatalogService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private lang: LangPipe,
              public dialog: MatDialog,
              private toastr: ToastrService) {
    this.page.size = this.size;
    this.page.pageNumber = 0;
  }

  ngOnInit() {
    this.loading = true;
    this.masterRecord = this.data.masterRecord;
    this.page.filterParams['MASTER_DATA'] = this.data.masterRecord ? 1 : 0;

    this.cols1 = [
      { field: 'MD0_ID',          header: _lang('MD0_ID'),          visible: false, width: 200 },
      { field: 'MD0_PARENT_ID',   header: _lang('MD0_PARENT_ID'),   visible: false, width: 120 },
      { field: 'ASSIGNED_TO',     header: _lang('Assigned to..'),   visible: true,  width: 200 },
      { field: 'MD0_PART_NUMBER', header: _lang('MD1_PART_NUMBER'), visible: true,  width: 70 },
      { field: 'MD0_DESC1',       header: _lang('MD1_DESC1'),       visible: true,  width: 250 },
      { field: 'MD0_UPC1',        header: _lang('MD1_UPC1'),        visible: true,  width: 80 },
      { field: 'UM1_PRICING_NAME', header: _lang('UM1_DEFAULT_PRICING'),  visible: true, width: 120 }
    ];

    this.cols2 = [
      { field: 'MD0_ID',          header: _lang('MD0_ID'),          visible: false, width: 200 },
      { field: 'VD0_SHORT_CODE',  header: _lang('VD1_SHORT_CODE'),  visible: true,  width: 120 },
      { field: 'VD0_NAME',        header: _lang('VD1_NAME'),        visible: true,  width: 200 },
      { field: 'MD0_PART_NUMBER', header: _lang('MD1_PART_NUMBER'), visible: true,  width: 70 },
      { field: 'MD0_DESC1',       header: _lang('MD1_DESC1'),       visible: true,  width: 250 },
      { field: 'MD0_UPC1',        header: _lang('MD1_UPC1'),        visible: true,  width: 80 },
      { field: 'UM1_NAME',        header: _lang('UM1_DEFAULT_PRICING'), visible: true, width: 120 }
    ];

    this.page.pageNumber = 0;
    this.page.filterParams['VD0_MANUFACTURER'] = 1;
    this.fillform();
  }

  getContentDialogHeight(): number{
    return (this.partsModal.nativeElement.offsetHeight ) -
        (this.header.nativeElement.offsetHeight + 51);
  }

  getScrollHeight1(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
        this.buttons.nativeElement.offsetHeight + 7);
  }

  getScrollHeight2(): number {
    return this.getContentDialogHeight() - (this.tableHeader.nativeElement.offsetHeight +
        this.buttons.nativeElement.offsetHeight + this.paginator.nativeElement.offsetHeight + 15);
  }

  getEmptyHeight1(): number {
    return this.getScrollHeight1() - 1;
  }

  getEmptyHeight2(): number {
    return this.getScrollHeight2() - 16;
  }

  fillform() {
    if (this.data.items && this.data.items.length > 0) {
      this.itemData = [];
      const iData: any = [];

      this.data.items.forEach(function (item) {
        if (item['MD0_ID'] && parseInt(item['MD0_ID']) > 0 && (!item['VD0_MANUFACTURER'] || parseInt(item['VD0_MANUFACTURER']) === 0)) {
          const obj: any = {};
          obj['MD0_ID'] = item['MD0_ID'];
          obj['MD0_PARENT_ID'] = null;
          obj['ASSIGNED_TO'] = null;
          obj['MD0_PART_NUMBER'] = item['MD0_PART_NUMBER'];
          obj['MD0_DESC1'] = item['MD0_DESC1'];
          obj['MD0_UPC1'] = item['MD0_UPC1'];
          obj['UM1_PRICING_NAME'] = item['UM1_PRICING_NAME'];

          iData.push(obj);
        }
      });

      this.itemData = iData;
    }

    this.getVendors();
  }

  getVendors() {
    this.masterVendorService.getAll(this.page).subscribe(res => {
          this.vendorData = this.vendorData.concat(res.data['currentVocabulary'].filter(v => v['VD0_NAME'] && v['VD0_NAME'] !== ''));
          this.getParts();
        },
        error => {
          this.toastr.error(_lang('Master Vendors') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
          this.getParts();
        });
  }

  getParts() {
    this.page.filterParams['VD0_ID'] = this.selectedVendor === 0 ? null : this.selectedVendor;
    this.selectedCatalogPart = null;

    this.masterVendorService.getAllParts(this.page).subscribe(res => {
          this.catalogData = res.data['currentVocabulary'];
          this.totalRecords = res.data['count'];

          setTimeout(() => {this.scrollHeight1 = this.getScrollHeight1() + 'px';
            this.scrollHeight2 = this.getScrollHeight2() + 'px';
            this.dialogContentHeight = this.getContentDialogHeight() + 'px';
            this.emptyHeight1 = this.getEmptyHeight1() + 'px';
            this.emptyHeight2 = this.getEmptyHeight2() + 'px';});

          this.loading = false;
        },
        error => {
          this.loading = false;
          this.toastr.error(_lang('Master Vendors parts') + ' ' + _lang('loading error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        });
  }

  onInput(value) {
    this.search = value;
    this.currentSearchIndex = -1;
    this.onSearch();
  }

  onSearch() {
    let index = 0;
    if (this.currentSearchIndex >= 0)
      index = this.currentSearchIndex  + 1;

    for (index; index < this.catalogData.length; index ++) {
      if (this.catalogData[index]) {
        if ((this.catalogData[index].MD0_PART_NUMBER && this.catalogData[index].MD0_PART_NUMBER.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.catalogData[index].MD0_DESC1 && this.catalogData[index].MD0_DESC1.toString().toLowerCase().search(this.search.toLowerCase()) !== -1) ||
            (this.catalogData[index].MD0_UPC1 && this.catalogData[index].MD0_UPC1.toString().toLowerCase().search(this.search.toLowerCase()) !== -1)) {
          this.currentSearchIndex = index;
          this.setCurrentPage(index);
          const row = 'row' + this.currentSearchIndex;

          setTimeout(() => {this.selectedCatalogPart = this.catalogData[index];
            const elem = document.getElementById(row);
            if (elem) {
              elem.scrollIntoView();
            }
          });
          break;
        }
      }
    }
  }

  onPage2(event) {
    this.page.size = event.rows;
    this.page.pageNumber = (event.first / event.rows);
    this.currentSearchIndex = 0;
    this.currentPageIndex = event.first;
    this.selectedCatalogPart = null;
  }

  setCurrentPage(index: number) {
    const n = index === 0 ? 0 : Math.ceil(index / this.page.size) - 1;
    this.currentPageIndex = this.page.size * n;
  }

  openLanguageEditor() {
    this.closeDialog()
    this.dialog.open(LanguageEditorModalComponent, {
      width: window.innerWidth-60+'px',
      height: window.innerHeight-40+'px',
      role: 'dialog',
      data: {
        title: _lang('Assign Form'),
        componentRef: this,
        data: [],
        componentName: 'LinkMcatalogModalComponent'
      },
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  onSave() {
    const parts: any = [];

    if (this.itemData) {
      this.loading1 = true;
      const cols = this.cols1;
      const m = this.masterRecord;
      this.itemData.forEach(function(item) {
        if (item['MD0_PARENT_ID']) {
          const part: any = {};
          part['MD1_ID'] = item['MD0_ID'];
          part['MD0_PARENT_ID'] = item['MD0_PARENT_ID'];
          part['MASTER_DATA'] = 1;
          parts.push(part);
        }
      });

      this.masterCatalogService.updateParts(parts).subscribe(result => {
        if (result.success) {
          this.loading1 = false;
        } else {
          this.loading1 = false;
          this.toastr.error(_lang('On save parts error occured'), _lang('Service Error'), {
            timeOut: 3000,
          });
        }
      });
    }
  }

  onAutoMatch() {
    this.loading1 = true;
    const catalogParts = this.catalogData;

    this.itemData.forEach(function(item) {
      catalogParts.forEach(function(item2) {
        if ((item.MD0_PART_NUMBER === item2.MD0_PART_NUMBER && item2.MD0_PART_NUMBER != '') ||
            (item.MD0_UPC1 === item2.MD0_UPC1 && item2.MD0_UPC1 != '')) {
          item['MD0_PARENT_ID'] = item2['MD0_ID'];
          item['ASSIGNED_TO'] = item2['VD0_SHORT_CODE'] + '-' + item2['MD0_PART_NUMBER'];
        }
      });
    });
    this.loading1 = false;
  }

  onClearSelected() {
    this.selectedVendorPart.forEach(function(item) {
      item['MD0_PARENT_ID'] = '';
      item['ASSIGNED_TO'] = '';
    });
  }

  onAssign() {
    const selectedCPart = this.selectedCatalogPart;
    this.selectedVendorPart.forEach(function(item) {
      item['MD0_PARENT_ID'] = selectedCPart['MD0_ID'];
      item['ASSIGNED_TO'] = selectedCPart['VD0_SHORT_CODE'] + '-' + selectedCPart['MD0_PART_NUMBER'];
    });
  }

}
