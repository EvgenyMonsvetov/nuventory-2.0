import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {ICustomer} from '../views/customers/customer';
import {GeneralService} from './general.service';
import {StoreService} from "./store.service";

@Injectable()
export class CustomerService extends GeneralService implements EDIServices<Observable<ICustomer>, ICustomer, number> {

    private store: StoreService;

    constructor(protected http: HttpClient) {
        super(http);

        this.store = new StoreService('customers', 'CU1_ID');
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/customers/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    save(customer: any, isCreate: boolean): Observable<any> {
        if (customer.CU1_ID === 0 || isCreate) {
            return this.create(customer);
        }
        return this.update(customer);
    }

    public load(): Observable<IResponse<any>>{
        return this.http.get<IResponse<any>>(this.apiUrl + '/customers');
    }

    public generateShortCode() {
        return this.http.get(this.apiUrl + '/customer/generate-short-code')
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getCustomers(page): Observable<IResponse<any>> {
        const current_page = page.pageNumber;
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', current_page);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/customers', { params : params });
    }

    public create(customer: any): Observable<any> {
        return this.http.post(this.apiUrl + '/customers', customer)
            .catch(this.handleError);
    }

    public update(customer: any): Observable<any> {
        const url = `${this.apiUrl + '/customers'}/${customer.CU1_ID}`;
        return this.http.put(url, customer)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/customers/' + id);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/customer/export';
        const defaultFileName = 'Customer.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    public initialize(): object {

        return {
            'CU1_ID': 0,
            'CO1_ID': null,
            'CU1_SHORT_CODE': null,
            'CU1_NAME': null,
            'CU1_ADDRESS1': null,
            'CU1_ADDRESS2': null,
            'CU1_CITY': null,
            'CU1_STATE': null,
            'CU1_ZIP': null,
            'CU1_PHONE': null,
            'CU1_EMAIL': null,
            'CU1_FAX': null,
            'CY1_ID': null,
            'CU1_DELETE_FLAG': null,
            'CU1_CREATED_BY': null,
            'CU1_CREATED_ON': null,
            'CU1_MODIFIED_BY': null,
            'CU1_MODIFIED_ON': null,
            'LO1_CREATED': null
        };
    }

    getStore(){
        return this.store;
    }
}
