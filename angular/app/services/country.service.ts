import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ICountry } from '../model/interfaces/ICountry';
import { Observable } from 'rxjs';
import { StoreService } from './store.service';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class CountryService extends GeneralService {
  private store: StoreService;

  constructor(protected http: HttpClient) {
    super(http);

    this.store = new StoreService('countries', 'CY1_ID');
  }

  getStore(): StoreService{
    return this.store;
  }

  public getAllCountries() {
    const url = `${this.apiUrl}/countries`;
    return this.http.get(url)
        .map(data => data)
        .do(data => {this.store.load(data['data']);})
        .catch(this.handleError);
  }

  public getCountries(page: any) {
    const current_page = page.pageNumber;
    let params = new HttpParams();
    params = this.applyFilters(page, params);

    params = params.append('perPage', page.size);
    params = params.append('page', current_page);
    if (!_.isUndefined(page.sortParams)) {
      for (const key of Object.keys(page.sortParams)) {
        const value = page.sortParams[key];
        params = params.append('sort[]', key + ',' + value );
      }
    }
    const url = `${this.apiUrl}/countries`;
    return this.http.get(url, { params : params })
        .map(data => data)
        .catch(this.handleError);
  }

  getById(id: number): Observable<any> {
    if (id === 0) {
      return Observable.of(this.initialize());
    } else {
      const country = this.store.getById(id);

      if (country) {
        return Observable.of(country);
      } else {
        const url = `${this.apiUrl}/countries/${id}`;
        return this.http.get(url)
            .map( data => this.extractPayload(data) )
            .do(data => this.store.save(data))
            .catch(this.handleError);
      }
    }
  }

  save(country: ICountry, isCreate: boolean): Observable<any> {
    if (country.CY1_ID === 0 || isCreate) {
      return this.create(country);
    }
    return this.update(country);
  }

  private create(country: ICountry): Observable<any> {
    return this.http.post(this.apiUrl + '/countries', country)
        .map(
            data => this.extractPayload(data)
        )
        .do(data => { if (data['data'] && data['data']['data']) this.store.add(data['data']['data']) })
        .catch(this.handleError);
  }

  private update(country: ICountry): Observable<any> {
    return this.http.post(this.apiUrl + '/countries', country)
        .map(
            data => this.extractPayload(data)
        )
        .do(data => { if (data['data'] && data['data']['data']) this.store.update(data['data']['data']) })
        .catch(this.handleError);
  }

  public delete(id: number) {
    return this.http.delete<any>(this.apiUrl + '/countries/' + id, {});
  }

  public downloadXlsx(page: any): Observable<any> {
    const apiUrl = '/countries/export';
    const defaultFileName = 'Countries.xls';
    return this.getXlsx(apiUrl, page, defaultFileName);
  }

  initialize(): any {
    // Return an initialized object
    return {
      CY1_ID: 0,
      CY1_SHORT_CODE: null,
      CY1_NAME: null,
      CY1_DELETE_FLAG: 0,
      CREATED_BY: null,
      MODIFIED_BY: null,
      CREATED_ON: null,
      MODIFIED_ON: null,
      errors: null
    };
  }
}
