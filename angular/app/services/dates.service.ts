import {Injectable} from "@angular/core";
import {DatePipe} from "@angular/common";

@Injectable()
export class DatesService {

  format( value, time: string = '00:00:00', timezone?: string)
  {
    var datePipe = new DatePipe('en-US');
    value = datePipe.transform(value, 'yyyy-MM-dd', timezone);
    value += ' ' + time;
    return value;
  }
}