import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';

@Injectable()
export class JobLineService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/job-lines/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/job-lines', { params : params });
    }

    public update(item: any): Observable<any> {
        return this.http.post(this.apiUrl + '/job-lines', item)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/job-lines/' + id, {});
    }

    initialize(): any {
        // Return an initialized object
        return {
            JT2_ID: 0,
            JT2_LINE: null,
            JT2_ORDER_QTY: 0,
            TE1_NAME: '',
            JT2_PART_NUMBER: null,
            JT2_DESC1: '',
            JT2_DESC2: '',
            UM1_NAME: '',
            JT2_UNIT_PRICE: null,
            JT1_NUMBER: null,
            JT2_STATUS_TEXT: '',
            TE1_SHORT_CODE: '',
            GL1_NAME: '',
            UM1_RECEIVE_NAME: '',
            JT2_TOTAL_VALUE: null,
            MODIFIED_ON: null,
            MODIFIED_BY: null,
            CREATED_BY: null,
            CREATED_ON: null
        };
    }
}
