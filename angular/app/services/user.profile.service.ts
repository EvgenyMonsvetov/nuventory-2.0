import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import {StoreService} from "./store.service";

@Injectable()
export class UserProfileService extends GeneralService {

    private store: StoreService;

    constructor(protected http: HttpClient) {
        super(http);

        this.store = new StoreService('userProfile', 'UP1_ID');
    }

    getStore(): StoreService{
        return this.store;
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return Observable.of(this.initialize());
        }else{
            let userProfile = this.store.getById(id);
            if(userProfile){
                return Observable.of(userProfile);
            }else{
                const url = `${this.apiUrl}/user-profiles/${id}`;
                return this.http.get(url)
                    .map(
                        data => this.extractPayload(data)
                    )
                    .do(data => this.store.save(data))
                    .catch(this.handleError);
            }
        }
    }

    getByUserId( userId ): Observable<any> {
        const url = `${this.apiUrl}/user-profiles/${userId}`;
        return this.http.get(url)
            .map(data => this.extractPayload(data))
            .do(data => this.store.save(data.profile))
            .catch(this.handleError);
    }

    save(data: any, synch: boolean = true): Observable<any> {
        if(synch){
          if (data.UP1_ID === 0) {
             //return this.create(user);
          }  else{
            return this.update(data);
          }
        }else{
          this.getStore().save(data);
        }
        return Observable.of(this.initialize());
    }

    private update(data: any): Observable<any> {
        const url = `${this.apiUrl + '/user-profiles/' + data.user_id}`;
        return this.http.put(url, data)
            .map(data => this.extractPayload(data))
            .do(data => this.store.update(data))
            .catch(this.handleError);
    }

    initialize(): any{
        // Return an initialized object
        return {
            UP1_ID: 0,
            user_id: null,
            UP1_RESTRICT_CUSTOMERS: null,
            UP1_RESTRICT_VENDORS: null
        };
    }
}
