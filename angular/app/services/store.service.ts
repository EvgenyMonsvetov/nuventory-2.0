
export class StoreService {

    constructor(private scope: string, private idProperty: string) {}

    get items(): any[]{
        return JSON.parse(localStorage.getItem(this.scope)) || [];
    }

    _setItems(items:any){
        localStorage.setItem(this.scope, JSON.stringify(items));
    }

    _updateRecord( oldRecord, newRecord){
        var record = {};
        for( var prop in oldRecord ){
            record[prop] = typeof newRecord[prop] !== 'undefined' ? newRecord[prop] : oldRecord[prop];
        }
        return record;
    }

    add(item: object){
        let items = this.items;
        items.push(item);
        this._setItems(items);
    }

    load(items:any, append: boolean = true){
        let elems = this.items;
        items.forEach(el=>{
            elems.push(el);
        });
        this._setItems(elems);
    }

    remove(id){
        let items = this.items.filter(item => item[this.idProperty] != id);
        this._setItems(items);
    }

    update(record: object) {
        var me = this;
        let items = this.items.map(item => {
            if(record[this.idProperty] == item[this.idProperty]){
                item = me._updateRecord( item, record );
            }
            return  item;
        });
        this._setItems(items);
    }

    getById(id: any){
        let items = this.items.filter(item => item[this.idProperty] === parseInt(id));
        return (items.length && items[0][this.idProperty]) ? items[0] : null;
    }

    save(record){
        let item = this.getById( record[this.idProperty] );
        if(item){
            this.update( record );
        }else{
            this.add( record );
        }
    }

    saveAll(records){
        var me = this;
        records.forEach( record => {
            let rec = me.getById( record[me.idProperty] );
            if( rec ){
                me.update( record );
            }else{
                me.add( record );
            }
        });
    }

    findBy( callback ){
        var me = this;
        let items = this.items.filter(item => {
            return callback.call(me, item);
        });
        return items;
    }
}