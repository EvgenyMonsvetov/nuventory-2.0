import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';

@Injectable()
export class OrderLineService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/order-lines/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/order-lines', { params : params });
    }

    preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/order-lines', { params : params });
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/order-lines/' + id, {});
    }

    public update(item: any): Observable<any> {
        const url = `${this.apiUrl + '/order-lines'}/${item.OR2_ID}`;
        return this.http.put(url, item)
            .catch(this.handleError);
    }

    initialize(): any {
        // Return an initialized object
        return {
            OR2_ID: 0,
            OR1_ID: null,
            OR1_NUMBER: null,
            OR2_LINE: null,
            OR2_TYPE: null,
            OR2_STATUS: null,
            OR2_STATUS_TEXT: null,
            MD1_ID: null,
            CO1_ID: null,
            LO1_TO_ID: null,
            FI1_ID: null,
            MD1_PART_NUMBER: null,
            GL1_NAME: null,
            MD1_UPC1: null,
            MD1_DESC1: null,
            MD1_DESC2: null,
            OR2_RECEIVING_QTY: null,
            OR2_RECEIVED_QTY: 0,
            QTY: 0,
            UM1_RECEIVE_NAME: null,
            MD1_ON_HAND_QTY: null,
            MD1_AVAILABLE_QTY: null,
            OR2_ORDER_QTY: null,
            UM1_NAME: null,
            MD1_UNIT_PRICE: null,
            OR2_EXTENDED_VALUE: null,
            OR2_MODIFIED_ON: null,
            OR2_CREATED_ON: null,
            OR2_MODIFIED_BY: null,
            MODIFIED_BY: null,
            OR2_CREATED_BY: null,
            CREATED_BY: null
        };
    }
}
