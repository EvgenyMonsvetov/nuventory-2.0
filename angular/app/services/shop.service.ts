import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class ShopService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public generateShopShortCode(): Observable<any>{
        return this.http.get<IResponse<any>>(this.apiUrl + '/shop/generate-shop-short-code');
    }

    public getItems(page:any, url:any=null): Observable<IResponse<object>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        if (!_.isUndefined(page.size)) {
            params = params.append('perPage', page.size);
        }
        if (!_.isUndefined(page.pageNumber)) {
            params = params.append('page', page.pageNumber);
        }
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }

        var url = url || '/shops';
        return this.http.get<IResponse<object>>(this.apiUrl + url, { params : params });
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return Observable.of(this.initialize());
        }

        const url = `${this.apiUrl}/shops/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(): Observable<IResponse<object>> {
        let params = new HttpParams();
        return this.http.get<IResponse<object>>(this.apiUrl + '/shop/all', { params : params });
    }

    public getByCountries(page:any, url:any=null): Observable<IResponse<object>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        if (!_.isUndefined(page.size)) {
            params = params.append('perPage', page.size);
        }
        if (!_.isUndefined(page.pageNumber)) {
            params = params.append('page', page.pageNumber);
        }
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }

        return this.http.get<IResponse<object>>(this.apiUrl + '/shop/all', { params : params });
    }

    // get Location Last ShortCode for setup wizard
    public getLocationLastShortCode(): Observable<IResponse<any>> {
        let params = new HttpParams();
        return this.http.get<IResponse<any>>(this.apiUrl + '/shop/last-short-code', { params : params });
    }

    create(record: any): Observable<any> {
        return this.http.post(this.apiUrl + '/shops', record)
            .catch(this.handleError);
    }

    createShops(records: any[]): Observable<any> {
        return this.http.post(this.apiUrl + '/shop/create-shops', records)
            .catch(this.handleError);
    }

    update(record: any): Observable<any> {
        const url = `${this.apiUrl + '/shops'}/${record.LO1_ID}`;
        return this.http.put(url, record)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/shops/' + id);
    }

    getMasterData( MD1_ID: any){
        const url = `${this.apiUrl}/shops/master-data/${MD1_ID}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public saveMaterialBudget(item: any): Observable<any> {
        return this.http.post(this.apiUrl + '/shop/save-material-budget', { MOUNTHLY_GROSS_SALE: item['MOUNTHLY_GROSS_SALE'], TARGET_PERCENTAGE: item['TARGET_PERCENTAGE'] })
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getMaterialBudget(): Observable<any>{
        let params = new HttpParams();
        return this.http.get<IResponse<any>>(this.apiUrl + '/shop/get-material-budget', { params : params });
    }

    public preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/shop/preload', { params : params });
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/shop/export';
        const defaultFileName = 'Shops.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize() {
        return {
            LO1_ID: null,
            CO1_ID: null,
            LO1_SHORT_CODE: null,
            LO1_NAME: null,
            LO1_TYPE: null,
            LO1_RACKS: null,
            LO1_ADDRESS1: null,
            LO1_ADDRESS2: null,
            LO1_CITY: null,
            LO1_STATE: null,
            LO1_ZIP: null,
            LO1_PHONE: null,
            LO1_FAX: null,
            lO1_TIMEZONE: null,
            LO1_MITCHELL_SHOP_ID: null,
            LO1_CCC_RFID: null,
            LO1_CCC_SHOP_ID: null,
            CY1_ID: null,
            LO1_DELETE_FLAG: null,
            LO1_CREATED_BY: null,
            LO1_CREATED_ON: null,
            LO1_MODIFIED_BY: null,
            LO1_MODIFIED_ON: null,
            LO1_CENTER_FLAG: null
        }
    }
}
