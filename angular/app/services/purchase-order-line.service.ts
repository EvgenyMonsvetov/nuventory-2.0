import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';

@Injectable()
export class PurchaseOrderLineService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/purchase-order-lines/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/purchase-order-lines', { params : params });
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/purchase-order-lines/' + id, {});
    }

    initialize(): any {
        // Return an initialized object
        return {
            PO2_ID: 0,
            PO2_LINE: 0,
            CA1_ID: null,
            CA1_NAME: null,
            CO1_ID: null,
            GL1_NAME: null,
            LO1_ID: null,
            MD1_ID: '',
            PO1_ID: null,
            PO1_MODIFIED_ON: '',
            PO1_NUMBER: null,
            PO1_STATUS_TEXT: '',
            PO2_CREATED_BY: null,
            PO2_DELETE_FLAG: 0,
            PO2_DESC1: '',
            PO2_DESC2: '',
            JT2_TOTAL_VALUE: null,
            PO2_MODIFIED_BY: null,
            PO2_ORDER_QTY: null,
            PO2_PART_NUMBER: '',
            PO2_RECEIVED_QTY: '',
            PO2_RECEIVING_QTY: '',
            PO2_STATUS: '',
            PO2_TOTAL_VALUE: '',
            PO2_UNIT_PRICE: '',
            UM1_ID: '',
            UM1_NAME: '',
            UM1_RECEIVE_NAME: '',
            MODIFIED_ON: null,
            MODIFIED_BY: null,
            CREATED_BY: null,
            CREATED_ON: null
        };
    }
}
