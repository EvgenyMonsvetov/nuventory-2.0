import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class MeasurementUnitService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getItems(page): Observable<IResponse<object>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<object>>(this.apiUrl + '/measurement-units', { params : params });
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return Observable.of(this.initialize());
        }

        const url = `${this.apiUrl}/measurement-units/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    create(um: any): Observable<any> {
        return this.http.post(this.apiUrl + '/measurement-units', um)
            .catch(this.handleError);
    }

    update(um: any): Observable<any> {
        const url = `${this.apiUrl + '/measurement-units'}/${um.UM1_ID}`;
        return this.http.put(url, um)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/measurement-units/' + id);
    }

    public deleteMultiple(UM1_IDs) {
        return this.http.post(this.apiUrl + '/measurement-unit/delete-multiple', { UM1_IDs: UM1_IDs })
            .catch(this.handleError);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/measurement-unit/export';
        const defaultFileName = 'MeasurementUnits.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize(){
        return {
            UM1_ID: null,
            UM1_SHORT_CODE: '',
            UM1_NAME: '',
            UM1_SIZE: '',
            UM1_CREATED_ON: '',
            UM1_CREATED_BY: '',
            UM1_MODIFIED_ON: '',
            UM1_MODIFIED_BY: ''
        }
    }
}
