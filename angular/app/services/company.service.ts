import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {ICompany} from "../views/companies/ICompany";
import {GeneralService} from "./general.service";
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class CompanyService extends GeneralService implements EDIServices<Observable<ICompany>, ICompany, number> {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/companies/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    save(company: ICompany, isCreate: boolean): Observable<ICompany> {
        if (company.CO1_ID === 0 || isCreate) {
            return this.create(company);
        }

        return this.update(company);
    }

    public getCompanies(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/companies', { params : params });
    }

    // get CompanyLocation for setup wizard
    public getCompanyLocation(): Observable<IResponse<any>> {
        let params = new HttpParams();
        return this.http.get<IResponse<any>>(this.apiUrl + '/company/company-location', { params : params });
    }

    // get Company Last ShortCode for setup wizard
    public getCompanyLastShortCode(): Observable<IResponse<any>> {
        let params = new HttpParams();
        return this.http.get<IResponse<any>>(this.apiUrl + '/company/last-short-code', { params : params });
    }

    public create(company: any): Observable<any> {
        return this.http.post(this.apiUrl + '/companies', company)
            .catch(this.handleError);
    }

    public createCompanies(companies: any[]): Observable<any> {
        return this.http.post(this.apiUrl + '/company/create-companies', companies)
            .catch(this.handleError);
    }

    public update(company: any): Observable<any> {
        const url = `${this.apiUrl + '/companies'}/${company.CO1_ID}`;
        return this.http.put(url, company)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/companies/' + id);
    }

    public preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/company/preload', { params : params });
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/company/export';
        const defaultFileName = 'Companies.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    public initialize() {
        return {
            'CO1_ID': 0,
            'CO1_NAME': null,
            'CO1_ADDRESS1': null,
            'CO1_ADDRESS2': null,
            'CO1_CITY': null,
            'CO1_STATE': null,
            'CO1_ZIP': null,
            'CO1_PHONE': null,
            'CO1_FAX': null,
            'CO1_MITCHELL_FLAG': null,
            'CO1_FM_FLAG': null,
            'CY1_ID': null,
            'CO1_DELETE_FLAG': null,
            'CO1_CREATED_BY': null,
            'CO1_CREATED_ON': null,
            'CO1_MODIFIED_BY': null,
            'CO1_MODIFIED_ON': null,
            'CO1_CCC_FLAG': null,
            'CO1_DANGELO': null,
            'CO1_TIMEZONE': null,
            'CO1_SUMMARY_REPORT_EMAIL': null,
            'CO1_LITE': null
        };
    }
}
