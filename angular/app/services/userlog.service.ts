import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {Userlog} from '../model/userlog';

@Injectable()
export class UserlogService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public delete(id) {
        return this.http.delete<any>(this.apiUrl + '/userlog/' + id);
    }

    public getUserlogs(page): Observable<IResponse<Userlog>> {
        const current_page = page.pageNumber;
        let params = new HttpParams();
        params = params.append('perPage', page.size);
        params = params.append('page', current_page);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort', key + ',' + value );
            }
        }
        if (!_.isUndefined(page.filterParams)) {
            for (const key of Object.keys(page.filterParams)) {
                const value = page.filterParams[key];
                if (value && value !== '') {
                    params = params.append('filter', key + ',' + value );
                }
            }
        }
        return this.http.get<IResponse<Userlog>>(this.apiUrl + '/userlog', { params : params });
    }

}
