import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';

@Injectable()
export class TechnicianService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/technicians/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getTechnicians(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/technicians', { params : params });
    }

    public create(technician: any): Observable<any> {
        return this.http.post(this.apiUrl + '/technicians', technician)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public update(technician: any): Observable<any> {
        return this.http.post(this.apiUrl + '/technicians', technician)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/technicians/' + id, {});
    }

    public preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/technician/preload', { params : params });
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/technician/export';
        const defaultFileName = 'Technicians.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    public printPDF(page: any): Observable<any> {
        const apiUrl = '/technician/print-pdf';
        const defaultFileName = 'Technicians.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    getLastShortCode(): Observable<any> {
        const url = `${this.apiUrl}/technician/last-short-code`;
        return this.http.get(url);
    }

    initialize(): any {
        // Return an initialized object
        return {
            TE1_ID: 0,
            TE1_SHORT_CODE:     null,
            TE1_NAME:           null,
            US1_LOGIN:          null,
            US1_PASS:           null,
            US1_ALLOW_MOBILE_JOB_CREATE: null,
            TE1_BODY:           null,
            TE1_PAINT:          null,
            TE1_DETAIL:         null,
            LO1_ID:             null,
            LO1_SHORT_CODE:     null,
            LO1_NAME:           null,
            CO1_ID:             null,
            CO1_SHORT_CODE:     null,
            CO1_NAME:           null,
            TE1_DELETE_FLAG:    0,
            TE1_CREATED_BY:     null,
            TE1_CREATED_ON:     null,
            TE1_MODIFIED_BY:    null,
            TE1_MODIFIED_ON:    null,
            TE1_ACTIVE:         true,
            user: {}
        };
    }

}
