import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';
import { HeaderService } from './header.service';
import { _lang } from '../pipes/lang';

@Injectable()
export class TechScanService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/tech-scans/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/tech-scans', { params : params });
    }

    public isScanCanBeCreated(headerService: HeaderService){
      var canBeCreated = false;
      var errors = [];

      if (!headerService.LO1_ID) {
          errors.push( _lang('current location is not selected') );
      } else if (parseInt(headerService.getCurrentLocation()['LO1_CENTER_FLAG'])) {
          errors.push( _lang('current location is Central Warehouse') );
      }
      if (!errors.length) {
          canBeCreated = true;
      }

      var error = '';
      if (errors.length) {
          error = _lang('Tech Scan can not be created! The reason is ') + errors.join('<br/>');
      }

      return {
          canBeCreated: canBeCreated,
          error: error
      }
    }

    public initOrder(page: any): Observable<any> {
        let params = new HttpParams();
        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/tech-scan/init-order', { params : params });
    }

    public preloadData(): Observable<any> {
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/tech-scan/preload', { params : params });
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/tech-scans/' + id, {});
    }

    public deleteMultiple(OR1_IDs){
        return this.http.post(this.apiUrl + '/tech-scan/delete-multiple', { OR1_IDs: OR1_IDs })
            .catch(this.handleError);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/tech-scan/export';
        const defaultFileName = 'TechScans.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    public printPDF(page: any): Observable<any> {
        const apiUrl = '/tech-scan/print-pdf';
        const defaultFileName = 'TechScans.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public initialize(): any {
        // Return an initialized object
        return {
            OR1_ID: 0,
            CA1_NAME: null,
            CO1_ID: null,
            COMPLETED_ON: null,
            CREATED_BY: null,
            CREATED_ON: null,
            LO1_FROM_ID: null,
            LO1_TO_ID: null,
            MODIFIED_BY: null,
            MODIFIED_ON: null,
            OR1_COMMENT: null,
            OR1_COMPLETED_ON: null,
            OR1_CREATED_ON: null,
            OR1_INV_STATUS: null,
            OR1_INV_STATUS_TEXT: null,
            OR1_MODIFIED_ON: null,
            OR1_NUMBER: null,
            OR1_STATUS: null,
            OR1_STATUS_TEXT: null,
            OR1_TOTAL_VALUE: null,
            OR1_TYPE: null,
            ORDERED_BY: null,
            ORDERED_ON: null,
            ORDER_BY_LOCATION_CODE: null,
            ORDER_BY_LOCATION_NAME: null,
            ORDER_FROM_LOCATION_CODE: null,
            ORDER_FROM_LOCATION_NAME: null,
            TE1_ID: null,
            TE1_NAME: null,
            TE1_SHORT_CODE: null,
            VD1_ID: null,
            VD1_NAME: null
        };
    }
}
