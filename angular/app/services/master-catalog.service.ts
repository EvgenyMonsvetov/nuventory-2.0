import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';

@Injectable()
export class MasterCatalogService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/master-catalogs/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-catalogs', { params : params });
    }

    public findLinkedParts(page){
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-catalog/linked-parts', { params : params }).
                map(
                    data => this.extractPayload(data)
                );
    }

    public getSearch(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-catalog/search', { params : params });
    }

    public create(item: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-catalogs', item)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public update(item: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-catalogs', item)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-catalog/preload', { params : params });
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/master-catalogs/' + id, {});
    }

    public deleteMultiple(MD0_IDs) {
        return this.http.post(this.apiUrl + '/master-catalog/delete-multiple', { MD0_IDs: MD0_IDs })
            .catch(this.handleError);
    }

    public getAllParts(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        if (!_.isUndefined(page.filterParams)) {
            for (const key of Object.keys(page.filterParams)) {
                const value = page.filterParams[key];
                if (value && value !== '') {
                    params = params.append(key, value);
                }
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-catalog/parts', { params : params });
    }

    public updateParts(items: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-catalog/update-parts', items)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/master-catalog/export';
        const defaultFileName = 'VendorCatalogs.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize(): any {
        // Return an initialized object
        return {
            MD1_ID: 0,
            MD0_ID: 0,
            CO1_ID: null,
            UM1_ID: null,
            MD0_DISCONTINUED: 0,
            UM1_DEFAULT_PRICING: '',
            UM1_DEFAULT_PURCHASE: '',
            UM1_DEFAULT_RECEIVE: '',
            VD0_MANUFACTURER: null,
            VD0_ID: null,
            VD0_NAME: null,
            VD0_SHORT_CODE: null,
            VD0_DISCOUNT: null,
            MD0_UNIT_PRICE: 0,
            FI1_ID: null,
            VD1_ID: null,
            C00_ID: null,
            C00_NAME: null,
            CA0_ID: null,
            CA0_NAME: null,
            CA1_ID: null,
            CA2_ID: null,
            MD1_URL: null,
            MD1_SAFETY_URL: null,
            MD1_MODIFIED_ON: null,
            MD1_MODIFIED_BY: null,
            MODIFIED_BY: null,
            MD1_CREATED_ON: null,
            MD1_CREATED_BY: null,
            CREATED_BY: null,
            LG2_DATE: null,
            LG2_OLD_PRICE: null,
            MD0_PART_NUMBER: null,
            MD0_VOC_FLAG: null,
            MD0_DELETE_FLAG: 0,
            MD0_BODY: 0,
            MD0_DETAIL: 0,
            MD0_PAINT: 0,
            MD0_VOC_UNIT: 0,
            UM1_DEFAULT_PURCHASE_FACTOR: 1,
            UM1_DEFAULT_RECEIVE_FACTOR: 1,
            FI1_FILE_PATH_EXIST: false
        };
    }

    setDefaultImages(MD0_ID, MD1_IDS): Observable<any> {
        return this.http.post(this.apiUrl + '/master-catalog/set-default-images', {
            MD0_ID: MD0_ID,
            MD1_IDS: MD1_IDS
        }).catch(this.handleError);
    }
}
