import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';
import { IMasterVendor } from '../model/interfaces/IMasterVendor';

@Injectable()
export class MasterVendorService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/master-vendors/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-vendors', { params : params });
    }

    public create(item: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-vendors', item)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public update(item: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-vendors', item)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/master-vendors/' + id, {});
    }

    public deleteMultiple(VD0_IDs) {
        return this.http.post(this.apiUrl + '/master-vendor/delete-multiple', { VD0_IDs: VD0_IDs })
            .catch(this.handleError);
    }

    public deleteMasterVendorParts(id: number) {
        return this.http.post<any>(this.apiUrl + '/master-vendor/delete-master-vendor-parts', { VD0_ID: id });
    }

    public deleteAllImages(id: number) {
        return this.http.post<any>(this.apiUrl + '/master-vendor/delete-all-images', { VD0_ID: id });
    }

    public uploadArchiveImages(VD0_ID: number, fileToUpload: File) {
        const formData: FormData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        formData.append('VD0_ID', VD0_ID.toFixed());
        return this.http.post(this.apiUrl + '/master-vendor/upload-archive-images', formData)
            .catch(this.handleError);
    }

    public getAllParts(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-vendor/parts', { params : params });
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/master-vendor/export';
        const defaultFileName = 'MasterVendors.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize(): IMasterVendor {
        // Return an initialized object
        return {
            VD0_ID: 0,
            VD0_EDI_GL: 0,
            VD0_SHORT_CODE: null,
            VD0_NAME: null,
            VD0_MANUFACTURER: null,
            VD0_ADDRESS1: null,
            VD0_ADDRESS2: null,
            VD0_CITY: null,
            VD0_STATE: null,
            VD0_ZIP: null,
            VD0_PHONE: null,
            VD0_FAX: null,
            VD0_URL: null,
            VD0_EMAIL: null,
            VD0_DELETE_FLAG: 0,
            CY1_ID: null,
            CY1_SHORT_CODE: null,
            CY1_NAME: null,
            VD0_MODIFIED_ON: null,
            VD0_CREATED_ON: null,
            CREATED_BY: null,
            MODIFIED_BY: null,
            VD0_MINIMUM_ORDER_CHECK: null,
            VD0_MINIMUM_ORDER: 0,
            VD0_DISCOUNT: 0,
            VD0_FLAG: null,
            VD0_EDI_VERSION: null,
            VD0_X12_STANDARD: null,
            VD0_SUPPLIER_CODE: null,
            VD0_DUNS: null,
            VD0_FACILITY: null,
            VD0_TRADING_PARTNER_QUALIFIER: null,
            VD0_TRADING_PARTNER_ID: null,
            VD0_TRADING_PARTNER_GS_ID: null,
            VD0_RECEIVER_QUALIFIER: null,
            VD0_RECEIVER_ID: null,
            VD0_RECEIVER_GS_ID: null,
            VD0_AS2_CERTIFICATE_FILENAME: null,
            VD0_AS2_RECEIVER_ID: null,
            VD0_AS2_TRADING_PARTNER_ID: null,
            VD0_SEND_EDI_PO: 0,
            VD0_PO_FORMAT: 0,
            VD0_SEND_FTP: 0,
            VD0_SEND_SFTP: 0,
            VD0_REMOTE_FTP_SERVER: null,
            VD0_REMOTE_FTP_DIRECTORY_SEND: null,
            VD0_REMOTE_FTP_DIRECTORY_PICKUP: null,
            VD0_REMOTE_FTP_USERNAME: null,
            VD0_REMOTE_FTP_PASSWORD: null,
            VD0_POST_HTTP: 0,
            VD0_POST_AS2: 0,
            VD0_AS2_REQUEST_RECEIPT: null,
            VD0_AS2_SIGN_MESSAGES: null,
            VD0_REMOTE_HTTP_SERVER: null,
            VD0_AS2_KEY_LENGTH: null,
            VD0_FTP_DIRECTORY: null,
            VD0_FTP_USER: null,
            VD0_FTP_PASSWORD: null,
            VD0_RECEIVE_EDI: 0,
            VD0_SEND_ACKNOWLEDGEMENT: 0,
            VD0_PICKUP_FTP: 0,
            VD0_PICKUP_SFTP: 0,
            VD0_PICKUP_DIRECTORY: null,
            VD0_RECEIVE_HTTP: 0,
            VD0_RECEIVE_AS2: 0,
            VD0_SHARED_SECRET: null,
            VD0_RECEIVE_FTP: 0,
            VD0_NO_INVENTORY: 0
        };
    }

}
