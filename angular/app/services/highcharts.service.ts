//import { PointObject } from 'highcharts';
import { Chart } from 'angular-highcharts';
import * as moment from 'moment-timezone';
import * as Highcharts from 'highcharts';
import { _lang } from '../pipes/lang';

export class HighchartsService {

  static DRILLDOWN_YEAR  = 4;
  static DRILLDOWN_MONTH = 3;
  static DRILLDOWN_WEEK  = 2;
  static DRILLDOWN_DAY   = 1;

  getSerieByPoint( chart: Chart, point: any ): any {
    var serie: any = {};
    for(var i = chart.ref.series.length - 1; i > -1; i--) {
      for(var dataIndex = 0; dataIndex < chart.ref.series[i].data.length; dataIndex ++){
        var data = chart.ref.series[i].data[dataIndex];
        if(data['name'] === point['name']){
          serie = data;
          break;
        }
      }
    }
    return serie;
  }

  updateXAxis( chart: Chart, aggrigatedBy: number, rotation: number = 0 ){
    var dateTimeLabelFormat = '';
    var xAxisTitle = '';

    if(aggrigatedBy == HighchartsService.DRILLDOWN_YEAR){
      dateTimeLabelFormat = '%Y';
      xAxisTitle = _lang('Yearly');
    }else if(aggrigatedBy == HighchartsService.DRILLDOWN_MONTH){
      dateTimeLabelFormat = '%b %Y';
      xAxisTitle = _lang('Monthly');
    }else if(aggrigatedBy == HighchartsService.DRILLDOWN_WEEK){
      dateTimeLabelFormat = '%W/%Y';
      xAxisTitle = _lang('Weekly');
    }else{
      dateTimeLabelFormat = '%b %e, %Y';
      xAxisTitle = _lang('Daily');
    }

    chart.ref.xAxis[0].update({
      title: {
          text: xAxisTitle
      },
      labels: {
        rotation: rotation,
        formatter: function() {

          function formatWeek( value ){
            var date  = moment.tz(new Date(value), 'UTC');
            var tDate = new Date( Date.UTC(date.year(), 0, 1) );
            var testDate = moment(tDate).tz('UTC');
            var dayNumber = Math.floor((date.valueOf() - testDate.valueOf()) / 86400000);
            return '' + (1 + Math.floor(dayNumber / 7.02));
          }

          var localFormat = dateTimeLabelFormat;
          if(/\%W/.test(dateTimeLabelFormat)){
            localFormat = dateTimeLabelFormat.replace( /\%W/ , formatWeek(this.value));
          }
          return Highcharts.dateFormat(localFormat, this.value);
        }
      }
      // ,
      // dateTimeLabelFormats: {
      //   day: dateTimeLabelFormat.toString()
      // }
    });
  }

  destroyDrillDown( chart: Chart ) {
    if(chart.ref['drilldownLevels']){
      chart.ref['drilldownLevels'] = [];
      chart.ref['applyDrilldown']();
      chart.ref['drillUpButton'] = chart.ref['drillUpButton'].destroy();
    }
  }

  removeAllSeries( chart: Chart ){
    var seriesLength = chart.ref.series.length;
    for(var i = seriesLength - 1; i > -1; i--) {
      chart.removeSeries(i);
    }
  }
}
