import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { GeneralService } from './general.service';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class MailService extends GeneralService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  public sendMail(postObject): Observable<IResponse<any>> {
    return this.http.post<IResponse<any>>(this.apiUrl + '/mail/send', postObject);
  }

}