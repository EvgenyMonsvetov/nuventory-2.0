import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppSidebarNavService extends Subject<any> {
  private _period: number = 1;
  private _limitToMinQty: boolean = true;
  private _fromDate: string = '2008-01-01';
  private _toDate: string = '2008-01-01';
  private _lastValue: any;
  private _deatachChangeDetection: boolean = false;

  public next(value?: any) {
    if (!value) {
      value = {
        period: this.period,
        limitToMinQty: this.limitToMinQty,
      }
    }
    this._lastValue = value;
    super.next(value);
  }

  public get lastValue() {
    return this._lastValue;
  }

  setPeriod ( period: number ) {
    this._period = period;
  }

  public get period() {
    return this._period;
  }

  setLimitToMinQty ( limitToMinQty: boolean ) {
    this._limitToMinQty = limitToMinQty;
  }

  public get limitToMinQty() {
    return this._limitToMinQty;
  }

  setFromDate ( value: string ) {
    this._fromDate = value;
  }

  public get fromDate() {
    return this._fromDate;
  }

  setToDate ( value: string ) {
    this._toDate = value;
  }

  public get toDate() {
    return this._toDate;
  }

  setDeatachChangeDetection ( value: boolean ) {
    this._deatachChangeDetection = value;
  }

  public get deatachChangeDetection() {
    return this._deatachChangeDetection;
  }
}
