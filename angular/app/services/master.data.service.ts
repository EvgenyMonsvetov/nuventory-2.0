import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';
import {Page} from '../model/page';

@Injectable()
export class MasterDataService extends GeneralService {

    static AUTO_ORDER_PO = 1;
    static AUTO_ORDER_SO = 2;

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/master-datas/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    checkDuplicates(page) {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-data/check-duplicate', { params : params });
    }

    public getCentralOrderParts(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-data/central-order-parts', { params : params });
    }

    public geAutoOrderParts(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-data/auto-order-parts', { params : params });
    }

    public getFullParts(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-data/full-parts', { params : params });
    }

    public getQuickAddParts(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-data/quick-add-parts', { params : params });
    }

    public getCreditMemoFullPart(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-data/credit-memo-full-part', { params : params });
    }

    public create(item: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-datas', item)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public createMultiple(MD0_IDs: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-data/create-multiple', { MD0_IDs: MD0_IDs })
            .catch(this.handleError);
    }

    public update(item: any): Observable<any> {
        const url = `${this.apiUrl + '/master-datas'}/${item.MD1_ID}`;
        return this.http.put(url, item).map(
                data => this.extractPayload(data)
            ).catch(this.handleError);
    }

    public updateInventory(items: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-data-location/update-inventory', items)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/master-datas/' + id, {});
    }

    public deleteMultiple(MD1_IDs){
        return this.http.post(this.apiUrl + '/master-data/delete-multiple', { MD1_IDs: MD1_IDs })
            .catch(this.handleError);
    }

    public getAll(page: Page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        params = params.append('perPage', page.size.toString());
        params = params.append('page', page.pageNumber.toString());
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }

        var url = page.url || '/master-datas';

        return this.http.get<IResponse<any>>(this.apiUrl + url, { params : params });
    }

    public preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/master-data/preload', { params : params });
    }

    public updateParts(items: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-data/update-parts', items)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getInventoryOptimization(page: Page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        params = params.append('perPage', page.size.toString());
        params = params.append('page', page.pageNumber.toString());
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        var url = page.url || '/master-data/inventory-optimization';
        return this.http.get<IResponse<any>>(this.apiUrl + url, { params : params });
    }

    public getTechUsage(page: Page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        var url = page.url || '/master-data/tech-usage';
        return this.http.get<IResponse<any>>(this.apiUrl + url, { params : params });
    }

    public getProjectedPOUsage(page: Page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        var url = page.url || '/master-data/projected-po-usage';
        return this.http.get<IResponse<any>>(this.apiUrl + url, { params : params });
    }

    public getTechUsageByMonths(page: Page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        var url = page.url || '/master-data/tech-usage-by-months';
        return this.http.get<IResponse<any>>(this.apiUrl + url, { params : params });
    }

    public getVOCUsageReport(page: Page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        var url = page.url || '/master-data/voc-usage';
        return this.http.get<IResponse<any>>(this.apiUrl + url, { params : params });
    }

    public printPDF(page: any): Observable<any> {
        const apiUrl = '/master-data/print-pdf';
        const defaultFileName = 'MasterData.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public printQtySheet(page: any): Observable<any> {
        const apiUrl = '/master-data/print-qty-sheet';
        const defaultFileName = 'MasterDataQty.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public printRack(page: any): Observable<any> {
        var apiUrl = '/master-data/print-rack';
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + apiUrl, { params : params });
    }

    public printCheckoutSheet(page: any): Observable<any> {
        var apiUrl = '/master-data/print-checkout-sheet';
        const defaultFileName = 'MasterDataCheckoutSheet.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public printInventoryOptimization(page: any): Observable<any> {
        const apiUrl = '/master-data/print-inventory-optimization';
        const defaultFileName = 'InventoryOptimization.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public printBelowMinimunPDF(page: any): Observable<any> {
        const apiUrl = '/master-data/print-below-minimum';
        const defaultFileName = 'BelowMinimumReport.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public printUsage(page: any): Observable<any> {
        const apiUrl = '/master-data/print-usage';
        const defaultFileName = 'VOCUsage.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public downloadXlsx(page: any, subtype: string = ''): Observable<any> {
        const apiUrl = '/master-data/export';
        const defaultFileName = 'MasterData.xls';
        return this.getXlsx(apiUrl, page, defaultFileName, subtype);
    }

    initialize(): any {
        // Return an initialized object
        return {

            'MD1_ID': null,
            'MD0_ID': 0,
            'CO1_ID': 0,
            'FI1_ID': 0,
            'CA1_ID': 0,
            'CA2_ID': 0,
            'GL1_ID': 0,
            'MA0_ID': 0,
            'C00_ID': 0,
            'MD1_PART_NUMBER': '',
            'MD1_UPC1': '',
            'MD1_UPC2': '',
            'MD1_UPC3': '',
            'MD1_DESC1': '',
            'MD1_DESC2': '',
            'MD1_UNIT_PRICE': null,
            'UM1_PRICING_ID': null,
            'VD1_ID': null,
            'MD1_DELETE_FLAG': null,
            'MD1_CREATED_BY': null,
            'MD1_CREATED_ON': null,
            'MD1_MODIFIED_BY': null,
            'MD1_MODIFIED_ON': null,
            'MD1_IMAGE': null,
            'CA1_NAME': null,
            'VD1_NAME': null,
            'MD1_UNIT': null,
            'MD1_PURCHASE_UNIT_SIZE': null,
            'MD1_CENTRAL_ORDER_FLAG': null,
            'MD1_VENDOR_PART_NUMBER': null,
            'MD1_OEM_NUMBER': null,
            'MD1_MARKUP': null,
            'MD1_URL': null,
            'MD1_SAFETY_URL': null,
            'MD1_VOC_FLAG': null,
            'MD1_VOC_MFG': null,
            'MD1_VOC_CATA': null,
            'MD1_VOC_VALUE': null,
            'MD1_VOC_UNIT': null,
            'MD1_FAVORITE': null,
            'MD1_TYPE': null,
            'MD1_BODY': null,
            'MD1_PAINT': null,
            'MD1_DETAIL': null,
            'UM1_DEFAULT_PRICING': null,
            'UM1_DEFAULT_PURCHASE': null,
            'UM1_DEFAULT_RECEIVE': null,
            'UM1_DEFAULT_PURCHASE_FACTOR': null,
            'UM1_DEFAULT_RECEIVE_FACTOR': null,
            'MD0_MANUFACTURER': null
        };
    }
}
