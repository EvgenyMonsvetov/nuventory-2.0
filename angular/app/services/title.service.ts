import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  constructor(private titleService: Title) {}
              
  setNewTitle(route: ActivatedRoute){
    const data = route.snapshot.data;
    if (data.title) {
      this.titleService.setTitle(route.snapshot.data.title);
    } else {
      this.titleService.setTitle("Nuventory");
    }
  }

}