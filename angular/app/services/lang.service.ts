import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { GeneralService } from './general.service';
import * as _ from 'lodash';
import { document } from 'ngx-bootstrap';
import { Observable ,  of } from 'rxjs';

@Injectable()
export class LangService extends GeneralService{

  constructor(protected http: HttpClient) {
    super(http);
  }

  get locale(): string{
    return localStorage.getItem('currentLocale');
  }

  setLocale(locale) {
    localStorage.setItem('currentLocale', locale);
  }

  get defaultLocale(): string{
    return localStorage.getItem('defaultLocale');
  }

  setDefaultLocale(locale) {
    localStorage.setItem('defaultLocale', locale);
  }

  get languages(): Array<any>{
    return JSON.parse(localStorage.getItem('languages')) || [];
  }

  setLanguages(languages) {
    localStorage.setItem('languages', JSON.stringify(languages));
  }

  get defaultVocabulary(): Array<any>{
    return JSON.parse(localStorage.getItem('defaultVocabulary')) || [];
  }

  setDefaultVocabulary(items) {
    localStorage.setItem('defaultVocabulary', JSON.stringify(items));
  }

  get vocabulary(): Array<any>{
    return JSON.parse(localStorage.getItem('currentVocabulary')) || [];
  }

  setVocabulary(items) {
    localStorage.setItem('currentVocabulary', JSON.stringify(items));
  }

  public initLanguages( locale?: string) {
    return new Promise((resolve, reject) => {

      var locale = locale || this.locale;
      if(locale == 'undefined'){
        locale = '';
      }
      const url = `${this.apiUrl}/language`;
      this.http.post(url, {
        locale: locale
      }).map(
          data => this.extractPayload(data)
      ).subscribe(data => {
        this.setLanguages( data.languages );

        this.setDefaultLocale( data.defaultLocale	 );
        var defaultVocab = {};
          _.forEach(data.defaultVocabulary, function (item) {
            defaultVocab[item['LA2_KEY']] = item['LA2_TEXT'] ? unescape(item['LA2_TEXT'].toString()) : '';
          });
        this.setDefaultVocabulary( defaultVocab );

        this.setLocale( data.currentLocale );
        var currentVocab = {};
          _.forEach(data.currentVocabulary, function (item) {
            currentVocab[item['LA2_KEY']] = item['LA2_TEXT'] ? unescape(item['LA2_TEXT'].toString()) : '';
          });
        this.setVocabulary( currentVocab );
        resolve();
      });
    });
  }

  public reloadVocabulay( locale: string ){
      this.setLocale( locale );
      document.location.reload();
  }

  public getLabelsByKeys(keys: string, page) {
    let params = new HttpParams();
    params = this.applyFilters(page, params);

    params = params.append('keys', keys);
    params = params.append('perPage', page.size);
    params = params.append('page', page.pageNumber);

    if (!_.isUndefined(page.sortParams)) {
      for (const key of Object.keys(page.sortParams)) {
        const value = page.sortParams[key];
        params = params.append('sort[]', key + ',' + value );
      }
    }

    return new Promise((resolve, reject) => {
      const url = `${this.apiUrl}/labels`;
      this.http.get(url, { params : params }).map(
          data => this.extractPayload(data)
      ).subscribe(data => {
        resolve(data);
      });
    });
  }

  public getAllLabels(page) {
    let params = new HttpParams();
    params = params.append('perPage', page.size);
    params = params.append('page', page.pageNumber);

    if (!_.isUndefined(page.sortParams)) {
      for (const key of Object.keys(page.sortParams)) {
        const value = page.sortParams[key];
        params = params.append('sort[]', key + ',' + value );
      }
    }
    if (!_.isUndefined(page.filterParams)) {
      for (const key of Object.keys(page.filterParams)) {
        const value = page.filterParams[key];
        if (value && value !== '') {
          params = params.append(key, value);
        }
      }
    }

    return new Promise((resolve, reject) => {
      const url = `${this.apiUrl}/labels`;
      this.http.get(url, { params : params }).map(
          data => this.extractPayload(data)
      ).subscribe(data => {
        resolve(data);
      });
    });
  }

  saveTranslation(label: any): any {
    return this.http.post(this.apiUrl + '/labels', label)
        .map( data => data )
        // .do(data => { if (data['data'] && data['data']['data']) this.store.update(data['data']['data']) })
        .catch(this.handleError);
  }

  public getLanguages(page): Observable<IResponse<object>> {
    const current_page = page.pageNumber;
    let params = new HttpParams();
    params = params.append('perPage', page.size);
    params = params.append('page', current_page);
    if (!_.isUndefined(page.sortParams)) {
      for (const key of Object.keys(page.sortParams)) {
        const value = page.sortParams[key];
        params = params.append('sort', key + ',' + value );
      }
    }
    if (!_.isUndefined(page.filterParams)) {
      for (const key of Object.keys(page.filterParams)) {
        const value = page.filterParams[key];
        if (value && value !== '') {
          params = params.append('filter', key + ',' + value );
        }
      }
    }
    return this.http.get<IResponse<object>>(this.apiUrl + '/languages', { params : params });
  }

  getLanguageById(id: number): Observable<any> {
    if (id === 0) {
      return of(this.initializeLang());
    }

    const url = `${this.apiUrl}/languages/${id}`;
    return this.http.get(url)
        .map(
            data => this.extractPayload(data)
        )
        .catch(this.handleError);
  }

  createLanguage(lang: any): Observable<any> {
    return this.http.post(this.apiUrl + '/languages', lang)
        .catch(this.handleError);
  }

  updateLanguage(lang: any): Observable<any> {
    const url = `${this.apiUrl + '/languages'}`;
    return this.http.post(url, lang)
        .catch(this.handleError);
  }

  public deleteLanguage(id: number) {
    return this.http.delete<any>(this.apiUrl + '/languages/' + id, {});
  }

  public downloadXlsx(page: any): Observable<any> {
    const apiUrl = '/languages/export';
    const defaultFileName = 'Languages.xls';
    return this.getXlsx(apiUrl, page, defaultFileName);
  }

  public downloadTranslationsXlsx(page: any): Observable<any> {
    const apiUrl = '/labels/export';
    const defaultFileName = 'Translations.xls';
    return this.getXlsx(apiUrl, page, defaultFileName);
  }

  initializeLang() {
    return {
      LA1_ID: null,
      LA1_LOCALE: '',
      LA1_SHORT_CODE: '',
      LA1_NAME: '',
      CREATED_BY: '',
      CREATED_ON:  '',
      MODIFIED_BY: '',
      MODIFIED_ON:   ''
    }
  }
}