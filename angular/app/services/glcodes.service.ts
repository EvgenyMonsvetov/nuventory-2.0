import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class GlCodesService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getItems(page): Observable<IResponse<object>> {
        const current_page = page.pageNumber;
        let params = new HttpParams();
        params = params.append('perPage', page.size);
        params = params.append('page', current_page);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort', key + ',' + value );
            }
        }
        if (!_.isUndefined(page.filterParams)) {
            for (const key of Object.keys(page.filterParams)) {
                const value = page.filterParams[key];
                if (value && value !== '') {
                    params = params.append('filter', key + ',' + value );
                }
            }
        }
        return this.http.get<IResponse<object>>(this.apiUrl + '/gl-codes', { params : params });
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/gl-codes/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    create(glcode: any): Observable<any> {
        return this.http.post(this.apiUrl + '/gl-codes', glcode)
            .catch(this.handleError);
    }

    update(glcode: any): Observable<any> {
        const url = `${this.apiUrl + '/gl-codes'}/${glcode.GL1_ID}`;
        return this.http.put(url, glcode)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/gl-codes/' + id);
    }

    public deleteMultiple(GL1_IDs) {
        return this.http.post(this.apiUrl + '/gl-code/delete-multiple', { GL1_IDs: GL1_IDs })
            .catch(this.handleError);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/gl-code/export';
        const defaultFileName = 'GLCodes.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize(){
        return {
            GL1_ID: null,
            CO1_ID: '',
            GL1_NAME: '',
            GL1_DESCRIPTION: '',
            GL1_NO_STATEMENT: '',
            GL1_CREATED_ON:       '',
            GL1_CREATED_BY_NAME:  '',
            GL1_MODIFIED_ON:      '',
            GL1_MODIFIED_BY_NAME: ''
        }
    }
}
