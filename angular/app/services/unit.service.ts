import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';

@Injectable()
export class UnitService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getAll(): Observable<IResponse<any>> {
        return this.http.get<IResponse<any>>(this.apiUrl + '/units')
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);;
    }

}
