import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class BudgetsService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    initialize(): any {
        return {
            BU1_ID: 0,
            CO1_ID: null,
            LO1_ID: null,
            BU1_DATE: null,
            BU1_DELETE_FLAG: null,
            BU1_CREATED_BY: null,
            BU1_MODIFIED_BY: null,
            BU1_CREATED_ON: null,
            BU1_MODIFIED_ON: null,
            BU1_MONTHLY_GROSS_SALE: null,
            BU1_TARGET_PERCENTAGE: null,
            BU1_FINAL_RESULT: null
        };
    }


    public create(item: any): Observable<any> {
        return this.http.post( this.apiUrl + '/budget/create', {
                BU1_DATE: item['BU1_DATE'],
                BU1_MONTHLY_GROSS_SALE: item['BU1_MONTHLY_GROSS_SALE'],
                BU1_TARGET_PERCENTAGE: item['BU1_TARGET_PERCENTAGE']
            })
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    update(budget: any): Observable<any> {
        const url = this.apiUrl + '/budget/update';
        return this.http.put(url, budget).map(
            data => this.extractPayload(data)
        )
            .catch(this.handleError);
    }

    deleteMultiple(BU1_IDs): Observable<any> {
        const url = this.apiUrl + '/budget/delete-multiple';
        return this.http.post(url, {BU1_IDs: BU1_IDs});
    }

    getById(id):  Observable<any> {
        if (id === 0) {
            return Observable.of(this.initialize());
        }
        return this.http.get<IResponse<any>>(this.apiUrl + `/budgets/${id}`);
    }

    getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        if (!_.isUndefined(page.size)) {
            params = params.append('perPage', page.size);
        }
        if (!_.isUndefined(page.pageNumber)) {
            params = params.append('page', page.pageNumber);
        }
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/budget', { params : params });
    }

}
