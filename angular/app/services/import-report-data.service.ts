import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import {_lang} from "../pipes/lang";

@Injectable()
export class ImportReportDataService extends GeneralService {

    static FT_JOB_COST_SUMMARY = 1;
    static FT_SALES_JOURNAL_REPORT = 2;

    getFileTypes(): Array<{type: number, name: string}> {
        return [
          {type: ImportReportDataService.FT_JOB_COST_SUMMARY,      name: _lang('Job Cost Summary')},
          {type: ImportReportDataService.FT_SALES_JOURNAL_REPORT,  name: _lang('Sales Journal Report')}
        ]
    }

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getImportReportData(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/import-report-data', { params : params });
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/import-report-data/export';
        const defaultFileName = 'ImportReportData.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize(): any {

    }

}
