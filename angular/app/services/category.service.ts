import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class CategoryService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getItems(page): Observable<IResponse<object>> {
        const current_page = page.pageNumber;
        let params = new HttpParams();
        params = params.append('perPage', page.size);
        params = params.append('page', current_page);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort', key + ',' + value );
            }
        }
        if (!_.isUndefined(page.filterParams)) {
            for (const key of Object.keys(page.filterParams)) {
                const value = page.filterParams[key];
                if (value && value !== '') {
                    params = params.append('filter', key + ',' + value );
                }
            }
        }
        return this.http.get<IResponse<object>>(this.apiUrl + '/categories', { params : params });
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/categories/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    create(cat: any): Observable<any> {
        return this.http.post(this.apiUrl + '/categories', cat)
            .catch(this.handleError);
    }

    update(cat: any): Observable<any> {
        const url = `${this.apiUrl + '/categories'}/${cat.CA1_ID}`;
        return this.http.put(url, cat)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/categories/' + id);
    }

    public deleteMultiple(CA1_IDs) {
        return this.http.post(this.apiUrl + '/category/delete-multiple', { CA1_IDs: CA1_IDs })
            .catch(this.handleError);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/category/export';
        const defaultFileName = 'Categories.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize() {
        return {
            CA1_ID: null,
            C00_ID: 0,
            CA0_ID: 0,
            CO1_ID: 0,
            CA1_NAME: '',
            CA1_DESCRIPTION:  '',
            CA1_NO_INVENTORY: 0,
            CA1_CREATED_BY:   '',
            CA1_CREATED_ON:   '',
            CA1_MODIFIED_BY:  '',
            CA1_MODIFIED_ON:  ''
        }
    }
}
