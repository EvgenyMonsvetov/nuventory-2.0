import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';
import {_lang} from '../pipes/lang';
import {ToastrService} from 'ngx-toastr';

@Injectable()
export class StatementService extends GeneralService {

    constructor(protected http: HttpClient,private toastr: ToastrService) {
        super(http);

    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/statements/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/statements', { params : params });
    }

    public generateStatement(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/statement/generate-statement', { params : params });
    }

    public generateAllInvoices(): Observable<IResponse<any>> {
        return this.http.get<IResponse<any>>(this.apiUrl + '/invoice/generate-all-invoices');
    }

    preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/statement/preload', { params : params });
    }

    public deleteMultiple(IS1_IDs) {
        return this.http.post(this.apiUrl + '/statement/delete-multiple', { IS1_IDs: IS1_IDs})
            .catch(this.handleError);
    }

    public printPDF(page: any): Observable<any> {
        const apiUrl = '/statement/print-pdf';
        const defaultFileName = 'Statements.pdf';
        return this.getPdf(apiUrl, page, defaultFileName, true);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/statement/export';
        const defaultFileName = 'Statements.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    public send(IS1_IDs): Observable<any> {
        const apiUrl = '/statement/send';
        return this.http.post(this.apiUrl + apiUrl, { IS1_IDs: IS1_IDs, hours_offset: this.getUTCOffsetHours() })
            .map(
                data => this.extractPayload(data)
            );
    }

    initialize(): any {
        // Return an initialized object
        return {
            IS1_ID: 0,
            CO1_ID: null,
            US1_ID: null,
            IS1_COMMENT: null,
            IS1_NUMBER: null,
            IS1_STATUS: null,
            IS1_STATUS_TEXT: null,
            IS1_TOTAL_VALUE: null,
            LO1_FROM_ID: null,
            FROM_NAME: null,
            LO1_TO_ID: null,
            TO_NAME: null,
            IS1_DELETE_FLAG: null,
            IS1_MODIFIED_BY: null,
            IS1_CREATED_BY: null,
            CREATED_ON: null,
            CREATED_BY: null,
            MODIFIED_BY: null,
            MODIFIED_ON: null
        };
    }
}
