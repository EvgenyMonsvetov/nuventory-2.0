import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class PriceService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getItems(page:any, url:any=null): Observable<IResponse<object>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }

        var url = url || '/prices';
        return this.http.get<IResponse<object>>(this.apiUrl + url, { params : params });
    }

    public downloadXlsx(page: any, subtype: string = ''): Observable<any> {
        const apiUrl = '/price/export';
        const defaultFileName = 'PriceChangeLog.xls';
        return this.getXlsx(apiUrl, page, defaultFileName, subtype);
    }

}
