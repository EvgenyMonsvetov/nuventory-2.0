import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import { saveAs } from '@progress/kendo-file-saver';
import { Page } from '../model/page';
import moment from 'moment';

export class GeneralService {

    public static CO1_ID: string = null;
    public static CO1_NAME: string = '';
    public static LO1_ID: string = null;
    public static LO1_NAME: string = '';

    constructor(protected http: HttpClient) {
    }

    protected get apiUrl() {
        if (environment.production || environment.testing) {
            return location.origin;
        } else {
            return environment.API_URL;
        }
    }

    protected handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json ? error.json() : 'Server error');
    }

    protected extractPayload(response) {
        return response.data || {}
    }

    protected getXlsx(apiUrl: string, page: any, defaultFileName: string, subtype: string = '') {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        params = params.append('type', 'xls');
        params = params.append('subtype', subtype);
        params = params.append('hours_offset', this.getUTCOffsetHours());

        return this.http.get(this.apiUrl + apiUrl, { params, responseType: 'blob', observe: 'response'} ).map(
            response => {
                const headers: HttpHeaders = response.headers;
                const disposition = headers.get('Content-Disposition');
                if (disposition) {
                    const match = disposition.match(/.*filename=\"?([^;\"]+)\"?.*/);
                    if (match[1])
                        defaultFileName = match[1];
                }
                defaultFileName = defaultFileName.replace(/[<>:"\/\\|?*]+/g, '_');
                const blob = new Blob([response.body], {type: 'application/vnd.ms-excel'});
                saveAs(blob, defaultFileName);
            })
            .catch(this.handleError);
    }

    protected getPdf(apiUrl: string, page: any = null, defaultFileName: string, autoPrint = false) {
        let params = new HttpParams();
        if (page) {
            params = this.applyFilters(page, params);
            params = params.append('perPage', page.size);
            params = params.append('page', page.pageNumber);
        }
        params = params.append('fileName', defaultFileName);
        params = params.append('hours_offset', this.getUTCOffsetHours());

        return this.http.get(this.apiUrl + apiUrl, { params, responseType: 'blob', observe: 'response'} ).map(
            response => {
                const headers: HttpHeaders = response.headers;
                const disposition = headers.get('Content-Disposition');
                if (disposition) {
                    const match = disposition.match(/.*filename=\"?([^;\"]+)\"?.*/);
                    if (match[1])
                        defaultFileName = match[1];
                }
                defaultFileName = defaultFileName.replace(/[<>:"\/\\|?*]+/g, '_');
                const blob = new Blob([response.body], {type: 'application/pdf'});

                // IE doesn't allow using a blob object directly as link href
                // instead it is necessary to use msSaveOrOpenBlob
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(blob);
                    return;
                }

                // For other browsers:
                // Create a link pointing to the ObjectURL containing the blob.
                const data = window.URL.createObjectURL(blob);
                let w = window.open(data);
                if (autoPrint) {
                    w.print();
                }
            })
            .catch(this.handleError);
    }

    protected applyFilters(page: Page, params: HttpParams){
        if (!_.isUndefined(page.filterParams)) {
            for (const key of Object.keys(page.filterParams)) {
                const value = page.filterParams[key];
                if (value && value !== '' || value === 0) {
                    params = params.append(key, value);
                }
            }
        }
        return params;
    }

    uploadFile(fileToUpload: File, type: string = 'image', fileName: string = '', CO1_ID: string = '', LO1_IDs: string = '', uploadUrl='uploads'): Observable<any> {
        const formData: FormData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        formData.append('type', type);
        formData.append('fileName', fileName);
        formData.append('CO1_ID', CO1_ID);
        formData.append('LO1_IDs', LO1_IDs);
        return this.http.post(this.apiUrl + '/' + uploadUrl, formData)
            .catch(this.handleError);
    }
    
    getUTCOffsetHours() {
        return ((moment().utcOffset()) / 60).toString();
    }

    clearSaveBarcode( context, targetField ){
        if( /\+?save/i.test(context[targetField]) ){
            context[targetField] = context[targetField].replace(/\+?save/igm, "");
        }
    }
}
