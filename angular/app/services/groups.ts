import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {_lang} from "../pipes/lang";

@Injectable()
export class GroupsService extends GeneralService {

    static GROUP_ADMIN:number = 0;
    static GROUP_COMPANY_GROUP_MANAGER:number = 1;
    static GROUP_COMPANY_MANAGER:number = 2;
    static GROUP_STANDARD_USER:number = 3;
    static GROUP_LOCATION_MANAGER:number = 4;
    static GROUP_MANUFACTURER_MANAGER:number = 5;

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getAccessLevelList()
    {
        return [{
            'GR1_ACCESS_LEVEL': GroupsService.GROUP_ADMIN,
            'GR1_ACCESS_LEVEL_LANG': 'GROUP_ADMIN',
            'GR1_ACCESS_LEVEL_DESC': _lang('GROUP_ADMIN')
        },{
            'GR1_ACCESS_LEVEL': GroupsService.GROUP_COMPANY_GROUP_MANAGER,
            'GR1_ACCESS_LEVEL_LANG': 'GROUP_COMPANY_GROUP_MANAGER',
            'GR1_ACCESS_LEVEL_DESC': _lang('GROUP_COMPANY_GROUP_MANAGER')
        },{
            'GR1_ACCESS_LEVEL': GroupsService.GROUP_COMPANY_MANAGER,
            'GR1_ACCESS_LEVEL_LANG': 'GROUP_COMPANY_MANAGER',
            'GR1_ACCESS_LEVEL_DESC': _lang('GROUP_COMPANY_MANAGER')
        },{
            'GR1_ACCESS_LEVEL': GroupsService.GROUP_STANDARD_USER,
            'GR1_ACCESS_LEVEL_LANG': 'GROUP_STANDARD_USER',
            'GR1_ACCESS_LEVEL_DESC': _lang('GROUP_STANDARD_USER')
        },{
            'GR1_ACCESS_LEVEL': GroupsService.GROUP_LOCATION_MANAGER,
            'GR1_ACCESS_LEVEL_LANG': 'GROUP_LOCATION_MANAGER',
            'GR1_ACCESS_LEVEL_DESC': _lang('GROUP_LOCATION_MANAGER')
        },{
            'GR1_ACCESS_LEVEL': GroupsService.GROUP_MANUFACTURER_MANAGER,
            'GR1_ACCESS_LEVEL_LANG': 'GROUP_MANUFACTURER_MANAGER',
            'GR1_ACCESS_LEVEL_DESC': _lang('GROUP_MANUFACTURER_MANAGER')
        } ]
    }

    public getItems(page): Observable<IResponse<object>> {
        const current_page = page.pageNumber;
        let params = new HttpParams();
        params = params.append('perPage', page.size);
        params = params.append('page', current_page);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort', key + ',' + value );
            }
        }
        if (!_.isUndefined(page.filterParams)) {
            for (const key of Object.keys(page.filterParams)) {
                const value = page.filterParams[key];
                if (value && value !== '') {
                    params = params.append('filter', key + ',' + value );
                }
            }
        }
        return this.http.get<IResponse<object>>(this.apiUrl + '/groups', { params : params });
    }

    public getAll(): any {
        return this.http.get<any>(this.apiUrl + '/groups');
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/groups/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    create(group: any): Observable<any> {
        return this.http.post(this.apiUrl + '/groups', group)
            .catch(this.handleError);
    }

    update(group: any): Observable<any> {
        const url = `${this.apiUrl + '/groups'}/${group.GR1_ID}`;
        return this.http.put(url, group)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/groups/' + id);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/group/export';
        const defaultFileName = 'Groups.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize() {
        return {
            GR1_ID: null,
            GR1_NAME: '',
            GR1_DESC: '',
            GR1_ACCESS_LEVEL: 0
        }
    }
}
