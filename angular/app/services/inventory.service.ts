import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { Page } from '../model/page';
import {HeaderService} from "./header.service";
import {_lang} from "../pipes/lang";

@Injectable()
export class InventoryService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getActivityLog(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/inventory/activity-log', { params : params });
    }

    public preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/inventory/preload', { params : params });
    }

    public printPDF(page: any): Observable<any> {
        const apiUrl = '/inventory/print-pdf';
        const defaultFileName = 'InventoryLog.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public downloadXlsx(page: any, subtype: string = ''): Observable<any> {
        const apiUrl = '/inventory/export';
        const defaultFileName = 'InventoryActivities.xls';
        return this.getXlsx(apiUrl, page, defaultFileName, subtype);
    }

    public getManualInventoryEdits(page: Page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        const url = page.url || '/inventory/manual-inventory-edits';
        return this.http.get<IResponse<any>>(this.apiUrl + url, { params : params });
    }

    public isEditInventoryCanBeCreated(headerService: HeaderService) {
        var canBeCreated = false;
        var error = '';
        if (!headerService.LO1_ID) {
            error = _lang('Select a Shop at the top panel!');
        }
        if (!error.length) {
            canBeCreated = true;
        }
        return {
            canBeCreated: canBeCreated,
            error: error
        }
    }

}
