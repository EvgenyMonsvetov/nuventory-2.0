import { Injectable } from '@angular/core';
import { StoreService } from './store.service';
import { HttpClient } from '@angular/common/http';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class TimezonesService extends GeneralService{
  private store: StoreService;

  constructor(protected http: HttpClient) {
    super(http);

    this.store = new StoreService('timezones', 'name');
  }

  getStore(): StoreService {
    return this.store;
  }

  getStoreTimezones() {
    return this.getStore().items;
  }

  public getAllTimezones() {
    const url = `${this.apiUrl}/timezones/`;
    return this.http.get(url)
        .map(
            data => {return this.parseTimezones(data, 'Name')}
        )
        .catch(this.handleError);
  }

  parseTimezones(value, gr: string): any {
    let groups = {};
    value.forEach(function(group) {
      const name = group[gr];
      groups[name] = groups[name] ? groups[name] : { 'name': name, list: [] };
      groups[name].list.push(group);
    });

    const source = Object.keys(groups).map(function (key) {return groups[key]});
    this.store.load(source);
    return source;
  }
}
