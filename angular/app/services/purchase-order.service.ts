import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';
import { HeaderService } from './header.service';
import { _lang } from '../pipes/lang';

@Injectable()
export class PurchaseOrderService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/purchase-orders/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/purchase-orders', { params : params });
    }

    public isPOCanBeCreated(headerService: HeaderService){
      var canBeCreated = false;
      var errors = [];

      if (!headerService.LO1_ID) {
          errors.push( _lang('current location is not selected') );
      }
      if (!errors.length) {
          canBeCreated = true;
      }

      var error = '';
      if(errors.length) {
          error = _lang('Purchase Order can not be created! The reason is ') + errors.join('<br/>');
      }

      return {
          canBeCreated: canBeCreated,
          error: error
      }
    }

    public preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/purchase-order/preload', { params : params });
    }

    public create(orders: any): Observable<any> {
        return this.http.post(this.apiUrl + '/purchase-orders', {orders: orders, hours_offset: this.getUTCOffsetHours()})
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public update(item: any): Observable<any> {
        return this.http.post(this.apiUrl + '/purchase-orders', item)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public receive( PO1_ID:number, orderLines: any ): Observable<any> {
        const url = `${this.apiUrl}/purchase-orders/receive`;
        return this.http.post(url, {PO1_ID: PO1_ID, orderLines: orderLines })
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public upload( PO1_IDs :number ): Observable<any> {
        const url = `${this.apiUrl}/purchase-orders/upload`;
        return this.http.post(url, {PO1_IDs: PO1_IDs })
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/purchase-orders/' + id, {});
    }

    public deleteMultiple(PO1_IDs){
        return this.http.post(this.apiUrl + '/purchase-order/delete-multiple', { PO1_IDs: PO1_IDs })
            .catch(this.handleError);
    }

    public initOrder(page: any): Observable<any>{
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);

        return this.http.get<IResponse<any>>(this.apiUrl + '/purchase-order/init-order', { params : params });
    }

    public printPDF(page: any): Observable<any> {
        const apiUrl = '/purchase-order/print-pdf';
        const defaultFileName = 'PurchaseOrders.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/purchase-order/export';
        const defaultFileName = 'PurchaseOrders.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    public getDashboardCharts(page: any = null) {
        let params = new HttpParams();
        if (page) {
            params = this.applyFilters(page, params);
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/purchase-order/dashboard-charts', { params : params });
    }
    
    public initialize(): any {
        // Return an initialized object
        return {
            PO1_ID: 0,
            PO1_ORDERED_BY: null,
            PO1_NUMBER: '',
            PO1_SHIPPING_METHOD: 'None',
            PO1_STATUS: null,
            PO1_UPLOAD_STATUS: null,
            PO1_TOTAL_VALUE: 0,
            PO1_COMMENT: '',
            PO1_DELETE_FLAG: 0,
            LO1_SHORT_CODE: '',
            LO1_NAME: '',
            VD1_ID: 0,
            VD1_NAME: '',
            VD1_SHORT_CODE: '',
            CA1_ID: null,
            CA1_NAME: '',
            PO1_STATUS_TEXT: null,
            CO1_ID: null,
            CO1_NAME: '',
            PO1_UPLOAD_STATUS_TEXT: '',
            CREATED_ON: null,
            MODIFIED_ON: null,
            ORDERED_ON: null,
            PO1_MODIFIED_BY: null,
            PO1_CREATED_BY: null,
            CREATED_BY: null,
            ORDERED_BY: null,
            MODIFIED_BY: null
        };
    }
}
