import { HttpClient } from '@angular/common/http';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AccessListService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }
}
