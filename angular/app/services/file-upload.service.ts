import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';

@Injectable()
export class FileUploadService extends GeneralService {

  constructor(protected http: HttpClient) {
    super(http);
  }
}
