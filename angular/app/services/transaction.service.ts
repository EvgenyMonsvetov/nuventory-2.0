import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {Transaction} from '../model/transaction';

@Injectable()
export class TransactionService extends GeneralService {

    static STATUS_NOT_SENT   = '0';
    static STATUS_PROCESSING = '1';
    static STATUS_SENT       = '2';
    static STATUS_FAILED     = '3';
    static STATUS_RESENDING  = '10';

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getTransactions(page): Observable<IResponse<Transaction>> {
        const current_page = page.pageNumber;
        let params = new HttpParams();
        params = params.append('perPage', page.size);
        params = params.append('page', current_page);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        if (!_.isUndefined(page.filterParams)) {
            for (const key of Object.keys(page.filterParams)) {
                const value = page.filterParams[key];
                if (value && value !== '') {
                    params = params.append(key, value);
                }
            }
        }
        return this.http.get<IResponse<Transaction>>(this.apiUrl + '/transactions', { params : params });
    }

    preloadData(): Observable<any>{
        return this.http.get(this.apiUrl + '/transactions/preload');
    }

    getLogs( CO1_ID, ED1_ID ): Observable<any>{

        let params = new HttpParams();
        params = params.append('CO1_ID', CO1_ID);
        params = params.append('ED1_ID', ED1_ID);

        return this.http.get(this.apiUrl + '/transactions/logs', { params : params });
    }

    getTransactionsPerWeek(params:any = null): Observable<any> {
      const url = `${this.apiUrl}/transactions/report`;
      return this.http.get(url, { params : params })
        .catch(this.handleError);
    }

    getDetailsReport(params:any = null): Observable<any> {
      const url = `${this.apiUrl}/transactions/details-report`;
      return this.http.get(url, { params : params })
        .map(data => this.extractPayload(data))
        .catch(this.handleError);
    }

    getSeries(series:Array<any> = null): Observable<any> {

        let params = {
            'serie': []
        };
        series.forEach( (serie,index) => {
            params['serie'].push(serie);
        });

        const url = `${this.apiUrl}/transactions/series`;
            return this.http.post(url, params)
            .catch(this.handleError);
    }

    getTradingPartnersReport(params:any = null): Observable<any> {
      const url = `${this.apiUrl}/transactions/trading-partners-report`;
      return this.http.get(url, { params : params })
        .catch(this.handleError);
    }

    getInOutBoundReport(params:any = null): Observable<any> {
      const url = `${this.apiUrl}/transactions/in-out-bound-report`;
      return this.http.get(url, { params : params })
        .catch(this.handleError);
    }

    resend(selected:Array<any>): Observable<any>{
        const url = `${this.apiUrl}/transactions/resend`;
          return this.http.post(url, {items: JSON.stringify(selected)})
            .map(data => this.extractPayload(data))
            .catch(this.handleError);
    }
}
