import {Subject} from "rxjs";
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class HeaderService extends Subject<any> {

    private _CO1_ID: number = null;
    private _CO1_NAME: string = '';
    private _companies: Array<any> = [];
    private _LO1_ID: number = null;
    private _LO1_NAME: string = '';
    private _locations: Array<any> = [];
    private _lastValue: any;
    private _centralLocation: any = false;

    setCompany( CO1_ID: number){
        this._CO1_ID = CO1_ID;
    }

    setCompanyName( CO1_NAME: string){
        this._CO1_NAME = CO1_NAME;
    }

    setCompanies( items: Array<any>){
        this._companies = items;
    }

    setLocation( LO1_ID: number){
        this._LO1_ID = LO1_ID;
    }

    setLocationName( LO1_NAME: string){
        this._LO1_NAME = LO1_NAME;
    }

    setLocations( items: Array<any>){
        this._locations = items;
    }

    setCentralLocation( location ){
        this._centralLocation = location ? location : null;
    }

    public get centralLocation(){
        return this._centralLocation;
    }

    private _getSelectedCompany() {
        var me = this;
        var selectedCompany: any;
        this._companies.forEach( function(company){
            if(company.CO1_ID == me.CO1_ID){
                selectedCompany = company;
            }
        });
        return selectedCompany;
    }

    private _getSelectedLocation() {
        var me = this;
        var selectedLocation: any;
        this._locations.forEach( function(location){
            if(location.LO1_ID == me.LO1_ID){
                selectedLocation = location;
            }
        });
        return selectedLocation;
    }

    public getCurrentLocation()
    {
        return this._getSelectedLocation();
    }

    public next(value?: any) {
        if(!value){
            value = {
                CO1_ID: this.CO1_ID,
                selectedCompany: this._getSelectedCompany(),
                LO1_ID: this.LO1_ID,
                selectedLocation: this._getSelectedLocation(),
                companyLocations: this._locations
            }
        }
        this._lastValue = value;
        super.next(value);
    }

    public get lastValue(){
        return this._lastValue;
    }

    public get CO1_ID(): any{
        return this._CO1_ID;
    }

    public get CO1_NAME(){
        return this._CO1_NAME;
    }

    public get LO1_ID(): any{
        return this._LO1_ID;
    }

    public get LO1_NAME(){
        return this._LO1_NAME;
    }

    public isCentralLocation( LO1_ID ){
        var me = this;
        var location: any;

        this._locations.forEach( function(item){
            if(item.LO1_ID == LO1_ID){
                location = item;
            }
        });
        return parseInt(location.LO1_CENTER_FLAG) ? true : false;
    }

    public companyHasCentralLocation() {
        var me = this;
        var hasCentral: boolean = false;

        if( this._centralLocation && this._centralLocation.LO1_ID ){
            hasCentral = true;
        }else{
            this._locations.forEach( function(item) {
                if (parseInt(item.LO1_CENTER_FLAG) === 1) {
                    me.setCentralLocation(item);
                    hasCentral = true;
                }
            });
        }
        return hasCentral;
    }
}
