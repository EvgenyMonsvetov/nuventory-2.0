import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class EdiTypeService extends GeneralService implements EDIServices<Observable<object>, object, number> {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getItems(page): Observable<IResponse<object>> {
        const current_page = page.pageNumber;
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', current_page);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort', key + ',' + value );
            }
        }
        return this.http.get<IResponse<object>>(this.apiUrl + '/edi-types', { params : params });
    }

    getById(id: number): Observable<object> {

        const url = `${this.apiUrl}/edi-types/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    save(data: object, isCreate: boolean): Observable<object> {
        return this.update( data );
    }

    private update(editype: any): Observable<object> {
        const url = `${this.apiUrl + '/edi-types'}/${editype.id}`;
        return this.http.put(url, editype)
            .catch(this.handleError);
    }

    public _synch(): Observable<any>{
        const url = `${this.apiUrl + '/edi-types/synch'}`;
        return this.http.get(url)
            .catch(this.handleError);
    }
}
