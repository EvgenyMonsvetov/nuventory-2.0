import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';

@Injectable()
export class JobInvoiceService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/job-invoices/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/job-invoice', { params : params });
    }

    preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/job-invoice/preload', { params : params });
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/job-invoices/' + id, {});
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/job-invoice/export';
        const defaultFileName = 'JobInvoices.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    public printPDF(page: any): Observable<any> {
        const apiUrl = '/job-invoice/print-pdf';
        const defaultFileName = 'JobInvoices.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    initialize(): any {
        // Return an initialized object
        return {
            IJ1_ID: 0,
            JT1_ID: null,
            CO1_ID: null,
            IJ1_NUMBER: null,
            JT1_NUMBER: null,
            IN1_NUMBER: null,
            IJ1_TOTAL_VALUE: null,
            ORDER_BY_COMPANY_CODE: null,
            ORDER_BY_COMPANY_NAME: null,
            IJ1_TOTAL_SELL: null,
            IJ1_COMMENT: null,
            LO1_ID: null,
            ORDER_BY_LOCATION_CODE: null,
            ORDER_BY_LOCATION_NAME: null,
            IJ1_CREATED_BY: null,
            IJ1_MODIFIED_BY: null,
            MODIFIED_ON: null,
            CREATED_ON: null,
            CREATED_BY: null,
            MODIFIED_BY: null
        };
    }
}
