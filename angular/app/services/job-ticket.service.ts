import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';
import { HeaderService } from './header.service';
import { _lang } from '../pipes/lang';

@Injectable()
export class JobTicketService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/job-tickets/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
            return this.http.get<IResponse<any>>(this.apiUrl + '/job-tickets', { params : params });
    }

    public initTicket(page: any): Observable<any>{
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);

        return this.http.get<IResponse<any>>(this.apiUrl + '/job-ticket/init-ticket', { params : params });
    }

    public isCanBeCreated(headerService: HeaderService){
      var canBeCreated = false;
      var errors = [];

      if (!headerService.LO1_ID) {
          errors.push( _lang('current location is not selected') );
      } else if (parseInt(headerService.getCurrentLocation()['LO1_CENTER_FLAG'])) {
          errors.push( _lang('current location is Central Warehouse') );
      }
      if (!errors.length) {
          canBeCreated = true;
      }

      var error = '';
      if (errors.length) {
          error = _lang('Job Ticket can not be created! The reason is ') + errors.join('<br/>');
      }

      return {
          canBeCreated: canBeCreated,
          error: error
      }
    }

    preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/job-ticket/preload', { params : params });
    }

    public create(orders: any): Observable<any> {
        return this.http.post(this.apiUrl + '/job-tickets', {orders: orders})
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public update(item: any): Observable<any> {
        const url = `${this.apiUrl + '/job-tickets'}/${item.JT1_ID}`;
        return this.http.put(url, item)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/job-tickets/' + id, {});
    }

    public deleteMultiple(JT1_IDs){
        return this.http.post(this.apiUrl + '/job-ticket/delete-multiple', { JT1_IDs: JT1_IDs })
            .catch(this.handleError);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/job-ticket/export';
        const defaultFileName = 'JobTickets.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    public printPDF(page: any): Observable<any> {
        const apiUrl = '/job-ticket/print-pdf';
        const defaultFileName = 'JobTickets.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    initialize(): any {
        // Return an initialized object
        return {
            JT1_ID: 0,
            CO1_ID: 0,
            CU1_ID: 0,
            LO1_ID: 0,
            VD1_ID: null,
            VD1_NAME: '',
            JT1_TOTAL_VALUE: null,
            JT1_COMMENT: '',
            CO1_SHORT_CODE: '',
            CO1_NAME: '',
            LO1_SHORT_CODE: '',
            LO1_NAME: '',
            JT1_STATUS: 0,
            JT1_STATUS_TEXT: null,
            JT1_NUMBER: null,
            MODIFIED_ON: null,
            CREATED_ON: null,
            ORDERED_ON: null,
            JT1_MODIFIED_BY: null,
            CREATED_BY: null,
            MODIFIED_BY: null
        };
    }
}
