import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class RawSalesDataStoreMonthService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/raw-sales-data-store-month', { params : params });
    }

    getSalesReport(page) {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/raw-sales-data-store-month/sales', { params : params });
    }

    getGrossProfitReport(page) {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/raw-sales-data-store-month/gross-profit', { params : params });
    }

    getSummaryReport(page) {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/raw-sales-data-store-month/summary', { params : params });
    }

    getSummaryChartReport(page) {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/raw-sales-data-store-month/summary-chart', { params : params });
    }

    getPMGrossProfitReport(page) {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/raw-sales-data-store-month/pm-gross-profit', { params : params });
    }

    getHighBarChartReport(page) {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/raw-sales-data-store-month/high-bar-chart', { params : params });
    }

    getAverageCircleChartReport(page) {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        return this.http.get<IResponse<any>>(this.apiUrl + '/raw-sales-data-store-month/average-circle-chart', { params : params });
    }

    public downloadXlsx(page: any, subtype: string = 'dash'): Observable<any> {
        const apiUrl = '/raw-sales-data-store-month/export';
        let defaultFileName;
        switch (subtype) {
            case 'summary':
                defaultFileName = 'SummaryReport.xls';
                break;
            default:
                defaultFileName = 'DashReport.xls';
                break;
        }
        return this.getXlsx(apiUrl, page, defaultFileName, subtype);
    }

    initialize(): any {
        // Return an initialized object
    }
}
