import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {IUser} from '../views/user/User';
import {StoreService} from "./store.service";

@Injectable()
export class UserService extends GeneralService implements EDIServices<Observable<IUser>, IUser, number> {

    static GROUP_ADMIN                 = 0;
    static GROUP_COMPANY_GROUP_MANAGER = 1;
    static GROUP_COMPANY_MANAGER       = 2;
    static GROUP_STANDARD_USER         = 3;
    static GROUP_LOCATION_MANAGER      = 4;
    static GROUP_MANUFACTURER_MANAGER  = 5;

    private store: StoreService;

    constructor(protected http: HttpClient) {
        super(http);
        this.store = new StoreService('users', 'id');
    }

    getStore(): StoreService{
        return this.store;
    }

    getById(id: number): Observable<any> {
        if (id == 0) {
            return of(this.initialize());
        }else{
            let user = this.store.getById(id);
            if(user){
                return Observable.of(user);
            }else{
                const url = `${this.apiUrl}/users/${id}`;
                return this.http.get(url)
                    .map(
                        data => this.extractPayload(data)
                    )
                    .do(data => this.store.add(data))
                    .catch(this.handleError);
            }
        }
    }

    save(user: any, isCreate: boolean): Observable<any> {
        if (user.US1_ID === 0 || isCreate) {
            return this.create(user);
        }
        return this.update(user);
    }

    changePassword(data) {
        const url = `${this.apiUrl + '/users/change-password/'}`;
        return this.http.put(url, data)
            .catch(this.handleError);
    }

    public getUsers(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/users', { params : params });
    }

    public create(user: IUser): Observable<any> {
        return this.http.post(this.apiUrl + '/users', user)
            .catch(this.handleError);
    }

    public createUsers(users: any[]): Observable<any> {
        return this.http.post(this.apiUrl + '/user/create-users', users)
            .catch(this.handleError);
    }

    public update(user: any): Observable<any> {
        const url = `${this.apiUrl + '/users'}/${user.US1_ID}`;
        return this.http.put(url, user)
            .do(data => this.store.update(data))
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/users/' + id);
    }

    public getAllUsers() {
        return JSON.parse(localStorage.getItem('users')) || [];
    }

    public preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/user/preload', { params : params });
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/user/export';
        const defaultFileName = 'Users.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    changePasswordAdmin(data) {
        const url = `${this.apiUrl + '/users/admin-change-password/'}`;
        return this.http.put(url, data)
            .catch(this.handleError);
    }

    initialize(): IUser {
        // Return an initialized object
        return {
            US1_ID: 0,
            US1_ACTIVE: true,
            US1_LOGIN: null,
            US1_EMAIL: '',
            password: null,
            repeat_password: null,
            US1_NAME: null,
            VD0_ID: 0,
            CG1_ID: 0,
            US1_TYPE: 0,
            errors: null,
            DELETED: 0
        };
    }
}
