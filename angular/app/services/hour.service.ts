import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class HourService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getHours(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        if (!_.isUndefined(page.size)) {
            params = params.append('perPage', page.size);
        }
        if (!_.isUndefined(page.pageNumber)) {
            params = params.append('page', page.pageNumber);
        }
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }

        return this.http.get<IResponse<any>>(this.apiUrl + '/hours', { params : params });
    }

    getTechHours(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        if (!_.isUndefined(page.size)) {
            params = params.append('perPage', page.size);
        }
        if (!_.isUndefined(page.pageNumber)) {
            params = params.append('page', page.pageNumber);
        }
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }

        return this.http.get<IResponse<any>>(this.apiUrl + '/hours/tech-hours', { params : params });
    }

    public save(hour: any): Observable<any> {
        return this.http.post(this.apiUrl + '/hours', hour)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public updateMultiple(hours: any): Observable<any> {
        return this.http.put(this.apiUrl + '/hours/update-multiple', {
            data: hours
        })
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public preloadData(): Observable<any> {
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/hour/preload', { params : params });
    }
    public delete(id: any) {
        return this.http.post<IResponse<any>>(this.apiUrl + '/hour/delete', { id : id });
    }

    initialize(): any {
        // Return an initialized object
        return {
            HO1_ID: 0,
            CO1_ID:             null,
            LO1_ID:             null,
            TE1_ID:             null,
            HO1_HOURS:          0,
            HO1_DATE:           null,
            HO1_DELETE_FLAG:    0,
            HO1_CREATED_BY:     null,
            HO1_CREATED_ON:     null,
            HO1_MODIFIED_BY:     null,
            HO1_MODIFIED_ON:     null
        };
    }

}
