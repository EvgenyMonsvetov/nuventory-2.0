interface EDIServices<T, T2, T3> {
    getById(id: T3, CO1_ID: T3): T;
    save(model: T2, isCreate: boolean): T;
}
