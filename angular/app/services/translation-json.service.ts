import { Injectable } from '@angular/core';
import ParsedJson from '../../resource.json';

@Injectable({
  providedIn: 'root'
})
export class TranslationJsonService {

  constructor() { }

  public getAll(): any {
    let ids = [];
    let obj = ParsedJson;

    for (let key in obj) {
      const value = obj[key];
      value.forEach(function(id) {
        if (ids.indexOf(id) === -1)
          ids.push(id);
      })
    }

    return ids;
  }

  public getKeysByClassName(value): any {
    return ParsedJson[value];
  }
}
