import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class c00CategoryService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getItems(page): Observable<IResponse<object>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<object>>(this.apiUrl + '/c00-categories', { params : params });
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return Observable.of(this.initialize());
        }

        const url = `${this.apiUrl}/c00-categories/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    create(category: any): Observable<any> {
        return this.http.post(this.apiUrl + '/c00-categories', category)
            .catch(this.handleError);
    }

    update(category: any): Observable<any> {
        const url = `${this.apiUrl + '/c00-categories'}/${category.C00_ID}`;
        return this.http.put(url, category)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/c00-categories/' + id);
    }

    public deleteMultiple(C00_IDs) {
        return this.http.post(this.apiUrl + '/c00-category/delete-multiple', { C00_IDs: C00_IDs })
            .catch(this.handleError);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/c00-category/export';
        const defaultFileName = 'NuventoryCategories.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize() {
        return {
            C00_ID: null,
            C00_NAME: '',
            C00_DESCRIPTION: ''
        }
    }
}
