import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class MasterCategoriesService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getItems(page): Observable<IResponse<object>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<object>>(this.apiUrl + '/master-categories', { params : params });
    }

    getById(id: number): Observable<any> {

        if (id === 0) {
            return Observable.of(this.initialize());
        }

        const url = `${this.apiUrl}/master-categories/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    create(um: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-categories', um)
            .catch(this.handleError);
    }

    update(um: any): Observable<any> {
        const url = `${this.apiUrl + '/master-categories'}/${um.CA0_ID}`;
        return this.http.put(url, um)
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/master-categories/' + id);
    }

    public deleteMultiple(CA0_IDs) {
        return this.http.post(this.apiUrl + '/master-category/delete-multiple', { CA0_IDs: CA0_IDs })
            .catch(this.handleError);
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/master-category/export';
        const defaultFileName = 'MasterCategories.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize(){
        return {
            CA0_ID: null,
            CO1_ID: '',
            CA0_NAME: '',
            CA0_DESCRIPTION: '',
            CA0_CREATED_ON: '',
            CA0_CREATED_BY: '',
            CA0_MODIFIED_ON: '',
            CA0_MODIFIED_BY: ''
        }
    }
}
