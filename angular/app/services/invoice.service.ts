import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { of } from 'rxjs/index';
import { HeaderService } from './header.service';
import { _lang } from '../pipes/lang';

@Injectable()
export class InvoiceService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/invoices/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getAll(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/invoices', { params : params });
    }

    public getAllCreditMemos(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/invoice/credit-memos', { params : params });
    }

    public createFromStoreOrder(OR1_IDs: any){
        const url = `${this.apiUrl}/invoice/create-from-store-order`;
        return this.http.post(url, {OR1_IDs: OR1_IDs})
        .map(
            data => this.extractPayload(data)
        )
        .catch(this.handleError);
    }

    public getLinkHeaders(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<any>>(this.apiUrl + '/invoice/link-headers', { params : params });
    }

    preloadData(): Observable<any>{
        let params = new HttpParams();
        params = params.append('perPage', '0');
        params = params.append('page', '1');
        return this.http.get<IResponse<any>>(this.apiUrl + '/invoice/preload', { params : params });
    }

    public create( orders: any, IN1_NUMBER: number){
        return this.http.post(this.apiUrl + '/invoices', {orders: orders, IN1_NUMBER: IN1_NUMBER})
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/invoices/' + id, {});
    }

    public deleteMultiple(IN1_IDs) {
        return this.http.post(this.apiUrl + '/invoice/delete-multiple', { IN1_IDs: IN1_IDs })
            .catch(this.handleError);
    }

    public isInvoiceCanBeCreated(headerService: HeaderService) {
        var canBeCreated = false;
        // var errors = [];
        //
        // var v = headerService.lastValue;
        // if( v ) {
        //     if (!v['LO1_ID']) {
        //         errors.push( _lang('current location is not selected') );
        //     } else if (parseInt(headerService.getCurrentLocation()['LO1_CENTER_FLAG'])) {
        //         errors.push( _lang('current location is Central Warehouse') );
        //     }
        //     if (!errors.length) {
                canBeCreated = true;
            // }
        // }

        var error = '';
        // if (errors.length) {
        //     error = _lang('Invoice can not be created! The reason is ') + errors.join('<br/>');
        // }

        return {
            canBeCreated: canBeCreated,
            error: error
        }
    }

    public isCreditMemoCanBeCreated(headerService: HeaderService) {
        var canBeCreated = false;
        var errors = [];

        if (!headerService.LO1_ID) {
            errors.push( _lang('current location is not selected') );
        } else if (!parseInt(headerService.getCurrentLocation()['LO1_CENTER_FLAG'])) {
            errors.push( _lang('current location is not a Central Warehouse') );
        }
        if (!errors.length) {
            canBeCreated = true;
        }

        var error = '';
        if (errors.length) {
             error = _lang('Credit Memo can not be created! The reason is ') + errors.join('<br/>');
        }

        return {
            canBeCreated: canBeCreated,
            error: error
        }
    }

    initInvoice(page: any): Observable<any>{
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);

        return this.http.get<IResponse<any>>(this.apiUrl + '/invoice/init-invoice', { params : params });
    }

    public printPDF(page: any = null): Observable<any> {
       const apiUrl = '/invoice/print-pdf';
        const defaultFileName = 'Invoices.pdf';
        return this.getPdf(apiUrl, page, defaultFileName);
    }

    public downloadXlsx(page: any, subtype: string): Observable<any> {
        const apiUrl = '/invoice/export';
        const defaultFileName = 'Invoices.xls';
        return this.getXlsx(apiUrl, page, defaultFileName, subtype);
    }

    initialize(): any {
        // Return an initialized object
        return {
            IN1_ID: 0,
            CO1_ID: 0,
            OR1_ID: 0,
            LO1_FROM_ID: 0,
            LO1_TO_ID: 0,
            IN1_NUMBER: null,
            OR1_NUMBER: '',
            IN1_TOTAL_VALUE: null,
            IN1_COMMENT: '',
            CO1_SHORT_CODE: '',
            CO1_NAME: '',
            LO1_FROM_SHORT_CODE: '',
            LO1_FROM_NAME: '',
            LO1_TO_SHORT_CODE: '',
            LO1_TO_NAME: '',
            IN1_STATUS: 0,
            IN1_DELETE_FLAG: null,
            IN1_MODIFIED_BY: null,
            IN1_CREATED_BY: null,
            INVOICEED_ON: null,
            COMPLETED_ON: null,
            CREATED_ON: null,
            CREATED_BY: null,
            MODIFIED_BY: null,
            ORDERED_BY: null
        };
    }
}
