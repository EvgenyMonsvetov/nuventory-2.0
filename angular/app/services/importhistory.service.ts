import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class ImportHistoryService extends GeneralService {

    static TYPE_TRANSACTIONS = '1';
    static TYPE_LOGS         = '2';
    static TYPE_CUSTOMERS    = '3';
    static TYPE_VENDORS      = '4';

    constructor(protected http: HttpClient) {
        super(http);
    }

    public getItems(page): Observable<IResponse<object>> {
        const current_page = page.pageNumber;
        let params = new HttpParams();
        params = params.append('perPage', page.size);
        params = params.append('page', current_page);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        if (!_.isUndefined(page.filterParams)) {
            for (const key of Object.keys(page.filterParams)) {
                const value = page.filterParams[key];
                if (value && value !== '') {
                    params = params.append(key, value);
                }
            }
        }
        return this.http.get<IResponse<object>>(this.apiUrl + '/import-histories', { params : params });
    }

    getById(id: number): Observable<object> {

        const url = `${this.apiUrl}/import-histories/${id}`;
        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    preloadData(): Observable<any>{
        return this.http.get(this.apiUrl + '/import-histories/preload');
    }
}