import * as _ from 'lodash';

export class GridFiltersService {

  prepare( filters ){

    var results = {};
    for( let key of Object.keys(filters) ){
        let value = filters[key];
        if(_.isArray(value)){
            _.forEach(value, function (item, index) {
              if( item.name && item.name.indexOf(':') > -1 ){
                let data = item.name.split(':');
                results[data[0]+'['+index+']'] = data[1];
              }else{
                results[key+'['+index+']'] = item;
              }
            });
        }else{
            if( typeof value !== 'undefined'){
                results[key] = value;
            }
        }
    }
    return results;
  }
}
