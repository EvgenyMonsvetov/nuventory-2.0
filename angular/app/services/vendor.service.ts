import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GeneralService} from './general.service';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {Vendor} from '../model/vendor';
import {IVendor} from '../model/interfaces/IVendor';
import { of } from 'rxjs/index';

@Injectable()
export class VendorService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    getById(id: number): Observable<any> {
        if (id === 0) {
            return of(this.initialize());
        }

        const url = `${this.apiUrl}/vendors/${id}`;

        return this.http.get(url)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public getVendors(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<Vendor>>(this.apiUrl + '/vendors', { params : params });
    }

    public getAllMasterVendors(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        if (!_.isUndefined(page.sortParams)) {
            for (const key of Object.keys(page.sortParams)) {
                const value = page.sortParams[key];
                params = params.append('sort[]', key + ',' + value );
            }
        }
        return this.http.get<IResponse<Vendor>>(this.apiUrl + '/vendor/master-vendors', { params : params });
    }

    public create(vendor: any): Observable<any> {
        return this.http.post(this.apiUrl + '/vendors', vendor)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public update(vendor: any): Observable<any> {
        return this.http.post(this.apiUrl + '/vendors', vendor)
            .map(
                data => this.extractPayload(data)
            )
            .catch(this.handleError);
    }

    public delete(id: number) {
        return this.http.delete<any>(this.apiUrl + '/vendors/' + id, {});
    }

    public getAllParts(page): Observable<IResponse<any>> {
        let params = new HttpParams();
        params = this.applyFilters(page, params);

        params = params.append('perPage', page.size);
        params = params.append('page', page.pageNumber);
        return this.http.get<IResponse<any>>(this.apiUrl + '/vendor/parts', { params : params });
    }

    public downloadXlsx(page: any): Observable<any> {
        const apiUrl = '/vendor/export';
        const defaultFileName = 'Vendors.xls';
        return this.getXlsx(apiUrl, page, defaultFileName);
    }

    initialize(): IVendor {
        // Return an initialized object
        return {
            VD1_ID: 0,
            VENDOR_ID: null,
            VD1_NAME: null,
            VD1_CREATED_BY: null,
            VD1_CREATED_ON: null,
            VD1_MODIFIED_BY: null,
            VD1_MODIFIED_ON: null,
            VD1_SHOW_DEFAULT: null,
            VD1_COMPANY_ID: null,
            VD1_RECEIVE_EDI: null,
            VD1_SEND_EDI_PO: null,
            VD1_SEND_ACKNOWLEDGEMENT: null,
            VD1_PO_FORMAT: null,
            VD1_SEND_FTP: null,
            VD1_SEND_SFTP: null,
            VD1_POST_HTTP: null,
            VD1_RECEIVE_FTP: null,
            VD1_PICKUP_FTP: null,
            VD1_PICKUP_SFTP: null,
            VD1_RECEIVE_HTTP: null,
            VD1_REMOTE_FTP_SERVER: null,
            VD1_REMOTE_FTP_USERNAME: null,
            VD1_REMOTE_FTP_PASSWORD: null,
            VD1_REMOTE_FTP_DIRECTORY_SEND: null,
            VD1_REMOTE_FTP_DIRECTORY_PICKUP: null,
            VD1_FTP_USER: null,
            VD1_FTP_PASSWORD: null,
            VD1_FTP_DIRECTORY: null,
            VD1_REMOTE_HTTP_SERVER: null,
            VD1_REMOTE_HTTP_USERNAME: null,
            VD1_REMOTE_HTTP_PASSWORD: null,
            VD1_SUPPLIER_CODE: null,
            VD1_RECEIVER_QUALIFIER: null,
            VD1_RECEIVER_ID: null,
            VD1_FACILITY: null,
            VD1_TRADING_PARTNER_QUALIFIER: null,
            VD1_TRADING_PARTNER_ID: null,
            VD1_TRADING_PARTNER_GS_ID: null,
            VD1_FLAG: null,
            VD1_X12_STANDARD: null,
            VD1_EDI_VERSION: null,
            VD1_DUNS: null,
            VD1_SHARED_SECRET: null,
            VD1_SEND_EDI_PO_CHANGE: null,
            VD1_SEND_ITEM_USAGE: null,
            VD1_ITEM_USAGE_FORMAT: null,
            VD1_ITEM_USAGE_SOURCE: null,
            VD1_POST_AS2: null,
            VD1_RECEIVE_AS2: null,
            VD1_CXML_PAYLOAD_ID: null,
            VD1_CHECK_P21_EDI_FLAG: null,
            VD1_SEND_EDI_PAYMENT_ADVICE: null,
            VD1_PAYMENT_ADVICE_FORMAT: null,
            VD1_BANK_ROUTING_NUMBER: null,
            VD1_BANK_ACCOUNT_NUMBER: null,
            VD1_AS2_CERTIFICATE_FILENAME: null,
            VD1_AS2_RECEIVER_ID: null,
            VD1_AS2_TRADING_PARTNER_ID: null,
            VD1_PICKUP_DIRECTORY: null,
            VD1_AS2_REQUEST_RECEIPT: null,
            VD1_AS2_SIGN_MESSAGES: null,
            VD1_AS2_KEY_LENGTH: null,
            VD1_AS2_ENCRYPTION_ALGORITHM: null,
            VD1_AS2_COMPATIBILITY_MODE: null,
            VD1_PAYMENT_QUALIFIER: null,
            VD1_PAYMENT_ID: null,
            VD1_MAP: null,
            VD1_CHECK_ACKNOWLEDGEMENTS: null,
            VD1_FTP_DOWNLOAD_FILTER: null,
            VD1_SFTP_PRIVATE_KEY_FILENAME: null,
            VD1_SEND_EDI_RELEASE_SCHEDULE: null,
            VD1_RELEASE_SCHEDULE_FORMAT: null,
            VD1_RECEIVE_ASN_AS_VESSEL_RECEIPT: null,
            VD1_RECEIVE_API_CALLS: null,
            VD1_API_USERNAME: null,
            VD1_API_PASSWORD: null,
            VD1_SEND_EDI_PRODUCT_TRANSFER_AND_RESALE_REPORT: null,
            VD1_PRODUCT_TRANSFER_AND_RESALE_REPORT_FORMAT: null,
            VD1_SYNCHRONIZED: null,
            VD1_CHECK_CONFIRMATIONS: null,
            VD1_CHECK_SHIPMENTS: null
        };
    }

}
