import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GeneralService } from './general.service';
import { Injectable } from '@angular/core';

@Injectable()
export class MasterDataLocationService extends GeneralService {

    constructor(protected http: HttpClient) {
        super(http);
    }

    public updateInventory(items: any): Observable<any> {
        return this.http.post(this.apiUrl + '/master-data-location/update-inventory', items)
            .catch(this.handleError);
    }
}
