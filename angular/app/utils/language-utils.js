const fs = require('fs');

//look for language keys in component and save it to file
exports.langScanner = function (source, dist) {
    let paths = getComponentsPaths(source);
    let list = [];

    paths.forEach(path => {
        let keyObject = generateKeysObject(path);
        list.push(keyObject);
    })

    let jsonStr = listToJson(list);

    fs.writeFile(dist + "resource.json", jsonStr, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
};

//look for component files and return list with this paths
//this is a recursive function because we start iteration from root dir and in each step we should check if present sub dir
function getComponentsPaths(dir) {
    let paths = [];

    fs.readdirSync(dir).forEach(function (file) {
        let isComponent = file.includes("component.ts");

        file = dir + '/' + file;
        let stat = fs.statSync(file);

        if (stat && stat.isDirectory()) {
            paths = paths.concat(getComponentsPaths(file))
        } else if (isComponent) {
            paths.push(file);
        }
    });

    return paths;
};

//generate object with ClassName in name field and language keys in keys field
function generateKeysObject(filePath) {
    let content = fs.readFileSync(filePath, 'utf-8');
    //content = content.replace('implements', '\nimplements');

    let matchCmp = content.match(/(?<=class\s)(.*)(?=\simplements)/);

    if(!matchCmp || matchCmp == undefined){
        matchCmp = content.match(/(?<=class\s)(.*)(?=\s{)/);
        if (matchCmp && matchCmp[0])
            console.log(matchCmp[0]);
    }
    const className = matchCmp && matchCmp.length ? matchCmp[0] : null;

    var jsonObj = {
        "name": '',
        "keys": []
    };

    if (className) {
        const matchTempl = content.match(/(?<=templateUrl:\s')(.*)(?=')/);
        let templPath = matchTempl && matchTempl.length ? matchTempl[0] : null;
        let componentKeys = [];

        componentKeys.push(...getRoutingKeys(filePath));
        componentKeys.push(...getKeysFromContent(content));

        if (templPath) {
            templPath = templPath.replace('./', '');

            let res = filePath.split("/");
            res[res.length - 1] = templPath;
            templPath = res.toString().replace(/,/g, '/');

            content = fs.readFileSync(templPath, 'utf-8');
            componentKeys.push(...getKeysFromContent(content));
        }

        jsonObj.name = className;
        jsonObj.keys = removeDuplicates(componentKeys);
    }

    return jsonObj;
}

//check routing for languages keys
function getRoutingKeys(filePath) {
    let res = filePath.split("/");
    res[res.length - 1] = '';
    let dir = res.toString().replace(/,/g, '/');
    let routingPath = null;

    fs.readdirSync(dir).forEach(function (file) {
        let isRouting = file.includes("routing.ts");
        if (isRouting) {
            routingPath = dir + '/' + file;
        }
    });

    if(routingPath){
        let content = fs.readFileSync(routingPath, 'utf-8');
        return getKeysFromContent(content)
    }
    return [];
}


﻿//get language keys from content
function getKeysFromContent(content) {
    var listkeys = [];
    var arrKeys;

    content = content.replace(/_lang/gi, '\r\n_lang')
    content = content.replace(/lang}}/gi, 'lang}}\r\n')

    var regKeys = [/(?<=_lang\(')(.*)(?='\))/gi, /(?<=<lng \[key\]="')(.*)(?='")/gi, /(?<={{')(.*)(?='\|lang)/gi];
    regKeys.forEach(reg => {
        arrKeys = content.match(reg);
        if(arrKeys && arrKeys.length > 0)	{
            arrKeys.forEach(key => {
                listkeys.push(key);
            });
        }
    });

    return listkeys;
}

//convert objects list to json
function listToJson(list) {
    var jsonStr = "{ \n";
    list.forEach(object => {
        var length = object.keys.length;

        if (object.keys.length > 0) {
            jsonStr += '\t"' + object.name + '":[\n';
            object.keys.forEach((key, index) => {
                var coma = index == length - 1 ? '' : ',';
               jsonStr += '\t\t"' + key + '"' + coma + '\n';
            });

            jsonStr += '\t],\n'
        }
    });

    jsonStr += '\t"":[\n';
    jsonStr += '\t]\n';
    jsonStr += '}';

    return jsonStr;
}

//look for duplicates and remove it
function removeDuplicates(list) {
    return list.filter(function (item, pos, self) {
        return self.indexOf(item) == pos;
    })
}