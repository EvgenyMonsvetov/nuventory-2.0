import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { LangService } from '../../services/lang.service';
import { Title } from '@angular/platform-browser';
import { _lang } from '../../pipes/lang';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    loginData = {};
    languages: Array<any> = [];
    locale: {} = {};
    vocabulary: any;

    public loading: boolean;
    private error: string;

    constructor(
      private authService: AuthService,
      private router: Router,
      private langService: LangService,
      private titleService: Title) {
    }

    ngOnInit() {
        this.titleService.setTitle(_lang('Nuventory 2.0'));

        this.loading = false;
        this.languages   = this.langService.languages;
        this.locale = this.langService.locale;
        this.vocabulary = this.langService.vocabulary;
        this.error   = '';
    }

    langChange(event) {
        this.langService.reloadVocabulay(event.value);
    }

    onSignUp(form: NgForm) {
        this.loading = true;

        const username = form.value.username;
        const password = form.value.password;
        this.loginData['username'] = username;
        this.loginData['password'] = password;

        this.authService.loginUser(this.loginData).subscribe(result => {
             if (result.user) {
                this.router.navigate(['dashboard']);
             } else {
                 this.loading = false;
                 this.error   = result.error;
             }
        });
    }
}
