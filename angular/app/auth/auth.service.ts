import {Injectable} from '@angular/core';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import * as moment from 'moment-timezone';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { environment } from '../../environments/environment';
import { LangService } from '../services/lang.service';
import { TimezonesService } from '../services/timezones.service';
import { GeneralService } from '../services/general.service';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class AuthService extends GeneralService{

    private expTimer: any = null;
    private modal: any = null;

    constructor(
        private router: Router,
        protected http: HttpClient,
        private permissionsService: NgxPermissionsService,
        private ngxSmartModalService: NgxSmartModalService,
        private languageService: LangService,
        private timezonesService: TimezonesService
    ) {
        super(http);
    }

    public authenticate(result) {
        if(result.user){
            this.token = result.user.token;
            this.setAccessLevel(result.user.GR1_ACCESS_LEVEL);
            this.setUserId( result.user.US1_ID );
            this.setUserName( result.user.US1_NAME ? result.user.US1_NAME.toString() : '');
            this.setCompanyId( result.user.CO1_ID ? result.user.CO1_ID.toString() : '');
            this.setLocationId( result.user.LO1_ID ? result.user.LO1_ID.toString() : '');
            this.setCentralLocation(result.user.centralLocation);
            this.serverTimeZone = result.user.timezone;
            this.tokenDtExp = result.user.exp;
            this.permissions = result.user.permissions;
            this.companies = result.companies;
            this.loadPermissions();
            this.initExpDetection();
            this.loadTimezones();
        }
    }

    set token(token) {
        localStorage.setItem( 'token', token);
    }

    get token() {
        return localStorage.getItem('token');
    }

    set tokenDtExp(dtExp){
        localStorage.setItem('tokenExp', dtExp);
    }

    get tokenExp() {
        return localStorage.getItem('tokenExp');
    }

    set serverTimeZone(serverTimeZone){
        localStorage.setItem('serverTimeZone', serverTimeZone);
    }

    get serverTimeZone(){
        return localStorage.getItem('serverTimeZone');
    }

    set permissions(permissions){
        localStorage.setItem('permissions', permissions);
    }

    get permissions():string{
        let permissions = localStorage.getItem('permissions');
        return permissions || '';
    }

    setAccessLevel(GR1_ACCESS_LEVEL){
        localStorage.setItem('GR1_ACCESS_LEVEL', GR1_ACCESS_LEVEL);
    }

    get accessLevel(): number {
        return JSON.parse(localStorage.getItem('GR1_ACCESS_LEVEL'));
    }

    setUserId(userid) {
        localStorage.setItem('userid', userid);
    }

    get userId(): number {
        return JSON.parse(localStorage.getItem('userid'));
    }

    setUserName(username) {
        localStorage.setItem('username', username);
    }

    get userName(): string {
        return localStorage.getItem('username');
    }

    get isAuthenticated() {
        return localStorage.getItem('token') ? true : false;
    }

    setCompanyId(CO1_ID, realUpdate:boolean = false) {
        localStorage.setItem('CO1_ID', CO1_ID);
        if(realUpdate){
            const url = `${this.apiUrl + '/set-company/'}`;
            return this.http.put(url, {
                CO1_ID: CO1_ID
            }).do(data => {

            }).catch(this.handleError);
        }
    }

    get companyId(): number{
        return parseInt(localStorage.getItem('CO1_ID'));
    }

    set companies(companies){
        localStorage.setItem('companies', JSON.stringify(companies));
    }

    get companies():Array<any>{
        let companies = JSON.parse(localStorage.getItem('companies'));
        return companies || '';
    }

    setLocationId(LO1_ID, realUpdate:boolean = false) {
        localStorage.setItem('LO1_ID', LO1_ID);

        if(realUpdate) {
            const url = `${this.apiUrl + '/set-location/'}`;
            return this.http.put(url, {
                LO1_ID: LO1_ID
            }).do(data => {

            }).catch(this.handleError);
        }
    }

    get locationId(): number{
        return parseInt(localStorage.getItem('LO1_ID'));
    }

    setCentralLocation(centralLocation: any) {
        localStorage.setItem('centralLocation', JSON.stringify(centralLocation));
    }

    get centralLocation() {
        return JSON.parse(localStorage.getItem('centralLocation'));
    }

    logout( showSingUp: boolean = true ) {
        clearInterval(this.expTimer);
        this.expTimer = null;

        const locale     = this.languageService.locale;
        const languages  = this.languageService.languages;
        const vocabulary = this.languageService.vocabulary;

        localStorage.clear();

        this.languageService.setLocale(locale);
        this.languageService.setLanguages(languages);
        this.languageService.setVocabulary(vocabulary);

        if(showSingUp){
            this.router.navigate(['signup']);
        }
    }

    loadPermissions(){
        this.permissionsService.flushPermissions();
        this.permissionsService.loadPermissions(this.permissions.split(','));
    }

    loginUser(loginData) {
        const url = `${this.apiUrl + '/login'}`;
        return this.http.post<any>(url, loginData)
            .do(result=> this.authenticate(result))
            .catch(this.handleError);
    }

    notifyUser(seconds){

        var browserTime = moment(new Date());
            browserTime.add(seconds, 'seconds');


        if(!this.modal){
            this.modal = this.ngxSmartModalService.getModal('auth');

            this.modal.onOpen.subscribe(() => {
              window.scrollTo(0, 0)
            });
            var me = this;
            this.modal.onCloseFinished.subscribe(() => {
                me.refreshToken(this.token).subscribe();
            });
        }

        this.modal.open();
        this.ngxSmartModalService.setModalData({
          time:    browserTime.format('HH:mm:ss A'),
          seconds: parseInt(seconds)
        }, 'auth', true);
    }

    public refreshToken( token ){
        const url = `${this.apiUrl + '/refresh-token/'}`;
        return this.http.put(url, {
            token: token })
        .do((result: any)=> {
            if(result.token){
                this.token = result.token;
                this.tokenDtExp = result.exp;
            }
        })
        .catch(this.handleError);
    }

    initExpDetection() {

        var me = this;
        clearInterval(this.expTimer);
        this.expTimer = null;

        this.expTimer = setInterval(()=> {

            var expTime = moment.tz(me.tokenExp, me.serverTimeZone);
            var localTime  = moment(new Date()).tz(me.serverTimeZone);

            if(localTime > expTime){
              me.logout();
            }else{
              var seconds = moment.duration(expTime.diff(localTime)).asSeconds();
              if(seconds <= environment.secondsTokenExpire){
                me.notifyUser(seconds);
              }
            }
        }, 1000 * 5);
    }

    loadTimezones() {
        this.timezonesService.getAllTimezones().subscribe( timezones => {
        });
    }
}
