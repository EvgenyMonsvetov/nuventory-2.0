import {BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, Injector, NgModule} from '@angular/core';
import {LocationStrategy, HashLocationStrategy, CommonModule} from '@angular/common';
import {BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {TabsModule } from 'ngx-bootstrap/tabs';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthService} from './auth/auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpAuthInterceptor} from './interceptors/http-auth.interceptor';
import {AuthGuardService} from './auth-guard.service';
import {SignupComponent} from './auth/signup/signup.component';
import {OAuthModule} from 'angular-oauth2-oidc';
import {HttpErrorInterceptor} from './interceptors/http-error.interceptor';
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from './interceptors/http-timeout.interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import { SettingsMenuComponent } from './components/settings-menu/settings-menu.component';
import {NgxPermissionsModule} from 'ngx-permissions';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxLoadingModule} from 'ngx-loading';
import {NavigationEnd, Router, RouterModule} from '@angular/router';
import {
    MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule, MatButtonModule, MatInputModule,
    MatAutocompleteModule, MatFormFieldModule, MatGridListModule, MatStepperModule, MatSelectModule, MatExpansionModule
} from '@angular/material';
import { TableModule } from 'primeng/table';

import { AppComponent } from './app.component';

// import { Highcharts } from 'angular-highcharts';
Highcharts.setOptions({
    lang: {
        thousandsSep: ' '
    }
});

// Import components
import {
    AppAsideComponent,
    AppBreadcrumbsComponent,
    AppFooterComponent,
    AppHeaderComponent,
    AppSidebarComponent,
    AppSidebarFooterComponent,
    AppSidebarFormComponent,
    AppSidebarHeaderComponent,
    AppSidebarMinimizerComponent,
    APP_SIDEBAR_NAV
} from './components';

const APP_COMPONENTS = [
    AppAsideComponent,
    AppBreadcrumbsComponent,
    AppFooterComponent,
    AppHeaderComponent,
    AppSidebarComponent,
    AppSidebarFooterComponent,
    AppSidebarFormComponent,
    AppSidebarHeaderComponent,
    AppSidebarMinimizerComponent,
    APP_SIDEBAR_NAV
]

// Import directives
import {
    AsideToggleDirective,
    NAV_DROPDOWN_DIRECTIVES,
    SIDEBAR_TOGGLE_DIRECTIVES
} from './directives';

const APP_DIRECTIVES = [
    AsideToggleDirective,
    NAV_DROPDOWN_DIRECTIVES,
    SIDEBAR_TOGGLE_DIRECTIVES
]

// Import routing module
import { TransactionService } from './services/transaction.service';
import { UserService } from './services/user.service';
import { PermissionsService } from './views/rights/permissions/permissions.service';
import { UserlogService } from './services/userlog.service';

import { PipesModule } from './views/pipes/pipes.module';

import { routes } from './app.routing';
import { RolesService } from './views/rights/roles/roles.service';
import { onAppInit } from './interceptors/initializer.interceptor';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { MaterialModule } from './views/material/material.module';
import { LangService } from './services/lang.service';
import { LanguageEditorModalComponent } from './components/language-editor-modal/language-editor-modal.component';
import { CompanyFinderModalComponent } from './components/company-finder-modal/company-finder-modal.component';
import { InventoryFinderModalComponent } from './components/inventory-finder-modal/inventory-finder-modal.component';
import { ShopFinderModalComponent } from './components/shop-finder-modal/shop-finder-modal.component';
import { SelectCountriesComponent } from './components/select-countries/select-countries.component';
import { ExportModalComponent } from './components/export-modal/export-modal.component';
import { GroupsFinderModalComponent } from './components/groups-finder-modal/groups-finder-modal.component';
import { MasterVendorFinderModalComponent } from './components/master-vendor-finder-modal/master-vendor-finder-modal.component';
import { CategoryFinderModalComponent } from './components/category-finder-modal/category-finder-modal.component';
import { GroupsModule } from './views/groups/groups.module';
import { CustomerFinderModalComponent } from './components/customer-finder-modal/customer-finder-modal.component';
import { JobLineModalComponent } from './components/job-line-modal/job-line-modal.component';
import { TechnicianFinderModalComponent } from './components/technician-finder-modal/technician-finder-modal.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { PurchaseOrderLineModalComponent } from './components/purchase-order-line-modal/purchase-order-line-modal.component';
import { OrderLineModalComponent } from './components/order-line-modal/order-line-modal.component';
import { AddCatalogListModalComponent } from './components/add-catalog-list-modal/add-catalog-list-modal.component';
import { FileUploaderModalComponent } from './components/file-uploader-modal/file-uploader-modal.component';
import { FileDropModule } from 'ngx-file-drop';
import { UpdatePartsModalComponent } from './components/update-parts-modal/update-parts-modal.component';
import { MasterDataModalComponent } from './components/master-data-modal/master-data-modal.component';
import { LinkMcatalogModalComponent } from './components/link-mcatalog-modal/link-mcatalog-modal.component';
import { VendorFinderModalComponent } from './components/vendor-finder-modal/vendor-finder-modal.component';
import { EditInventoryModalComponent } from './components/edit-inventory-modal/edit-inventory-modal.component';
import { ConfirmDialogModule, DropdownModule, TreeTableModule } from 'primeng/primeng';
import { LinkCatalogModalComponent } from './components/link-catalog-modal/link-catalog-modal.component';
import { PdfPrintSettingModalComponent } from './components/pdf-print-setting-modal/pdf-print-setting-modal.component';
import { PdfPrintCheckoutSheetSettingsModalComponent } from './components/pdf-print-checkout-sheet-settings-modal/pdf-print-checkout-sheet-settings-modal.component';
import { CurrencyModule } from './components/currency/currency.module';
import { LinkCatalogPartsModalComponent } from './components/link-catalog-parts-modal/link-catalog-parts-modal.component';
import { SetupWizardModalComponent } from './components/setup-wizard-modal/setup-wizard-modal.component';
import { ImportModalComponent } from './components/import-modal/import-modal.component';
import { CompanyDropdownModule } from './components/company-dropdown/company-dropdown.module';
import { CompanyLocationDropdownModule } from './components/company-location-dropdown/company-location-dropdown.module';
import { VendorSearchModule } from './components/vendor-search/vendor-search.module';
import { ImportMasterDataModule } from './components/import-master-data/import-master-data.module';
import { ImportMasterDataModalComponent } from './components/import-master-data-modal/import-master-data-modal.component';
import { PriceChangeLogModalComponent } from './components/price-change-log-modal/price-change-log-modal.component';
import { LinkedPartsModalComponent } from './components/linked-parts-modal/linked-parts-modal.component';
import { CompaniesCheckboxDropdownModule } from './components/companies-checkbox-dropdown/companies-checkbox-dropdown.module';
import { ShopsCheckboxDropdownModule } from './components/shops-checkbox-dropdown/shops-checkbox-dropdown.module';
import { SummaryChartModalComponent } from './components/summary-chart-modal/summary-chart-modal.component';
import { СomboboxSearchModule } from './components/combobox-search/combobox-search.module';
import { PaginatorModule } from 'primeng/paginator';
import { RackDropdownModule } from './components/rack-dropdown/rack-dropdown.module';
import { DrawerDropdownModule } from './components/drawer-dropdown/drawer-dropdown.module';
import { BinDropdownModule } from './components/bin-dropdown/bin-dropdown.module';
import { ReportingFilterModule } from './components/reporting-filter/reporting-filter.module';
import { GenerateStatementModalComponent } from './components/generate-statement-modal/generate-statement-modal.component';
import * as Highcharts from 'highcharts';
import { PrintRackComponent } from './views/print-rack/print-rack.component';
import { VendorCategoryFinderModalComponent } from './components/vendor-category-finder-modal/vendor-category-finder-modal.component';
import { TechUsageTechnicianDetailListModalComponent } from './components/tech-usage-technician-detail-list-modal/tech-usage-technician-detail-list-modal.component';
import { GroupsCheckboxDropdownModule } from './components/groups-checkbox-dropdown/groups-checkbox-dropdown.module';
import { EditLaborHoursModalComponent } from './components/edit-labor-hours-modal/edit-labor-hours-modal.component';
import { AverageChartModalComponent } from './components/average-chart-modal/average-chart-modal.component';
import { ImagesUploaderModalComponent } from './components/images-uploader-modal/images-uploader-modal.component';
import { BaseModalComponent } from './components/base-modal/base-modal.component';
import { TechScanAddModalComponent } from './components/tech-scan-add-modal/tech-scan-add-modal.component';
import { TechScanAddFormModule } from './components/tech-scan-add-form/tech-scan-add-form.module';
import { DefaultImageEditorModalComponent } from './components/default-image-editor-modal/default-image-editor-modal.component';
import { UserGuardService } from './user-guard.service';

@NgModule({
    exports: [RouterModule],
    imports: [
        ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
        BrowserModule,
        NgxSmartModalModule.forRoot(),
        HttpClientModule,
        OAuthModule.forRoot(),
        RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}),
        BsDropdownModule.forRoot(),
        TabsModule.forRoot(),
        NgbModule.forRoot(),
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        FormsModule,
        CommonModule,
        NgxPermissionsModule.forRoot(),
        NgxLoadingModule,
        MaterialModule,
        PipesModule,
        MatDialogModule,
        MatButtonModule,
        TableModule,
        TreeTableModule,
        ReactiveFormsModule,
        GroupsModule,
        MatInputModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        NgxMatSelectSearchModule,
        MatSelectModule,
        MatGridListModule,
        FileDropModule,
        ConfirmDialogModule,
        CurrencyModule,
        MatStepperModule,
        CompanyDropdownModule,
        CompanyLocationDropdownModule,
        MatExpansionModule,
        VendorSearchModule,
        СomboboxSearchModule,
        ImportMasterDataModule,
        CompaniesCheckboxDropdownModule,
        ShopsCheckboxDropdownModule,
        GroupsCheckboxDropdownModule,
        PaginatorModule,
        RackDropdownModule,
        DrawerDropdownModule,
        BinDropdownModule,
        ReportingFilterModule,
        DropdownModule,
        TechScanAddFormModule
],
    declarations: [
        AppComponent,
        ...APP_COMPONENTS,
        ...APP_DIRECTIVES,
        SignupComponent,
        PrintRackComponent,
        SettingsMenuComponent,
        LanguageEditorModalComponent,
        CompanyFinderModalComponent,
        InventoryFinderModalComponent,
        ShopFinderModalComponent,
        SelectCountriesComponent,
        ExportModalComponent,
        GroupsFinderModalComponent,
        MasterVendorFinderModalComponent,
        CategoryFinderModalComponent,
        CustomerFinderModalComponent,
        JobLineModalComponent,
        TechnicianFinderModalComponent,
        PurchaseOrderLineModalComponent,
        OrderLineModalComponent,
        AddCatalogListModalComponent,
        FileUploaderModalComponent,
        UpdatePartsModalComponent,
        MasterDataModalComponent,
        LinkMcatalogModalComponent,
        VendorFinderModalComponent,
        EditInventoryModalComponent,
        LinkCatalogModalComponent,
        PdfPrintSettingModalComponent,
        PdfPrintCheckoutSheetSettingsModalComponent,
        LinkCatalogPartsModalComponent,
        SetupWizardModalComponent,
        ImportModalComponent,
        ImportMasterDataModalComponent,
        PriceChangeLogModalComponent,
        LinkedPartsModalComponent,
        SummaryChartModalComponent,
        GenerateStatementModalComponent,
        VendorCategoryFinderModalComponent,
        TechUsageTechnicianDetailListModalComponent,
        EditLaborHoursModalComponent,
        AverageChartModalComponent,
        ImagesUploaderModalComponent,
        BaseModalComponent,
        TechScanAddModalComponent,
        DefaultImageEditorModalComponent
    ],
    entryComponents : [
        LanguageEditorModalComponent,
        CompanyFinderModalComponent,
        InventoryFinderModalComponent,
        ShopFinderModalComponent,
        SelectCountriesComponent,
        ExportModalComponent,
        GroupsFinderModalComponent,
        MasterVendorFinderModalComponent,
        CategoryFinderModalComponent,
        CustomerFinderModalComponent,
        JobLineModalComponent,
        TechnicianFinderModalComponent,
        PurchaseOrderLineModalComponent,
        OrderLineModalComponent,
        AddCatalogListModalComponent,
        FileUploaderModalComponent,
        UpdatePartsModalComponent,
        MasterDataModalComponent,
        LinkMcatalogModalComponent,
        VendorFinderModalComponent,
        EditInventoryModalComponent,
        LinkCatalogModalComponent,
        PdfPrintSettingModalComponent,
        PdfPrintCheckoutSheetSettingsModalComponent,
        LinkCatalogPartsModalComponent,
        SetupWizardModalComponent,
        ImportModalComponent,
        ImportMasterDataModalComponent,
        PriceChangeLogModalComponent,
        LinkedPartsModalComponent,
        SummaryChartModalComponent,
        GenerateStatementModalComponent,
        VendorCategoryFinderModalComponent,
        TechUsageTechnicianDetailListModalComponent,
        EditLaborHoursModalComponent,
        AverageChartModalComponent,
        ImagesUploaderModalComponent,
        BaseModalComponent,
        TechScanAddModalComponent,
        DefaultImageEditorModalComponent
    ],
    providers: [{
        provide: LocationStrategy,
        useClass: HashLocationStrategy,
    },
        {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}},
        AuthService,
        LangService,
        TransactionService,
        UserService,
        UserlogService,
        PermissionsService,
        RolesService,
        {
            provide: APP_INITIALIZER,
            useFactory: onAppInit,
            deps: [Injector],
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpAuthInterceptor,
            multi: true
        },
        { provide: HTTP_INTERCEPTORS,
            useClass: HttpErrorInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TimeoutInterceptor, multi: true
        },
        { provide: DEFAULT_TIMEOUT, useValue: 10000 },

        AuthGuardService,
        UserGuardService
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {
    constructor(
      private router: Router
    ){
        this.router.events.subscribe(path => {
          if( path instanceof NavigationEnd){
              if(window['NProgress']){
                  window['NProgress'].set(1);
                  setTimeout( function () {
                     window['NProgress'].done();
                     window['NProgress'].remove();
                     delete window['NProgress'];
                     window['NProgress'] = null;
                  }, 2000);
              }
          }
        });
    }
}

