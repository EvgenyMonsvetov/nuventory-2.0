import { Pipe, PipeTransform } from '@angular/core';
import {UserStatusService} from "../views/user/user-status/user-status.service";

@Pipe({
  name: 'userstatus'
})
export class UserstatusPipe implements PipeTransform {
  constructor(private userStatusService: UserStatusService) {}

  transform(value: number, args?: any): any {
    let result = this.userStatusService.getStatuses().filter(status => {
      return status.status == value;
    })[0];
    return result ? result.name : '-';
  }

}
