import { LangPipe } from '../views/pipes/lang.pipe';
import { LangService } from '../services/lang.service';
import { HttpClient } from '@angular/common/http';

export function _lang(value: string, data: Array<any> = null): any {
    const lng = new LangPipe(new LangService(new HttpClient(null)));
    return lng.transform(value, data);
}