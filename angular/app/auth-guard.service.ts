import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth/auth.service';
import {NgxPermissionsService} from 'ngx-permissions';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(public auth: AuthService, public router: Router, private permissionsService: NgxPermissionsService) {
    }

    canActivate(): boolean {
        if (!this.auth.isAuthenticated) {
            this.router.navigate(['signup']);
            return false;
        }

        return true;
    }
}
