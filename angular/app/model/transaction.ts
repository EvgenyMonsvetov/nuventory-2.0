export class Transaction {
    private _id: string;
    private _name: string;
    private _VD1_ID: string;
    private _VENDOR_ID: string;
    private _checked: boolean;

    constructor(id: string, name: string) {
        this._id = id;
        this._name = name;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }


    get VD1_ID(): string {
        return this._VD1_ID;
    }

    set VD1_ID(value: string) {
        this._VD1_ID = value;
    }


    get VENDOR_ID(): string {
        return this._VENDOR_ID;
    }

    set VENDOR_ID(value: string) {
        this._VENDOR_ID = value;
    }

    get checked(): boolean {
        return this._checked;
    }

    set checked(value: boolean) {
        this._checked = value;
    }
}
