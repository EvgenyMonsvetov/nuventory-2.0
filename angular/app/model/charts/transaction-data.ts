export class TransactionData {
    private _transactions: number[];
    private _categories: string[];

    constructor(transactions: number[], categories: string[]) {
        this._transactions = transactions;
        this._categories = categories;
    }

    get transactions(): number[] {
        return this._transactions;
    }

    set transactions(value: number[]) {
        this._transactions = value;
    }

    get categories(): string[] {
        return this._categories;
    }

    set categories(value: string[]) {
        this._categories = value;
    }
}
