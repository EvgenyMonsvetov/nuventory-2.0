export interface ICountry {
  'CY1_ID': number,
  'CY1_SHORT_CODE': string,
  'CY1_NAME': string,
  'CY1_DELETE_FLAG': number,
  'CREATED_BY': string,
  'MODIFIED_BY': string,
  'CY1_CREATED_ON': string,
  'CY1_MODIFIED_ON':  string,
  'errors':  any
}