export interface IMasterVendor {
  'VD0_ID': number,
  'VD0_EDI_GL': number,
  'VD0_SHORT_CODE': string,
  'VD0_NAME': string,
  'VD0_ADDRESS1': string,
  'VD0_ADDRESS2': string,
  'VD0_CITY': string,
  'VD0_STATE': string,
  'VD0_ZIP': string,
  'VD0_PHONE': string,
  'VD0_FAX': string,
  'VD0_URL': string,
  'VD0_EMAIL': string,
  'VD0_DELETE_FLAG': number,
  'CY1_ID': number,
  'CY1_SHORT_CODE': string,
  'CY1_NAME': string,
  'CREATED_BY': string,
  'VD0_CREATED_ON': string,
  'MODIFIED_BY': string,
  'VD0_MODIFIED_ON': string,
  'VD0_MINIMUM_ORDER_CHECK': number,
  'VD0_MINIMUM_ORDER': number,
  'VD0_DISCOUNT': number,
  'VD0_FLAG': string,
  'VD0_EDI_VERSION': string,
  'VD0_X12_STANDARD': string,
  'VD0_SUPPLIER_CODE': string,
  'VD0_DUNS': string,
  'VD0_FACILITY': string,
  'VD0_TRADING_PARTNER_QUALIFIER': string,
  'VD0_TRADING_PARTNER_ID': string,
  'VD0_TRADING_PARTNER_GS_ID': string,
  'VD0_RECEIVER_QUALIFIER': string,
  'VD0_RECEIVER_ID': string,
  'VD0_RECEIVER_GS_ID': string,
  'VD0_AS2_CERTIFICATE_FILENAME': string,
  'VD0_AS2_RECEIVER_ID': string,
  'VD0_AS2_TRADING_PARTNER_ID': string,
  'VD0_SEND_EDI_PO': number,
  'VD0_PO_FORMAT': number,
  'VD0_SEND_FTP': number,
  'VD0_SEND_SFTP': number,
  'VD0_REMOTE_FTP_SERVER': string,
  'VD0_REMOTE_FTP_DIRECTORY_SEND': string,
  'VD0_REMOTE_FTP_DIRECTORY_PICKUP': string,
  'VD0_REMOTE_FTP_USERNAME': string,
  'VD0_REMOTE_FTP_PASSWORD': string,
  'VD0_POST_HTTP': number,
  'VD0_POST_AS2': number,
  'VD0_AS2_REQUEST_RECEIPT': string,
  'VD0_AS2_SIGN_MESSAGES': string,
  'VD0_REMOTE_HTTP_SERVER': string,
  'VD0_AS2_KEY_LENGTH': string,
  'VD0_PICKUP_SFTP': number,
  'VD0_FTP_DIRECTORY': string,
  'VD0_FTP_USER': string,
  'VD0_FTP_PASSWORD': string,
  'VD0_RECEIVE_EDI': number,
  'VD0_SEND_ACKNOWLEDGEMENT': number,
  'VD0_PICKUP_FTP': number,
  'VD0_PICKUP_DIRECTORY': string,
  'VD0_RECEIVE_HTTP': number,
  'VD0_RECEIVE_AS2': number,
  'VD0_SHARED_SECRET': string,
  'VD0_RECEIVE_FTP': number,
  'VD0_NO_INVENTORY': number,
  'VD0_MANUFACTURER': number
}