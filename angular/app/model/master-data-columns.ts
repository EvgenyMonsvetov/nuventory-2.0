import {_lang} from "../pipes/lang";

export const MasterDataColumns = [
  { field: 'VD1_NAME',                header: _lang('VD1_NAME'),              visible: true, width: 120 },
  { field: 'MD2_ACTIVE',              header: _lang('MD2_ACTIVE'),            visible: true, width: 40 },
  { field: 'MD2_PRINT_FLAG',          header: _lang('MD2_PRINT_FLAG'),        visible: true, width: 40 },
  { field: 'MD1_PART_NUMBER',         header: _lang('MD1_PART_NUMBER'),       visible: true, width: 100 },
  { field: 'MD1_VENDOR_PART_NUMBER',  header: _lang('MD1_VENDOR_PART_NUMBER'),visible: true, width: 100 },
  { field: 'MD1_OEM_NUMBER',          header: _lang('MD1_OEM_NUMBER'),        visible: true, width: 100 },
  { field: 'MD1_DESC1',               header: _lang('MD1_DESC1'),             visible: true, width: 100 },
  { field: 'MD1_UNIT_PRICE',          header: _lang('MD1_UNIT_PRICE'),        visible: true, width: 100 },
  { field: 'MD2_STOCK_LEVEL',         header: _lang('MD2_STOCK_LEVEL'),       visible: true, width: 50 },
  { field: 'MD2_ON_HAND_QTY',         header: _lang('MD2_ON_HAND_QTY'),       visible: true, width: 50 },
  { field: 'MD2_AVAILABLE_QTY',       header: _lang('MD2_AVAILABLE_QTY'),     visible: true, width: 50 },
  { field: 'MD2_MIN_QTY',             header: _lang('MD2_MIN_QTY'),           visible: true, width: 50 },
  { field: 'MD2_RACK',                header: _lang('MD2_RACK'),              visible: true, width: 100 },
  { field: 'UM1_NAME',                header: _lang('UM1_NAME'),              visible: true, width: 100 },
  { field: 'C00_NAME',                header: _lang('Nuventory Category'),    visible: true, width: 100 },
  { field: 'CA1_NAME',                header: _lang('CA1_NAME'),              visible: true, width: 100 },
  { field: 'GL1_NAME',                header: _lang('GL1_NAME'),              visible: true, width: 100 },
  { field: 'MD1_UPC1',                header: _lang('MD1_UPC1'),              visible: true, width: 100 },
  { field: 'MD1_BODY',                header: _lang('MD1_BODY'),              visible: true, width: 50 },
  { field: 'MD1_DETAIL',              header: _lang('MD1_DETAIL'),            visible: true, width: 50 },
  { field: 'MD1_PAINT',               header: _lang('MD1_PAINT'),             visible: true, width: 50 }
];