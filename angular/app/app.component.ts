import { AfterViewInit, Component, HostListener, ViewChild } from '@angular/core';
import { AppHeaderComponent } from './components/app-header/app-header.component';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import { AuthService } from './auth/auth.service';
import { MatDialog, MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { LangService } from './services/lang.service';
import { environment } from "../environments/environment";
import { TechScanAddModalComponent } from './components/tech-scan-add-modal/tech-scan-add-modal.component';
import {_lang} from "./pipes/lang";
import {Constants} from "./constants";

@Component({
    // tslint:disable-next-line
    selector:    'body',
    styleUrls: ['./app.component.scss'],
    templateUrl: 'app.component.html',
    preserveWhitespaces: true
})
export class AppComponent implements AfterViewInit {
    @ViewChild('header') header:  AppHeaderComponent;
    headerHeight = 0;
    private icon_path = './assets/img/lang/';
    private barcode: string = '';
    private printRack: boolean = false;
    private slideShow: boolean = false;
    private previousUrl: string = '';
    private currentUrl: string = '';
    private slideShowInterval;
    private techScanModalRef;

    constructor(private router: Router,
                private auth: AuthService,
                private route: ActivatedRoute,
                private langService: LangService,
                private matIconRegistry: MatIconRegistry,
                public  dialog: MatDialog,
                private domSanitizer: DomSanitizer) {
        window['NProgress'].done();
        this.matIconRegistry.addSvgIcon('EN', this.domSanitizer.bypassSecurityTrustResourceUrl(this.icon_path + 'us.svg'));
        this.matIconRegistry.addSvgIcon('FR', this.domSanitizer.bypassSecurityTrustResourceUrl(this.icon_path + 'fr.svg'));
        this.matIconRegistry.addSvgIcon('ES', this.domSanitizer.bypassSecurityTrustResourceUrl(this.icon_path + 'es.svg'));
        this.matIconRegistry.addSvgIcon('DE', this.domSanitizer.bypassSecurityTrustResourceUrl(this.icon_path + 'de.svg'));
        this.start();
    }

    private start(): void {
        window.addEventListener('storage', this.storageEventListener.bind(this));
        this.router.events.subscribe(event => {
            if (event.hasOwnProperty('url')) {
                this.currentUrl = event['url'];
                this.printRack = this.currentUrl.includes('/master-data/print-rack');
                this.slideShow = this.currentUrl.includes('/charts-slide-show/');
                if (this.slideShow && this.previousUrl !== this.currentUrl) {
                    this.startSlideShow();
                }
                this.previousUrl = this.currentUrl;
                // this.printRack = (event['url'] === '/master-data/print-rack');
            }
        });
    }

    private storageEventListener(event: StorageEvent) {
        if (event.storageArea === localStorage) {
            let languages = localStorage.getItem('languages');
            let currentVocabulary = localStorage.getItem('currentVocabulary');

            if (!event.key || !languages || !currentVocabulary ) {
                this.langService.initLanguages();
            }
        }
    }

    getHeaderHeight() {
        setTimeout(() => this.headerHeight =  this.header ? this.header.getHeaderHeight() : 0, 0);
    }

    ngAfterViewInit() {
        this.getHeaderHeight();
        if (environment.production || environment.testing) {
            setTimeout(() => this.auth.logout(), 100);
        }
    }

    @HostListener('window:keydown', ['$event'])
    onKeyDown(event) {
        if (this.slideShow) {
            if (event.key === 'Escape') {
                this.closeSlideShow();
            }
        } else if (event.key !== 'Enter') {
			if(event.key == '+'){
				this.barcode = '';
			}
			if(event.key != 'Shift'){
				this.barcode += event.key;
			}
        } else {
            let scannedText = this.barcode;
            this.barcode = '';
            let indexTechnician = scannedText.indexOf('TECH-');

            if (indexTechnician >= 0) {
                if (this.currentUrl !== '/tech-scans/add/0') {
                    if (!this.techScanModalRef) {
                        event.stopImmediatePropagation();
                        let tech_id = scannedText;
                        tech_id = tech_id.substring(indexTechnician + 5, tech_id.length);

                        let index = tech_id.indexOf('Shift');
                        while (index !== -1) {
                            tech_id = tech_id.replace('Shift', '');
                            index = tech_id.indexOf('Shift');
                        }

                        tech_id = tech_id.replace('STARTSTRING', '');
                        tech_id = tech_id.replace('ENDSTRING', '');
                        tech_id = tech_id.replace('BATCHSTART', '');
                        tech_id = tech_id.replace('BATCHEND', '');
                        tech_id = tech_id.replace('SCNSTART', '');
                        tech_id = tech_id.replace('SCNEND', '');


                        this.techScanModalRef = this.dialog.open(TechScanAddModalComponent, {
                            width: window.innerWidth - 60 + 'px',
                            height: window.innerHeight - 40 + 'px',
                            role: 'dialog',
                            data: {title: _lang('Tech Scan'), componentRef: this, technicianShortCode: tech_id},
                        });

                        this.techScanModalRef.beforeClose().subscribe( result => {
                            this.techScanModalRef = null;
                        });

                        return false;
                    }
                }

            }else if( /\+?save/i.test(scannedText) ){
                var elements = document.getElementsByClassName(Constants.ACCEPT_SAVE_BARCODE);
                if(elements.length){
                    var element = <HTMLElement>elements[0];
                    element.click();
                }
                this.barcode = '';
            }
        }
    }

    @HostListener('window:unload', [ '$event' ])
    unloadHandler(event) {
        if(environment.production){
            this.auth.logout( false );
        }
    }

    pauseSlideShow() {
        clearInterval(this.slideShowInterval);
        this.slideShowInterval = null;
    }

    startSlideShow() {
        let me = this;
        this.slideShowInterval = setInterval(() => {
            clearInterval(this.slideShowInterval);
            me.showNextChartSlide(me.currentUrl);
        }, environment.slideShowDelay);
    }

    showNextChartSlide(eventUrl) {
        switch (eventUrl) {
            case '/charts-slide-show/dashboard':
                this.router.navigate(['charts-slide-show/projected-po-usage-report']);
                break;
            case '/charts-slide-show/projected-po-usage-report':
                this.router.navigate(['charts-slide-show/tech-usage-mtd-report']);
                break;
            case '/charts-slide-show/tech-usage-mtd-report':
                this.router.navigate(['charts-slide-show/paint-labor-vs-sales-report']);
                break;
            case '/charts-slide-show/paint-labor-vs-sales-report':
                this.router.navigate(['charts-slide-show/paint-materials-gp-report']);
                break;
            case '/charts-slide-show/paint-materials-gp-report':
                this.router.navigate(['charts-slide-show/dashboard']);
                break;
        }
    }

    closeSlideShow() {
        clearInterval(this.slideShowInterval);
        this.slideShowInterval = null;
        this.router.navigate(['dashboard']);
    }
}

