export class Constants {
    public static drawerDP: any[] = [
        {label: 'N/A',  data: '0'},
        {label: '1',    data: '1'},
        {label: '2',    data: '2'},
        {label: '3',    data: '3'},
        {label: '4',    data: '4'}
    ];

    public static binDP: any[] = [
        {label: 'N/A',  data: '0'},
        {label: '1',    data: '1'},
        {label: '2',    data: '2'},
        {label: '3',    data: '3'},
        {label: '4',    data: '4'},
        {label: '5',    data: '5'},
        {label: '6',    data: '6'},
        {label: '7',    data: '7'},
        {label: '8',    data: '8'},
        {label: '9',    data: '9'},
        {label: '10',   data: '10'},
        {label: '11',   data: '11'},
        {label: '12',   data: '12'},
        {label: '13',   data: '13'},
        {label: '14',   data: '14'},
        {label: '15',   data: '15'},
        {label: '16',   data: '16'},
        {label: '17',   data: '17'},
        {label: '18',   data: '18'},
        {label: '19',   data: '19'},
        {label: '20',   data: '20'},
        {label: '21',   data: '21'},
        {label: '22',   data: '22'},
        {label: '23',   data: '23'},
        {label: '24',   data: '24'}
    ];

    public static ACCEPT_SAVE_BARCODE = 'accept-save-barcode';
}