export const environment = {
  production: true,
  testing: false,
  slideShowDelay: 30 * 1000,
  'API_URL': 'http://cloud2.nuventory.com:8181',
  'secondsTokenExpire': 60, // time in seconds to notify user before token will expire
  'maxUploadZipFile': 20 * 1024 * 1024
};
