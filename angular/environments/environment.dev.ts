
export const environment = {
  production: false,
  testing: true,
  slideShowDelay: 10 * 1000,
  'API_URL': 'http://nuventory:80',
  'secondsTokenExpire': 60, // time in seconds to notify user before token will expire
  'maxUploadZipFile': 20 * 1024 * 1024
};