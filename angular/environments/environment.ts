// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  testing: true,
  slideShowDelay: 30 * 1000,
  'API_URL': 'http://nuventory:80',
  'secondsTokenExpire': 60, // time in seconds to notify user before token will expire
  'maxUploadZipFile': 20 * 1024 * 1024
};