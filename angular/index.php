<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
<link rel="stylesheet" type="text/css" href="./assets/js/nprogress/nprogress.css" >
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/nprogress/nprogress.js"></script>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <script type="text/javascript">
    NProgress.configure({
        easing: 'linear',
        speed: 1,
        trickleSpeed: 200,
        showSpinner: false
    });
    window['NProgress'] = NProgress;
    window['NProgress'].start();
  </script>
  <!-- Enable bootstrap 4 theme -->
  <script>window.__theme = 'bs4';</script>
</body>
