<?php

namespace app\bootstraps;

use yii\base\BootstrapInterface;
use \yii\web\User;

class AppBootstrap implements BootstrapInterface{

   public function bootstrap($app){
      $app->user->on(User::EVENT_AFTER_LOGIN,  ['app\models\User', 'afterLogin']);
   }
}