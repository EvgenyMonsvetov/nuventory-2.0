<?php

namespace app\pdfs;

use app\models\MasterData;
use app\models\Technician;
use Yii;
use TCPDF;
use app\models\Location;
use app\models\Company;
use app\models\Inventory as InventoryModel;
use app\models\Invoice as InvoiceModel;
use app\models\InvoiceLine as InvoiceLineModel;
use app\models\JobInvoice as JobInvoiceModel;
use app\models\JobTicket as JobTicketModel;
use app\models\JobInvoiceLine as JobInvoiceLineModel;
use app\models\MasterData as MasterDataModel;
use app\models\PurchaseOrder as PurchaseOrderModel;
use app\models\PurchaseOrderLine as PurchaseOrderLineModel;
use app\models\Statement as StatementModel;
use app\models\StatementLine as StatementLineModel;
use app\models\OrderHeader as OrderHeaderModel;
use app\models\OrderLine as OrderLineModel;
use yii\data\ActiveDataProvider;

class Pdf
{
	// define barcode style
	private $styleLabels = array(
		'position' => '',
		'align' => 'L',
		'stretch' => false,
		'fitwidth' => true,
		'cellfitalign' => '',
		'border' => false,
		'hpadding' => '',
		'vpadding' => 'auto',
		'fgcolor' => array(0, 0, 0),
		'bgcolor' => false,
		'text' => true,
		'font' => 'helvetica',
		'fontsize' => 8,
		'stretchtext' => 1
	);

	private $styleDiagrams = array(
		'position' => '',
		'align' => 'L',
		'stretch' => false,
		'fitwidth' => true,
		'cellfitalign' => '',
		'border' => false,
		'hpadding' => '',
		'vpadding' => 'auto',
		'fgcolor' => array(0, 0, 0),
		'bgcolor' => false,
		'text' => true,
		'font' => 'helvetica',
		'fontsize' => 7,
		'stretchtext' => 1
	);

	private $topMargin = 10;
	private $bottomMargin = 10;
	private $pageNum;
	private $xPos = 10;
	private $yPos = 10;
	private $lineHeight = 4;
	private $option = 2;

	private $PADDING_LEFT = 10;
	private $PADDING_TOP = 10;
	private $PADDING_BOTTOM = 10;

	private $LETTER_PADDING_LEFT = 6.5;
	private $LETTER_PADDING_RIGHT = 7.9;
	private $LETTER_PADDING_TOP = 12.7;
	private $LETTER_PADDING_BOTTOM = 10.6;

	private $CHECKOUT_COLUMNS = 2;
	private $CHECKOUT_ROWS = 20;
	private $CHECKOUT_MARGIN_TOP = 10;
	private $CHECKOUT_MARGIN_BOTTOM = 10;
	private $CHECKOUT_MARGIN_LEFT = 10;
	private $CHECKOUT_MARGIN_RIGHT = 10;
	private $CHECKOUT_HEADER_HEIGHT = 4;
	private $CHECKOUT_COLUMN_DATA = [
		['field' => 'MD1_IMAGE', 'width' => .13, 'align' => 'L', 'headerText' => 'Image'],
		['field' => 'MD1_PART', 'width' => .55, 'align' => 'L', 'headerText' => 'Part Info'],
		['field' => 'UM1_NAME', 'width' => .15, 'align' => 'L', 'headerText' => 'Unit'],
		['field' => 'MD1_QTY', 'width' => .15, 'align' => 'R', 'headerText' => 'Qty']];

	// Inv. Activity Logs
	public function printActivities($params) {
		if (!empty($params['option'])) {
			$this->option = (int)$params['option'];
		}

		$headerModel = new InventoryModel();
		$inventoryLines = $headerModel->getActivityLogsForXLS( $params );

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Times', '', 7);
		$pdf->SetMargins(0, 0, 0, 0);

		$headerText = 'INVENTORY LOG';

		$pdf = $this->_printInventoryLogHeader($pdf, $headerText);
		$pdf = $this->_printInventoryLogLineHeader($pdf);
		foreach($inventoryLines as $line) {
			$pdf = $this->_printInventoryLogLine($pdf, $line);
		}

		return $pdf;
	}

	private function _printInventoryLogHeader($pdf, $headerText) {
		$pdf->AddPage('L', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$user = Yii::$app->user->getIdentity();
		$LO1_ID = $user->LO1_ID;
		$CO1_ID = $user->CO1_ID;

		$locationModel = new Location();
		$query = $locationModel->getSearchQuery(['LO1_ID' => $LO1_ID]);
		$query->asArray();
		$location = $query->one();
		$LO1_NAME = isset($location['LO1_NAME']) ? $location['LO1_NAME'] : 'All Stores';

		$companyModel = new Company();
		$query2 = $companyModel->getSearchQuery(['CO1_ID' => $CO1_ID]);
		$query2->asArray();
		$company = $query2->one();
		$CO1_NAME = isset($company['CO1_NAME']) ? $company['CO1_NAME'] : 'All Companies';

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, $headerText . "\n" . $CO1_NAME . ( $LO1_NAME != '' ? ' - ' . $LO1_NAME : '') . "\n" . Date('m/d/Y'), 0, 'C', 0, 0, $this->xPos, $this->yPos, true);

		$this->yPos = $this->yPos + ($this->lineHeight * 6);

		return $pdf;
	}

	private function _printInventoryLogLineHeader($pdf) {
		$this->yPos += $this->lineHeight;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.17, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.25, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.33, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.45, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.50, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.56, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.59, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.91, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.94, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.97, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->lineHeight, 'Date', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.09);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'By', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.17);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'Shop', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.25);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'Tech', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.33);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'Desc', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.45);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Type', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.50);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'Number', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.56);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'Line', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.59);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'Part Desc', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'Unit', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.91);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'Old', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.94);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'New', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.97);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'Adj', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos = $this->yPos + $this->lineHeight;

		return $pdf;
	}

	private function _printInventoryLogLine($pdf, $line) {
		$pdf = $this->_checkPage($pdf, '_printInventoryLogLineHeader', true, 'L', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.17, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.25, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.33, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.45, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.50, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.56, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.59, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.91, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.94, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.97, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');

		$pdf->SetFont('Times', '', 8);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->lineHeight, $line['LG1_CREATED_ON'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*0.8, $line['CREATED_BY'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*0.8, $line['LO1_NAME'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.17, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*0.8, $line['TE1_NAME'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.25, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight*0.8, $line['LG1_DESC'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.33, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.45);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['LTYPE'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.50);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, $line['NUMBER'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.56);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, $line['LINE'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.59);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, $line['MD1_PART_NUMBER'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight*0.8, $line['MD1_DESC1'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, '$' . Yii::$app->formatter->format($line['LG1_UNIT_COST'], ['decimal', 5]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.91);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, $line['LG1_OLD_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.94);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, $line['LG1_NEW_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.97);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, $line['LG1_ADJ_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight;

		return $pdf;
	}

	// Invoices
	public function printInvoices($params) {
		$invoiceModel = new InvoiceModel();
		$query = $invoiceModel->getQuery( $params );
		$invoiceModels = $query->asArray()->all();

		$model = new InvoiceLineModel();
		$dataProviderLines = $model->getPdfInvoiceLines($params);
		$dataProviderLines->query->asArray();
		$invoiceLines =  $dataProviderLines->getModels();
		$categories = [];

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(0, 0, 0, 0);

		foreach ($invoiceLines as $record) {
			$contained = false;
			foreach($categories as $cat) {
				if($cat["CA1_ID"] == $record["CA1_ID"] && $cat['IN1_ID'] == $record['IN1_ID']) {
					$cat["CA1_SUBTOTAL"] = $cat["CA1_SUBTOTAL"] + $record['IN2_TOTAL_VALUE'];
					$contained = true;
				}
			}

			if(!$contained) {
				$newCat = [ 'CA1_ID' => $record["CA1_ID"],
					'IN1_ID' => $record['IN1_ID'],
					'CA1_NAME' => $record["CA1_NAME"],
					'CA1_SUBTOTAL' => $record['IN2_TOTAL_VALUE']
				];
				$categories[] = $newCat;
			}
		}

		foreach($invoiceModels as $header) {
			$pdf = $this->_printInvoiceHeader($pdf, $header);

			foreach($categories as $category) {
				if ($header['IN1_ID'] == $category['IN1_ID']) {
					$pdf = $this->_printInvoiceLineHeader($pdf, (int)$header['IN1_TOTAL_VALUE'] < 0);
					for($i = 0; $i < sizeof($invoiceLines); $i++) {
						if($invoiceLines[$i]["CA1_ID"] == $category["CA1_ID"] && $invoiceLines[$i]["IN1_ID"] == $category["IN1_ID"]
							&& $invoiceLines[$i]["IN1_ID"] == $header["IN1_ID"]) {
							$pdf = $this->_printInvoiceLine($pdf, $invoiceLines[$i], (int)$header['IN1_TOTAL_VALUE'] < 0);
						}
					}
				}
			}
			$pdf = $this->_printInvoiceFooter($pdf, $header, $categories);
		}

		return $pdf;
	}

	private function _printInvoiceHeader($pdf, $header) {
		$pdf->AddPage('P', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, ( (int)$header['IN1_TOTAL_VALUE']<0 ? 'CREDIT MEMO #':'INVOICE/PICK TICKET #') . $header["IN1_NUMBER"], 0, 'C', 0, 0, $this->xPos, $this->yPos, true);
		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*7, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*7, 'D');

		if ((int)$header['IN1_TOTAL_VALUE'] > 0) {
			$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*7, 'D');
		}

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos);

		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'From:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$fromString = $header['CO1_NAME'] . "\n" . $header['FROM_NAME'] . "\n" . $header['FROM_ADDRESS1'] .
			(strlen($header['FROM_ADDRESS2']) > 0 ? ", " . $header['FROM_ADDRESS2'] . "\n" : "\n") .
			$header['FROM_CITY'] . ", " . $header['FROM_STATE'] . " " . $header['FROM_ZIP'] . "\n" .
			(strlen($header['FROM_COUNTRY']) > 0 ? $header['FROM_COUNTRY'] . "\n" : "") .
			(strlen($header['FROM_PHONE']) > 0 ? "Phone: " . $header['FROM_PHONE'] . "\n" : "") .
			(strlen($header['FROM_FAX']) > 0 ? "Fax: " . $header['FROM_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $fromString, 0, 'L', 0, 0,
			$this->xPos, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'To:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$toString = $header['CO1_NAME'] . "\n" . $header['TO_NAME'] . "\n" . $header['TO_ADDRESS1'] .
			(strlen($header['TO_ADDRESS2']) > 0 ? ", " . $header['TO_ADDRESS2'] . "\n" : "\n") .
			$header['TO_CITY'] . ", " . $header['TO_STATE'] . " " . $header['TO_ZIP'] . "\n" .
			(strlen($header['TO_COUNTRY']) > 0 ? $header['TO_COUNTRY'] . "\n" : "") .
			(strlen($header['TO_PHONE']) > 0 ? "Phone: " . $header['TO_PHONE'] . "\n" : "") .
			(strlen($header['TO_FAX']) > 0 ? "Fax: " . $header['TO_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $toString, 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*3, true);

		if ((int)$header['IN1_TOTAL_VALUE'] > 0) {
			$pdf->SetFont('Times', 'B', 9);
			$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
			$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666);
			$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'Order info:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$vendString = 'Order #: ' . $header['OR1_NUMBER'] . "\n" .
				'Comments: ' . $header['OR1_COMMENT'] . "\n" .
				'Shipping Method: ' . $header['OR1_SHIPPING_METHOD'] . "\n" .
				'Ordered On: ' . $header['CREATED_ON'];

			$pdf->SetFont('Times', '', 9);
			$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $vendString, 0, 'L', 0, 0,
				$this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*3, true);
		}

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$this->yPos = $this->yPos + $this->lineHeight * 11;

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()/2 - $this->xPos*2 -2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos, ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + $pdf->getPageWidth()/2 + 2, $this->yPos, $pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + $pdf->getPageWidth()/2 + 2, $this->yPos + $this->lineHeight*1, $pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*3, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + $pdf->getPageWidth()/2 + 2);
		$pdf->Cell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'Comment:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*0.8, $header['IN1_COMMENT'], 0, 'L', 0, 0,
			$this->xPos + $pdf->getPageWidth()/2 +2, $this->yPos + $this->lineHeight, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->lineHeight, ((int)$header['IN1_TOTAL_VALUE'] < 0) ? 'Credited On:' : 'Invoiced On:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight*0.8, $header['CREATED_ON'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos, true);

		$this->yPos = $this->yPos + ($this->lineHeight * 6);

		return $pdf;
	}

	private function _printInvoiceLineHeader($pdf, $isCreditMemo = false) {
		$this->yPos += $this->lineHeight;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		if ($isCreditMemo) {
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->lineHeight, 'D');
		} else {
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->lineHeight, 'D');
		}

		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.43, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Line', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		if ($isCreditMemo) {
			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'Inv Link', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->lineHeight, 'Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		} else {
			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->lineHeight, 'Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		}

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'Category', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.43);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->lineHeight, 'Description', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Qty', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.1, $this->lineHeight, 'Price', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'UoM', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'Total', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight;

		return $pdf;
	}

	private function _printInvoiceLine($pdf, $line, $isCreditMemo = false) {
		$pdf = $this->_checkPage($pdf, '_printInvoiceLineHeader', true, 'L', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		if ($isCreditMemo) {
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->lineHeight*2, 'D');
		} else {
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->lineHeight*2, 'D');
		}

		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.43, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight*2, 'D');

		$pdf->SetFont('Times', '', 9);
		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['IN2_LINE'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		if ($isCreditMemo) {
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, $line['IN2_LINK_FROM'], 0, 'L', 0, 0,
				$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, true);

			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->lineHeight, $line['MD1_PART_NUMBER'], 0, 'L', 0, 0,
				$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, true);
		} else {
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->lineHeight, $line['MD1_PART_NUMBER'], 0, 'L', 0, 0,
				$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, true);
		}

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, $line['CA1_NAME'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, true);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->lineHeight, $line['MD1_DESC1'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.43, $this->yPos, true);

		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, Yii::$app->formatter->format($line['IN2_INVOICE_QTY'], ['integer']), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, '$'. Yii::$app->formatter->format($line['IN2_UNIT_PRICE'], ['decimal', 5]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['UM1_SHORT_CODE'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($line['IN2_TOTAL_VALUE'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight*2;

		return $pdf;
	}

	private function _printInvoiceFooter($pdf, $header, $categories) {
		$this->yPos += $this->lineHeight;

		$pdf = $this->_checkPage($pdf, '_printInvoiceLineHeader', false, 'L', $this->PADDING_LEFT, $this->PADDING_TOP);

		foreach($categories as $cat) {
			if ($cat['IN1_ID'] == $header['IN1_ID']) {
				$pdf->SetFont('Times', '', 9);
				$pdf->SetY($this->yPos);
				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.34);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.55, $this->lineHeight, $cat['CA1_NAME'] . " Subtotal:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($cat['CA1_SUBTOTAL'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

				$this->yPos += $this->lineHeight;
				$pdf = $this->_checkPage($pdf, '_printInvoiceLineHeader', false, 'L', $this->PADDING_LEFT, $this->PADDING_TOP);
			}
		}

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, "Invoice Total:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($header['IN1_TOTAL_VALUE'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		return $pdf;
	}

	// Job Invoices
	public function printJobInvoices($params, $flag = 0) {
		if (!empty($params['option'])) {
			$this->option = (int)$params['option'];
		}

		if ($flag == 0) {
			$headerModel = new JobInvoiceModel();
			$jobInvoiceHeaders = $headerModel->getPdfJobInvoices( $params );
		} else {
			$headerModel = new JobTicketModel();
			$jobInvoiceHeaders = $headerModel->getPdfJobTickets( $params );
		}

		$model = new JobInvoiceLineModel();
		$jobInvoiceLines = $model->getPdfLines($params);

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(0, 0, 0, 0);

		foreach($jobInvoiceHeaders as $header) {
			$pdf = $this->_printJobInvoiceHeader($pdf, $header);
			$pdf = $this->_printJobInvoiceLineHeader($pdf);

			foreach($jobInvoiceLines as $line) {
				if($line["IJ1_ID"] == $header["IJ1_ID"]) {
					$pdf = $this->_printJobInvoiceLine($pdf, $line);
				}
			}

			$pdf = $this->_printJobInvoiceFooter($pdf, $header);
		}

		return $pdf;
	}

	private function _printJobInvoiceHeader($pdf, $header) {
		$pdf->AddPage('P', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, 'INVOICE #' . $header['IJ1_NUMBER'], 0, 'C', 0, 0, $this->xPos, $this->yPos, true);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*4, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*4, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*4, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos);

		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'Vendor:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$vendString = $header['VD1_NAME'] . "\n" . $header['VD1_ADDRESS1'] .
			(strlen($header['VD1_ADDRESS2']) > 0 ? ", " . $header['VD1_ADDRESS2'] . "\n" : "\n") .
			$header['VD1_CITY'] . ", " . $header['VD1_STATE'] . " " . $header['VD1_ZIP'] . "\n";

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $vendString, 0, 'L', 0, 0, $this->xPos, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'Sold to:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$shopString = $header['CO1_NAME'] . "\n" . $header['LO1_NAME'] . "\n" . $header['LO1_ADDRESS1'] .
			(strlen($header['LO1_ADDRESS2']) > 0 ? ", " . $header['LO1_ADDRESS2'] . "\n" : "\n") .
			$header['LO1_CITY'] . ", " . $header['LO1_STATE'] . " " . $header['LO1_ZIP'];

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $shopString, 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'Customer:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$customerString = $header['CU1_NAME'] . "\n";

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $customerString, 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$this->yPos = $this->yPos + $this->lineHeight * 8;

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight, ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->yPos, ($pdf->getPageWidth() - $this->xPos*2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->yPos + $this->lineHeight, ($pdf->getPageWidth() - $this->xPos*2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos + $this->lineHeight, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*2, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5);
		$pdf->Cell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'Comment:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$commentText = str_replace('/\n/gm', '/', str_replace('/\r/gm', '/', str_replace('/\t/gm', '/', $header['IJ1_COMMENT'])));
		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*0.8, $commentText, 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos + $this->lineHeight, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + 2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->lineHeight, 'Invoiced On:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.35, $this->lineHeight*0.8, $header['IJ1_CREATED_ON'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->yPos, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight + 2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->lineHeight, 'Repair Order #:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.35, $this->lineHeight*0.8, $header['JT1_NUMBER'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->yPos + $this->lineHeight, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 + 2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->lineHeight, 'Ticket Created:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.35, $this->lineHeight*0.8, $header['JT1_CREATED_ON'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.15, $this->yPos + $this->lineHeight*2, true);

		$this->yPos = $this->yPos + ($this->lineHeight * 4);

		return $pdf;
	}

	private function _printJobInvoiceLineHeader($pdf) {
		$this->yPos += $this->lineHeight;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.40, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.77, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, 'Line', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.04);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->lineHeight, 'Vendor Part Number', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.40, $this->lineHeight, 'Part Description', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'Quantity', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.77);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'UM', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'Price' . ($this->option == 1 ? '(Cost)' : ''), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'Total' . ($this->option == 1 ? '(Cost)' : ''), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos = $this->yPos + $this->lineHeight;

		return $pdf;
	}

	private function _printJobInvoiceFooter($pdf, $header) {
		$this->yPos += 4;

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, "Invoice Total" . ($this->option == 1 ? '(Cost):':':'), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($header['IJ1_TOTAL_VALUE'], ['decimal', 2]) . ($this->option == 1 ? "\n($". Yii::$app->formatter->format($header['IJ1_TOTAL_SELL'], ['decimal', 2]) .')' : ''), 0, 'R', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89, $this->yPos + $this->lineHeight - 2, true);

		return $pdf;
	}

	private function _printJobInvoiceLine($pdf, $line) {
		$pdf = $this->_checkPage($pdf, '_printLineHeader', true, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.40, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.77, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*2, 'D');


		$pdf->SetFont('Times', '', 9);
		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, $line['IJ2_LINE'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.04);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->lineHeight, $line['MD1_VENDOR_PART_NUMBER'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30);
		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->lineHeight, $line['IJ2_DESC1'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.40, $this->yPos, true);

		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, $line['IJ2_INVOICE_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.77);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, $line['UM1_SHORT_CODE'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, '$'. Yii::$app->formatter->format($line['IJ2_SELL_PRICE'], ['decimal', 2]) . ($this->option == 1 ? "\n($". Yii::$app->formatter->format($line['IJ2_UNIT_PRICE'], ['decimal', 2]) .')' : ''), 0, 'R', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, true);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, '$'. Yii::$app->formatter->format($line['IJ2_TOTAL_SELL'], ['decimal', 2]) . ($this->option == 1 ? "\n($". Yii::$app->formatter->format($line['IJ2_TOTAL_VALUE'], ['decimal', 2]) .')' : ''), 0, 'R', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92, $this->yPos, true);

		$this->yPos += $this->lineHeight*2;

		return $pdf;
	}

	// Master Data
	public function printMasterData($params) {
		$masterData = new MasterDataModel();
		$dataProvider = $masterData->search( $params );
		$dataProvider->query->asArray();
		$models = $dataProvider->getModels();

		$pdf = $this->_printLabels($params, $models);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');

		return $pdf;
	}

	public function generatePrintQtySheet($params) {
		$labels = array(
			[ 'MD1_DESC1' => '1', 'MD1_UPC1' => '+QTY1', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '2', 'MD1_UPC1' => '+QTY2', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '3', 'MD1_UPC1' => '+QTY3', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '4', 'MD1_UPC1' => '+QTY4', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '5', 'MD1_UPC1' => '+QTY5', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '6', 'MD1_UPC1' => '+QTY6', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '7', 'MD1_UPC1' => '+QTY7', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '8', 'MD1_UPC1' => '+QTY8', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '9', 'MD1_UPC1' => '+QTY9', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '10', 'MD1_UPC1' => '+QTY10', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '11', 'MD1_UPC1' => '+QTY11', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '12', 'MD1_UPC1' => '+QTY12', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '13', 'MD1_UPC1' => '+QTY13', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '14', 'MD1_UPC1' => '+QTY14', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '15', 'MD1_UPC1' => '+QTY15', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '16', 'MD1_UPC1' => '+QTY16', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '17', 'MD1_UPC1' => '+QTY17', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '18', 'MD1_UPC1' => '+QTY18', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '19', 'MD1_UPC1' => '+QTY19', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '20', 'MD1_UPC1' => '+QTY20', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '21', 'MD1_UPC1' => '+QTY21', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '22', 'MD1_UPC1' => '+QTY22', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '23', 'MD1_UPC1' => '+QTY23', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '24', 'MD1_UPC1' => '+QTY24', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '25', 'MD1_UPC1' => '+QTY25', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '26', 'MD1_UPC1' => '+QTY26', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '27', 'MD1_UPC1' => '+QTY27', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '28', 'MD1_UPC1' => '+QTY28', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '29', 'MD1_UPC1' => '+QTY29', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '30', 'MD1_UPC1' => '+QTY30', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.10', 'MD1_UPC1' => '+QTY.1', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.20', 'MD1_UPC1' => '+QTY.2', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.30', 'MD1_UPC1' => '+QTY.3', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.40', 'MD1_UPC1' => '+QTY.4', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.50', 'MD1_UPC1' => '+QTY.5', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.60', 'MD1_UPC1' => '+QTY.6', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.70', 'MD1_UPC1' => '+QTY.7', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.80', 'MD1_UPC1' => '+QTY.8', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.90', 'MD1_UPC1' => '+QTY.9', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.25', 'MD1_UPC1' => '+QTY.25', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.50', 'MD1_UPC1' => '+QTY.5', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '.75', 'MD1_UPC1' => '+QTY.75', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 1', 'MD1_UPC1' => '+QTY1', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 2', 'MD1_UPC1' => '+QTY2', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 3', 'MD1_UPC1' => '+QTY3', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 4', 'MD1_UPC1' => '+QTY4', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 5', 'MD1_UPC1' => '+QTY5', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 6', 'MD1_UPC1' => '+QTY6', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 7', 'MD1_UPC1' => '+QTY7', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 8', 'MD1_UPC1' => '+QTY8', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 9', 'MD1_UPC1' => '+QTY9', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 10', 'MD1_UPC1' => '+QTY10', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 20', 'MD1_UPC1' => '+QTY20', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 30', 'MD1_UPC1' => '+QTY30', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 40', 'MD1_UPC1' => '+QTY40', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 50', 'MD1_UPC1' => '+QTY50', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 60', 'MD1_UPC1' => '+QTY60', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 70', 'MD1_UPC1' => '+QTY70', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 80', 'MD1_UPC1' => '+QTY80', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 90', 'MD1_UPC1' => '+QTY90', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 100', 'MD1_UPC1' => '+QTY100', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => ' 500', 'MD1_UPC1' => '+QTY500', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => 'DELETE QTY', 'MD1_UPC1' => '+QTY-RESET', 'MD1_PART_NUMBER' => '' ],
			[ 'MD1_DESC1' => 'SAVE',       'MD1_UPC1' => '+SAVE',      'MD1_PART_NUMBER' => '' ]
		);

		$pdf = $this->_printLabels($params, $labels);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');

		return $pdf;
	}

	private function _printLabels($params, $items)
	{
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->SetAutoPageBreak(TRUE, 0);

		$cols = 3;
		$colsPadding = isset($params['cols']) && $params['cols'] !== 0 ? $params['cols'] : 1;
		$rowsPadding = isset($params['rows']) && $params['rows'] !== 0 ? $params['rows'] : 0;

		$emptyItemsCountsToPush = $rowsPadding * 3 + ($colsPadding - 1);
		for ($emptyItemsIndex = 0; $emptyItemsIndex < $emptyItemsCountsToPush; $emptyItemsIndex++) {
			array_unshift($items, ['MD1_PART_NUMBER' => '', 'MD1_DESC1' => '', 'MD1_UPC1' => '', 'MD1_PART_NUMBER' => '']);
		}

		$numPages = ceil(sizeof($items) / ($cols * 10));

		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(0, $this->topMargin, 0, $this->bottomMargin);

		$this->yPos = $this->LETTER_PADDING_TOP + 2;
		$this->xPos = $this->LETTER_PADDING_LEFT;

		$itemWidth = (($pdf->getPageWidth()-$this->LETTER_PADDING_LEFT-$this->LETTER_PADDING_RIGHT) - 5 * ($cols-1)) / $cols;
		$itemHeight = (($pdf->getPageHeight()-$this->LETTER_PADDING_TOP - $this->LETTER_PADDING_BOTTOM)) / 10 - 2;

		$barcodeWidth = (($pdf->getPageWidth()-$this->LETTER_PADDING_LEFT-$this->LETTER_PADDING_RIGHT) - 10 * ($cols-1)) / $cols;
		$barcodeHeight = 10;
		$barcodeType = isset($params['barcodeType']) ? $params['barcodeType'] : 'C39';
		$pdf->SetLineStyle(array('width' => 0.25));

		for ($pageIndex = 0; $pageIndex < $numPages; $pageIndex++) {
			$pdf->AddPage('P', 'LETTER');
			for ($rowIndex = 0; $rowIndex < 10; $rowIndex++) {
				$arr[] = $this->yPos + $itemHeight * $rowIndex;
				if ((0 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex)) < sizeof($items) && $items[(0 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex))]) {
					if ((0 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex)) < sizeof($items)) {
						$item = $items[(0 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex))];
						$pdf->SetY($this->yPos + $itemHeight * $rowIndex);
						$pdf->SetX($this->xPos + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, $item['MD1_PART_NUMBER'], 0, 1);

						$pdf->SetY($this->yPos + $itemHeight * $rowIndex + $this->lineHeight);
						$pdf->SetX($this->xPos + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, substr($item['MD1_DESC1'], 0, 28), 0, 1);

						if ($item['MD1_UPC1'] && $item['MD1_UPC1'] !== '') {
							$label = $item['MD1_UPC1'];
						} else if ($item['MD1_PART_NUMBER'] && $item['MD1_PART_NUMBER'] !== '') {
							$label = $item['MD1_PART_NUMBER'];
						} else {
							$label = '';
						}

						if ($label != '') {
							$pdf->write1DBarcode($label, $barcodeType, $this->xPos + 2, $this->yPos + $itemHeight * $rowIndex + $this->lineHeight * 2, $barcodeWidth, $barcodeHeight, 0.19, $this->styleLabels, 'L');
						}
					}

					if ((1 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex)) < sizeof($items) && $cols > 1) {
						$item = $items[(1 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex))];
						$pdf->SetY($this->yPos + $itemHeight * $rowIndex);
						$pdf->SetX($this->xPos + ($itemWidth + 5 + 3) + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, $item['MD1_PART_NUMBER'], 0, 1);

						$pdf->SetY($this->yPos + $itemHeight * $rowIndex + $this->lineHeight);
						$pdf->SetX($this->xPos + ($itemWidth + 5 + 3) + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, substr($item['MD1_DESC1'], 0, 28), 0, 1);

						if ($item['MD1_UPC1'] && $item['MD1_UPC1'] !== '') {
							$label = $item['MD1_UPC1'];
						} else if ($item['MD1_PART_NUMBER'] && $item['MD1_PART_NUMBER'] !== '') {
							$label = $item['MD1_PART_NUMBER'];
						} else {
							$label = '';
						}

						if ($label != '') {
							$pdf->write1DBarcode($label, $barcodeType, $this->xPos + ($itemWidth + 5 + 3) + 2, $this->yPos + $itemHeight * $rowIndex + $this->lineHeight * 2, $barcodeWidth, $barcodeHeight, 0.19, $this->styleLabels, 'L');
						}
					}

					if ((2 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex)) < sizeof($items) && $cols > 2) {
						$item = $items[(2 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex))];
						$pdf->SetY($this->yPos + $itemHeight * $rowIndex);
						$pdf->SetX($this->xPos + (($itemWidth + 5 + 3) * 2) + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, $item['MD1_PART_NUMBER'], 0, 1);

						$pdf->SetY($this->yPos + $itemHeight * $rowIndex + $this->lineHeight);
						$pdf->SetX($this->xPos + (($itemWidth + 5 + 3) * 2) + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, substr($item['MD1_DESC1'], 0, 28), 0, 1);

						if ($item['MD1_UPC1'] && $item['MD1_UPC1'] !== '') {
							$label = $item['MD1_UPC1'];
						} else if ($item['MD1_PART_NUMBER'] && $item['MD1_PART_NUMBER'] !== '') {
							$label = $item['MD1_PART_NUMBER'];
						} else {
							$label = '';
						}

						if ($label != '') {
							$pdf->write1DBarcode($label, $barcodeType, $this->xPos + (($itemWidth + 5 + 3) * 2) + 2, $this->yPos + $itemHeight * $rowIndex + $this->lineHeight * 2, $barcodeWidth, $barcodeHeight, 0.19, $this->styleLabels, 'L');
						}
					}

				}
			}
		}

		return $pdf;
	}

	public function printRack($params) {
		try {
			$masterData = new MasterDataModel();
			$items = $masterData->getRack( $params );
		} catch (\Exception $exception) {
			return [
				'error' => $exception
			];
		}

		$pdf = new TCPDF('L', PDF_UNIT, 'TABLOID', true, 'windows-1252', false);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');

		$selectedItems = [];

		$user = Yii::$app->user->getIdentity();
		$fromLocation = Location::findOne(['LO1_ID' => $user->LO1_ID]);

		if ($fromLocation && isset($fromLocation['LO1_RACKS'])) {
			for ($i = 0; $i < intval($fromLocation['LO1_RACKS']) * 4 * 24; $i++)
				$selectedItems[] = [];
		}

		foreach ($items as $record) {
			$idx = ((intval($record['MD2_RACK'])-1)*4*24) + ((intval($record['MD2_DRAWER'])-1)*24) + ((intval($record['MD2_BIN'])-1));
			if (isset($idx) && $idx >= 0) {
				$counter = 1;
				if (isset($selectedItems[$idx]) && $selectedItems[$idx] && isset($selectedItems[$idx]['counter']))
					$counter = $selectedItems[$idx]['counter'] + 1;
				$selectedItems[$idx] = [
					'MD1_ID' => $record['MD1_ID'],
					'MD2_ID' => $record['MD2_ID'],
					'VD1_ID' => $record['VD1_ID'],
					'CA1_ID' => $record['CA1_ID'],
					'VD1_NAME' => $record['VD1_NAME'],
					'CA1_NAME' => $record['CA1_NAME'],
					'MD1_PART_NUMBER' => $record['MD1_PART_NUMBER'],
					'MD1_IMAGE' => $record['MD1_IMAGE'],
					'FI1_FILE_PATH' => $record['FI1_FILE_PATH'],
					'MD1_VENDOR_PART_NUMBER' => $record['MD1_VENDOR_PART_NUMBER'],
					'MD1_UPC1' => $record['MD1_UPC1'],
					'MD1_DESC1' => $record['MD1_DESC1'],
					'UM1_NAME' => $record['UM1_NAME'],
					'MD2_MIN_QTY' => $record['MD2_MIN_QTY'],
					'MD2_RACK' => $record['MD2_RACK'],
					'MD2_DRAWER' => $record['MD2_DRAWER'],
					'MD2_BIN' => $record['MD2_BIN'],
					'counter' => $counter];
			}
		}

		$numPages = ceil(sizeof($selectedItems) / 24);

		$noPhotoImageUrl = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['partsImagesPath']  . DIRECTORY_SEPARATOR . 'noimage.png';
		$noPhotoImage = file_get_contents($noPhotoImageUrl);

		$pdf->setCellHeightRatio(0.85);

		for ($pageIndex = 0; $pageIndex < $numPages; $pageIndex++) {
			$pdf->AddPage('L', 'TABLOID');

			$logo = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['partsImagesPath']  . DIRECTORY_SEPARATOR . 'nuventory-large.jpg';
			$img = file_get_contents($logo);
			$pdf->Image('@' . $img, $this->PADDING_LEFT+2, $this->PADDING_TOP+1, '', 10, 'jpg', '', 'T', false, 300, '', false, false, 0, false, false, false);

			$pdf->SetY($pdf->getPageHeight() - $this->PADDING_BOTTOM - 10);
			$pdf->SetFont('Courier', '', 12);
			$pdf->Cell(0, 10, 'http://www.nuventory.com/ Copyright 2008-2019 Nuventory, All Rights Reserved. +1(833)688-3689', 0, false, 'C', 0, '', 0, false, 'T', 'M');

			// HEADER
			$pdf->SetFont('Courier', 'B', 18);
			// Title
			$pdf->SetY($this->PADDING_TOP + 6);
			$text = "Nuventory Schema       \t\tRack " . floor($pageIndex/4+1) . ", Drawer " . ($pageIndex%4+1);
			$pdf->Cell(0, 15, $text, 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$xOff = 3;
			$yOff = 3;
			$this->lineHeight = 4;

			$this->yPos = $this->PADDING_TOP + 11;
			$this->xPos = $this->PADDING_LEFT;

			$pageHeight = $pdf->getPageHeight() - $this->PADDING_TOP - $this->PADDING_BOTTOM - 21;
			$pageWidth = $pdf->getPageWidth() - $this->PADDING_LEFT*2;
			$itemWidth = ($pageWidth) / 6;
			$itemHeight = ($pageHeight) / 4;
			$barcodeWidth = (($pageWidth) / 6) - $xOff*2;
			$barcodeHeight = 12;

			//draw grid
			$pdf->SetLineStyle(array('width' => 0.2));
			for($itemsIndex = 0; $itemsIndex < 24; $itemsIndex++) {
				$pdf->Rect($this->xPos + (($pageWidth)/6*($itemsIndex%6)), $this->yPos + (($pageHeight)/4*floor($itemsIndex/6)), ($pageWidth)/6, ($pageHeight)/4, 'D');
			}

			for($itemsIndex = 0; $itemsIndex < 24; $itemsIndex++) {
				if($itemsIndex + $pageIndex*24 < sizeof($selectedItems) && isset($selectedItems[$itemsIndex+$pageIndex*24]['MD1_ID']) && $selectedItems[$itemsIndex+$pageIndex*24]['MD1_ID']) {
					$pdf->SetFont('Courier', 'B', 14);
					$pdf->SetY($this->yPos + (($pageHeight)/4)*floor($itemsIndex/6) + $yOff);
					$pdf->SetX($this->xPos + ($pageWidth)/6*($itemsIndex%6) + $xOff);
					$pdf->Cell($itemWidth - $xOff*2, $this->lineHeight, ($selectedItems[$itemsIndex+$pageIndex*24]['counter'] > 1?$selectedItems[$itemsIndex+$pageIndex*24]['counter'] . "*":'') .
						$selectedItems[$itemsIndex+$pageIndex*24]['MD2_RACK'] . "-" .
						$selectedItems[$itemsIndex+$pageIndex*24]['MD2_DRAWER'] . "-" .
						$selectedItems[$itemsIndex+$pageIndex*24]['MD2_BIN'] . "   " . $selectedItems[$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER'], 0, 1);

					$pdf->SetFont('Courier', '', 13);
					$pdf->MultiCell($itemWidth - $xOff*2, $this->lineHeight*0.8, substr($selectedItems[$itemsIndex+$pageIndex*24]['MD1_DESC1'],0,68), 0, 'L', 0, 0, $this->xPos + ($pageWidth)/6*($itemsIndex%6) + $xOff, $this->yPos + (($pageHeight)/4)*floor($itemsIndex/6) + $this->lineHeight + $yOff, true);

					$min = intval($selectedItems[$itemsIndex+$pageIndex*24]['MD2_MIN_QTY']);

					if ($selectedItems[$itemsIndex+$pageIndex*24]['FI1_FILE_PATH'] && $selectedItems[$itemsIndex+$pageIndex*24]['FI1_FILE_PATH'] !== '' && file_exists(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['partsImagesPath']  . DIRECTORY_SEPARATOR . str_replace('/web', '', $selectedItems[$itemsIndex+$pageIndex*24]['FI1_FILE_PATH']))) {
						$imgUrl = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['partsImagesPath']  . DIRECTORY_SEPARATOR . str_replace('/web', '', $selectedItems[$itemsIndex+$pageIndex*24]['FI1_FILE_PATH']);
						$img = file_get_contents($imgUrl);
						$pdf->Image('@' . $img, $this->xPos + $itemWidth*($itemsIndex%6) + $xOff*5.25, $this->yPos + ($itemHeight)*floor($itemsIndex/6) + ($this->lineHeight * 4.5) + $yOff, ($itemWidth - $xOff*4)*.33, ($itemWidth - $xOff*4)*.33, 'jpg', '', 'T', false, 300, '', false, false, 0, false, false, false);
					} else {
						$pdf->Image('@' . $noPhotoImage, $this->xPos + $itemWidth*($itemsIndex%6) + $xOff*5.25, $this->yPos + ($itemHeight)*floor($itemsIndex/6) + ($this->lineHeight * 4.5) + $yOff, ($itemWidth - $xOff*4)*.33, ($itemWidth - $xOff*4)*.33, 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);
					}

					$pdf->SetFont('Courier', '', 13);
					$pdf->SetY($this->yPos + (($pageHeight)/4)*floor($itemsIndex/6) + $this->lineHeight * 9.25 + $yOff);
					$pdf->SetX($this->xPos + ($pageWidth)/6*($itemsIndex%6) + $xOff);
					$pdf->Cell($itemWidth - $xOff*2, $this->lineHeight, "MIN: " . ((!is_nan($min)&&$min>0)?$min:0) . "  \t" . $selectedItems[$itemsIndex+$pageIndex*24]['CA1_NAME'], 0, 1);

					if($selectedItems[$itemsIndex+$pageIndex*24]['MD1_UPC1'] && $selectedItems[$itemsIndex+$pageIndex*24]['MD1_UPC1'] !== '') {
						$label = $selectedItems[$itemsIndex+$pageIndex*24]['MD1_UPC1'];
					} else if($selectedItems[$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER'] && $selectedItems[$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER'] !== '') {
						$label = $selectedItems[$itemsIndex+$pageIndex*24]['MD1_PART_NUMBER'];
					} else {
						$label = $labelText = "";
					}

					if ($label != '') {
						$pdf->write1DBarcode($label, 'C39', $this->xPos + $itemWidth*($itemsIndex%6) + $xOff, $this->yPos + ($itemHeight)*floor($itemsIndex/6) + $this->lineHeight * 10.5 + $xOff, $barcodeWidth, $barcodeHeight, 0.4, $this->styleDiagrams, 'L');
					}
				} else {
					$pdf->SetFont('Courier', 'B', 14);
					$pdf->SetY($this->yPos + (($pageHeight)/4)*floor($itemsIndex/6) + $yOff);
					$pdf->SetX($this->xPos + ($pageWidth)/6*($itemsIndex%6) + $xOff);
					$pdf->Cell($itemWidth - $xOff*2, $this->lineHeight, 'EMPTY', 0, 1);
					$pdf->Image('@' . $noPhotoImage, $this->xPos + $itemWidth*($itemsIndex%6) + $xOff*5.25, $this->yPos + ($itemHeight)*floor($itemsIndex/6) + ($this->lineHeight * 4.5) + $yOff, ($itemWidth - $xOff*4)*.33, ($itemWidth - $xOff*4)*.33, 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);
				}
			}
		}

		return $pdf;
	}

	public function printCheckoutSheets($params) {
		$masterData = new MasterDataModel();
		$items = $masterData->getRack( $params );

		$pdf = new TCPDF('P', PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);

		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(10, $this->topMargin, 12, $this->bottomMargin);

		$this->yPos = $this->CHECKOUT_MARGIN_TOP;
		$this->xPos = $this->CHECKOUT_MARGIN_LEFT;

		$selectedItems = [];
		foreach ($items as $record) {
			$selectedItems[] = [
				'MD1_ID' => $record['MD1_ID'],
				'MD2_ID' => $record['MD2_ID'],
				'VD1_ID' => $record['VD1_ID'],
				'CA1_ID' => $record['CA1_ID'],
				'VD1_NAME' => $record['VD1_NAME'],
				'CA1_NAME' => $record['CA1_NAME'],
				'MD1_PART' => $record['MD1_PART_NUMBER'] . "\n" . $record['MD1_DESC1'],
				'MD1_PART_NUMBER' => $record['MD1_PART_NUMBER'],
				'MD1_IMAGE' => $record['MD1_IMAGE'],
				'FI1_FILE_PATH' => $record['FI1_FILE_PATH'],
				'MD1_VENDOR_PART_NUMBER' => $record['MD1_VENDOR_PART_NUMBER'],
				'MD1_UPC1' => $record['MD1_UPC1'],
				'MD1_DESC1' => $record['MD1_DESC1'],
				'UM1_NAME' => $record['UM1_NAME'],
				'MD2_MIN_QTY' => $record['MD2_MIN_QTY'],
				'MD1_QTY' => ''];
		}

		$numPages = ceil(sizeof($items) / ($this->CHECKOUT_COLUMNS * $this->CHECKOUT_ROWS));
		$noPhotoImageUrl = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['partsImagesPath']  . DIRECTORY_SEPARATOR . 'noimage.png';
		$noPhotoImage = file_get_contents($noPhotoImageUrl);

		$user = Yii::$app->user->getIdentity();
		$fromLocation = Location::findOne(['LO1_ID' => $user->LO1_ID]);

		for ($pageIndex = 0; $pageIndex < $numPages || $numPages == 0 && $pageIndex == 0; $pageIndex++) {
			$pdf->AddPage('P', 'LETTER');

			$this->yPos = $this->CHECKOUT_MARGIN_TOP;
			$this->xPos = $this->CHECKOUT_MARGIN_LEFT;
			$pdf->SetFont('Courier', 'B', 10);
			$pdf->SetY($this->yPos);
			$pdf->SetX($this->xPos);
			$text = "Nuventory Checkout Sheet " . $fromLocation['LO1_NAME'] . " - " . $params['label'];
			$pdf->Cell(0, 10, $text, 0, false, 'C', 0, '', 0, false, 'M', 'M');

			$pdf->SetY($pdf->getPageHeight() - $this->CHECKOUT_MARGIN_TOP - 10);
			$pdf->SetFont('Courier', '', 8);
			$pdf->Cell(0, 10, 'http://www.nuventory.com/ Copyright 2008-2019 Nuventory, All Rights Reserved. +1(833)688-3689', 0, false, 'C', 0, '', 0, false, 'T', 'M');

			$pdf->SetY($this->CHECKOUT_MARGIN_TOP + 10);
			$pdf->SetX($this->CHECKOUT_MARGIN_LEFT);

			$pageHeight = $pdf->getPageHeight() - ($this->CHECKOUT_MARGIN_TOP + $this->CHECKOUT_MARGIN_BOTTOM + 20);
			$pageWidth = $pdf->getPageWidth() - ($this->CHECKOUT_MARGIN_LEFT + $this->CHECKOUT_MARGIN_RIGHT);

			$xOff = 0;
			$yOff = 0;

			for ($columnIndex = 0; $columnIndex < $this->CHECKOUT_COLUMNS; $columnIndex++) {
				$offsetX = 0;
				foreach ($this->CHECKOUT_COLUMN_DATA as $column) {
					$pdf->SetLineStyle(array('width' => 0.25));
					$this->xPos = $this->CHECKOUT_MARGIN_LEFT + (($pageWidth / $this->CHECKOUT_COLUMNS) * $columnIndex) + $offsetX;
					$this->yPos = $this->CHECKOUT_MARGIN_TOP + 10 - $this->CHECKOUT_HEADER_HEIGHT;
					$pdf->Rect($this->xPos, $this->yPos, ($pageWidth / $this->CHECKOUT_COLUMNS) * $column['width'], $this->CHECKOUT_HEADER_HEIGHT, 'D');

					$this->xPos = $this->CHECKOUT_MARGIN_LEFT + (($pageWidth / $this->CHECKOUT_COLUMNS) * $columnIndex) + $offsetX;
					$this->yPos = $this->CHECKOUT_MARGIN_TOP + 10 - $this->CHECKOUT_HEADER_HEIGHT;
					$pdf->SetY($this->yPos);
					$pdf->SetX($this->xPos);

					$offsetX += ($pageWidth / $this->CHECKOUT_COLUMNS) * $column['width'];
					$pdf->SetFont('Courier', 'B', 9);
					$pdf->SetY($this->yPos + $yOff);
					$pdf->SetX($this->xPos + $xOff);
					$pdf->MultiCell(($pageWidth / $this->CHECKOUT_COLUMNS) * $column['width'] - ($xOff * 2), $this->lineHeight,
						$column['headerText'], 0, $column['align'], 0, 0, $this->xPos + $xOff, $this->yPos + $yOff, true);
				}
			}

			for ($itemsIndex = 0; $itemsIndex + ($pageIndex * $this->CHECKOUT_COLUMNS * $this->CHECKOUT_ROWS) < sizeof($selectedItems) && $itemsIndex < $this->CHECKOUT_COLUMNS * $this->CHECKOUT_ROWS; $itemsIndex++) {
				$offsetX = 0;
				foreach ($this->CHECKOUT_COLUMN_DATA as $column) {
					$pdf->SetLineStyle(array('width' => 0.25));
					$this->xPos = $this->CHECKOUT_MARGIN_LEFT + (($pageWidth / $this->CHECKOUT_COLUMNS) * floor($itemsIndex / $this->CHECKOUT_ROWS)) + $offsetX;
					$this->yPos = $this->CHECKOUT_MARGIN_TOP + 10 + (($pageHeight / $this->CHECKOUT_ROWS) * ($itemsIndex % $this->CHECKOUT_ROWS));
					$pdf->Rect($this->xPos, $this->yPos, ($pageWidth / $this->CHECKOUT_COLUMNS) * $column['width'], ($pageHeight / $this->CHECKOUT_ROWS), 'D');

					$this->xPos = $this->CHECKOUT_MARGIN_LEFT + (($pageWidth / $this->CHECKOUT_COLUMNS) * floor($itemsIndex / $this->CHECKOUT_ROWS)) + $offsetX;
					$this->yPos = $this->CHECKOUT_MARGIN_TOP + 10 + (($pageHeight / $this->CHECKOUT_ROWS) * ($itemsIndex % $this->CHECKOUT_ROWS));
					$pdf->SetY($this->yPos);
					$pdf->SetX($this->xPos);
					$offsetX += ($pageWidth / $this->CHECKOUT_COLUMNS) * $column['width'];

					if ($column['field'] == 'MD1_IMAGE') {
						$imgUrl = $this->populateImage($selectedItems[$itemsIndex + $pageIndex * $this->CHECKOUT_COLUMNS * $this->CHECKOUT_ROWS]['MD1_IMAGE']);
						if ($imgUrl) {
							$img = file_get_contents($imgUrl);
							$pdf->Image('@' . $img, $this->xPos, $this->yPos, ($pageWidth / $this->CHECKOUT_COLUMNS) * $column['width'], ($pageHeight / $this->CHECKOUT_ROWS), 'jpg', '', 'T', false, 300, '', false, false, 0, false, false, false);
						} else {
							$pdf->Image('@' . $noPhotoImage, $this->xPos, $this->yPos, ($pageWidth / $this->CHECKOUT_COLUMNS) * $column['width'], ($pageHeight / $this->CHECKOUT_ROWS), 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);
						}
					} else {
						$pdf->SetFont('Courier', '', 9);
						$pdf->MultiCell(($pageWidth / $this->CHECKOUT_COLUMNS) * $column['width'] - ($xOff * 2), $this->lineHeight,
							substr($selectedItems[$itemsIndex + $pageIndex * $this->CHECKOUT_COLUMNS * $this->CHECKOUT_ROWS][$column['field']], 0, 70), 0, $column['align'], 0, 0, $this->xPos + $xOff, $this->yPos + $yOff, true);
					}
				}
			}
		}

		return $pdf;
	}

	private function populateImage($data) {
		$basePath = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . Yii::$app->params['partsImagesPath']  . DIRECTORY_SEPARATOR;

		if ( $data && file_exists( $basePath . $data) ) {
			return $basePath . $data;
		} else {
			return false;
		}
	}

	public function printInventoryOptimization($params) {
		if (!empty($params['option'])) {
			$this->option = (int)$params['option'];
		}

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Times', '', 7);
		$pdf->SetMargins(0, 0, 0, 0);

		switch ($params['REPORT_TYPE']) {
			case 'physicalInventory':
				$headerText = 'PHYSICAL INVENTORY REPORT';
				break;
			case 'inventoryOptimization':
			default:
				$headerText = 'INVENTORY OPTIMIZATION REPORT';
				break;
		}

		$pdf = $this->_printInventoryHeader($pdf, $headerText);
		$pdf = $this->_printInventoryLineHeader($pdf);

		$headerModel = new MasterDataModel();
		$query = $headerModel->getInventoryOptimizationQuery( $params );
		$inventoryLines = $query->asArray()->all();

		foreach($inventoryLines as $line) {
			if ($params['REPORT_TYPE'] == 'inventoryOptimization') {
				if ($line['PART_STATUS'] == 0) {
					$line['PART_STATUS'] = 'Normal';
				} elseif ($line['PART_STATUS'] == 1) {
					$line['PART_STATUS'] = 'Under';
				} elseif ($line['PART_STATUS'] == 2) {
					$line['PART_STATUS'] = 'Over';
				}
			}

			$pdf = $this->_printInventoryLine($pdf, $line);
		}
		$queryTotals = $headerModel->getInventoryOptimizationQuery( $params, true );
		$inventoryTotalValue = $queryTotals->asArray()->one()['EXTENDED_STOCK'];

		$pdf = $this->_printInventoryFooter($pdf, $inventoryTotalValue);
		return $pdf;
	}

	private function _printInventoryHeader($pdf, $headerText) {
		$pdf->AddPage('P', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$user = Yii::$app->user->getIdentity();
		$LO1_ID = $user->LO1_ID;
		$CO1_ID = $user->CO1_ID;

		$locationModel = new Location();
		$query = $locationModel->getSearchQuery(['LO1_ID' => $LO1_ID]);
		$query->asArray();
		$location = $query->one();
		$LO1_NAME = isset($location['LO1_NAME']) ? $location['LO1_NAME'] : '';

		$companyModel = new Company();
		$query2 = $companyModel->getSearchQuery(['CO1_ID' => $CO1_ID]);
		$query2->asArray();
		$company = $query2->one();
		$CO1_NAME = isset($company['CO1_NAME']) ? $company['CO1_NAME'] : 'All Companies';

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, $headerText . "\n" . $CO1_NAME . ( $LO1_NAME != '' ? ' - ' . $LO1_NAME : '') . "\n" . Date('m/d/Y'), 0, 'C', 0, 0, $this->xPos, $this->yPos, true);

		$this->yPos = $this->yPos + ($this->lineHeight * 6);

		return $pdf;
	}

	private function _printInventoryLineHeader($pdf) {
		$this->yPos += $this->lineHeight;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.18, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.48, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.22, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.77, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.18, $this->lineHeight, 'Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->lineHeight, 'Desc', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.48);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.22, $this->lineHeight, 'Vendor', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70);
        $pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'On Hand', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.77);
        $pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'Min', 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'Receipt', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'In Stock', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos = $this->yPos + $this->lineHeight;

		return $pdf;
	}

	private function _printInventoryFooter($pdf, $inventoryTotalValue) {
		$this->yPos += 4;

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, "Total In Stock:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($inventoryTotalValue, ['decimal', 2]), 0, 'R', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89, $this->yPos + $this->lineHeight - 2, true);

		return $pdf;
	}

	private function _printInventoryLine($pdf, $line) {
        $descriptionlength = strlen($line['MD1_DESC1']);
        $lineLength = $this->lineHeight;
        $numberOfLines = ceil($descriptionlength / 45);
        $lineLength *= $numberOfLines;

		$pdf = $this->_checkPage($pdf, '_printInventoryLineHeader', false, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.18,  $lineLength, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.30, $lineLength, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.48, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.22, $lineLength, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $lineLength, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.77, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $lineLength, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $lineLength, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $lineLength, 'D');

		$pdf->SetFont('Times', '', 8);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.18, $lineLength, $line['MD1_PART_NUMBER'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18);
		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.30, $lineLength*0.8, $line['MD1_DESC1'], 0, 'L', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18, $this->yPos, true);

		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.48);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.22, $lineLength, $line['VD1_NAME'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70);
        $pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $lineLength, $line['MD2_ON_HAND_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.77);
        $pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $lineLength, $line['MD2_MIN_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $lineLength, $line['UM1_NAME'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $lineLength, Yii::$app->formatter->format($line['EXTENDED_STOCK'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $lineLength;

		return $pdf;
	}

	// Purchase Orders
	public function printPurchaseOrders($params) {
		$orderHeaderModel = new PurchaseOrderModel();
		$query = $orderHeaderModel->getQuery( $params );
		$query->asArray();

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => false
		]);

		$orderHeaders = $dataProvider->getModels();

		$model = new PurchaseOrderLineModel();
		$orderLines = $model->getPdfOrderLines($params);

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(0, 0, 0, 0);

		$categories = [];
		foreach ($orderLines as &$record) {

			$contained = false;
			foreach($categories as &$category) {
				if($category["CA1_ID"] == $record["CA1_ID"] && $category['PO1_ID'] == $record['PO1_ID']) {
					$category["CA1_SUBTOTAL"] += $record['PO2_TOTAL_VALUE'];
					$contained = true;
				}
			}

			if(!$contained) {
				$categories[] = [
				    'CA1_ID'       => $record["CA1_ID"],
					'PO1_ID'       => $record['PO1_ID'],
					'CA1_NAME'     => $record["CA1_NAME"],
					'CA1_SUBTOTAL' => $record['PO2_TOTAL_VALUE']
				];
			}
		}

		foreach($orderHeaders as $header) {
			$pdf = $this->_printPOHeader($pdf, $header);

			foreach($categories as $category1) {
				if ($header['PO1_ID'] == $category1['PO1_ID']) {
					$pdf = $this->_printPOLineHeader($pdf);
					for($i = 0; $i < sizeof($orderLines); $i++) {
						if($orderLines[$i]["CA1_ID"] == $category1["CA1_ID"] && $orderLines[$i]["PO1_ID"] == $header["PO1_ID"]) {
							$pdf = $this->_printPOLine($pdf, $orderLines[$i]);
						}
					}
				}
			}
			$pdf = $this->_printPOFooter($pdf, $header, $categories);
		}

		return $pdf;
	}

	private function _printPOHeader($pdf, $header) {
		$pdf->AddPage('P', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, 'PURCHASE ORDER #' . $header["PO1_NUMBER"], 0, 'C', 0, 0, $this->xPos, $this->yPos, true);
		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*7, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*7, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos);

		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'Vendor:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$fromString = $header['VEND_NAME'] . "\n" . $header['VEND_ADDRESS1'] .
			(strlen($header['VEND_ADDRESS2']) > 0 ? ", " . $header['VEND_ADDRESS2'] . "\n" : "\n") .
			$header['VEND_CITY'] . ", " . $header['VEND_STATE'] . " " . $header['VEND_ZIP'] . "\n" .
			(strlen($header['VEND_COUNTRY']) > 0 ? $header['VEND_COUNTRY'] . "\n" : "") .
			(strlen($header['VEND_PHONE']) > 0 ? "Phone: " . $header['VEND_PHONE'] . "\n" : "") .
			(strlen($header['VEND_FAX']) > 0 ? "Fax: " . $header['VEND_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*0.8, $fromString, 0, 'L', 0, 0,
			$this->xPos, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight, 'Ordered By:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$toString = $header['CO1_NAME'] . "\n" . $header['LO1_NAME'] .
			(strlen($header['VEND_CUSTOMER_NO']) > 0 ? " - " . $header['VEND_CUSTOMER_NO'] : "") . "\n" .
			$header['LO1_ADDRESS1'] .
			(strlen($header['LO1_ADDRESS2']) > 0 ? ", " . $header['LO1_ADDRESS2'] . "\n" : "\n") .
			$header['LO1_CITY'] . ", " . $header['LO1_STATE'] . " " . $header['LO1_ZIP'] . "\n" .
			(strlen($header['LO1_COUNTRY']) > 0 ? $header['LO1_COUNTRY'] . "\n" : "") .
			(strlen($header['LO1_PHONE']) > 0 ? "Phone: " . $header['LO1_PHONE'] . "\n" : "") .
			(strlen($header['LO1_FAX']) > 0 ? "Fax: " . $header['LO1_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*0.8, $toString, 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos + $this->lineHeight*3, true);

		$this->yPos = $this->yPos + $this->lineHeight * 11;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()/2 - $this->xPos*2 -2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*1, ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos, ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos + $this->lineHeight*1, ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + $pdf->getPageWidth()/2 + 2, $this->yPos, $pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + $pdf->getPageWidth()/2 + 2, $this->yPos + $this->lineHeight*1, $pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*3, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + 2);
		$pdf->SetX($this->xPos + $pdf->getPageWidth()/2 + 2);
		$pdf->Cell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'Comment:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*0.8, $header['PO1_COMMENT'], 0, 'L', 0, 0, $this->xPos + $pdf->getPageWidth()/2 +2, $this->yPos + $this->lineHeight, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + 2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->lineHeight*2, 'Ordered On:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight*0.8, $header['CREATED_ON'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight + 2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->lineHeight*2, 'Shipping Method:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight*0.8, $header['PO1_SHIPPING_METHOD'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos + $this->lineHeight, true);

		$this->yPos = $this->yPos + ($this->lineHeight * 6);

		return $pdf;
	}

	private function _printPOLineHeader($pdf) {
		$this->yPos += $this->lineHeight;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.27, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.38, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.62, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Line', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.1, $this->lineHeight, 'Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'Vendor Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.27);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'Category', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.38);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, 'Description', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.62);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'GL Code', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Qty', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.1, $this->lineHeight, 'Price', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'UoM', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'Total', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight;

		return $pdf;
	}

	private function _printPOLine($pdf, $line) {
		$pdf = $this->_checkPage($pdf, '_printPOLineHeader', true, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.27, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.38, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.62, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight*2, 'D');

		$pdf->SetFont('Times', '', 9);
		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['PO2_LINE'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.1, $this->lineHeight, $line['PO2_PART_NUMBER'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, true);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, $line['MD1_VENDOR_PART_NUMBER'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, true);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, $line['CA1_NAME'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.27, $this->yPos, true);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, $line['PO2_DESC1'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.38, $this->yPos, true);

		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.62);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, $line['GL1_NAME'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, Yii::$app->formatter->format($line['PO2_ORDER_QTY'], ['integer']), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, '$'. Yii::$app->formatter->format($line['PO2_UNIT_PRICE'], ['decimal', 5]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['UM1_SHORT_CODE'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($line['PO2_TOTAL_VALUE'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight*2;

		return $pdf;
	}

	private function _printPOFooter($pdf, $header, $categories) {
		$this->yPos += $this->lineHeight;

		$pdf = $this->_checkPage($pdf, '_printPOLineHeader', false, 'P', $this->PADDING_LEFT , $this->PADDING_TOP);

		foreach($categories as $cat) {
			if ($cat['PO1_ID'] == $header['PO1_ID']) {
				$pdf->SetFont('Times', '', 9);
				$pdf->SetY($this->yPos);
				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, $cat['CA1_NAME'] . " Subtotal:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($cat['CA1_SUBTOTAL'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

				$this->yPos += $this->lineHeight;
				$pdf = $this->_checkPage($pdf, '_printPOLineHeader', false, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);
			}
		}

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, "Order Total:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($header['PO1_TOTAL_VALUE'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		return $pdf;
	}

	// Statement
	public function printStatement($params) {
		$statementModel = new StatementModel();
		$query = $statementModel->getQuery( $params );

		$sumInvoiceHeaders = $query->asArray()->all();

		$model = new StatementLineModel();
		$invoiceGLCodes = $model->getInvoiceGLCodes($params);
		$sumInvoiceLines = $model->getPdfStatementLines($params);


		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(0, 0, 0, 0);

		foreach($sumInvoiceHeaders as $header) {
			$pdf = $this->_printSumInvoiceHeader($pdf, $header);

			for($i = 0; $i < sizeof($sumInvoiceLines); $i++) {
				if ($i == 0) {
					$this->_printSumInvoiceLineHeader($pdf);
				}
				if($sumInvoiceLines[$i]["IS1_ID"] == $header["IS1_ID"]) {
					$pdf = $this->_printSumInvoiceLine($pdf, $sumInvoiceLines[$i]);
				}
			}

			$pdf = $this->_printSumInvoiceFooter($pdf, $header, $invoiceGLCodes);
		}

		return $pdf;
	}

	private function _printSumInvoiceHeader($pdf, $header) {
		$pdf->AddPage('P', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, 'Statement for Period ' . $header["IS1_COMMENT"], 0, 'C', 0, 0, $this->xPos, $this->yPos, true);
		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*7, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*7, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos);

		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'From:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$fromString = $header['CO1_NAME'] . "\n" . $header['FROM_NAME'] . "\n" . $header['FROM_ADDRESS1'] .
			(strlen($header['FROM_ADDRESS2']) > 0 ? ", " . $header['FROM_ADDRESS2'] . "\n" : "\n") .
			$header['FROM_CITY'] . ", " . $header['FROM_STATE'] . " " . $header['FROM_ZIP'] . "\n" .
			(strlen($header['FROM_COUNTRY']) > 0 ? $header['FROM_COUNTRY'] . "\n" : "") .
			(strlen($header['FROM_PHONE']) > 0 ? "Phone: " . $header['FROM_PHONE'] . "\n" : "") .
			(strlen($header['FROM_FAX']) > 0 ? "Fax: " . $header['FROM_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*0.8, $fromString, 0, 'L', 0, 0,
			$this->xPos, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight, 'To:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$toString = $header['CO1_NAME'] . "\n" . $header['TO_NAME'] . "\n" . $header['TO_ADDRESS1'] .
			(strlen($header['TO_ADDRESS2']) > 0 ? ", " . $header['TO_ADDRESS2'] . "\n" : "\n") .
			$header['TO_CITY'] . ", " . $header['TO_STATE'] . " " . $header['TO_ZIP'] . "\n" .
			(strlen($header['TO_COUNTRY']) > 0 ? $header['TO_COUNTRY'] . "\n" : "") .
			(strlen($header['TO_PHONE']) > 0 ? "Phone: " . $header['TO_PHONE'] . "\n" : "") .
			(strlen($header['TO_FAX']) > 0 ? "Fax: " . $header['TO_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->lineHeight*0.8, $toString, 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.5, $this->yPos + $this->lineHeight*3, true);

		$this->yPos = $this->yPos + $this->lineHeight * 11;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()/2 - $this->xPos*2 -2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos, ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + $pdf->getPageWidth()/2 + 2, $this->yPos, $pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + $pdf->getPageWidth()/2 + 2, $this->yPos + $this->lineHeight*1, $pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*3, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + 2);
		$pdf->SetX($this->xPos + $pdf->getPageWidth()/2 + 2);
		$pdf->Cell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'Comment:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*0.8, 'Statement #: ' . $header['IS1_NUMBER'], 0, 'L', 0, 0, $this->xPos + $pdf->getPageWidth()/2 +2, $this->yPos + $this->lineHeight, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + 2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->lineHeight*2, 'Stmt Created On:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight*0.8, $header['CREATED_ON'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos, true);

		$this->yPos = $this->yPos + ($this->lineHeight * 6);

		return $pdf;
	}

	private function _printSumInvoiceLineHeader($pdf) {
		$this->yPos += $this->lineHeight;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.39, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.54, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Line', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'Invoice #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, 'Comment', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.39);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'Ordering From', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.54);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'Ordered By', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, 'Total Amount', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'Invoiced On', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight;

		return $pdf;
	}

	private function _printSumInvoiceLine($pdf, $line) {
		$pdf = $this->_checkPage($pdf, '_printSumInvoiceLineHeader', true, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.39, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.54, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight*2, 'D');

		$pdf->SetFont('Times', '', 9);
		$pdf->SetY($this->yPos+2);

		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['IS2_LINE'] . ((float)$line['IS2_INVOICED_AMOUNT'] < 0 ? '(CR)' : ''), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, $line['IN1_NUMBER'], 0, false, 'L', 0, '', 0, false, 'M', 'M');


		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, $line['IN1_COMMENT'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->yPos, true);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, $line['LO1_FROM_NAME'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.39, $this->yPos, true);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, $line['LO1_TO_NAME'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.54, $this->yPos, true);

		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, '$'. Yii::$app->formatter->format($line['IS2_INVOICED_AMOUNT'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, $line['IN1_CREATED_ON'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight*2;

		return $pdf;
	}

	private function _printSumInvoiceFooter($pdf, $header, $invoiceGLCodes) {
		$this->yPos += $this->lineHeight;
		$pdf = $this->_checkPage($pdf, '_printSumInvoiceLineHeader', false, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);

		foreach($invoiceGLCodes as $glcode) {
			if ($glcode['IS1_ID'] == $header['IS1_ID']) {
				$pdf->SetFont('Times', '', 9);
				$pdf->SetY($this->yPos);
				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.44);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.45, $this->lineHeight, $glcode['GL1_NAME'] . ( strlen($glcode['GL1_DESCRIPTION']) > 0 ? (' (' . $glcode['GL1_DESCRIPTION'] . ')') : '') . " Subtotal:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($glcode['GL1_SUBTOTAL'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

				$this->yPos += $this->lineHeight;
				$pdf = $this->_checkPage($pdf, '_printSumInvoiceLineHeader', false, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);
			}
		}

		$this->yPos += $this->lineHeight;
		$pdf = $this->_checkPage($pdf, '_printSumInvoiceLineHeader', false, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, "Statement Total:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($header['IS1_TOTAL_VALUE'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		return $pdf;
	}

	// Store Orders / Tech Scans
	public function printStoreOrder($params, $type) {
		$orderHeaderModel = new OrderHeaderModel();
		$query = $orderHeaderModel->getQuery( $params, $type );
		$query->asArray();

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => false
		]);

		$orderHeaders = $dataProvider->getModels();

		$model = new OrderLineModel();
		$orderLines = $model->getPdfOrderLines($params);
		$categories = [];

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(0, 0, 0, 0);

		foreach ($orderLines as $record) {
			$contained = false;
			foreach($categories as &$cat) {
				if($cat["CA1_ID"] == $record["CA1_ID"] && $cat['OR1_ID'] == $record['OR1_ID']) {
					$cat["CA1_SUBTOTAL"] = $cat["CA1_SUBTOTAL"] + $record['OR2_TOTAL_SELL'];
					$contained = true;
				}
			}

			if(!$contained) {
				$newCat = [
				    'CA1_ID'       => $record["CA1_ID"],
					'OR1_ID'       => $record['OR1_ID'],
					'CA1_NAME'     => $record["CA1_NAME"],
					'CA1_SUBTOTAL' => $record['OR2_TOTAL_SELL']
				];
				$categories[] = $newCat;
			}
		}

		foreach($orderHeaders as $header) {
			$pdf = $this->_printOrderHeader($pdf, $header);

			foreach($categories as $category) {
				if ($header['OR1_ID'] == $category['OR1_ID']) {
					$pdf = $this->_printLineHeader($pdf);
					for($i = 0; $i < sizeof($orderLines); $i++) {
						if($orderLines[$i]["CA1_ID"] == $category["CA1_ID"]) {
							if($orderLines[$i]["OR1_ID"] == $header["OR1_ID"]) {
								$pdf = $this->_printOrderLine($pdf, $orderLines[$i]);
							}
						}
					}
				}
			}
			$pdf = $this->_printOrderFooter($pdf, $header, $categories);
		}

		return $pdf;
	}

	private function _printOrderHeader($pdf, $header) {
		$pdf->AddPage('P', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, (($header['LO1_TO_ID'] == $header['LO1_FROM_ID']) ? 'TECH ORDER #':'ORDER #') . $header["OR1_NUMBER"], 0, 'C', 0, 0, $this->xPos, $this->yPos, true);
		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*2, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*7, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*7, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*3, ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*7, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos);

		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'From:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$fromString = $header['CO1_NAME'] . "\n" . $header['FROM_NAME'] . "\n" . $header['FROM_ADDRESS1'] .
			(strlen($header['FROM_ADDRESS2']) > 0 ? ", " . $header['FROM_ADDRESS2'] . "\n" : "\n") .
			$header['FROM_CITY'] . ", " . $header['FROM_STATE'] . " " . $header['FROM_ZIP'] . "\n" .
			(strlen($header['FROM_COUNTRY']) > 0 ? $header['FROM_COUNTRY'] . "\n" : "") .
			(strlen($header['FROM_PHONE']) > 0 ? "Phone: " . $header['FROM_PHONE'] . "\n" : "") .
			(strlen($header['FROM_FAX']) > 0 ? "Fax: " . $header['FROM_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $fromString, 0, 'L', 0, 0, $this->xPos, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'Vendor:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$vendString = $header['CO1_NAME'] . "\n" . $header['VEND_NAME'] . "\n" . $header['VEND_ADDRESS1'] .
			(strlen($header['VEND_ADDRESS2']) > 0 ? ", " . $header['VEND_ADDRESS2'] . "\n" : "\n") .
			$header['VEND_CITY'] . ", " . $header['VEND_STATE'] . " " . $header['VEND_ZIP'] . "\n" .
			(strlen($header['VEND_COUNTRY']) > 0 ? $header['VEND_COUNTRY'] . "\n" : "") .
			(strlen($header['VEND_PHONE']) > 0 ? "Phone: " . $header['VEND_PHONE'] . "\n" : "") .
			(strlen($header['VEND_FAX']) > 0 ? "Fax: " . $header['VEND_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $vendString, 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetY($this->yPos + $this->lineHeight*2 +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666);
		$pdf->Cell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight, 'To:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$toString = $header['CO1_NAME'] . "\n" . $header['TO_NAME'] . "\n" . $header['TO_ADDRESS1'] .
			(strlen($header['TO_ADDRESS2']) > 0 ? ", " . $header['TO_ADDRESS2'] . "\n" : "\n") .
			$header['TO_CITY'] . ", " . $header['TO_STATE'] . " " . $header['TO_ZIP'] . "\n" .
			(strlen($header['TO_COUNTRY']) > 0 ? $header['TO_COUNTRY'] . "\n" : "") .
			(strlen($header['TO_PHONE']) > 0 ? "Phone: " . $header['TO_PHONE'] . "\n" : "") .
			(strlen($header['TO_FAX']) > 0 ? "Fax: " . $header['TO_FAX'] . "\n" : "");

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth() - $this->xPos*2)*0.3333, $this->lineHeight*0.8, $toString, 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth() - $this->xPos*2)*0.6666, $this->yPos + $this->lineHeight*3, true);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$this->yPos = $this->yPos + $this->lineHeight * 11;

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()/2 - $this->xPos*2 -2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos, $this->yPos + $this->lineHeight*1, ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos, ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos + $this->lineHeight*1, ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + $pdf->getPageWidth()/2 + 2, $this->yPos, $pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + $pdf->getPageWidth()/2 + 2, $this->yPos + $this->lineHeight*1, $pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*3, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + $pdf->getPageWidth()/2 + 2);
		$pdf->Cell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight, 'Comment:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell($pdf->getPageWidth()/2 - $this->xPos*2 - 2, $this->lineHeight*0.8, $header['OR1_COMMENT'], 0, 'L', 0, 0, $this->xPos + $pdf->getPageWidth()/2 +2, $this->yPos + $this->lineHeight, true);

		if ($header['LO1_TO_ID'] == $header['LO1_FROM_ID']) {
			$pdf->SetFont('Times', 'B', 9);
			$pdf->SetY($this->yPos +2);
			$pdf->SetX($this->xPos);
			$pdf->Cell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->lineHeight, 'Technician:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetFont('Times', '', 9);
			$pdf->MultiCell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight*0.8, $header['TE1_SHORT_CODE'] . ' - ' . $header['TE1_NAME'], 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos, true);
		}

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos + $this->lineHeight +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->lineHeight, 'Ordered On:', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetFont('Times', '', 9);
		$pdf->MultiCell(($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.65, $this->lineHeight*0.8, $header['CREATED_ON'], 0, 'L', 0, 0,
			$this->xPos + ($pdf->getPageWidth()/2 - $this->xPos*2 - 2)*0.35, $this->yPos + $this->lineHeight, true);


		$this->yPos = $this->yPos + ($this->lineHeight * 6);

		return $pdf;
	}

	private function _printLineHeader($pdf) {
		$this->yPos += $this->lineHeight;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.45, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Line', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, 'Vendor Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'Category', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.45);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, 'Description', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Qty', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'Price', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'UoM', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'Total', 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight;

		return $pdf;
	}

	private function _printOrderLine($pdf, $line) {
		$pdf = $this->_checkPage($pdf, '_printLineHeader', true, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.45, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight*2, 'D');

		$pdf->SetFont('Times', '', 9);
		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['OR2_LINE'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.05);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, $line['MD1_PART_NUMBER'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, $line['MD1_VENDOR_PART_NUMBER'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, $line['CA1_NAME'], 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, true);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.24, $this->lineHeight, $line['MD1_DESC1'], 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.45, $this->yPos, true);

		$pdf->SetY($this->yPos+2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.69);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, Yii::$app->formatter->format($line['OR2_ORDER_QTY'], ['integer']), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, '$'. Yii::$app->formatter->format($line['MD1_UNIT_PRICE'], ['decimal', 5]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['UM1_SHORT_CODE'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($line['OR2_TOTAL_SELL'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight*2;

		return $pdf;
	}

	private function _printOrderFooter($pdf, $header, $categories) {
		$this->yPos += 4;

		foreach($categories as $cat) {
			if ($cat['OR1_ID'] == $header['OR1_ID']) {
				$pdf->SetFont('Times', '', 9);
				$pdf->SetY($this->yPos);
				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.34);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.55, $this->lineHeight, $cat['CA1_NAME'] . " Subtotal:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($cat['CA1_SUBTOTAL'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

				$this->yPos += $this->lineHeight;
				$pdf = $this->_checkPage($pdf, '_printLineHeader', false, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);
			}
		}

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.74);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.15, $this->lineHeight, "Order Total:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.89);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($header['OR1_TOTAL_VALUE'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		return $pdf;
	}

	private function _checkPage($pdf, $callback_func, $line = false, $orientation = 'L', $xPos = 10, $yPos = 10, $params = null) {
		if ( $this->yPos + ($line ? $this->lineHeight*2 : $this->lineHeight) > $pdf->getPageHeight() - $this->PADDING_BOTTOM) {
			$pdf->AddPage($orientation, 'LETTER');
			$this->yPos = $yPos;
			$this->xPos = $xPos;
			$this->pageNum += 1;
			if ($line) {
				if ($params) {
					$pdf = $this->$callback_func($pdf, $params);
				} else {
					$pdf = $this->$callback_func($pdf);
				}
			}
		}

		return $pdf;
	}


	// Technicians
	public function printTechnician($params) {
		$technician = new Technician();
		$dataProvider = $technician->getAllTechnicians( $params );
		$dataProvider->query->asArray();
		$models = $dataProvider->getModels();

		$pdf = $this->_printTechnicianLabels($params, $models);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');

		return $pdf;
	}

	private function _printTechnicianLabels($params, $items)
	{
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->SetAutoPageBreak(TRUE, 0);

		$cols = 3;
		$numPages = ceil(sizeof($items) / ($cols * 10));

		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(10, $this->topMargin, 10, $this->bottomMargin);

		$this->xPos = 10;
		$this->yPos = 12;

		$itemWidth = ($pdf->getPageWidth() - 10 * $cols) / $cols;
		$itemHeight = ($pdf->getPageHeight() - 30) / 10;

		$barcodeWidth = ($pdf->getPageWidth() - 10 * $cols) / $cols;
		$barcodeHeight = 12;

		for ($pageIndex = 0; $pageIndex < $numPages; $pageIndex++) {
			$pdf->AddPage('P', 'LETTER');
			$pdf->SetY(10);
			$pdf->SetX(10);

			for ($rowIndex = 0; $rowIndex < 10; $rowIndex++) {
				if ((0 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex)) < sizeof($items) && $items[(0 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex))]) {
					if ((0 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex)) < sizeof($items)) {
						$item = $items[(0 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex))];
						$pdf->SetY($this->yPos + $itemHeight * $rowIndex);
						$pdf->SetX($this->xPos + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, $item['TE1_SHORT_CODE'], 0, 1);

						$pdf->SetY($this->yPos + $itemHeight * $rowIndex + $this->lineHeight);
						$pdf->SetX($this->xPos + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, substr($item['TE1_NAME'], 0, 28), 0, 1);

						$label = 'TECH-' . $item['TE1_SHORT_CODE'];
						$pdf->write1DBarcode($label, 'C39', $this->xPos + 2, $this->yPos + $itemHeight * $rowIndex + $this->lineHeight * 2, $barcodeWidth, $barcodeHeight, 0.4, $this->styleLabels, 'L');
					}

					if ((1 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex)) < sizeof($items) && $cols > 1) {
						$item = $items[(1 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex))];
						$pdf->SetY($this->yPos + $itemHeight * $rowIndex);
						$pdf->SetX($this->xPos + ($itemWidth + 5) + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, $item['TE1_SHORT_CODE'], 0, 1);

						$pdf->SetY($this->yPos + $itemHeight * $rowIndex + $this->lineHeight);
						$pdf->SetX($this->xPos + ($itemWidth + 5) + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, substr($item['TE1_NAME'], 0, 28), 0, 1);

						$label = 'TECH-' . $item['TE1_SHORT_CODE'];
						$pdf->write1DBarcode($label, 'C39', $this->xPos + ($itemWidth + 5) + 2, $this->yPos + $itemHeight * $rowIndex + $this->lineHeight * 2, $barcodeWidth, $barcodeHeight, 0.4, $this->styleLabels, 'L');
					}

					if ((2 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex)) < sizeof($items) && $cols > 2) {
						$item = $items[(2 + ($rowIndex * $cols) + ($cols * 10 * $pageIndex))];
						$pdf->SetY($this->yPos + $itemHeight * $rowIndex);
						$pdf->SetX($this->xPos + (($itemWidth + 5) * 2) + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, $item['TE1_SHORT_CODE'], 0, 1);

						$pdf->SetY($this->yPos + $itemHeight * $rowIndex + $this->lineHeight);
						$pdf->SetX($this->xPos + (($itemWidth + 5) * 2) + 2);
						$pdf->Cell($itemWidth, $this->lineHeight, substr($item['TE1_NAME'], 0, 28), 0, 1);

						$label = 'TECH-' . $item['TE1_SHORT_CODE'];
						$pdf->write1DBarcode($label, 'C39', $this->xPos + (($itemWidth + 5) * 2) + 2, $this->yPos + $itemHeight * $rowIndex + $this->lineHeight * 2, $barcodeWidth, $barcodeHeight, 0.4, $this->styleLabels, 'L');
					}

				}
			}
		}

		return $pdf;
	}


	// Below Minimun Report
	public function prinBelowMinimum($params) {
		if (!empty($params['option'])) {
			$this->option = (int)$params['option'];
		}

		$this->xPos = 4;
		$this->PADDING_LEFT = 4;

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Times', '', 7);
		$pdf->SetMargins(0, 0, 0, 0);


		$headerText = 'BELOW MINIMUM INVENTORY REPORT';

		$pdf = $this->_printBelowHeader($pdf, $headerText);
		$pdf = $this->_printBelowLineHeader($pdf);

		$masterDataModel = new MasterDataModel();
		$inventoryLinesQuery = $masterDataModel->getSearchQuery( $params, false );
		$inventoryLines = $inventoryLinesQuery->asArray()->all();

		foreach($inventoryLines as $line) {
			$pdf = $this->_printBelowLine($pdf, $line);
		}

		return $pdf;
	}

	private function _printBelowHeader($pdf, $headerText) {
		$pdf->AddPage('P', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$user = Yii::$app->user->getIdentity();
		$LO1_ID = $user->LO1_ID;
		$CO1_ID = $user->CO1_ID;

		$locationModel = new Location();
		$queryLocation = $locationModel->getSearchQuery(['LO1_ID' => $LO1_ID]);
		$location = $queryLocation->asArray()->one();
		$LO1_NAME = isset($location['LO1_NAME']) ? $location['LO1_NAME'] : '';

		$companyModel = new Company();
		$queryCompany = $companyModel->getSearchQuery(['CO1_ID' => $CO1_ID]);
		$company = $queryCompany->asArray()->one();
		$CO1_NAME = isset($company['CO1_NAME']) ? $company['CO1_NAME'] : 'All Companies';

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, $headerText . "\n" . $CO1_NAME . ( $LO1_NAME != '' ? ' - ' . $LO1_NAME : '') . "\n" . Date('m/d/Y'), 0, 'C', 0, 0, $this->xPos, $this->yPos, true);

		$this->yPos = $this->yPos + ($this->lineHeight * 6);

		return $pdf;
	}

	private function _printBelowLineHeader($pdf) {
		$this->yPos += $this->lineHeight;

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos,                                             $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.51, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.56, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.61, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.66, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.71, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.78, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*2, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*2, 'D');

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'Vendor', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, 'Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'Vendor Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'OEM ', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, 'Desc', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*0.8, 'Unit Price', 0, 'R', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.51, $this->yPos, true, 0, false, true, $this->lineHeight*2);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*0.8, 'On Hand', 0, 'R', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.56, $this->yPos, true, 0, false, true, $this->lineHeight*2);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*0.8, 'Avai lable', 0, 'R', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.61, $this->yPos, true, 0, false, true, $this->lineHeight*2);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*0.8, 'Min QTY', 0, 'R', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.66, $this->yPos, true, 0, false, true, $this->lineHeight*2);

		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.71);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'Rack', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight*0.8, 'Pricing UM', 0, 'L', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.78, $this->yPos, true, 0, false, true, $this->lineHeight*2);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*0.8, 'Nuventory Category', 0, 'L', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, true, 0, false, true, $this->lineHeight*2);

		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*0.8, 'Company Category', 0, 'L', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92, $this->yPos, true, 0, false, true, $this->lineHeight*2);

		$this->yPos = $this->yPos + $this->lineHeight*2;

		return $pdf;
	}

	private function _printBelowLine($pdf, $line) {
		$pdf = $this->_checkPage($pdf, '_printInventoryLineHeader', false, 'P', $this->PADDING_LEFT, $this->PADDING_TOP);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$pdf->Rect($this->xPos,                                             $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.51, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.56, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.61, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.66, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.71, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.78, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
		$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');


		$pdf->SetFont('Times', '', 8);
		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos);
		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*0.8, $line['VD1_NAME'], 0, 'L', 0, 1,
			$this->xPos, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10);
		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*0.8, $line['MD1_PART_NUMBER'], 0, 'L', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20);
		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*0.8, $line['MD1_VENDOR_PART_NUMBER'], 0, 'L', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['MD1_OEM_NUMBER'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35);
		$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight*0.8, $line['MD1_DESC1'], 0, 'L', 0, 1,
			$this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.35, $this->yPos, true, 0, false, true, $this->lineHeight);

		$pdf->SetY($this->yPos +2);
		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.51);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, Yii::$app->formatter->format($line['MD1_UNIT_PRICE'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.56);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['MD2_ON_HAND_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.61);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['MD2_AVAILABLE_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.66);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, $line['MD2_MIN_QTY'], 0, false, 'R', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.71);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, $line['MD2_RACK'] . ' - '. $line['MD2_DRAWER'] . ' - '. $line['MD2_BIN'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.78);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, $line['UM1_PRICE_NAME'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.84);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, $line['C00_NAME'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.92);
		$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, $line['CA1_NAME'], 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$this->yPos += $this->lineHeight;

		return $pdf;
	}

	// VOC Usage, Inventory Usage, Tech Usage
	public function printUsage($params) {
		$masterDataModel = new MasterData();
		$query = $masterDataModel->getVOCUsageQuery( $params );

		$usageLines = $query->asArray()->all();
		$categories = [];

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'LETTER', true, 'windows-1252', false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetAutoPageBreak(TRUE, 0);
		$pdf->SetCreator('');
		$pdf->SetAuthor('');
		$pdf->SetTitle('');
		$pdf->SetSubject('');
		$pdf->SetFont('Courier', '', 9);
		$pdf->SetMargins(0, 0, 0, 0);
		$isVOC = isset($params['isVOC']) && $params['isVOC'] ? true : false;

		$isSinglePage = true;

		foreach ($usageLines as $record) {
			$contained = false;
			foreach($categories as &$cat) {
				if ($isVOC) {
					if($cat["CA1_ID"] == $record["MD1_PART_NUMBER"]) {
						$cat["CA1_SUBTOTAL"] = $cat["CA1_SUBTOTAL"] + $record['LINE_QTY'];
						$contained = true;
					}
				} else {
					if($cat["CA1_ID"] == $record["CA1_ID"]) {
						$cat["CA1_SUBTOTAL"] = $cat["CA1_SUBTOTAL"] + $record['TOTAL_VALUE'];
						$contained = true;
					}
				}
			}

			if (!$contained) {
				$newCat = [
					'CA1_ID'       => $isVOC ? $record["MD1_PART_NUMBER"] : $record["CA1_ID"],
					'CA1_NAME'     => $isVOC ? $record["MD1_DESC1"] : $record["CA1_NAME"],
					'CA1_SUBTOTAL' => $isVOC ? $record["LINE_QTY"] : $record['TOTAL_VALUE']
				];
				$categories[] = $newCat;
			}
		}

		$usageTotalValue = 0;
		$usageTotalUsage = 0;
		$pdf = $this->_printUsageHeader($pdf, $params);

		if ($isVOC) {
			$partNumber = "";
			for($i = 0; $i < sizeof($usageLines); $i++) {
				if($usageLines[$i]["MD1_PART_NUMBER"] !== $partNumber) {
					if ($i !== 0) {
						$pdf = $this->_printUsageFooter($pdf, $params, $categories, $usageLines[$i-1]["MD1_PART_NUMBER"], $usageTotalValue);
						if (!$isSinglePage) {
							$pdf->AddPage('L', 'LETTER');
							$this->pageNum++;
							$this->yPos = $this->PADDING_TOP;
							$this->xPos = $this->PADDING_LEFT;
						}
					}
					$partNumber = $usageLines[$i]["MD1_PART_NUMBER"];
					$pdf = $this->_printUsageLineHeader($pdf, $params);
					$usageTotalValue = 0;
				}
				$pdf = $this->_printUsageLine($pdf, $params, $usageLines[$i]);
				$usageTotalValue += floatval($usageLines[$i]['TOTAL_VALUE']);
				$usageTotalUsage += floatval($usageLines[$i]['LINE_QTY']);
				if ($i == sizeof($usageLines) - 1) {
					$pdf = $this->_printUsageFooter($pdf, $params, $categories, $usageLines[$i]["MD1_PART_NUMBER"], $usageTotalValue);
				}
			}
			$pdf = $this->_printUsageFooter($pdf, $params, $categories,'$$VOC_TOTAL', $usageTotalValue);
		} else {
			$pdf = $this->_printUsageLineHeader($pdf, $params);
			for($i = 0; $i < sizeof($usageLines); $i++) {
				$pdf = $this->_printUsageLineHeader($pdf, $params);
			}
			$pdf = $this->_printUsageFooter($pdf, $params, $categories, "", $usageTotalValue);
		}

		return $pdf;
	}

	private function _printUsageHeader($pdf, $params) {
		$pdf->AddPage('L', 'LETTER');
		$this->pageNum++;
		$this->yPos = $this->PADDING_TOP;
		$this->xPos = $this->PADDING_LEFT;

		$isVOC = isset($params['isVOC']) && $params['isVOC'] ? true : false;
		$noUsage = isset($params['noUsage']) && $params['noUsage'] ? true : false;
		$isTechUsage = isset($params['isTechUsage']) && $params['isTechUsage'] ? true : false;

		$user = Yii::$app->user->getIdentity();

		$companyModel = new Company();
		$CO1_ID = $user->CO1_ID;
		$queryCompany = $companyModel->getSearchQuery(['CO1_ID' => $CO1_ID]);
		$company = $queryCompany->asArray()->one();
		$CO1_NAME = isset($company['CO1_NAME']) ? $company['CO1_NAME'] : 'All Companies';

		$searchQuery = isset($params['SEARCH']) && $params['SEARCH'] ? $params['SEARCH'] : false;
		$headerLabel = ($isVOC ? "VOC USAGE REPORT" : ($isTechUsage ? "TECH USAGE REPORT" : ($noUsage ? "NO USAGE REPORT":"USAGE REPORT"))) . ($searchQuery ? " - Query: \"" . $searchQuery . "\":\n" : "\n") . ($company ? ($CO1_NAME . "\n") : "") . $params['START_DATE'] . " - " . $params['END_DATE'];

		$pdf->SetFont('Times', 'B', 14);
		$pdf->MultiCell($pdf->getPageWidth() - $this->xPos*2, $this->lineHeight*0.8, $headerLabel, 0, 'C', 0, 0, $this->xPos, $this->yPos, true);
		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		$this->yPos = $this->yPos + ($this->lineHeight * ($company ? 4 : 3));

		return $pdf;
	}

	private function _printUsageLineHeader($pdf, $params) {
		$isVOC = isset($params['isVOC']) && $params['isVOC'] ? true : false;

		$this->yPos += $this->lineHeight;
		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));

		if ($isVOC) {
			$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.19, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.39, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.51, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.63, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.68, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.13, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.81, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.90, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.96, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, 'D');
		}
		else {
			$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.6, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.23, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.33, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.44, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.60, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.76, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.80, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.88, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.94, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
		}

		$pdf->SetFont('Times', 'B', 9);
		$pdf->SetY($this->yPos +2);

		if ($isVOC) {
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, "Date", 0, 'L', 0, 0, $this->xPos, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, "Technician", 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.19, $this->lineHeight, "Coating Category", 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, "Coating ID", 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.39, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, "Catalyst", 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.51, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, "Coating", 0, 'R', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.63, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.13, $this->lineHeight, "Catalyst by Weight", 0, 'R', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.68, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->lineHeight, "VOC Applied", 0, 'R', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.81, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, "GL/LBS", 0, 'L', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.90, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, "Qty", 0, 'R', 0, 0, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.96, $this->yPos, true);
		}
		else {
			$pdf->SetX($this->xPos);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'Type', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.06);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'Date', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'Order', 0, false, 'R', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.23);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'Line', 0, false, 'R', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.26);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'Technician', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.33);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'Part #', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.44);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, 'Desc', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.60);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'Vendor', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'Category', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.76);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, 'Qty', 0, false, 'R', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.80);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'Purchase UM', 0, false, 'L', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.88);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'Unit price', 0, false, 'R', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.94);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'Total Cost', 0, false, 'R', 0, '', 0, false, 'M', 'M');
		}

		$this->yPos += $this->lineHeight;

		return $pdf;
	}

	private function _printUsageLine($pdf, $params, $line) {
		$isVOC = isset($params['isVOC']) && $params['isVOC'] ? true : false;

		$pdf = $this->_checkPage($pdf, '_printUsageLineHeader', true, 'L', $this->PADDING_LEFT, $this->PADDING_TOP, $params);

		$pdf->SetLineStyle(array('width' => 0.2, 'height' => 0.3, 'color' => 0x000000 ));
		if ($isVOC) {
			$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.19, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.39, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.51, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.63, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.68, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.13, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.81, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.90, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.96, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, 'D');
		} else {
			$pdf->Rect($this->xPos, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.6, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.23, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.33, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.44, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.60, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.76, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.80, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.88, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
			$pdf->Rect($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.94, $this->yPos, ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight, 'D');
		}

		$pdf->SetFont('Times', '', 9);
		$pdf->SetY($this->yPos+2);

		if ($isVOC) {
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*0.8, $line['CREATED_ON'], 0, 'L', 0, 1, $this->xPos, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*0.8, $line['TE1_NAME'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.19, $this->lineHeight*0.8, $line['MD1_DESC1'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.20, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight*0.8, $line['MD1_PART_NUMBER'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.39, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight*0.8, $line['MD1_VOC_CATA'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.51, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*0.8, "1", 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.63, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.13, $this->lineHeight*0.8, "2%", 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.68, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.09, $this->lineHeight*0.8, Yii::$app->formatter->format($line['MD1_VOC_VALUE'], ['decimal', 5]), 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.81, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight*0.8, (int)$line['MD1_VOC_UNIT'] == 1 ? 'LBS':'G/L', 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.90, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight*0.8, $line['LINE_QTY'], 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.96, $this->yPos, true, 0, false, true, $this->lineHeight);
		} else {
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight*0.8, $line['LINE_TYPE'], 0, 'L', 0, 1, $this->xPos, $this->yPos, true);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.12, $this->lineHeight*0.8, $line['CREATED_ON'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.05, $this->lineHeight*0.8, $line['HEADER_NUMBER'], 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.18, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.03, $this->lineHeight*0.8, $line['LINE_NUMBER'], 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.23, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.07, $this->lineHeight*0.8, $line['TE1_NAME'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.26, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight*0.8, $line['MD1_PART_NUMBER'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.33, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.16, $this->lineHeight*0.8, $line['MD1_DESC1'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.44, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.10, $this->lineHeight*0.8, $line['VD1_NAME'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.60, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight*0.8, $line['CA1_NAME'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.70, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.04, $this->lineHeight*0.8, $line['LINE_QTY'], 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.76, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.08, $this->lineHeight*0.8, $line['UM1_NAME'], 0, 'L', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.80, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight*0.8, "$" . Yii::$app->formatter->format($line['UNIT_PRICE'], ['decimal', 5]), 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.88, $this->yPos, true, 0, false, true, $this->lineHeight);
			$pdf->MultiCell(($pdf->getPageWidth()-$this->xPos*2)*0.06, $this->lineHeight*0.8, "$" . Yii::$app->formatter->format($line['MD1_VOC_VALUE'], ['decimal', 5]), 0, 'R', 0, 1, $this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.94, $this->yPos, true, 0, false, true, $this->lineHeight);
		}

		$this->yPos += $this->lineHeight;

		return $pdf;
	}

	private function _printUsageFooter($pdf, $params, $categories, $limiter, $usageTotalValue) {
		$isVOC = isset($params['isVOC']) && $params['isVOC'] ? true : false;
		$isTechUsage = isset($params['isTechUsage']) && $params['isTechUsage'] ? true : false;

		$this->yPos += 4;

		if ($isVOC || $isTechUsage) {
			$catCount = 0;
			foreach($categories as $cat) {
				if (strlen($limiter) > 0 && $limiter !== $cat['CA1_ID']) {
					continue;
				} else {
					$catCount ++;
				}
			}
			$pdf = $this->_checkUsagePage($pdf, '_printUsageFooter', $catCount, 'L', $this->PADDING_LEFT, $this->PADDING_TOP, $params, $categories, $limiter, $usageTotalValue);

			foreach($categories as $cat) {
				if (strlen($limiter) > 0 && $limiter !== $cat['CA1_ID'])
					continue;

				$pdf->SetFont('Times', '', 9);
				$pdf->SetY($this->yPos);
				$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.30);
				$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.55, $this->lineHeight, $cat['CA1_NAME'] . " Subtotal:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

				if ($isVOC) {
					$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85);
					$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, Yii::$app->formatter->format($cat['CA1_SUBTOTAL'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');
				} else {
					$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85);
					$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, '$'. Yii::$app->formatter->format($cat['CA1_SUBTOTAL'], ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');
				}


				$this->yPos += $this->lineHeight;
				$pdf = $this->_checkUsagePage($pdf, '_printUsageFooter', $catCount, 'L', $this->PADDING_LEFT, $this->PADDING_TOP, $params, $categories, $limiter, $usageTotalValue);
			}


		}

		if (strlen($limiter) == 0 || $limiter == '$$VOC_TOTAL') {
			$pdf->SetFont('Times', 'B', 9);
			$pdf->SetY($this->yPos);
			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.60);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.25, $this->lineHeight, "Total Value:", 0, false, 'R', 0, '', 0, false, 'M', 'M');

			$pdf->SetX($this->xPos + ($pdf->getPageWidth()-$this->xPos*2)*0.85);
			$pdf->Cell(($pdf->getPageWidth()-$this->xPos*2)*0.11, $this->lineHeight, "$" . Yii::$app->formatter->format($usageTotalValue, ['decimal', 2]), 0, false, 'R', 0, '', 0, false, 'M', 'M');
		}

		return $pdf;
	}

	private function _checkUsagePage($pdf, $callback_func, $lines = 1, $orientation = 'L', $xPos = 10, $yPos = 10, $params = null, $categories = null, $limiter = null, $usageTotalValue = null) {
		if ( $this->yPos + ($lines ? $this->lineHeight* $lines : $this->lineHeight) > $pdf->getPageHeight() - $this->PADDING_BOTTOM) {
			$pdf->AddPage($orientation, 'LETTER');
			$this->yPos = $yPos;
			$this->xPos = $xPos;
			$this->pageNum += 1;
			if ($lines) {
				$pdf = $this->$callback_func($pdf, $params, $categories, $limiter, $usageTotalValue);
			}
		}

		return $pdf;
	}

}